.class public Lcom/android/calendar/event/ay;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field public static final aA:Ljava/util/Formatter;

.field private static final aN:[Ljava/lang/String;

.field public static ar:Z

.field public static final az:Ljava/lang/StringBuilder;

.field private static final bo:I

.field private static final bp:I

.field private static cZ:[Landroid/text/InputFilter;

.field private static ci:I

.field private static cj:I


# instance fields
.field A:Landroid/widget/TextView;

.field B:Landroid/widget/TextView;

.field C:Landroid/widget/TextView;

.field D:Landroid/widget/TextView;

.field E:Landroid/widget/TextView;

.field F:Landroid/widget/RadioGroup;

.field G:Landroid/view/View;

.field H:Landroid/widget/AutoCompleteTextView;

.field I:Landroid/widget/AutoCompleteTextView;

.field J:Landroid/widget/EditText;

.field K:Landroid/widget/TextView;

.field L:Landroid/widget/TextView;

.field M:Landroid/widget/EditText;

.field N:Landroid/widget/LinearLayout;

.field O:[Z

.field P:Landroid/widget/TextView;

.field Q:Landroid/widget/TextView;

.field R:Landroid/widget/LinearLayout;

.field S:Landroid/widget/LinearLayout;

.field T:Landroid/widget/ImageButton;

.field U:Landroid/widget/MultiAutoCompleteTextView;

.field V:Landroid/widget/ImageButton;

.field W:Lcom/android/calendar/event/AttendeesView;

.field X:Lcom/android/calendar/event/fb;

.field Y:Landroid/view/View;

.field Z:Landroid/view/View;

.field a:Landroid/widget/ScrollView;

.field public aB:J

.field public aC:Z

.field aD:Lcom/android/calendar/event/fc;

.field aE:Lcom/android/calendar/event/fd;

.field aF:Lcom/android/calendar/event/fm;

.field aG:Z

.field public aH:Lcom/android/calendar/d/g;

.field public aI:Ljava/lang/String;

.field public aJ:Ljava/util/ArrayList;

.field aK:Landroid/view/View$OnClickListener;

.field aL:Landroid/content/DialogInterface$OnClickListener;

.field private final aM:Ljava/lang/String;

.field private aO:Landroid/app/AlertDialog;

.field private aP:Landroid/app/Dialog;

.field private aQ:Landroid/app/AlertDialog;

.field private aR:Landroid/animation/AnimatorSet;

.field private aS:Landroid/animation/LayoutTransition;

.field private aT:Landroid/view/ViewGroup;

.field private aU:Landroid/widget/LinearLayout;

.field private aV:Landroid/widget/RadioButton;

.field private aW:Landroid/widget/RadioButton;

.field private aX:Landroid/widget/TextView;

.field private aY:Landroid/widget/TextView;

.field private aZ:Landroid/view/ViewGroup;

.field aa:Landroid/view/View;

.field ab:Landroid/view/View;

.field ac:Landroid/view/ViewStub;

.field ad:Landroid/view/View;

.field ae:Landroid/view/View;

.field af:Landroid/view/View;

.field ag:Landroid/view/View;

.field ah:Landroid/view/View;

.field public ai:Lcom/android/calendar/common/extension/a;

.field public aj:Lcom/android/calendar/common/extension/c;

.field public ak:Lcom/android/calendar/common/extension/k;

.field al:Ljava/util/ArrayList;

.field am:Landroid/widget/ImageButton;

.field an:Landroid/view/ViewStub;

.field ao:Landroid/view/View;

.field ap:Landroid/widget/ImageView;

.field aq:Landroid/widget/ImageButton;

.field public as:Z

.field public at:Landroid/app/Dialog;

.field public au:Z

.field public av:Lcom/android/calendar/al;

.field aw:[Ljava/lang/String;

.field ax:[Ljava/lang/String;

.field ay:[Ljava/lang/String;

.field b:Landroid/view/ViewStub;

.field private bA:Landroid/database/Cursor;

.field private bB:Ljava/lang/String;

.field private bC:Lcom/android/ex/a/a;

.field private bD:Lcom/android/b/b;

.field private bE:Lcom/android/calendar/timezone/v;

.field private bF:Z

.field private bG:Z

.field private bH:Z

.field private bI:Z

.field private bJ:Landroid/widget/ImageButton;

.field private final bK:Ljava/util/ArrayList;

.field private bL:Landroid/content/AsyncQueryHandler;

.field private bM:J

.field private bN:Ljava/util/ArrayList;

.field private bO:Ljava/util/ArrayList;

.field private bP:Ljava/util/ArrayList;

.field private bQ:Ljava/util/ArrayList;

.field private bR:Ljava/util/ArrayList;

.field private bS:I

.field private bT:Landroid/text/format/Time;

.field private bU:Landroid/text/format/Time;

.field private bV:Ljava/lang/String;

.field private bW:I

.field private final bX:Lcom/android/a/c;

.field private final bY:Ljava/util/ArrayList;

.field private final bZ:Ljava/util/ArrayList;

.field private ba:Landroid/widget/Spinner;

.field private bb:Landroid/widget/Button;

.field private bc:Landroid/widget/EditText;

.field private bd:I

.field private be:I

.field private bf:I

.field private bg:Lcom/android/calendar/gl;

.field private bh:Ljava/util/ArrayList;

.field private bi:Ljava/lang/String;

.field private bj:Ljava/lang/String;

.field private bk:Landroid/view/ViewGroup;

.field private bl:Landroid/widget/CheckBox;

.field private final bm:[I

.field private final bn:[Landroid/widget/ToggleButton;

.field private final bq:Z

.field private final br:[I

.field private bs:Z

.field private bt:Landroid/app/AlertDialog;

.field private bu:Landroid/app/Activity;

.field private bv:Lcom/android/calendar/event/fe;

.field private bw:Z

.field private bx:Lcom/android/calendar/event/ax;

.field private by:Landroid/view/View;

.field private bz:Lcom/android/calendar/as;

.field c:Landroid/view/View;

.field private cA:I

.field private cB:[Z

.field private final cC:Z

.field private cD:[Ljava/lang/String;

.field private cE:[Ljava/lang/String;

.field private cF:[Ljava/lang/String;

.field private cG:[Ljava/lang/CharSequence;

.field private cH:I

.field private cI:[Ljava/lang/CharSequence;

.field private cJ:I

.field private cK:I

.field private cL:I

.field private cM:Z

.field private cN:Lcom/android/calendar/d/a/a;

.field private cO:I

.field private cP:I

.field private cQ:I

.field private cR:Ljava/lang/String;

.field private cS:Ljava/lang/String;

.field private cT:Landroid/view/View;

.field private cU:[I

.field private cV:Landroid/app/PendingIntent;

.field private cW:Landroid/location/LocationManager;

.field private cX:Z

.field private cY:Z

.field private ca:Landroid/text/format/Time;

.field private cb:I

.field private cc:I

.field private cd:I

.field private ce:Z

.field private cf:I

.field private cg:I

.field private ch:Ljava/lang/String;

.field private final ck:Ljava/util/ArrayList;

.field private cl:[Ljava/lang/CharSequence;

.field private cm:I

.field private cn:I

.field private co:Ljava/lang/String;

.field private cp:Ljava/util/ArrayList;

.field private cq:Landroid/widget/ArrayAdapter;

.field private cr:I

.field private cs:Landroid/text/format/Time;

.field private ct:I

.field private cu:I

.field private cv:Ljava/lang/String;

.field private cw:I

.field private cx:I

.field private cy:I

.field private cz:Z

.field d:Landroid/widget/LinearLayout;

.field private da:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

.field e:Landroid/widget/ImageView;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/ImageButton;

.field h:Landroid/widget/LinearLayout;

.field i:Landroid/widget/ImageView;

.field j:Landroid/widget/LinearLayout;

.field k:Landroid/widget/LinearLayout;

.field l:Landroid/widget/TextView;

.field m:Landroid/widget/TextView;

.field n:Landroid/widget/TextView;

.field o:Landroid/widget/TextView;

.field p:Landroid/view/View;

.field q:Landroid/view/View;

.field r:Landroid/widget/CheckBox;

.field s:Landroid/widget/Button;

.field t:Landroid/widget/LinearLayout;

.field u:Landroid/widget/Spinner;

.field v:Landroid/widget/Spinner;

.field w:Landroid/widget/Spinner;

.field x:Landroid/widget/Spinner;

.field y:Landroid/widget/Spinner;

.field z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0xff

    const/16 v2, 0x6b

    const/4 v3, 0x0

    .line 209
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/event/ay;->aN:[Ljava/lang/String;

    .line 393
    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/android/calendar/event/ay;->bo:I

    .line 395
    invoke-static {v2, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/android/calendar/event/ay;->bp:I

    .line 491
    sput-boolean v3, Lcom/android/calendar/event/ay;->ar:Z

    .line 553
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    sput-object v0, Lcom/android/calendar/event/ay;->az:Ljava/lang/StringBuilder;

    .line 555
    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/android/calendar/event/ay;->az:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/calendar/event/ay;->aA:Ljava/util/Formatter;

    .line 593
    sput v3, Lcom/android/calendar/event/ay;->ci:I

    .line 595
    sput v3, Lcom/android/calendar/event/ay;->cj:I

    .line 4505
    new-array v0, v5, [Landroid/text/InputFilter;

    new-instance v1, Lcom/android/b/a;

    invoke-direct {v1}, Lcom/android/b/a;-><init>()V

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/calendar/event/ay;->cZ:[Landroid/text/InputFilter;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/android/calendar/event/ax;)V
    .locals 10

    .prologue
    const v9, 0x3f733333    # 0.95f

    const v8, 0x3f666666    # 0.9f

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    const-string v0, "extra_prefs_show_button_bar"

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aM:Ljava/lang/String;

    .line 243
    iput-object v7, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 245
    iput-object v7, p0, Lcom/android/calendar/event/ay;->aP:Landroid/app/Dialog;

    .line 247
    iput-object v7, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    .line 262
    iput-object v7, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    .line 347
    iput v1, p0, Lcom/android/calendar/event/ay;->bd:I

    .line 349
    iput v2, p0, Lcom/android/calendar/event/ay;->be:I

    .line 351
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/ay;->bf:I

    .line 355
    iput-object v7, p0, Lcom/android/calendar/event/ay;->bh:Ljava/util/ArrayList;

    .line 377
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bm:[I

    .line 389
    const/4 v0, 0x7

    new-array v0, v0, [Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    .line 391
    const/4 v0, 0x7

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/android/calendar/event/ay;->O:[Z

    .line 398
    iput-boolean v2, p0, Lcom/android/calendar/event/ay;->bq:Z

    .line 438
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/calendar/event/ay;->br:[I

    .line 440
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->bs:Z

    .line 442
    iput-object v7, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    .line 454
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->bw:Z

    .line 464
    iput-object v7, p0, Lcom/android/calendar/event/ay;->bB:Ljava/lang/String;

    .line 474
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->bG:Z

    .line 477
    iput-boolean v2, p0, Lcom/android/calendar/event/ay;->bH:Z

    .line 479
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    .line 493
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->bI:Z

    .line 495
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->as:Z

    .line 502
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->au:Z

    .line 506
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    .line 510
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/calendar/event/ay;->bM:J

    .line 516
    iput-object v7, p0, Lcom/android/calendar/event/ay;->ay:[Ljava/lang/String;

    .line 518
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bN:Ljava/util/ArrayList;

    .line 545
    iput v1, p0, Lcom/android/calendar/event/ay;->bW:I

    .line 547
    new-instance v0, Lcom/android/a/c;

    invoke-direct {v0}, Lcom/android/a/c;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    .line 549
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    .line 551
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bZ:Ljava/util/ArrayList;

    .line 558
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    .line 570
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/ay;->cb:I

    .line 572
    iput v1, p0, Lcom/android/calendar/event/ay;->cc:I

    .line 574
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/calendar/event/ay;->aB:J

    .line 580
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/ay;->cf:I

    .line 582
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/calendar/event/ay;->cg:I

    .line 584
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->aC:Z

    .line 589
    iput-object v7, p0, Lcom/android/calendar/event/ay;->aD:Lcom/android/calendar/event/fc;

    .line 591
    iput-object v7, p0, Lcom/android/calendar/event/ay;->aE:Lcom/android/calendar/event/fd;

    .line 597
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ck:Ljava/util/ArrayList;

    .line 600
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cl:[Ljava/lang/CharSequence;

    .line 602
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/event/ay;->cm:I

    .line 604
    iput v1, p0, Lcom/android/calendar/event/ay;->cn:I

    .line 613
    iput v1, p0, Lcom/android/calendar/event/ay;->cr:I

    .line 625
    const/4 v0, 0x7

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cB:[Z

    .line 627
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->cC:Z

    .line 628
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cD:[Ljava/lang/String;

    .line 629
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cE:[Ljava/lang/String;

    .line 630
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cF:[Ljava/lang/String;

    .line 644
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->aG:Z

    .line 647
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cG:[Ljava/lang/CharSequence;

    .line 649
    iput v1, p0, Lcom/android/calendar/event/ay;->cH:I

    .line 652
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cI:[Ljava/lang/CharSequence;

    .line 654
    iput v1, p0, Lcom/android/calendar/event/ay;->cJ:I

    .line 656
    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 658
    iput v1, p0, Lcom/android/calendar/event/ay;->cL:I

    .line 660
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->cM:Z

    .line 665
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    .line 667
    iput v1, p0, Lcom/android/calendar/event/ay;->cO:I

    .line 669
    iput-object v7, p0, Lcom/android/calendar/event/ay;->aI:Ljava/lang/String;

    .line 671
    iput v1, p0, Lcom/android/calendar/event/ay;->cP:I

    .line 672
    iput v1, p0, Lcom/android/calendar/event/ay;->cQ:I

    .line 674
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    .line 675
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    .line 677
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cT:Landroid/view/View;

    .line 679
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cU:[I

    .line 689
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cV:Landroid/app/PendingIntent;

    .line 692
    iput-object v7, p0, Lcom/android/calendar/event/ay;->cW:Landroid/location/LocationManager;

    .line 699
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->cX:Z

    .line 1929
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->cY:Z

    .line 2800
    new-instance v0, Lcom/android/calendar/event/bu;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/bu;-><init>(Lcom/android/calendar/event/ay;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aK:Landroid/view/View$OnClickListener;

    .line 3646
    new-instance v0, Lcom/android/calendar/event/cb;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/cb;-><init>(Lcom/android/calendar/event/ay;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aL:Landroid/content/DialogInterface$OnClickListener;

    .line 7827
    new-instance v0, Lcom/android/calendar/event/fa;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/fa;-><init>(Lcom/android/calendar/event/ay;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->da:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    .line 963
    iput-object p1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    .line 964
    iput-object p2, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    .line 965
    iput-object p3, p0, Lcom/android/calendar/event/ay;->bx:Lcom/android/calendar/event/ax;

    .line 966
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->av:Lcom/android/calendar/al;

    .line 968
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0f0030

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ch:Ljava/lang/String;

    .line 969
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0a000a

    invoke-static {v0, v3}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    .line 970
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0a0002

    invoke-static {v0, v3}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->bG:Z

    .line 971
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0a000b

    invoke-static {v0, v3}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->cX:Z

    .line 972
    new-instance v0, Lcom/android/calendar/event/fe;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/event/fe;-><init>(Lcom/android/calendar/event/ay;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bv:Lcom/android/calendar/event/fe;

    .line 974
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iput v2, v0, Lcom/android/a/c;->e:I

    .line 975
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iput v1, v0, Lcom/android/a/c;->d:I

    .line 976
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->P()V

    .line 978
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    iput v0, p0, Lcom/android/calendar/event/ay;->cL:I

    .line 981
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    .line 982
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 983
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    .line 987
    :cond_1
    invoke-static {p1, v7}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    .line 989
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    .line 990
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    .line 991
    new-instance v0, Lcom/android/b/b;

    invoke-direct {v0, v7}, Lcom/android/b/b;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    .line 994
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->F()V

    .line 998
    new-instance v3, Lcom/android/calendar/en;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v3, v0}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    .line 1001
    const v0, 0x7f1200e3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->a:Landroid/widget/ScrollView;

    .line 1004
    const v0, 0x7f1200e5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    .line 1005
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v0, :cond_17

    .line 1006
    const v0, 0x7f1200e7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->A:Landroid/widget/TextView;

    .line 1007
    const v0, 0x7f120093

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->z:Landroid/view/View;

    .line 1008
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    new-instance v4, Lcom/android/calendar/event/az;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/az;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1019
    :goto_1
    const v0, 0x7f12002d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    .line 1021
    iget v0, p0, Lcom/android/calendar/event/ay;->cL:I

    const/16 v4, 0x140

    if-gt v0, v4, :cond_2

    .line 1022
    const v0, 0x8c001

    .line 1025
    iget-object v4, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4, v0}, Landroid/widget/AutoCompleteTextView;->setRawInputType(I)V

    .line 1028
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    new-array v4, v2, [Landroid/text/InputFilter;

    aput-object v3, v4, v1

    invoke-virtual {v0, v4}, Landroid/widget/AutoCompleteTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 1032
    const v0, 0x7f1200e8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    .line 1034
    iget-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1035
    iget-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    new-instance v4, Lcom/android/calendar/event/bp;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/bp;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1043
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/AutoCompleteTextView;->setTag(Ljava/lang/Object;)V

    .line 1044
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    new-instance v4, Lcom/android/calendar/event/ca;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/ca;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1059
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    new-instance v4, Lcom/android/calendar/event/cl;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/cl;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1080
    const v0, 0x7f12003c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    .line 1081
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 1082
    const-string v4, "Eesti (Estonia)"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1083
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    const/high16 v4, 0x41880000    # 17.0f

    invoke-virtual {v0, v4}, Landroid/widget/AutoCompleteTextView;->setTextSize(F)V

    .line 1086
    :cond_4
    iget v0, p0, Lcom/android/calendar/event/ay;->cL:I

    const/16 v4, 0x140

    if-gt v0, v4, :cond_5

    .line 1087
    const v0, 0x8c001

    .line 1090
    iget-object v4, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4, v0}, Landroid/widget/AutoCompleteTextView;->setRawInputType(I)V

    .line 1093
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/AutoCompleteTextView;->setTag(Ljava/lang/Object;)V

    .line 1095
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    new-array v4, v2, [Landroid/text/InputFilter;

    aput-object v3, v4, v1

    invoke-virtual {v0, v4}, Landroid/widget/AutoCompleteTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 1099
    iget-object v0, p0, Lcom/android/calendar/event/ay;->br:[I

    iget-object v4, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->getPaddingLeft()I

    move-result v4

    aput v4, v0, v1

    .line 1100
    iget-object v0, p0, Lcom/android/calendar/event/ay;->br:[I

    iget-object v4, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->getPaddingTop()I

    move-result v4

    aput v4, v0, v2

    .line 1101
    iget-object v0, p0, Lcom/android/calendar/event/ay;->br:[I

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v5}, Landroid/widget/AutoCompleteTextView;->getPaddingRight()I

    move-result v5

    aput v5, v0, v4

    .line 1102
    iget-object v0, p0, Lcom/android/calendar/event/ay;->br:[I

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v5}, Landroid/widget/AutoCompleteTextView;->getPaddingBottom()I

    move-result v5

    aput v5, v0, v4

    .line 1105
    const v0, 0x7f1200eb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->an:Landroid/view/ViewStub;

    .line 1106
    const v0, 0x7f1200ea

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->am:Landroid/widget/ImageButton;

    .line 1107
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1108
    iget-object v0, p0, Lcom/android/calendar/event/ay;->am:Landroid/widget/ImageButton;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f01aa

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/event/ay;->ch:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1114
    :goto_2
    invoke-static {}, Lcom/android/calendar/dz;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/android/calendar/dz;->w()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1115
    iget-object v0, p0, Lcom/android/calendar/event/ay;->am:Landroid/widget/ImageButton;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1117
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->am:Landroid/widget/ImageButton;

    new-instance v4, Lcom/android/calendar/event/cx;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/cx;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1135
    const v0, 0x7f1200ec

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    .line 1136
    const v0, 0x7f1200ef

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    .line 1137
    const v0, 0x7f1200ee

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->l:Landroid/widget/TextView;

    .line 1138
    const v0, 0x7f1200ed

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->m:Landroid/widget/TextView;

    .line 1139
    const v0, 0x7f1200f1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->n:Landroid/widget/TextView;

    .line 1140
    const v0, 0x7f1200f0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->o:Landroid/widget/TextView;

    .line 1142
    iget v0, p0, Lcom/android/calendar/event/ay;->cL:I

    const/16 v4, 0x140

    if-gt v0, v4, :cond_7

    invoke-static {}, Lcom/android/calendar/hj;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1143
    iget-object v0, p0, Lcom/android/calendar/event/ay;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 1144
    iget-object v0, p0, Lcom/android/calendar/event/ay;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 1145
    iget-object v0, p0, Lcom/android/calendar/event/ay;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 1146
    iget-object v0, p0, Lcom/android/calendar/event/ay;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 1149
    :cond_7
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1150
    iget-object v0, p0, Lcom/android/calendar/event/ay;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 1151
    iget-object v0, p0, Lcom/android/calendar/event/ay;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 1152
    iget-object v0, p0, Lcom/android/calendar/event/ay;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 1153
    iget-object v0, p0, Lcom/android/calendar/event/ay;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 1156
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1157
    iget-object v0, p0, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setHoverPopupType(I)V

    .line 1158
    iget-object v0, p0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setHoverPopupType(I)V

    .line 1162
    :cond_9
    const v0, 0x7f1200d0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    .line 1163
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 1164
    iget-object v0, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    const v4, 0x7f1200ce

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cT:Landroid/view/View;

    .line 1165
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cT:Landroid/view/View;

    new-instance v4, Lcom/android/calendar/event/di;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/di;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1172
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1173
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setHoverPopupType(I)V

    .line 1177
    :cond_a
    const v0, 0x7f1200f2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->q:Landroid/view/View;

    .line 1178
    const v0, 0x7f1200f3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->Q:Landroid/widget/TextView;

    .line 1180
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1181
    iget-object v0, p0, Lcom/android/calendar/event/ay;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1185
    :cond_b
    const v0, 0x7f1200f4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    .line 1186
    const v0, 0x7f120112

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ac:Landroid/view/ViewStub;

    .line 1188
    const v0, 0x7f1200f8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    .line 1189
    const v0, 0x7f1200f5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    .line 1190
    const v0, 0x7f1200f7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->T:Landroid/widget/ImageButton;

    .line 1193
    const v0, 0x7f1200f9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->s:Landroid/widget/Button;

    .line 1196
    const v0, 0x7f1200fa

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->h:Landroid/widget/LinearLayout;

    .line 1199
    const v0, 0x7f1200fe

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->j(I)Landroid/widget/MultiAutoCompleteTextView;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    .line 1200
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Landroid/widget/MultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1202
    const v0, 0x7f1200fd

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    .line 1203
    const v0, 0x7f1200ff

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    .line 1204
    const v0, 0x7f1200fb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/AttendeesView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    .line 1205
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/AttendeesView;->setmAttendeesEditText(Landroid/widget/MultiAutoCompleteTextView;)V

    .line 1206
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    new-instance v4, Lcom/android/calendar/event/du;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/du;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/AttendeesView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1220
    const v0, 0x7f1200fc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    .line 1221
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    new-instance v4, Lcom/android/calendar/event/ef;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/ef;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1232
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    new-instance v4, Lcom/android/calendar/event/eq;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/eq;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/widget/MultiAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1290
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v4}, Landroid/widget/MultiAutoCompleteTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/MultiAutoCompleteTextView;->setTag(Ljava/lang/Object;)V

    .line 1292
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    new-array v4, v2, [Landroid/text/InputFilter;

    aput-object v3, v4, v1

    invoke-virtual {v0, v4}, Landroid/widget/MultiAutoCompleteTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 1296
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1297
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    new-instance v3, Lcom/android/calendar/event/ba;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/ba;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1393
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1394
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f01ab

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/ay;->ch:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1400
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->da:Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0, v3, v4}, Lcom/android/calendar/hj;->a(Landroid/view/View;Lcom/samsung/android/sdk/look/airbutton/SlookAirButton$ItemSelectListener;Landroid/app/Activity;)Lcom/samsung/android/sdk/look/airbutton/SlookAirButton;

    .line 1402
    new-instance v0, Lcom/android/calendar/event/fb;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/fb;-><init>(Lcom/android/calendar/event/ay;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->X:Lcom/android/calendar/event/fb;

    .line 1403
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->X:Lcom/android/calendar/event/fb;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/AttendeesView;->setOnCleanedListerner(Lcom/android/calendar/event/fb;)V

    .line 1406
    const v0, 0x7f120100

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->t:Landroid/widget/LinearLayout;

    .line 1408
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const v0, 0x7f09003c

    move v3, v0

    .line 1410
    :goto_4
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_1b

    .line 1411
    const v0, 0x7f12013d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->x:Landroid/widget/Spinner;

    .line 1412
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/android/calendar/event/ay;->b(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1413
    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v5, 0x7f04002c

    invoke-direct {v3, v4, v5, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1415
    const v0, 0x7f04009c

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 1416
    iget-object v0, p0, Lcom/android/calendar/event/ay;->x:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1423
    :goto_5
    const v0, 0x7f120101

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->af:Landroid/view/View;

    .line 1424
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v0, :cond_c

    .line 1425
    iget-object v0, p0, Lcom/android/calendar/event/ay;->af:Landroid/view/View;

    new-instance v3, Lcom/android/calendar/event/bc;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/bc;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1434
    :cond_c
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_1c

    .line 1435
    const v0, 0x7f12013f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->y:Landroid/widget/Spinner;

    .line 1436
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090039

    invoke-static {v0, v3}, Lcom/android/calendar/event/ay;->b(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1438
    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v5, 0x7f04002c

    invoke-direct {v3, v4, v5, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1440
    const v0, 0x7f04009c

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 1441
    iget-object v0, p0, Lcom/android/calendar/event/ay;->y:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1448
    :goto_6
    const v0, 0x7f120104

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ag:Landroid/view/View;

    .line 1449
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v0, :cond_d

    .line 1450
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ag:Landroid/view/View;

    new-instance v3, Lcom/android/calendar/event/bd;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/bd;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1459
    :cond_d
    const v0, 0x7f120107

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ae:Landroid/view/View;

    .line 1460
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_1d

    .line 1461
    const v0, 0x7f120140

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->w:Landroid/widget/Spinner;

    .line 1463
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090031

    invoke-static {v0, v3}, Lcom/android/calendar/event/ay;->b(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1464
    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v5, 0x7f04002c

    invoke-direct {v3, v4, v5, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1466
    const v0, 0x7f04009c

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 1467
    iget-object v0, p0, Lcom/android/calendar/event/ay;->w:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1468
    iget-object v0, p0, Lcom/android/calendar/event/ay;->w:Landroid/widget/Spinner;

    iget v3, p0, Lcom/android/calendar/event/ay;->cm:I

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1481
    :goto_7
    const v0, 0x7f12010a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    .line 1482
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v0, :cond_e

    .line 1483
    const v0, 0x7f12010c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    .line 1484
    iget-object v3, p0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->cq:Landroid/widget/ArrayAdapter;

    iget v4, p0, Lcom/android/calendar/event/ay;->cr:I

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1486
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    new-instance v3, Lcom/android/calendar/event/bf;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/bf;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1496
    :cond_e
    const v0, 0x7f12010d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    .line 1498
    iget v0, p0, Lcom/android/calendar/event/ay;->cL:I

    const/16 v3, 0x140

    if-gt v0, v3, :cond_f

    .line 1503
    const v0, 0xac001

    .line 1504
    iget-object v3, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 1507
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 1510
    new-instance v0, Lcom/android/calendar/em;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v3}, Lcom/android/calendar/em;-><init>(Landroid/content/Context;)V

    .line 1512
    iget-object v3, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    new-array v2, v2, [Landroid/text/InputFilter;

    aput-object v0, v2, v1

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1517
    const v0, 0x7f12010e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->b:Landroid/view/ViewStub;

    .line 1520
    iget-object v0, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    const v2, 0x7f120111

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->P:Landroid/widget/TextView;

    .line 1521
    const v0, 0x7f12010f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    .line 1523
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1524
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1528
    :cond_10
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aS:Landroid/animation/LayoutTransition;

    if-nez v0, :cond_11

    .line 1529
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aS:Landroid/animation/LayoutTransition;

    .line 1531
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->A()V

    .line 1534
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/event/ay;->s:Landroid/widget/Button;

    new-instance v2, Lcom/android/calendar/event/bg;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/bg;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1561
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->ce:Z

    .line 1562
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_12

    .line 1563
    const v0, 0x7f120124

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    .line 1564
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    new-instance v2, Lcom/android/calendar/event/bh;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/bh;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1709
    :cond_12
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_13

    .line 1710
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setTag(Ljava/lang/Object;)V

    .line 1713
    :cond_13
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_14

    .line 1714
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    new-instance v2, Lcom/android/calendar/event/bk;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/bk;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1739
    :cond_14
    invoke-virtual {p0, v7}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/as;)V

    .line 1741
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->ce:Z

    .line 1744
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->G()V

    .line 1746
    invoke-virtual {p0, v7}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;)V

    .line 1747
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->O()V

    .line 1751
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "EditTaskFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/fq;

    .line 1753
    if-eqz v0, :cond_15

    .line 1754
    iget-object v1, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v1, v1, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v0, v0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1757
    :cond_15
    return-void

    :cond_16
    move v0, v1

    .line 627
    goto/16 :goto_0

    .line 1015
    :cond_17
    const v0, 0x7f12012a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    goto/16 :goto_1

    .line 1111
    :cond_18
    iget-object v0, p0, Lcom/android/calendar/event/ay;->am:Landroid/widget/ImageButton;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/calendar/event/ay;->ch:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f01aa

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1397
    :cond_19
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/calendar/event/ay;->ch:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f01ab

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1408
    :cond_1a
    const v0, 0x7f090007

    move v3, v0

    goto/16 :goto_4

    .line 1418
    :cond_1b
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cG:[Ljava/lang/CharSequence;

    .line 1419
    const v0, 0x7f120103

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->D:Landroid/widget/TextView;

    .line 1420
    iget-object v0, p0, Lcom/android/calendar/event/ay;->D:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->cG:[Ljava/lang/CharSequence;

    iget v4, p0, Lcom/android/calendar/event/ay;->cH:I

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 1443
    :cond_1c
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090039

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cI:[Ljava/lang/CharSequence;

    .line 1444
    const v0, 0x7f120106

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->E:Landroid/widget/TextView;

    .line 1445
    iget-object v0, p0, Lcom/android/calendar/event/ay;->E:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->cI:[Ljava/lang/CharSequence;

    iget v4, p0, Lcom/android/calendar/event/ay;->cJ:I

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 1470
    :cond_1d
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090031

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cl:[Ljava/lang/CharSequence;

    .line 1471
    const v0, 0x7f120109

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->B:Landroid/widget/TextView;

    .line 1472
    iget-object v0, p0, Lcom/android/calendar/event/ay;->B:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->cl:[Ljava/lang/CharSequence;

    iget v4, p0, Lcom/android/calendar/event/ay;->cm:I

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1473
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ae:Landroid/view/View;

    new-instance v3, Lcom/android/calendar/event/be;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/be;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_7

    .line 377
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
    .end array-data

    .line 679
    :array_1
    .array-data 4
        0x7f0f01c4
        0x7f0f01cc
        0x7f0f01ca
        0x7f0f01c2
        0x7f0f01cb
        0x7f0f01cd
        0x7f0f01c7
        0x7f0f01c6
        0x7f0f01c3
        0x7f0f01c9
        0x7f0f01c8
        0x7f0f01c5
    .end array-data
.end method

.method static synthetic A(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cc:I

    return v0
.end method

.method private A()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v3, 0x2

    const-wide/16 v4, 0x12c

    .line 1829
    .line 1831
    iget-object v0, p0, Lcom/android/calendar/event/ay;->h:Landroid/widget/LinearLayout;

    const-string v1, "alpha"

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1832
    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1833
    new-instance v1, Landroid/view/animation/interpolator/SineInOut80;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineInOut80;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1835
    new-instance v1, Lcom/android/calendar/event/bo;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/bo;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1870
    iget-object v1, p0, Lcom/android/calendar/event/ay;->s:Landroid/widget/Button;

    const-string v2, "alpha"

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1871
    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1873
    iget-object v2, p0, Lcom/android/calendar/event/ay;->aS:Landroid/animation/LayoutTransition;

    invoke-virtual {v2, v6, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 1874
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aS:Landroid/animation/LayoutTransition;

    invoke-virtual {v1, v6, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 1876
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aS:Landroid/animation/LayoutTransition;

    new-instance v2, Lcom/android/calendar/event/bq;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/event/bq;-><init>(Lcom/android/calendar/event/ay;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {v1, v2}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 1897
    return-void

    .line 1831
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1870
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private B()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x0

    .line 2088
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 2089
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 2091
    iget-boolean v4, p0, Lcom/android/calendar/event/ay;->cC:Z

    if-eqz v4, :cond_1

    .line 2092
    cmp-long v4, v2, v6

    if-nez v4, :cond_0

    .line 2093
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v2

    .line 2095
    :cond_0
    cmp-long v4, v0, v6

    if-nez v4, :cond_1

    .line 2096
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 2099
    :cond_1
    iget-object v4, p0, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-direct {p0, v4, v2, v3}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    .line 2100
    iget-object v2, p0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    .line 2102
    iget-object v0, p0, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/calendar/event/ff;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/event/ff;-><init>(Lcom/android/calendar/event/ay;Landroid/text/format/Time;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2103
    iget-object v0, p0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/calendar/event/ff;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/event/ff;-><init>(Lcom/android/calendar/event/ay;Landroid/text/format/Time;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2105
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->s()V

    .line 2106
    return-void
.end method

.method static synthetic B(Lcom/android/calendar/event/ay;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->ce:Z

    return v0
.end method

.method static synthetic C(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cO:I

    return v0
.end method

.method private C()V
    .locals 2

    .prologue
    .line 2109
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2125
    :cond_0
    :goto_0
    return-void

    .line 2113
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/bs;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/bs;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2120
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2121
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2122
    const v1, 0x7f0f032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2123
    iget-object v1, p0, Lcom/android/calendar/event/ay;->P:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    return-object v0
.end method

.method private D()V
    .locals 2

    .prologue
    .line 2128
    iget-object v0, p0, Lcom/android/calendar/event/ay;->q:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2144
    :cond_0
    :goto_0
    return-void

    .line 2132
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->q:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/bt;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/bt;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2139
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2140
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2141
    const v1, 0x7f0f032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2142
    iget-object v1, p0, Lcom/android/calendar/event/ay;->Q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private E()V
    .locals 3

    .prologue
    .line 2147
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2148
    const-string v1, "query"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2149
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-class v2, Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 2150
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const/16 v2, 0x131

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2151
    return-void
.end method

.method static synthetic E(Lcom/android/calendar/event/ay;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->cY:Z

    return v0
.end method

.method private F()V
    .locals 11

    .prologue
    .line 2154
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 2155
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 2157
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2169
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cE:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2170
    const v0, 0x7f090017

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cE:[Ljava/lang/String;

    .line 2173
    :cond_0
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_1
    const/4 v0, 0x2

    .line 2175
    :goto_0
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    .line 2188
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->H()Z

    move-result v6

    .line 2189
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->I()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->aG:Z

    .line 2191
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/android/calendar/event/ay;->cO:I

    if-lez v0, :cond_9

    const/4 v0, 0x1

    .line 2193
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    .line 2194
    new-instance v7, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2196
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const v3, 0x7f0f0168

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2197
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2199
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const v3, 0x7f0f0122

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2200
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2203
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2206
    const v2, 0x7f0f0478

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v4, Landroid/text/format/Time;->weekDay:I

    aget-object v1, v1, v10

    aput-object v1, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2207
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_a

    .line 2208
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v1, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2221
    :cond_2
    :goto_2
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2223
    const v1, 0x7f0f02af

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2225
    if-nez v0, :cond_b

    .line 2226
    iget v2, v4, Landroid/text/format/Time;->monthDay:I

    .line 2234
    :goto_3
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_c

    .line 2235
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v1, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2253
    :cond_3
    :goto_4
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2256
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->G:Z

    if-eqz v1, :cond_d

    .line 2257
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 2258
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    .line 2260
    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2261
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 2262
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 2267
    :goto_5
    const v1, 0x7f0f048a

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2268
    const/16 v1, 0x8

    .line 2269
    iget-object v10, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v10}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2270
    const/16 v1, 0x88

    .line 2274
    :cond_4
    if-nez v0, :cond_f

    .line 2275
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v10, "ar"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2276
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 2277
    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 2278
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v0, Landroid/text/format/Time;->monthDay:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Landroid/text/format/Time;->month:I

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 2290
    :goto_6
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {v9, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_10

    .line 2291
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v9, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v9, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2298
    :cond_5
    :goto_7
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2300
    if-eqz v6, :cond_6

    .line 2301
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const v1, 0x7f0f011b

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2302
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2304
    :cond_6
    iput-object v7, p0, Lcom/android/calendar/event/ay;->bN:Ljava/util/ArrayList;

    .line 2307
    iget v0, p0, Lcom/android/calendar/event/ay;->cr:I

    if-nez v0, :cond_7

    .line 2308
    iget-object v0, p0, Lcom/android/calendar/event/ay;->O:[Z

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 2311
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v0, v0, Lcom/android/a/c;->o:I

    if-lez v0, :cond_11

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v0, v0, Lcom/android/a/c;->m:[I

    if-eqz v0, :cond_11

    .line 2313
    const/4 v0, 0x0

    :goto_8
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->o:I

    if-ge v0, v1, :cond_11

    .line 2314
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->m:[I

    aget v1, v1, v0

    invoke-static {v1}, Lcom/android/a/c;->c(I)I

    move-result v1

    .line 2315
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->V:[Z

    const/4 v3, 0x1

    aput-boolean v3, v2, v1

    .line 2316
    iget-object v2, p0, Lcom/android/calendar/event/ay;->O:[Z

    const/4 v3, 0x1

    aput-boolean v3, v2, v1

    .line 2313
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 2173
    :cond_8
    const/4 v0, 0x3

    goto/16 :goto_0

    .line 2191
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2209
    :cond_a
    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 2210
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const/4 v3, 0x0

    const-string v9, "("

    invoke-virtual {v1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v1, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2229
    :cond_b
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    iget v3, v4, Landroid/text/format/Time;->year:I

    iget v9, v4, Landroid/text/format/Time;->month:I

    iget v10, v4, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v2, v3, v9, v10}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 2230
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v2}, Lcom/android/calendar/d/a/a;->c()I

    goto/16 :goto_3

    .line 2236
    :cond_c
    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 2237
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const/4 v3, 0x0

    const-string v9, "("

    invoke-virtual {v1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v1, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 2264
    :cond_d
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    goto/16 :goto_5

    .line 2281
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    goto/16 :goto_6

    .line 2285
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    iget v1, p0, Lcom/android/calendar/event/ay;->cO:I

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/d/a/a;JI)Ljava/lang/String;

    goto/16 :goto_6

    .line 2292
    :cond_10
    const-string v0, "("

    invoke-virtual {v9, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 2293
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    const/4 v1, 0x0

    const-string v2, "("

    invoke-virtual {v9, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v9, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 2320
    :cond_11
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2321
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    if-eqz v1, :cond_15

    .line 2322
    if-eqz v6, :cond_13

    .line 2323
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2345
    :goto_9
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->c:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 2346
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->c:Ljava/lang/String;

    invoke-virtual {v4, v1}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 2347
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v1, v1, Lcom/android/calendar/event/fm;->c:[Landroid/text/format/Time;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 2348
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v1, v1, Lcom/android/calendar/event/fm;->a:[I

    const/4 v2, 0x2

    aput v2, v1, v0

    .line 2349
    iput v0, p0, Lcom/android/calendar/event/ay;->cc:I

    .line 2393
    :cond_12
    :goto_a
    iget-boolean v1, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v1, :cond_19

    .line 2394
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v2, 0x7f04002c

    iget-object v3, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cq:Landroid/widget/ArrayAdapter;

    .line 2396
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cq:Landroid/widget/ArrayAdapter;

    const v1, 0x7f04009c

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 2406
    :goto_b
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->G()V

    .line 2407
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->Q()V

    .line 2408
    return-void

    .line 2325
    :cond_13
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->b:I

    packed-switch v1, :pswitch_data_0

    goto :goto_9

    .line 2327
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_9

    .line 2330
    :pswitch_1
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_9

    .line 2334
    :pswitch_2
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_9

    .line 2338
    :pswitch_3
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_9

    .line 2350
    :cond_14
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->d:I

    if-lez v1, :cond_12

    .line 2351
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v1, v1, Lcom/android/calendar/event/fm;->d:[I

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->d:I

    aput v2, v1, v0

    .line 2352
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v1, v1, Lcom/android/calendar/event/fm;->a:[I

    const/4 v2, 0x3

    aput v2, v1, v0

    .line 2353
    iput v0, p0, Lcom/android/calendar/event/ay;->cc:I

    goto :goto_a

    .line 2355
    :cond_15
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 2356
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2357
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 2358
    const/4 v2, 0x1

    aget-object v0, v0, v2

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2360
    const/4 v0, 0x2

    new-array v3, v0, [Landroid/text/format/Time;

    .line 2361
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2362
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v2, v0

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 2363
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-virtual {v4}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/android/calendar/as;->O:Ljava/lang/String;

    .line 2364
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v5, v5, Lcom/android/calendar/as;->O:Ljava/lang/String;

    iput-object v5, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 2366
    const/4 v0, 0x0

    :goto_c
    array-length v5, v3

    if-ge v0, v5, :cond_17

    .line 2367
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v0

    .line 2368
    aget-object v5, v3, v0

    aget-object v6, v2, v0

    invoke-virtual {v5, v6}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 2369
    iget-object v5, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    if-eqz v5, :cond_16

    .line 2370
    iget-object v5, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    aget-object v6, v3, v0

    iget v6, v6, Landroid/text/format/Time;->year:I

    aget-object v8, v3, v0

    iget v8, v8, Landroid/text/format/Time;->month:I

    aget-object v9, v3, v0

    iget v9, v9, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v5, v6, v8, v9}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 2371
    aget-object v5, v3, v0

    iget-object v6, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v6}, Lcom/android/calendar/d/a/a;->a()I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->year:I

    .line 2372
    aget-object v5, v3, v0

    iget-object v6, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v6}, Lcom/android/calendar/d/a/a;->b()I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->month:I

    .line 2373
    aget-object v5, v3, v0

    iget-object v6, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v6}, Lcom/android/calendar/d/a/a;->c()I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->monthDay:I

    .line 2366
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 2376
    :cond_17
    const/4 v0, 0x0

    aget-object v0, v3, v0

    iget v0, v0, Landroid/text/format/Time;->year:I

    const/4 v1, 0x1

    aget-object v1, v3, v1

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-eq v0, v1, :cond_18

    const/4 v0, 0x0

    aget-object v0, v3, v0

    iget v0, v0, Landroid/text/format/Time;->month:I

    const/4 v1, 0x1

    aget-object v1, v3, v1

    iget v1, v1, Landroid/text/format/Time;->month:I

    if-ne v0, v1, :cond_18

    .line 2377
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2378
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    const/4 v2, 0x1

    aget-object v2, v3, v2

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/4 v5, 0x0

    aget-object v3, v3, v5

    iget v3, v3, Landroid/text/format/Time;->year:I

    sub-int/2addr v2, v3

    iput v2, v1, Lcom/android/calendar/as;->S:I

    .line 2379
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    const/4 v2, 0x7

    iput v2, v1, Lcom/android/a/c;->b:I

    .line 2387
    :goto_d
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v2, v2, Lcom/android/calendar/as;->S:I

    iput v2, v1, Lcom/android/a/c;->e:I

    .line 2388
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v1, v1, Lcom/android/calendar/event/fm;->c:[Landroid/text/format/Time;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 2389
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v1, v1, Lcom/android/calendar/event/fm;->a:[I

    const/4 v2, 0x2

    aput v2, v1, v0

    .line 2390
    iput v0, p0, Lcom/android/calendar/event/ay;->cc:I

    goto/16 :goto_a

    .line 2381
    :cond_18
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2382
    const/4 v1, 0x1

    aget-object v1, v3, v1

    iget v1, v1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x0

    aget-object v2, v3, v2

    iget v2, v2, Landroid/text/format/Time;->month:I

    sub-int/2addr v1, v2

    .line 2383
    const/4 v2, 0x1

    aget-object v2, v3, v2

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/4 v5, 0x0

    aget-object v3, v3, v5

    iget v3, v3, Landroid/text/format/Time;->year:I

    sub-int/2addr v2, v3

    .line 2384
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    mul-int/lit8 v2, v2, 0xc

    add-int/2addr v1, v2

    iput v1, v3, Lcom/android/calendar/as;->S:I

    .line 2385
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    const/4 v2, 0x6

    iput v2, v1, Lcom/android/a/c;->b:I

    goto :goto_d

    .line 2398
    :cond_19
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f04009b

    iget-object v4, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cq:Landroid/widget/ArrayAdapter;

    .line 2400
    iget v1, p0, Lcom/android/calendar/event/ay;->cr:I

    if-nez v1, :cond_1a

    .line 2401
    iput v0, p0, Lcom/android/calendar/event/ay;->cr:I

    .line 2403
    :cond_1a
    iput v0, p0, Lcom/android/calendar/event/ay;->cc:I

    goto/16 :goto_b

    .line 2325
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic F(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->E()V

    return-void
.end method

.method private G()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2411
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cq:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_1

    .line 2412
    const-string v0, "EditEvent"

    const-string v1, "Couldn\'t update RepeatLayout. mRepeatAdapter is null."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 2459
    :cond_0
    :goto_0
    return-void

    .line 2416
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_3

    .line 2417
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    if-eqz v0, :cond_2

    .line 2418
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cq:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2419
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ay;->cc:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2447
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2448
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_9

    .line 2449
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 2450
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setEnabled(Z)V

    goto :goto_0

    .line 2422
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2423
    :cond_4
    new-instance v1, Lcom/android/a/c;

    invoke-direct {v1}, Lcom/android/a/c;-><init>()V

    .line 2424
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2425
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 2432
    :cond_5
    :goto_2
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2433
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v2, v2, Lcom/android/calendar/as;->G:Z

    if-eqz v2, :cond_6

    .line 2434
    const-string v2, "UTC"

    iput-object v2, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2436
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v2, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 2437
    invoke-virtual {v1, v0}, Lcom/android/a/c;->a(Landroid/text/format/Time;)V

    .line 2438
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_8

    const-string v0, "UTC"

    .line 2439
    :goto_3
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v1, v4, v0}, Lcom/android/calendar/dp;->a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/android/a/c;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2441
    iget-object v1, p0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2426
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2427
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v0, v0, Lcom/android/a/c;->b:I

    iput v0, v1, Lcom/android/a/c;->b:I

    .line 2428
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->S:I

    iput v0, v1, Lcom/android/a/c;->e:I

    .line 2429
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->O:Ljava/lang/String;

    iput-object v0, v1, Lcom/android/a/c;->c:Ljava/lang/String;

    goto :goto_2

    .line 2438
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    goto :goto_3

    .line 2453
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2454
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 2455
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    goto/16 :goto_0
.end method

.method static synthetic G(Lcom/android/calendar/event/ay;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->cX:Z

    return v0
.end method

.method static synthetic H(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->N()V

    return-void
.end method

.method private H()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2462
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->b:I

    if-nez v2, :cond_1

    .line 2487
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 2466
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->b:I

    packed-switch v2, :pswitch_data_0

    :cond_2
    move v0, v1

    .line 2487
    goto :goto_0

    .line 2471
    :pswitch_1
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    invoke-virtual {v2}, Lcom/android/a/c;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2473
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->y:I

    if-nez v2, :cond_2

    goto :goto_0

    .line 2479
    :pswitch_2
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->o:I

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->w:I

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 2480
    goto :goto_0

    .line 2466
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic I(Lcom/android/calendar/event/ay;)Lcom/android/calendar/event/ax;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bx:Lcom/android/calendar/event/ax;

    return-object v0
.end method

.method private I()Z
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v1, 0x0

    .line 2491
    const/4 v0, 0x1

    .line 2495
    iget-object v2, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/calendar/event/ay;->cO:I

    if-lez v2, :cond_0

    move v0, v1

    .line 2500
    :cond_0
    const-string v2, "FRISAT"

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/calendar/hj;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2502
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v4, :cond_2

    :cond_1
    move v0, v1

    .line 2510
    :cond_2
    :goto_0
    return v0

    .line 2506
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v4, :cond_2

    :cond_4
    move v0, v1

    .line 2507
    goto :goto_0
.end method

.method static synthetic J(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private J()Z
    .locals 13

    .prologue
    const/16 v5, 0xc

    const/16 v9, 0xb

    const/4 v12, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2809
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-nez v0, :cond_0

    .line 3168
    :goto_0
    return v8

    .line 2812
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bQ:Ljava/util/ArrayList;

    invoke-static {v1, v2, v3}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    .line 2814
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2815
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-virtual {v0}, Lcom/android/calendar/as;->e()Z

    .line 2816
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_10

    move v0, v7

    :goto_1
    iput-boolean v0, v1, Lcom/android/calendar/as;->H:Z

    .line 2817
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 2818
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/calendar/as;->G:Z

    .line 2819
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 2820
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    .line 2821
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2822
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 2824
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2825
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    .line 2828
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->F:Landroid/widget/RadioGroup;

    if-nez v0, :cond_11

    move v0, v8

    .line 2834
    :goto_2
    if-eqz v0, :cond_3

    .line 2835
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput v0, v1, Lcom/android/calendar/as;->X:I

    .line 2838
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_4
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-nez v0, :cond_12

    .line 2841
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 2843
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 2844
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->a()V

    .line 2845
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-boolean v8, v0, Lcom/android/calendar/as;->W:Z

    .line 2909
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->aq:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 2912
    :cond_6
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_1d

    .line 2913
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->c:J

    .line 2914
    iget-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 2922
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->aq:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/calendar/event/ay;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2925
    :cond_7
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2926
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2928
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2930
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2932
    sget-object v3, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 2933
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v4, "preference_defaultCalendar"

    invoke-static {v3, v4, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2935
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v4, "preference_defaultCalendar_account_type"

    invoke-static {v3, v4, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2938
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v3, "preference_defaultCalendar_display_name"

    invoke-static {v1, v3, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2942
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-object v0, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 2943
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-object v0, v1, Lcom/android/calendar/as;->t:Ljava/lang/String;

    .line 2944
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->c:J

    .line 2945
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    .line 2947
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->m:Ljava/lang/String;

    .line 2952
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->aA:Lcom/android/calendar/colorpicker/e;

    if-eqz v0, :cond_a

    .line 2953
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->aA:Lcom/android/calendar/colorpicker/e;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->m:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->l:Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/event/ay;->cP:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/calendar/colorpicker/e;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/as;->i:I

    .line 2957
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_1f

    .line 2958
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2959
    iput v8, v0, Landroid/text/format/Time;->hour:I

    .line 2960
    iput v8, v0, Landroid/text/format/Time;->minute:I

    .line 2961
    iput v8, v0, Landroid/text/format/Time;->second:I

    .line 2962
    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2963
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/android/calendar/as;->y:J

    .line 2965
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2966
    iput v8, v0, Landroid/text/format/Time;->hour:I

    .line 2967
    iput v8, v0, Landroid/text/format/Time;->minute:I

    .line 2968
    iput v8, v0, Landroid/text/format/Time;->second:I

    .line 2969
    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2972
    invoke-virtual {v0, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    .line 2973
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v2, v2, Lcom/android/calendar/as;->y:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_1e

    .line 2975
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->y:J

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    .line 2979
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    const-string v1, "UTC"

    iput-object v1, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 2995
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Lcom/android/calendar/as;)V

    .line 2997
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_21

    .line 2998
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->y:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    iput v1, v0, Lcom/android/calendar/as;->au:I

    .line 2999
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->x:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_20

    move v0, v7

    :goto_6
    iput-boolean v0, v1, Lcom/android/calendar/as;->I:Z

    .line 3008
    :goto_7
    iget v0, p0, Lcom/android/calendar/event/ay;->bW:I

    if-ne v0, v7, :cond_23

    move v0, v8

    .line 3023
    :goto_8
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v1}, Lcom/android/calendar/d/g;->c()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 3024
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v1, p0, Lcom/android/calendar/event/ay;->cO:I

    if-lez v1, :cond_26

    move v1, v7

    :goto_9
    iput-boolean v1, v2, Lcom/android/calendar/as;->ay:Z

    .line 3031
    :cond_b
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->e:I

    if-lez v1, :cond_c

    .line 3032
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->e:I

    iput v2, v1, Lcom/android/calendar/as;->S:I

    .line 3036
    :cond_c
    iget-object v1, p0, Lcom/android/calendar/event/ay;->O:[Z

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->V:[Z

    const/4 v3, 0x7

    invoke-static {v1, v8, v2, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3038
    iget v1, p0, Lcom/android/calendar/event/ay;->cf:I

    packed-switch v1, :pswitch_data_0

    .line 3141
    :goto_a
    iget v1, p0, Lcom/android/calendar/event/ay;->cf:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_d

    .line 3142
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v2}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/av;->a(ILcom/android/calendar/as;I)V

    .line 3147
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-nez v0, :cond_e

    .line 3148
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/android/calendar/timezone/v;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 3151
    :cond_e
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    if-eqz v0, :cond_f

    .line 3152
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_2b

    .line 3153
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->w:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    iput v1, v0, Lcom/android/calendar/as;->J:I

    .line 3160
    :cond_f
    :goto_b
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->c()I

    move-result v0

    const v2, 0x55d4a81

    if-eq v0, v2, :cond_2c

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->c()I

    move-result v0

    :goto_c
    iput v0, v1, Lcom/android/calendar/as;->ai:I

    .line 3162
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->d()I

    move-result v0

    const v2, 0xaba9501

    if-eq v0, v2, :cond_2d

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->d()I

    move-result v0

    :goto_d
    iput v0, v1, Lcom/android/calendar/as;->aj:I

    .line 3164
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    .line 3166
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v1, p0, Lcom/android/calendar/event/ay;->cQ:I

    iput v1, v0, Lcom/android/calendar/as;->e:I

    move v8, v7

    .line 3168
    goto/16 :goto_0

    :cond_10
    move v0, v8

    .line 2816
    goto/16 :goto_1

    .line 2831
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/event/ay;->F:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/event/hm;->b(I)I

    move-result v0

    goto/16 :goto_2

    .line 2848
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    if-eqz v0, :cond_14

    .line 2849
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    invoke-virtual {v0, v7}, Lcom/android/b/b;->a(Z)V

    .line 2852
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_34

    move v0, v8

    .line 2856
    :goto_e
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v1

    if-eqz v1, :cond_18

    if-nez v0, :cond_18

    .line 2857
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->a(Landroid/widget/MultiAutoCompleteTextView;)V

    .line 2859
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/event/ay;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 2862
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v1, 0x7f0f024b

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2888
    :cond_13
    :goto_f
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2889
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 2890
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/as;->a(Ljava/lang/String;Lcom/android/b/b;)V

    .line 2891
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    invoke-virtual {v0, v8}, Lcom/android/b/b;->a(Z)V

    .line 2894
    :cond_14
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 2895
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v1

    .line 2896
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    move v0, v8

    .line 2897
    :goto_10
    if-ge v0, v1, :cond_5

    .line 2898
    iget-object v2, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/event/AttendeesView;->b(I)Lcom/android/calendar/at;

    move-result-object v2

    .line 2899
    if-eqz v2, :cond_15

    iget-object v3, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v3, v0}, Lcom/android/calendar/event/AttendeesView;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 2897
    :cond_15
    :goto_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 2865
    :cond_16
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2867
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_17

    .line 2868
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2870
    :cond_17
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_f

    .line 2872
    :cond_18
    if-nez v0, :cond_13

    .line 2873
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->performValidation()V

    .line 2875
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 2877
    :cond_19
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v1, 0x7f0f024b

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_f

    .line 2880
    :cond_1a
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2882
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1b

    .line 2883
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2885
    :cond_1b
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_f

    .line 2902
    :cond_1c
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-virtual {v3, v2}, Lcom/android/calendar/as;->a(Lcom/android/calendar/at;)V

    goto/16 :goto_11

    .line 2916
    :cond_1d
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    sget v1, Lcom/android/calendar/event/ay;->ci:I

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/android/calendar/as;->c:J

    .line 2917
    sget v0, Lcom/android/calendar/event/ay;->ci:I

    goto/16 :goto_3

    .line 2977
    :cond_1e
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-wide v0, v2, Lcom/android/calendar/as;->B:J

    goto/16 :goto_4

    .line 2981
    :cond_1f
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2982
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2983
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    .line 2984
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->second:I

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->minute:I

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->hour:I

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->monthDay:I

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->month:I

    iget-object v6, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v6, v6, Landroid/text/format/Time;->year:I

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v0

    .line 2986
    iget-boolean v2, p0, Lcom/android/calendar/event/ay;->cC:Z

    if-eqz v2, :cond_32

    .line 2987
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-static {v10, v11, v2}, Lcom/android/calendar/hj;->a(JLandroid/text/format/Time;)J

    move-result-wide v2

    .line 2988
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-static {v0, v1, v4}, Lcom/android/calendar/hj;->a(JLandroid/text/format/Time;)J

    move-result-wide v0

    .line 2990
    :goto_12
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-wide v2, v4, Lcom/android/calendar/as;->y:J

    .line 2991
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-wide v0, v2, Lcom/android/calendar/as;->B:J

    .line 2992
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    goto/16 :goto_5

    :cond_20
    move v0, v8

    .line 2999
    goto/16 :goto_6

    .line 3001
    :cond_21
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v1, p0, Lcom/android/calendar/event/ay;->cJ:I

    iput v1, v0, Lcom/android/calendar/as;->au:I

    .line 3002
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, p0, Lcom/android/calendar/event/ay;->cH:I

    if-eqz v0, :cond_22

    move v0, v7

    :goto_13
    iput-boolean v0, v1, Lcom/android/calendar/as;->I:Z

    goto/16 :goto_7

    :cond_22
    move v0, v8

    goto :goto_13

    .line 3011
    :cond_23
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_24

    .line 3012
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ay;->cr:I

    .line 3015
    :cond_24
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bN:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/event/ay;->cr:I

    if-le v0, v1, :cond_25

    .line 3016
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bN:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/event/ay;->cr:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto/16 :goto_8

    :cond_25
    move v0, v8

    .line 3018
    goto/16 :goto_8

    :cond_26
    move v1, v8

    .line 3024
    goto/16 :goto_9

    .line 3040
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput v7, v1, Lcom/android/calendar/as;->N:I

    .line 3041
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v2, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 3043
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v3, v1, Landroid/text/format/Time;->year:I

    .line 3044
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v2, v1, Landroid/text/format/Time;->month:I

    .line 3045
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    .line 3049
    iget-object v4, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v4}, Lcom/android/calendar/d/g;->c()Z

    move-result v4

    if-eqz v4, :cond_31

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v4, v4, Lcom/android/calendar/as;->ay:Z

    if-eqz v4, :cond_31

    iget-object v4, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    if-eqz v4, :cond_31

    .line 3052
    iget-object v4, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v4, v3, v2, v1}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 3053
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->a()I

    move-result v4

    .line 3054
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->b()I

    move-result v3

    .line 3055
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->c()I

    move-result v2

    .line 3056
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->d()Z

    move-result v1

    .line 3060
    :goto_14
    add-int/lit8 v2, v2, -0x1

    .line 3062
    iget v5, p0, Lcom/android/calendar/event/ay;->cd:I

    packed-switch v5, :pswitch_data_1

    .line 3083
    :goto_15
    iget-object v5, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v5}, Lcom/android/calendar/d/g;->c()Z

    move-result v5

    if-eqz v5, :cond_27

    .line 3084
    iget-object v5, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v5, v5, Lcom/android/calendar/as;->ay:Z

    if-eqz v5, :cond_27

    iget-object v5, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    if-eqz v5, :cond_27

    .line 3086
    if-gtz v2, :cond_30

    .line 3087
    if-nez v1, :cond_29

    .line 3088
    add-int/lit8 v3, v3, -0x1

    move v8, v1

    move v1, v3

    .line 3093
    :goto_16
    if-gez v1, :cond_2f

    .line 3095
    add-int/lit8 v4, v4, -0x1

    move v3, v9

    .line 3097
    :goto_17
    if-gtz v2, :cond_2e

    .line 3098
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1, v4, v3, v8}, Lcom/android/calendar/d/a/a;->a(IIZ)I

    move-result v1

    .line 3102
    :goto_18
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v2, v4, v3, v1, v8}, Lcom/android/calendar/d/a/a;->a(IIIZ)V

    .line 3104
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->a()I

    move-result v4

    .line 3105
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->b()I

    move-result v3

    .line 3106
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->c()I

    move-result v2

    .line 3111
    :cond_27
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v1, v2, v3, v4}, Landroid/text/format/Time;->set(III)V

    .line 3113
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-static {v1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    if-ne v1, v7, :cond_28

    .line 3114
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    const v2, 0x259d23

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 3116
    :cond_28
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v1, v7}, Landroid/text/format/Time;->normalize(Z)J

    .line 3119
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-direct {p0, v1}, Lcom/android/calendar/event/ay;->b(Landroid/text/format/Time;)V

    .line 3121
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/as;->O:Ljava/lang/String;

    goto/16 :goto_a

    .line 3064
    :pswitch_1
    add-int/lit8 v4, v4, 0x1

    .line 3065
    goto :goto_15

    .line 3068
    :pswitch_2
    add-int/lit8 v4, v4, 0x5

    .line 3069
    goto :goto_15

    .line 3072
    :pswitch_3
    add-int/lit8 v4, v4, 0xa

    .line 3073
    goto :goto_15

    .line 3075
    :pswitch_4
    add-int/lit8 v4, v4, 0x32

    .line 3076
    goto :goto_15

    :cond_29
    move v1, v3

    .line 3090
    goto :goto_16

    .line 3125
    :pswitch_5
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-static {v1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    if-ne v1, v7, :cond_2a

    .line 3126
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    const v2, 0x259d23

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 3128
    :cond_2a
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput v12, v1, Lcom/android/calendar/as;->N:I

    .line 3129
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v1, v7}, Landroid/text/format/Time;->normalize(Z)J

    .line 3130
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/as;->O:Ljava/lang/String;

    goto/16 :goto_a

    .line 3134
    :pswitch_6
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    const/4 v2, 0x3

    iput v2, v1, Lcom/android/calendar/as;->N:I

    .line 3135
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v2, p0, Lcom/android/calendar/event/ay;->cg:I

    iput v2, v1, Lcom/android/calendar/as;->R:I

    goto/16 :goto_a

    .line 3155
    :cond_2b
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v1, p0, Lcom/android/calendar/event/ay;->cm:I

    iput v1, v0, Lcom/android/calendar/as;->J:I

    goto/16 :goto_b

    .line 3160
    :cond_2c
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->ai:I

    goto/16 :goto_c

    .line 3162
    :cond_2d
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->aj:I

    goto/16 :goto_d

    :cond_2e
    move v1, v2

    goto/16 :goto_18

    :cond_2f
    move v3, v1

    goto/16 :goto_17

    :cond_30
    move v8, v1

    move v1, v3

    goto/16 :goto_16

    :cond_31
    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v8

    goto/16 :goto_14

    :cond_32
    move-wide v2, v10

    goto/16 :goto_12

    :cond_33
    move v0, v8

    goto/16 :goto_3

    :cond_34
    move v0, v7

    goto/16 :goto_e

    .line 3038
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 3062
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic K(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    return-object v0
.end method

.method private K()V
    .locals 14

    .prologue
    const/4 v11, 0x0

    .line 3201
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 3203
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->c:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->aq:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->az:Ljava/lang/String;

    const-string v2, "com.android.exchange"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3206
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v2, p0, Lcom/android/calendar/event/ay;->cn:I

    iput v2, v1, Lcom/android/calendar/as;->f:I

    .line 3207
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->co:Ljava/lang/String;

    iput-object v2, v1, Lcom/android/calendar/as;->g:Ljava/lang/String;

    .line 3215
    :cond_1
    const v1, 0x7f090028

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    .line 3216
    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Landroid/content/res/Resources;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bP:Ljava/util/ArrayList;

    .line 3217
    const v1, 0x7f090026

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bQ:Ljava/util/ArrayList;

    .line 3218
    const v1, 0x7f090025

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->b(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bR:Ljava/util/ArrayList;

    .line 3222
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3223
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bQ:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bR:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->g:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 3229
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->H:Z

    if-eqz v0, :cond_8

    .line 3230
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    .line 3232
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 3233
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 3235
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 3237
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 3238
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bQ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/calendar/au;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3239
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bP:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/calendar/au;->a()I

    move-result v0

    invoke-static {v3, v4, v5, v0}, Lcom/android/calendar/event/hm;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    goto :goto_0

    .line 3248
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3249
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/calendar/au;

    .line 3250
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bQ:Ljava/util/ArrayList;

    invoke-virtual {v8}, Lcom/android/calendar/au;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v8}, Lcom/android/calendar/au;->b()I

    move-result v0

    if-nez v0, :cond_6

    .line 3252
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->a:Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->aK:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bP:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/calendar/event/ay;->bQ:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/calendar/event/ay;->bR:Ljava/util/ArrayList;

    const v9, 0x7fffffff

    const/4 v10, 0x0

    invoke-static/range {v0 .. v11}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/calendar/au;ILandroid/widget/AdapterView$OnItemSelectedListener;Z)Z

    goto :goto_1

    .line 3261
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    move v11, v12

    .line 3268
    :goto_2
    invoke-direct {p0, v11}, Lcom/android/calendar/event/ay;->i(I)V

    .line 3269
    return-void

    .line 3265
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_2
.end method

.method static synthetic L(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cK:I

    return v0
.end method

.method private L()V
    .locals 3

    .prologue
    .line 3583
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v1, 0x7f0f0344

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 3584
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3585
    return-void
.end method

.method static synthetic M(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private M()V
    .locals 4

    .prologue
    .line 3588
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 3590
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-nez v1, :cond_1

    .line 3604
    :cond_0
    :goto_0
    return-void

    .line 3593
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 3594
    iget-object v2, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    invoke-direct {p0, v1, v2}, Lcom/android/calendar/event/ay;->a(Ljava/lang/StringBuilder;Landroid/view/View;)V

    .line 3595
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3597
    const/16 v2, 0x8

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 3598
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 3599
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 3600
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3601
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 3603
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method static synthetic N(Lcom/android/calendar/event/ay;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    return-object v0
.end method

.method private N()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 4264
    iget v1, p0, Lcom/android/calendar/event/ay;->bS:I

    .line 4266
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bQ:Ljava/util/ArrayList;

    invoke-static {v2, v3, v4}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    .line 4270
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 4271
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 4272
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/calendar/au;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 4273
    if-ltz v0, :cond_2

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    if-ge v0, v2, :cond_2

    .line 4274
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v8, v0

    .line 4278
    :goto_0
    invoke-static {v11}, Lcom/android/calendar/event/hm;->a(Z)V

    .line 4279
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->a:Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->aK:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bO:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bP:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/calendar/event/ay;->bQ:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/calendar/event/ay;->bR:Ljava/util/ArrayList;

    invoke-static {v8}, Lcom/android/calendar/au;->a(I)Lcom/android/calendar/au;

    move-result-object v8

    iget-object v9, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v9, v9, Lcom/android/calendar/as;->f:I

    const/4 v10, 0x0

    invoke-static/range {v0 .. v11}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/calendar/au;ILandroid/widget/AdapterView$OnItemSelectedListener;Z)Z

    .line 4286
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->i(I)V

    .line 4288
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 4289
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v1, v1, Lcom/android/calendar/as;->f:I

    if-lt v0, v1, :cond_0

    .line 4290
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 4291
    const v1, 0x7f12014e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 4293
    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 4298
    :cond_0
    :goto_1
    return-void

    .line 4296
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->T:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_1

    :cond_2
    move v8, v1

    goto :goto_0
.end method

.method private O()V
    .locals 5

    .prologue
    const v4, 0x7f0c016a

    const v3, 0x7f0c0129

    .line 4945
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 4948
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4951
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    const v2, 0x7f0c0168

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4952
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4955
    iget-boolean v1, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v1, :cond_0

    .line 4956
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    const v2, 0x7f0c0125

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4962
    :goto_0
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    const v2, 0x7f0c010f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4965
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    const v2, 0x7f0c0177

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4969
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    const v2, 0x7f0c0149

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4972
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    const v2, 0x7f0c013a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4975
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4978
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    const v2, 0x7f0c0148

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4981
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    const v2, 0x7f0c010b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 4983
    return-void

    .line 4958
    :cond_0
    iget v1, p0, Lcom/android/calendar/event/ay;->cK:I

    int-to-float v1, v1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cK:I

    goto :goto_0
.end method

.method static synthetic O(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ac()V

    return-void
.end method

.method static synthetic P(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cQ:I

    return v0
.end method

.method private P()V
    .locals 7

    .prologue
    const/16 v6, 0x9

    .line 5520
    new-instance v0, Lcom/android/calendar/event/fm;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/fm;-><init>(Lcom/android/calendar/event/ay;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    .line 5522
    new-array v1, v6, [I

    fill-array-data v1, :array_0

    .line 5525
    new-array v2, v6, [I

    fill-array-data v2, :array_1

    .line 5529
    new-array v3, v6, [Landroid/text/format/Time;

    .line 5530
    new-array v4, v6, [I

    fill-array-data v4, :array_2

    .line 5533
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_0

    .line 5534
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    aput-object v5, v3, v0

    .line 5533
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5537
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iput-object v1, v0, Lcom/android/calendar/event/fm;->a:[I

    .line 5538
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iput-object v2, v0, Lcom/android/calendar/event/fm;->b:[I

    .line 5539
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iput-object v3, v0, Lcom/android/calendar/event/fm;->c:[Landroid/text/format/Time;

    .line 5540
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iput-object v4, v0, Lcom/android/calendar/event/fm;->d:[I

    .line 5541
    return-void

    .line 5522
    :array_0
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 5525
    :array_1
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 5530
    :array_2
    .array-data 4
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
    .end array-data
.end method

.method private Q()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, -0x1

    .line 5546
    iget v0, p0, Lcom/android/calendar/event/ay;->cb:I

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    if-nez v0, :cond_1

    .line 5568
    :cond_0
    :goto_0
    return-void

    .line 5550
    :cond_1
    const-string v0, "%d"

    .line 5551
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/event/ay;->cb:I

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    .line 5552
    const-string v0, "%d"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 5554
    if-eq v2, v3, :cond_0

    .line 5555
    const-string v0, "%d"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int v3, v2, v0

    .line 5556
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5557
    iget-object v0, p0, Lcom/android/calendar/event/ay;->K:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 5558
    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 5559
    iget-object v0, p0, Lcom/android/calendar/event/ay;->L:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5561
    iget-object v0, p0, Lcom/android/calendar/event/ay;->K:Landroid/widget/TextView;

    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 5563
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->L:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5565
    iget-object v0, p0, Lcom/android/calendar/event/ay;->K:Landroid/widget/TextView;

    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic Q(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->X()V

    return-void
.end method

.method private R()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6179
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cs:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 6180
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, p0, Lcom/android/calendar/event/ay;->ct:I

    iput v1, v0, Lcom/android/a/c;->d:I

    .line 6181
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, p0, Lcom/android/calendar/event/ay;->cu:I

    iput v1, v0, Lcom/android/a/c;->e:I

    .line 6182
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cv:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 6183
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, p0, Lcom/android/calendar/event/ay;->cw:I

    iput v1, v0, Lcom/android/a/c;->q:I

    .line 6184
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, p0, Lcom/android/calendar/event/ay;->cx:I

    iput v1, v0, Lcom/android/a/c;->o:I

    .line 6185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v1, p0, Lcom/android/calendar/event/ay;->cy:I

    iput v1, v0, Lcom/android/calendar/as;->T:I

    .line 6186
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v1, p0, Lcom/android/calendar/event/ay;->cz:Z

    iput-boolean v1, v0, Lcom/android/calendar/as;->U:Z

    .line 6187
    iget v0, p0, Lcom/android/calendar/event/ay;->cA:I

    iput v0, p0, Lcom/android/calendar/event/ay;->cg:I

    .line 6188
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cB:[Z

    iget-object v1, p0, Lcom/android/calendar/event/ay;->O:[Z

    iget-object v2, p0, Lcom/android/calendar/event/ay;->cB:[Z

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6189
    return-void
.end method

.method static synthetic R(Lcom/android/calendar/event/ay;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cl:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic S(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cm:I

    return v0
.end method

.method private S()V
    .locals 4

    .prologue
    .line 6427
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6428
    const v1, 0x7f0f033d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 6429
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cG:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/ay;->cH:I

    new-instance v3, Lcom/android/calendar/event/eh;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/eh;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6444
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/ei;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/ei;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6453
    new-instance v1, Lcom/android/calendar/event/ej;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ej;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 6460
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6461
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 6463
    :cond_0
    return-void
.end method

.method static synthetic T(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cb:I

    return v0
.end method

.method private T()V
    .locals 4

    .prologue
    .line 6467
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6468
    const v1, 0x7f0f0349

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 6469
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cI:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/ay;->cJ:I

    new-instance v3, Lcom/android/calendar/event/ek;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/ek;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6483
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/el;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/el;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6492
    new-instance v1, Lcom/android/calendar/event/em;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/em;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 6499
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6500
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 6502
    :cond_0
    return-void
.end method

.method private U()V
    .locals 3

    .prologue
    .line 6506
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6507
    const v1, 0x7f0f00b2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 6508
    const v1, 0x7f0f00b1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 6509
    const v1, 0x7f0f00a4

    new-instance v2, Lcom/android/calendar/event/eo;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/eo;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/en;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/en;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 6525
    const v1, 0x7f0f009e

    new-instance v2, Lcom/android/calendar/event/er;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/er;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/ep;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/ep;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 6549
    new-instance v1, Lcom/android/calendar/event/es;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/es;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 6556
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6557
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 6559
    :cond_0
    return-void
.end method

.method static synthetic U(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->Q()V

    return-void
.end method

.method static synthetic V(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cu:I

    return v0
.end method

.method private V()V
    .locals 3

    .prologue
    .line 6563
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6564
    const v1, 0x7f0f02c8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 6565
    const v1, 0x7f0f02c9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 6566
    const v1, 0x7f0f009e

    new-instance v2, Lcom/android/calendar/event/et;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/et;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6574
    new-instance v1, Lcom/android/calendar/event/eu;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/eu;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 6581
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6582
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 6584
    :cond_0
    return-void
.end method

.method static synthetic W(Lcom/android/calendar/event/ay;)Landroid/widget/RadioButton;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aW:Landroid/widget/RadioButton;

    return-object v0
.end method

.method private W()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 6681
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6682
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6683
    const-string v2, "location"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6684
    invoke-static {}, Lcom/android/calendar/dz;->w()Z

    move-result v0

    if-nez v0, :cond_3

    .line 6685
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-class v2, Lcom/android/calendar/event/SelectMapActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 6694
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "clipboardEx"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/ClipboardExManager;

    .line 6696
    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 6697
    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 6700
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v2, "accessibility"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6701
    iget-object v0, p0, Lcom/android/calendar/event/ay;->am:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->sendAccessibilityEvent(I)V

    .line 6702
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/android/calendar/event/ev;

    invoke-direct {v2, p0, v1}, Lcom/android/calendar/event/ev;-><init>(Lcom/android/calendar/event/ay;Landroid/content/Intent;)V

    const-wide/16 v4, 0x2bc

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 6714
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sec_container_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6715
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->as:Z

    .line 6716
    iget-object v0, p0, Lcom/android/calendar/event/ay;->am:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 6718
    :cond_2
    return-void

    .line 6687
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-class v2, Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 6688
    const-string v0, "map_latitude"

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v2, v2, Lcom/android/calendar/as;->ai:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6689
    const-string v0, "map_longtitude"

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v2, v2, Lcom/android/calendar/as;->aj:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6690
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->g()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 6691
    const-string v0, "mode"

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 6710
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const/16 v2, 0x65

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method static synthetic X(Lcom/android/calendar/event/ay;)Landroid/widget/RadioButton;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aV:Landroid/widget/RadioButton;

    return-object v0
.end method

.method private X()V
    .locals 3

    .prologue
    .line 6724
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preferences_save_rec_lun_event_in_sol"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 6725
    if-eqz v0, :cond_1

    .line 6734
    :cond_0
    :goto_0
    return-void

    .line 6728
    :cond_1
    iget v0, p0, Lcom/android/calendar/event/ay;->cO:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    const-string v1, "local"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6729
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->a(Landroid/app/Activity;)Landroid/app/AlertDialog;

    move-result-object v0

    .line 6730
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6731
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private Y()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6774
    iget-object v0, p0, Lcom/android/calendar/event/ay;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->n:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_1

    .line 6789
    :cond_0
    :goto_0
    return-void

    .line 6778
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0127

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 6781
    iget-object v1, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/android/calendar/event/ay;->cO:I

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ag()Z

    move-result v1

    if-nez v1, :cond_2

    .line 6782
    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    .line 6785
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/ay;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 6786
    iget-object v1, p0, Lcom/android/calendar/event/ay;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 6787
    iget-object v1, p0, Lcom/android/calendar/event/ay;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 6788
    iget-object v1, p0, Lcom/android/calendar/event/ay;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method static synthetic Y(Lcom/android/calendar/event/ay;)[Landroid/widget/ToggleButton;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic Z(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private Z()Landroid/app/Dialog;
    .locals 8

    .prologue
    .line 6971
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 6972
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->J()Z

    .line 6975
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 6976
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->p()V

    .line 6977
    const/4 v0, 0x0

    .line 7010
    :goto_0
    return-object v0

    .line 6981
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v0

    if-nez v0, :cond_7

    .line 6982
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 6987
    :goto_1
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ad()V

    .line 6989
    iget-object v5, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    .line 6990
    iget-object v0, p0, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 6991
    iget-object v0, p0, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 6993
    :cond_4
    new-instance v7, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-direct {v7, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 6995
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->y:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 6996
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v0, v7, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 6997
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->y:J

    invoke-virtual {v7, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 6998
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 7001
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 7002
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 7003
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    invoke-virtual {v1, v0}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 7005
    :cond_6
    new-instance v0, Lcom/android/calendar/event/ig;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v3, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/event/ig;-><init>(Lcom/android/calendar/event/ay;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->at:Landroid/app/Dialog;

    .line 7009
    iget-object v0, p0, Lcom/android/calendar/event/ay;->at:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 7010
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->aa()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 6984
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method private a(Landroid/database/Cursor;)I
    .locals 14

    .prologue
    const/16 v13, 0xc

    const/4 v12, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 3930
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    move v4, v5

    .line 4034
    :cond_0
    :goto_0
    return v4

    .line 3934
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preference_google_account_set_flag"

    invoke-static {v0, v1, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 3936
    invoke-static {p1}, Lcom/android/calendar/event/av;->a(Landroid/database/Cursor;)Z

    move-result v1

    .line 3938
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 3939
    invoke-static {p1}, Lcom/android/calendar/event/av;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 3940
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v2, "preference_defaultCalendar"

    invoke-static {v1, v2, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3942
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preference_defaultCalendar_account_type"

    const-string v2, "com.google"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3945
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preference_google_account_set_flag"

    invoke-static {v0, v1, v12}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 3949
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preference_defaultCalendar"

    const-string v2, "local"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3951
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preference_defaultCalendar_account_type"

    const-string v2, "local"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3953
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v2, "preference_defaultCalendar_display_name"

    const-string v6, "My calendar"

    invoke-static {v0, v2, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3958
    sget-object v0, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 3959
    sget-object v3, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    .line 3960
    const-string v1, "com.seven.Z7.work"

    .line 3963
    :cond_3
    if-eqz v3, :cond_0

    .line 3967
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "account_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3968
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v7, "account_type"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3970
    if-eqz v2, :cond_c

    if-eqz v0, :cond_c

    const-string v7, "com.android.exchange"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 3974
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v3, "preference_defaultCalendar"

    invoke-static {v1, v3, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3976
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v3, "preference_defaultCalendar_account_type"

    invoke-static {v1, v3, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 3981
    :goto_1
    invoke-static {v1}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v3

    .line 3982
    if-eqz v1, :cond_b

    .line 3983
    if-eqz v3, :cond_6

    const-string v2, "preferences_confirm_new_event_account"

    .line 3985
    :goto_2
    iget-object v7, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v7, v2, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    .line 3988
    :goto_3
    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/android/calendar/event/ay;->bI:Z

    if-nez v2, :cond_5

    invoke-static {}, Lcom/android/calendar/dz;->i()Z

    move-result v2

    if-nez v2, :cond_5

    .line 3990
    invoke-static {p1}, Lcom/android/calendar/event/av;->a(Landroid/database/Cursor;)Z

    move-result v7

    .line 3991
    if-eqz v7, :cond_4

    if-nez v3, :cond_5

    .line 3992
    :cond_4
    new-instance v8, Landroid/os/Handler;

    invoke-direct {v8}, Landroid/os/Handler;-><init>()V

    .line 3994
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 3995
    const/16 v9, 0x168

    if-gt v2, v9, :cond_7

    .line 3996
    const/16 v2, 0x15e

    .line 4001
    :goto_4
    new-instance v9, Lcom/android/calendar/event/cd;

    invoke-direct {v9, p0, v3, v7}, Lcom/android/calendar/event/cd;-><init>(Lcom/android/calendar/event/ay;ZZ)V

    int-to-long v10, v2

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4010
    :cond_5
    const-string v2, "ownerAccount"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 4012
    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v2, v4

    .line 4013
    :goto_5
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 4014
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {p1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 4017
    invoke-interface {p1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 4020
    if-eqz v3, :cond_8

    invoke-interface {p1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 4023
    add-int/lit8 v2, v2, 0x1

    .line 4024
    goto :goto_5

    .line 3983
    :cond_6
    const-string v2, "preferences_confirm_new_event"

    goto :goto_2

    .line 3997
    :cond_7
    iget-boolean v2, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v2, :cond_a

    .line 3998
    const/16 v2, 0xfa

    goto :goto_4

    .line 4028
    :cond_8
    invoke-virtual {p0, v5}, Lcom/android/calendar/event/ay;->b(Ljava/lang/String;)V

    move v4, v2

    .line 4030
    goto/16 :goto_0

    .line 4032
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_a
    move v2, v4

    goto :goto_4

    :cond_b
    move v2, v4

    goto/16 :goto_3

    :cond_c
    move-object v0, v1

    move-object v1, v3

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cf:I

    return p1
.end method

.method public static a(Ljava/lang/String;J)J
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 6616
    if-nez p0, :cond_0

    .line 6617
    const-string p0, "GMT"

    .line 6620
    :cond_0
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7, p0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 6621
    invoke-virtual {v7, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 6622
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 6623
    iget v1, v7, Landroid/text/format/Time;->second:I

    iget v2, v7, Landroid/text/format/Time;->minute:I

    iget v3, v7, Landroid/text/format/Time;->hour:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v5, v7, Landroid/text/format/Time;->month:I

    iget v6, v7, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 6626
    invoke-virtual {v7, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    sub-long v0, v2, v0

    return-wide v0
.end method

.method private a(Landroid/app/Activity;)Landroid/app/AlertDialog;
    .locals 5

    .prologue
    .line 6737
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 6739
    const v1, 0x7f04003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 6740
    const v1, 0x7f12007b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 6741
    const v2, 0x7f120143

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 6743
    const v3, 0x7f12007e

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 6746
    new-instance v4, Lcom/android/calendar/event/ew;

    invoke-direct {v4, p0, v3}, Lcom/android/calendar/event/ew;-><init>(Lcom/android/calendar/event/ay;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6754
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6756
    const v4, 0x7f0f0399

    invoke-virtual {p1, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6757
    const v1, 0x7f0f02e1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02e3

    new-instance v4, Lcom/android/calendar/event/ex;

    invoke-direct {v4, p0, v3, p1}, Lcom/android/calendar/event/ex;-><init>(Lcom/android/calendar/event/ay;Landroid/widget/CheckBox;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6770
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;Landroid/text/format/Time;)Landroid/text/format/Time;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    return-object p1
.end method

.method private a(Lcom/android/calendar/d/a/a;JI)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 6793
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 6794
    const v1, 0x7f0f0128

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 6795
    const v1, 0x7f0f0125

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 6797
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6798
    const v1, 0x7f0f0127

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 6799
    const v1, 0x7f0f0126

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 6802
    :cond_0
    const v1, 0x7f0f0137

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 6803
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6804
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6808
    :cond_1
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    .line 6809
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MMM dd"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 6810
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz p4, :cond_3

    .line 6811
    const-string v2, "DMY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 6812
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MMM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6824
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    if-eqz p4, :cond_5

    const/4 v4, 0x1

    :goto_1
    move-wide v2, p2

    invoke-static/range {v0 .. v7}, Lcom/android/calendar/d/b;->a(Ljava/lang/String;Lcom/android/calendar/d/a/a;JZZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 6814
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MMM d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6817
    :cond_3
    const-string v2, "DMY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 6818
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dd"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MMM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6820
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MMM dd"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    move v4, v5

    .line 6824
    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/calendar/event/ay;->bB:Ljava/lang/String;

    return-object p1
.end method

.method private static a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 3175
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    .line 3176
    array-length v0, v1

    .line 3177
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 3179
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v1, v0

    .line 3180
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3183
    :cond_0
    return-object v2
.end method

.method private a(JJ)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 7523
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->f()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 7530
    :goto_0
    return-void

    .line 7526
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/event/ay;->b(JJ)Landroid/net/Uri;

    move-result-object v3

    .line 7527
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/event/ay;->c(JJ)Ljava/lang/String;

    move-result-object v5

    .line 7529
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bv:Lcom/android/calendar/event/fe;

    const/4 v1, 0x0

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/fe;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/widget/AutoCompleteTextView;II)V
    .locals 3

    .prologue
    .line 7795
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 7797
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 7798
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v1, v2

    .line 7799
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 7800
    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 7801
    iget v0, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/widget/AutoCompleteTextView;->setDropDownWidth(I)V

    .line 7802
    return-void
.end method

.method private a(Landroid/view/View;J)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4510
    invoke-direct {p0, p2, p3}, Lcom/android/calendar/event/ay;->a(J)[Ljava/lang/String;

    move-result-object v1

    .line 4511
    invoke-direct {p0, p2, p3}, Lcom/android/calendar/event/ay;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 4513
    iget-boolean v2, p0, Lcom/android/calendar/event/ay;->cX:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    .line 4514
    iget-object v2, p0, Lcom/android/calendar/event/ay;->m:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4515
    iget-object v2, p0, Lcom/android/calendar/event/ay;->o:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4521
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4522
    iget-object v2, p0, Lcom/android/calendar/event/ay;->l:Landroid/widget/TextView;

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4523
    iget-object v2, p0, Lcom/android/calendar/event/ay;->l:Landroid/widget/TextView;

    aget-object v1, v1, v4

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4524
    iget-object v1, p0, Lcom/android/calendar/event/ay;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4534
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4535
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->Y()V

    .line 4537
    :cond_0
    return-void

    .line 4517
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/ay;->m:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4518
    iget-object v2, p0, Lcom/android/calendar/event/ay;->o:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 4525
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4526
    iget-object v2, p0, Lcom/android/calendar/event/ay;->n:Landroid/widget/TextView;

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4527
    iget-object v2, p0, Lcom/android/calendar/event/ay;->n:Landroid/widget/TextView;

    aget-object v1, v1, v4

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4528
    iget-object v1, p0, Lcom/android/calendar/event/ay;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    move-object v0, p1

    .line 4530
    check-cast v0, Landroid/widget/Button;

    aget-object v2, v1, v3

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4531
    aget-object v0, v1, v4

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private a(Landroid/widget/TextView;Z)V
    .locals 2

    .prologue
    .line 7749
    if-nez p1, :cond_0

    .line 7767
    :goto_0
    return-void

    .line 7756
    :cond_0
    if-eqz p2, :cond_1

    .line 7757
    invoke-virtual {p1}, Landroid/widget/TextView;->getInputType()I

    move-result v0

    const/high16 v1, 0x80000

    or-int/2addr v1, v0

    .line 7758
    const-string v0, "inputType=PredictionOff"

    .line 7765
    :goto_1
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setInputType(I)V

    .line 7766
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    goto :goto_0

    .line 7761
    :cond_1
    invoke-virtual {p1}, Landroid/widget/TextView;->getInputType()I

    move-result v0

    const v1, -0x80001

    and-int/2addr v1, v0

    .line 7762
    const-string v0, ""

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;Landroid/view/View;J)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 3607
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 3636
    :cond_0
    :goto_0
    return-void

    .line 3610
    :cond_1
    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 3611
    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 3612
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3613
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3615
    :cond_2
    instance-of v0, p2, Landroid/widget/RadioGroup;

    if-eqz v0, :cond_3

    move-object v0, p2

    .line 3616
    check-cast v0, Landroid/widget/RadioGroup;

    .line 3617
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 3618
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3619
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3621
    :cond_3
    instance-of v0, p2, Landroid/widget/Spinner;

    if-eqz v0, :cond_4

    .line 3622
    check-cast p2, Landroid/widget/Spinner;

    .line 3623
    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3624
    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 3625
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3626
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3629
    :cond_4
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 3630
    check-cast p2, Landroid/view/ViewGroup;

    .line 3631
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 3632
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_0

    .line 3633
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/android/calendar/event/ay;->a(Ljava/lang/StringBuilder;Landroid/view/View;)V

    .line 3632
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private a(Ljava/util/HashMap;)V
    .locals 2

    .prologue
    .line 4227
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/AttendeesView;->setRfc822Validator(Lcom/android/b/b;)V

    .line 4228
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, p1}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/util/HashMap;)V

    .line 4229
    return-void
.end method

.method private a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 7054
    const/4 v0, 0x0

    .line 7055
    if-eqz p1, :cond_0

    .line 7056
    array-length v0, p1

    .line 7058
    :cond_0
    add-int/lit8 v1, v0, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->aw:[Ljava/lang/String;

    .line 7059
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aw:[Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->n:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 7061
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/calendar/event/ay;->aw:[Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p1, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7064
    const/4 v0, 0x0

    .line 7065
    if-eqz p2, :cond_1

    .line 7066
    array-length v0, p2

    .line 7067
    :cond_1
    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ax:[Ljava/lang/String;

    .line 7068
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ax:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 7069
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ax:[Ljava/lang/String;

    const-string v2, "444444444444444444444444444444444444444444444444"

    aput-object v2, v1, v0

    .line 7068
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7073
    :cond_2
    iget-wide v0, p0, Lcom/android/calendar/event/ay;->bM:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 7074
    const-string v0, "content://com.android.email.provider/account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 7075
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "emailAddress = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v6, v6, Lcom/android/calendar/as;->n:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 7082
    if-eqz v1, :cond_3

    .line 7083
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7084
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/ay;->bM:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7088
    :cond_3
    if-eqz v1, :cond_4

    .line 7089
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7095
    :cond_4
    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "to"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "displayName"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "email"

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "mergedFreeBusy"

    aput-object v1, v4, v0

    .line 7100
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v0, "accountId=? AND startTime=\'?\' AND endTime=\'?\' AND "

    invoke-direct {v5, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 7102
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 7105
    if-eqz p2, :cond_7

    .line 7106
    array-length v2, p2

    .line 7107
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_7

    .line 7108
    aget-object v3, p2, v0

    .line 7109
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 7110
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 7111
    const-string v6, "<"

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v6, ">"

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 7112
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 7113
    aput-object v3, p2, v0

    .line 7114
    const-string v3, " OR to=\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    aget-object v6, p2, v0

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v6, "\'"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7107
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7088
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    .line 7089
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 7121
    :cond_7
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 7122
    const/4 v0, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(to=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 7123
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 7127
    :goto_2
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 7130
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 7131
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 7132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    sub-int v0, v1, v0

    int-to-long v0, v0

    .line 7135
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 7136
    iget-object v6, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v3, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 7137
    const/4 v6, 0x0

    iput v6, v3, Landroid/text/format/Time;->hour:I

    .line 7138
    const/4 v6, 0x0

    iput v6, v3, Landroid/text/format/Time;->minute:I

    .line 7139
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 7143
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 7144
    iget-object v7, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v6, v7}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 7145
    const/16 v7, 0x17

    iput v7, v6, Landroid/text/format/Time;->hour:I

    .line 7146
    const/16 v7, 0x3b

    iput v7, v6, Landroid/text/format/Time;->minute:I

    .line 7147
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/text/format/Time;->normalize(Z)J

    .line 7149
    invoke-virtual {v2}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 7150
    new-instance v7, Ljava/sql/Date;

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v7}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v7

    if-nez v7, :cond_a

    new-instance v7, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v7}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 7152
    invoke-virtual {v2}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v2

    int-to-long v8, v2

    sub-long/2addr v0, v8

    .line 7162
    :cond_8
    :goto_3
    const-class v2, Ljava/util/Locale;

    monitor-enter v2

    .line 7163
    :try_start_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    .line 7164
    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v8}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 7166
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-virtual {v3, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 7167
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->normalize(Z)J

    .line 7168
    const-string v8, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 7170
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    sub-long v0, v8, v0

    invoke-virtual {v6, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 7171
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 7172
    const-string v0, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7174
    invoke-static {v7}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 7175
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 7177
    const/4 v1, 0x3

    new-array v6, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-wide v8, p0, Lcom/android/calendar/event/ay;->bM:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    const/4 v1, 0x1

    aput-object v3, v6, v1

    const/4 v1, 0x2

    aput-object v0, v6, v1

    .line 7181
    const-string v0, "EditEvent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selection = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 7182
    const-string v0, "EditEvent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAccountId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/calendar/event/ay;->bM:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 7184
    iput-object p2, p0, Lcom/android/calendar/event/ay;->ay:[Ljava/lang/String;

    .line 7186
    new-instance v0, Lcom/android/calendar/event/fk;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/fk;-><init>(Lcom/android/calendar/event/ay;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bL:Landroid/content/AsyncQueryHandler;

    .line 7187
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bL:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/android/calendar/event/ParticipantScheduleActivity;->a:Landroid/net/Uri;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 7191
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v1, 0x7f0f0244

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 7192
    return-void

    .line 7125
    :cond_9
    const-string v0, "to=\'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 7153
    :cond_a
    new-instance v7, Ljava/sql/Date;

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v7}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v7

    if-eqz v7, :cond_8

    new-instance v7, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v7}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 7155
    invoke-virtual {v2}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v2

    int-to-long v8, v2

    add-long/2addr v0, v8

    goto/16 :goto_3

    .line 7175
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 6673
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 6676
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 6677
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/text/format/Time;Landroid/text/format/Time;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 6645
    iput v0, p1, Landroid/text/format/Time;->hour:I

    .line 6646
    iput v0, p1, Landroid/text/format/Time;->minute:I

    .line 6647
    iput v0, p1, Landroid/text/format/Time;->second:I

    .line 6648
    invoke-virtual {p1, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 6650
    iput v0, p2, Landroid/text/format/Time;->hour:I

    .line 6651
    iput v0, p2, Landroid/text/format/Time;->minute:I

    .line 6652
    iput v0, p2, Landroid/text/format/Time;->second:I

    .line 6653
    invoke-virtual {p2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 6657
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->b(Landroid/text/format/Time;)V

    .line 6658
    invoke-direct {p0, p2}, Lcom/android/calendar/event/ay;->b(Landroid/text/format/Time;)V

    .line 6660
    invoke-virtual {p2, p1}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6661
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0f0344

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 6662
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 6667
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 6665
    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;Landroid/text/format/Time;Landroid/text/format/Time;)Z
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/event/ay;->a(Landroid/text/format/Time;Landroid/text/format/Time;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/ay;Z)Z
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/android/calendar/event/ay;->bH:Z

    return p1
.end method

.method private a(J)[Ljava/lang/String;
    .locals 17

    .prologue
    .line 4540
    const/4 v2, 0x2

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v12, v2

    const/4 v2, 0x1

    const-string v3, ""

    aput-object v3, v12, v2

    .line 4544
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    if-nez v2, :cond_0

    move-object v2, v12

    .line 4647
    :goto_0
    return-object v2

    .line 4549
    :cond_0
    new-instance v4, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([C)V

    .line 4551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4552
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0f0001

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 4554
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ko"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 4556
    :cond_1
    const-string v2, "MDY"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4558
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f02a2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    .line 4585
    :goto_1
    invoke-static {}, Lcom/android/calendar/hj;->o()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 4586
    const/16 v2, 0x2c

    const/16 v4, 0x60c

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    move-object v11, v3

    .line 4589
    :goto_2
    const-class v3, Ljava/util/TimeZone;

    monitor-enter v3

    .line 4590
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    .line 4593
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    .line 4594
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4596
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    move-wide/from16 v0, p1

    invoke-static {v2, v0, v1}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;J)J

    move-result-wide v14

    .line 4597
    const-string v2, "EditEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tzOffet:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " millies:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 4599
    sub-long v2, p1, v14

    const-wide v4, -0x1f3a565e880L

    cmp-long v2, v2, v4

    if-gez v2, :cond_9

    .line 4600
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 4601
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x76e

    invoke-virtual/range {v2 .. v8}, Landroid/text/format/Time;->set(IIIIII)V

    .line 4602
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide p1

    .line 4603
    const-string v2, "EditEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "after min tzOffet:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " millies:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 4613
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->c()Z

    move-result v2

    if-nez v2, :cond_a

    .line 4614
    const-string v2, "EditEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DateFormat millies:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, p1, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 4615
    sub-long v2, p1, v14

    invoke-static {v11, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4616
    sub-long v4, p1, v14

    invoke-static {v10, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4644
    :goto_4
    const/4 v4, 0x0

    aput-object v3, v12, v4

    .line 4645
    const/4 v3, 0x1

    aput-object v2, v12, v3

    move-object v2, v12

    .line 4647
    goto/16 :goto_0

    .line 4560
    :cond_3
    const-string v2, "YMD"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0490

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4562
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0491

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    goto/16 :goto_1

    .line 4565
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f013c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4566
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f013d

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    goto/16 :goto_1

    .line 4570
    :cond_5
    const-string v5, "MDY"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 4571
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4572
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0002

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 4580
    :cond_6
    :goto_5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "kn"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ml"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 4581
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EEEE"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v10, v2

    goto/16 :goto_1

    .line 4574
    :cond_8
    const-string v5, "YMD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 4575
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4576
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0003

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 4594
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 4604
    :cond_9
    sub-long v2, p1, v14

    const-wide v4, 0x1ec4d45f520L

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 4605
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 4606
    const/4 v3, 0x0

    const/16 v4, 0x3b

    const/16 v5, 0x17

    const/16 v6, 0x1f

    const/16 v7, 0xb

    const/16 v8, 0x7f4

    invoke-virtual/range {v2 .. v8}, Landroid/text/format/Time;->set(IIIIII)V

    .line 4607
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide p1

    .line 4608
    const-string v2, "EditEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "after max tzOffet:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " millies:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 4620
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0f0128

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 4621
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0f0125

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 4623
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 4624
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0f0127

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 4625
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f0f0126

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 4628
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    sub-long v4, p1, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ay;->cO:I

    if-eqz v2, :cond_c

    const/4 v6, 0x1

    :goto_6
    const/4 v7, 0x1

    move-object v2, v11

    invoke-static/range {v2 .. v9}, Lcom/android/calendar/d/b;->a(Ljava/lang/String;Lcom/android/calendar/d/a/a;JZZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 4632
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    sub-long v4, p1, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ay;->cO:I

    if-eqz v2, :cond_d

    const/4 v6, 0x1

    :goto_7
    const/4 v7, 0x1

    move-object v2, v10

    invoke-static/range {v2 .. v9}, Lcom/android/calendar/d/b;->a(Ljava/lang/String;Lcom/android/calendar/d/a/a;JZZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4638
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v3

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ay;->cO:I

    if-eqz v3, :cond_e

    .line 4639
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0377

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v11, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    .line 4628
    :cond_c
    const/4 v6, 0x0

    goto :goto_6

    .line 4632
    :cond_d
    const/4 v6, 0x0

    goto :goto_7

    :cond_e
    move-object v3, v11

    goto/16 :goto_4

    :cond_f
    move-object v11, v3

    goto/16 :goto_2

    :cond_10
    move-object v10, v2

    goto/16 :goto_1
.end method

.method private aa()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 7014
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->au:Z

    if-eqz v0, :cond_0

    .line 7015
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->p()V

    .line 7016
    iget-object v0, p0, Lcom/android/calendar/event/ay;->at:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 7017
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->au:Z

    .line 7018
    const/4 v0, 0x0

    .line 7020
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->at:Landroid/app/Dialog;

    goto :goto_0
.end method

.method static synthetic aa(Lcom/android/calendar/event/ay;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic ab(Lcom/android/calendar/event/ay;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bl:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private ab()V
    .locals 3

    .prologue
    .line 7030
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 7031
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->J()Z

    .line 7035
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 7036
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->p()V

    .line 7049
    :cond_3
    :goto_0
    return-void

    .line 7040
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ad()V

    .line 7042
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 7043
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 7044
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 7045
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v1

    .line 7044
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 7047
    :cond_5
    invoke-direct {p0, v2, v2}, Lcom/android/calendar/event/ay;->a([Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic ac(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cf:I

    return v0
.end method

.method private ac()V
    .locals 3

    .prologue
    .line 7294
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-nez v0, :cond_1

    .line 7318
    :cond_0
    :goto_0
    return-void

    .line 7299
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    const-string v1, "maxReminders"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 7300
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lcom/android/calendar/as;->f:I

    .line 7301
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->f:I

    iput v0, p0, Lcom/android/calendar/event/ay;->cn:I

    .line 7302
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    const-string v1, "allowedReminders"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 7304
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/as;->g:Ljava/lang/String;

    .line 7305
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->co:Ljava/lang/String;

    .line 7310
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 7311
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->aw:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 7312
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, Lcom/android/calendar/as;->H:Z

    .line 7314
    iget-object v0, p0, Lcom/android/calendar/event/ay;->a:Landroid/widget/ScrollView;

    const v1, 0x7f1200f8

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 7316
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 7317
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->K()V

    goto :goto_0

    .line 7312
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic ad(Lcom/android/calendar/event/ay;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    return-object v0
.end method

.method private ad()V
    .locals 4

    .prologue
    .line 7454
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 7455
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v1

    .line 7456
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 7457
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 7458
    iget-object v2, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/event/AttendeesView;->b(I)Lcom/android/calendar/at;

    move-result-object v2

    .line 7459
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v3, v0}, Lcom/android/calendar/event/AttendeesView;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 7457
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7462
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    iget-object v2, v2, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 7465
    :cond_2
    return-void
.end method

.method private ae()V
    .locals 1

    .prologue
    .line 7484
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    if-eqz v0, :cond_0

    .line 7485
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->a()V

    .line 7487
    :cond_0
    return-void
.end method

.method static synthetic ae(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->R()V

    return-void
.end method

.method static synthetic af(Lcom/android/calendar/event/ay;)Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cq:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method private af()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xc8

    const/4 v2, 0x2

    .line 7635
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aR:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    .line 7636
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aR:Landroid/animation/AnimatorSet;

    .line 7638
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 7639
    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 7640
    new-instance v1, Lcom/android/calendar/event/ey;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ey;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 7649
    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 7650
    invoke-virtual {v1, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 7651
    new-instance v2, Lcom/android/calendar/event/ez;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/ez;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 7661
    iget-object v2, p0, Lcom/android/calendar/event/ay;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 7662
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aR:Landroid/animation/AnimatorSet;

    new-instance v1, Landroid/view/animation/interpolator/SineInOut70;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineInOut70;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 7666
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 7667
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 7668
    return-void

    .line 7638
    :array_0
    .array-data 4
        0x3f59999a    # 0.85f
        0x3f800000    # 1.0f
    .end array-data

    .line 7649
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic ag(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->be:I

    return v0
.end method

.method private ag()Z
    .locals 2

    .prologue
    .line 7770
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic ah(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cg:I

    return v0
.end method

.method static synthetic ai(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->bd:I

    return v0
.end method

.method static synthetic aj(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bN:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic ak(Lcom/android/calendar/event/ay;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cG:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic al(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cH:I

    return v0
.end method

.method static synthetic am(Lcom/android/calendar/event/ay;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cI:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic an(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cJ:I

    return v0
.end method

.method static synthetic ao(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bK:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cO:I

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/ay;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private b(JJ)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 7533
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 7534
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 7535
    invoke-static {v0, p3, p4}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 7536
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    return-object v0
.end method

.method private b(J)Ljava/lang/String;
    .locals 9

    .prologue
    .line 4651
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 4652
    const-string v0, ""

    .line 4671
    :goto_0
    return-object v0

    .line 4655
    :cond_0
    const/16 v0, 0x301

    .line 4656
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4657
    const/16 v0, 0x381

    .line 4661
    :cond_1
    const-class v1, Ljava/util/TimeZone;

    monitor-enter v1

    .line 4662
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    .line 4663
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v2, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 4664
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 4665
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4667
    :cond_2
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    .line 4669
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/android/calendar/event/ay;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    return-object p1
.end method

.method private static b(Landroid/content/res/Resources;I)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 3190
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 3191
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private b(Landroid/text/format/Time;)V
    .locals 2

    .prologue
    .line 6632
    const-string v0, "America/Sao_Paulo"

    iget-object v1, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "America/Santiago"

    iget-object v1, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Asia/Shanghai"

    iget-object v1, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Asia/Harbin"

    iget-object v1, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Asia/Chongqing"

    iget-object v1, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6637
    :cond_0
    iget-boolean v0, p1, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_2

    iget v0, p1, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_1

    iget v0, p1, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_1

    iget v0, p1, Landroid/text/format/Time;->second:I

    if-eqz v0, :cond_2

    .line 6638
    :cond_1
    invoke-static {p1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 6641
    :cond_2
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 4301
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 4302
    if-nez v5, :cond_1

    .line 4398
    :cond_0
    :goto_0
    return-void

    .line 4305
    :cond_1
    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 4306
    if-eqz v4, :cond_0

    .line 4310
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    .line 4311
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 4312
    new-instance v1, Landroid/view/animation/interpolator/SineInOut80;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineInOut80;-><init>()V

    invoke-virtual {v3, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 4313
    const-wide/16 v6, 0x10e

    invoke-virtual {v3, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 4314
    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 4316
    new-instance v1, Lcom/android/calendar/event/cn;

    invoke-direct {v1, p0, v4}, Lcom/android/calendar/event/cn;-><init>(Lcom/android/calendar/event/ay;Landroid/widget/LinearLayout;)V

    invoke-virtual {v3, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 4339
    new-instance v1, Lcom/android/calendar/event/co;

    invoke-direct {v1, p0, v4, v0, v5}, Lcom/android/calendar/event/co;-><init>(Lcom/android/calendar/event/ay;Landroid/widget/LinearLayout;ILandroid/widget/LinearLayout;)V

    invoke-virtual {v3, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 4356
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v1, 0x7f05000d

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v6

    .line 4357
    new-instance v0, Landroid/view/animation/interpolator/SineInOut60;

    invoke-direct {v0}, Landroid/view/animation/interpolator/SineInOut60;-><init>()V

    invoke-virtual {v6, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 4358
    new-instance v0, Lcom/android/calendar/event/cp;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/event/cp;-><init>(Lcom/android/calendar/event/ay;Landroid/view/View;Landroid/animation/ValueAnimator;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;)V

    invoke-virtual {v6, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4397
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 4311
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic b(Lcom/android/calendar/event/ay;Z)Z
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/android/calendar/event/ay;->ce:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cc:I

    return p1
.end method

.method public static c(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 6604
    if-nez p0, :cond_0

    .line 6605
    const-string p0, "GMT"

    .line 6608
    :cond_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 6609
    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 6610
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-long v0, v0

    .line 6612
    return-wide v0
.end method

.method static synthetic c(Lcom/android/calendar/event/ay;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    return-object v0
.end method

.method private c(JJ)Ljava/lang/String;
    .locals 7

    .prologue
    .line 7540
    const-string v0, "visible=1 AND deleted!=1"

    .line 7541
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7542
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7545
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7547
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND allDay!=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7548
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND begin!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7549
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND end!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7551
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->b:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->aq:Z

    if-nez v1, :cond_1

    .line 7552
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND event_id!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7555
    :cond_1
    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/ay;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/android/calendar/event/ay;Z)Z
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/android/calendar/event/ay;->cY:Z

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cd:I

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->F()V

    return-void
.end method

.method static synthetic d(Lcom/android/calendar/event/ay;Z)Z
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/android/calendar/event/ay;->bI:Z

    return p1
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6897
    invoke-static {p0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v2

    .line 6898
    array-length v3, v2

    if-ne v3, v0, :cond_0

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cg:I

    return p1
.end method

.method static synthetic e(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->L()V

    return-void
.end method

.method static synthetic e(Lcom/android/calendar/event/ay;Z)Z
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/android/calendar/event/ay;->bs:Z

    return p1
.end method

.method static synthetic f(I)I
    .locals 0

    .prologue
    .line 185
    sput p0, Lcom/android/calendar/event/ay;->ci:I

    return p0
.end method

.method static synthetic f(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->bS:I

    return p1
.end method

.method static synthetic f(Lcom/android/calendar/event/ay;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/event/ay;Z)Z
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/android/calendar/event/ay;->bw:Z

    return p1
.end method

.method static synthetic g(I)I
    .locals 0

    .prologue
    .line 185
    sput p0, Lcom/android/calendar/event/ay;->cj:I

    return p0
.end method

.method static synthetic g(Lcom/android/calendar/event/ay;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/event/ay;I)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->i(I)V

    return-void
.end method

.method private g(Z)V
    .locals 7

    .prologue
    .line 5259
    new-instance v2, Lcom/android/calendar/event/dg;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/dg;-><init>(Lcom/android/calendar/event/ay;)V

    .line 5267
    const/4 v4, 0x0

    .line 5269
    if-eqz p1, :cond_2

    .line 5270
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->aA:Lcom/android/calendar/colorpicker/e;

    if-eqz v0, :cond_0

    .line 5271
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->aA:Lcom/android/calendar/colorpicker/e;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/android/calendar/colorpicker/e;->a(Ljava/lang/String;Ljava/lang/String;)[I

    move-result-object v4

    .line 5281
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 5282
    sget v0, Lcom/android/calendar/event/ay;->ci:I

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    iget v5, p0, Lcom/android/calendar/event/ay;->cP:I

    iget v6, p0, Lcom/android/calendar/event/ay;->cQ:I

    move v3, p1

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/colorpicker/a;->a(JLcom/android/calendar/colorpicker/d;Z[III)Lcom/android/calendar/colorpicker/a;

    move-result-object v0

    .line 5285
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 5286
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "ColorPickerDialog"

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/colorpicker/a;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 5288
    :cond_1
    return-void

    .line 5275
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->aB:Lcom/android/calendar/colorpicker/e;

    if-eqz v0, :cond_0

    .line 5276
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->aB:Lcom/android/calendar/colorpicker/e;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/android/calendar/colorpicker/e;->a(Ljava/lang/String;Ljava/lang/String;)[I

    move-result-object v4

    goto :goto_0
.end method

.method static synthetic h(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->cr:I

    return v0
.end method

.method static synthetic h(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cP:I

    return p1
.end method

.method private h(I)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3824
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_6

    .line 3825
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-virtual {v0}, Lcom/android/calendar/as;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3826
    iget-object v0, p0, Lcom/android/calendar/event/ay;->s:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 3827
    iget-object v0, p0, Lcom/android/calendar/event/ay;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3828
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->z()V

    .line 3829
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->C()V

    .line 3830
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->D()V

    .line 3831
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3832
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 3836
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->f(Z)V

    .line 3839
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 3841
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_2

    move v1, v2

    .line 3842
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 3843
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v4, v0, Lcom/android/calendar/as;->c:J

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-nez v0, :cond_8

    .line 3844
    iget-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 3845
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3851
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-eqz v0, :cond_5

    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->aq:Z

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_5
    if-ne p1, v8, :cond_9

    .line 3855
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 3860
    :goto_2
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_a

    .line 3861
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 3867
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->c(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3868
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3917
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 3918
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->b(Ljava/lang/String;)V

    .line 3921
    :cond_6
    return-void

    .line 3834
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 3842
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 3857
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    .line 3863
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 3864
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_3

    .line 3870
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 3874
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-nez v0, :cond_f

    .line 3875
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 3899
    :cond_d
    :goto_5
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->H()Z

    move-result v1

    .line 3900
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    if-nez v0, :cond_18

    .line 3901
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_15

    .line 3902
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    if-nez v1, :cond_e

    move v2, v3

    :cond_e
    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    goto :goto_4

    .line 3877
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->aq:Z

    if-eqz v0, :cond_11

    :cond_10
    if-ne p1, v8, :cond_11

    .line 3879
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_5

    .line 3880
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-nez v0, :cond_14

    if-ne p1, v8, :cond_14

    .line 3883
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    move v1, v2

    .line 3885
    :goto_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 3886
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v4, v0, Lcom/android/calendar/as;->c:J

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-nez v0, :cond_13

    .line 3887
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_12

    .line 3888
    iget-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 3890
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_5

    .line 3885
    :cond_13
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 3895
    :cond_14
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_5

    .line 3904
    :cond_15
    iget-object v4, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    if-nez v1, :cond_16

    move v0, v3

    :goto_7
    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 3905
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    if-nez v1, :cond_17

    :goto_8
    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto/16 :goto_4

    :cond_16
    move v0, v2

    .line 3904
    goto :goto_7

    :cond_17
    move v3, v2

    .line 3905
    goto :goto_8

    .line 3908
    :cond_18
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_19

    .line 3909
    iget-object v0, p0, Lcom/android/calendar/event/ay;->v:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    goto/16 :goto_4

    .line 3911
    :cond_19
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 3912
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    goto/16 :goto_4
.end method

.method static synthetic i(Lcom/android/calendar/event/ay;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/android/calendar/event/ay;->bW:I

    return v0
.end method

.method static synthetic i(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cQ:I

    return p1
.end method

.method private i(I)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 4232
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->f:I

    if-lt p1, v0, :cond_1

    .line 4233
    iget-object v0, p0, Lcom/android/calendar/event/ay;->T:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4234
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v0, :cond_0

    .line 4235
    iget-object v0, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 4236
    iget-object v0, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 4258
    :cond_0
    :goto_0
    return-void

    .line 4238
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->f:I

    if-ge p1, v0, :cond_0

    .line 4239
    iget-object v0, p0, Lcom/android/calendar/event/ay;->T:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4240
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v0, :cond_3

    .line 4241
    iget-object v0, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 4242
    iget-object v0, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 4243
    if-gtz p1, :cond_2

    .line 4244
    iget-object v0, p0, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 4246
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 4249
    :cond_3
    if-gtz p1, :cond_4

    .line 4250
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 4251
    iget-object v0, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0

    .line 4253
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 4254
    iget-object v0, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method private j(I)Landroid/widget/MultiAutoCompleteTextView;
    .locals 3

    .prologue
    .line 4402
    iget-object v0, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/MultiAutoCompleteTextView;

    .line 4403
    new-instance v1, Lcom/android/calendar/fk;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/android/calendar/fk;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    .line 4404
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    check-cast v1, Lcom/android/calendar/fk;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 4405
    new-instance v1, Landroid/text/util/Rfc822Tokenizer;

    invoke-direct {v1}, Landroid/text/util/Rfc822Tokenizer;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    .line 4406
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    .line 4407
    new-instance v1, Lcom/android/calendar/event/cr;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cr;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 4438
    sget-object v1, Lcom/android/calendar/event/ay;->cZ:[Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 4440
    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/event/ay;I)Landroid/widget/MultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->k(I)Landroid/widget/MultiAutoCompleteTextView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cm:I

    return p1
.end method

.method private k(I)Landroid/widget/MultiAutoCompleteTextView;
    .locals 3

    .prologue
    .line 4445
    iget-object v0, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/MultiAutoCompleteTextView;

    .line 4447
    iget-boolean v1, p0, Lcom/android/calendar/event/ay;->bs:Z

    if-eqz v1, :cond_0

    .line 4448
    new-instance v1, Lcom/android/calendar/fk;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/android/calendar/fk;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    .line 4449
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    check-cast v1, Lcom/android/calendar/fk;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 4454
    :goto_0
    new-instance v1, Landroid/text/util/Rfc822Tokenizer;

    invoke-direct {v1}, Landroid/text/util/Rfc822Tokenizer;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    .line 4455
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    .line 4456
    new-instance v1, Lcom/android/calendar/event/cs;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cs;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 4497
    return-object v0

    .line 4451
    :cond_0
    new-instance v1, Lcom/android/calendar/fv;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/android/calendar/fv;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    .line 4452
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    check-cast v1, Lcom/android/calendar/fv;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/calendar/event/ay;)Z
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->J()Z

    move-result v0

    return v0
.end method

.method static synthetic l(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cr:I

    return p1
.end method

.method private l(I)Landroid/app/AlertDialog;
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 5571
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 5572
    const v1, 0x7f040082

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 5575
    new-instance v1, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-direct {v1, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cs:Landroid/text/format/Time;

    .line 5576
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->d:I

    iput v1, p0, Lcom/android/calendar/event/ay;->ct:I

    .line 5577
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->e:I

    iput v1, p0, Lcom/android/calendar/event/ay;->cu:I

    .line 5578
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cv:Ljava/lang/String;

    .line 5579
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->q:I

    iput v1, p0, Lcom/android/calendar/event/ay;->cw:I

    .line 5580
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->o:I

    iput v1, p0, Lcom/android/calendar/event/ay;->cx:I

    .line 5581
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v1, v1, Lcom/android/calendar/as;->T:I

    iput v1, p0, Lcom/android/calendar/event/ay;->cy:I

    .line 5582
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->U:Z

    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->cz:Z

    .line 5583
    iget v1, p0, Lcom/android/calendar/event/ay;->cg:I

    iput v1, p0, Lcom/android/calendar/event/ay;->cA:I

    .line 5584
    iget-object v1, p0, Lcom/android/calendar/event/ay;->O:[Z

    iget-object v4, p0, Lcom/android/calendar/event/ay;->cB:[Z

    iget-object v5, p0, Lcom/android/calendar/event/ay;->O:[Z

    array-length v5, v5

    invoke-static {v1, v6, v4, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5586
    new-instance v7, Lcom/android/calendar/event/fl;

    invoke-direct {v7, p0}, Lcom/android/calendar/event/fl;-><init>(Lcom/android/calendar/event/ay;)V

    .line 5589
    const v1, 0x7f120269

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->aT:Landroid/view/ViewGroup;

    .line 5590
    const v1, 0x7f12026b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    .line 5591
    const v1, 0x7f12026a

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->K:Landroid/widget/TextView;

    .line 5592
    const v1, 0x7f12026c

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->L:Landroid/widget/TextView;

    .line 5594
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->e:I

    const/16 v4, 0x63

    if-le v1, v4, :cond_0

    .line 5595
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    const/16 v4, 0x63

    iput v4, v1, Lcom/android/a/c;->e:I

    .line 5597
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 5598
    iget-object v4, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 5599
    iget-object v4, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    invoke-virtual {v4, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 5600
    iget-object v1, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setSelection(I)V

    .line 5603
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    new-instance v4, Lcom/android/calendar/event/do;

    const/16 v5, 0x63

    invoke-direct {v4, p0, v11, v11, v5}, Lcom/android/calendar/event/do;-><init>(Lcom/android/calendar/event/ay;III)V

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 5615
    iget-object v1, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    new-instance v4, Lcom/android/calendar/event/dp;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/dp;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 5637
    const v1, 0x7f120275

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->aU:Landroid/widget/LinearLayout;

    .line 5639
    const v1, 0x7f120277

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->aW:Landroid/widget/RadioButton;

    .line 5640
    const v1, 0x7f120276

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->aY:Landroid/widget/TextView;

    .line 5641
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aY:Landroid/widget/TextView;

    new-instance v4, Lcom/android/calendar/event/dr;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/dr;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5650
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aW:Landroid/widget/RadioButton;

    new-instance v4, Lcom/android/calendar/event/ds;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/ds;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5661
    const v1, 0x7f120279

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->aV:Landroid/widget/RadioButton;

    .line 5662
    const v1, 0x7f120278

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->aX:Landroid/widget/TextView;

    .line 5663
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aX:Landroid/widget/TextView;

    new-instance v4, Lcom/android/calendar/event/dt;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/dt;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5672
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aV:Landroid/widget/RadioButton;

    new-instance v4, Lcom/android/calendar/event/dv;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/dv;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5682
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 5683
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cE:[Ljava/lang/String;

    if-nez v1, :cond_2

    .line 5684
    const v1, 0x7f090017

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cE:[Ljava/lang/String;

    .line 5687
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cF:[Ljava/lang/String;

    if-nez v1, :cond_3

    .line 5688
    const v1, 0x7f09002a

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cF:[Ljava/lang/String;

    .line 5691
    :cond_3
    invoke-static {}, Lcom/android/calendar/hj;->o()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 5692
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const v4, 0x7f0f012e

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v6

    const v4, 0x7f0f012f

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v11

    const v4, 0x7f0f0130

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    const v4, 0x7f0f0131

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const v4, 0x7f0f0132

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v12

    const/4 v4, 0x5

    const v5, 0x7f0f0133

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x6

    const v5, 0x7f0f0134

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cD:[Ljava/lang/String;

    .line 5717
    :goto_0
    const v1, 0x7f12026d

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->N:Landroid/widget/LinearLayout;

    .line 5718
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    const v1, 0x7f12026e

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    aput-object v1, v4, v6

    .line 5719
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    const v1, 0x7f12026f

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    aput-object v1, v4, v11

    .line 5720
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    const v1, 0x7f120270

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    aput-object v1, v4, v2

    .line 5721
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    const v1, 0x7f120271

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    aput-object v1, v4, v3

    .line 5722
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    const v1, 0x7f120272

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    aput-object v1, v4, v12

    .line 5723
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    const/4 v5, 0x5

    const v1, 0x7f120273

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    aput-object v1, v4, v5

    .line 5724
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    const/4 v5, 0x6

    const v1, 0x7f120274

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    aput-object v1, v4, v5

    move v5, v6

    move v4, v6

    .line 5729
    :goto_1
    const/4 v1, 0x7

    if-ge v5, v1, :cond_a

    .line 5730
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    aget-object v1, v1, v5

    iget-object v9, p0, Lcom/android/calendar/event/ay;->bm:[I

    aget v9, v9, v5

    invoke-static {v9, v6}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ToggleButton;->setText(Ljava/lang/CharSequence;)V

    .line 5731
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    aget-object v1, v1, v5

    iget-object v9, p0, Lcom/android/calendar/event/ay;->bm:[I

    aget v9, v9, v5

    invoke-static {v9, v6}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    .line 5732
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    aget-object v1, v1, v5

    iget-object v9, p0, Lcom/android/calendar/event/ay;->bm:[I

    aget v9, v9, v5

    invoke-static {v9, v6}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ToggleButton;->setTextOn(Ljava/lang/CharSequence;)V

    .line 5734
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v1

    if-eqz v1, :cond_9

    move v1, v2

    .line 5735
    :goto_2
    iget-object v9, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    aget-object v9, v9, v5

    iget-object v10, p0, Lcom/android/calendar/event/ay;->bm:[I

    aget v10, v10, v5

    invoke-static {v10, v1}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/widget/ToggleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 5737
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->V:[Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->V:[Z

    aget-boolean v1, v1, v5

    if-nez v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/android/calendar/event/ay;->O:[Z

    aget-boolean v1, v1, v5

    if-eqz v1, :cond_6

    .line 5739
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    aget-object v1, v1, v5

    iget-object v9, p0, Lcom/android/calendar/event/ay;->bm:[I

    aget v9, v9, v5

    invoke-static {v9, v6}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ToggleButton;->setText(Ljava/lang/CharSequence;)V

    .line 5740
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    aget-object v1, v1, v5

    invoke-virtual {v1, v11}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 5743
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    aget-object v1, v1, v5

    new-instance v9, Lcom/android/calendar/event/dw;

    invoke-direct {v9, p0}, Lcom/android/calendar/event/dw;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v9}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5770
    iget-object v1, p0, Lcom/android/calendar/event/ay;->O:[Z

    aget-boolean v1, v1, v5

    if-nez v1, :cond_10

    .line 5771
    add-int/lit8 v1, v4, 0x1

    .line 5729
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v1

    goto/16 :goto_1

    .line 5702
    :cond_7
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x14

    .line 5703
    :goto_4
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {v11, v1}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v1}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v11

    invoke-static {v3, v1}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v12, v1}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v5, 0x5

    invoke-static {v5, v1}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v12

    const/4 v5, 0x5

    const/4 v9, 0x6

    invoke-static {v9, v1}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x6

    const/4 v9, 0x7

    invoke-static {v9, v1}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    iput-object v4, p0, Lcom/android/calendar/event/ay;->cD:[Ljava/lang/String;

    goto/16 :goto_0

    .line 5702
    :cond_8
    const/16 v1, 0xa

    goto :goto_4

    :cond_9
    move v1, v3

    .line 5734
    goto/16 :goto_2

    .line 5776
    :cond_a
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    if-eqz v1, :cond_b

    const/4 v1, 0x7

    if-ne v4, v1, :cond_b

    .line 5777
    iget-object v1, p0, Lcom/android/calendar/event/ay;->O:[Z

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->weekDay:I

    aput-boolean v11, v1, v3

    .line 5778
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->weekDay:I

    aget-object v1, v1, v3

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bm:[I

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->weekDay:I

    aget v3, v3, v4

    invoke-static {v3, v6}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ToggleButton;->setText(Ljava/lang/CharSequence;)V

    .line 5780
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bn:[Landroid/widget/ToggleButton;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->weekDay:I

    aget-object v1, v1, v3

    invoke-virtual {v1, v11}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 5784
    :cond_b
    const v1, 0x7f12027a

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->aZ:Landroid/view/ViewGroup;

    .line 5785
    const v1, 0x7f0f036a

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bi:Ljava/lang/String;

    .line 5786
    const v1, 0x7f0f0369

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bj:Ljava/lang/String;

    .line 5789
    const v1, 0x7f12027b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    .line 5790
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    invoke-virtual {v1, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 5791
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bh:Ljava/util/ArrayList;

    if-nez v1, :cond_c

    .line 5792
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bh:Ljava/util/ArrayList;

    .line 5793
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bh:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 5794
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bh:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bi:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5795
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bh:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bj:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5797
    :cond_c
    new-instance v1, Lcom/android/calendar/gl;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v3, 0x7f040081

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bh:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/calendar/gl;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bg:Lcom/android/calendar/gl;

    .line 5798
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bg:Lcom/android/calendar/gl;

    const v2, 0x7f040080

    invoke-virtual {v1, v2}, Lcom/android/calendar/gl;->setDropDownViewResource(I)V

    .line 5799
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bg:Lcom/android/calendar/gl;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 5800
    const v1, 0x7f12027c

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    .line 5802
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v1}, Lcom/android/calendar/d/g;->c()Z

    move-result v1

    if-eqz v1, :cond_d

    iget v1, p0, Lcom/android/calendar/event/ay;->cO:I

    if-eqz v1, :cond_d

    .line 5803
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getTextSize()F

    move-result v1

    const v2, 0x3f666666    # 0.9f

    mul-float/2addr v1, v2

    .line 5804
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    invoke-virtual {v2, v6, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 5807
    :cond_d
    const v1, 0x7f12027d

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    .line 5809
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->d:I

    if-le v1, v11, :cond_f

    .line 5810
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v2, v2, Lcom/android/a/c;->d:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 5815
    :goto_5
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    new-instance v2, Lcom/android/calendar/event/dx;

    const/4 v3, 0x5

    const/16 v4, 0x2da

    invoke-direct {v2, p0, v11, v3, v4}, Lcom/android/calendar/event/dx;-><init>(Lcom/android/calendar/event/ay;III)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 5827
    const v1, 0x7f12027e

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bk:Landroid/view/ViewGroup;

    .line 5828
    const v1, 0x7f120280

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/android/calendar/event/ay;->bl:Landroid/widget/CheckBox;

    .line 5829
    const v1, 0x7f12027f

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/dy;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/dy;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5837
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bl:Landroid/widget/CheckBox;

    new-instance v2, Lcom/android/calendar/event/dz;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/dz;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 5869
    invoke-virtual {v7, p1}, Lcom/android/calendar/event/fl;->a(I)V

    .line 5871
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->m(I)V

    .line 5872
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->Q()V

    .line 5874
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0379

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/ec;

    invoke-direct {v1, p0, p1}, Lcom/android/calendar/event/ec;-><init>(Lcom/android/calendar/event/ay;I)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0398

    invoke-virtual {v0, v1, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f00a4

    new-instance v2, Lcom/android/calendar/event/eb;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/eb;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/ea;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ea;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 5906
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 5908
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bN:Ljava/util/ArrayList;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ne p1, v0, :cond_e

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->a(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preferences_confirm_leap_event"

    invoke-static {v0, v1, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_e

    .line 5911
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {p0, v0, v6, v6, v11}, Lcom/android/calendar/event/ay;->a(Landroid/app/Activity;ZZZ)V

    .line 5914
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    return-object v0

    .line 5812
    :cond_f
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    iget v2, p0, Lcom/android/calendar/event/ay;->cg:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_10
    move v1, v4

    goto/16 :goto_3
.end method

.method static synthetic l(Lcom/android/calendar/event/ay;)Lcom/android/a/c;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->bf:I

    return p1
.end method

.method static synthetic m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    return-object v0
.end method

.method private m(I)V
    .locals 11

    .prologue
    const/4 v10, -0x2

    const/4 v9, 0x2

    const/16 v3, 0x8

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 5919
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 5921
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->c:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->g()I

    move-result v0

    if-nez v0, :cond_12

    .line 5924
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    const/16 v1, 0x7b2

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_2

    .line 5925
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->n(I)Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 5926
    iget v0, p0, Lcom/android/calendar/event/ay;->bd:I

    iput v0, p0, Lcom/android/calendar/event/ay;->bf:I

    .line 5935
    :cond_2
    :goto_0
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 5937
    packed-switch p1, :pswitch_data_0

    .line 6013
    :goto_1
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/event/ay;->N:Landroid/widget/LinearLayout;

    if-ne p1, v9, :cond_19

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 6014
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aU:Landroid/widget/LinearLayout;

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1a

    move v0, v2

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 6016
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aT:Landroid/view/ViewGroup;

    if-nez p1, :cond_1b

    move v0, v3

    :goto_4
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 6017
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aZ:Landroid/view/ViewGroup;

    if-nez p1, :cond_1c

    move v0, v3

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 6018
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bk:Landroid/view/ViewGroup;

    if-nez p1, :cond_1d

    move v0, v3

    :goto_6
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 6023
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v0, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_3
    iget v0, p0, Lcom/android/calendar/event/ay;->bf:I

    iget v1, p0, Lcom/android/calendar/event/ay;->bd:I

    if-ne v0, v1, :cond_1e

    .line 6024
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ay;->bd:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 6025
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 6026
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 6036
    :cond_5
    :goto_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-ne v0, v8, :cond_6

    .line 6037
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    const v1, 0x259d23

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 6040
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget-boolean v0, v0, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->second:I

    if-eqz v0, :cond_8

    .line 6042
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 6045
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v0, v0, Lcom/android/calendar/event/fm;->c:[Landroid/text/format/Time;

    aget-object v0, v0, p1

    .line 6047
    iget-boolean v1, v0, Landroid/text/format/Time;->allDay:Z

    if-eqz v1, :cond_a

    iget v1, v0, Landroid/text/format/Time;->hour:I

    if-nez v1, :cond_9

    iget v1, v0, Landroid/text/format/Time;->minute:I

    if-nez v1, :cond_9

    iget v1, v0, Landroid/text/format/Time;->second:I

    if-eqz v1, :cond_a

    .line 6048
    :cond_9
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 6051
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v0, v0, Lcom/android/calendar/event/fm;->c:[Landroid/text/format/Time;

    aget-object v0, v0, p1

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v0, v0, Lcom/android/calendar/event/fm;->a:[I

    aget v0, v0, p1

    if-ne v0, v9, :cond_b

    .line 6053
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iget-object v3, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v3, v3, Lcom/android/calendar/event/fm;->c:[Landroid/text/format/Time;

    aget-object v3, v3, p1

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gtz v0, :cond_b

    .line 6054
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->aF:Lcom/android/calendar/event/fm;

    iget-object v1, v1, Lcom/android/calendar/event/fm;->c:[Landroid/text/format/Time;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 6060
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-boolean v0, v0, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->second:I

    if-eqz v0, :cond_d

    .line 6062
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 6065
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-nez v0, :cond_e

    .line 6066
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 6071
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-boolean v0, v0, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->second:I

    if-eqz v0, :cond_10

    .line 6073
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 6076
    :cond_10
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->U:Z

    if-eqz v0, :cond_22

    .line 6077
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v3, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_21

    .line 6078
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-direct {p0, v0, v4, v5}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    .line 6079
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 6083
    :goto_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bl:Landroid/widget/CheckBox;

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 6084
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 6085
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 6098
    :goto_9
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/ff;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/event/ff;-><init>(Lcom/android/calendar/event/ay;Landroid/text/format/Time;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6101
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    if-eqz v0, :cond_11

    .line 6102
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 6103
    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 6104
    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 6105
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 6107
    :cond_11
    return-void

    .line 5929
    :cond_12
    iget v0, p0, Lcom/android/calendar/event/ay;->cc:I

    if-ne v0, p1, :cond_13

    iget v0, p0, Lcom/android/calendar/event/ay;->cf:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    :cond_13
    if-eqz p1, :cond_2

    .line 5931
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->n(I)Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto/16 :goto_0

    .line 5939
    :pswitch_1
    iput v2, p0, Lcom/android/calendar/event/ay;->cd:I

    .line 5940
    const v0, 0x7f0e0005

    iput v0, p0, Lcom/android/calendar/event/ay;->cb:I

    goto/16 :goto_1

    .line 5946
    :pswitch_2
    iput v8, p0, Lcom/android/calendar/event/ay;->cd:I

    .line 5947
    const v0, 0x7f0e0007

    iput v0, p0, Lcom/android/calendar/event/ay;->cb:I

    goto/16 :goto_1

    .line 5951
    :pswitch_3
    iput v9, p0, Lcom/android/calendar/event/ay;->cd:I

    .line 5952
    const v0, 0x7f0e0006

    iput v0, p0, Lcom/android/calendar/event/ay;->cb:I

    .line 5954
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->T:I

    if-nez v0, :cond_16

    .line 5955
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aW:Landroid/widget/RadioButton;

    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 5956
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aV:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 5963
    :cond_14
    :goto_a
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v0, v0, Lcom/android/a/c;->q:I

    if-ne v0, v8, :cond_17

    .line 5964
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aW:Landroid/widget/RadioButton;

    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 5965
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aV:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 5971
    :cond_15
    :goto_b
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 5975
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_18

    iget v0, p0, Lcom/android/calendar/event/ay;->cO:I

    if-eqz v0, :cond_18

    .line 5976
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aW:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 5977
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aV:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 5978
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aX:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5979
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iput v8, v0, Lcom/android/a/c;->q:I

    .line 5980
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput v2, v0, Lcom/android/calendar/as;->T:I

    .line 5982
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    iget v5, v4, Landroid/text/format/Time;->year:I

    iget v6, v4, Landroid/text/format/Time;->month:I

    iget v7, v4, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v0, v5, v6, v7}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 5983
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/d/a/a;->c()I

    move-result v0

    .line 5985
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 5986
    const-string v1, " ("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v6, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v7, 0x7f0f0127

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v6, ")"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 5987
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5992
    :goto_c
    iget-object v5, p0, Lcom/android/calendar/event/ay;->aY:Landroid/widget/TextView;

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5995
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 5996
    iget v0, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    .line 5997
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aX:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->cF:[Ljava/lang/String;

    aget-object v0, v5, v0

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/android/calendar/event/ay;->cD:[Ljava/lang/String;

    iget v4, v4, Landroid/text/format/Time;->weekDay:I

    aget-object v4, v6, v4

    aput-object v4, v5, v2

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 5957
    :cond_16
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->T:I

    if-ne v0, v8, :cond_14

    .line 5958
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aW:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 5959
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aV:Landroid/widget/RadioButton;

    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_a

    .line 5966
    :cond_17
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v0, v0, Lcom/android/a/c;->o:I

    if-ne v0, v8, :cond_15

    .line 5967
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aW:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 5968
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aV:Landroid/widget/RadioButton;

    invoke-virtual {v0, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_b

    .line 5990
    :cond_18
    iget v0, v4, Landroid/text/format/Time;->monthDay:I

    goto :goto_c

    .line 6001
    :pswitch_4
    iput v9, p0, Lcom/android/calendar/event/ay;->cd:I

    .line 6002
    const v0, 0x7f0e0008

    iput v0, p0, Lcom/android/calendar/event/ay;->cb:I

    goto/16 :goto_1

    :cond_19
    move v0, v3

    .line 6013
    goto/16 :goto_2

    :cond_1a
    move v0, v3

    .line 6014
    goto/16 :goto_3

    :cond_1b
    move v0, v2

    .line 6016
    goto/16 :goto_4

    :cond_1c
    move v0, v2

    .line 6017
    goto/16 :goto_5

    :cond_1d
    move v0, v2

    .line 6018
    goto/16 :goto_6

    .line 6027
    :cond_1e
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v0, v0, Lcom/android/a/c;->d:I

    if-gtz v0, :cond_20

    :cond_1f
    iget v0, p0, Lcom/android/calendar/event/ay;->bf:I

    iget v1, p0, Lcom/android/calendar/event/ay;->be:I

    if-ne v0, v1, :cond_5

    .line 6028
    :cond_20
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ay;->be:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 6029
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 6030
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 6031
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 6032
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setSelected(Z)V

    goto/16 :goto_7

    .line 6081
    :cond_21
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v1, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    invoke-direct {p0, v0, v4, v5}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    goto/16 :goto_8

    .line 6087
    :cond_22
    iget-object v0, p0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cq:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_23

    .line 6088
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->c(Ljava/lang/String;)J

    move-result-wide v0

    .line 6089
    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    add-long/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 6091
    :cond_23
    const-wide/16 v0, 0x0

    .line 6092
    iget-object v2, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 6093
    const-string v0, "UTC"

    invoke-static {v0}, Lcom/android/calendar/event/ay;->c(Ljava/lang/String;)J

    move-result-wide v0

    .line 6095
    :cond_24
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    sub-long v0, v4, v0

    invoke-direct {p0, v2, v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    goto/16 :goto_9

    .line 5937
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic n(Lcom/android/calendar/event/ay;I)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->l(I)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private n(I)Landroid/text/format/Time;
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x0

    .line 6292
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 6293
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v1, Landroid/text/format/Time;->year:I

    .line 6294
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v5, v1, Landroid/text/format/Time;->month:I

    .line 6295
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    .line 6296
    const-string v3, "EditEvent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Y:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " M:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "D:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 6301
    iget-object v3, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v3}, Lcom/android/calendar/d/g;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/android/calendar/event/ay;->cO:I

    if-lez v3, :cond_2

    .line 6302
    iget-object v3, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v3, v2, v5, v1}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 6303
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->a()I

    move-result v3

    .line 6304
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->b()I

    move-result v5

    .line 6305
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->c()I

    move-result v2

    .line 6306
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->d()Z

    move-result v1

    move v9, v1

    move v1, v2

    move v2, v3

    move v3, v9

    .line 6310
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 6335
    :pswitch_0
    add-int/lit8 v1, v1, 0x1

    .line 6341
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v4}, Lcom/android/calendar/d/g;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/calendar/event/ay;->cO:I

    if-lez v4, :cond_1

    .line 6343
    :try_start_0
    iget-object v4, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v4, v2, v5, v1, v3}, Lcom/android/calendar/d/a/a;->a(IIIZ)V

    .line 6345
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->a()I

    move-result v2

    .line 6346
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->b()I

    move-result v5

    .line 6347
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cN:Lcom/android/calendar/d/a/a;

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->c()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v4, v1

    move v6, v2

    .line 6357
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->second:I

    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->minute:I

    iget-object v3, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->hour:I

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 6360
    iget-boolean v1, p0, Lcom/android/calendar/event/ay;->cC:Z

    if-eqz v1, :cond_0

    .line 6361
    iget-object v1, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget-boolean v1, v1, Landroid/text/format/Time;->allDay:Z

    iput-boolean v1, v0, Landroid/text/format/Time;->allDay:Z

    .line 6364
    :cond_0
    return-object v0

    .line 6312
    :pswitch_1
    iput v4, p0, Lcom/android/calendar/event/ay;->cd:I

    .line 6313
    add-int/lit8 v2, v2, 0x1

    .line 6314
    goto :goto_1

    .line 6319
    :pswitch_2
    const/4 v4, 0x1

    iput v4, p0, Lcom/android/calendar/event/ay;->cd:I

    .line 6320
    add-int/lit8 v2, v2, 0x5

    .line 6321
    goto :goto_1

    .line 6325
    :pswitch_3
    iput v8, p0, Lcom/android/calendar/event/ay;->cd:I

    .line 6326
    add-int/lit8 v2, v2, 0xa

    .line 6327
    goto :goto_1

    .line 6330
    :pswitch_4
    iput v8, p0, Lcom/android/calendar/event/ay;->cd:I

    .line 6331
    add-int/lit8 v2, v2, 0x32

    .line 6332
    goto :goto_1

    .line 6348
    :catch_0
    move-exception v1

    .line 6349
    const-string v2, "EditEvent"

    const-string v3, "Wrong date argument in solarLunar converter"

    invoke-static {v2, v3, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6350
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v1, Landroid/text/format/Time;->year:I

    .line 6351
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v5, v1, Landroid/text/format/Time;->month:I

    .line 6352
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    move v4, v1

    move v6, v2

    goto :goto_2

    :cond_1
    move v4, v1

    move v6, v2

    goto :goto_2

    :cond_2
    move v3, v4

    goto :goto_0

    .line 6310
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic n(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->W()V

    return-void
.end method

.method static synthetic o(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cH:I

    return p1
.end method

.method static synthetic o(Lcom/android/calendar/event/ay;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    return v0
.end method

.method static synthetic p(Lcom/android/calendar/event/ay;I)I
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/android/calendar/event/ay;->cJ:I

    return p1
.end method

.method static synthetic p(Lcom/android/calendar/event/ay;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic q(Lcom/android/calendar/event/ay;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    return v0
.end method

.method static synthetic r(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ab()V

    return-void
.end method

.method static synthetic s(Lcom/android/calendar/event/ay;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->Z()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic t(Lcom/android/calendar/event/ay;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic v(Lcom/android/calendar/event/ay;)Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bH:Z

    return v0
.end method

.method static synthetic w(Lcom/android/calendar/event/ay;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ch:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x()I
    .locals 1

    .prologue
    .line 185
    sget v0, Lcom/android/calendar/event/ay;->ci:I

    return v0
.end method

.method static synthetic x(Lcom/android/calendar/event/ay;)Landroid/animation/LayoutTransition;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aS:Landroid/animation/LayoutTransition;

    return-object v0
.end method

.method static synthetic y(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->C()V

    return-void
.end method

.method static synthetic y()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/android/calendar/event/ay;->aN:[Ljava/lang/String;

    return-object v0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 1760
    iget-object v0, p0, Lcom/android/calendar/event/ay;->c:Landroid/view/View;

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-nez v0, :cond_1

    .line 1826
    :cond_0
    :goto_0
    return-void

    .line 1763
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->b:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->c:Landroid/view/View;

    .line 1764
    iget-object v0, p0, Lcom/android/calendar/event/ay;->c:Landroid/view/View;

    const v1, 0x7f120132

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    .line 1765
    iget-object v0, p0, Lcom/android/calendar/event/ay;->c:Landroid/view/View;

    const v1, 0x7f120133

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    .line 1766
    iget-object v0, p0, Lcom/android/calendar/event/ay;->c:Landroid/view/View;

    const v1, 0x7f120134

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    .line 1767
    iget-object v0, p0, Lcom/android/calendar/event/ay;->c:Landroid/view/View;

    const v1, 0x7f120135

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->g:Landroid/widget/ImageButton;

    .line 1768
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->b()V

    .line 1769
    iget-object v0, p0, Lcom/android/calendar/event/ay;->g:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/event/bm;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/bm;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method static synthetic z(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->z()V

    return-void
.end method


# virtual methods
.method public a()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 1900
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getAttendeePopupDialog()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 3924
    iput p1, p0, Lcom/android/calendar/event/ay;->bW:I

    .line 3925
    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 7611
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 7612
    iget-object v1, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 7613
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-nez v1, :cond_2

    .line 7614
    iget-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 7615
    iget-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 7616
    iget-object v0, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 7628
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_1

    .line 7629
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-object p3, v0, Lcom/android/calendar/as;->as:Ljava/lang/String;

    .line 7630
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-wide p1, v0, Lcom/android/calendar/as;->ar:J

    .line 7632
    :cond_1
    return-void

    .line 7617
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7619
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 7620
    invoke-static {p3, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 7621
    iget-object v1, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 7622
    iget-object v0, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 7623
    iget-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 7625
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->af()V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;ZZZ)V
    .locals 8

    .prologue
    const v7, 0x7f0f01b1

    const v6, 0x7f0f02e3

    .line 4039
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4216
    :cond_0
    :goto_0
    return-void

    .line 4042
    :cond_1
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 4044
    const v1, 0x7f04003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 4045
    const v1, 0x7f12007b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 4046
    const v2, 0x7f120143

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 4048
    const v3, 0x7f12007e

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 4051
    new-instance v4, Lcom/android/calendar/event/ce;

    invoke-direct {v4, p0, v3}, Lcom/android/calendar/event/ce;-><init>(Lcom/android/calendar/event/ay;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4059
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 4064
    :cond_2
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 4066
    if-eqz p2, :cond_4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    const/16 v5, 0x64

    if-ge v4, v5, :cond_4

    .line 4072
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v5, 0x7f0f01ae

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4073
    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/ch;

    invoke-direct {v1, p0, v3}, Lcom/android/calendar/event/ch;-><init>(Lcom/android/calendar/event/ay;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v6, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f002e

    new-instance v3, Lcom/android/calendar/event/cg;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/cg;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/cf;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cf;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 4119
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4121
    :try_start_0
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    .line 4122
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4123
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 4125
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4126
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    goto/16 :goto_0

    .line 4131
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 4128
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 4136
    :cond_4
    invoke-static {}, Lcom/android/calendar/dz;->j()Z

    move-result v4

    if-nez v4, :cond_7

    if-eqz p3, :cond_7

    const-string v4, "USA"

    invoke-static {}, Lcom/android/calendar/dz;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "ATT"

    invoke-static {}, Lcom/android/calendar/dz;->G()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 4144
    :cond_5
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v5, 0x7f0f01b0

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4145
    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/cj;

    invoke-direct {v1, p0, v3}, Lcom/android/calendar/event/cj;-><init>(Lcom/android/calendar/event/ay;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v6, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/ci;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ci;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 4169
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4171
    :try_start_1
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    .line 4172
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4173
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 4175
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 4176
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    goto/16 :goto_0

    .line 4181
    :catch_1
    move-exception v0

    goto/16 :goto_0

    .line 4178
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 4186
    :cond_7
    if-eqz p4, :cond_0

    .line 4187
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v5, 0x7f0f027d

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4188
    const v1, 0x7f0f0379

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/cm;

    invoke-direct {v1, p0, v3}, Lcom/android/calendar/event/cm;-><init>(Lcom/android/calendar/event/ay;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v6, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/ck;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ck;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 4207
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4209
    :try_start_2
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 4210
    :catch_2
    move-exception v0

    goto/16 :goto_0
.end method

.method public a(Landroid/database/Cursor;ZZI)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 3672
    iput-object p1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    .line 3673
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 3674
    :cond_0
    if-nez p2, :cond_2

    .line 3808
    :cond_1
    :goto_0
    return-void

    .line 3679
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3680
    const v1, 0x7f0f02db

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f02d0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 3684
    const v1, 0x7f0f002e

    iget-object v2, p0, Lcom/android/calendar/event/ay;->aL:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    iget-object v3, p0, Lcom/android/calendar/event/ay;->aL:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3687
    new-instance v1, Lcom/android/calendar/event/cc;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cc;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 3697
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3698
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    goto :goto_0

    .line 3703
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ay;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 3704
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move v0, v1

    .line 3705
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 3706
    iget-object v3, p0, Lcom/android/calendar/event/ay;->ck:Ljava/util/ArrayList;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3707
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 3705
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3711
    :cond_4
    if-eqz p3, :cond_9

    .line 3712
    sput p4, Lcom/android/calendar/event/ay;->ci:I

    .line 3717
    :goto_2
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_a

    .line 3718
    new-instance v0, Lcom/android/calendar/event/fd;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v2, p1}, Lcom/android/calendar/event/fd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aE:Lcom/android/calendar/event/fd;

    .line 3719
    iget-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->aE:Lcom/android/calendar/event/fd;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 3720
    iget-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    sget v2, Lcom/android/calendar/event/ay;->ci:I

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 3721
    iget-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 3722
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bB:Ljava/lang/String;

    .line 3724
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    sget v2, Lcom/android/calendar/event/ay;->ci:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3725
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ay;->cQ:I

    .line 3726
    iget v0, p0, Lcom/android/calendar/event/ay;->cQ:I

    if-nez v0, :cond_5

    .line 3727
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0073

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ay;->cQ:I

    .line 3770
    :cond_5
    :goto_3
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    .line 3771
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    .line 3773
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3774
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    const-string v2, "com.android.exchange"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 3775
    iput-boolean v5, p0, Lcom/android/calendar/event/ay;->bs:Z

    .line 3779
    :goto_4
    const v0, 0x7f1200fe

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->k(I)Landroid/widget/MultiAutoCompleteTextView;

    .line 3782
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->b(Ljava/lang/String;)V

    .line 3783
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    const-string v2, "com.android.exchange"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 3784
    iput-boolean v5, p0, Lcom/android/calendar/event/ay;->bs:Z

    .line 3785
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_7

    .line 3786
    iget-object v0, p0, Lcom/android/calendar/event/ay;->w:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ay;->cm:I

    .line 3789
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_8

    .line 3790
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v2, p0, Lcom/android/calendar/event/ay;->cm:I

    iput v2, v0, Lcom/android/calendar/as;->J:I

    .line 3793
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getAttendeeItemCount()I

    move-result v0

    if-lez v0, :cond_f

    .line 3794
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3804
    :goto_5
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ay;->cn:I

    .line 3805
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->co:Ljava/lang/String;

    goto/16 :goto_0

    .line 3714
    :cond_9
    sput v2, Lcom/android/calendar/event/ay;->ci:I

    goto/16 :goto_2

    .line 3730
    :cond_a
    new-instance v0, Lcom/android/calendar/event/fc;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v2, p1}, Lcom/android/calendar/event/fc;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aD:Lcom/android/calendar/event/fc;

    .line 3731
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    sget v2, Lcom/android/calendar/event/ay;->ci:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3734
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    const/16 v2, 0xc

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3736
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v2, "preferences_samsung_account_validation"

    invoke-static {v0, v2, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_b

    .line 3739
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.calendar.event.action.accountvalidationcheck"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3740
    const-string v2, "request_module"

    const/16 v3, 0x1f5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3743
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const/16 v3, 0x1f4

    invoke-virtual {v2, v0, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3749
    :goto_6
    sput v1, Lcom/android/calendar/event/ay;->cj:I

    .line 3752
    :cond_b
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3753
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/ay;->bB:Ljava/lang/String;

    .line 3755
    const-string v2, "My calendar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 3756
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f02ba

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3759
    :cond_c
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 3760
    if-nez v2, :cond_d

    .line 3761
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0073

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 3764
    :cond_d
    invoke-static {v2}, Lcom/android/calendar/hj;->b(I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/event/ay;->cQ:I

    .line 3766
    iget-object v2, p0, Lcom/android/calendar/event/ay;->A:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3767
    iget-object v0, p0, Lcom/android/calendar/event/ay;->z:Landroid/view/View;

    iget v2, p0, Lcom/android/calendar/event/ay;->cQ:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_3

    .line 3745
    :catch_0
    move-exception v0

    .line 3746
    const-string v0, "EditEvent"

    const-string v2, "Error: Could not find AccountValidationCheckActivity.ACTION activity."

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 3777
    :cond_e
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->bs:Z

    goto/16 :goto_4

    .line 3796
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 3799
    :cond_10
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->bs:Z

    .line 3801
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5
.end method

.method public a(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4835
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    if-nez v0, :cond_0

    .line 4836
    iget-object v0, p0, Lcom/android/calendar/event/ay;->an:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    .line 4837
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    const v1, 0x7f120118

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ap:Landroid/widget/ImageView;

    .line 4839
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    const v1, 0x7f120119

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aq:Landroid/widget/ImageButton;

    .line 4840
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aq:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/event/ct;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ct;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4850
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p2}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 4851
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v0

    .line 4852
    iget-object v1, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 4853
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ap:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 4854
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0, p1}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/graphics/Bitmap;)V

    .line 4855
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4856
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aq:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 4857
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1907
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, p1}, Lcom/android/calendar/event/AttendeesView;->a(Landroid/net/Uri;)V

    .line 1909
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 7775
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bG:Z

    if-nez v0, :cond_1

    .line 7791
    :cond_0
    :goto_0
    return-void

    .line 7778
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ag()Z

    move-result v0

    .line 7780
    iget-object v1, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/event/ay;->a(Landroid/widget/TextView;Z)V

    .line 7781
    iget-object v1, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/event/ay;->a(Landroid/widget/TextView;Z)V

    .line 7782
    iget-object v1, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/event/ay;->a(Landroid/widget/TextView;Z)V

    .line 7783
    iget-object v1, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/event/ay;->a(Landroid/widget/TextView;Z)V

    .line 7785
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 7786
    check-cast p1, Landroid/widget/EditText;

    .line 7787
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 7789
    invoke-virtual {v0, p1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(Landroid/widget/MultiAutoCompleteTextView;)V
    .locals 6

    .prologue
    .line 6902
    new-instance v2, Landroid/text/util/Rfc822Tokenizer;

    invoke-direct {v2}, Landroid/text/util/Rfc822Tokenizer;-><init>()V

    .line 6904
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    if-nez v0, :cond_1

    .line 6922
    :cond_0
    return-void

    .line 6908
    :cond_1
    invoke-virtual {p1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 6909
    invoke-virtual {p1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 6910
    :goto_0
    if-lez v0, :cond_0

    .line 6911
    invoke-virtual {v2, v3, v0}, Landroid/text/util/Rfc822Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    .line 6912
    invoke-virtual {v2, v3, v1}, Landroid/text/util/Rfc822Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v4

    .line 6913
    invoke-interface {v3, v1, v4}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    .line 6914
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 6915
    const-string v4, ""

    invoke-interface {v3, v1, v0, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    :cond_2
    :goto_1
    move v0, v1

    .line 6921
    goto :goto_0

    .line 6916
    :cond_3
    iget-object v5, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    invoke-virtual {v5, v4}, Lcom/android/b/b;->isValid(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 6917
    iget-object v5, p0, Lcom/android/calendar/event/ay;->bD:Lcom/android/b/b;

    invoke-virtual {v5, v4}, Lcom/android/b/b;->fixText(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/text/util/Rfc822Tokenizer;->terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v3, v1, v0, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1
.end method

.method public a(Lcom/android/calendar/as;)V
    .locals 12

    .prologue
    const/4 v11, -0x1

    const/16 v10, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3279
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v1, v3

    .line 3282
    :goto_0
    iput-object p1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    .line 3284
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    instance-of v0, v0, Lcom/android/calendar/dg;

    if-eqz v0, :cond_1

    .line 3285
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    check-cast v0, Lcom/android/calendar/dg;

    invoke-virtual {v0}, Lcom/android/calendar/dg;->b()V

    .line 3286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bC:Lcom/android/ex/a/a;

    .line 3289
    :cond_1
    if-nez p1, :cond_3

    .line 3580
    :goto_1
    return-void

    .line 3279
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    move v1, v0

    goto :goto_0

    .line 3296
    :cond_3
    invoke-static {p1}, Lcom/android/calendar/event/av;->d(Lcom/android/calendar/as;)Z

    move-result v4

    .line 3298
    iget-wide v6, p1, Lcom/android/calendar/as;->y:J

    .line 3299
    iget-wide v8, p1, Lcom/android/calendar/as;->B:J

    .line 3300
    iget-object v0, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    .line 3302
    invoke-static {}, Lcom/android/calendar/dz;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3303
    iget-boolean v0, p1, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_4

    const-string v0, "Africa/Casablanca"

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3304
    const-string v0, "UTC"

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    .line 3309
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3310
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 3311
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 3313
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3314
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 3315
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 3317
    iget-object v0, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 3318
    iget-object v5, p1, Lcom/android/calendar/as;->s:Ljava/lang/String;

    .line 3319
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 3320
    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 3321
    iget-object v5, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    invoke-virtual {v5, v0}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 3323
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v0, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_1f

    .line 3324
    new-instance v0, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-direct {v0, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 3325
    iget-object v5, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget-object v5, v5, Lcom/android/a/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 3326
    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v5

    if-ne v5, v2, :cond_7

    .line 3327
    const v5, 0x259d23

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 3329
    :cond_7
    const/4 v5, 0x2

    iput v5, p0, Lcom/android/calendar/event/ay;->cf:I

    .line 3330
    iget-object v5, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 3331
    iget-object v5, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 3341
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3342
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->ay:Z

    if-eqz v0, :cond_21

    move v0, v2

    :goto_3
    iput v0, p0, Lcom/android/calendar/event/ay;->cO:I

    .line 3348
    :cond_9
    iget-boolean v0, p1, Lcom/android/calendar/as;->W:Z

    if-nez v0, :cond_a

    .line 3349
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3350
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v10}, Lcom/android/calendar/event/AttendeesView;->setVisibility(I)V

    .line 3353
    :cond_a
    iget-boolean v0, p1, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_22

    .line 3356
    invoke-virtual {p0, v2}, Lcom/android/calendar/event/ay;->b(Z)V

    .line 3358
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_b

    .line 3359
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3363
    :cond_b
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    .line 3364
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3365
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 3366
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3367
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 3378
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    new-instance v5, Lcom/android/calendar/event/bv;

    invoke-direct {v5, p0}, Lcom/android/calendar/event/bv;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 3399
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 3400
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/android/calendar/event/ay;->bS:I

    .line 3405
    :goto_5
    if-nez v1, :cond_c

    .line 3406
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->K()V

    .line 3409
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0f000d

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3414
    new-instance v0, Lcom/android/calendar/event/bw;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/bw;-><init>(Lcom/android/calendar/event/ay;)V

    .line 3422
    iget-boolean v1, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v1, :cond_24

    .line 3423
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3424
    iget-object v1, p0, Lcom/android/calendar/event/ay;->T:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3430
    :goto_6
    iget-object v0, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 3431
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 3432
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v0

    .line 3433
    iget-object v1, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 3436
    :cond_d
    iget-object v0, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 3437
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 3438
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v0

    .line 3439
    iget-object v1, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 3442
    :cond_e
    iget-object v0, p1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 3443
    iget-object v0, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 3444
    iget-object v0, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 3445
    iget-object v1, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 3448
    :cond_f
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_26

    .line 3449
    iget-object v0, p0, Lcom/android/calendar/event/ay;->x:Landroid/widget/Spinner;

    iget-boolean v1, p1, Lcom/android/calendar/as;->I:Z

    if-eqz v1, :cond_25

    :goto_7
    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 3450
    iget-object v0, p0, Lcom/android/calendar/event/ay;->y:Landroid/widget/Spinner;

    iget v1, p1, Lcom/android/calendar/as;->au:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 3458
    :goto_8
    if-eqz v4, :cond_28

    .line 3459
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    if-nez v0, :cond_10

    .line 3460
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ac:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    .line 3462
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    const v1, 0x7f120147

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/android/calendar/event/ay;->F:Landroid/widget/RadioGroup;

    .line 3463
    iget-object v0, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    const v1, 0x7f120146

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->G:Landroid/view/View;

    .line 3466
    :cond_10
    iget v0, p1, Lcom/android/calendar/as;->X:I

    invoke-static {v0}, Lcom/android/calendar/event/hm;->c(I)I

    move-result v0

    .line 3467
    iget-object v1, p0, Lcom/android/calendar/event/ay;->F:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 3468
    iget-object v0, p0, Lcom/android/calendar/event/ay;->F:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 3469
    iget-object v0, p0, Lcom/android/calendar/event/ay;->G:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3471
    iget-object v0, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    const v1, 0x7f120148

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 3472
    iget-object v1, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    const v2, 0x7f120149

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 3473
    iget-object v2, p0, Lcom/android/calendar/event/ay;->by:Landroid/view/View;

    const v4, 0x7f12014a

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    .line 3476
    new-instance v4, Lcom/android/calendar/event/bx;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/bx;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3482
    new-instance v0, Lcom/android/calendar/event/by;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/by;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3488
    new-instance v0, Lcom/android/calendar/event/bz;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/bz;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3503
    :cond_11
    :goto_9
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->e()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-wide v0, p1, Lcom/android/calendar/as;->c:J

    const-wide/16 v4, 0x2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_13

    :cond_12
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->f()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-wide v0, p1, Lcom/android/calendar/as;->c:J

    const-wide/16 v4, 0x3

    cmp-long v0, v0, v4

    if-nez v0, :cond_14

    .line 3505
    :cond_13
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/android/calendar/event/ay;->G:Landroid/view/View;

    if-eqz v0, :cond_14

    .line 3506
    iget-object v0, p0, Lcom/android/calendar/event/ay;->G:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3507
    iget-object v0, p0, Lcom/android/calendar/event/ay;->F:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v10}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 3508
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3514
    :cond_14
    iget-object v0, p1, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 3518
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 3519
    iget-object v1, p0, Lcom/android/calendar/event/ay;->A:Landroid/widget/TextView;

    if-eqz v1, :cond_17

    .line 3520
    const-string v1, ""

    .line 3521
    iget-object v1, p1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    iget-wide v4, p1, Lcom/android/calendar/as;->c:J

    const-wide/16 v6, 0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_16

    :cond_15
    iget-object v1, p1, Lcom/android/calendar/as;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 3523
    :cond_16
    const v1, 0x7f0f02ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3525
    iget-object v1, p0, Lcom/android/calendar/event/ay;->A:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3530
    :cond_17
    :goto_a
    iget v0, p0, Lcom/android/calendar/event/ay;->cQ:I

    if-nez v0, :cond_18

    .line 3531
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ay;->cQ:I

    .line 3537
    :cond_18
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v0, :cond_19

    .line 3538
    iget-object v0, p0, Lcom/android/calendar/event/ay;->z:Landroid/view/View;

    iget v1, p0, Lcom/android/calendar/event/ay;->cQ:I

    invoke-static {v1}, Lcom/android/calendar/hj;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 3542
    :cond_19
    iget v0, p1, Lcom/android/calendar/as;->J:I

    if-eq v0, v11, :cond_1a

    .line 3543
    iget v0, p1, Lcom/android/calendar/as;->J:I

    iput v0, p0, Lcom/android/calendar/event/ay;->cm:I

    .line 3545
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/android/calendar/event/ay;->w:Landroid/widget/Spinner;

    if-eqz v0, :cond_2a

    .line 3546
    iget-object v0, p0, Lcom/android/calendar/event/ay;->w:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ay;->cm:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 3552
    :cond_1a
    :goto_b
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->B()V

    .line 3553
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->F()V

    .line 3554
    iget-object v0, p1, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->a(Ljava/util/HashMap;)V

    .line 3556
    iget-object v0, p1, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1b

    .line 3557
    iget-object v0, p1, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    iget-object v1, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 3560
    :cond_1b
    invoke-static {}, Lcom/android/calendar/dz;->w()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 3561
    iget v0, p1, Lcom/android/calendar/as;->ai:I

    if-eqz v0, :cond_1c

    iget v0, p1, Lcom/android/calendar/as;->aj:I

    if-eqz v0, :cond_1c

    .line 3562
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    iget v1, p1, Lcom/android/calendar/as;->ai:I

    iget v2, p1, Lcom/android/calendar/as;->aj:I

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/event/EditEventActivity;->a(II)V

    .line 3567
    :cond_1c
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->ar:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1d

    .line 3568
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->z()V

    .line 3569
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->ar:J

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v4, v4, Lcom/android/calendar/as;->ar:J

    invoke-static {v2, v4, v5}, Lcom/android/calendar/gx;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/calendar/event/ay;->a(JLjava/lang/String;)V

    .line 3573
    :cond_1d
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-nez v0, :cond_1e

    iget v0, p1, Lcom/android/calendar/as;->J:I

    if-eq v0, v11, :cond_1e

    .line 3574
    iget-object v0, p0, Lcom/android/calendar/event/ay;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cl:[Ljava/lang/CharSequence;

    iget v2, p1, Lcom/android/calendar/as;->J:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3577
    :cond_1e
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->d()V

    .line 3578
    iget-object v0, p0, Lcom/android/calendar/event/ay;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 3579
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->M()V

    goto/16 :goto_1

    .line 3332
    :cond_1f
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v0, v0, Lcom/android/a/c;->d:I

    if-eqz v0, :cond_20

    .line 3333
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/event/ay;->cf:I

    .line 3334
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v0, v0, Lcom/android/a/c;->d:I

    iput v0, p0, Lcom/android/calendar/event/ay;->cg:I

    goto/16 :goto_2

    .line 3336
    :cond_20
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/calendar/event/ay;->cf:I

    goto/16 :goto_2

    :cond_21
    move v0, v3

    .line 3342
    goto/16 :goto_3

    .line 3369
    :cond_22
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3370
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3371
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 3372
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3373
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    goto/16 :goto_4

    .line 3402
    :cond_23
    const/16 v0, 0xf

    iput v0, p0, Lcom/android/calendar/event/ay;->bS:I

    goto/16 :goto_5

    .line 3426
    :cond_24
    iget-object v1, p0, Lcom/android/calendar/event/ay;->T:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 3427
    iget-object v1, p0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_6

    :cond_25
    move v2, v3

    .line 3449
    goto/16 :goto_7

    .line 3452
    :cond_26
    iget-boolean v0, p1, Lcom/android/calendar/as;->I:Z

    if-eqz v0, :cond_27

    :goto_c
    iput v2, p0, Lcom/android/calendar/event/ay;->cH:I

    .line 3453
    iget-object v0, p0, Lcom/android/calendar/event/ay;->D:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cG:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/ay;->cH:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3454
    iget v0, p1, Lcom/android/calendar/as;->au:I

    iput v0, p0, Lcom/android/calendar/event/ay;->cJ:I

    .line 3455
    iget-object v0, p0, Lcom/android/calendar/event/ay;->E:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cI:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/ay;->cJ:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    :cond_27
    move v2, v3

    .line 3452
    goto :goto_c

    .line 3495
    :cond_28
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/android/calendar/event/ay;->G:Landroid/view/View;

    if-eqz v0, :cond_11

    .line 3496
    iget-object v0, p0, Lcom/android/calendar/event/ay;->G:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3497
    iget-object v0, p0, Lcom/android/calendar/event/ay;->F:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v10}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 3498
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    .line 3527
    :cond_29
    iget-object v0, p0, Lcom/android/calendar/event/ay;->A:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/android/calendar/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 3547
    :cond_2a
    iget-object v0, p0, Lcom/android/calendar/event/ay;->B:Landroid/widget/TextView;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/android/calendar/event/ay;->cl:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_1a

    .line 3548
    iget-object v0, p0, Lcom/android/calendar/event/ay;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->cl:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/ay;->cm:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_b
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1915
    if-eqz p1, :cond_0

    .line 1916
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 1917
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v0

    .line 1918
    iget-object v1, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 1921
    :cond_0
    if-eqz p2, :cond_1

    .line 1922
    iget-object v0, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 1923
    iget-object v0, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 1924
    iget-object v1, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 1926
    :cond_1
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5207
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5208
    iget-object v2, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 5210
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 5211
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/AttendeesView;->setVisibility(I)V

    .line 5212
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    if-eqz v0, :cond_1

    .line 5213
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 5217
    :goto_1
    return-void

    .line 5215
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 4219
    iput-boolean p1, p0, Lcom/android/calendar/event/ay;->bI:Z

    .line 4220
    return-void
.end method

.method public a(ZI)V
    .locals 10

    .prologue
    const-wide/16 v8, 0xc8

    const/4 v6, 0x0

    const-wide/16 v4, 0x64

    .line 4986
    .line 4987
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 4989
    if-nez p1, :cond_1

    .line 5198
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 4993
    :cond_1
    if-nez p2, :cond_6

    .line 4994
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 4995
    const-string v0, "action_view_focus"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 4996
    const-string v3, "action_view_focus"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5001
    :goto_1
    if-nez v0, :cond_2

    iget-boolean v2, p0, Lcom/android/calendar/event/ay;->aC:Z

    if-nez v2, :cond_2

    .line 5002
    const/4 v0, 0x1

    .line 5005
    :cond_2
    iput-boolean v6, p0, Lcom/android/calendar/event/ay;->aC:Z

    .line 5007
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 5011
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->ah:Landroid/view/View;

    .line 5013
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bt:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5019
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ah:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ah:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 5020
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ah:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/cu;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cu;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_6
    move v0, p2

    .line 4998
    goto :goto_1

    .line 5038
    :pswitch_2
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bF:Z

    if-eqz v0, :cond_7

    .line 5039
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/event/cv;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cv;-><init>(Lcom/android/calendar/event/ay;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 5057
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/event/cw;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cw;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/widget/AutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 5072
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/event/cy;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cy;-><init>(Lcom/android/calendar/event/ay;)V

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5081
    :pswitch_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->Y:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/cz;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cz;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5090
    :pswitch_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/event/da;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/da;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/widget/MultiAutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5104
    :pswitch_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/android/calendar/event/db;

    invoke-direct {v2, p0, v1}, Lcom/android/calendar/event/db;-><init>(Lcom/android/calendar/event/ay;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v2, v4, v5}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5116
    :pswitch_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->af:Landroid/view/View;

    new-instance v2, Lcom/android/calendar/event/dc;

    invoke-direct {v2, p0, v1}, Lcom/android/calendar/event/dc;-><init>(Lcom/android/calendar/event/ay;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v2, v8, v9}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5138
    :pswitch_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ag:Landroid/view/View;

    new-instance v2, Lcom/android/calendar/event/dd;

    invoke-direct {v2, p0, v1}, Lcom/android/calendar/event/dd;-><init>(Lcom/android/calendar/event/ay;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v2, v8, v9}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5163
    :pswitch_9
    iget-object v0, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    new-instance v1, Lcom/android/calendar/event/de;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/de;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5177
    :pswitch_a
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/event/df;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/df;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/widget/AutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 5007
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/text/format/Time;)Z
    .locals 2

    .prologue
    .line 5516
    iget v0, p1, Landroid/text/format/Time;->year:I

    rem-int/lit16 v0, v0, 0x190

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/text/format/Time;->year:I

    rem-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_1

    iget v0, p1, Landroid/text/format/Time;->year:I

    rem-int/lit8 v0, v0, 0x64

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v1, 0x1d

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4675
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bE:Lcom/android/calendar/timezone/v;

    if-nez v2, :cond_0

    .line 4676
    new-instance v2, Lcom/android/calendar/timezone/v;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/android/calendar/timezone/v;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    iput-object v2, p0, Lcom/android/calendar/event/ay;->bE:Lcom/android/calendar/timezone/v;

    .line 4679
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4706
    :cond_1
    :goto_0
    return v0

    .line 4683
    :cond_2
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4684
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 4685
    aget-object p1, v2, v0

    .line 4688
    :cond_3
    iput-object p1, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    .line 4689
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bE:Lcom/android/calendar/timezone/v;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/calendar/timezone/v;->b(Ljava/lang/String;)V

    .line 4691
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bE:Lcom/android/calendar/timezone/v;

    invoke-virtual {v2, p1}, Lcom/android/calendar/timezone/v;->a(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    .line 4695
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bE:Lcom/android/calendar/timezone/v;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bE:Lcom/android/calendar/timezone/v;

    invoke-virtual {v2, p1}, Lcom/android/calendar/timezone/v;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/timezone/v;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/w;

    .line 4697
    iget-object v2, p0, Lcom/android/calendar/event/ay;->P:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/w;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4698
    iget-object v2, p0, Lcom/android/calendar/event/ay;->Q:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/w;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4700
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v3, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 4701
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 4702
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    iput-object v3, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 4703
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 4704
    iget-object v2, p0, Lcom/android/calendar/event/ay;->P:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/w;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4705
    iget-object v2, p0, Lcom/android/calendar/event/ay;->Q:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/w;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 4706
    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 1932
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preference_recent_updated"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 1934
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/android/calendar/event/br;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/br;-><init>(Lcom/android/calendar/event/ay;)V

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1943
    return-void

    .line 1934
    :cond_0
    const-wide/16 v0, 0xc8

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 5220
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5221
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 5256
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 5223
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->l()V

    goto :goto_0

    .line 5226
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->g()V

    goto :goto_0

    .line 5229
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->h()V

    goto :goto_0

    .line 5234
    :pswitch_4
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->S()V

    goto :goto_0

    .line 5237
    :pswitch_5
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->T()V

    goto :goto_0

    .line 5240
    :pswitch_6
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->V()V

    goto :goto_0

    .line 5243
    :pswitch_7
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->g(Z)V

    goto :goto_0

    .line 5246
    :pswitch_8
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->g(Z)V

    goto :goto_0

    .line 5249
    :pswitch_9
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->U()V

    goto :goto_0

    .line 5221
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 4860
    if-eqz p1, :cond_7

    invoke-static {p1}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.android.sharepoint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.osp.app.signin"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4865
    :cond_0
    invoke-static {p1}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4866
    iget-object v0, p0, Lcom/android/calendar/event/ay;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 4869
    :cond_1
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4870
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4871
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/AttendeesView;->setVisibility(I)V

    .line 4880
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->af:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4881
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ag:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4882
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ae:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4884
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4886
    const-string v0, "com.android.sharepoint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4887
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4888
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4889
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4941
    :cond_3
    :goto_1
    return-void

    .line 4873
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4874
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/AttendeesView;->setVisibility(I)V

    .line 4875
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 4876
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 4891
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4892
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4896
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->f(Z)V

    .line 4897
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4898
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 4894
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 4901
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4902
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4906
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->f(Z)V

    .line 4907
    iget-object v0, p0, Lcom/android/calendar/event/ay;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 4908
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4909
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/AttendeesView;->setVisibility(I)V

    .line 4910
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4911
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4913
    if-eqz p1, :cond_a

    const-string v0, "com.google"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4914
    iget-object v0, p0, Lcom/android/calendar/event/ay;->af:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4915
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ag:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4916
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ae:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4918
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4919
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 4920
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getAttendeeItemCount()I

    move-result v0

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->d(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4921
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 4904
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 4923
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 4926
    :cond_a
    if-eqz p1, :cond_3

    const-string v0, "com.android.exchange"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4927
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ae:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4928
    iget-object v0, p0, Lcom/android/calendar/event/ay;->af:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4929
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ag:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4931
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getAttendeeItemCount()I

    move-result v0

    if-lez v0, :cond_b

    .line 4932
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 4936
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 4937
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ab:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 4934
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_4
.end method

.method public b(Ljava/util/ArrayList;)V
    .locals 14

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 7322
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 7323
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 7327
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 7330
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 7331
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 7332
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0f01a9

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7337
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v6, v0, Lcom/android/calendar/as;->G:Z

    .line 7338
    const-string v0, ""

    .line 7339
    const-string v0, ""

    .line 7340
    if-eqz v6, :cond_3

    .line 7341
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v2, v2, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 7342
    iput v9, v1, Landroid/text/format/Time;->hour:I

    .line 7343
    iput v9, v1, Landroid/text/format/Time;->minute:I

    .line 7344
    iput v9, v1, Landroid/text/format/Time;->second:I

    .line 7345
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->E:Ljava/lang/String;

    iput-object v2, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 7346
    invoke-virtual {v1, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 7348
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v12, v1, Lcom/android/calendar/as;->B:J

    invoke-virtual {v4, v12, v13}, Landroid/text/format/Time;->set(J)V

    .line 7349
    iput v9, v4, Landroid/text/format/Time;->hour:I

    .line 7350
    iput v9, v4, Landroid/text/format/Time;->minute:I

    .line 7351
    iput v9, v4, Landroid/text/format/Time;->second:I

    .line 7352
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    iput-object v1, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 7353
    invoke-virtual {v4, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 7355
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    move v8, v7

    move v10, v9

    invoke-static/range {v1 .. v10}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJZZZZZ)Ljava/lang/String;

    move-result-object v1

    .line 7389
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f047a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7390
    if-nez v6, :cond_1

    .line 7391
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7395
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f01ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7399
    if-eqz p1, :cond_7

    .line 7400
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v9

    .line 7401
    :goto_1
    if-ge v9, v2, :cond_7

    .line 7402
    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 7403
    add-int/lit8 v1, v1, 0x1

    .line 7404
    const-string v3, "My calendar"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 7405
    if-ge v1, v2, :cond_6

    .line 7406
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7401
    :cond_2
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 7358
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v2, v0, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 7359
    invoke-virtual {v1, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 7361
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->B:J

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 7362
    invoke-virtual {v4, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 7365
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 7366
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 7371
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/util/TimeZone;)J

    move-result-wide v12

    .line 7374
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    sub-long/2addr v2, v12

    sub-long/2addr v4, v12

    move v8, v9

    move v10, v9

    invoke-static/range {v1 .. v10}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJZZZZZ)Ljava/lang/String;

    move-result-object v1

    .line 7377
    new-instance v0, Lcom/android/calendar/timezone/v;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/android/calendar/timezone/v;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    .line 7379
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/calendar/timezone/v;->a(Ljava/lang/String;)I

    move-result v2

    .line 7381
    if-gez v2, :cond_5

    .line 7382
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    goto/16 :goto_0

    .line 7368
    :cond_4
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_3

    .line 7384
    :cond_5
    invoke-virtual {v0, v2}, Lcom/android/calendar/timezone/v;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/w;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/w;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7385
    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 7408
    :cond_6
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 7414
    :cond_7
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aI:Ljava/lang/String;

    .line 7419
    return-void
.end method

.method protected b(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 4713
    if-eqz p1, :cond_3

    .line 4714
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_1

    .line 4715
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 4716
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 4721
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4722
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 4723
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 4725
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    .line 4727
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4741
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->cX:Z

    if-eqz v0, :cond_2

    .line 4742
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->m()V

    .line 4744
    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/ay;->f(Z)V

    .line 4745
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->s()V

    .line 4746
    return-void

    .line 4729
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_5

    .line 4730
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 4731
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 4732
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 4733
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->hour:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 4736
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 4737
    iget-object v2, p0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    .line 4739
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 5291
    if-nez p1, :cond_0

    .line 5292
    iget v0, p0, Lcom/android/calendar/event/ay;->cQ:I

    iput v0, p0, Lcom/android/calendar/event/ay;->cP:I

    .line 5297
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->f()V

    .line 5298
    return-void

    .line 5294
    :cond_0
    iput p1, p0, Lcom/android/calendar/event/ay;->cP:I

    goto :goto_0
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 6962
    sput-boolean p1, Lcom/android/calendar/event/ay;->ar:Z

    .line 6963
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 2791
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2792
    :cond_0
    const/4 v0, 0x0

    .line 2794
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->J()Z

    move-result v0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 3813
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-nez v0, :cond_0

    .line 3821
    :goto_0
    return-void

    .line 3816
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3817
    iget v0, p0, Lcom/android/calendar/event/ay;->bW:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->h(I)V

    goto :goto_0

    .line 3819
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->h(I)V

    goto :goto_0
.end method

.method public d(I)V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 5305
    iget-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 5345
    :goto_0
    return-void

    .line 5309
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_5

    .line 5310
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->aA:Lcom/android/calendar/colorpicker/e;

    if-nez v0, :cond_1

    .line 5311
    iget-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 5314
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->aA:Lcom/android/calendar/colorpicker/e;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/colorpicker/e;->a(Ljava/lang/String;Ljava/lang/String;)[I

    move-result-object v2

    .line 5317
    if-eqz v2, :cond_2

    array-length v0, v2

    if-ge v0, v5, :cond_3

    .line 5318
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 5321
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    move v0, v1

    .line 5324
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_4

    .line 5325
    aget v3, v2, v0

    if-ne v3, p1, :cond_6

    .line 5332
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 5333
    iget-object v3, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/ay;->cU:[I

    aget v0, v4, v0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5334
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5335
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f016e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5336
    iget-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 5341
    :cond_5
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 5342
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 5343
    new-array v2, v5, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v2, v1

    .line 5344
    iget-object v0, p0, Lcom/android/calendar/event/ay;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/calendar/colorpicker/k;

    invoke-direct {v1, v2, p1}, Lcom/android/calendar/colorpicker/k;-><init>([Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 5328
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 7603
    iput-boolean p1, p0, Lcom/android/calendar/event/ay;->cM:Z

    .line 7604
    return-void
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 7480
    iput p1, p0, Lcom/android/calendar/event/ay;->cn:I

    .line 7481
    return-void
.end method

.method public e(Z)V
    .locals 3

    .prologue
    .line 7706
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 7746
    :cond_0
    :goto_0
    return-void

    .line 7710
    :cond_1
    if-eqz p1, :cond_3

    .line 7712
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    sget v1, Lcom/android/calendar/event/ay;->ci:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 7719
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 7721
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    const/16 v2, 0xc

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    .line 7724
    const-string v1, "My calendar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7725
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 7728
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 7729
    invoke-static {v1}, Lcom/android/calendar/hj;->b(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cQ:I

    .line 7731
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    const/16 v2, 0xb

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    .line 7734
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/calendar/event/ay;->b(Ljava/lang/String;)V

    .line 7735
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ac()V

    .line 7737
    iget-object v1, p0, Lcom/android/calendar/event/ay;->A:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7738
    iget-object v0, p0, Lcom/android/calendar/event/ay;->z:Landroid/view/View;

    iget v1, p0, Lcom/android/calendar/event/ay;->cQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 7739
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->b(Ljava/lang/String;)V

    .line 7741
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->f()V

    .line 7743
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7744
    const v0, 0x7f1200fe

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ay;->k(I)Landroid/widget/MultiAutoCompleteTextView;

    goto :goto_0

    .line 7715
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bA:Landroid/database/Cursor;

    sget v1, Lcom/android/calendar/event/ay;->cj:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 7716
    sget v0, Lcom/android/calendar/event/ay;->cj:I

    sput v0, Lcom/android/calendar/event/ay;->ci:I

    goto :goto_1
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 4223
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bI:Z

    return v0
.end method

.method public e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 6958
    const-string v0, "My calendar"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 5301
    iget v0, p0, Lcom/android/calendar/event/ay;->cP:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->d(I)V

    .line 5302
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 7468
    if-eqz p1, :cond_0

    const-string v0, "com.android.exchange"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7469
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    .line 7473
    :goto_0
    return-void

    .line 7471
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    goto :goto_0
.end method

.method public f(Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 7808
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "preferences_home_tz_enabled"

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 7810
    if-eqz p1, :cond_0

    .line 7811
    iget-object v0, p0, Lcom/android/calendar/event/ay;->q:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 7825
    :goto_0
    return-void

    .line 7813
    :cond_0
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v1

    .line 7814
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 7817
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->D()V

    .line 7818
    iget-object v0, p0, Lcom/android/calendar/event/ay;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 7819
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 7821
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->q:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 7822
    iget-object v0, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 5348
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5349
    const v1, 0x7f0f01a8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 5350
    iget-object v1, p0, Lcom/android/calendar/event/ay;->aD:Lcom/android/calendar/event/fc;

    sget v2, Lcom/android/calendar/event/ay;->ci:I

    new-instance v3, Lcom/android/calendar/event/dh;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/dh;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5453
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/dj;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/dj;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5462
    new-instance v1, Lcom/android/calendar/event/dk;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/dk;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 5469
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5470
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 5472
    :cond_0
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 7476
    iput-object p1, p0, Lcom/android/calendar/event/ay;->bB:Ljava/lang/String;

    .line 7477
    return-void
.end method

.method public h()V
    .locals 4

    .prologue
    .line 5475
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5476
    const v1, 0x7f0f01ad

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 5477
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cl:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/ay;->cm:I

    new-instance v3, Lcom/android/calendar/event/dl;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/dl;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5494
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/dm;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/dm;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5503
    new-instance v1, Lcom/android/calendar/event/dn;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/dn;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 5510
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5511
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 5513
    :cond_0
    return-void
.end method

.method public h(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 7697
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bB:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bB:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bB:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7699
    :cond_0
    const/4 v0, 0x1

    .line 7701
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()V
    .locals 5

    .prologue
    .line 6156
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->J()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 6158
    :cond_0
    new-instance v0, Lcom/android/a/c;

    invoke-direct {v0}, Lcom/android/a/c;-><init>()V

    .line 6159
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 6160
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 6166
    :cond_1
    :goto_0
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 6167
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-boolean v2, v2, Lcom/android/calendar/as;->G:Z

    if-eqz v2, :cond_2

    .line 6168
    const-string v2, "UTC"

    iput-object v2, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 6170
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 6171
    invoke-virtual {v0, v1}, Lcom/android/a/c;->a(Landroid/text/format/Time;)V

    .line 6172
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/calendar/event/ay;->bV:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/android/calendar/dp;->a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/android/a/c;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6174
    iget-object v1, p0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6176
    :cond_3
    return-void

    .line 6161
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 6162
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bX:Lcom/android/a/c;

    iget v1, v1, Lcom/android/a/c;->b:I

    iput v1, v0, Lcom/android/a/c;->b:I

    .line 6163
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget v1, v1, Lcom/android/calendar/as;->S:I

    iput v1, v0, Lcom/android/a/c;->e:I

    .line 6164
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->O:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method j()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6196
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->yearDay:I

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->yearDay:I

    sub-int/2addr v0, v2

    const/4 v2, 0x7

    if-lt v0, v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->yearDay:I

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->yearDay:I

    add-int/2addr v0, v2

    const/4 v2, 0x6

    if-ge v0, v2, :cond_5

    :cond_1
    move v0, v1

    .line 6200
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/event/ay;->O:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 6201
    iget-object v2, p0, Lcom/android/calendar/event/ay;->O:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_4

    .line 6203
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    iget-object v3, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->weekDay:I

    if-gt v2, v3, :cond_3

    .line 6204
    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    if-gt v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    if-ge v0, v2, :cond_4

    .line 6216
    :cond_2
    :goto_1
    return v1

    .line 6208
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/event/ay;->ca:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    if-le v0, v2, :cond_4

    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    if-lt v0, v2, :cond_2

    .line 6200
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6216
    :cond_5
    const/4 v1, 0x1

    goto :goto_1
.end method

.method k()V
    .locals 3

    .prologue
    .line 6221
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 6223
    iget-object v0, p0, Lcom/android/calendar/event/ay;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 6224
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getImeOptions()I

    move-result v0

    and-int/lit8 v0, v0, -0x7

    or-int/lit8 v0, v0, 0x5

    move v1, v0

    .line 6230
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 6232
    iget-object v2, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v1}, Landroid/widget/AutoCompleteTextView;->setImeOptions(I)V

    .line 6233
    iget-object v1, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 6235
    :cond_0
    return-void

    .line 6227
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getImeOptions()I

    move-result v0

    and-int/lit8 v0, v0, -0x6

    or-int/lit8 v0, v0, 0x6

    move v1, v0

    goto :goto_0
.end method

.method public l()V
    .locals 4

    .prologue
    .line 6368
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6369
    const v1, 0x7f0f01ac

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 6370
    iget-object v1, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 6371
    iget-object v2, p0, Lcom/android/calendar/event/ay;->cp:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 6372
    iget v2, p0, Lcom/android/calendar/event/ay;->cr:I

    new-instance v3, Lcom/android/calendar/event/ed;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/ed;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6403
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/ee;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/ee;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6412
    new-instance v1, Lcom/android/calendar/event/eg;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/eg;-><init>(Lcom/android/calendar/event/ay;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 6419
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6420
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 6422
    :cond_0
    return-void
.end method

.method public m()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x0

    .line 6588
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 6589
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 6591
    iget-boolean v4, p0, Lcom/android/calendar/event/ay;->cC:Z

    if-eqz v4, :cond_1

    .line 6592
    cmp-long v4, v2, v6

    if-nez v4, :cond_0

    .line 6593
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v2

    .line 6595
    :cond_0
    cmp-long v4, v0, v6

    if-nez v4, :cond_1

    .line 6596
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 6599
    :cond_1
    iget-object v4, p0, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-direct {p0, v4, v2, v3}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    .line 6600
    iget-object v2, p0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;J)V

    .line 6601
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6925
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    if-eqz v0, :cond_0

    .line 6926
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->dismiss()V

    .line 6927
    iput-object v1, p0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    .line 6930
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ak:Lcom/android/calendar/common/extension/k;

    if-eqz v0, :cond_1

    .line 6931
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ak:Lcom/android/calendar/common/extension/k;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/k;->dismiss()V

    .line 6932
    iput-object v1, p0, Lcom/android/calendar/event/ay;->ak:Lcom/android/calendar/common/extension/k;

    .line 6935
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aP:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ay;->aP:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6936
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aP:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 6937
    iput-object v1, p0, Lcom/android/calendar/event/ay;->aP:Landroid/app/Dialog;

    .line 6940
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6941
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 6942
    iput-object v1, p0, Lcom/android/calendar/event/ay;->aQ:Landroid/app/AlertDialog;

    .line 6945
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6946
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 6947
    iput-object v1, p0, Lcom/android/calendar/event/ay;->aO:Landroid/app/AlertDialog;

    .line 6950
    :cond_4
    iput-object v1, p0, Lcom/android/calendar/event/ay;->bE:Lcom/android/calendar/timezone/v;

    .line 6951
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    if-eqz v0, :cond_5

    .line 6952
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/c;->dismiss()V

    .line 6953
    iput-object v1, p0, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    .line 6955
    :cond_5
    return-void
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 6966
    sget-boolean v0, Lcom/android/calendar/event/ay;->ar:Z

    return v0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const v4, 0x7f0f024b

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 6839
    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    .line 6892
    :cond_0
    :goto_0
    return v2

    .line 6843
    :cond_1
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 6844
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gtz v0, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->bH:Z

    .line 6845
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v3, 0x42

    if-ne v0, v3, :cond_0

    .line 6846
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bH:Z

    if-nez v0, :cond_3

    .line 6847
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->performValidation()V

    .line 6849
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 6850
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 6851
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/event/ay;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 6853
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 6887
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 6888
    iput-boolean v1, p0, Lcom/android/calendar/event/ay;->bH:Z

    :cond_3
    move v2, v1

    .line 6890
    goto :goto_0

    :cond_4
    move v0, v2

    .line 6844
    goto :goto_1

    .line 6856
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v3}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 6858
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 6859
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 6860
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    if-eqz v0, :cond_7

    .line 6861
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6866
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_2

    .line 6863
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3

    .line 6869
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 6870
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-static {v0, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 6873
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v3, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v3}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 6875
    iget-object v0, p0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_a

    .line 6876
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 6877
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    if-eqz v0, :cond_b

    .line 6878
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 6883
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_2

    .line 6880
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bJ:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_4
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 4750
    iget-object v0, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ay;->ba:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4752
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bg:Lcom/android/calendar/gl;

    invoke-virtual {v0, p3}, Lcom/android/calendar/gl;->a(I)V

    .line 4753
    packed-switch p3, :pswitch_data_0

    .line 4773
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bc:Landroid/widget/EditText;

    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 4774
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bb:Landroid/widget/Button;

    if-nez p3, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 4826
    :cond_0
    :goto_3
    return-void

    .line 4756
    :pswitch_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/event/ay;->cf:I

    .line 4757
    iget-object v0, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    const v1, 0x2000006

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 4758
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 4760
    iget-object v1, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    goto :goto_0

    .line 4764
    :pswitch_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/event/ay;->cf:I

    .line 4765
    iget-object v0, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    const v1, 0x2000005

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 4766
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 4768
    iget-object v1, p0, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    goto :goto_0

    .line 4773
    :cond_1
    const/16 v0, 0x8

    goto :goto_1

    .line 4774
    :cond_2
    const/16 v0, 0x8

    goto :goto_2

    .line 4775
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->u:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4778
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 4779
    if-nez v0, :cond_4

    .line 4780
    const-string v0, "EditEvent"

    const-string v1, "Cursor not set on calendar item"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 4784
    :cond_4
    const-string v1, "calendar_color"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 4785
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 4786
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    .line 4787
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 4788
    if-nez v1, :cond_5

    .line 4789
    const v1, 0x7f0b0073

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 4793
    :cond_5
    const-string v2, "_id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 4794
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 4795
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v4, v4, Lcom/android/calendar/as;->c:J

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 4798
    iget-object v4, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-wide v2, v4, Lcom/android/calendar/as;->c:J

    .line 4799
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    invoke-static {v1}, Lcom/android/calendar/hj;->b(I)I

    move-result v1

    iput v1, v2, Lcom/android/calendar/as;->e:I

    .line 4800
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 4801
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/hj;->b(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ay;->cQ:I

    .line 4804
    const/16 v1, 0xc

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    .line 4805
    const/16 v1, 0xb

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ay;->cR:Ljava/lang/String;

    .line 4807
    iget v0, p0, Lcom/android/calendar/event/ay;->cQ:I

    iput v0, p0, Lcom/android/calendar/event/ay;->cP:I

    .line 4808
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->f()V

    .line 4810
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    const-string v1, "com.android.exchange"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4811
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    .line 4815
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ay;->b(Ljava/lang/String;)V

    .line 4818
    iget-object v0, p0, Lcom/android/calendar/event/ay;->cS:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4819
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ae()V

    .line 4824
    :cond_6
    invoke-direct {p0}, Lcom/android/calendar/event/ay;->ac()V

    goto/16 :goto_3

    .line 4813
    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/ay;->bs:Z

    goto :goto_4

    .line 4753
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 6834
    const/4 v0, 0x0

    return v0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 4831
    return-void
.end method

.method public p()V
    .locals 3

    .prologue
    .line 7025
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    const v1, 0x7f0f0202

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 7026
    return-void
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7422
    iget-object v0, p0, Lcom/android/calendar/event/ay;->aI:Ljava/lang/String;

    return-object v0
.end method

.method public r()Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 7426
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 7428
    invoke-virtual {p0}, Lcom/android/calendar/event/ay;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7429
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 7430
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    .line 7436
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 7437
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    .line 7438
    invoke-static {v0}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7442
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7446
    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/event/ay;->b(Ljava/util/ArrayList;)V

    .line 7450
    :cond_2
    return-object v1
.end method

.method public s()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 7490
    iget-object v0, p0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 7495
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-nez v1, :cond_0

    .line 7520
    :goto_0
    return-void

    .line 7499
    :cond_0
    if-eqz v0, :cond_1

    .line 7500
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 7501
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 7502
    iput v4, v0, Landroid/text/format/Time;->hour:I

    .line 7503
    iput v4, v0, Landroid/text/format/Time;->minute:I

    .line 7504
    iput v4, v0, Landroid/text/format/Time;->second:I

    .line 7505
    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 7506
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 7508
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 7509
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 7510
    iput v4, v0, Landroid/text/format/Time;->hour:I

    .line 7511
    iput v4, v0, Landroid/text/format/Time;->minute:I

    .line 7512
    iput v4, v0, Landroid/text/format/Time;->second:I

    .line 7513
    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 7514
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    const-wide/32 v4, 0x5265c00

    add-long/2addr v0, v4

    .line 7519
    :goto_1
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/calendar/event/ay;->a(JJ)V

    goto :goto_0

    .line 7516
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bT:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 7517
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bU:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    goto :goto_1
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 7574
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->bw:Z

    return v0
.end method

.method public u()V
    .locals 5

    .prologue
    const v4, 0x7f0c016a

    .line 7578
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/ay;->a:Landroid/widget/ScrollView;

    if-eqz v0, :cond_4

    .line 7579
    iget-object v0, p0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->requestFocus()Z

    .line 7580
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bu:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 7581
    iget v0, p0, Lcom/android/calendar/event/ay;->cK:I

    .line 7582
    iget-object v2, p0, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 7583
    int-to-float v0, v0

    const v2, 0x7f0c0170

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 7585
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 7586
    int-to-float v0, v0

    const v2, 0x7f0c0216

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 7588
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/ay;->bY:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 7589
    iget-object v2, p0, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 7591
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 7593
    iget-object v2, p0, Lcom/android/calendar/event/ay;->J:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 7594
    int-to-float v0, v0

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 7597
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/event/ay;->a:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 7600
    :cond_4
    return-void
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 7607
    iget-boolean v0, p0, Lcom/android/calendar/event/ay;->cM:Z

    return v0
.end method

.method public w()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 7671
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->ar:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->as:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 7672
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 7673
    iget-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 7674
    iget-object v0, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 7694
    :cond_1
    :goto_0
    return-void

    .line 7677
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->as:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 7678
    iget-object v1, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 7679
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7681
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 7682
    iget-object v1, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->as:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 7683
    iget-object v1, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 7684
    iget-object v0, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 7685
    iget-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 7687
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    iput-wide v6, v0, Lcom/android/calendar/as;->ar:J

    .line 7688
    iget-object v0, p0, Lcom/android/calendar/event/ay;->bz:Lcom/android/calendar/as;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/as;->as:Ljava/lang/String;

    .line 7689
    iget-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 7690
    iget-object v0, p0, Lcom/android/calendar/event/ay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 7691
    iget-object v0, p0, Lcom/android/calendar/event/ay;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
