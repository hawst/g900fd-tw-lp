.class Lcom/android/calendar/event/lg;
.super Ljava/lang/Object;
.source "SimpleAddDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/text/format/Time;

.field final synthetic b:Lcom/android/calendar/event/ld;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ld;Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    iput-object p2, p0, Lcom/android/calendar/event/lg;->a:Landroid/text/format/Time;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 372
    invoke-static {}, Lcom/android/calendar/event/ld;->c()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 373
    const-wide/16 v0, 0x1

    .line 374
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.EDIT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 375
    invoke-static {}, Lcom/android/calendar/event/ld;->c()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 376
    const-string v3, "launch_from_inside"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 377
    iget-object v3, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v3}, Lcom/android/calendar/event/ld;->b(Lcom/android/calendar/event/ld;)Landroid/widget/EditText;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 378
    const-string v3, "title"

    iget-object v4, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v4}, Lcom/android/calendar/event/ld;->b(Lcom/android/calendar/event/ld;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 382
    :goto_0
    const-string v3, "beginTime"

    iget-object v4, p0, Lcom/android/calendar/event/lg;->a:Landroid/text/format/Time;

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 383
    const-string v3, "copyEvent"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 384
    iget-object v3, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v3}, Lcom/android/calendar/event/ld;->a(Lcom/android/calendar/event/ld;)I

    move-result v3

    if-ne v3, v6, :cond_3

    .line 385
    const-string v3, "allDay"

    invoke-static {}, Lcom/android/calendar/event/ld;->d()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 386
    const-string v3, "edit_mode"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 387
    const-string v0, "calendar_id"

    iget-object v1, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v1}, Lcom/android/calendar/event/ld;->c(Lcom/android/calendar/event/ld;)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 388
    const-string v0, "simple_add_flag"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 389
    const-string v0, "simple_add_account_position"

    iget-object v1, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v1}, Lcom/android/calendar/event/ld;->c(Lcom/android/calendar/event/ld;)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 403
    :cond_0
    :goto_1
    invoke-static {}, Lcom/android/calendar/event/ld;->c()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 404
    iget-object v0, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v0}, Lcom/android/calendar/event/ld;->g(Lcom/android/calendar/event/ld;)V

    .line 406
    :cond_1
    return-void

    .line 380
    :cond_2
    const-string v3, "title"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 391
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v0}, Lcom/android/calendar/event/ld;->a(Lcom/android/calendar/event/ld;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 392
    const-string v0, "task"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 393
    const-string v0, "_id"

    iget-object v1, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v1}, Lcom/android/calendar/event/ld;->d(Lcom/android/calendar/event/ld;)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 395
    iget-object v0, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v0}, Lcom/android/calendar/event/ld;->e(Lcom/android/calendar/event/ld;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v0}, Lcom/android/calendar/event/ld;->e(Lcom/android/calendar/event/ld;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v1}, Lcom/android/calendar/event/ld;->f(Lcom/android/calendar/event/ld;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    invoke-static {}, Lcom/android/calendar/event/ld;->c()Landroid/app/Activity;

    move-result-object v1

    const-string v3, "preference_defaultTaskId"

    iget-object v0, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    iget-object v0, v0, Lcom/android/calendar/event/ld;->f:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/event/lg;->b:Lcom/android/calendar/event/ld;

    invoke-static {v4}, Lcom/android/calendar/event/ld;->d(Lcom/android/calendar/event/ld;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget v0, v0, Lcom/android/calendar/task/ab;->c:I

    invoke-static {v1, v3, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1
.end method
