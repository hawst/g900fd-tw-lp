.class public Lcom/android/calendar/event/ig;
.super Landroid/app/Dialog;
.source "ExchangeActiveSyncView.java"


# static fields
.field private static w:I

.field private static x:I

.field private static final z:Landroid/net/Uri;


# instance fields
.field private A:Ljava/lang/Thread;

.field private a:Lcom/android/calendar/event/ay;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/ArrayList;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:J

.field private k:Landroid/text/format/Time;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/ScrollView;

.field private s:Lcom/android/calendar/event/NewHorizontalScrollView;

.field private t:Lcom/android/calendar/event/NewHorizontalScrollView;

.field private u:Landroid/widget/LinearLayout;

.field private v:Landroid/widget/LinearLayout;

.field private y:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/event/ig;->w:I

    .line 84
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/event/ig;->x:I

    .line 86
    const-string v0, "content://com.android.exchange.directory.provider/resolverecipients"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/ig;->z:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/event/ay;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Landroid/text/format/Time;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 94
    invoke-direct {p0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 58
    const-string v0, "444444444444444444444444444444444444444444444444"

    iput-object v0, p0, Lcom/android/calendar/event/ig;->h:Ljava/lang/String;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/ig;->i:Z

    .line 62
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/event/ig;->j:J

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ig;->y:Ljava/util/ArrayList;

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/ig;->A:Ljava/lang/Thread;

    .line 95
    invoke-virtual {p0, v2}, Lcom/android/calendar/event/ig;->setCanceledOnTouchOutside(Z)V

    .line 96
    invoke-virtual {p0, v2}, Lcom/android/calendar/event/ig;->requestWindowFeature(I)Z

    .line 97
    invoke-virtual {p0}, Lcom/android/calendar/event/ig;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f0b0027

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 98
    const v0, 0x7f040056

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->setContentView(I)V

    .line 100
    iput-object p1, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    .line 101
    iput-object p2, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    .line 103
    iput-object p3, p0, Lcom/android/calendar/event/ig;->c:Ljava/lang/String;

    .line 104
    iput-object p4, p0, Lcom/android/calendar/event/ig;->d:Ljava/lang/String;

    .line 105
    iput-object p5, p0, Lcom/android/calendar/event/ig;->f:Ljava/util/ArrayList;

    .line 106
    iput-object p7, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    .line 107
    iput-object p6, p0, Lcom/android/calendar/event/ig;->g:Ljava/lang/String;

    .line 109
    const v0, 0x7f1201cc

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ig;->l:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f1201cd

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ig;->m:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f1201ac

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ig;->n:Landroid/view/View;

    .line 113
    const v0, 0x7f1201ad

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ig;->o:Landroid/view/View;

    .line 115
    const v0, 0x7f1201d2

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ig;->p:Landroid/view/View;

    .line 116
    const v0, 0x7f1201d5

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ig;->q:Landroid/view/View;

    .line 118
    const v0, 0x7f1201cf

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/calendar/event/ig;->r:Landroid/widget/ScrollView;

    .line 120
    const v0, 0x7f1201ce

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/NewHorizontalScrollView;

    iput-object v0, p0, Lcom/android/calendar/event/ig;->s:Lcom/android/calendar/event/NewHorizontalScrollView;

    .line 121
    iget-object v0, p0, Lcom/android/calendar/event/ig;->s:Lcom/android/calendar/event/NewHorizontalScrollView;

    new-instance v1, Lcom/android/calendar/event/ih;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ih;-><init>(Lcom/android/calendar/event/ig;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/NewHorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 129
    const v0, 0x7f1201d3

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/NewHorizontalScrollView;

    iput-object v0, p0, Lcom/android/calendar/event/ig;->t:Lcom/android/calendar/event/NewHorizontalScrollView;

    .line 130
    iget-object v0, p0, Lcom/android/calendar/event/ig;->t:Lcom/android/calendar/event/NewHorizontalScrollView;

    iget-object v1, p0, Lcom/android/calendar/event/ig;->s:Lcom/android/calendar/event/NewHorizontalScrollView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/NewHorizontalScrollView;->setFollowView(Lcom/android/calendar/event/NewHorizontalScrollView;)V

    .line 132
    const v0, 0x7f1201d1

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ig;->u:Landroid/widget/LinearLayout;

    .line 133
    const v0, 0x7f1201d4

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ig;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/ig;->v:Landroid/widget/LinearLayout;

    .line 135
    iget-object v0, p0, Lcom/android/calendar/event/ig;->n:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/ii;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ii;-><init>(Lcom/android/calendar/event/ig;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v0, p0, Lcom/android/calendar/event/ig;->o:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/ij;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ij;-><init>(Lcom/android/calendar/event/ig;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->d()V

    .line 150
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->e()V

    .line 151
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->f()V

    .line 152
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/android/calendar/event/ig;->w:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/ig;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/android/calendar/event/ig;->y:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/ig;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ig;->a([Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14

    .prologue
    .line 510
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 513
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v7, v0, [Ljava/lang/String;

    .line 514
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/calendar/event/ig;->c:Ljava/lang/String;

    aput-object v1, v7, v0

    .line 515
    const/4 v0, 0x0

    const/4 v1, 0x1

    array-length v2, p1

    invoke-static {p1, v0, v7, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 518
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v8, v0, [Ljava/lang/String;

    .line 519
    const/4 v0, 0x0

    :goto_0
    array-length v1, v8

    if-ge v0, v1, :cond_0

    .line 520
    iget-object v1, p0, Lcom/android/calendar/event/ig;->h:Ljava/lang/String;

    aput-object v1, v8, v0

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 524
    :cond_0
    iget-wide v0, p0, Lcom/android/calendar/event/ig;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 525
    const-string v0, "content://com.android.email.provider/account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 526
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "emailAddress = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v9, p0, Lcom/android/calendar/event/ig;->c:Ljava/lang/String;

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 532
    if-eqz v1, :cond_1

    .line 533
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/ig;->j:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    :cond_1
    if-eqz v1, :cond_2

    .line 538
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 544
    :cond_2
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "to"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "displayName"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "email"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "mergedFreeBusy"

    aput-object v1, v2, v0

    .line 549
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v0, "accountId=? AND startTime=\'?\' AND endTime=\'?\' AND "

    invoke-direct {v3, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 551
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 553
    array-length v4, p1

    .line 554
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_5

    .line 555
    aget-object v5, p1, v0

    .line 556
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 557
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 558
    const-string v9, "<"

    const-string v10, ""

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const-string v9, ">"

    const-string v10, ""

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const-string v9, "?"

    const-string v10, ""

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 559
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 560
    aput-object v5, p1, v0

    .line 561
    const-string v5, " OR to=\'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    aget-object v9, p1, v0

    invoke-virtual {v5, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v9, "\'"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 554
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 537
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 538
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 567
    :cond_5
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_b

    .line 568
    const/4 v0, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "(to=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/event/ig;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 569
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 573
    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 576
    iget-object v0, p0, Lcom/android/calendar/event/ig;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    .line 577
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 578
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    sub-int v0, v1, v0

    int-to-long v0, v0

    .line 581
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 582
    iget-object v9, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v5, v9}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 583
    const/4 v9, 0x0

    iput v9, v5, Landroid/text/format/Time;->hour:I

    .line 584
    const/4 v9, 0x0

    iput v9, v5, Landroid/text/format/Time;->minute:I

    .line 585
    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Landroid/text/format/Time;->normalize(Z)J

    .line 589
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    .line 590
    iget-object v10, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v9, v10}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 591
    const/16 v10, 0x17

    iput v10, v9, Landroid/text/format/Time;->hour:I

    .line 592
    const/16 v10, 0x3b

    iput v10, v9, Landroid/text/format/Time;->minute:I

    .line 593
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/text/format/Time;->normalize(Z)J

    .line 595
    invoke-virtual {v4}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 596
    new-instance v10, Ljava/sql/Date;

    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    invoke-direct {v10, v12, v13}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v4, v10}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v10

    if-nez v10, :cond_c

    new-instance v10, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-direct {v10, v12, v13}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v4, v10}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 598
    invoke-virtual {v4}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v4

    int-to-long v10, v4

    sub-long/2addr v0, v10

    .line 604
    :cond_6
    :goto_3
    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    sub-long/2addr v10, v0

    invoke-virtual {v5, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 605
    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 607
    const-string v4, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v5, v4}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 609
    const/4 v4, 0x1

    invoke-virtual {v9, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    sub-long v0, v10, v0

    invoke-virtual {v9, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 610
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 612
    const-string v0, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v9, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 614
    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-wide v10, p0, Lcom/android/calendar/event/ig;->j:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v1

    const/4 v1, 0x1

    aput-object v5, v4, v1

    const/4 v1, 0x2

    aput-object v0, v4, v1

    .line 618
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/ig;->z:Landroid/net/Uri;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 623
    if-eqz v2, :cond_10

    .line 624
    :try_start_1
    const-string v0, "to"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 625
    const-string v0, "displayName"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 626
    const-string v0, "mergedFreeBusy"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 632
    :cond_7
    :goto_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 633
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 634
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 635
    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 637
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_12

    .line 638
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ig;->h:Ljava/lang/String;

    move-object v1, v0

    .line 642
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/event/ig;->c:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 643
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 644
    const/4 v0, 0x0

    aput-object v10, v7, v0

    .line 646
    :cond_9
    const/4 v0, 0x0

    aput-object v1, v8, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_4

    .line 660
    :catchall_1
    move-exception v0

    if-eqz v2, :cond_a

    .line 661
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v0

    .line 571
    :cond_b
    const-string v0, "to=\'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v4, p0, Lcom/android/calendar/event/ig;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 599
    :cond_c
    new-instance v10, Ljava/sql/Date;

    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    invoke-direct {v10, v12, v13}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v4, v10}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v10

    if-eqz v10, :cond_6

    new-instance v10, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-direct {v10, v12, v13}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v4, v10}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 601
    invoke-virtual {v4}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v4

    int-to-long v10, v4

    add-long/2addr v0, v10

    goto/16 :goto_3

    .line 648
    :cond_d
    const/4 v0, 0x0

    :goto_6
    :try_start_2
    array-length v11, p1

    if-ge v0, v11, :cond_7

    .line 649
    aget-object v11, p1, v0

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 650
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_e

    .line 651
    add-int/lit8 v11, v0, 0x1

    aput-object v10, v7, v11

    .line 653
    :cond_e
    add-int/lit8 v11, v0, 0x1

    aput-object v1, v8, v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 648
    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 660
    :cond_10
    if-eqz v2, :cond_11

    .line 661
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 664
    :cond_11
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    return-object v6

    :cond_12
    move-object v1, v0

    goto :goto_5
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 156
    iput-boolean v2, p0, Lcom/android/calendar/event/ig;->i:Z

    .line 157
    sget v0, Lcom/android/calendar/event/ig;->w:I

    if-ne p1, v0, :cond_2

    .line 158
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 159
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 160
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    const/16 v1, 0x76e

    if-ge v0, v1, :cond_1

    .line 161
    iget-object v0, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->av:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    .line 162
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 163
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 180
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/ig;->i:Z

    .line 181
    return-void

    .line 165
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->e()V

    .line 166
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->f()V

    goto :goto_0

    .line 168
    :cond_2
    sget v0, Lcom/android/calendar/event/ig;->x:I

    if-ne p1, v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 170
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 171
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    const/16 v1, 0x7f4

    if-le v0, v1, :cond_3

    .line 172
    iget-object v0, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->av:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    .line 173
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 174
    iget-object v0, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    goto :goto_0

    .line 176
    :cond_3
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->e()V

    .line 177
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->f()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/ig;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->c()V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/ig;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ig;->a(I)V

    return-void
.end method

.method private a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 10

    .prologue
    .line 674
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 675
    const-string p1, "-"

    .line 678
    :cond_0
    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v6, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/calendar/event/ig;->h:Ljava/lang/String;

    aput-object v1, v6, v0

    .line 683
    iget-wide v0, p0, Lcom/android/calendar/event/ig;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 684
    const-string v0, "content://com.android.email.provider/account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 685
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "emailAddress = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/android/calendar/event/ig;->c:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 692
    if-eqz v1, :cond_1

    .line 693
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 694
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/ig;->j:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 697
    :cond_1
    if-eqz v1, :cond_2

    .line 698
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 704
    :cond_2
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "to"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "displayName"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "email"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "mergedFreeBusy"

    aput-object v1, v2, v0

    .line 709
    const-string v3, "accountId=? AND startTime=\'?\' AND endTime=\'?\' AND to=\'?\'"

    .line 711
    iget-object v0, p0, Lcom/android/calendar/event/ig;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 712
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 713
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-long v0, v0

    .line 716
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 717
    iget-object v5, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 718
    const/4 v5, 0x0

    iput v5, v4, Landroid/text/format/Time;->hour:I

    .line 719
    const/4 v5, 0x0

    iput v5, v4, Landroid/text/format/Time;->minute:I

    .line 720
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 721
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-virtual {v4, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 722
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 724
    const-string v5, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 726
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 727
    iget-object v7, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 728
    const/16 v7, 0x17

    iput v7, v4, Landroid/text/format/Time;->hour:I

    .line 729
    const/16 v7, 0x3b

    iput v7, v4, Landroid/text/format/Time;->minute:I

    .line 730
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->normalize(Z)J

    .line 731
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    sub-long v0, v8, v0

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 732
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 734
    const-string v0, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 736
    const/4 v1, 0x4

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-wide v8, p0, Lcom/android/calendar/event/ig;->j:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v1

    const/4 v1, 0x1

    aput-object v5, v4, v1

    const/4 v1, 0x2

    aput-object v0, v4, v1

    const/4 v0, 0x3

    aput-object p1, v4, v0

    .line 740
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/ig;->z:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 745
    if-eqz v1, :cond_9

    .line 746
    :try_start_1
    const-string v0, "to"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 747
    const-string v0, "displayName"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 748
    const-string v0, "mergedFreeBusy"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 754
    :cond_3
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 756
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 757
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 758
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 760
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_5

    .line 761
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ig;->h:Ljava/lang/String;

    .line 764
    :cond_5
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 765
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 766
    const/4 v5, 0x0

    aput-object v7, v6, v5

    .line 768
    :cond_6
    const/4 v5, 0x1

    aput-object v0, v6, v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 773
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_7

    .line 774
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 697
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_8

    .line 698
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 773
    :cond_9
    if-eqz v1, :cond_a

    .line 774
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 777
    :cond_a
    return-object v6
.end method

.method static synthetic b()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/android/calendar/event/ig;->x:I

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/event/ig;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/calendar/event/ig;->g()V

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/ig;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 186
    iget-object v0, p0, Lcom/android/calendar/event/ig;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 187
    return-void
.end method

.method private final d()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/calendar/event/ig;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/ig;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    return-void
.end method

.method private final e()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 196
    const v6, 0x18016

    .line 200
    sget-object v0, Lcom/android/calendar/event/ay;->az:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 201
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    sget-object v1, Lcom/android/calendar/event/ay;->aA:Ljava/util/Formatter;

    iget-object v2, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v2, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/event/ig;->k:Landroid/text/format/Time;

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ig;->e:Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/android/calendar/event/ig;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/ig;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    return-void
.end method

.method private final f()V
    .locals 12

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 219
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 221
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 224
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0447

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 227
    iget-object v0, p0, Lcom/android/calendar/event/ig;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ig;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/android/calendar/event/ig;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 229
    new-array v4, v3, [Ljava/lang/String;

    .line 230
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 231
    iget-object v0, p0, Lcom/android/calendar/event/ig;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v1

    .line 230
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 234
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 235
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 236
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 237
    iget-object v3, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0f0287

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 238
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 239
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v5, -0x2

    invoke-virtual {v3, v2, v5}, Landroid/view/Window;->setLayout(II)V

    .line 240
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/android/calendar/event/ik;

    invoke-direct {v3, p0, v4, v0, v1}, Lcom/android/calendar/event/ik;-><init>(Lcom/android/calendar/event/ig;[Ljava/lang/String;Landroid/os/Handler;Landroid/app/ProgressDialog;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/android/calendar/event/ig;->A:Ljava/lang/Thread;

    .line 254
    iget-object v0, p0, Lcom/android/calendar/event/ig;->A:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 359
    :goto_1
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ig;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ig;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 260
    const/4 v0, 0x1

    aget-object v0, v6, v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ig;->h:Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v1, v6, v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 261
    :cond_2
    iget-boolean v0, p0, Lcom/android/calendar/event/ig;->i:Z

    if-eqz v0, :cond_3

    .line 262
    iget-object v0, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->p()V

    .line 263
    invoke-virtual {p0}, Lcom/android/calendar/event/ig;->cancel()V

    goto :goto_1

    .line 265
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/calendar/event/ay;->au:Z

    goto :goto_1

    .line 270
    :cond_4
    const/4 v1, -0x1

    .line 272
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 273
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/16 v8, 0x32

    invoke-direct {v2, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 276
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 277
    const/16 v2, 0x13

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 278
    const/4 v2, 0x0

    aget-object v2, v6, v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 280
    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 281
    iget-object v2, p0, Lcom/android/calendar/event/ig;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 283
    new-instance v7, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 284
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v8, -0x2

    invoke-direct {v0, v2, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 286
    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 288
    const/4 v0, 0x0

    :goto_2
    const/16 v2, 0x30

    if-ge v0, v2, :cond_7

    .line 289
    const/4 v2, 0x1

    aget-object v2, v6, v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 292
    if-eqz v0, :cond_5

    rem-int/lit8 v8, v0, 0x4

    if-nez v8, :cond_5

    .line 293
    new-instance v8, Landroid/view/View;

    iget-object v9, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 294
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v10, 0x32

    invoke-direct {v9, v5, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 296
    if-ne v1, v2, :cond_6

    .line 297
    packed-switch v2, :pswitch_data_0

    .line 317
    :goto_3
    invoke-virtual {v8, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 318
    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 323
    :cond_5
    new-instance v1, Landroid/view/View;

    iget-object v8, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v1, v8}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 324
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v9, 0x32

    invoke-direct {v8, v4, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 326
    packed-switch v2, :pswitch_data_1

    .line 343
    :goto_4
    invoke-virtual {v1, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 344
    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 288
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_2

    .line 299
    :pswitch_0
    invoke-virtual {v8, v3}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3

    .line 302
    :pswitch_1
    const/16 v1, 0xe8

    const/16 v10, 0xa0

    const/16 v11, 0x24

    invoke-static {v1, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v8, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3

    .line 305
    :pswitch_2
    const/16 v1, 0xdc

    const/16 v10, 0x72

    const/16 v11, 0xa4

    invoke-static {v1, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v8, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3

    .line 308
    :pswitch_3
    const/16 v1, 0x8c

    const/16 v10, 0x88

    const/16 v11, 0xdd

    invoke-static {v1, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v8, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3

    .line 311
    :pswitch_4
    invoke-virtual {v8, v3}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3

    .line 315
    :cond_6
    invoke-virtual {v8, v3}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3

    .line 328
    :pswitch_5
    const/16 v9, 0xff

    const/16 v10, 0xff

    const/16 v11, 0xff

    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 331
    :pswitch_6
    const/16 v9, 0xe8

    const/16 v10, 0xa0

    const/16 v11, 0x24

    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 334
    :pswitch_7
    const/16 v9, 0xdc

    const/16 v10, 0x72

    const/16 v11, 0xa4

    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 337
    :pswitch_8
    const/16 v9, 0x8c

    const/16 v10, 0x88

    const/16 v11, 0xdd

    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 340
    :pswitch_9
    const/16 v9, 0xff

    const/16 v10, 0xff

    const/16 v11, 0xff

    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_4

    .line 346
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ig;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 349
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 352
    iget-object v1, p0, Lcom/android/calendar/event/ig;->r:Landroid/widget/ScrollView;

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 355
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x1

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 356
    iget-object v1, p0, Lcom/android/calendar/event/ig;->p:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 357
    iget-object v1, p0, Lcom/android/calendar/event/ig;->q:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 326
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private g()V
    .locals 15

    .prologue
    .line 365
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 366
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 368
    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    .line 370
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 371
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 372
    iget-object v0, p0, Lcom/android/calendar/event/ig;->y:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    array-length v2, v0

    .line 373
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 374
    iget-object v3, p0, Lcom/android/calendar/event/ig;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/event/ig;->y:Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/android/calendar/event/ig;->y:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    iget-object v0, p0, Lcom/android/calendar/event/ig;->y:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 381
    :cond_1
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v9, :cond_2

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 383
    :cond_2
    iget-boolean v0, p0, Lcom/android/calendar/event/ig;->i:Z

    if-eqz v0, :cond_3

    .line 384
    iget-object v0, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->p()V

    .line 385
    invoke-virtual {p0}, Lcom/android/calendar/event/ig;->cancel()V

    .line 506
    :goto_1
    return-void

    .line 387
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/calendar/event/ay;->au:Z

    goto :goto_1

    .line 392
    :cond_4
    const/4 v1, -0x1

    .line 393
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 394
    const/4 v0, 0x0

    move v4, v0

    :goto_2
    if-ge v4, v10, :cond_b

    .line 395
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x30

    if-ge v0, v2, :cond_5

    .line 396
    iget-object v0, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->p()V

    goto :goto_1

    .line 400
    :cond_5
    new-instance v2, Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 401
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/16 v11, 0x32

    invoke-direct {v0, v3, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 404
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 405
    const/16 v0, 0x13

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 406
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    const/high16 v0, -0x1000000

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 408
    const/high16 v0, 0x41900000    # 18.0f

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 409
    iget-object v0, p0, Lcom/android/calendar/event/ig;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 411
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v4, v0, :cond_6

    .line 412
    new-instance v0, Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 413
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v11, 0x1

    invoke-direct {v2, v3, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 415
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 416
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 417
    iget-object v2, p0, Lcom/android/calendar/event/ig;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 420
    :cond_6
    new-instance v11, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v11, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 421
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 423
    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 425
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_3
    const/16 v0, 0x30

    if-ge v1, v0, :cond_9

    .line 426
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 428
    if-eqz v1, :cond_7

    rem-int/lit8 v0, v1, 0x4

    if-nez v0, :cond_7

    .line 429
    new-instance v0, Landroid/view/View;

    iget-object v12, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v0, v12}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 430
    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v13, 0x32

    invoke-direct {v12, v7, v13}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 432
    if-ne v2, v3, :cond_8

    .line 433
    packed-switch v3, :pswitch_data_0

    .line 453
    :goto_4
    invoke-virtual {v0, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 454
    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 459
    :cond_7
    new-instance v0, Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 460
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v12, 0x32

    invoke-direct {v2, v6, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 462
    packed-switch v3, :pswitch_data_1

    .line 479
    :goto_5
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 480
    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 425
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_3

    .line 435
    :pswitch_0
    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 438
    :pswitch_1
    const/16 v2, 0xe8

    const/16 v13, 0xa0

    const/16 v14, 0x24

    invoke-static {v2, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 441
    :pswitch_2
    const/16 v2, 0xdc

    const/16 v13, 0x72

    const/16 v14, 0xa4

    invoke-static {v2, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 444
    :pswitch_3
    const/16 v2, 0x8c

    const/16 v13, 0x88

    const/16 v14, 0xdd

    invoke-static {v2, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 447
    :pswitch_4
    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 451
    :cond_8
    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 464
    :pswitch_5
    const/16 v12, 0xa6

    const/16 v13, 0xd7

    const/16 v14, 0x31

    invoke-static {v12, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v12

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_5

    .line 467
    :pswitch_6
    const/16 v12, 0xe8

    const/16 v13, 0xa0

    const/16 v14, 0x24

    invoke-static {v12, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v12

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_5

    .line 470
    :pswitch_7
    const/16 v12, 0xdc

    const/16 v13, 0x72

    const/16 v14, 0xa4

    invoke-static {v12, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v12

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_5

    .line 473
    :pswitch_8
    const/16 v12, 0x8c

    const/16 v13, 0x88

    const/16 v14, 0xdd

    invoke-static {v12, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v12

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_5

    .line 476
    :pswitch_9
    const/16 v12, 0xff

    const/16 v13, 0xff

    const/16 v14, 0xff

    invoke-static {v12, v13, v14}, Landroid/graphics/Color;->rgb(III)I

    move-result v12

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_5

    .line 482
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/ig;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 485
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v4, v0, :cond_a

    .line 486
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/event/ig;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 487
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v11, 0x1

    invoke-direct {v1, v3, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 489
    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 490
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 491
    iget-object v1, p0, Lcom/android/calendar/event/ig;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 394
    :cond_a
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v1, v2

    goto/16 :goto_2

    .line 496
    :cond_b
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_c

    .line 497
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 500
    iget-object v1, p0, Lcom/android/calendar/event/ig;->r:Landroid/widget/ScrollView;

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 502
    :cond_c
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/16 v2, 0xfe

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 504
    iget-object v1, p0, Lcom/android/calendar/event/ig;->r:Landroid/widget/ScrollView;

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 433
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 462
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public onStop()V
    .locals 1

    .prologue
    .line 782
    iget-object v0, p0, Lcom/android/calendar/event/ig;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->at:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 783
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 784
    return-void
.end method
