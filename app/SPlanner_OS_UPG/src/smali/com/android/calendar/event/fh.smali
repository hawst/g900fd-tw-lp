.class Lcom/android/calendar/event/fh;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Lcom/android/calendar/common/extension/i;


# instance fields
.field a:Landroid/view/View;

.field final synthetic b:Lcom/android/calendar/event/ay;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ay;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 819
    iput-object p1, p0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 820
    iput-object p2, p0, Lcom/android/calendar/event/fh;->a:Landroid/view/View;

    .line 821
    return-void
.end method


# virtual methods
.method public a(Landroid/widget/DatePicker;Landroid/widget/TimePicker;IIIIIZ)V
    .locals 0

    .prologue
    .line 959
    return-void
.end method

.method public a(Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Z)V
    .locals 27

    .prologue
    .line 826
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v2

    .line 827
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v20

    .line 828
    const-wide/16 v12, 0x0

    .line 829
    const-wide/16 v10, 0x0

    .line 830
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v8, v3, Landroid/text/format/Time;->year:I

    .line 831
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v7, v3, Landroid/text/format/Time;->month:I

    .line 832
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v6, v3, Landroid/text/format/Time;->monthDay:I

    .line 833
    iget v0, v2, Landroid/text/format/Time;->weekDay:I

    move/from16 v21, v0

    .line 834
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v0, v3, Landroid/text/format/Time;->hour:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v5, v3, Landroid/text/format/Time;->minute:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    .line 835
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v0, v4, Landroid/text/format/Time;->year:I

    move/from16 v17, v0

    .line 836
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v0, v4, Landroid/text/format/Time;->month:I

    move/from16 v16, v0

    .line 837
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v9, v4, Landroid/text/format/Time;->monthDay:I

    .line 838
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v15, v4, Landroid/text/format/Time;->hour:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v14, v4, Landroid/text/format/Time;->minute:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v0, v4, Landroid/text/format/Time;->second:I

    move/from16 v22, v0

    .line 839
    const/4 v4, 0x0

    .line 840
    if-eqz p1, :cond_11

    .line 841
    invoke-virtual/range {p1 .. p1}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v19

    move/from16 v0, v19

    if-ne v6, v0, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/widget/DatePicker;->getMonth()I

    move-result v6

    if-ne v7, v6, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/widget/DatePicker;->getYear()I

    move-result v6

    if-eq v8, v6, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v6}, Lcom/android/calendar/event/ay;->h(Lcom/android/calendar/event/ay;)I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 843
    const/4 v4, 0x1

    .line 845
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/widget/DatePicker;->getYear()I

    move-result v8

    .line 846
    invoke-virtual/range {p1 .. p1}, Landroid/widget/DatePicker;->getMonth()I

    move-result v7

    .line 847
    invoke-virtual/range {p1 .. p1}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v6

    move/from16 v19, v4

    .line 849
    :goto_0
    if-eqz p2, :cond_10

    .line 850
    invoke-virtual/range {p2 .. p2}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 851
    invoke-virtual/range {p2 .. p2}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 853
    :goto_1
    if-eqz p3, :cond_f

    .line 854
    invoke-virtual/range {p3 .. p3}, Landroid/widget/DatePicker;->getYear()I

    move-result v17

    .line 855
    invoke-virtual/range {p3 .. p3}, Landroid/widget/DatePicker;->getMonth()I

    move-result v16

    .line 856
    invoke-virtual/range {p3 .. p3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v9

    move/from16 v26, v9

    move/from16 v9, v17

    move/from16 v17, v16

    move/from16 v16, v26

    .line 858
    :goto_2
    if-eqz p4, :cond_2

    .line 859
    invoke-virtual/range {p4 .. p4}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 860
    invoke-virtual/range {p4 .. p4}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 863
    :cond_2
    if-eqz p5, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/android/calendar/d/g;->c()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v18, v0

    const/16 v23, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;I)I

    .line 869
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/fh;->a:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 870
    move-object/from16 v0, v20

    iget v10, v0, Landroid/text/format/Time;->second:I

    iget v11, v2, Landroid/text/format/Time;->second:I

    sub-int v12, v10, v11

    .line 871
    move-object/from16 v0, v20

    iget v10, v0, Landroid/text/format/Time;->minute:I

    iget v11, v2, Landroid/text/format/Time;->minute:I

    sub-int v13, v10, v11

    .line 872
    move-object/from16 v0, v20

    iget v10, v0, Landroid/text/format/Time;->hour:I

    iget v11, v2, Landroid/text/format/Time;->hour:I

    sub-int v18, v10, v11

    .line 873
    move-object/from16 v0, v20

    iget v10, v0, Landroid/text/format/Time;->monthDay:I

    iget v11, v2, Landroid/text/format/Time;->monthDay:I

    sub-int v23, v10, v11

    .line 874
    move-object/from16 v0, v20

    iget v10, v0, Landroid/text/format/Time;->month:I

    iget v11, v2, Landroid/text/format/Time;->month:I

    sub-int v24, v10, v11

    .line 875
    move-object/from16 v0, v20

    iget v10, v0, Landroid/text/format/Time;->year:I

    iget v11, v2, Landroid/text/format/Time;->year:I

    sub-int v25, v10, v11

    .line 878
    invoke-static/range {v2 .. v8}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v10

    .line 882
    if-nez p3, :cond_6

    .line 883
    invoke-virtual {v2, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 884
    iget v3, v2, Landroid/text/format/Time;->second:I

    add-int v4, v3, v12

    iget v3, v2, Landroid/text/format/Time;->minute:I

    add-int v5, v3, v13

    iget v3, v2, Landroid/text/format/Time;->hour:I

    add-int v6, v3, v18

    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int v7, v3, v23

    iget v3, v2, Landroid/text/format/Time;->month:I

    add-int v8, v3, v24

    iget v3, v2, Landroid/text/format/Time;->year:I

    add-int v9, v3, v25

    move-object/from16 v3, v20

    invoke-static/range {v3 .. v9}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    .line 889
    invoke-static/range {v20 .. v20}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 890
    const v3, 0x259d23

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 904
    :cond_3
    :goto_4
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 906
    if-eqz v19, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->i(Lcom/android/calendar/event/ay;)I

    move-result v3

    const/4 v6, 0x1

    if-eq v3, v6, :cond_8

    .line 907
    const/4 v3, 0x7

    new-array v6, v3, [Z

    .line 908
    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    sub-int v2, v2, v21

    .line 909
    if-gez v2, :cond_4

    .line 910
    add-int/lit8 v2, v2, 0x7

    .line 912
    :cond_4
    const/4 v3, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v7}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v7

    iget-object v7, v7, Lcom/android/calendar/as;->V:[Z

    array-length v7, v7

    if-ge v3, v7, :cond_7

    .line 913
    add-int v7, v3, v2

    rem-int/lit8 v7, v7, 0x7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    iget-object v8, v8, Lcom/android/calendar/event/ay;->O:[Z

    aget-boolean v8, v8, v3

    aput-boolean v8, v6, v7

    .line 912
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 866
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v18, v0

    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;I)I

    goto/16 :goto_3

    :cond_6
    move-object/from16 v3, v20

    move/from16 v4, v22

    move v5, v14

    move v6, v15

    move/from16 v7, v16

    move/from16 v8, v17

    .line 893
    invoke-static/range {v3 .. v9}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    .line 896
    move-object/from16 v0, v20

    invoke-static {v2, v0}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v3

    if-lez v3, :cond_3

    .line 901
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->e(Lcom/android/calendar/event/ay;)V

    goto :goto_4

    .line 915
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    iput-object v6, v2, Lcom/android/calendar/event/ay;->O:[Z

    .line 916
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->k(Lcom/android/calendar/event/ay;)Z

    .line 917
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v2

    iget-object v2, v2, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 918
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;)Lcom/android/a/c;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 923
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->d(Lcom/android/calendar/event/ay;)V

    move-wide v2, v4

    move-wide v4, v10

    .line 942
    :goto_6
    cmp-long v6, v4, v2

    if-lez v6, :cond_9

    .line 943
    const-wide/32 v2, 0x36ee80

    add-long/2addr v2, v4

    .line 944
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v6}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 946
    :cond_9
    invoke-static/range {v20 .. v20}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v6

    if-eqz v6, :cond_a

    .line 947
    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    move-wide v2, v4

    .line 950
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    iget-object v7, v7, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-static {v6, v7, v4, v5}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Landroid/view/View;J)V

    .line 951
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    iget-object v5, v5, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-static {v4, v5, v2, v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Landroid/view/View;J)V

    .line 953
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v2}, Lcom/android/calendar/event/ay;->s()V

    .line 954
    return-void

    .line 924
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/fh;->a:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 925
    if-eqz p1, :cond_c

    .line 926
    invoke-static/range {v2 .. v8}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    .line 929
    :cond_c
    invoke-static/range {v2 .. v8}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v10

    move-object/from16 v3, v20

    move/from16 v4, v22

    move v5, v14

    move v6, v15

    move/from16 v7, v16

    move/from16 v8, v17

    .line 932
    invoke-static/range {v3 .. v9}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v4

    .line 935
    move-object/from16 v0, v20

    invoke-static {v2, v0}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v2

    if-lez v2, :cond_d

    .line 939
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fh;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->e(Lcom/android/calendar/event/ay;)V

    :cond_d
    move-wide v2, v4

    move-wide v4, v10

    goto/16 :goto_6

    :cond_e
    move-wide v2, v10

    move-wide v4, v12

    goto/16 :goto_6

    :cond_f
    move/from16 v26, v9

    move/from16 v9, v17

    move/from16 v17, v16

    move/from16 v16, v26

    goto/16 :goto_2

    :cond_10
    move v4, v5

    move/from16 v5, v18

    goto/16 :goto_1

    :cond_11
    move/from16 v19, v4

    goto/16 :goto_0
.end method
