.class Lcom/android/calendar/event/lk;
.super Landroid/content/AsyncQueryHandler;
.source "SimpleAddDialogFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ld;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ld;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 888
    iput-object p1, p0, Lcom/android/calendar/event/lk;->a:Lcom/android/calendar/event/ld;

    .line 889
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 890
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 894
    if-nez p3, :cond_0

    .line 941
    :goto_0
    return-void

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/lk;->a:Lcom/android/calendar/event/ld;

    invoke-virtual {v0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 899
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 900
    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 904
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 938
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 909
    :pswitch_0
    :try_start_0
    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 915
    iget-object v1, p0, Lcom/android/calendar/event/lk;->a:Lcom/android/calendar/event/ld;

    invoke-static {v1, v0}, Lcom/android/calendar/event/ld;->a(Lcom/android/calendar/event/ld;Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 918
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 922
    :pswitch_1
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 923
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v1, "TOKEN_TASKACCOUNT cursor count is 0 "

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 929
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/event/lk;->a:Lcom/android/calendar/event/ld;

    invoke-static {p3}, Lcom/android/calendar/task/ab;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/event/ld;->f:Ljava/util/ArrayList;

    .line 930
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 931
    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 932
    iget-object v1, p0, Lcom/android/calendar/event/lk;->a:Lcom/android/calendar/event/ld;

    invoke-static {v1, v0}, Lcom/android/calendar/event/ld;->b(Lcom/android/calendar/event/ld;Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 934
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_1
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 904
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
