.class Lcom/android/calendar/event/kh;
.super Ljava/lang/Object;
.source "SelectMapChinaActivity.java"

# interfaces
.implements Lcom/amap/api/services/poisearch/PoiSearch$OnPoiSearchListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectMapChinaActivity;


# direct methods
.method private constructor <init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V
    .locals 0

    .prologue
    .line 852
    iput-object p1, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/android/calendar/event/jq;)V
    .locals 0

    .prologue
    .line 852
    invoke-direct {p0, p1}, Lcom/android/calendar/event/kh;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    return-void
.end method


# virtual methods
.method public onPoiItemDetailSearched(Lcom/amap/api/services/poisearch/PoiItemDetail;I)V
    .locals 0

    .prologue
    .line 855
    return-void
.end method

.method public onPoiSearched(Lcom/amap/api/services/poisearch/PoiResult;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 859
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPoiSearched_ErrorCode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    if-nez p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiResult;->getPois()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiResult;->getPois()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->t(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 870
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-virtual {v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0290

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->l(Lcom/android/calendar/event/SelectMapChinaActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 872
    iget-object v2, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 911
    :cond_1
    :goto_0
    return-void

    .line 875
    :cond_2
    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiResult;->getPois()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->t(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 876
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    const v2, 0x7f0f02d9

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 880
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z

    .line 881
    sget-object v0, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 882
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiResult;->getPageCount()I

    move-result v2

    invoke-static {v0, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;I)I

    .line 883
    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiResult;->getPois()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v0, v1

    .line 885
    :goto_1
    if-ge v0, v2, :cond_4

    .line 886
    sget-object v3, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiResult;->getPois()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 887
    iget-object v3, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->u(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/android/calendar/event/ke;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/calendar/event/ke;->notifyDataSetInvalidated()V

    .line 885
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 890
    :cond_4
    if-lez v2, :cond_7

    .line 891
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->v(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 892
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->v(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_5

    .line 893
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->v(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->w(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 894
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->x(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 895
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget v0, v0, Lcom/android/calendar/event/SelectMapChinaActivity;->f:I

    if-nez v0, :cond_8

    const/16 v0, 0x14

    if-ge v2, v0, :cond_8

    .line 896
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->v(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 897
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->v(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->w(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 902
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(I)V

    .line 905
    :cond_7
    const/4 v0, 0x1

    if-le v2, v0, :cond_1

    .line 906
    new-instance v0, Lcom/amap/api/maps/overlay/PoiOverlay;

    iget-object v1, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->y(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/AMap;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/amap/api/maps/overlay/PoiOverlay;-><init>(Lcom/amap/api/maps/AMap;Ljava/util/List;)V

    .line 907
    invoke-virtual {v0}, Lcom/amap/api/maps/overlay/PoiOverlay;->removeFromMap()V

    .line 908
    invoke-virtual {v0}, Lcom/amap/api/maps/overlay/PoiOverlay;->addToMap()V

    .line 909
    invoke-virtual {v0}, Lcom/amap/api/maps/overlay/PoiOverlay;->zoomToSpan()V

    goto/16 :goto_0

    .line 898
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget v0, v0, Lcom/android/calendar/event/SelectMapChinaActivity;->f:I

    if-nez v0, :cond_6

    .line 899
    iget-object v0, p0, Lcom/android/calendar/event/kh;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->x(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method
