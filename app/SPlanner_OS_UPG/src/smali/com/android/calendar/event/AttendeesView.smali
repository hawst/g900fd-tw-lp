.class public Lcom/android/calendar/event/AttendeesView;
.super Landroid/widget/RelativeLayout;
.source "AttendeesView.java"


# static fields
.field private static final c:[Ljava/lang/String;

.field private static final d:Landroid/net/Uri;

.field private static t:[Ljava/lang/String;

.field private static u:Ljava/lang/String;


# instance fields
.field private a:Ljava/util/regex/Pattern;

.field private b:Lcom/android/calendar/event/fb;

.field private final e:Landroid/content/Context;

.field private final f:Landroid/view/LayoutInflater;

.field private final g:Lcom/android/calendar/event/k;

.field private final h:Landroid/graphics/drawable/Drawable;

.field private final i:Landroid/graphics/ColorMatrixColorFilter;

.field private final j:I

.field private final k:I

.field private l:Lcom/android/b/b;

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Landroid/app/AlertDialog;

.field private s:Landroid/widget/MultiAutoCompleteTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 79
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "data1"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/AttendeesView;->c:[Ljava/lang/String;

    .line 85
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/calendar/event/AttendeesView;->d:Landroid/net/Uri;

    .line 110
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/calendar/event/AttendeesView;->t:[Ljava/lang/String;

    .line 111
    const-string v0, "display_name =? AND data1 =? "

    sput-object v0, Lcom/android/calendar/event/AttendeesView;->u:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 116
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    const-string v0, "(.*?);(.*?);(.*?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/AttendeesView;->a:Ljava/util/regex/Pattern;

    .line 74
    iput-object v1, p0, Lcom/android/calendar/event/AttendeesView;->b:Lcom/android/calendar/event/fb;

    .line 106
    iput-object v1, p0, Lcom/android/calendar/event/AttendeesView;->r:Landroid/app/AlertDialog;

    .line 108
    iput-object v1, p0, Lcom/android/calendar/event/AttendeesView;->s:Landroid/widget/MultiAutoCompleteTextView;

    .line 117
    iput-object p1, p0, Lcom/android/calendar/event/AttendeesView;->e:Landroid/content/Context;

    .line 118
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/event/AttendeesView;->f:Landroid/view/LayoutInflater;

    .line 119
    new-instance v0, Lcom/android/calendar/event/k;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/k;-><init>(Lcom/android/calendar/event/AttendeesView;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/AttendeesView;->g:Lcom/android/calendar/event/k;

    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 122
    const v1, 0x7f020100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/AttendeesView;->h:Landroid/graphics/drawable/Drawable;

    .line 123
    const v1, 0x7f0d001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/AttendeesView;->j:I

    .line 125
    const v1, 0x7f0d0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/AttendeesView;->k:I

    .line 127
    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->q:I

    .line 128
    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    .line 129
    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->n:I

    .line 132
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 133
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 134
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    iput-object v1, p0, Lcom/android/calendar/event/AttendeesView;->i:Landroid/graphics/ColorMatrixColorFilter;

    .line 135
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/AttendeesView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/android/calendar/event/AttendeesView;->r:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/AttendeesView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/AttendeesView;Lcom/android/calendar/event/aw;)Landroid/view/View;
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/event/aw;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/android/calendar/event/aw;)Landroid/view/View;
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f040029

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/android/calendar/event/aw;->e:Landroid/view/View;

    .line 162
    invoke-direct {p0, p1}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/event/aw;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 406
    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    .line 407
    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->n:I

    .line 408
    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->o:I

    .line 409
    iput v2, p0, Lcom/android/calendar/event/AttendeesView;->p:I

    .line 411
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/aw;

    .line 412
    iget-boolean v1, v0, Lcom/android/calendar/event/aw;->a:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, v0, Lcom/android/calendar/event/aw;->a:Z

    .line 413
    iget v1, p0, Lcom/android/calendar/event/AttendeesView;->q:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/calendar/event/AttendeesView;->q:I

    .line 415
    iget-object v1, p0, Lcom/android/calendar/event/AttendeesView;->b:Lcom/android/calendar/event/fb;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/calendar/event/AttendeesView;->q:I

    if-nez v1, :cond_0

    .line 416
    iget-object v1, p0, Lcom/android/calendar/event/AttendeesView;->b:Lcom/android/calendar/event/fb;

    invoke-virtual {v1, p1}, Lcom/android/calendar/event/fb;->a(Landroid/view/View;)V

    .line 418
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/event/aw;)Landroid/view/View;

    .line 420
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 421
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v4

    move v1, v2

    .line 423
    :goto_1
    if-ge v1, v4, :cond_4

    .line 424
    invoke-virtual {p0, v1}, Lcom/android/calendar/event/AttendeesView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 425
    instance-of v5, v0, Landroid/widget/TextView;

    if-eqz v5, :cond_3

    .line 423
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 412
    goto :goto_0

    .line 428
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/aw;

    .line 429
    iget-boolean v5, v0, Lcom/android/calendar/event/aw;->a:Z

    if-nez v5, :cond_1

    .line 430
    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/aw;)Landroid/view/View;

    move-result-object v5

    .line 431
    invoke-virtual {v5, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 432
    iget-object v0, v0, Lcom/android/calendar/event/aw;->e:Landroid/view/View;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 436
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->removeAllViews()V

    .line 437
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 438
    :goto_3
    if-ge v2, v1, :cond_5

    .line 439
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->setAttendeeLayout(Landroid/view/View;)V

    .line 438
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 442
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->s:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->requestFocus()Z

    .line 443
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/AttendeesView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/android/calendar/event/AttendeesView;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 70
    sput-object p0, Lcom/android/calendar/event/AttendeesView;->t:[Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/android/calendar/event/AttendeesView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->r:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private b(Lcom/android/calendar/event/aw;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v2, 0x8

    .line 169
    iget-object v3, p1, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    .line 170
    iget-object v4, p1, Lcom/android/calendar/event/aw;->e:Landroid/view/View;

    .line 171
    const v0, 0x7f120060

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 173
    iget-object v1, p0, Lcom/android/calendar/event/AttendeesView;->e:Landroid/content/Context;

    iget-object v5, v3, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/android/calendar/event/hm;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/android/calendar/at;->a:Ljava/lang/String;

    .line 174
    iget-object v1, v3, Lcom/android/calendar/at;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v3, Lcom/android/calendar/at;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-boolean v1, p1, Lcom/android/calendar/event/aw;->a:Z

    if-eqz v1, :cond_3

    .line 176
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 183
    :goto_1
    const v1, 0x7f12009e

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 184
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x0

    :cond_0
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 185
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 186
    iget-boolean v2, p1, Lcom/android/calendar/event/aw;->a:Z

    if-nez v2, :cond_1

    .line 187
    iget-object v2, p0, Lcom/android/calendar/event/AttendeesView;->e:Landroid/content/Context;

    const v5, 0x7f0f0018

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 189
    :cond_1
    new-instance v2, Lcom/android/calendar/event/g;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/g;-><init>(Lcom/android/calendar/event/AttendeesView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    new-instance v2, Lcom/android/calendar/event/h;

    invoke-direct {v2, p0, v3, v1}, Lcom/android/calendar/event/h;-><init>(Lcom/android/calendar/event/AttendeesView;Lcom/android/calendar/at;Landroid/widget/ImageView;)V

    .line 197
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 200
    return-object v4

    .line 174
    :cond_2
    iget-object v1, v3, Lcom/android/calendar/at;->a:Ljava/lang/String;

    goto :goto_0

    .line 178
    :cond_3
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    and-int/lit8 v1, v1, -0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_1
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 70
    sput-object p0, Lcom/android/calendar/event/AttendeesView;->u:Ljava/lang/String;

    return-object p0
.end method

.method private b(Lcom/android/calendar/at;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 475
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/at;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->e:Landroid/content/Context;

    const v1, 0x7f0f0183

    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 491
    :goto_0
    return-void

    .line 479
    :cond_0
    new-instance v2, Lcom/android/calendar/event/aw;

    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->h:Landroid/graphics/drawable/Drawable;

    invoke-direct {v2, p1, v0}, Lcom/android/calendar/event/aw;-><init>(Lcom/android/calendar/at;Landroid/graphics/drawable/Drawable;)V

    .line 481
    invoke-direct {p0, v2}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/aw;)Landroid/view/View;

    move-result-object v0

    .line 482
    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 483
    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->setAttendeeLayout(Landroid/view/View;)V

    .line 484
    iget v0, p0, Lcom/android/calendar/event/AttendeesView;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/AttendeesView;->q:I

    .line 486
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->g:Lcom/android/calendar/event/k;

    iget v1, v2, Lcom/android/calendar/event/aw;->d:I

    add-int/lit8 v1, v1, 0x1

    sget-object v3, Lcom/android/calendar/event/AttendeesView;->d:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/AttendeesView;->c:[Ljava/lang/String;

    const-string v5, "data1 IN (?)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    iget-object v7, p1, Lcom/android/calendar/at;->b:Ljava/lang/String;

    aput-object v7, v6, v8

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/k;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/event/AttendeesView;)Landroid/widget/MultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->s:Landroid/widget/MultiAutoCompleteTextView;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 502
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 503
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 504
    array-length v1, v0

    if-ne v1, v2, :cond_0

    .line 505
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 514
    :goto_0
    return-object v0

    .line 507
    :cond_0
    aget-object v0, v0, v2

    goto :goto_0

    .line 510
    :cond_1
    array-length v1, v0

    if-eq v1, v2, :cond_2

    array-length v1, v0

    if-ne v1, v3, :cond_3

    .line 512
    :cond_2
    aget-object v0, v0, v2

    goto :goto_0

    .line 514
    :cond_3
    aget-object v0, v0, v3

    goto :goto_0
.end method

.method static synthetic c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/android/calendar/event/AttendeesView;->t:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/android/calendar/event/AttendeesView;->u:Ljava/lang/String;

    return-object v0
.end method

.method private setAttendeeLayout(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, -0x2

    const/4 v7, 0x0

    .line 778
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 781
    iget v0, p0, Lcom/android/calendar/event/AttendeesView;->n:I

    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 782
    iget v0, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 783
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 784
    invoke-virtual {p1, v7, v7}, Landroid/view/View;->measure(II)V

    .line 786
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0c009c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0c009d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0c0295

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0c0294

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v1, v0

    .line 791
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 792
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 794
    if-le v0, v1, :cond_0

    .line 796
    const v0, 0x7f120060

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    int-to-float v4, v1

    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0145

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    move v0, v1

    .line 801
    :cond_0
    iget v4, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    add-int/2addr v4, v0

    iput v4, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    .line 802
    iget v4, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    if-le v4, v1, :cond_1

    .line 803
    iput v0, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    .line 804
    iget v1, p0, Lcom/android/calendar/event/AttendeesView;->n:I

    int-to-float v1, v1

    iget v4, p0, Lcom/android/calendar/event/AttendeesView;->p:I

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0143

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/AttendeesView;->n:I

    .line 806
    iget v1, p0, Lcom/android/calendar/event/AttendeesView;->n:I

    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 807
    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 809
    :cond_1
    iget v1, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c0144

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    .line 810
    iput v0, p0, Lcom/android/calendar/event/AttendeesView;->o:I

    .line 811
    iput v3, p0, Lcom/android/calendar/event/AttendeesView;->p:I

    .line 812
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/AttendeesView;->addView(Landroid/view/View;)V

    .line 814
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 470
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->removeAllViews()V

    .line 471
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/AttendeesView;->q:I

    .line 472
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 207
    .line 211
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "display_name"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "_id"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 215
    if-eqz v7, :cond_2

    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 216
    const-string v0, "display_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 218
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 222
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "data1"

    aput-object v4, v2, v3

    const-string v3, "contact_id =? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v5, v4, v10

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 231
    :try_start_2
    const-string v0, ""

    .line 232
    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 233
    const-string v0, "data1"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    move v3, v8

    .line 237
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 238
    invoke-virtual {p0, v3}, Lcom/android/calendar/event/AttendeesView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 239
    instance-of v0, v4, Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 240
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/aw;

    iget-object v5, v0, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    .line 241
    iget-object v0, v5, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iput-object v9, v5, Lcom/android/calendar/at;->a:Ljava/lang/String;

    .line 244
    const v0, 0x7f120060

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 245
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/h;

    iget-object v4, v5, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v0, v9, v4}, Lcom/android/calendar/event/h;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 237
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move-object v6, v2

    .line 259
    :cond_2
    if-eqz v6, :cond_3

    .line 260
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 262
    :cond_3
    if-eqz v7, :cond_4

    .line 263
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 266
    :cond_4
    :goto_2
    return-void

    .line 255
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 256
    :goto_3
    :try_start_3
    const-string v2, "AttendeesView"

    const-string v3, "IllegalArgumentException from refreshAttendees"

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 259
    if-eqz v1, :cond_5

    .line 260
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 262
    :cond_5
    if-eqz v6, :cond_4

    .line 263
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 259
    :catchall_0
    move-exception v0

    move-object v7, v6

    :goto_4
    if-eqz v6, :cond_6

    .line 260
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 262
    :cond_6
    if-eqz v7, :cond_7

    .line 263
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 259
    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v6, v2

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v7, v6

    move-object v6, v1

    goto :goto_4

    .line 255
    :catch_1
    move-exception v0

    move-object v1, v6

    move-object v6, v7

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v1, v2

    move-object v6, v7

    goto :goto_3

    :cond_8
    move-object v1, v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 521
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522
    const-string v0, "(.*?);(.*?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/AttendeesView;->a:Ljava/util/regex/Pattern;

    .line 523
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 528
    const-string v0, ""

    .line 529
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 530
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 531
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 538
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->l:Lcom/android/b/b;

    invoke-static {v1, v0}, Lcom/android/calendar/event/av;->a(Ljava/lang/String;Lcom/android/b/b;)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 540
    monitor-enter p0

    .line 541
    :try_start_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 542
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v3, p1

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/util/Rfc822Token;

    .line 543
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v2

    .line 544
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 545
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v0, v1

    .line 549
    :cond_2
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "@"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    :cond_3
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v6

    if-eqz v6, :cond_8

    const-string v6, "@"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 551
    :cond_4
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 586
    :cond_5
    :goto_2
    new-instance v6, Lcom/android/calendar/at;

    invoke-direct {v6, v2, v0}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    invoke-direct {p0, v3}, Lcom/android/calendar/event/AttendeesView;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 590
    iget-object v2, v6, Lcom/android/calendar/at;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 591
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 592
    iget-object v0, v6, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iput-object v0, v6, Lcom/android/calendar/at;->a:Ljava/lang/String;

    .line 597
    :cond_6
    :goto_3
    invoke-direct {p0, v6}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/at;)V

    goto :goto_1

    .line 599
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 534
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 557
    :cond_8
    :try_start_1
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 558
    invoke-static {v0}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 559
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f020f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 561
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 562
    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 563
    const/16 v2, 0x40

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 564
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 565
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v3, v0

    .line 570
    goto :goto_2

    .line 572
    :cond_9
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f020f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 574
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 575
    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 576
    const/16 v2, 0x40

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 577
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 578
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v3, v0

    .line 581
    goto :goto_2

    .line 594
    :cond_a
    iput-object v0, v6, Lcom/android/calendar/at;->a:Ljava/lang/String;

    goto :goto_3

    .line 599
    :cond_b
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600
    return-void

    :cond_c
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 603
    new-instance v0, Lcom/android/calendar/at;

    invoke-direct {v0, p1, p2}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/at;)V

    .line 605
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 608
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 609
    monitor-enter p0

    .line 610
    :try_start_0
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 611
    array-length v6, v4

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_6

    aget-object v1, v4, v3

    .line 612
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->l:Lcom/android/b/b;

    invoke-static {v1, v0}, Lcom/android/calendar/event/av;->a(Ljava/lang/String;Lcom/android/b/b;)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 616
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/util/Rfc822Token;

    .line 617
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v2

    .line 618
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 620
    const-string v8, "@"

    invoke-virtual {v1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 621
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 648
    :cond_1
    :goto_2
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 650
    new-instance v8, Lcom/android/calendar/at;

    invoke-direct {v8, v2, v0}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    iget-object v0, v8, Lcom/android/calendar/at;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 653
    iget-object v0, v8, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iput-object v0, v8, Lcom/android/calendar/at;->a:Ljava/lang/String;

    .line 655
    :cond_2
    invoke-direct {p0, v8}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/at;)V

    goto :goto_1

    .line 659
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 625
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 626
    invoke-static {v0}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 627
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f020f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 630
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 631
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 632
    const/16 v2, 0x40

    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 633
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object v0, v1

    move-object v2, v1

    .line 636
    goto :goto_2

    .line 638
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f020f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 640
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 641
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 642
    const/16 v2, 0x40

    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 643
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object v0, v1

    move-object v2, v1

    .line 645
    goto :goto_2

    .line 611
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 659
    :cond_6
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 660
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 2

    .prologue
    .line 494
    monitor-enter p0

    .line 495
    :try_start_0
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 496
    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/at;)V

    goto :goto_0

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499
    return-void
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 667
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/AttendeesView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 668
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 669
    const/4 v0, 0x0

    .line 671
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/aw;

    iget-boolean v0, v0, Lcom/android/calendar/event/aw;->a:Z

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/at;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 454
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v3

    move v2, v1

    .line 455
    :goto_0
    if-ge v2, v3, :cond_2

    .line 456
    invoke-virtual {p0, v2}, Lcom/android/calendar/event/AttendeesView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 457
    instance-of v4, v0, Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 455
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 460
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/aw;

    .line 461
    iget-object v4, p1, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iget-object v5, v0, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    iget-object v5, v5, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v0, v0, Lcom/android/calendar/event/aw;->a:Z

    if-nez v0, :cond_0

    .line 463
    const/4 v0, 0x1

    .line 466
    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public b(I)Lcom/android/calendar/at;
    .locals 2

    .prologue
    .line 738
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/AttendeesView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 739
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 740
    const/4 v0, 0x0

    .line 742
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/aw;

    iget-object v0, v0, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    goto :goto_0
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 750
    iput v1, p0, Lcom/android/calendar/event/AttendeesView;->m:I

    .line 751
    iput v1, p0, Lcom/android/calendar/event/AttendeesView;->n:I

    .line 752
    iput v1, p0, Lcom/android/calendar/event/AttendeesView;->o:I

    .line 753
    iput v1, p0, Lcom/android/calendar/event/AttendeesView;->p:I

    .line 754
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 755
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v4

    move v2, v1

    .line 757
    :goto_0
    if-ge v2, v4, :cond_1

    .line 758
    invoke-virtual {p0, v2}, Lcom/android/calendar/event/AttendeesView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 759
    instance-of v5, v0, Landroid/widget/TextView;

    if-eqz v5, :cond_0

    .line 757
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 762
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/aw;

    .line 764
    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/aw;)Landroid/view/View;

    move-result-object v5

    .line 765
    invoke-virtual {v5, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 766
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 769
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->removeAllViews()V

    .line 770
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 771
    :goto_2
    if-ge v1, v2, :cond_2

    .line 772
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/AttendeesView;->setAttendeeLayout(Landroid/view/View;)V

    .line 771
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 775
    :cond_2
    return-void
.end method

.method public getAttendeeItemCount()I
    .locals 1

    .prologue
    .line 817
    iget v0, p0, Lcom/android/calendar/event/AttendeesView;->q:I

    return v0
.end method

.method public getAttendeePopupDialog()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/android/calendar/event/AttendeesView;->r:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 140
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 141
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 142
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/event/AttendeesView;->getChildCount()I

    move-result v2

    .line 143
    :goto_1
    if-ge v1, v2, :cond_2

    .line 144
    invoke-virtual {p0, v1}, Lcom/android/calendar/event/AttendeesView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 145
    const v4, 0x7f12009e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 146
    if-eqz v3, :cond_0

    .line 147
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 143
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 141
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 150
    :cond_2
    return-void
.end method

.method public setOnCleanedListerner(Lcom/android/calendar/event/fb;)V
    .locals 0

    .prologue
    .line 746
    iput-object p1, p0, Lcom/android/calendar/event/AttendeesView;->b:Lcom/android/calendar/event/fb;

    .line 747
    return-void
.end method

.method public setRfc822Validator(Lcom/android/b/b;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/android/calendar/event/AttendeesView;->l:Lcom/android/b/b;

    .line 154
    return-void
.end method

.method public setmAttendeesEditText(Landroid/widget/MultiAutoCompleteTextView;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/android/calendar/event/AttendeesView;->s:Landroid/widget/MultiAutoCompleteTextView;

    .line 447
    return-void
.end method
