.class Lcom/android/calendar/event/kz;
.super Ljava/lang/Object;
.source "SelectStickerFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/android/calendar/event/kx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/kx;I)V
    .locals 0

    .prologue
    .line 922
    iput-object p1, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iput p2, p0, Lcom/android/calendar/event/kz;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 926
    iget-object v1, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iget-object v1, v1, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->a(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/view/GestureDetector;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 927
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 958
    :cond_0
    :goto_0
    return v0

    .line 931
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    .line 932
    iget-object v0, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iget-object v0, v0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v0, v1}, Lcom/android/calendar/event/SelectStickerFragment;->a(Lcom/android/calendar/event/SelectStickerFragment;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 935
    invoke-virtual {p1, v6}, Landroid/view/View;->setPressed(Z)V

    move v0, v6

    .line 936
    goto :goto_0

    .line 937
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v6, :cond_5

    .line 938
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 940
    iget-object v1, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iget-object v1, v1, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->h(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 942
    invoke-virtual {p1}, Landroid/view/View;->isSoundEffectsEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 943
    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 945
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    invoke-static {v0}, Lcom/android/calendar/event/kx;->a(Lcom/android/calendar/event/kx;)Landroid/widget/GridView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iget-object v1, v1, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 946
    iget-object v0, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iget-object v0, v0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->h:Landroid/widget/AdapterView$OnItemClickListener;

    iget-object v1, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iget-object v1, v1, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v1

    iget v3, p0, Lcom/android/calendar/event/kz;->a:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    int-to-long v4, v2

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :goto_1
    move v0, v6

    .line 952
    goto/16 :goto_0

    .line 949
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iget-object v0, v0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->i:Landroid/widget/AdapterView$OnItemClickListener;

    iget-object v1, p0, Lcom/android/calendar/event/kz;->b:Lcom/android/calendar/event/kx;

    iget-object v1, v1, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v1

    iget v3, p0, Lcom/android/calendar/event/kz;->a:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    int-to-long v4, v2

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_1

    .line 954
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 955
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    move v0, v6

    .line 956
    goto/16 :goto_0
.end method
