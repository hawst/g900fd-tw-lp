.class public Lcom/android/calendar/event/fq;
.super Landroid/app/Fragment;
.source "EditTaskFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# instance fields
.field a:Lcom/android/calendar/hh;

.field b:Lcom/android/calendar/event/fx;

.field public c:Ljava/util/ArrayList;

.field private d:Z

.field private e:Z

.field private f:I

.field private g:Lcom/android/calendar/event/fv;

.field private h:Lcom/android/calendar/hh;

.field private i:Lcom/android/calendar/hh;

.field private j:Lcom/android/calendar/event/fu;

.field private k:Lcom/android/calendar/aq;

.field private l:Landroid/net/Uri;

.field private m:J

.field private n:J

.field private o:Landroid/app/Activity;

.field private p:Lcom/android/calendar/event/ft;

.field private q:Z

.field private r:Landroid/view/inputmethod/InputMethodManager;

.field private s:Landroid/content/Intent;

.field private t:Z

.field private u:Landroid/view/View;

.field private v:Landroid/content/AsyncQueryHandler;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 304
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/android/calendar/event/fq;-><init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V

    .line 305
    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 307
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/fq;->e:Z

    .line 78
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/calendar/event/fq;->f:I

    .line 93
    new-instance v0, Lcom/android/calendar/event/ft;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/ft;-><init>(Lcom/android/calendar/event/fq;)V

    iput-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    .line 95
    iput-boolean v1, p0, Lcom/android/calendar/event/fq;->q:Z

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/fq;->c:Ljava/util/ArrayList;

    .line 103
    iput-boolean v1, p0, Lcom/android/calendar/event/fq;->t:Z

    .line 108
    const-string v0, "com.osp.app.signin"

    iput-object v0, p0, Lcom/android/calendar/event/fq;->w:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_sync_account_type != \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/fq;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fq;->x:Ljava/lang/String;

    .line 112
    const-string v0, "_sync_account NOT LIKE \'task_personal_%\'"

    iput-object v0, p0, Lcom/android/calendar/event/fq;->y:Ljava/lang/String;

    .line 308
    iput-object p1, p0, Lcom/android/calendar/event/fq;->k:Lcom/android/calendar/aq;

    .line 309
    iput-object p3, p0, Lcom/android/calendar/event/fq;->s:Landroid/content/Intent;

    .line 310
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/ft;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 289
    monitor-enter p0

    .line 290
    :try_start_0
    iget v0, p0, Lcom/android/calendar/event/fq;->f:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/fq;->f:I

    .line 291
    iget v0, p0, Lcom/android/calendar/event/fq;->f:I

    if-nez v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/android/calendar/event/fq;->h:Lcom/android/calendar/hh;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/android/calendar/event/fq;->h:Lcom/android/calendar/hh;

    iput-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/hh;)V

    .line 300
    :cond_1
    monitor-exit p0

    .line 301
    return-void

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/android/calendar/event/fq;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/calendar/event/fq;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/fq;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/android/calendar/event/fq;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/fq;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->l:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/fu;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->j:Lcom/android/calendar/event/fu;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/event/fq;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/event/fq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->x:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/event/fq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->y:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const-wide/16 v6, -0x1

    const/4 v2, 0x0

    .line 332
    iput-object v2, p0, Lcom/android/calendar/event/fq;->l:Landroid/net/Uri;

    .line 333
    iput-wide v6, p0, Lcom/android/calendar/event/fq;->m:J

    .line 334
    iput-wide v6, p0, Lcom/android/calendar/event/fq;->n:J

    .line 336
    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-wide v4, v0, Lcom/android/calendar/hh;->b:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    .line 337
    sget-object v0, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-wide v4, v3, Lcom/android/calendar/hh;->b:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fq;->l:Landroid/net/Uri;

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fq;->k:Lcom/android/calendar/aq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/fq;->k:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    if-eqz v0, :cond_3

    .line 342
    iget-object v0, p0, Lcom/android/calendar/event/fq;->k:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/fq;->m:J

    .line 343
    iget-object v0, p0, Lcom/android/calendar/event/fq;->k:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/android/calendar/event/fq;->k:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/fq;->n:J

    .line 347
    :cond_1
    iget-wide v4, p0, Lcom/android/calendar/event/fq;->m:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    iget-wide v4, p0, Lcom/android/calendar/event/fq;->n:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 349
    iget-object v0, p0, Lcom/android/calendar/event/fq;->g:Lcom/android/calendar/event/fv;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/event/fv;->a(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/fq;->m:J

    .line 352
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    if-eqz v0, :cond_3

    .line 353
    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-wide v4, p0, Lcom/android/calendar/event/fq;->m:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    .line 358
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/fq;->l:Landroid/net/Uri;

    if-nez v0, :cond_4

    move v0, v1

    .line 359
    :goto_0
    if-nez v0, :cond_5

    .line 360
    const/16 v0, 0xf

    iput v0, p0, Lcom/android/calendar/event/fq;->f:I

    .line 364
    iget-object v0, p0, Lcom/android/calendar/event/fq;->j:Lcom/android/calendar/event/fu;

    iget-object v3, p0, Lcom/android/calendar/event/fq;->l:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/fv;->a:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/fu;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    :goto_1
    return-void

    .line 358
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 371
    :cond_5
    const/16 v0, 0xc

    iput v0, p0, Lcom/android/calendar/event/fq;->f:I

    .line 372
    const-string v7, " _id desc"

    .line 379
    iget-object v0, p0, Lcom/android/calendar/event/fq;->j:Lcom/android/calendar/event/fu;

    const/16 v1, 0x10

    sget-object v3, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/fv;->a:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/fu;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 386
    const-string v0, " "

    .line 392
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/fq;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 394
    iget-object v0, p0, Lcom/android/calendar/event/fq;->j:Lcom/android/calendar/event/fu;

    const/16 v1, 0x8

    sget-object v3, Lcom/android/calendar/event/fv;->g:Landroid/net/Uri;

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/fu;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 388
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/calendar/event/fq;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic g(Lcom/android/calendar/event/fq;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/event/fq;->h()V

    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 664
    new-instance v0, Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/hh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    .line 666
    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v0, v0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 667
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    .line 672
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v0, v0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 673
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    .line 677
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v1, v1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    iput-object v1, v0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    .line 678
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v1, v1, Lcom/android/calendar/hh;->j:I

    iput v1, v0, Lcom/android/calendar/hh;->j:I

    .line 679
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget v1, v1, Lcom/android/calendar/event/fx;->h:I

    iput v1, v0, Lcom/android/calendar/hh;->d:I

    .line 680
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v1, v1, Lcom/android/calendar/hh;->o:I

    iput v1, v0, Lcom/android/calendar/hh;->o:I

    .line 681
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v1, v1, Lcom/android/calendar/hh;->p:I

    iput v1, v0, Lcom/android/calendar/hh;->p:I

    .line 682
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-wide v2, v1, Lcom/android/calendar/hh;->r:J

    iput-wide v2, v0, Lcom/android/calendar/hh;->r:J

    .line 683
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v1, v1, Lcom/android/calendar/hh;->F:I

    iput v1, v0, Lcom/android/calendar/hh;->F:I

    .line 684
    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v0, v0, Lcom/android/calendar/hh;->F:I

    if-nez v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    .line 687
    :cond_0
    return-void

    .line 669
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v1, v1, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    goto :goto_0

    .line 675
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v1, v1, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic h(Lcom/android/calendar/event/fq;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/android/calendar/event/fq;->d:Z

    return v0
.end method

.method static synthetic i(Lcom/android/calendar/event/fq;)Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->v:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/fv;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->g:Lcom/android/calendar/event/fv;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/event/fq;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/fq;->r:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 624
    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    if-eqz v0, :cond_1

    .line 625
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0}, Lcom/android/calendar/event/fx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/hh;->a(Lcom/android/calendar/hh;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ft;->a(I)V

    .line 627
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    invoke-virtual {v0}, Lcom/android/calendar/event/ft;->run()V

    .line 636
    :goto_0
    return-void

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/ft;->a(I)V

    .line 630
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    invoke-virtual {v0}, Lcom/android/calendar/event/ft;->run()V

    goto :goto_0

    .line 633
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/ft;->a(I)V

    .line 634
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    invoke-virtual {v0}, Lcom/android/calendar/event/ft;->run()V

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 611
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x20

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/event/fq;->q:Z

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0}, Lcom/android/calendar/event/fx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ft;->a(I)V

    .line 614
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    invoke-virtual {v0}, Lcom/android/calendar/event/ft;->run()V

    .line 617
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 643
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ft;->a(I)V

    .line 644
    iget-object v0, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    invoke-virtual {v0}, Lcom/android/calendar/event/ft;->run()V

    .line 645
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 649
    iget-object v0, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    if-nez v0, :cond_1

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 652
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0}, Lcom/android/calendar/event/fx;->a()Z

    .line 655
    iget-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->i:Lcom/android/calendar/hh;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/hh;->a(Lcom/android/calendar/hh;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 656
    invoke-virtual {p0}, Lcom/android/calendar/event/fq;->a()V

    goto :goto_0

    .line 658
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    const v1, 0x7f0f042a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fq;->u:Landroid/view/View;

    .line 693
    :cond_0
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 696
    iget-object v0, p0, Lcom/android/calendar/event/fq;->u:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/android/calendar/event/fq;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 698
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 700
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-nez v1, :cond_0

    .line 701
    iget-object v1, p0, Lcom/android/calendar/event/fq;->u:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 704
    :cond_0
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 603
    const-wide/16 v0, 0x200

    return-wide v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 401
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 402
    iput-object p1, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    .line 404
    new-instance v0, Lcom/android/calendar/event/fv;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/android/calendar/event/fv;-><init>(Landroid/content/Context;Lcom/android/calendar/hh;)V

    iput-object v0, p0, Lcom/android/calendar/event/fq;->g:Lcom/android/calendar/event/fv;

    .line 405
    new-instance v0, Lcom/android/calendar/event/fu;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/fu;-><init>(Lcom/android/calendar/event/fq;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/fq;->j:Lcom/android/calendar/event/fu;

    .line 406
    new-instance v0, Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->s:Landroid/content/Intent;

    invoke-direct {v0, p1, v1}, Lcom/android/calendar/hh;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    .line 407
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/calendar/event/fq;->r:Landroid/view/inputmethod/InputMethodManager;

    .line 409
    invoke-direct {p0}, Lcom/android/calendar/event/fq;->f()V

    .line 410
    new-instance v0, Lcom/android/calendar/event/fr;

    iget-object v1, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/fr;-><init>(Lcom/android/calendar/event/fq;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/fq;->v:Landroid/content/AsyncQueryHandler;

    .line 412
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 436
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 438
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v0, :cond_2

    .line 439
    iget-boolean v0, p0, Lcom/android/calendar/event/fq;->e:Z

    if-eqz v0, :cond_1

    .line 440
    const/4 v0, 0x0

    .line 441
    invoke-virtual {p0}, Lcom/android/calendar/event/fq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 442
    const/4 v0, 0x1

    .line 444
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/fx;->a(Z)V

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->i:Lcom/android/calendar/common/extension/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->i:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 448
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 449
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 450
    new-instance v1, Lcom/android/calendar/event/fs;

    invoke-direct {v1, p0, v0}, Lcom/android/calendar/event/fs;-><init>(Lcom/android/calendar/event/fq;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 459
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 463
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 464
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/fq;->d:Z

    .line 465
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    const v1, 0x7f0a0002

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/fq;->e:Z

    .line 466
    monitor-enter p0

    .line 467
    if-eqz p1, :cond_0

    .line 468
    :try_start_0
    const-string v0, "key_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    const-string v0, "key_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/hh;

    iput-object v0, p0, Lcom/android/calendar/event/fq;->h:Lcom/android/calendar/hh;

    .line 473
    :cond_0
    monitor-exit p0

    .line 474
    return-void

    .line 473
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 420
    iget-boolean v0, p0, Lcom/android/calendar/event/fq;->d:Z

    if-eqz v0, :cond_0

    .line 421
    const v0, 0x7f040045

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 425
    :goto_0
    new-instance v1, Lcom/android/calendar/event/fx;

    iget-object v2, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    iget-object v3, p0, Lcom/android/calendar/event/fq;->p:Lcom/android/calendar/event/ft;

    invoke-direct {v1, v2, v0, v3}, Lcom/android/calendar/event/fx;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/android/calendar/event/fw;)V

    iput-object v1, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    .line 426
    return-object v0

    .line 423
    :cond_0
    const v0, 0x7f040044

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 570
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/hh;)V

    .line 572
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0}, Lcom/android/calendar/event/fx;->f()V

    .line 573
    iput-object v1, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    .line 576
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 578
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 431
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 432
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 3

    .prologue
    .line 316
    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    .line 318
    if-nez p1, :cond_0

    .line 319
    const-string v0, "EditTaskFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hidden: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "EditEventFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ak;

    .line 322
    iget-object v1, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v1, :cond_0

    .line 323
    iget-object v1, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v2, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->o:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/event/fx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/calendar/event/fx;->m:Z

    .line 549
    iget-boolean v0, p0, Lcom/android/calendar/event/fq;->d:Z

    if-eqz v0, :cond_2

    .line 550
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    const v1, 0x7f120168

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 551
    if-eqz v0, :cond_0

    .line 552
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 555
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    const v1, 0x7f12016a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 556
    if-eqz v0, :cond_1

    .line 557
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 560
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    const v1, 0x7f12016b

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 561
    if-eqz v0, :cond_2

    .line 562
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 565
    :cond_2
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 566
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 478
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 479
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0}, Lcom/android/calendar/event/fx;->e()V

    .line 480
    iget-object v0, p0, Lcom/android/calendar/event/fq;->o:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->f()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 481
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-boolean v1, p0, Lcom/android/calendar/event/fq;->t:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/event/fx;->a(ZI)V

    .line 482
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 588
    iget-boolean v0, p0, Lcom/android/calendar/event/fq;->d:Z

    if-eqz v0, :cond_1

    .line 589
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->onSaveInstanceState()Landroid/os/Bundle;

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    if-eqz v0, :cond_1

    .line 592
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/k;->onSaveInstanceState()Landroid/os/Bundle;

    .line 595
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0}, Lcom/android/calendar/event/fx;->a()Z

    .line 596
    const-string v0, "key_model"

    iget-object v1, p0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 598
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 599
    return-void
.end method
