.class Lcom/android/calendar/event/ar;
.super Landroid/content/BroadcastReceiver;
.source "EditEventFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ak;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ak;)V
    .locals 0

    .prologue
    .line 1542
    iput-object p1, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 1546
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1548
    const-string v1, "com.android.provider.calendar.ACCOUNT_DELETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1550
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->n(Lcom/android/calendar/event/ak;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1552
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1553
    const-string v3, "calendar_access_level>=500 AND account_name!=\'docomo\' AND deleted!=1"

    .line 1563
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1566
    invoke-static {v2}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v3

    .line 1570
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1571
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget-boolean v1, v0, Lcom/android/calendar/aq;->m:Z

    .line 1572
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget v0, v0, Lcom/android/calendar/aq;->n:I

    .line 1577
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    iget-object v4, v4, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v4, Lcom/android/calendar/as;->c:J

    const-wide/16 v10, -0x1

    cmp-long v4, v4, v10

    if-eqz v4, :cond_0

    .line 1579
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->c:J

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/hm;->a(JLandroid/database/Cursor;)I

    move-result v0

    move v1, v8

    .line 1584
    :cond_0
    if-eq v0, v6, :cond_6

    invoke-virtual {v3}, Landroid/database/MatrixCursor;->getCount()I

    move-result v4

    if-gt v4, v0, :cond_6

    .line 1588
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v3, v8, v1, v7}, Lcom/android/calendar/event/ay;->a(Landroid/database/Cursor;ZZI)V

    .line 1590
    if-eqz v2, :cond_1

    .line 1591
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1595
    :cond_1
    return-void

    .line 1555
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->C(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1556
    const-string v3, "calendar_access_level>=500 AND calendar_displayName!=\'Direct Share\' AND deleted!=1"

    goto :goto_0

    .line 1557
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ar;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1558
    const-string v3, "calendar_access_level>=500 AND deleted!=1"

    goto :goto_0

    .line 1560
    :cond_4
    const-string v3, "calendar_access_level>=500 AND deleted!=1 AND account_type!=\'com.osp.app.signin\'"

    goto :goto_0

    :cond_5
    move v0, v6

    move v1, v7

    .line 1574
    goto :goto_1

    :cond_6
    move v7, v0

    goto :goto_2
.end method
