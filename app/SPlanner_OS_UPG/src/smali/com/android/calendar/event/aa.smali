.class Lcom/android/calendar/event/aa;
.super Ljava/lang/Object;
.source "EasyEditEventView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/v;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/v;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/android/calendar/event/aa;->a:Lcom/android/calendar/event/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    .line 357
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 358
    iget-object v0, p0, Lcom/android/calendar/event/aa;->a:Lcom/android/calendar/event/v;

    invoke-virtual {v0, p2}, Lcom/android/calendar/event/v;->a(Z)V

    .line 359
    iget-object v0, p0, Lcom/android/calendar/event/aa;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 361
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    if-eqz p2, :cond_1

    .line 364
    iget-object v1, p0, Lcom/android/calendar/event/aa;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 370
    :goto_0
    const/16 v2, 0x8

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 372
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 374
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 376
    :cond_0
    return-void

    .line 367
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/aa;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
