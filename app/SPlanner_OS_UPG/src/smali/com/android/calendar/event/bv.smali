.class Lcom/android/calendar/event/bv;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 3378
    iput-object p1, p0, Lcom/android/calendar/event/bv;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 3381
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 3382
    iget-object v0, p0, Lcom/android/calendar/event/bv;->a:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, p2}, Lcom/android/calendar/event/ay;->b(Z)V

    .line 3383
    iget-object v0, p0, Lcom/android/calendar/event/bv;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 3385
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3386
    iget-object v0, p0, Lcom/android/calendar/event/bv;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 3390
    :cond_0
    if-eqz p2, :cond_1

    .line 3391
    iget-object v0, p0, Lcom/android/calendar/event/bv;->a:Lcom/android/calendar/event/ay;

    const/16 v1, 0x2d0

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->f(Lcom/android/calendar/event/ay;I)I

    .line 3395
    :goto_0
    return-void

    .line 3393
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/bv;->a:Lcom/android/calendar/event/ay;

    const/16 v1, 0xf

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->f(Lcom/android/calendar/event/ay;I)I

    goto :goto_0
.end method
