.class Lcom/android/calendar/event/jt;
.super Ljava/lang/Object;
.source "SelectMapChinaActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectMapChinaActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V
    .locals 0

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1065
    iget-object v1, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/SearchView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1068
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 1069
    packed-switch v0, :pswitch_data_0

    .line 1086
    :goto_0
    return-void

    .line 1071
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->finish()V

    goto :goto_0

    .line 1074
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/SearchView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1075
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-object v1, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->A(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/MapView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/maps/MapView;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/maps/AMap;->getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v1, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->A(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/MapView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/maps/MapView;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/maps/AMap;->getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 1079
    iget-object v1, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/SearchView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v2, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/amap/api/maps/model/LatLng;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 1081
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->y(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/AMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/jt;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->B(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/android/calendar/event/kd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMap;->getMapScreenShot(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V

    goto :goto_0

    .line 1069
    nop

    :pswitch_data_0
    .packed-switch 0x7f1200d4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
