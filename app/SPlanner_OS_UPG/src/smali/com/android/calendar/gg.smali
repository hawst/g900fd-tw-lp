.class public Lcom/android/calendar/gg;
.super Landroid/app/DialogFragment;
.source "SDImportExportDialogFragment.java"


# static fields
.field private static final a:Z


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Lcom/android/calendar/al;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/android/calendar/gg;->a:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/gg;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/calendar/gg;->c:Lcom/android/calendar/al;

    return-object v0
.end method

.method public static a(Landroid/app/FragmentManager;)V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/android/calendar/gg;

    invoke-direct {v0}, Lcom/android/calendar/gg;-><init>()V

    .line 52
    const-string v1, "SDImportExportDialogFragment"

    invoke-virtual {v0, p0, v1}, Lcom/android/calendar/gg;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Lcom/android/calendar/gg;->a:Z

    return v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 58
    iput-object p1, p0, Lcom/android/calendar/gg;->b:Landroid/app/Activity;

    .line 59
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/gg;->c:Lcom/android/calendar/al;

    .line 60
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/16 v4, 0x64

    .line 67
    invoke-virtual {p0}, Lcom/android/calendar/gg;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 71
    new-instance v1, Lcom/android/calendar/gh;

    invoke-virtual {p0}, Lcom/android/calendar/gg;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f04008c

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/android/calendar/gh;-><init>(Lcom/android/calendar/gg;Landroid/content/Context;ILandroid/view/LayoutInflater;)V

    .line 84
    iget-object v0, p0, Lcom/android/calendar/gg;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/gj;->a(Landroid/app/Activity;)V

    .line 87
    invoke-static {}, Lcom/android/calendar/gj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const v0, 0x7f0f0241

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 89
    :cond_0
    invoke-static {}, Lcom/android/calendar/gj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-ge v0, v4, :cond_1

    .line 90
    const v0, 0x7f0f0240

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 93
    :cond_1
    invoke-static {}, Lcom/android/calendar/gj;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    const v0, 0x7f0f01e5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 95
    :cond_2
    invoke-static {}, Lcom/android/calendar/gj;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-ge v0, v4, :cond_3

    .line 96
    const v0, 0x7f0f01e4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 99
    :cond_3
    new-instance v0, Lcom/android/calendar/gi;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/gi;-><init>(Lcom/android/calendar/gg;Landroid/widget/ArrayAdapter;)V

    .line 141
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/gg;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f0160

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
