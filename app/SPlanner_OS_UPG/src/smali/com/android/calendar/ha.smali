.class Lcom/android/calendar/ha;
.super Landroid/content/AsyncQueryHandler;
.source "StickerUtils.java"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 280
    invoke-direct {p0, p1}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 281
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/calendar/ha;->a:Ljava/lang/ref/WeakReference;

    .line 282
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 286
    invoke-super {p0, p1, p2, p3}, Landroid/content/AsyncQueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 288
    const/16 v0, 0xa

    new-array v5, v0, [I

    fill-array-data v5, :array_0

    .line 292
    if-eqz p3, :cond_1

    :try_start_0
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v6

    array-length v0, v5

    if-ge v6, v0, :cond_1

    .line 294
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 295
    add-int/lit8 v1, v6, 0x1

    move v4, v3

    .line 298
    :goto_0
    array-length v0, v5

    sub-int/2addr v0, v6

    if-ge v4, v0, :cond_1

    .line 299
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 301
    const-string v0, "recently"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move v2, v3

    .line 304
    :goto_1
    array-length v0, v5

    if-ge v2, v0, :cond_4

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget v8, v5, v2

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " AND "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "recently"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "=0"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 308
    iget-object v0, p0, Lcom/android/calendar/ha;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v7, v8, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    .line 310
    add-int/lit8 v0, v1, 0x1

    .line 298
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    .line 313
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 315
    goto :goto_1

    .line 320
    :cond_1
    if-eqz p3, :cond_2

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 321
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 324
    :cond_2
    return-void

    .line 320
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 321
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    move v0, v1

    goto :goto_2

    .line 288
    :array_0
    .array-data 4
        0x32
        0x90
        0x1
        0x25
        0x41
        0x61
        0x81
        0x97
        0xa4
        0x18
    .end array-data
.end method
