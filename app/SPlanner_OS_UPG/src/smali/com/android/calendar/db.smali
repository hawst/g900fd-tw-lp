.class public Lcom/android/calendar/db;
.super Landroid/widget/BaseAdapter;
.source "DrawerAdapter.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/List;

.field private c:Lcom/android/calendar/dc;

.field private d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/android/calendar/db;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/db;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 66
    iput-object p1, p0, Lcom/android/calendar/db;->d:Landroid/content/Context;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    .line 69
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 71
    invoke-static {}, Lcom/android/calendar/hj;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/android/calendar/hj;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 72
    :cond_1
    new-instance v0, Lcom/android/calendar/dc;

    invoke-direct {v0, p1}, Lcom/android/calendar/dc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/db;->c:Lcom/android/calendar/dc;

    .line 73
    iget-object v0, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    iget-object v3, p0, Lcom/android/calendar/db;->c:Lcom/android/calendar/dc;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_2
    const v0, 0x7f090008

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 77
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    .line 78
    iget-object v6, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    new-instance v7, Lcom/android/calendar/df;

    add-int/lit8 v2, v1, 0x1

    invoke-direct {v7, p1, v5, v1}, Lcom/android/calendar/df;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 80
    :cond_3
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 6

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dd;

    .line 130
    int-to-long v2, p1

    invoke-interface {v0}, Lcom/android/calendar/dd;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 131
    iget-object v1, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 134
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/calendar/db;->c:Lcom/android/calendar/dc;

    if-nez v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/db;->c:Lcom/android/calendar/dc;

    invoke-virtual {v0}, Lcom/android/calendar/dc;->d()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/calendar/db;->c:Lcom/android/calendar/dc;

    if-nez v0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/db;->c:Lcom/android/calendar/dc;

    invoke-virtual {v0}, Lcom/android/calendar/dc;->e()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/android/calendar/db;->c:Lcom/android/calendar/dc;

    if-nez v0, :cond_0

    .line 126
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/db;->c:Lcom/android/calendar/dc;

    invoke-virtual {v0}, Lcom/android/calendar/dc;->f()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dd;

    invoke-interface {v0}, Lcom/android/calendar/dd;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dd;

    invoke-interface {v0}, Lcom/android/calendar/dd;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dd;

    invoke-interface {v0}, Lcom/android/calendar/dd;->a()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/calendar/db;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dd;

    invoke-interface {v0, p2, p3}, Lcom/android/calendar/dd;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 84
    invoke-static {}, Lcom/android/calendar/de;->values()[Lcom/android/calendar/de;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
