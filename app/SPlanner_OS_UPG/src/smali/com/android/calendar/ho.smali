.class Lcom/android/calendar/ho;
.super Ljava/lang/Object;
.source "WeatherSettingDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/hm;


# direct methods
.method constructor <init>(Lcom/android/calendar/hm;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/android/calendar/ho;->a:Lcom/android/calendar/hm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/calendar/ho;->a:Lcom/android/calendar/hm;

    invoke-static {v0}, Lcom/android/calendar/hm;->a(Lcom/android/calendar/hm;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/android/calendar/ho;->a:Lcom/android/calendar/hm;

    invoke-static {v0}, Lcom/android/calendar/hm;->b(Lcom/android/calendar/hm;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_weather_settings"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 121
    :goto_0
    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 124
    :cond_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/ho;->a:Lcom/android/calendar/hm;

    invoke-static {v0}, Lcom/android/calendar/hm;->b(Lcom/android/calendar/hm;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_weather_settings"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method
