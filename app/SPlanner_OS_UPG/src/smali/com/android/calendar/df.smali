.class Lcom/android/calendar/df;
.super Ljava/lang/Object;
.source "DrawerAdapter.java"

# interfaces
.implements Lcom/android/calendar/dd;


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/df;->a:Landroid/view/LayoutInflater;

    .line 317
    iput-object p2, p0, Lcom/android/calendar/df;->b:Ljava/lang/String;

    .line 318
    iput p3, p0, Lcom/android/calendar/df;->c:I

    .line 319
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lcom/android/calendar/de;->b:Lcom/android/calendar/de;

    invoke-virtual {v0}, Lcom/android/calendar/de;->ordinal()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 324
    if-nez p1, :cond_0

    .line 325
    iget-object v0, p0, Lcom/android/calendar/df;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f040036

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 329
    :cond_0
    const v0, 0x7f1200c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 330
    iget-object v1, p0, Lcom/android/calendar/df;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    return-object p1
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/android/calendar/df;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 346
    iget v0, p0, Lcom/android/calendar/df;->c:I

    int-to-long v0, v0

    return-wide v0
.end method
