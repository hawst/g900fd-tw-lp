.class Lcom/android/calendar/cm;
.super Landroid/os/AsyncTask;
.source "DeleteEventHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/cj;

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/ref/WeakReference;

.field private d:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Lcom/android/calendar/cj;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 446
    iput-object p1, p0, Lcom/android/calendar/cm;->a:Lcom/android/calendar/cj;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 441
    const-class v0, Lcom/android/calendar/cm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/cm;->b:Ljava/lang/String;

    .line 447
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    .line 448
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    .line 449
    return-void
.end method

.method private a(Ljava/util/ArrayList;I)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 507
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 510
    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 521
    :goto_0
    return v0

    :cond_0
    move v3, v1

    move v2, p2

    .line 514
    :goto_1
    if-ge v3, v4, :cond_1

    .line 515
    new-array v0, v7, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/calendar/cm;->publishProgress([Ljava/lang/Object;)V

    .line 516
    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v5, v0, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    .line 514
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 519
    :cond_1
    invoke-direct {p0, v7}, Lcom/android/calendar/cm;->a(Z)V

    move v0, v2

    .line 521
    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;ILandroid/net/Uri;Ljava/lang/String;Z)I
    .locals 10

    .prologue
    .line 537
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 539
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 540
    const/4 v0, 0x0

    move v4, v0

    move v2, p2

    :goto_0
    if-ge v4, v5, :cond_4

    .line 541
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 543
    const/16 v0, 0x32

    sub-int v1, v5, v4

    if-ge v0, v1, :cond_0

    const/16 v0, 0x32

    move v1, v0

    .line 547
    :goto_1
    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v1, :cond_2

    .line 548
    if-nez v3, :cond_1

    .line 549
    const-string v0, " ( "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    :goto_3
    add-int v0, v4, v3

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 554
    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 547
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 543
    :cond_0
    sub-int v0, v5, v4

    move v1, v0

    goto :goto_1

    .line 551
    :cond_1
    const-string v0, ", "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 556
    :cond_2
    const-string v0, " ) "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    .line 559
    iget-object v0, p0, Lcom/android/calendar/cm;->b:Ljava/lang/String;

    const-string v1, "Activity ref is null in deleteEvents()"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    const/4 v0, 0x0

    .line 568
    :goto_4
    return v0

    .line 563
    :cond_3
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/calendar/cm;->publishProgress([Ljava/lang/Object;)V

    .line 564
    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " in "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, p3, v1, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    .line 566
    invoke-direct {p0, p5}, Lcom/android/calendar/cm;->a(Z)V

    .line 540
    add-int/lit8 v0, v4, 0x32

    move v4, v0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 568
    goto :goto_4
.end method

.method static synthetic a(Lcom/android/calendar/cm;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 572
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-nez v0, :cond_0

    .line 580
    :goto_0
    return-void

    .line 575
    :cond_0
    if-eqz p1, :cond_1

    .line 576
    const-string v0, "GATE"

    const-string v1, "<GATE-M>TASK_DELETED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 578
    :cond_1
    const-string v0, "GATE"

    const-string v1, "<GATE-M>EVENT_DELETED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Long;)Ljava/lang/Integer;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 463
    if-nez p1, :cond_0

    .line 464
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 494
    :goto_0
    return-object v0

    .line 466
    :cond_0
    array-length v0, p1

    .line 467
    iget-object v1, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 470
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 471
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 472
    array-length v3, p1

    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, p1, v0

    .line 473
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-gez v5, :cond_1

    .line 474
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    neg-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 476
    :cond_1
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 482
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 483
    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "_id"

    move-object v0, p0

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/cm;->a(Ljava/util/ArrayList;ILandroid/net/Uri;Ljava/lang/String;Z)I

    move-result v0

    add-int/2addr v2, v0

    .line 486
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 488
    invoke-direct {p0, v6, v2}, Lcom/android/calendar/cm;->a(Ljava/util/ArrayList;I)I

    move-result v0

    add-int/2addr v2, v0

    .line 491
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/cm;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Completed to delete the events and tasks ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 591
    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/cm;->b:Ljava/lang/String;

    const-string v1, "Activity ref is null in onPostExecute()"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    :cond_1
    :goto_0
    return-void

    .line 596
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 597
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getMax()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 598
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 599
    iput-object v2, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    .line 602
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/cm;->a:Lcom/android/calendar/cj;

    invoke-static {v0}, Lcom/android/calendar/cj;->e(Lcom/android/calendar/cj;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 603
    iget-object v0, p0, Lcom/android/calendar/cm;->a:Lcom/android/calendar/cj;

    invoke-static {v0}, Lcom/android/calendar/cj;->e(Lcom/android/calendar/cj;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 604
    iget-object v0, p0, Lcom/android/calendar/cm;->a:Lcom/android/calendar/cj;

    invoke-static {v0, v2}, Lcom/android/calendar/cj;->a(Lcom/android/calendar/cj;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 607
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/cm;->a:Lcom/android/calendar/cj;

    invoke-static {v0}, Lcom/android/calendar/cj;->g(Lcom/android/calendar/cj;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 608
    iget-object v0, p0, Lcom/android/calendar/cm;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/android/calendar/cn;

    invoke-direct {v1, p0}, Lcom/android/calendar/cn;-><init>(Lcom/android/calendar/cm;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 584
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 587
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 440
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/android/calendar/cm;->a([Ljava/lang/Long;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 440
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/cm;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 453
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    const v1, 0x7f0f01cf

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 454
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 455
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 456
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 457
    iget-object v0, p0, Lcom/android/calendar/cm;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 458
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 459
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 440
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/cm;->a([Ljava/lang/Integer;)V

    return-void
.end method
