.class Lcom/android/calendar/sticker/n;
.super Landroid/content/AsyncQueryHandler;
.source "StickerDeleteFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/sticker/c;


# direct methods
.method public constructor <init>(Lcom/android/calendar/sticker/c;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    .line 172
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 173
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 177
    if-nez p3, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/c;->isAdded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 181
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->c(Lcom/android/calendar/sticker/c;)V

    .line 189
    iget-object v0, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    iget-object v1, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v1, p3}, Lcom/android/calendar/sticker/c;->a(Lcom/android/calendar/sticker/c;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    .line 190
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 191
    iget-object v0, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 192
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->d(Lcom/android/calendar/sticker/c;)Landroid/widget/ListView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 194
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    iget-object v1, p0, Lcom/android/calendar/sticker/n;->a:Lcom/android/calendar/sticker/c;

    iget-object v1, v1, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/sticker/a;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
