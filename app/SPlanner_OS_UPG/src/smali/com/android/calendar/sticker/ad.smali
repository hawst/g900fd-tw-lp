.class Lcom/android/calendar/sticker/ad;
.super Landroid/content/AsyncQueryHandler;
.source "StickerDownloadListFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/sticker/r;


# direct methods
.method public constructor <init>(Lcom/android/calendar/sticker/r;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 421
    iput-object p1, p0, Lcom/android/calendar/sticker/ad;->a:Lcom/android/calendar/sticker/r;

    .line 422
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 423
    return-void
.end method


# virtual methods
.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 427
    check-cast p2, Ljava/lang/Integer;

    .line 428
    iget-object v0, p0, Lcom/android/calendar/sticker/ad;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->b(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 429
    iget-object v0, p0, Lcom/android/calendar/sticker/ad;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->b(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/android/calendar/a/a/b;->a:J

    .line 430
    iget-object v0, p0, Lcom/android/calendar/sticker/ad;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->b(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    invoke-virtual {v0, v4}, Lcom/android/calendar/a/a/b;->a(I)V

    .line 431
    iget-object v0, p0, Lcom/android/calendar/sticker/ad;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->e(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/sticker/ad;->a:Lcom/android/calendar/sticker/r;

    iget v1, v1, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    .line 432
    iget-object v0, p0, Lcom/android/calendar/sticker/ad;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->j(Lcom/android/calendar/sticker/r;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/sticker/ad;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v1}, Lcom/android/calendar/sticker/r;->j(Lcom/android/calendar/sticker/r;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f015a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 434
    :cond_0
    return-void
.end method
