.class final Lcom/android/calendar/sticker/j;
.super Lcom/android/calendar/common/a/a;
.source "StickerDeleteFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/sticker/c;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/calendar/sticker/c;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 408
    iput-object p1, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    .line 409
    invoke-direct {p0, p2}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;)V

    .line 410
    iput-object p2, p0, Lcom/android/calendar/sticker/j;->d:Landroid/content/Context;

    .line 411
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/c;->a()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 461
    const v0, 0x7f120324

    const v1, 0x7f0f0398

    invoke-interface {p2, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 462
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/sticker/a;->a(Z)V

    .line 421
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/sticker/a;->a(Z)V

    .line 426
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/a;->notifyDataSetChanged()V

    .line 441
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/a;->b()Z

    move-result v0

    return v0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->d:Landroid/content/Context;

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->c:Landroid/view/Menu;

    const v1, 0x7f120324

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/calendar/sticker/j;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 436
    return-void

    .line 435
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 450
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f120324

    if-ne v0, v1, :cond_2

    .line 451
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->f(Lcom/android/calendar/sticker/c;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->f(Lcom/android/calendar/sticker/c;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 452
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->g(Lcom/android/calendar/sticker/c;)V

    .line 454
    :cond_1
    const/4 v0, 0x1

    .line 456
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 466
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 467
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    iget-object v1, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v1}, Lcom/android/calendar/sticker/c;->b(Lcom/android/calendar/sticker/c;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/sticker/a;->a(Landroid/view/ActionMode;)V

    .line 468
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    .line 478
    iget-object v0, p0, Lcom/android/calendar/sticker/j;->a:Lcom/android/calendar/sticker/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/sticker/c;->a(Lcom/android/calendar/sticker/c;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 479
    invoke-super {p0, p1}, Lcom/android/calendar/common/a/a;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 480
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 473
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
