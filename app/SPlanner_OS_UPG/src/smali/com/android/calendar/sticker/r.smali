.class public Lcom/android/calendar/sticker/r;
.super Landroid/app/DialogFragment;
.source "StickerDownloadListFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/android/calendar/a/a/a/d;
.implements Lcom/android/calendar/a/a/a/g;
.implements Lcom/android/calendar/a/a/a/j;


# static fields
.field public static a:I

.field public static b:I

.field public static c:I

.field public static d:I

.field private static l:I


# instance fields
.field public e:I

.field private f:Landroid/view/View;

.field private g:Landroid/widget/HorizontalScrollView;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/ListView;

.field private j:Landroid/view/GestureDetector;

.field private k:I

.field private m:I

.field private n:Landroid/widget/LinearLayout;

.field private o:Ljava/util/ArrayList;

.field private p:Ljava/util/ArrayList;

.field private q:Ljava/util/ArrayList;

.field private r:Lcom/android/calendar/sticker/ad;

.field private s:[Ljava/lang/String;

.field private t:Landroid/app/Activity;

.field private u:Landroid/widget/ProgressBar;

.field private v:Landroid/app/ProgressDialog;

.field private w:Lcom/android/calendar/sticker/x;

.field private x:Landroid/app/AlertDialog;

.field private y:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/sticker/r;->l:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/sticker/r;-><init>(I)V

    .line 113
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 115
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 81
    iput-object v1, p0, Lcom/android/calendar/sticker/r;->g:Landroid/widget/HorizontalScrollView;

    .line 86
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/calendar/sticker/r;->k:I

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/sticker/r;->e:I

    .line 109
    iput-object v1, p0, Lcom/android/calendar/sticker/r;->x:Landroid/app/AlertDialog;

    .line 361
    new-instance v0, Lcom/android/calendar/sticker/u;

    invoke-direct {v0, p0}, Lcom/android/calendar/sticker/u;-><init>(Lcom/android/calendar/sticker/r;)V

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->y:Landroid/content/DialogInterface$OnCancelListener;

    .line 116
    add-int/lit8 v0, p1, -0x2

    iput v0, p0, Lcom/android/calendar/sticker/r;->e:I

    .line 117
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget v0, p0, Lcom/android/calendar/sticker/r;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/sticker/r;->e:I

    .line 120
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/sticker/r;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/android/calendar/sticker/r;->m:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/sticker/r;I)I
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/android/calendar/sticker/r;->m:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/sticker/r;Landroid/view/GestureDetector;)Landroid/view/GestureDetector;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/android/calendar/sticker/r;->j:Landroid/view/GestureDetector;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/sticker/r;Lcom/android/calendar/sticker/x;)Lcom/android/calendar/sticker/x;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/android/calendar/sticker/r;->w:Lcom/android/calendar/sticker/x;

    return-object p1
.end method

.method static synthetic b(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/sticker/r;I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/calendar/sticker/r;->c(I)V

    return-void
.end method

.method static synthetic c(Lcom/android/calendar/sticker/r;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->n:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private c(I)V
    .locals 6

    .prologue
    .line 403
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 405
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    .line 406
    iget-object v1, v0, Lcom/android/calendar/a/a/b;->f:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/calendar/a/a/b;->f:Ljava/lang/String;

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, v0, Lcom/android/calendar/a/a/b;->f:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 407
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "filepath LIKE \'%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\' AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "packageId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "downloaded"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 409
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->r:Lcom/android/calendar/sticker/ad;

    if-nez v0, :cond_0

    .line 410
    new-instance v0, Lcom/android/calendar/sticker/ad;

    iget-object v1, p0, Lcom/android/calendar/sticker/r;->t:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/sticker/ad;-><init>(Lcom/android/calendar/sticker/r;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->r:Lcom/android/calendar/sticker/ad;

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->r:Lcom/android/calendar/sticker/ad;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/sticker/ad;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 417
    :cond_1
    return-void

    .line 413
    :catchall_0
    move-exception v0

    throw v0
.end method

.method static synthetic d(Lcom/android/calendar/sticker/r;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->u:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f()I
    .locals 1

    .prologue
    .line 75
    sget v0, Lcom/android/calendar/sticker/r;->l:I

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/sticker/r;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->q:Ljava/util/ArrayList;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 755
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 757
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 762
    :goto_0
    return-void

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 760
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic h(Lcom/android/calendar/sticker/r;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/sticker/r;)Lcom/android/calendar/sticker/x;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->w:Lcom/android/calendar/sticker/x;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/sticker/r;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->t:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/sticker/r;)Landroid/view/GestureDetector;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->j:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/sticker/r;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->x:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/sticker/r;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/android/calendar/sticker/r;->k:I

    return v0
.end method

.method static synthetic n(Lcom/android/calendar/sticker/r;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->v:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic o(Lcom/android/calendar/sticker/r;)Landroid/content/DialogInterface$OnCancelListener;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->y:Landroid/content/DialogInterface$OnCancelListener;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 869
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->v:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 870
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 871
    iget-object v1, p0, Lcom/android/calendar/sticker/r;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 873
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 875
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->v:Landroid/app/ProgressDialog;

    .line 877
    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/a/a/b;->a(I)V

    .line 373
    invoke-static {}, Lcom/android/calendar/a/a/a/b;->b()Lcom/android/calendar/a/a/a/b;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    iget-object v0, v0, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/android/calendar/a/a/a/b;->a(Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    .line 377
    :cond_0
    return-void
.end method

.method public a(ILcom/android/calendar/a/a/a/a;)V
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x0

    const/16 v2, 0x8

    const/4 v10, 0x0

    .line 606
    if-nez p1, :cond_8

    .line 607
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 609
    instance-of v0, p2, Lcom/android/calendar/a/a/g;

    if-eqz v0, :cond_6

    move-object v6, p2

    .line 610
    check-cast v6, Lcom/android/calendar/a/a/g;

    .line 611
    invoke-virtual {p2}, Lcom/android/calendar/a/a/a/a;->j()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/ArrayList;

    .line 612
    if-eqz v7, :cond_4

    .line 613
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 614
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v10

    move v9, v10

    .line 616
    :goto_0
    if-ge v9, v11, :cond_3

    .line 617
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/h;

    .line 619
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "splanner_download_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/android/calendar/ak;->a:[Ljava/lang/String;

    iget v4, v6, Lcom/android/calendar/a/a/g;->a:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v0, Lcom/android/calendar/a/a/h;->f:Ljava/util/HashMap;

    const-string v4, "en"

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".png"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 621
    new-instance v12, Lcom/android/calendar/a/a/b;

    invoke-direct {v12}, Lcom/android/calendar/a/a/b;-><init>()V

    .line 622
    iget-object v3, v0, Lcom/android/calendar/a/a/h;->a:Ljava/lang/String;

    iput-object v3, v12, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    .line 623
    sget v3, Lcom/android/calendar/sticker/r;->l:I

    iput v3, v12, Lcom/android/calendar/a/a/b;->c:I

    .line 624
    iget-wide v4, v0, Lcom/android/calendar/a/a/h;->d:J

    iput-wide v4, v12, Lcom/android/calendar/a/a/b;->d:J

    .line 625
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/android/calendar/gx;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v12, Lcom/android/calendar/a/a/b;->f:Ljava/lang/String;

    .line 626
    const-wide/16 v4, 0x0

    iput-wide v4, v12, Lcom/android/calendar/a/a/b;->e:J

    .line 627
    iput-object v0, v12, Lcom/android/calendar/a/a/b;->h:Ljava/lang/Object;

    .line 632
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "filepath LIKE \'%"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "packageId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v12, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 634
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->t:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 637
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 638
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 639
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v12, Lcom/android/calendar/a/a/b;->a:J

    .line 640
    const/4 v0, 0x2

    invoke-virtual {v12, v0}, Lcom/android/calendar/a/a/b;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 643
    :cond_0
    if-eqz v1, :cond_1

    .line 644
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 649
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 616
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto/16 :goto_0

    .line 643
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_2

    .line 644
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 645
    :cond_2
    throw v0

    .line 651
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 653
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/sticker/r;->g()V

    .line 654
    iget v0, p0, Lcom/android/calendar/sticker/r;->e:I

    if-gez v0, :cond_7

    .line 655
    iput v10, p0, Lcom/android/calendar/sticker/r;->e:I

    .line 659
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    .line 687
    :cond_6
    :goto_3
    return-void

    .line 656
    :cond_7
    iget v0, p0, Lcom/android/calendar/sticker/r;->e:I

    iget v1, p0, Lcom/android/calendar/sticker/r;->k:I

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_5

    .line 657
    iget v0, p0, Lcom/android/calendar/sticker/r;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/sticker/r;->e:I

    goto :goto_2

    .line 663
    :cond_8
    const/4 v0, -0x7

    if-ne p1, v0, :cond_9

    .line 664
    instance-of v0, p2, Lcom/android/calendar/a/a/g;

    if-eqz v0, :cond_6

    .line 665
    check-cast p2, Lcom/android/calendar/a/a/g;

    .line 667
    iget v0, p2, Lcom/android/calendar/a/a/g;->a:I

    iget v1, p0, Lcom/android/calendar/sticker/r;->e:I

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_6

    .line 668
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 669
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 670
    invoke-direct {p0}, Lcom/android/calendar/sticker/r;->g()V

    .line 671
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    goto :goto_3

    .line 676
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 677
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 678
    if-eqz v0, :cond_6

    .line 679
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0406

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 680
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 681
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_6

    .line 682
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_3

    .line 643
    :catchall_1
    move-exception v0

    goto/16 :goto_1
.end method

.method public a(ILcom/android/calendar/a/a/a;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 710
    if-nez p1, :cond_2

    .line 711
    invoke-virtual {p2}, Lcom/android/calendar/a/a/a;->d()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 713
    iget-object v1, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 714
    :goto_0
    if-ge v1, v2, :cond_0

    .line 715
    invoke-virtual {p2}, Lcom/android/calendar/a/a/a;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    iget-object v0, v0, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 716
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 717
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 718
    const-string v3, "file_path"

    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    iget-object v0, v0, Lcom/android/calendar/a/a/b;->f:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    const-string v3, "packageId"

    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    iget-object v0, v0, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 720
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->t:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 751
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    .line 752
    return-void

    .line 714
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 727
    :cond_2
    const/4 v1, -0x8

    if-eq p1, v1, :cond_0

    .line 730
    const/16 v1, -0x9

    if-ne p1, v1, :cond_3

    .line 731
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 732
    const v2, 0x7f0f0409

    new-array v3, v5, [Ljava/lang/Object;

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {p0, v2, v3}, Lcom/android/calendar/sticker/r;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 733
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 737
    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 738
    if-eqz v0, :cond_0

    .line 739
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0404

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 740
    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 741
    invoke-static {v0}, Lcom/android/calendar/hj;->x(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 742
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0406

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 743
    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 744
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 745
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method

.method public a(IZ)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 330
    invoke-static {}, Lcom/android/calendar/a/a/a/b;->b()Lcom/android/calendar/a/a/a/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/calendar/a/a/a/b;->a(Lcom/android/calendar/a/a/a/d;)V

    .line 331
    if-nez p2, :cond_1

    .line 332
    iget v0, p0, Lcom/android/calendar/sticker/r;->m:I

    sget v1, Lcom/android/calendar/sticker/r;->l:I

    if-ne v0, v1, :cond_0

    .line 333
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    invoke-virtual {v0, v3}, Lcom/android/calendar/a/a/b;->a(I)V

    .line 336
    invoke-static {}, Lcom/android/calendar/a/a/a/b;->b()Lcom/android/calendar/a/a/a/b;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/a/a/c;

    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    invoke-direct {v2, v0}, Lcom/android/calendar/a/a/c;-><init>(Lcom/android/calendar/a/a/b;)V

    invoke-virtual {v1, v2}, Lcom/android/calendar/a/a/a/b;->a(Lcom/android/calendar/a/a/a;)V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    .line 359
    return-void

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v7

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/android/calendar/a/a/b;

    .line 344
    invoke-virtual {v6}, Lcom/android/calendar/a/a/b;->a()I

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/android/calendar/sticker/r;->m:I

    sget v2, Lcom/android/calendar/sticker/r;->l:I

    if-ne v0, v2, :cond_3

    .line 346
    if-nez v1, :cond_2

    .line 347
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0173

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/android/calendar/sticker/r;->y:Landroid/content/DialogInterface$OnCancelListener;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->v:Landroid/app/ProgressDialog;

    .line 350
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->v:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    move v1, v3

    .line 353
    :cond_2
    invoke-virtual {v6, v3}, Lcom/android/calendar/a/a/b;->a(I)V

    .line 354
    invoke-static {}, Lcom/android/calendar/a/a/a/b;->b()Lcom/android/calendar/a/a/a/b;

    move-result-object v0

    new-instance v2, Lcom/android/calendar/a/a/c;

    invoke-direct {v2, v6}, Lcom/android/calendar/a/a/c;-><init>(Lcom/android/calendar/a/a/b;)V

    invoke-virtual {v0, v2}, Lcom/android/calendar/a/a/a/b;->a(Lcom/android/calendar/a/a/a;)V

    :cond_3
    move v0, v1

    move v1, v0

    .line 356
    goto :goto_0
.end method

.method public a(JJLcom/android/calendar/a/a/a;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 692
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    .line 693
    invoke-virtual {p5}, Lcom/android/calendar/a/a/a;->c()J

    move-result-wide v4

    .line 694
    invoke-virtual {p5}, Lcom/android/calendar/a/a/a;->e()J

    move-result-wide v6

    move v1, v2

    .line 695
    :goto_0
    if-ge v1, v3, :cond_0

    .line 696
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 697
    const v8, 0x7f1202c2

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 698
    invoke-virtual {p5}, Lcom/android/calendar/a/a/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getTag()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 699
    const-wide/16 v8, 0x64

    mul-long/2addr v4, v8

    div-long/2addr v4, v6

    long-to-int v1, v4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 700
    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 701
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->invalidate()V

    .line 705
    :cond_0
    return-void

    .line 695
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected b()V
    .locals 5

    .prologue
    .line 239
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 240
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 241
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 242
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 244
    if-eqz v0, :cond_0

    .line 245
    iget v4, p0, Lcom/android/calendar/sticker/r;->e:I

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v4, v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->g:Landroid/widget/HorizontalScrollView;

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, Landroid/widget/HorizontalScrollView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 247
    invoke-virtual {v3}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    .line 240
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 253
    :cond_1
    return-void
.end method

.method public b(I)V
    .locals 3

    .prologue
    const v2, 0x7f0f0143

    .line 380
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 383
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 384
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 386
    const v1, 0x7f0f0436

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 387
    new-instance v1, Lcom/android/calendar/sticker/v;

    invoke-direct {v1, p0, p1}, Lcom/android/calendar/sticker/v;-><init>(Lcom/android/calendar/sticker/r;I)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 393
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/android/calendar/sticker/w;

    invoke-direct {v2, p0}, Lcom/android/calendar/sticker/w;-><init>(Lcom/android/calendar/sticker/r;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 399
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->x:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method protected c()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v4, 0x0

    .line 256
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    .line 258
    invoke-static {v4}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 259
    const-string v0, "sec-roboto-light"

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v7

    .line 260
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 261
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    move v3, v4

    .line 265
    :goto_0
    if-ge v3, v5, :cond_5

    .line 266
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 267
    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 269
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 270
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v2, p0, Lcom/android/calendar/sticker/r;->k:I

    if-ge v0, v2, :cond_2

    .line 271
    const v0, 0x7f1202b8

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 272
    const v2, 0x7f1202b9

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 273
    iget v11, p0, Lcom/android/calendar/sticker/r;->e:I

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v11, v1, :cond_1

    .line 274
    iget-object v1, p0, Lcom/android/calendar/sticker/r;->g:Landroid/widget/HorizontalScrollView;

    const/4 v11, 0x0

    invoke-virtual {v1, v11, v10}, Landroid/widget/HorizontalScrollView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 275
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v10, v1}, Landroid/view/View;->setAlpha(F)V

    .line 276
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 277
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 278
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 265
    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 280
    :cond_1
    const v1, 0x3f333333    # 0.7f

    invoke-virtual {v10, v1}, Landroid/view/View;->setAlpha(F)V

    .line 281
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 282
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 283
    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 286
    :cond_2
    iget v2, p0, Lcom/android/calendar/sticker/r;->e:I

    move-object v0, v1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v11, p0, Lcom/android/calendar/sticker/r;->k:I

    sub-int/2addr v0, v11

    if-eq v2, v0, :cond_3

    iget v0, p0, Lcom/android/calendar/sticker/r;->e:I

    add-int/lit8 v0, v0, 0x1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/android/calendar/sticker/r;->k:I

    sub-int/2addr v1, v2

    if-ne v0, v1, :cond_4

    .line 288
    :cond_3
    invoke-virtual {v10, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 290
    :cond_4
    invoke-virtual {v10, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 295
    :cond_5
    return-void
.end method

.method protected d()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 298
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    move v2, v3

    .line 299
    :goto_0
    if-ge v2, v4, :cond_0

    .line 300
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 301
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 302
    if-eqz v1, :cond_1

    move-object v0, v1

    .line 303
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v6, p0, Lcom/android/calendar/sticker/r;->k:I

    if-ge v0, v6, :cond_1

    .line 304
    iget v0, p0, Lcom/android/calendar/sticker/r;->e:I

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 305
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->g:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 311
    :cond_0
    return-void

    .line 299
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    .line 323
    invoke-virtual {v0}, Lcom/android/calendar/a/a/b;->a()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 324
    const/4 v0, 0x0

    .line 326
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 125
    iput-object p1, p0, Lcom/android/calendar/sticker/r;->t:Landroid/app/Activity;

    .line 126
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 130
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    move-object v0, v4

    .line 235
    :goto_0
    return-object v0

    .line 135
    :cond_0
    const v0, 0x7f0400a3

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->f:Landroid/view/View;

    .line 136
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->f:Landroid/view/View;

    const v1, 0x7f12029d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->g:Landroid/widget/HorizontalScrollView;

    .line 137
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->f:Landroid/view/View;

    const v1, 0x7f12029e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    .line 138
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->f:Landroid/view/View;

    const v1, 0x7f1202c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->u:Landroid/widget/ProgressBar;

    .line 139
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->s:[Ljava/lang/String;

    .line 141
    new-instance v5, Lcom/android/calendar/sticker/s;

    invoke-direct {v5, p0}, Lcom/android/calendar/sticker/s;-><init>(Lcom/android/calendar/sticker/r;)V

    move v2, v3

    .line 173
    :goto_1
    iget v0, p0, Lcom/android/calendar/sticker/r;->k:I

    if-ge v2, v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->t:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04009f

    iget-object v6, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v6, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 175
    const v1, 0x7f1202b8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 177
    iget-object v6, p0, Lcom/android/calendar/sticker/r;->s:[Ljava/lang/String;

    add-int/lit8 v7, v2, 0x2

    aget-object v6, v6, v7

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 180
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v1, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 173
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/sticker/t;

    invoke-direct {v1, p0}, Lcom/android/calendar/sticker/t;-><init>(Lcom/android/calendar/sticker/r;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 196
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->f:Landroid/view/View;

    const v1, 0x7f1202c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    .line 198
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/android/calendar/sticker/r;->t:Landroid/app/Activity;

    new-instance v2, Lcom/android/calendar/sticker/ac;

    invoke-direct {v2, p0, v4}, Lcom/android/calendar/sticker/ac;-><init>(Lcom/android/calendar/sticker/r;Lcom/android/calendar/sticker/s;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->j:Landroid/view/GestureDetector;

    .line 199
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 201
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->f:Landroid/view/View;

    const v1, 0x7f1202c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->n:Landroid/widget/LinearLayout;

    .line 202
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 204
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 205
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 206
    iput-object v4, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    .line 208
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    .line 209
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 210
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 211
    iput-object v4, p0, Lcom/android/calendar/sticker/r;->q:Ljava/util/ArrayList;

    .line 213
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/sticker/r;->q:Ljava/util/ArrayList;

    move v0, v3

    .line 214
    :goto_2
    iget v1, p0, Lcom/android/calendar/sticker/r;->k:I

    if-ge v0, v1, :cond_4

    .line 215
    new-instance v1, Lcom/android/calendar/sticker/aa;

    iget-object v2, p0, Lcom/android/calendar/sticker/r;->p:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/sticker/aa;-><init>(Lcom/android/calendar/sticker/r;Ljava/util/ArrayList;)V

    .line 216
    iget-object v2, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    add-int/lit8 v1, v0, 0x1

    .line 219
    new-instance v2, Lcom/android/calendar/a/a/g;

    invoke-direct {v2, v1}, Lcom/android/calendar/a/a/g;-><init>(I)V

    .line 220
    iget-object v1, p0, Lcom/android/calendar/sticker/r;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 223
    :cond_4
    iget v0, p0, Lcom/android/calendar/sticker/r;->e:I

    if-gez v0, :cond_5

    .line 224
    iput v3, p0, Lcom/android/calendar/sticker/r;->e:I

    .line 226
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 227
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->i:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 228
    iput v3, p0, Lcom/android/calendar/sticker/r;->m:I

    .line 230
    iget v0, p0, Lcom/android/calendar/sticker/r;->m:I

    if-nez v0, :cond_6

    .line 231
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 232
    invoke-static {}, Lcom/android/calendar/a/a/a/h;->b()Lcom/android/calendar/a/a/a/h;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/sticker/r;->q:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/a/a;

    invoke-virtual {v1, v0, p0}, Lcom/android/calendar/a/a/a/h;->a(Lcom/android/calendar/a/a/a/a;Lcom/android/calendar/a/a/a/j;)V

    .line 234
    :cond_6
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->c()V

    .line 235
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->f:Landroid/view/View;

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 315
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 316
    invoke-static {}, Lcom/android/calendar/a/a/a/b;->b()Lcom/android/calendar/a/a/a/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/calendar/a/a/a/b;->a(Lcom/android/calendar/a/a/a/g;)V

    .line 317
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->o:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    .line 318
    invoke-virtual {p0}, Lcom/android/calendar/sticker/r;->b()V

    .line 319
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lcom/android/calendar/sticker/r;->j:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
