.class public Lcom/android/calendar/sticker/c;
.super Landroid/app/DialogFragment;
.source "StickerDeleteFragment.java"


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field public a:I

.field public b:Ljava/util/ArrayList;

.field public c:Lcom/android/calendar/sticker/a;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ListView;

.field private g:Lcom/android/calendar/sticker/n;

.field private h:Landroid/app/AlertDialog;

.field private i:Landroid/app/Activity;

.field private j:Lcom/android/calendar/sticker/j;

.field private k:Landroid/view/ActionMode;

.field private l:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/android/calendar/sticker/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/sticker/c;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/sticker/c;-><init>(I)V

    .line 76
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 57
    iput-object v1, p0, Lcom/android/calendar/sticker/c;->e:Landroid/view/View;

    .line 58
    iput-object v1, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/sticker/c;->a:I

    .line 64
    iput-object v1, p0, Lcom/android/calendar/sticker/c;->h:Landroid/app/AlertDialog;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->l:Ljava/util/ArrayList;

    .line 79
    iput p1, p0, Lcom/android/calendar/sticker/c;->a:I

    .line 80
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/sticker/c;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/android/calendar/sticker/c;->k:Landroid/view/ActionMode;

    return-object p1
.end method

.method private a(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 14

    .prologue
    .line 200
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 201
    const-string v1, "sticker_name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 202
    const-string v1, "filepath"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 203
    const-string v1, "sticker_group"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 205
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 207
    const/4 v1, -0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 208
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 210
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 211
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 212
    invoke-interface {p1, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 213
    const-wide/16 v6, 0x0

    .line 214
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 215
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 216
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 218
    :cond_0
    new-instance v1, Lcom/android/calendar/sticker/m;

    invoke-direct/range {v1 .. v8}, Lcom/android/calendar/sticker/m;-><init>(JLjava/lang/String;Ljava/lang/String;JI)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 221
    :cond_1
    return-object v12
.end method

.method static synthetic a(Lcom/android/calendar/sticker/c;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->l:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/sticker/c;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/calendar/sticker/c;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/sticker/c;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->k:Landroid/view/ActionMode;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const v5, 0x7f0f0143

    const/4 v4, 0x1

    .line 226
    invoke-virtual {p0}, Lcom/android/calendar/sticker/c;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/sticker/c;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 230
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 231
    invoke-virtual {p0}, Lcom/android/calendar/sticker/c;->a()I

    move-result v0

    .line 232
    if-eqz v0, :cond_0

    .line 237
    if-ne v0, v4, :cond_2

    .line 238
    invoke-virtual {p0}, Lcom/android/calendar/sticker/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0436

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 242
    :goto_1
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 243
    new-instance v0, Lcom/android/calendar/sticker/g;

    invoke-direct {v0, p0}, Lcom/android/calendar/sticker/g;-><init>(Lcom/android/calendar/sticker/c;)V

    invoke-virtual {v1, v5, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 250
    const/high16 v0, 0x1040000

    new-instance v2, Lcom/android/calendar/sticker/h;

    invoke-direct {v2, p0}, Lcom/android/calendar/sticker/h;-><init>(Lcom/android/calendar/sticker/c;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 256
    new-instance v0, Lcom/android/calendar/sticker/i;

    invoke-direct {v0, p0}, Lcom/android/calendar/sticker/i;-><init>(Lcom/android/calendar/sticker/c;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 264
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->h:Landroid/app/AlertDialog;

    goto :goto_0

    .line 240
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/sticker/c;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f014b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private c()V
    .locals 4

    .prologue
    .line 269
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/a;->a()Ljava/util/Set;

    move-result-object v0

    .line 273
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    .line 274
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 276
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/Long;

    .line 277
    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 279
    new-instance v0, Lcom/android/calendar/sticker/k;

    iget-object v3, p0, Lcom/android/calendar/sticker/c;->i:Landroid/app/Activity;

    invoke-direct {v0, v3, v1}, Lcom/android/calendar/sticker/k;-><init>(Landroid/app/Activity;I)V

    .line 280
    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {v0, v2}, Lcom/android/calendar/sticker/k;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/sticker/c;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/sticker/c;->d()V

    return-void
.end method

.method static synthetic d(Lcom/android/calendar/sticker/c;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    .line 380
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->j:Lcom/android/calendar/sticker/j;

    if-nez v0, :cond_0

    .line 401
    new-instance v0, Lcom/android/calendar/sticker/j;

    invoke-virtual {p0}, Lcom/android/calendar/sticker/c;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/sticker/j;-><init>(Lcom/android/calendar/sticker/c;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->j:Lcom/android/calendar/sticker/j;

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->i:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/sticker/c;->j:Lcom/android/calendar/sticker/j;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->k:Landroid/view/ActionMode;

    .line 404
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/sticker/c;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/sticker/c;->c()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/sticker/c;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->h:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/sticker/c;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/sticker/c;->b()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    iget-object v1, p0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 368
    iget-object v1, p0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    if-eqz v1, :cond_0

    .line 369
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/a;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 372
    :cond_0
    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 85
    iput-object p1, p0, Lcom/android/calendar/sticker/c;->i:Landroid/app/Activity;

    .line 86
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 96
    const v0, 0x7f0400a0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->e:Landroid/view/View;

    .line 98
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->e:Landroid/view/View;

    const v1, 0x7f1202ba

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    .line 99
    new-instance v0, Lcom/android/calendar/sticker/a;

    iget-object v1, p0, Lcom/android/calendar/sticker/c;->i:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/sticker/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    .line 100
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 101
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 102
    invoke-direct {p0}, Lcom/android/calendar/sticker/c;->e()V

    .line 104
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->i:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 106
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/sticker/d;

    invoke-direct {v1, p0}, Lcom/android/calendar/sticker/d;-><init>(Lcom/android/calendar/sticker/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/sticker/e;

    invoke-direct {v1, p0}, Lcom/android/calendar/sticker/e;-><init>(Lcom/android/calendar/sticker/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 143
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/sticker/f;

    invoke-direct {v1, p0}, Lcom/android/calendar/sticker/f;-><init>(Lcom/android/calendar/sticker/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->e:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 91
    invoke-direct {p0}, Lcom/android/calendar/sticker/c;->d()V

    .line 92
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 159
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 160
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 161
    new-instance v0, Lcom/android/calendar/sticker/n;

    iget-object v1, p0, Lcom/android/calendar/sticker/c;->i:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/sticker/n;-><init>(Lcom/android/calendar/sticker/c;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/sticker/c;->g:Lcom/android/calendar/sticker/n;

    .line 162
    const-string v5, "packageId > 0"

    .line 163
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->g:Lcom/android/calendar/sticker/n;

    const/4 v1, 0x0

    sget-object v3, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/gx;->b:[Ljava/lang/String;

    const-string v7, "_id DESC"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/sticker/n;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    iget-object v1, p0, Lcom/android/calendar/sticker/c;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/sticker/a;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
