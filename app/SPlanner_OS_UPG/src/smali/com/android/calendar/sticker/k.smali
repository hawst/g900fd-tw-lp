.class Lcom/android/calendar/sticker/k;
.super Landroid/os/AsyncTask;
.source "StickerDeleteFragment.java"


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 289
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 287
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    .line 290
    iput-object p1, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    .line 292
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    .line 293
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    const v1, 0x7f0f01cf

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 294
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p2}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 295
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 296
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 297
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 298
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/sticker/k;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Long;)Ljava/lang/Integer;
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 302
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 304
    if-nez p1, :cond_0

    .line 305
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 329
    :goto_0
    return-object v0

    .line 307
    :cond_0
    array-length v0, p1

    if-le v0, v1, :cond_2

    move v0, v1

    .line 310
    :goto_1
    array-length v8, p1

    move v6, v2

    move v3, v2

    :goto_2
    if-ge v6, v8, :cond_4

    aget-object v4, p1, v6

    .line 311
    sget-object v9, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 312
    if-nez v4, :cond_3

    .line 310
    :cond_1
    :goto_3
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    :cond_2
    move v0, v2

    .line 307
    goto :goto_1

    .line 317
    :cond_3
    if-eqz v7, :cond_5

    .line 318
    invoke-virtual {v7, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 321
    :goto_4
    if-lez v4, :cond_1

    .line 325
    if-eqz v0, :cond_1

    .line 326
    const/4 v4, 0x2

    new-array v9, v4, [Ljava/lang/Integer;

    add-int/lit8 v4, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v9, v2

    array-length v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v9, v1

    invoke-virtual {p0, v9}, Lcom/android/calendar/sticker/k;->publishProgress([Ljava/lang/Object;)V

    move v3, v4

    goto :goto_3

    :cond_4
    move-object v0, v5

    .line 329
    goto :goto_0

    :cond_5
    move v4, v2

    goto :goto_4
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getMax()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 349
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    new-instance v1, Lcom/android/calendar/sticker/l;

    invoke-direct {v1, p0}, Lcom/android/calendar/sticker/l;-><init>(Lcom/android/calendar/sticker/k;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 362
    :cond_1
    return-void
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 334
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/sticker/k;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 339
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 341
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/sticker/k;->b:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 285
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/android/calendar/sticker/k;->a([Ljava/lang/Long;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 285
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/sticker/k;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 285
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/sticker/k;->a([Ljava/lang/Integer;)V

    return-void
.end method
