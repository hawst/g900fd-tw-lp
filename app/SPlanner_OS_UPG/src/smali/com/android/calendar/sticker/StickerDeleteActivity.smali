.class public Lcom/android/calendar/sticker/StickerDeleteActivity;
.super Landroid/app/Activity;
.source "StickerDeleteActivity.java"


# instance fields
.field private a:Lcom/android/calendar/sticker/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 71
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    if-eqz v0, :cond_0

    .line 72
    const v0, 0x7f12006b

    invoke-virtual {p0, v0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 73
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 74
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setDimAmount(F)V

    .line 75
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 76
    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 77
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 78
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 79
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 89
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 91
    :cond_0
    return-void

    .line 81
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Landroid/view/Window;->setDimAmount(F)V

    .line 82
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 83
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0446

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 84
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 85
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 86
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 87
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 106
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 116
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 108
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->finish()V

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 122
    invoke-direct {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->a()V

    .line 123
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->invalidateOptionsMenu()V

    .line 124
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f12006b

    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 48
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 49
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 52
    :cond_0
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->setContentView(I)V

    .line 54
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/c;

    iput-object v0, p0, Lcom/android/calendar/sticker/StickerDeleteActivity;->a:Lcom/android/calendar/sticker/c;

    .line 57
    iget-object v0, p0, Lcom/android/calendar/sticker/StickerDeleteActivity;->a:Lcom/android/calendar/sticker/c;

    if-nez v0, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "category_id"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 59
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 61
    new-instance v2, Lcom/android/calendar/sticker/c;

    invoke-direct {v2, v0}, Lcom/android/calendar/sticker/c;-><init>(I)V

    iput-object v2, p0, Lcom/android/calendar/sticker/StickerDeleteActivity;->a:Lcom/android/calendar/sticker/c;

    .line 62
    iget-object v0, p0, Lcom/android/calendar/sticker/StickerDeleteActivity;->a:Lcom/android/calendar/sticker/c;

    invoke-virtual {v1, v3, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 63
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 66
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->a()V

    .line 68
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 96
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/event/EditEventActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    const-string v1, "event_create"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0, v0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->startActivity(Landroid/content/Intent;)V

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDeleteActivity;->invalidateOptionsMenu()V

    .line 102
    return-void
.end method
