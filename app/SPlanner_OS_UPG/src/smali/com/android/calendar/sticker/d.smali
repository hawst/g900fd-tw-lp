.class Lcom/android/calendar/sticker/d;
.super Ljava/lang/Object;
.source "StickerDeleteFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/sticker/c;


# direct methods
.method constructor <init>(Lcom/android/calendar/sticker/c;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/android/calendar/sticker/d;->a:Lcom/android/calendar/sticker/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/calendar/sticker/d;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    if-nez v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/d;->a:Lcom/android/calendar/sticker/c;

    iget-object v0, v0, Lcom/android/calendar/sticker/c;->c:Lcom/android/calendar/sticker/a;

    iget-object v1, p0, Lcom/android/calendar/sticker/d;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v1}, Lcom/android/calendar/sticker/c;->a(Lcom/android/calendar/sticker/c;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/sticker/a;->b(Ljava/util/ArrayList;)V

    .line 128
    iget-object v0, p0, Lcom/android/calendar/sticker/d;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->a(Lcom/android/calendar/sticker/c;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/calendar/sticker/d;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->a(Lcom/android/calendar/sticker/c;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/android/calendar/sticker/d;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->a(Lcom/android/calendar/sticker/c;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/d;->a:Lcom/android/calendar/sticker/c;

    invoke-static {v0}, Lcom/android/calendar/sticker/c;->a(Lcom/android/calendar/sticker/c;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
