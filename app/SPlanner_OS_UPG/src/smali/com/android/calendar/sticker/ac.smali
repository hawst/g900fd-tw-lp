.class Lcom/android/calendar/sticker/ac;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "StickerDownloadListFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/sticker/r;


# direct methods
.method private constructor <init>(Lcom/android/calendar/sticker/r;)V
    .locals 0

    .prologue
    .line 764
    iput-object p1, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/sticker/r;Lcom/android/calendar/sticker/s;)V
    .locals 0

    .prologue
    .line 764
    invoke-direct {p0, p1}, Lcom/android/calendar/sticker/ac;-><init>(Lcom/android/calendar/sticker/r;)V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 768
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->e(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->f(Lcom/android/calendar/sticker/r;)Landroid/widget/ListView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 814
    :goto_0
    return v0

    .line 772
    :cond_1
    sget v0, Lcom/android/calendar/sticker/r;->c:I

    sget v2, Lcom/android/calendar/sticker/r;->a:I

    sub-int/2addr v0, v2

    .line 773
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 774
    sget v3, Lcom/android/calendar/sticker/r;->d:I

    sget v4, Lcom/android/calendar/sticker/r;->b:I

    sub-int/2addr v3, v4

    .line 775
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 777
    const/16 v4, 0x32

    if-lt v2, v4, :cond_5

    if-le v2, v3, :cond_5

    .line 779
    if-lez v0, :cond_2

    .line 780
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iget v2, v0, Lcom/android/calendar/sticker/r;->e:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/android/calendar/sticker/r;->e:I

    .line 781
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iget v0, v0, Lcom/android/calendar/sticker/r;->e:I

    if-gez v0, :cond_3

    .line 782
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iput v1, v0, Lcom/android/calendar/sticker/r;->e:I

    move v0, v1

    .line 783
    goto :goto_0

    .line 787
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iget v2, v0, Lcom/android/calendar/sticker/r;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/android/calendar/sticker/r;->e:I

    .line 788
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iget v0, v0, Lcom/android/calendar/sticker/r;->e:I

    iget-object v2, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v2}, Lcom/android/calendar/sticker/r;->m(Lcom/android/calendar/sticker/r;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-le v0, v2, :cond_3

    .line 789
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iget-object v2, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v2}, Lcom/android/calendar/sticker/r;->m(Lcom/android/calendar/sticker/r;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/android/calendar/sticker/r;->e:I

    move v0, v1

    .line 790
    goto :goto_0

    .line 794
    :cond_3
    invoke-static {}, Lcom/android/calendar/a/a/a/h;->b()Lcom/android/calendar/a/a/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/h;->c()V

    .line 795
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->a(Lcom/android/calendar/sticker/r;)I

    move-result v0

    if-nez v0, :cond_4

    .line 797
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->b(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 798
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->c(Lcom/android/calendar/sticker/r;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 799
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->d(Lcom/android/calendar/sticker/r;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 801
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->f(Lcom/android/calendar/sticker/r;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->e(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iget v3, v3, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 802
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0, v1}, Lcom/android/calendar/sticker/r;->a(Lcom/android/calendar/sticker/r;I)I

    .line 804
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->a(Lcom/android/calendar/sticker/r;)I

    move-result v0

    if-nez v0, :cond_4

    .line 805
    invoke-static {}, Lcom/android/calendar/a/a/a/h;->b()Lcom/android/calendar/a/a/a/h;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->g(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iget v2, v2, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/a/a;

    iget-object v2, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/a/a/a/h;->a(Lcom/android/calendar/a/a/a/a;Lcom/android/calendar/a/a/a/j;)V

    .line 809
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/r;->c()V

    .line 810
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/r;->b()V

    .line 811
    iget-object v0, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->e(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/sticker/ac;->a:Lcom/android/calendar/sticker/r;

    iget v1, v1, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    .line 812
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 814
    goto/16 :goto_0
.end method
