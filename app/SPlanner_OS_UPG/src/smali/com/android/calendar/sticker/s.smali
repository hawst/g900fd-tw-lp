.class Lcom/android/calendar/sticker/s;
.super Ljava/lang/Object;
.source "StickerDownloadListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/sticker/r;


# direct methods
.method constructor <init>(Lcom/android/calendar/sticker/r;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 146
    iget-object v1, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    iget v1, v1, Lcom/android/calendar/sticker/r;->e:I

    if-ne v1, v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 149
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    iput v0, v1, Lcom/android/calendar/sticker/r;->e:I

    .line 150
    invoke-static {}, Lcom/android/calendar/a/a/a/h;->b()Lcom/android/calendar/a/a/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/h;->c()V

    .line 151
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->a(Lcom/android/calendar/sticker/r;)I

    move-result v0

    if-nez v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->b(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 154
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->c(Lcom/android/calendar/sticker/r;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->d(Lcom/android/calendar/sticker/r;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->f(Lcom/android/calendar/sticker/r;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->e(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    iget v2, v2, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 159
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0, v3}, Lcom/android/calendar/sticker/r;->a(Lcom/android/calendar/sticker/r;I)I

    .line 161
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->a(Lcom/android/calendar/sticker/r;)I

    move-result v0

    if-nez v0, :cond_1

    .line 162
    invoke-static {}, Lcom/android/calendar/a/a/a/h;->b()Lcom/android/calendar/a/a/a/h;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->g(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    iget v2, v2, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/a/a;

    iget-object v2, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/a/a/a/h;->a(Lcom/android/calendar/a/a/a/a;Lcom/android/calendar/a/a/a/j;)V

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/r;->c()V

    .line 168
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/r;->b()V

    .line 169
    iget-object v0, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->e(Lcom/android/calendar/sticker/r;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/sticker/s;->a:Lcom/android/calendar/sticker/r;

    iget v1, v1, Lcom/android/calendar/sticker/r;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/aa;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/aa;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method
