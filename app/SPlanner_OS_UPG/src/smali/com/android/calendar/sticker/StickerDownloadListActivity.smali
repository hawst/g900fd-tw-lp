.class public Lcom/android/calendar/sticker/StickerDownloadListActivity;
.super Landroid/app/Activity;
.source "StickerDownloadListActivity.java"


# instance fields
.field private a:Lcom/android/calendar/sticker/r;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/sticker/StickerDownloadListActivity;)Lcom/android/calendar/sticker/r;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 83
    iget-boolean v0, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->b:Z

    if-eqz v0, :cond_0

    .line 84
    const v0, 0x7f12006b

    invoke-virtual {p0, v0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 85
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 86
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setDimAmount(F)V

    .line 87
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 88
    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 89
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 90
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 91
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 101
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 103
    :cond_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Landroid/view/Window;->setDimAmount(F)V

    .line 94
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 95
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0446

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 96
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 97
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 98
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 99
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 227
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 217
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/sticker/r;->a:I

    .line 218
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/sticker/r;->b:I

    goto :goto_0

    .line 222
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/sticker/r;->c:I

    .line 223
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/sticker/r;->d:I

    goto :goto_0

    .line 215
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 177
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 179
    iget-object v1, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    if-eqz v1, :cond_1

    .line 180
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    iget v2, v1, Lcom/android/calendar/sticker/r;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/android/calendar/sticker/r;->e:I

    .line 183
    :cond_0
    const-string v1, "tabId"

    iget-object v2, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    iget v2, v2, Lcom/android/calendar/sticker/r;->e:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 185
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->setResult(ILandroid/content/Intent;)V

    .line 187
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 188
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 197
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 199
    invoke-direct {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a()V

    .line 200
    iget-object v0, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    if-eqz v0, :cond_0

    .line 201
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 202
    new-instance v1, Lcom/android/calendar/sticker/q;

    invoke-direct {v1, p0}, Lcom/android/calendar/sticker/q;-><init>(Lcom/android/calendar/sticker/StickerDownloadListActivity;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->invalidateOptionsMenu()V

    .line 210
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v4, 0x7f12006b

    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 57
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 58
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 61
    :cond_0
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->b:Z

    .line 63
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->setContentView(I)V

    .line 65
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/r;

    iput-object v0, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    .line 71
    iget-object v0, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    if-nez v0, :cond_1

    .line 72
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "category_id"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 73
    new-instance v2, Lcom/android/calendar/sticker/r;

    invoke-direct {v2, v0}, Lcom/android/calendar/sticker/r;-><init>(I)V

    iput-object v2, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    .line 74
    iget-object v0, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v1, v4, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 75
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 78
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a()V

    .line 80
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 119
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11000f

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 121
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 122
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 124
    return v2
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 172
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 173
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 141
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 166
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 143
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->onBackPressed()V

    goto :goto_0

    .line 146
    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 147
    const v1, 0x7f0f0172

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0171

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02e3

    new-instance v2, Lcom/android/calendar/sticker/p;

    invoke-direct {v2, p0}, Lcom/android/calendar/sticker/p;-><init>(Lcom/android/calendar/sticker/StickerDownloadListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f00a4

    new-instance v2, Lcom/android/calendar/sticker/o;

    invoke-direct {v2, p0}, Lcom/android/calendar/sticker/o;-><init>(Lcom/android/calendar/sticker/StickerDownloadListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 141
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f12034b -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 192
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 193
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 130
    iget-object v0, p0, Lcom/android/calendar/sticker/StickerDownloadListActivity;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/r;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 136
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 133
    :cond_0
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 108
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/event/EditEventActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    const-string v1, "event_create"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0, v0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->startActivity(Landroid/content/Intent;)V

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/sticker/StickerDownloadListActivity;->invalidateOptionsMenu()V

    .line 114
    return-void
.end method
