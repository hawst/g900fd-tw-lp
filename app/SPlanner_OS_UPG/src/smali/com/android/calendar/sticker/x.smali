.class Lcom/android/calendar/sticker/x;
.super Landroid/app/DialogFragment;
.source "StickerDownloadListFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/sticker/r;


# direct methods
.method public constructor <init>(Lcom/android/calendar/sticker/r;)V
    .locals 1

    .prologue
    .line 826
    iput-object p1, p0, Lcom/android/calendar/sticker/x;->a:Lcom/android/calendar/sticker/r;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 827
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/sticker/x;->setRetainInstance(Z)V

    .line 828
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 833
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/sticker/x;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 834
    const v1, 0x7f0f00a5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f038b

    new-instance v3, Lcom/android/calendar/sticker/z;

    invoke-direct {v3, p0}, Lcom/android/calendar/sticker/z;-><init>(Lcom/android/calendar/sticker/x;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0386

    new-instance v3, Lcom/android/calendar/sticker/y;

    invoke-direct {v3, p0}, Lcom/android/calendar/sticker/y;-><init>(Lcom/android/calendar/sticker/x;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 854
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 862
    invoke-virtual {p0}, Lcom/android/calendar/sticker/x;->dismiss()V

    .line 863
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 864
    return-void
.end method
