.class Lcom/android/calendar/sticker/aa;
.super Landroid/widget/BaseAdapter;
.source "StickerDownloadListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/sticker/r;

.field private b:Ljava/util/ArrayList;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/android/calendar/sticker/r;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 451
    iput-object p1, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 448
    iput-object v0, p0, Lcom/android/calendar/sticker/aa;->b:Ljava/util/ArrayList;

    .line 449
    iput-object v0, p0, Lcom/android/calendar/sticker/aa;->c:Landroid/view/LayoutInflater;

    .line 452
    iput-object p2, p0, Lcom/android/calendar/sticker/aa;->b:Ljava/util/ArrayList;

    .line 453
    invoke-virtual {p1}, Lcom/android/calendar/sticker/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/sticker/aa;->c:Landroid/view/LayoutInflater;

    .line 454
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/android/calendar/sticker/aa;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 459
    const/4 v0, 0x0

    .line 461
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/sticker/aa;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/android/calendar/sticker/aa;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 471
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    .line 477
    .line 479
    if-nez p2, :cond_1

    .line 480
    iget-object v0, p0, Lcom/android/calendar/sticker/aa;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0400a2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 482
    new-instance v1, Lcom/android/calendar/sticker/ae;

    invoke-direct {v1}, Lcom/android/calendar/sticker/ae;-><init>()V

    .line 483
    const v0, 0x7f1202bf

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/sticker/ae;->b:Landroid/widget/TextView;

    .line 484
    const v0, 0x7f1202c1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/calendar/sticker/ae;->a:Landroid/widget/ImageView;

    .line 485
    const v0, 0x7f1202c2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v1, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    .line 486
    const v0, 0x7f1202c3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    .line 487
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v1

    .line 493
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    new-instance v1, Landroid/view/GestureDetector;

    iget-object v3, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v3}, Lcom/android/calendar/sticker/r;->j(Lcom/android/calendar/sticker/r;)Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Lcom/android/calendar/sticker/ac;

    iget-object v5, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/android/calendar/sticker/ac;-><init>(Lcom/android/calendar/sticker/r;Lcom/android/calendar/sticker/s;)V

    invoke-direct {v1, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    invoke-static {v0, v1}, Lcom/android/calendar/sticker/r;->a(Lcom/android/calendar/sticker/r;Landroid/view/GestureDetector;)Landroid/view/GestureDetector;

    .line 494
    new-instance v0, Lcom/android/calendar/sticker/ab;

    invoke-direct {v0, p0}, Lcom/android/calendar/sticker/ab;-><init>(Lcom/android/calendar/sticker/aa;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 501
    iput p1, v2, Lcom/android/calendar/sticker/ae;->e:I

    .line 502
    iget-object v0, p0, Lcom/android/calendar/sticker/aa;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/b;

    .line 503
    const-string v1, ""

    .line 505
    iget v3, v0, Lcom/android/calendar/a/a/b;->c:I

    invoke-static {}, Lcom/android/calendar/sticker/r;->f()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 506
    iget-object v1, v0, Lcom/android/calendar/a/a/b;->h:Ljava/lang/Object;

    check-cast v1, Lcom/android/calendar/a/a/h;

    .line 507
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 508
    iget-wide v4, v1, Lcom/android/calendar/a/a/h;->d:J

    const-wide/16 v6, 0x400

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 509
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v1, Lcom/android/calendar/a/a/h;->d:J

    const-wide/16 v10, 0x400

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    :goto_1
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 514
    iget-object v4, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v4}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f01ec

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    iget-object v4, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    iget-object v5, v0, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setTag(Ljava/lang/Object;)V

    .line 516
    iget-object v1, v1, Lcom/android/calendar/a/a/h;->b:Ljava/lang/String;

    .line 518
    invoke-virtual {v0}, Lcom/android/calendar/a/a/b;->a()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 519
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    const v4, 0x7f02018a

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 520
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v4}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0296

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 521
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 522
    const-string v0, " / "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    iget-object v0, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v0}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0f0405

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 545
    :goto_2
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    move-object v0, v1

    .line 548
    iget-object v1, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 549
    iget-object v1, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 550
    iget-object v1, v2, Lcom/android/calendar/sticker/ae;->a:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/a/a;->a(Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V

    .line 551
    return-object p2

    .line 490
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/ae;

    move-object v2, v0

    goto/16 :goto_0

    .line 511
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 525
    :cond_3
    invoke-virtual {v0}, Lcom/android/calendar/a/a/b;->a()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 526
    iget-object v4, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    const v5, 0x7f020189

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 527
    iget-object v4, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v5}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f00a4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 528
    iget-object v4, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 529
    iget-object v4, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 530
    iget-object v4, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    iget-wide v6, v0, Lcom/android/calendar/a/a/b;->e:J

    const-wide/16 v8, 0x64

    mul-long/2addr v6, v8

    iget-wide v8, v0, Lcom/android/calendar/a/a/b;->d:J

    div-long/2addr v6, v8

    long-to-int v0, v6

    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 531
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_2

    .line 533
    :cond_4
    invoke-virtual {v0}, Lcom/android/calendar/a/a/b;->a()I

    move-result v0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_5

    .line 534
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    const v4, 0x7f020189

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 535
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v4}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00a4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 536
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 537
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/16 v4, 0x64

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 538
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 539
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto/16 :goto_2

    .line 541
    :cond_5
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    const v4, 0x7f02018b

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 542
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->d:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-virtual {v4}, Lcom/android/calendar/sticker/r;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0402

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 543
    iget-object v0, v2, Lcom/android/calendar/sticker/ae;->c:Landroid/widget/ProgressBar;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v0}, Lcom/android/calendar/sticker/r;->j(Lcom/android/calendar/sticker/r;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 578
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 579
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 556
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/sticker/ae;

    .line 557
    if-eqz v0, :cond_0

    .line 558
    iget-object v1, p0, Lcom/android/calendar/sticker/aa;->b:Ljava/util/ArrayList;

    iget v2, v0, Lcom/android/calendar/sticker/ae;->e:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/a/a/b;

    .line 559
    invoke-virtual {v1}, Lcom/android/calendar/a/a/b;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 570
    iget-object v1, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    iget v0, v0, Lcom/android/calendar/sticker/ae;->e:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/sticker/r;->a(IZ)V

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 562
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    iget v0, v0, Lcom/android/calendar/sticker/ae;->e:I

    invoke-virtual {v1, v0}, Lcom/android/calendar/sticker/r;->a(I)V

    goto :goto_0

    .line 565
    :pswitch_1
    iget-object v1, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v1}, Lcom/android/calendar/sticker/r;->l(Lcom/android/calendar/sticker/r;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    invoke-static {v1}, Lcom/android/calendar/sticker/r;->l(Lcom/android/calendar/sticker/r;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 566
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/sticker/aa;->a:Lcom/android/calendar/sticker/r;

    iget v0, v0, Lcom/android/calendar/sticker/ae;->e:I

    invoke-virtual {v1, v0}, Lcom/android/calendar/sticker/r;->b(I)V

    goto :goto_0

    .line 559
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
