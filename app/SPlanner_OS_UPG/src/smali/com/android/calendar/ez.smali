.class public Lcom/android/calendar/ez;
.super Landroid/app/Fragment;
.source "MiniCalendarFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# static fields
.field public static a:I


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageButton;

.field private d:Landroid/widget/ImageButton;

.field private e:Lcom/android/calendar/al;

.field private f:Landroid/app/Activity;

.field private g:Landroid/view/View;

.field private h:Landroid/text/format/Time;

.field private i:[Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/ez;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-direct {p0, v0}, Lcom/android/calendar/ez;-><init>(Landroid/text/format/Time;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    .line 90
    iput-object p1, p0, Lcom/android/calendar/ez;->h:Landroid/text/format/Time;

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/ez;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/ez;->f:Landroid/app/Activity;

    return-object v0
.end method

.method private a()V
    .locals 9

    .prologue
    const v8, 0x7f120076

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 229
    const/4 v1, 0x0

    .line 231
    if-nez v1, :cond_1

    .line 233
    invoke-virtual {p0}, Lcom/android/calendar/ez;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 235
    const/high16 v2, 0x7f060000

    const v4, 0x7f060001

    invoke-virtual {v1, v2, v4}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    move v2, v0

    move-object v4, v1

    .line 237
    :goto_0
    new-instance v1, Lcom/android/calendar/month/k;

    iget-object v5, p0, Lcom/android/calendar/ez;->h:Landroid/text/format/Time;

    invoke-virtual {v5, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-direct {v1, v6, v7, v0}, Lcom/android/calendar/month/k;-><init>(JZ)V

    .line 238
    iget-object v3, p0, Lcom/android/calendar/ez;->e:Lcom/android/calendar/al;

    move-object v0, v1

    check-cast v0, Lcom/android/calendar/ap;

    invoke-virtual {v3, v8, v0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 240
    invoke-virtual {v4, v8, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 241
    if-eqz v2, :cond_0

    .line 242
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 244
    :cond_0
    return-void

    :cond_1
    move v2, v3

    move-object v4, v1

    goto :goto_0
.end method

.method private a(ILandroid/text/format/Time;)V
    .locals 3

    .prologue
    .line 181
    packed-switch p1, :pswitch_data_0

    .line 219
    :goto_0
    return-void

    .line 183
    :pswitch_0
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x0

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 186
    :pswitch_1
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x1

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 189
    :pswitch_2
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x2

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 192
    :pswitch_3
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x3

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 195
    :pswitch_4
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x4

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 198
    :pswitch_5
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x5

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 201
    :pswitch_6
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x6

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 204
    :pswitch_7
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x7

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 207
    :pswitch_8
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/16 v1, 0x8

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 210
    :pswitch_9
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/16 v1, 0x9

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 213
    :pswitch_a
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/16 v1, 0xa

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 216
    :pswitch_b
    iget v0, p2, Landroid/text/format/Time;->monthDay:I

    const/16 v1, 0xb

    iget v2, p2, Landroid/text/format/Time;->year:I

    invoke-virtual {p2, v0, v1, v2}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x7f12022d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method static synthetic a(Lcom/android/calendar/ez;ILandroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/ez;->a(ILandroid/text/format/Time;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/ez;Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/android/calendar/ez;->b(Landroid/text/format/Time;)V

    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v7, 0xc

    const/4 v6, 0x3

    .line 247
    new-array v3, v7, [Ljava/lang/String;

    .line 249
    new-array v0, v7, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/ez;->i:[Landroid/widget/TextView;

    move v1, v2

    .line 251
    :goto_0
    if-ge v1, v7, :cond_1

    .line 252
    iget-object v4, p0, Lcom/android/calendar/ez;->i:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    const v5, 0x7f12022d

    add-int/2addr v5, v1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    .line 253
    iget-object v0, p0, Lcom/android/calendar/ez;->i:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 254
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v4, "vi"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    invoke-static {v1, v6, v2}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    .line 260
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/ez;->i:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    iget-object v0, p0, Lcom/android/calendar/ez;->i:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    invoke-virtual {p0}, Lcom/android/calendar/ez;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0131

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 262
    iget-object v0, p0, Lcom/android/calendar/ez;->i:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    new-instance v4, Lcom/android/calendar/fc;

    invoke-direct {v4, p0}, Lcom/android/calendar/fc;-><init>(Lcom/android/calendar/ez;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 258
    :cond_0
    const/4 v0, 0x1

    invoke-static {v1, v6, v0}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    goto :goto_1

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/ez;->h:Landroid/text/format/Time;

    invoke-virtual {p0, v0}, Lcom/android/calendar/ez;->a(Landroid/text/format/Time;)V

    .line 274
    const v0, 0x7f120229

    invoke-virtual {p0, v0}, Lcom/android/calendar/ez;->b(I)V

    .line 275
    const v0, 0x7f12022b

    invoke-virtual {p0, v0}, Lcom/android/calendar/ez;->b(I)V

    .line 276
    const v0, 0x7f12022a

    invoke-virtual {p0, v0}, Lcom/android/calendar/ez;->a(I)V

    .line 277
    return-void
.end method

.method private b(Landroid/text/format/Time;)V
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 223
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 224
    iget-object v0, p0, Lcom/android/calendar/ez;->e:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v6, v5

    move-object v12, v5

    move-object v13, v5

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 226
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/ez;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/calendar/ez;->a()V

    return-void
.end method

.method static synthetic c(Lcom/android/calendar/ez;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/ez;->h:Landroid/text/format/Time;

    return-object v0
.end method


# virtual methods
.method protected a(I)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 138
    new-instance v1, Lcom/android/calendar/fa;

    invoke-direct {v1, p0}, Lcom/android/calendar/fa;-><init>(Lcom/android/calendar/ez;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    return-void
.end method

.method protected a(Landroid/text/format/Time;)V
    .locals 3

    .prologue
    .line 123
    const-string v0, "MiniCalendarFragment"

    const-string v1, "setCalendar"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    const v1, 0x7f120229

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/ez;->c:Landroid/widget/ImageButton;

    .line 125
    iget-object v0, p0, Lcom/android/calendar/ez;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 127
    iget-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    const v1, 0x7f12022b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/ez;->d:Landroid/widget/ImageButton;

    .line 128
    iget-object v0, p0, Lcom/android/calendar/ez;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 130
    iget-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    const v1, 0x7f12022a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/ez;->b:Landroid/widget/TextView;

    .line 131
    iget-object v0, p0, Lcom/android/calendar/ez;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lcom/android/calendar/ez;->b:Landroid/widget/TextView;

    const-string v1, "%Y"

    invoke-virtual {p1, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/android/calendar/ez;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/calendar/ez;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0131

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 134
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/calendar/ez;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/android/calendar/ez;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const-string v2, "%Y"

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/android/calendar/ez;->h:Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 112
    iget-object v0, p0, Lcom/android/calendar/ez;->h:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 113
    iget-object v0, p0, Lcom/android/calendar/ez;->h:Landroid/text/format/Time;

    invoke-virtual {p0, v0}, Lcom/android/calendar/ez;->a(Landroid/text/format/Time;)V

    .line 115
    :cond_0
    return-void
.end method

.method protected b(I)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 157
    new-instance v1, Lcom/android/calendar/fb;

    invoke-direct {v1, p0, p1}, Lcom/android/calendar/fb;-><init>(Lcom/android/calendar/ez;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 103
    const-wide/16 v0, 0x820

    return-wide v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 81
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/ez;->e:Lcom/android/calendar/al;

    .line 82
    iput-object p1, p0, Lcom/android/calendar/ez;->f:Landroid/app/Activity;

    .line 83
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 96
    const v0, 0x7f040071

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    .line 97
    invoke-direct {p0}, Lcom/android/calendar/ez;->b()V

    .line 98
    iget-object v0, p0, Lcom/android/calendar/ez;->g:Landroid/view/View;

    return-object v0
.end method
