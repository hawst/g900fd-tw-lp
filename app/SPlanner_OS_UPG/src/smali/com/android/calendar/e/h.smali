.class Lcom/android/calendar/e/h;
.super Ljava/lang/Object;
.source "DayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/android/calendar/e/h;->a:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/calendar/e/h;->a:Lcom/android/calendar/e/g;

    iget-object v0, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/android/calendar/e/h;->a:Lcom/android/calendar/e/g;

    iget-object v1, v1, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 220
    iget-object v1, p0, Lcom/android/calendar/e/h;->a:Lcom/android/calendar/e/g;

    iget-object v1, v1, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 221
    iget-object v1, p0, Lcom/android/calendar/e/h;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;)Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/android/calendar/e/h;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->b(Lcom/android/calendar/e/g;)V

    .line 223
    return-void
.end method
