.class Lcom/android/calendar/e/ae;
.super Ljava/lang/Object;
.source "DayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/android/calendar/e/g;Z)V
    .locals 0

    .prologue
    .line 4444
    iput-object p1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4445
    iput-boolean p2, p0, Lcom/android/calendar/e/ae;->b:Z

    .line 4446
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 4452
    iget-boolean v0, p0, Lcom/android/calendar/e/ae;->b:Z

    if-eqz v0, :cond_2

    .line 4453
    iget-object v0, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 4480
    :cond_0
    :goto_0
    return-void

    .line 4456
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->m(Lcom/android/calendar/e/g;)I

    move-result v0

    .line 4464
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1, v0}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;I)I

    .line 4466
    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v1

    if-gez v1, :cond_3

    .line 4467
    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v1

    .line 4468
    iget-object v3, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v3, v2}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    .line 4473
    :goto_2
    add-int/2addr v0, v1

    .line 4474
    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1, v0}, Lcom/android/calendar/e/g;->f(Lcom/android/calendar/e/g;I)I

    .line 4475
    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->o(Lcom/android/calendar/e/g;)V

    .line 4476
    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1, v0}, Lcom/android/calendar/e/g;->g(Lcom/android/calendar/e/g;I)I

    .line 4477
    iget-object v0, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 4478
    iget-object v0, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, v2}, Lcom/android/calendar/e/g;->h(Lcom/android/calendar/e/g;Z)Z

    .line 4479
    iget-object v0, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 4458
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v0

    if-lez v0, :cond_0

    .line 4461
    iget-object v0, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->m(Lcom/android/calendar/e/g;)I

    move-result v0

    neg-int v0, v0

    goto :goto_1

    .line 4469
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v1

    iget-object v3, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v3}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v3

    if-le v1, v3, :cond_4

    .line 4470
    iget-object v1, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v1

    iget-object v3, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v3}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v3

    sub-int/2addr v1, v3

    .line 4471
    iget-object v3, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    iget-object v4, p0, Lcom/android/calendar/e/ae;->a:Lcom/android/calendar/e/g;

    invoke-static {v4}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_2
.end method
