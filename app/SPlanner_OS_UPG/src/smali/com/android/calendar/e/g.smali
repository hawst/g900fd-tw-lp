.class public Lcom/android/calendar/e/g;
.super Landroid/view/View;
.source "DayView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field private static A:I

.field private static B:I

.field private static L:I

.field static a:I

.field private static aM:I

.field private static aN:I

.field private static aO:I

.field private static aP:I

.field private static aQ:I

.field private static aR:I

.field private static aS:F

.field private static aT:F

.field private static aU:F

.field private static aV:F

.field private static aW:I

.field private static aX:I

.field private static aY:I

.field private static aZ:I

.field private static as:I

.field static b:I

.field private static bE:I

.field private static bF:I

.field private static bR:I

.field private static bU:Z

.field private static ba:I

.field private static bb:I

.field private static bc:I

.field private static bd:I

.field private static be:I

.field private static bf:I

.field private static bh:F

.field private static bi:F

.field private static bl:I

.field private static bm:I

.field private static bn:I

.field private static bo:I

.field private static bp:I

.field private static bq:I

.field private static br:I

.field private static bs:I

.field private static bt:I

.field private static bu:I

.field private static bv:I

.field private static cE:I

.field protected static f:Ljava/lang/StringBuilder;

.field protected static g:Ljava/util/Formatter;

.field static p:Z

.field private static r:Ljava/lang/String;

.field private static s:Z

.field private static t:Z

.field private static u:F

.field private static v:I

.field private static w:I

.field private static y:I

.field private static z:I


# instance fields
.field private C:Z

.field private D:Z

.field private E:Landroid/os/Handler;

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:J

.field private final M:Lcom/android/calendar/e/x;

.field private N:Landroid/text/format/Time;

.field private final O:Lcom/android/calendar/e/ah;

.field private P:I

.field private final Q:Landroid/graphics/Typeface;

.field private R:I

.field private S:I

.field private T:[I

.field private U:Ljava/lang/String;

.field private V:Landroid/net/Uri;

.field private W:Z

.field private final aA:Landroid/graphics/Paint;

.field private aB:Landroid/graphics/Paint;

.field private aC:[F

.field private aD:[Landroid/graphics/Path;

.field private aE:I

.field private aF:Landroid/widget/PopupWindow;

.field private aG:Landroid/view/View;

.field private aH:Lcom/android/calendar/e/ao;

.field private final aI:Lcom/android/calendar/e/z;

.field private final aJ:Lcom/android/calendar/e/aa;

.field private aK:Z

.field private final aL:Lcom/android/calendar/dj;

.field private aa:I

.field private ab:I

.field private ac:Lcom/android/calendar/AllInOneActivity;

.field private final ad:Ljava/lang/Runnable;

.field private ae:Ljava/util/ArrayList;

.field private af:Ljava/util/ArrayList;

.field private ag:[Landroid/text/StaticLayout;

.field private ah:[Landroid/text/StaticLayout;

.field private ai:I

.field private aj:I

.field private ak:Z

.field private al:I

.field private am:I

.field private an:Lcom/android/calendar/dh;

.field private ao:I

.field private ap:I

.field private aq:Ljava/lang/String;

.field private ar:I

.field private final at:Landroid/graphics/Rect;

.field private final au:Landroid/graphics/Rect;

.field private final av:Landroid/graphics/Rect;

.field private final aw:Landroid/graphics/Paint;

.field private final ax:Landroid/graphics/Paint;

.field private final ay:Landroid/graphics/Paint;

.field private final az:Landroid/graphics/Paint;

.field private bA:I

.field private bB:I

.field private bC:I

.field private bD:I

.field private bG:I

.field private bH:I

.field private bI:F

.field private bJ:F

.field private bK:I

.field private bL:I

.field private bM:F

.field private bN:F

.field private bO:Z

.field private bP:I

.field private bQ:I

.field private bS:I

.field private final bT:I

.field private bV:I

.field private bW:I

.field private bX:I

.field private bY:I

.field private bZ:I

.field private bg:I

.field private bj:I

.field private bk:I

.field private bw:F

.field private bx:I

.field private by:I

.field private bz:I

.field protected c:Z

.field private cA:I

.field private cB:Ljava/lang/String;

.field private cC:Ljava/lang/String;

.field private final cD:Lcom/android/calendar/cj;

.field private cF:Landroid/graphics/RectF;

.field private cG:Lcom/android/calendar/e/af;

.field private cH:Z

.field private cI:Z

.field private cJ:Landroid/content/AsyncQueryHandler;

.field private cK:J

.field private cL:J

.field private cM:I

.field private cN:Lcom/android/calendar/e/ag;

.field private cO:Lcom/android/calendar/e/ab;

.field private cP:I

.field private cQ:Z

.field private cR:F

.field private cS:F

.field private cT:I

.field private cU:I

.field private cV:I

.field private cW:Z

.field private cX:Z

.field private cY:I

.field private cZ:Lcom/android/calendar/e/am;

.field private ca:[Ljava/lang/String;

.field private cb:[Ljava/lang/String;

.field private cc:[Ljava/lang/String;

.field private cd:Z

.field private ce:Z

.field private final cf:Z

.field private cg:Z

.field private final ch:Z

.field private ci:I

.field private cj:I

.field private final ck:Ljava/util/ArrayList;

.field private cl:Z

.field private cm:Z

.field private cn:Lcom/android/calendar/dh;

.field private co:Lcom/android/calendar/dh;

.field private cp:Lcom/android/calendar/dh;

.field private cq:Z

.field private final cr:Landroid/graphics/Rect;

.field private final cs:Landroid/graphics/drawable/Drawable;

.field private final ct:Landroid/graphics/drawable/Drawable;

.field private cu:Landroid/graphics/Rect;

.field private cv:Landroid/graphics/drawable/Drawable;

.field private cw:Landroid/graphics/drawable/Drawable;

.field private cx:Landroid/graphics/drawable/Drawable;

.field private cy:I

.field private cz:I

.field protected d:Landroid/content/Context;

.field private dA:Lcom/android/calendar/dh;

.field private dB:J

.field private dC:Z

.field private dD:Z

.field private dE:Lcom/samsung/android/service/gesture/GestureManager;

.field private dF:Z

.field private dG:Z

.field private dH:Z

.field private dI:I

.field private dJ:I

.field private dK:I

.field private dL:I

.field private dM:I

.field private dN:F

.field private dO:I

.field private dP:Landroid/util/LruCache;

.field private dQ:Z

.field private dR:Ljava/lang/Runnable;

.field private dS:Ljava/lang/Runnable;

.field private dT:I

.field private dU:I

.field private dV:I

.field private dW:I

.field private dX:I

.field private dY:I

.field private dZ:I

.field private da:Landroid/app/DialogFragment;

.field private db:Lcom/android/calendar/dh;

.field private dc:J

.field private dd:J

.field private de:J

.field private final df:Lcom/android/calendar/al;

.field private final dg:Lcom/android/calendar/e/d;

.field private final dh:Landroid/view/GestureDetector;

.field private di:Z

.field private final dj:Landroid/widget/OverScroller;

.field private final dk:I

.field private dl:F

.field private final dm:Landroid/widget/EdgeEffect;

.field private final dn:Landroid/widget/EdgeEffect;

.field private do:Landroid/view/accessibility/AccessibilityManager;

.field private dp:Z

.field private dq:Z

.field private final dr:Ljava/lang/String;

.field private ds:I

.field private dt:I

.field private du:I

.field private dv:I

.field private dw:I

.field private dx:Z

.field private dy:Z

.field private dz:Z

.field e:Landroid/text/format/Time;

.field private ea:I

.field private final eb:Ljava/util/regex/Pattern;

.field private ec:Z

.field private ed:Ljava/lang/Runnable;

.field private ee:I

.field private ef:I

.field private eg:Z

.field private eh:Z

.field private ei:Lcom/samsung/android/service/gesture/GestureListener;

.field private ej:Ljava/lang/Runnable;

.field private ek:Ljava/lang/Runnable;

.field protected final h:Lcom/android/calendar/e/al;

.field protected i:I

.field protected final j:Landroid/content/res/Resources;

.field protected final k:I

.field protected final l:Landroid/graphics/drawable/Drawable;

.field protected final m:Landroid/graphics/drawable/Drawable;

.field protected final n:Landroid/graphics/drawable/Drawable;

.field protected final o:Landroid/graphics/drawable/Drawable;

.field public q:I

.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/high16 v7, 0x41600000    # 14.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 133
    const-class v0, Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    .line 138
    sput-boolean v3, Lcom/android/calendar/e/g;->s:Z

    .line 139
    sput-boolean v3, Lcom/android/calendar/e/g;->t:Z

    .line 143
    sput v6, Lcom/android/calendar/e/g;->u:F

    .line 147
    const/4 v0, 0x4

    sput v0, Lcom/android/calendar/e/g;->a:I

    .line 148
    const/4 v0, 0x3

    sput v0, Lcom/android/calendar/e/g;->b:I

    .line 150
    const/16 v0, 0x32

    sput v0, Lcom/android/calendar/e/g;->v:I

    .line 151
    const/16 v0, 0x8c

    sput v0, Lcom/android/calendar/e/g;->w:I

    .line 154
    const/16 v0, 0x64

    sput v0, Lcom/android/calendar/e/g;->y:I

    .line 156
    const/16 v0, 0x19

    sput v0, Lcom/android/calendar/e/g;->z:I

    .line 157
    const/16 v0, 0xc

    sput v0, Lcom/android/calendar/e/g;->A:I

    .line 158
    const/16 v0, 0x8

    sput v0, Lcom/android/calendar/e/g;->B:I

    .line 188
    const/16 v0, 0x80

    sput v0, Lcom/android/calendar/e/g;->L:I

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    sput-object v0, Lcom/android/calendar/e/g;->f:Ljava/lang/StringBuilder;

    .line 209
    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/android/calendar/e/g;->f:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/calendar/e/g;->g:Ljava/util/Formatter;

    .line 295
    sput v5, Lcom/android/calendar/e/g;->aM:I

    .line 306
    sput v3, Lcom/android/calendar/e/g;->aN:I

    .line 308
    const/16 v0, 0x22

    sput v0, Lcom/android/calendar/e/g;->aO:I

    .line 310
    sput v3, Lcom/android/calendar/e/g;->aP:I

    .line 311
    sput v4, Lcom/android/calendar/e/g;->aQ:I

    .line 312
    const/4 v0, 0x5

    sput v0, Lcom/android/calendar/e/g;->aR:I

    .line 320
    const/high16 v0, 0x41400000    # 12.0f

    sput v0, Lcom/android/calendar/e/g;->aS:F

    .line 321
    sput v7, Lcom/android/calendar/e/g;->aT:F

    .line 322
    sput v7, Lcom/android/calendar/e/g;->aU:F

    .line 323
    const/high16 v0, 0x41300000    # 11.0f

    sput v0, Lcom/android/calendar/e/g;->aV:F

    .line 324
    const/16 v0, 0xe

    sput v0, Lcom/android/calendar/e/g;->aW:I

    .line 327
    sput v4, Lcom/android/calendar/e/g;->aX:I

    .line 328
    sput v4, Lcom/android/calendar/e/g;->aY:I

    .line 329
    sput v5, Lcom/android/calendar/e/g;->aZ:I

    .line 330
    sput v5, Lcom/android/calendar/e/g;->ba:I

    .line 331
    sput v5, Lcom/android/calendar/e/g;->bb:I

    .line 332
    sput v4, Lcom/android/calendar/e/g;->bc:I

    .line 333
    sput v4, Lcom/android/calendar/e/g;->bd:I

    .line 334
    const/4 v0, 0x6

    sput v0, Lcom/android/calendar/e/g;->be:I

    .line 335
    const/4 v0, 0x6

    sput v0, Lcom/android/calendar/e/g;->bf:I

    .line 339
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/android/calendar/e/g;->bh:F

    .line 340
    sput v6, Lcom/android/calendar/e/g;->bi:F

    .line 365
    sput v3, Lcom/android/calendar/e/g;->bE:I

    .line 397
    sput v3, Lcom/android/calendar/e/g;->bR:I

    .line 409
    sput-boolean v3, Lcom/android/calendar/e/g;->bU:Z

    .line 458
    sput v3, Lcom/android/calendar/e/g;->cE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/al;Lcom/android/calendar/dj;I)V
    .locals 11

    .prologue
    const/4 v6, -0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 588
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 161
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->D:Z

    .line 162
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->c:Z

    .line 164
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->F:Z

    .line 165
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->G:Z

    .line 166
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->H:Z

    .line 167
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->I:Z

    .line 168
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->J:Z

    .line 190
    new-instance v0, Lcom/android/calendar/e/x;

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/e/x;-><init>(Lcom/android/calendar/e/g;Lcom/android/calendar/e/h;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->M:Lcom/android/calendar/e/x;

    .line 197
    new-instance v0, Lcom/android/calendar/e/ah;

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/e/ah;-><init>(Lcom/android/calendar/e/g;Lcom/android/calendar/e/h;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->O:Lcom/android/calendar/e/ah;

    .line 200
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/android/calendar/e/g;->Q:Landroid/graphics/Typeface;

    .line 211
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->W:Z

    .line 212
    iput v1, p0, Lcom/android/calendar/e/g;->aa:I

    .line 215
    new-instance v0, Lcom/android/calendar/e/h;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/h;-><init>(Lcom/android/calendar/e/g;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    .line 239
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->af:Ljava/util/ArrayList;

    .line 240
    iput-object v3, p0, Lcom/android/calendar/e/g;->ag:[Landroid/text/StaticLayout;

    .line 241
    iput-object v3, p0, Lcom/android/calendar/e/g;->ah:[Landroid/text/StaticLayout;

    .line 261
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->at:Landroid/graphics/Rect;

    .line 262
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->au:Landroid/graphics/Rect;

    .line 263
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->av:Landroid/graphics/Rect;

    .line 266
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->aw:Landroid/graphics/Paint;

    .line 267
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    .line 268
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ay:Landroid/graphics/Paint;

    .line 269
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->az:Landroid/graphics/Paint;

    .line 270
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->aA:Landroid/graphics/Paint;

    .line 271
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->aB:Landroid/graphics/Paint;

    .line 283
    new-instance v0, Lcom/android/calendar/e/z;

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/e/z;-><init>(Lcom/android/calendar/e/g;Lcom/android/calendar/e/h;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->aI:Lcom/android/calendar/e/z;

    .line 284
    new-instance v0, Lcom/android/calendar/e/aa;

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/e/aa;-><init>(Lcom/android/calendar/e/g;Lcom/android/calendar/e/h;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    .line 286
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->aK:Z

    .line 357
    iput v10, p0, Lcom/android/calendar/e/g;->bw:F

    .line 364
    iput v6, p0, Lcom/android/calendar/e/g;->bD:I

    .line 374
    iput v10, p0, Lcom/android/calendar/e/g;->bI:F

    .line 375
    iput v10, p0, Lcom/android/calendar/e/g;->bJ:F

    .line 380
    iput v10, p0, Lcom/android/calendar/e/g;->bM:F

    .line 381
    iput v10, p0, Lcom/android/calendar/e/g;->bN:F

    .line 387
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->bO:Z

    .line 405
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/calendar/e/g;->bT:I

    .line 411
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/e/g;->i:I

    .line 412
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/calendar/e/g;->bV:I

    .line 419
    iput v6, p0, Lcom/android/calendar/e/g;->bY:I

    .line 429
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/e/g;->ch:Z

    .line 432
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    .line 438
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 439
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    .line 448
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    .line 459
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->cF:Landroid/graphics/RectF;

    .line 462
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cH:Z

    .line 464
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cI:Z

    .line 482
    sget-object v0, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 491
    sget-object v0, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 501
    iput v1, p0, Lcom/android/calendar/e/g;->cP:I

    .line 503
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cQ:Z

    .line 509
    iput v1, p0, Lcom/android/calendar/e/g;->cT:I

    .line 510
    iput v1, p0, Lcom/android/calendar/e/g;->cU:I

    .line 511
    iput v1, p0, Lcom/android/calendar/e/g;->cV:I

    .line 512
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cW:Z

    .line 513
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cX:Z

    .line 518
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/calendar/e/g;->dc:J

    .line 519
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/calendar/e/g;->dd:J

    .line 520
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/calendar/e/g;->de:J

    .line 532
    iput-object v3, p0, Lcom/android/calendar/e/g;->do:Landroid/view/accessibility/AccessibilityManager;

    .line 533
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dp:Z

    .line 534
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dq:Z

    .line 543
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dy:Z

    .line 551
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dC:Z

    .line 552
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dD:Z

    .line 556
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dF:Z

    .line 557
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dG:Z

    .line 558
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dH:Z

    .line 571
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/calendar/e/g;->dK:I

    .line 580
    iput v6, p0, Lcom/android/calendar/e/g;->dO:I

    .line 584
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dQ:Z

    .line 1814
    new-instance v0, Lcom/android/calendar/e/p;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/p;-><init>(Lcom/android/calendar/e/g;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    .line 1859
    new-instance v0, Lcom/android/calendar/e/q;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/q;-><init>(Lcom/android/calendar/e/g;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->dS:Ljava/lang/Runnable;

    .line 3766
    const-string v0, "[\t\n],"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->eb:Ljava/util/regex/Pattern;

    .line 4612
    new-instance v0, Lcom/android/calendar/e/v;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/v;-><init>(Lcom/android/calendar/e/g;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ed:Ljava/lang/Runnable;

    .line 5536
    iput v6, p0, Lcom/android/calendar/e/g;->q:I

    .line 6421
    new-instance v0, Lcom/android/calendar/e/l;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/l;-><init>(Lcom/android/calendar/e/g;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ei:Lcom/samsung/android/service/gesture/GestureListener;

    .line 6471
    new-instance v0, Lcom/android/calendar/e/m;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/m;-><init>(Lcom/android/calendar/e/g;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ej:Ljava/lang/Runnable;

    .line 6483
    new-instance v0, Lcom/android/calendar/e/n;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/n;-><init>(Lcom/android/calendar/e/g;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->ek:Ljava/lang/Runnable;

    .line 589
    iput-object p1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    .line 590
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    .line 591
    new-instance v0, Lcom/android/calendar/e/y;

    iget-object v3, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/e/y;-><init>(Lcom/android/calendar/e/g;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->cJ:Landroid/content/AsyncQueryHandler;

    .line 592
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    .line 593
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0f01b1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->dr:Ljava/lang/String;

    .line 594
    iput p4, p0, Lcom/android/calendar/e/g;->i:I

    .line 595
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    if-le v0, v2, :cond_5

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/e/g;->cg:Z

    .line 597
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0205

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    sput v0, Lcom/android/calendar/e/g;->aU:F

    .line 598
    sget v0, Lcom/android/calendar/e/g;->aU:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    float-to-int v0, v0

    neg-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aP:I

    .line 599
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c006c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    sput v0, Lcom/android/calendar/e/g;->aV:F

    .line 600
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0d000f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->cY:I

    .line 602
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0204

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aQ:I

    .line 604
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0071

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->aS:F

    .line 605
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0072

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->aT:F

    .line 606
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0341

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->v:I

    .line 608
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cg:Z

    if-eqz v0, :cond_6

    .line 609
    sget v0, Lcom/android/calendar/e/g;->v:I

    int-to-double v4, v0

    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    int-to-double v6, v0

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    div-double/2addr v6, v8

    mul-double/2addr v4, v6

    double-to-int v0, v4

    sput v0, Lcom/android/calendar/e/g;->w:I

    .line 613
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c01f1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->bF:I

    .line 615
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c01f5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->bc:I

    .line 616
    sget v0, Lcom/android/calendar/e/g;->bc:I

    sput v0, Lcom/android/calendar/e/g;->bd:I

    .line 618
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c01f4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->be:I

    .line 620
    sget v0, Lcom/android/calendar/e/g;->be:I

    sput v0, Lcom/android/calendar/e/g;->bf:I

    .line 622
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0344

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    sput v0, Lcom/android/calendar/e/g;->bi:F

    .line 624
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0342

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->bj:I

    .line 625
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0343

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->bk:I

    .line 626
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0345

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->bg:I

    .line 628
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00ca

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->z:I

    .line 629
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00c9

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->A:I

    .line 630
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00cb

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->B:I

    .line 632
    sget v0, Lcom/android/calendar/e/g;->u:F

    cmpl-float v0, v0, v10

    if-nez v0, :cond_1

    .line 633
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/android/calendar/e/g;->u:F

    .line 634
    sget v0, Lcom/android/calendar/e/g;->u:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    .line 635
    sget v0, Lcom/android/calendar/e/g;->aN:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aN:I

    .line 636
    sget v0, Lcom/android/calendar/e/g;->aO:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aO:I

    .line 638
    sget v0, Lcom/android/calendar/e/g;->aW:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aW:I

    .line 640
    sget v0, Lcom/android/calendar/e/g;->y:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->y:I

    .line 641
    sget v0, Lcom/android/calendar/e/g;->bR:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->bR:I

    .line 642
    sget v0, Lcom/android/calendar/e/g;->aX:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aX:I

    .line 643
    sget v0, Lcom/android/calendar/e/g;->aY:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aY:I

    .line 644
    sget v0, Lcom/android/calendar/e/g;->aZ:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aZ:I

    .line 645
    sget v0, Lcom/android/calendar/e/g;->ba:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->ba:I

    .line 646
    sget v0, Lcom/android/calendar/e/g;->bb:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->bb:I

    .line 647
    sget v0, Lcom/android/calendar/e/g;->aM:I

    int-to-float v0, v0

    sget v3, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/g;->aM:I

    .line 650
    :cond_1
    sput v1, Lcom/android/calendar/e/g;->bR:I

    .line 652
    const/4 v0, 0x7

    if-ne p4, v0, :cond_2

    .line 653
    sget v0, Lcom/android/calendar/e/g;->b:I

    sput v0, Lcom/android/calendar/e/g;->a:I

    .line 656
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b0047

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->k:I

    .line 657
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v3, p0, Lcom/android/calendar/e/g;->k:I

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->l:Landroid/graphics/drawable/Drawable;

    .line 658
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f020102

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->m:Landroid/graphics/drawable/Drawable;

    .line 659
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0201f6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->n:Landroid/graphics/drawable/Drawable;

    .line 661
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0201f9

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->o:Landroid/graphics/drawable/Drawable;

    .line 663
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v0, :cond_7

    .line 664
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b0102

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 665
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v3, p0, Lcom/android/calendar/e/g;->cs:Landroid/graphics/drawable/Drawable;

    .line 675
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f020064

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->ct:Landroid/graphics/drawable/Drawable;

    .line 676
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f020001

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->cx:Landroid/graphics/drawable/Drawable;

    .line 677
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0346

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->cy:I

    .line 679
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f02015d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->cv:Landroid/graphics/drawable/Drawable;

    .line 680
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f02015e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->cw:Landroid/graphics/drawable/Drawable;

    .line 681
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0349

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->cz:I

    .line 682
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0348

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->cA:I

    .line 684
    iput-object p3, p0, Lcom/android/calendar/e/g;->aL:Lcom/android/calendar/dj;

    .line 685
    new-instance v0, Lcom/android/calendar/e/al;

    invoke-direct {v0}, Lcom/android/calendar/e/al;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    .line 686
    iget-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    sget v3, Lcom/android/calendar/e/g;->bF:I

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/android/calendar/e/al;->b(F)V

    .line 687
    iget-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Lcom/android/calendar/e/al;->a(F)V

    .line 688
    iget-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/al;->a(I)V

    .line 689
    iget-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    sget v2, Lcom/android/calendar/e/g;->aM:I

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/al;->b(I)V

    .line 690
    iget-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    iget v2, p0, Lcom/android/calendar/e/g;->cz:I

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/al;->c(I)V

    .line 691
    iget-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/al;->d(I)V

    .line 692
    new-instance v2, Lcom/android/calendar/cj;

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v2, v0, v1}, Lcom/android/calendar/cj;-><init>(Landroid/app/Activity;Z)V

    iput-object v2, p0, Lcom/android/calendar/e/g;->cD:Lcom/android/calendar/cj;

    .line 693
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/e/g;->K:J

    .line 694
    iput-object p2, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    move-object v0, p1

    .line 695
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 696
    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/e/d;

    iput-object v0, p0, Lcom/android/calendar/e/g;->dg:Lcom/android/calendar/e/d;

    .line 697
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/android/calendar/e/w;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/w;-><init>(Lcom/android/calendar/e/g;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->dh:Landroid/view/GestureDetector;

    .line 698
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 699
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/g;->dk:I

    .line 700
    new-instance v1, Landroid/widget/OverScroller;

    invoke-direct {v1, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/e/g;->dj:Landroid/widget/OverScroller;

    .line 701
    new-instance v1, Landroid/widget/EdgeEffect;

    invoke-direct {v1, p1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/e/g;->dm:Landroid/widget/EdgeEffect;

    .line 702
    new-instance v1, Landroid/widget/EdgeEffect;

    invoke-direct {v1, p1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/e/g;->dn:Landroid/widget/EdgeEffect;

    .line 703
    sget v1, Lcom/android/calendar/e/g;->bE:I

    if-nez v1, :cond_3

    .line 704
    sget v1, Lcom/android/calendar/e/g;->v:I

    sput v1, Lcom/android/calendar/e/g;->bE:I

    .line 706
    :cond_3
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/g;->dI:I

    .line 707
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->dJ:I

    .line 708
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    iput-object v0, p0, Lcom/android/calendar/e/g;->ac:Lcom/android/calendar/AllInOneActivity;

    .line 709
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->a(Landroid/content/Context;)V

    .line 710
    return-void

    :cond_4
    move v0, v1

    .line 429
    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 595
    goto/16 :goto_1

    .line 611
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c0347

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->w:I

    goto/16 :goto_2

    .line 667
    :cond_7
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v3, "HK"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v3, "TW"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 669
    :cond_8
    sget v0, Lcom/android/calendar/e/g;->aV:F

    float-to-double v4, v0

    const-wide v6, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v4, v6

    double-to-int v0, v4

    int-to-float v0, v0

    sput v0, Lcom/android/calendar/e/g;->aV:F

    .line 671
    :cond_9
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 672
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 673
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0201f4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->cs:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_3
.end method

.method private A()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v1, 0x17

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2208
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    if-gez v0, :cond_0

    .line 2209
    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 2210
    iget v0, p0, Lcom/android/calendar/e/g;->bS:I

    if-lez v0, :cond_0

    .line 2211
    iput-object v4, p0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    .line 2212
    iput-boolean v3, p0, Lcom/android/calendar/e/g;->ak:Z

    .line 2216
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    if-le v0, v1, :cond_1

    .line 2217
    iput v1, p0, Lcom/android/calendar/e/g;->aj:I

    .line 2222
    :cond_1
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    iget v1, p0, Lcom/android/calendar/e/g;->bY:I

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_4

    .line 2230
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    iget v1, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v0, v1

    .line 2231
    iget v1, p0, Lcom/android/calendar/e/g;->bS:I

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/android/calendar/e/g;->T:[I

    aget v0, v1, v0

    iget v1, p0, Lcom/android/calendar/e/g;->aj:I

    if-le v0, v1, :cond_3

    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    if-lez v0, :cond_3

    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_3

    .line 2233
    iput-object v4, p0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    .line 2234
    iput-boolean v3, p0, Lcom/android/calendar/e/g;->ak:Z

    .line 2235
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 2260
    :cond_2
    :goto_0
    return-void

    .line 2239
    :cond_3
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    if-lez v0, :cond_4

    .line 2240
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/e/g;->bY:I

    .line 2241
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    .line 2242
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    if-gez v0, :cond_2

    .line 2243
    iput v2, p0, Lcom/android/calendar/e/g;->by:I

    goto :goto_0

    .line 2249
    :cond_4
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    iget v1, p0, Lcom/android/calendar/e/g;->bY:I

    iget v2, p0, Lcom/android/calendar/e/g;->bV:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x3

    if-le v0, v1, :cond_2

    .line 2250
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    iget v1, p0, Lcom/android/calendar/e/g;->bV:I

    rsub-int/lit8 v1, v1, 0x18

    if-ge v0, v1, :cond_5

    .line 2251
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/e/g;->bY:I

    .line 2252
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    .line 2253
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iget v1, p0, Lcom/android/calendar/e/g;->bA:I

    if-le v0, v1, :cond_2

    .line 2254
    iget v0, p0, Lcom/android/calendar/e/g;->bA:I

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    goto :goto_0

    .line 2256
    :cond_5
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    iget v1, p0, Lcom/android/calendar/e/g;->bV:I

    rsub-int/lit8 v1, v1, 0x18

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/android/calendar/e/g;->bZ:I

    if-lez v0, :cond_2

    .line 2257
    iget v0, p0, Lcom/android/calendar/e/g;->bA:I

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    goto :goto_0
.end method

.method static synthetic A(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/calendar/e/g;->w()V

    return-void
.end method

.method static synthetic B(Lcom/android/calendar/e/g;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    return-object v0
.end method

.method private B()V
    .locals 8

    .prologue
    .line 2380
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_2

    :cond_0
    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-nez v0, :cond_2

    .line 2381
    invoke-direct {p0}, Lcom/android/calendar/e/g;->O()V

    .line 2399
    :cond_1
    :goto_0
    return-void

    .line 2385
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_1

    .line 2386
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v0, :cond_1

    .line 2387
    const/4 v1, 0x0

    .line 2388
    iget-object v0, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 2389
    iget-wide v4, v0, Lcom/android/calendar/dh;->q:J

    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v6, v0, Lcom/android/calendar/dh;->q:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 2390
    const/4 v0, 0x1

    .line 2394
    :goto_1
    if-nez v0, :cond_1

    .line 2395
    invoke-direct {p0}, Lcom/android/calendar/e/g;->O()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method static synthetic C(Lcom/android/calendar/e/g;)Lcom/android/calendar/e/ah;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->O:Lcom/android/calendar/e/ah;

    return-object v0
.end method

.method private C()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2435
    .line 2436
    iget-object v1, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    .line 2438
    iget v2, p0, Lcom/android/calendar/e/g;->S:I

    iget v3, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    new-array v4, v2, [I

    .line 2439
    invoke-static {v4, v0}, Ljava/util/Arrays;->fill([II)V

    .line 2440
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 2441
    iget v2, v0, Lcom/android/calendar/dh;->i:I

    iget v3, p0, Lcom/android/calendar/e/g;->S:I

    if-gt v2, v3, :cond_0

    iget v2, v0, Lcom/android/calendar/dh;->j:I

    iget v3, p0, Lcom/android/calendar/e/g;->R:I

    if-lt v2, v3, :cond_0

    .line 2444
    invoke-virtual {v0}, Lcom/android/calendar/dh;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2446
    iget v2, v0, Lcom/android/calendar/dh;->i:I

    iget v3, p0, Lcom/android/calendar/e/g;->R:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2447
    iget v3, v0, Lcom/android/calendar/dh;->j:I

    iget v6, p0, Lcom/android/calendar/e/g;->S:I

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v3, v2

    move v2, v1

    .line 2448
    :goto_1
    if-gt v3, v6, :cond_1

    .line 2449
    iget v1, p0, Lcom/android/calendar/e/g;->R:I

    sub-int v7, v3, v1

    aget v1, v4, v7

    add-int/lit8 v1, v1, 0x1

    aput v1, v4, v7

    .line 2450
    if-ge v2, v1, :cond_7

    .line 2448
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_1

    .line 2455
    :cond_1
    iget v0, v0, Lcom/android/calendar/dh;->i:I

    iget v1, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v0, v1

    .line 2456
    if-gez v0, :cond_2

    :cond_2
    move v0, v2

    :goto_3
    move v1, v0

    .line 2474
    goto :goto_0

    .line 2460
    :cond_3
    iget v2, v0, Lcom/android/calendar/dh;->i:I

    iget v3, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v2, v3

    .line 2461
    iget v3, v0, Lcom/android/calendar/dh;->k:I

    div-int/lit8 v3, v3, 0x3c

    .line 2462
    if-ltz v2, :cond_4

    iget-object v6, p0, Lcom/android/calendar/e/g;->T:[I

    aget v6, v6, v2

    if-ge v3, v6, :cond_4

    .line 2463
    iget-object v6, p0, Lcom/android/calendar/e/g;->T:[I

    aput v3, v6, v2

    .line 2468
    :cond_4
    iget v2, v0, Lcom/android/calendar/dh;->j:I

    iget v3, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v2, v3

    .line 2469
    iget v0, v0, Lcom/android/calendar/dh;->l:I

    div-int/lit8 v0, v0, 0x3c

    .line 2470
    iget v3, p0, Lcom/android/calendar/e/g;->i:I

    if-ge v2, v3, :cond_5

    iget-object v3, p0, Lcom/android/calendar/e/g;->T:[I

    aget v3, v3, v2

    if-ge v0, v3, :cond_5

    .line 2471
    iget-object v3, p0, Lcom/android/calendar/e/g;->T:[I

    aput v0, v3, v2

    :cond_5
    move v0, v1

    goto :goto_3

    .line 2475
    :cond_6
    iput v1, p0, Lcom/android/calendar/e/g;->bS:I

    .line 2476
    return-void

    :cond_7
    move v1, v2

    goto :goto_2
.end method

.method private D()V
    .locals 4

    .prologue
    .line 3068
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3070
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-eqz v0, :cond_1

    .line 3071
    invoke-direct {p0}, Lcom/android/calendar/e/g;->F()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    if-eqz v0, :cond_2

    .line 3072
    :cond_0
    iget-wide v0, p0, Lcom/android/calendar/e/g;->dc:J

    iget-object v2, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget-wide v2, v2, Lcom/android/calendar/dh;->q:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 3073
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 3074
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3075
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3076
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dS:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3089
    :cond_1
    :goto_0
    return-void

    .line 3080
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 3081
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3082
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3083
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dS:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic D(Lcom/android/calendar/e/g;)Z
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v0

    return v0
.end method

.method private E()Z
    .locals 1

    .prologue
    .line 3092
    iget v0, p0, Lcom/android/calendar/e/g;->cP:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->ak:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/e/g;->F()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic E(Lcom/android/calendar/e/g;)Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->J:Z

    return v0
.end method

.method static synthetic F(Lcom/android/calendar/e/g;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->cM:I

    return v0
.end method

.method private F()Z
    .locals 1

    .prologue
    .line 3213
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cq:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic G(Lcom/android/calendar/e/g;)J
    .locals 2

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/android/calendar/e/g;->cK:J

    return-wide v0
.end method

.method private G()V
    .locals 39

    .prologue
    .line 3328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v28

    .line 3329
    if-eqz v28, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/e/g;->F()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3608
    :cond_0
    :goto_0
    return-void

    .line 3334
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 3335
    const/4 v4, 0x0

    iput-object v4, v2, Lcom/android/calendar/dh;->I:Lcom/android/calendar/dh;

    .line 3336
    const/4 v4, 0x0

    iput-object v4, v2, Lcom/android/calendar/dh;->J:Lcom/android/calendar/dh;

    .line 3337
    const/4 v4, 0x0

    iput-object v4, v2, Lcom/android/calendar/dh;->H:Lcom/android/calendar/dh;

    .line 3338
    const/4 v4, 0x0

    iput-object v4, v2, Lcom/android/calendar/dh;->G:Lcom/android/calendar/dh;

    goto :goto_1

    .line 3341
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 3342
    const v12, 0x186a0

    .line 3343
    const v11, 0x186a0

    .line 3344
    const/4 v9, 0x0

    .line 3349
    const/4 v8, 0x0

    .line 3350
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/e/g;->getCurrentSelectionPosition()Landroid/graphics/Rect;

    move-result-object v29

    .line 3351
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    if-eqz v3, :cond_9

    .line 3352
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    iget v3, v3, Lcom/android/calendar/dh;->E:F

    float-to-int v3, v3

    .line 3353
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    iget v4, v4, Lcom/android/calendar/dh;->F:F

    float-to-int v6, v4

    .line 3354
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    iget v4, v4, Lcom/android/calendar/dh;->C:F

    float-to-int v5, v4

    .line 3355
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    iget v4, v4, Lcom/android/calendar/dh;->D:F

    float-to-int v4, v4

    .line 3359
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v7, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    if-le v6, v7, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    if-le v4, v7, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    if-lt v5, v7, :cond_7

    .line 3361
    :cond_3
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    .line 3362
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v6, v3, Landroid/graphics/Rect;->top:I

    .line 3363
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v5, v3, Landroid/graphics/Rect;->bottom:I

    .line 3364
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    .line 3365
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    .line 3384
    :goto_2
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/Rect;->right:I

    if-lt v5, v3, :cond_a

    .line 3386
    const/16 v8, 0x8

    .line 3387
    add-int v3, v7, v6

    div-int/lit8 v3, v3, 0x2

    move v9, v8

    move v8, v3

    .line 3404
    :cond_4
    :goto_3
    const/4 v3, 0x0

    move/from16 v27, v3

    move-object v14, v2

    :goto_4
    move/from16 v0, v27

    move/from16 v1, v28

    if-ge v0, v1, :cond_2a

    .line 3405
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    move/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 3407
    iget v0, v2, Lcom/android/calendar/dh;->k:I

    move/from16 v30, v0

    .line 3408
    iget v0, v2, Lcom/android/calendar/dh;->l:I

    move/from16 v31, v0

    .line 3409
    iget v3, v2, Lcom/android/calendar/dh;->C:F

    float-to-int v0, v3

    move/from16 v32, v0

    .line 3410
    iget v3, v2, Lcom/android/calendar/dh;->D:F

    float-to-int v0, v3

    move/from16 v33, v0

    .line 3411
    iget v3, v2, Lcom/android/calendar/dh;->E:F

    float-to-int v3, v3

    .line 3412
    move-object/from16 v0, v29

    iget v10, v0, Landroid/graphics/Rect;->top:I

    if-ge v3, v10, :cond_33

    .line 3413
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/Rect;->top:I

    move v10, v3

    .line 3415
    :goto_5
    iget v3, v2, Lcom/android/calendar/dh;->F:F

    float-to-int v3, v3

    .line 3416
    move-object/from16 v0, v29

    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    if-le v3, v13, :cond_32

    .line 3417
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v3

    .line 3430
    :goto_6
    const/16 v22, 0x2710

    .line 3431
    const/16 v21, 0x2710

    .line 3432
    const/16 v20, 0x2710

    .line 3433
    const/16 v19, 0x2710

    .line 3434
    const/16 v18, 0x0

    .line 3435
    const/16 v17, 0x0

    .line 3436
    const/16 v16, 0x0

    .line 3437
    const/4 v15, 0x0

    .line 3441
    const/4 v3, 0x0

    .line 3442
    const/4 v13, 0x0

    .line 3443
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v9, v0, :cond_e

    .line 3444
    move/from16 v0, v32

    if-lt v0, v8, :cond_d

    .line 3445
    sub-int v3, v32, v8

    .line 3449
    :cond_5
    :goto_7
    sub-int v13, v10, v6

    move/from16 v38, v13

    move v13, v3

    move/from16 v3, v38

    .line 3472
    :goto_8
    if-lt v13, v12, :cond_6

    if-ne v13, v12, :cond_30

    if-ge v3, v11, :cond_30

    :cond_6
    move v11, v3

    move v12, v13

    move-object v13, v2

    .line 3481
    :goto_9
    const/4 v3, 0x0

    move/from16 v25, v3

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move/from16 v18, v19

    move/from16 v19, v20

    move/from16 v20, v21

    move/from16 v21, v22

    :goto_a
    move/from16 v0, v25

    move/from16 v1, v28

    if-ge v0, v1, :cond_29

    .line 3482
    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_17

    move-object v3, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move/from16 v17, v18

    move/from16 v18, v19

    move/from16 v19, v20

    move/from16 v20, v21

    .line 3481
    :goto_b
    add-int/lit8 v21, v25, 0x1

    move/from16 v25, v21

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v17

    move-object/from16 v17, v16

    move-object/from16 v16, v15

    move-object v15, v14

    move-object v14, v3

    goto :goto_a

    .line 3368
    :cond_7
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    if-ge v3, v7, :cond_8

    .line 3369
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    .line 3371
    :cond_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    if-le v6, v7, :cond_34

    .line 3372
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    move v7, v3

    goto/16 :goto_2

    .line 3377
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v6, v3, Landroid/graphics/Rect;->top:I

    .line 3378
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v5, v3, Landroid/graphics/Rect;->bottom:I

    .line 3379
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    .line 3380
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    goto/16 :goto_2

    .line 3388
    :cond_a
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/Rect;->left:I

    if-gt v4, v3, :cond_b

    .line 3390
    const/4 v8, 0x4

    .line 3391
    add-int v3, v7, v6

    div-int/lit8 v3, v3, 0x2

    move v9, v8

    move v8, v3

    goto/16 :goto_3

    .line 3392
    :cond_b
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/Rect;->top:I

    if-gt v6, v3, :cond_c

    .line 3394
    const/4 v8, 0x1

    .line 3395
    add-int v3, v5, v4

    div-int/lit8 v3, v3, 0x2

    move v9, v8

    move v8, v3

    goto/16 :goto_3

    .line 3396
    :cond_c
    move-object/from16 v0, v29

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    if-lt v7, v3, :cond_4

    .line 3398
    const/4 v8, 0x2

    .line 3399
    add-int v3, v5, v4

    div-int/lit8 v3, v3, 0x2

    move v9, v8

    move v8, v3

    goto/16 :goto_3

    .line 3446
    :cond_d
    move/from16 v0, v33

    if-gt v0, v8, :cond_5

    .line 3447
    sub-int v3, v8, v33

    goto/16 :goto_7

    .line 3450
    :cond_e
    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v9, v0, :cond_11

    .line 3451
    move/from16 v0, v32

    if-lt v0, v8, :cond_10

    .line 3452
    sub-int v3, v32, v8

    .line 3456
    :cond_f
    :goto_c
    sub-int v13, v7, v26

    move/from16 v38, v13

    move v13, v3

    move/from16 v3, v38

    goto/16 :goto_8

    .line 3453
    :cond_10
    move/from16 v0, v33

    if-gt v0, v8, :cond_f

    .line 3454
    sub-int v3, v8, v33

    goto :goto_c

    .line 3457
    :cond_11
    const/16 v23, 0x4

    move/from16 v0, v23

    if-ne v9, v0, :cond_14

    .line 3458
    move/from16 v0, v26

    if-gt v0, v8, :cond_13

    .line 3459
    sub-int v3, v8, v26

    .line 3463
    :cond_12
    :goto_d
    sub-int v13, v32, v4

    move/from16 v38, v13

    move v13, v3

    move/from16 v3, v38

    goto/16 :goto_8

    .line 3460
    :cond_13
    if-lt v10, v8, :cond_12

    .line 3461
    sub-int v3, v10, v8

    goto :goto_d

    .line 3464
    :cond_14
    const/16 v23, 0x8

    move/from16 v0, v23

    if-ne v9, v0, :cond_31

    .line 3465
    move/from16 v0, v26

    if-gt v0, v8, :cond_16

    .line 3466
    sub-int v3, v8, v26

    .line 3470
    :cond_15
    :goto_e
    sub-int v13, v5, v33

    move/from16 v38, v13

    move v13, v3

    move/from16 v3, v38

    goto/16 :goto_8

    .line 3467
    :cond_16
    if-lt v10, v8, :cond_15

    .line 3468
    sub-int v3, v10, v8

    goto :goto_e

    .line 3485
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/calendar/dh;

    .line 3486
    iget v0, v3, Lcom/android/calendar/dh;->C:F

    move/from16 v22, v0

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v34, v0

    .line 3487
    iget v0, v3, Lcom/android/calendar/dh;->D:F

    move/from16 v22, v0

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v35, v0

    .line 3488
    iget v0, v3, Lcom/android/calendar/dh;->l:I

    move/from16 v22, v0

    move/from16 v0, v22

    move/from16 v1, v30

    if-gt v0, v1, :cond_1e

    .line 3491
    move/from16 v0, v34

    move/from16 v1, v33

    if-ge v0, v1, :cond_2d

    move/from16 v0, v35

    move/from16 v1, v32

    if-le v0, v1, :cond_2d

    .line 3492
    iget v0, v3, Lcom/android/calendar/dh;->l:I

    move/from16 v22, v0

    sub-int v22, v30, v22

    .line 3493
    move/from16 v0, v22

    move/from16 v1, v21

    if-ge v0, v1, :cond_1a

    move-object/from16 v17, v3

    move/from16 v21, v22

    :cond_18
    :goto_f
    move/from16 v22, v21

    move/from16 v21, v20

    .line 3552
    :goto_10
    move/from16 v0, v34

    move/from16 v1, v33

    if-lt v0, v1, :cond_25

    .line 3555
    add-int v20, v10, v26

    div-int/lit8 v23, v20, 0x2

    .line 3556
    const/16 v20, 0x0

    .line 3557
    iget v0, v3, Lcom/android/calendar/dh;->F:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    .line 3558
    iget v0, v3, Lcom/android/calendar/dh;->E:F

    move/from16 v35, v0

    move/from16 v0, v35

    float-to-int v0, v0

    move/from16 v35, v0

    .line 3559
    move/from16 v0, v24

    move/from16 v1, v23

    if-gt v0, v1, :cond_23

    .line 3560
    sub-int v20, v23, v24

    .line 3564
    :cond_19
    :goto_11
    move/from16 v0, v20

    move/from16 v1, v18

    if-ge v0, v1, :cond_24

    move/from16 v14, v20

    :goto_12
    move/from16 v18, v19

    move/from16 v20, v22

    move/from16 v19, v21

    move/from16 v38, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move/from16 v17, v38

    .line 3576
    goto/16 :goto_b

    .line 3496
    :cond_1a
    move/from16 v0, v22

    move/from16 v1, v21

    if-ne v0, v1, :cond_18

    .line 3497
    add-int v23, v32, v33

    div-int/lit8 v36, v23, 0x2

    .line 3498
    const/16 v23, 0x0

    .line 3499
    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/calendar/dh;->C:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    .line 3500
    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/calendar/dh;->D:F

    move/from16 v37, v0

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v37, v0

    .line 3501
    move/from16 v0, v37

    move/from16 v1, v36

    if-gt v0, v1, :cond_1c

    .line 3502
    sub-int v23, v36, v37

    move/from16 v24, v23

    .line 3507
    :goto_13
    const/16 v23, 0x0

    .line 3508
    move/from16 v0, v35

    move/from16 v1, v36

    if-gt v0, v1, :cond_1d

    .line 3509
    sub-int v23, v36, v35

    .line 3513
    :cond_1b
    :goto_14
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_18

    move-object/from16 v17, v3

    move/from16 v21, v22

    .line 3515
    goto/16 :goto_f

    .line 3503
    :cond_1c
    move/from16 v0, v24

    move/from16 v1, v36

    if-lt v0, v1, :cond_2f

    .line 3504
    sub-int v23, v24, v36

    move/from16 v24, v23

    goto :goto_13

    .line 3510
    :cond_1d
    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_1b

    .line 3511
    sub-int v23, v34, v36

    goto :goto_14

    .line 3519
    :cond_1e
    iget v0, v3, Lcom/android/calendar/dh;->k:I

    move/from16 v22, v0

    move/from16 v0, v22

    move/from16 v1, v31

    if-lt v0, v1, :cond_2d

    .line 3522
    move/from16 v0, v34

    move/from16 v1, v33

    if-ge v0, v1, :cond_2d

    move/from16 v0, v35

    move/from16 v1, v32

    if-le v0, v1, :cond_2d

    .line 3523
    iget v0, v3, Lcom/android/calendar/dh;->k:I

    move/from16 v22, v0

    sub-int v22, v22, v31

    .line 3524
    move/from16 v0, v22

    move/from16 v1, v20

    if-ge v0, v1, :cond_1f

    move-object/from16 v16, v3

    move/from16 v38, v22

    move/from16 v22, v21

    move/from16 v21, v38

    .line 3526
    goto/16 :goto_10

    .line 3527
    :cond_1f
    move/from16 v0, v22

    move/from16 v1, v20

    if-ne v0, v1, :cond_2d

    .line 3528
    add-int v23, v32, v33

    div-int/lit8 v36, v23, 0x2

    .line 3529
    const/16 v23, 0x0

    .line 3530
    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/calendar/dh;->C:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    .line 3531
    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/calendar/dh;->D:F

    move/from16 v37, v0

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v37, v0

    .line 3532
    move/from16 v0, v37

    move/from16 v1, v36

    if-gt v0, v1, :cond_21

    .line 3533
    sub-int v23, v36, v37

    move/from16 v24, v23

    .line 3538
    :goto_15
    const/16 v23, 0x0

    .line 3539
    move/from16 v0, v35

    move/from16 v1, v36

    if-gt v0, v1, :cond_22

    .line 3540
    sub-int v23, v36, v35

    .line 3544
    :cond_20
    :goto_16
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_2d

    move-object/from16 v16, v3

    move/from16 v38, v22

    move/from16 v22, v21

    move/from16 v21, v38

    .line 3546
    goto/16 :goto_10

    .line 3534
    :cond_21
    move/from16 v0, v24

    move/from16 v1, v36

    if-lt v0, v1, :cond_2e

    .line 3535
    sub-int v23, v24, v36

    move/from16 v24, v23

    goto :goto_15

    .line 3541
    :cond_22
    move/from16 v0, v34

    move/from16 v1, v36

    if-lt v0, v1, :cond_20

    .line 3542
    sub-int v23, v34, v36

    goto :goto_16

    .line 3561
    :cond_23
    move/from16 v0, v35

    move/from16 v1, v23

    if-lt v0, v1, :cond_19

    .line 3562
    sub-int v20, v35, v23

    goto/16 :goto_11

    .line 3567
    :cond_24
    move/from16 v0, v20

    move/from16 v1, v18

    if-ne v0, v1, :cond_2c

    .line 3569
    sub-int v23, v34, v33

    .line 3570
    iget v0, v14, Lcom/android/calendar/dh;->C:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    sub-int v24, v24, v33

    .line 3571
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_2c

    move/from16 v14, v20

    .line 3573
    goto/16 :goto_12

    .line 3576
    :cond_25
    move/from16 v0, v35

    move/from16 v1, v32

    if-gt v0, v1, :cond_2b

    .line 3579
    add-int v20, v10, v26

    div-int/lit8 v23, v20, 0x2

    .line 3580
    const/16 v20, 0x0

    .line 3581
    iget v0, v3, Lcom/android/calendar/dh;->F:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    .line 3582
    iget v0, v3, Lcom/android/calendar/dh;->E:F

    move/from16 v34, v0

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    .line 3583
    move/from16 v0, v24

    move/from16 v1, v23

    if-gt v0, v1, :cond_27

    .line 3584
    sub-int v20, v23, v24

    .line 3588
    :cond_26
    :goto_17
    move/from16 v0, v20

    move/from16 v1, v19

    if-ge v0, v1, :cond_28

    move-object/from16 v15, v16

    move/from16 v19, v21

    move-object/from16 v16, v17

    move/from16 v17, v18

    move/from16 v18, v20

    move/from16 v20, v22

    move-object/from16 v38, v3

    move-object v3, v14

    move-object/from16 v14, v38

    .line 3590
    goto/16 :goto_b

    .line 3585
    :cond_27
    move/from16 v0, v34

    move/from16 v1, v23

    if-lt v0, v1, :cond_26

    .line 3586
    sub-int v20, v34, v23

    goto :goto_17

    .line 3591
    :cond_28
    move/from16 v0, v20

    move/from16 v1, v19

    if-ne v0, v1, :cond_2b

    .line 3593
    sub-int v23, v32, v35

    .line 3594
    iget v0, v15, Lcom/android/calendar/dh;->D:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    sub-int v24, v32, v24

    .line 3595
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_2b

    move-object/from16 v15, v16

    move/from16 v19, v21

    move-object/from16 v16, v17

    move/from16 v17, v18

    move/from16 v18, v20

    move/from16 v20, v22

    move-object/from16 v38, v3

    move-object v3, v14

    move-object/from16 v14, v38

    .line 3597
    goto/16 :goto_b

    .line 3602
    :cond_29
    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/android/calendar/dh;->I:Lcom/android/calendar/dh;

    .line 3603
    move-object/from16 v0, v16

    iput-object v0, v2, Lcom/android/calendar/dh;->J:Lcom/android/calendar/dh;

    .line 3604
    iput-object v15, v2, Lcom/android/calendar/dh;->H:Lcom/android/calendar/dh;

    .line 3605
    iput-object v14, v2, Lcom/android/calendar/dh;->G:Lcom/android/calendar/dh;

    .line 3404
    add-int/lit8 v2, v27, 0x1

    move/from16 v27, v2

    move-object v14, v13

    goto/16 :goto_4

    .line 3607
    :cond_2a
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    goto/16 :goto_0

    :cond_2b
    move-object v3, v14

    move/from16 v20, v22

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move/from16 v17, v18

    move/from16 v18, v19

    move/from16 v19, v21

    goto/16 :goto_b

    :cond_2c
    move-object v3, v14

    move/from16 v14, v18

    goto/16 :goto_12

    :cond_2d
    move/from16 v22, v21

    move/from16 v21, v20

    goto/16 :goto_10

    :cond_2e
    move/from16 v24, v23

    goto/16 :goto_15

    :cond_2f
    move/from16 v24, v23

    goto/16 :goto_13

    :cond_30
    move-object v13, v14

    goto/16 :goto_9

    :cond_31
    move/from16 v38, v13

    move v13, v3

    move/from16 v3, v38

    goto/16 :goto_8

    :cond_32
    move/from16 v26, v3

    goto/16 :goto_6

    :cond_33
    move v10, v3

    goto/16 :goto_5

    :cond_34
    move v7, v3

    goto/16 :goto_2
.end method

.method private H()V
    .locals 10

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 4006
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/e/g;->cP:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/e/g;->cP:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 4008
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 4009
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aI:Lcom/android/calendar/e/z;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4060
    :cond_1
    :goto_0
    return-void

    .line 4013
    :cond_2
    iget-wide v0, p0, Lcom/android/calendar/e/g;->K:J

    iget-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v2, v2, Lcom/android/calendar/dh;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 4017
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v0, v0, Lcom/android/calendar/dh;->b:J

    iput-wide v0, p0, Lcom/android/calendar/e/g;->K:J

    .line 4020
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 4021
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aI:Lcom/android/calendar/e/z;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4024
    :cond_3
    iget-object v9, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 4025
    iget-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    const v1, 0x7f120034

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 4026
    iget-object v1, v9, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4028
    iget-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    const v1, 0x7f120081

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 4029
    iget-boolean v1, v9, Lcom/android/calendar/dh;->o:Z

    if-eqz v1, :cond_5

    move v1, v7

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4031
    iget-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    const v1, 0x7f120065

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 4032
    iget-boolean v1, v9, Lcom/android/calendar/dh;->p:Z

    if-eqz v1, :cond_6

    move v1, v7

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4035
    iget-boolean v0, v9, Lcom/android/calendar/dh;->f:Z

    if-eqz v0, :cond_7

    .line 4036
    const v6, 0x82012

    .line 4044
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-wide v2, v9, Lcom/android/calendar/dh;->m:J

    iget-wide v4, v9, Lcom/android/calendar/dh;->n:J

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    .line 4046
    iget-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    const v2, 0x7f120082

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 4047
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4049
    iget-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    const v1, 0x7f120066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 4050
    iget-object v1, v9, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 4051
    if-eqz v1, :cond_8

    :goto_4
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4052
    if-nez v1, :cond_4

    .line 4053
    iget-object v1, v9, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4056
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    const/16 v1, 0x53

    iget v2, p0, Lcom/android/calendar/e/g;->bW:I

    const/4 v3, 0x5

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 4057
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 4058
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aI:Lcom/android/calendar/e/z;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_5
    move v1, v8

    .line 4029
    goto :goto_1

    :cond_6
    move v1, v8

    .line 4032
    goto :goto_2

    .line 4039
    :cond_7
    const v6, 0x81413

    goto :goto_3

    :cond_8
    move v8, v7

    .line 4051
    goto :goto_4
.end method

.method private H(Lcom/android/calendar/e/g;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1254
    iget v2, p0, Lcom/android/calendar/e/g;->aj:I

    invoke-direct {p1, v2}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 1255
    iget-object v2, p1, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1256
    iput-boolean v0, p1, Lcom/android/calendar/e/g;->cl:Z

    .line 1257
    iget v2, p0, Lcom/android/calendar/e/g;->bY:I

    iput v2, p1, Lcom/android/calendar/e/g;->bY:I

    .line 1258
    iget v2, p0, Lcom/android/calendar/e/g;->bZ:I

    iput v2, p1, Lcom/android/calendar/e/g;->bZ:I

    .line 1259
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getHeight()I

    move-result v3

    invoke-direct {p1, v2, v3}, Lcom/android/calendar/e/g;->a(II)V

    .line 1261
    invoke-direct {p1, v4}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 1262
    iput-object v4, p1, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    .line 1263
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 1264
    iget v2, p0, Lcom/android/calendar/e/g;->aE:I

    iput v2, p1, Lcom/android/calendar/e/g;->aE:I

    .line 1265
    iget-object v2, p1, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->ak:Z

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p1, Lcom/android/calendar/e/g;->ak:Z

    .line 1270
    invoke-direct {p1}, Lcom/android/calendar/e/g;->u()V

    .line 1271
    return-void

    :cond_0
    move v0, v1

    .line 1265
    goto :goto_0
.end method

.method private I()V
    .locals 4

    .prologue
    .line 4342
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dG:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dF:Z

    if-eqz v0, :cond_0

    .line 4344
    :try_start_0
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v1, "register listener"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 4345
    iget-object v0, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ei:Lcom/samsung/android/service/gesture/GestureListener;

    const-string v2, "ir_provider"

    const-string v3, "air_motion_item_move"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;)V

    .line 4346
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->dH:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4351
    :cond_0
    :goto_0
    return-void

    .line 4347
    :catch_0
    move-exception v0

    .line 4348
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v1, "Fail to register gesture listener"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private J()V
    .locals 3

    .prologue
    .line 4354
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dG:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dF:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dH:Z

    if-eqz v0, :cond_0

    .line 4356
    :try_start_0
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v1, "unregister listener"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 4357
    iget-object v0, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ei:Lcom/samsung/android/service/gesture/GestureListener;

    const-string v2, "ir_provider"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 4358
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->dH:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4363
    :cond_0
    :goto_0
    return-void

    .line 4359
    :catch_0
    move-exception v0

    .line 4360
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v1, "Fail to unregister gesture listener"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private K()V
    .locals 0

    .prologue
    .line 4587
    return-void
.end method

.method private L()V
    .locals 4

    .prologue
    .line 4836
    iget-object v0, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/e/g;->aS:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 4837
    iget-object v0, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    .line 4838
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, p0, Lcom/android/calendar/e/g;->dL:I

    .line 4839
    iget-object v1, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 4840
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->dM:I

    .line 4841
    return-void
.end method

.method private M()Z
    .locals 2

    .prologue
    .line 4911
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dx:Z

    if-eqz v0, :cond_0

    .line 4912
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_0

    .line 4913
    const/4 v0, 0x1

    .line 4916
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private N()Z
    .locals 2

    .prologue
    .line 4925
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private O()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 4929
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dx:Z

    .line 4930
    sget-object v0, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 4931
    iput-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 4932
    iput v1, p0, Lcom/android/calendar/e/g;->dT:I

    .line 4933
    iput v1, p0, Lcom/android/calendar/e/g;->dU:I

    .line 4934
    iput v1, p0, Lcom/android/calendar/e/g;->dV:I

    .line 4935
    iput v1, p0, Lcom/android/calendar/e/g;->dW:I

    .line 4936
    iput v1, p0, Lcom/android/calendar/e/g;->dX:I

    .line 4937
    iput v1, p0, Lcom/android/calendar/e/g;->ea:I

    .line 4938
    iput v1, p0, Lcom/android/calendar/e/g;->dY:I

    .line 4939
    iput v1, p0, Lcom/android/calendar/e/g;->dZ:I

    .line 4940
    iput-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 4941
    iput v1, p0, Lcom/android/calendar/e/g;->cP:I

    .line 4942
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 4943
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->F:Z

    .line 4944
    sput-boolean v1, Lcom/android/calendar/e/g;->p:Z

    .line 4945
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dB:J

    .line 4946
    invoke-direct {p0}, Lcom/android/calendar/e/g;->J()V

    .line 4947
    iget-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_0

    .line 4948
    iget-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->g()V

    .line 4950
    :cond_0
    return-void
.end method

.method private P()Z
    .locals 1

    .prologue
    .line 4988
    invoke-direct {p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/e/g;->N()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Q()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5269
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_1

    .line 5270
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/g;->dW:I

    iget v1, p0, Lcom/android/calendar/e/g;->dZ:I

    iget v2, p0, Lcom/android/calendar/e/g;->ea:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->dW:I

    .line 5272
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_3

    .line 5273
    :cond_2
    iget v0, p0, Lcom/android/calendar/e/g;->dX:I

    iget v1, p0, Lcom/android/calendar/e/g;->dZ:I

    iget v2, p0, Lcom/android/calendar/e/g;->ea:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->dX:I

    .line 5275
    :cond_3
    iput v3, p0, Lcom/android/calendar/e/g;->ea:I

    .line 5276
    iput v3, p0, Lcom/android/calendar/e/g;->dY:I

    .line 5277
    iput v3, p0, Lcom/android/calendar/e/g;->dZ:I

    .line 5278
    sget-object v0, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 5279
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 5280
    return-void
.end method

.method private R()V
    .locals 3

    .prologue
    .line 6193
    iget-object v0, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    if-nez v0, :cond_0

    .line 6194
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dG:Z

    if-eqz v0, :cond_0

    .line 6195
    new-instance v0, Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    new-instance v2, Lcom/android/calendar/e/k;

    invoke-direct {v2, p0}, Lcom/android/calendar/e/k;-><init>(Lcom/android/calendar/e/g;)V

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;-><init>(Landroid/content/Context;Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    .line 6208
    :cond_0
    return-void
.end method

.method private S()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 6211
    iget-object v0, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    if-eqz v0, :cond_0

    .line 6213
    :try_start_0
    invoke-direct {p0}, Lcom/android/calendar/e/g;->J()V

    .line 6214
    iget-object v0, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    invoke-virtual {v0}, Lcom/samsung/android/service/gesture/GestureManager;->unbindFromService()V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6218
    iput-object v3, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    .line 6219
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->dF:Z

    .line 6222
    :cond_0
    :goto_0
    return-void

    .line 6215
    :catch_0
    move-exception v0

    .line 6216
    :try_start_1
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v1, "This devices doesn\'t include gesture api"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6218
    iput-object v3, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    .line 6219
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->dF:Z

    goto :goto_0

    .line 6218
    :catchall_0
    move-exception v0

    iput-object v3, p0, Lcom/android/calendar/e/g;->dE:Lcom/samsung/android/service/gesture/GestureManager;

    .line 6219
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->dF:Z

    throw v0
.end method

.method static synthetic a(Lcom/android/calendar/e/g;F)F
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/android/calendar/e/g;->dl:F

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/e/g;I)I
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/android/calendar/e/g;->ci:I

    return p1
.end method

.method private a(Landroid/graphics/Rect;Lcom/android/calendar/dh;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 3710
    invoke-virtual {p2}, Lcom/android/calendar/dh;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3763
    :cond_0
    :goto_0
    return-object p1

    .line 3714
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v0, :cond_0

    iget-wide v0, p2, Lcom/android/calendar/dh;->q:J

    iget-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v2, v2, Lcom/android/calendar/dh;->q:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3718
    invoke-direct {p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3719
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 3720
    iget v0, p2, Lcom/android/calendar/dh;->F:F

    iget v1, p2, Lcom/android/calendar/dh;->E:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    sget v1, Lcom/android/calendar/e/g;->aX:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->aY:I

    sub-int/2addr v0, v1

    .line 3721
    iget v1, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 3723
    iget v1, p0, Lcom/android/calendar/e/g;->bA:I

    iget v2, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v1, v2

    sget v2, Lcom/android/calendar/e/g;->bR:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/e/g;->bQ:I

    sub-int/2addr v1, v2

    .line 3724
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    if-le v2, v1, :cond_2

    .line 3725
    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 3727
    :cond_2
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 3729
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    .line 3730
    iget v1, p0, Lcom/android/calendar/e/g;->bW:I

    sget v2, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/calendar/e/g;->i:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 3731
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/android/calendar/e/g;->dY:I

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 3732
    iget v2, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 3733
    iget v0, p1, Landroid/graphics/Rect;->right:I

    if-le v0, v1, :cond_0

    .line 3734
    iput v1, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 3736
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_5

    .line 3737
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 3739
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    .line 3740
    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    .line 3742
    if-ge v0, v1, :cond_4

    .line 3743
    iget v0, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 3746
    :cond_4
    iget v0, p0, Lcom/android/calendar/e/g;->bA:I

    iget v1, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->bR:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->bQ:I

    sub-int/2addr v0, v1

    .line 3747
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    if-le v1, v0, :cond_0

    .line 3748
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_0

    .line 3750
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_0

    .line 3751
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 3753
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    .line 3754
    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    .line 3756
    if-ge v0, v1, :cond_6

    .line 3757
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 3759
    :cond_6
    iget v0, p1, Landroid/graphics/Rect;->top:I

    if-gez v0, :cond_0

    .line 3760
    const/4 v0, 0x0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    goto/16 :goto_0
.end method

.method private a(Lcom/android/calendar/dh;)Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 3311
    iget-object v0, p0, Lcom/android/calendar/e/g;->at:Landroid/graphics/Rect;

    .line 3318
    iget v1, p1, Lcom/android/calendar/dh;->E:F

    float-to-int v1, v1

    sget v2, Lcom/android/calendar/e/g;->aX:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 3319
    iget v1, p1, Lcom/android/calendar/dh;->F:F

    float-to-int v1, v1

    sget v2, Lcom/android/calendar/e/g;->aY:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 3320
    iget v1, p1, Lcom/android/calendar/dh;->C:F

    float-to-int v1, v1

    sget v2, Lcom/android/calendar/e/g;->aZ:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 3321
    iget v1, p1, Lcom/android/calendar/dh;->D:F

    float-to-int v1, v1

    sget v2, Lcom/android/calendar/e/g;->ba:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 3322
    return-object v0
.end method

.method private a(Lcom/android/calendar/dh;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;II)Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/16 v6, 0xb2

    const/4 v3, 0x2

    .line 3632
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;)Landroid/graphics/Rect;

    move-result-object v1

    .line 3633
    invoke-direct {p0, v1, p1}, Lcom/android/calendar/e/g;->a(Landroid/graphics/Rect;Lcom/android/calendar/dh;)Landroid/graphics/Rect;

    .line 3634
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3638
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3639
    iget v0, p0, Lcom/android/calendar/e/g;->cP:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 3641
    iput-object p1, p0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    .line 3642
    sget v0, Lcom/android/calendar/e/g;->bl:I

    .line 3666
    :goto_0
    iget v2, p1, Lcom/android/calendar/dh;->B:I

    if-ne v2, v3, :cond_0

    .line 3667
    invoke-static {v0}, Lcom/android/calendar/hj;->c(I)I

    move-result v0

    .line 3670
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->d(I)I

    move-result v0

    .line 3671
    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 3673
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v0, :cond_9

    iget-wide v2, p1, Lcom/android/calendar/dh;->q:J

    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v4, v0, Lcom/android/calendar/dh;->q:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_9

    invoke-direct {p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/calendar/e/g;->N()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v2, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    if-ne v0, v2, :cond_9

    .line 3675
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->cF:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3676
    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 3677
    iget-object v0, p0, Lcom/android/calendar/e/g;->cF:Landroid/graphics/RectF;

    sget v2, Lcom/android/calendar/e/g;->bi:F

    sget v3, Lcom/android/calendar/e/g;->bi:F

    invoke-virtual {p2, v0, v2, v3, p3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 3678
    iget-object v0, p0, Lcom/android/calendar/e/g;->cF:Landroid/graphics/RectF;

    sget v2, Lcom/android/calendar/e/g;->bi:F

    sget v3, Lcom/android/calendar/e/g;->bi:F

    iget-object v4, p0, Lcom/android/calendar/e/g;->az:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 3680
    iget v0, p0, Lcom/android/calendar/e/g;->bk:I

    .line 3681
    div-int/lit8 v2, v0, 0x2

    .line 3683
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 3684
    iget v4, v1, Landroid/graphics/Rect;->left:I

    iget v5, v1, Landroid/graphics/Rect;->right:I

    iget v6, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    sub-int/2addr v4, v2

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 3685
    iget v4, v3, Landroid/graphics/RectF;->left:F

    iget v5, p0, Lcom/android/calendar/e/g;->bk:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 3686
    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v2

    iget v5, p0, Lcom/android/calendar/e/g;->bj:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 3687
    iget v4, v3, Landroid/graphics/RectF;->top:F

    int-to-float v5, v0

    add-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 3688
    iget-object v4, p0, Lcom/android/calendar/e/g;->aA:Landroid/graphics/Paint;

    invoke-virtual {p2, v3, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 3690
    iget v4, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v4, v2

    iget v4, p0, Lcom/android/calendar/e/g;->bj:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v2, v4

    int-to-float v2, v2

    iput v2, v3, Landroid/graphics/RectF;->top:F

    .line 3691
    iget v2, v3, Landroid/graphics/RectF;->top:F

    int-to-float v0, v0

    add-float/2addr v0, v2

    iput v0, v3, Landroid/graphics/RectF;->bottom:F

    .line 3692
    iget-object v0, p0, Lcom/android/calendar/e/g;->aA:Landroid/graphics/Paint;

    invoke-virtual {p2, v3, v0}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 3698
    :goto_1
    return-object v1

    .line 3643
    :cond_2
    iget v0, p0, Lcom/android/calendar/e/g;->cP:I

    if-ne v0, v3, :cond_3

    .line 3645
    iput-object p1, p0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    .line 3646
    sget v0, Lcom/android/calendar/e/g;->bl:I

    goto/16 :goto_0

    .line 3647
    :cond_3
    iget v0, p0, Lcom/android/calendar/e/g;->cP:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    .line 3648
    iget v0, p1, Lcom/android/calendar/dh;->c:I

    goto/16 :goto_0

    .line 3650
    :cond_4
    iget v0, p1, Lcom/android/calendar/dh;->c:I

    goto/16 :goto_0

    .line 3652
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3653
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3654
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dQ:Z

    if-eqz v0, :cond_6

    .line 3655
    iget v0, p1, Lcom/android/calendar/dh;->c:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->c(I)I

    move-result v0

    goto/16 :goto_0

    .line 3657
    :cond_6
    iget v0, p1, Lcom/android/calendar/dh;->c:I

    goto/16 :goto_0

    .line 3660
    :cond_7
    iget v0, p1, Lcom/android/calendar/dh;->c:I

    goto/16 :goto_0

    .line 3663
    :cond_8
    iget v0, p1, Lcom/android/calendar/dh;->c:I

    goto/16 :goto_0

    .line 3694
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/e/g;->cF:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 3695
    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 3696
    iget-object v0, p0, Lcom/android/calendar/e/g;->cF:Landroid/graphics/RectF;

    sget v2, Lcom/android/calendar/e/g;->bi:F

    sget v3, Lcom/android/calendar/e/g;->bi:F

    invoke-virtual {p2, v0, v2, v3, p3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/e/g;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/e/g;Landroid/text/format/Time;)Landroid/text/format/Time;
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    return-object p1
.end method

.method private a(ZIFF)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2071
    sub-float v0, p4, p3

    iput v0, p0, Lcom/android/calendar/e/g;->bw:F

    .line 2072
    sget-boolean v0, Lcom/android/calendar/e/g;->s:Z

    if-eqz v0, :cond_0

    .line 2073
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchViews("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") O:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Dist:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/e/g;->bw:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 2076
    :cond_0
    new-instance v2, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2077
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 2078
    if-eqz p1, :cond_2

    .line 2079
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v0, p2

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    .line 2083
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v2, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/al;->a(J)V

    .line 2087
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    .line 2088
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2089
    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->b(Landroid/text/format/Time;)V

    move-object v1, v0

    .line 2092
    :goto_1
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2093
    iget v0, v4, Landroid/text/format/Time;->monthDay:I

    iget v3, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v0, v3

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    .line 2095
    if-eqz p1, :cond_3

    .line 2096
    iget-object v0, p0, Lcom/android/calendar/e/g;->dg:Lcom/android/calendar/e/d;

    iget-object v0, v0, Lcom/android/calendar/e/d;->a:Landroid/view/animation/Animation;

    move-object v3, v0

    .line 2101
    :goto_2
    new-instance v0, Lcom/android/calendar/e/ac;

    invoke-direct {v0, p0, v2, v4}, Lcom/android/calendar/e/ac;-><init>(Lcom/android/calendar/e/g;Landroid/text/format/Time;Landroid/text/format/Time;)V

    .line 2102
    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2104
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/a;

    if-eqz v0, :cond_1

    .line 2105
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    check-cast v0, Lcom/android/calendar/a;

    .line 2106
    invoke-virtual {v0}, Lcom/android/calendar/a;->a()V

    .line 2107
    if-eqz p1, :cond_4

    .line 2108
    invoke-virtual {v0}, Lcom/android/calendar/a;->b()V

    .line 2109
    invoke-virtual {v0, v6}, Lcom/android/calendar/a;->a(I)V

    .line 2110
    invoke-virtual {v0, v3}, Lcom/android/calendar/a;->a(Landroid/view/animation/Animation;)V

    .line 2115
    :goto_3
    invoke-virtual {v0}, Lcom/android/calendar/a;->invalidateOptionsMenu()V

    .line 2117
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->dg:Lcom/android/calendar/e/d;

    invoke-virtual {v0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    .line 2118
    invoke-virtual {v0}, Lcom/android/calendar/e/g;->h()V

    .line 2119
    iget-object v0, p0, Lcom/android/calendar/e/g;->dg:Lcom/android/calendar/e/d;

    invoke-virtual {v0}, Lcom/android/calendar/e/d;->b()Lcom/android/calendar/e/a;

    move-result-object v0

    .line 2120
    invoke-virtual {v0}, Lcom/android/calendar/e/a;->c()V

    .line 2121
    invoke-virtual {v0}, Lcom/android/calendar/e/a;->e()V

    .line 2122
    invoke-virtual {v0, v1, v7, v6}, Lcom/android/calendar/e/a;->a(Landroid/text/format/Time;ZZ)V

    .line 2123
    iget-object v0, p0, Lcom/android/calendar/e/g;->dg:Lcom/android/calendar/e/d;

    invoke-virtual {v0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    .line 2124
    invoke-virtual {p0, v7}, Lcom/android/calendar/e/g;->a(Z)V

    .line 2125
    invoke-virtual {v0}, Lcom/android/calendar/e/g;->requestFocus()Z

    .line 2126
    invoke-virtual {v0}, Lcom/android/calendar/e/g;->b()V

    .line 2127
    return-object v0

    .line 2081
    :cond_2
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    sub-int/2addr v0, p2

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    goto :goto_0

    .line 2098
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->dg:Lcom/android/calendar/e/d;

    iget-object v0, v0, Lcom/android/calendar/e/d;->b:Landroid/view/animation/Animation;

    move-object v3, v0

    goto :goto_2

    .line 2112
    :cond_4
    invoke-virtual {v0, v6}, Lcom/android/calendar/a;->a(I)V

    .line 2113
    invoke-virtual {v0, v3}, Lcom/android/calendar/a;->b(Landroid/view/animation/Animation;)V

    goto :goto_3

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method

.method static a(IJI)Lcom/android/calendar/dh;
    .locals 7

    .prologue
    .line 3222
    invoke-static {}, Lcom/android/calendar/dh;->a()Lcom/android/calendar/dh;

    move-result-object v0

    .line 3223
    iput p0, v0, Lcom/android/calendar/dh;->i:I

    .line 3224
    iput p0, v0, Lcom/android/calendar/dh;->j:I

    .line 3225
    iput-wide p1, v0, Lcom/android/calendar/dh;->m:J

    .line 3226
    iget-wide v2, v0, Lcom/android/calendar/dh;->m:J

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/android/calendar/dh;->n:J

    .line 3227
    iput p3, v0, Lcom/android/calendar/dh;->k:I

    .line 3228
    iget v1, v0, Lcom/android/calendar/dh;->k:I

    add-int/lit8 v1, v1, 0x3c

    iput v1, v0, Lcom/android/calendar/dh;->l:I

    .line 3229
    return-object v0
.end method

.method private a(JJZ)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 2003
    const-string v0, ""

    .line 2004
    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    .line 2005
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2006
    if-eqz p5, :cond_1

    .line 2007
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2015
    :cond_0
    :goto_0
    return-object v0

    .line 2008
    :cond_1
    cmp-long v1, p1, v4

    if-eqz v1, :cond_0

    cmp-long v1, p3, v4

    if-eqz v1, :cond_0

    .line 2009
    invoke-virtual {v2, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 2010
    invoke-direct {p0}, Lcom/android/calendar/e/g;->getTimeFormat()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2011
    invoke-virtual {v2, p3, p4}, Landroid/text/format/Time;->set(J)V

    .line 2012
    invoke-direct {p0}, Lcom/android/calendar/e/g;->getTimeFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2013
    iget-object v2, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0f0036

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3772
    iget-object v0, p0, Lcom/android/calendar/e/g;->eb:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 3773
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3774
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 3777
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 3778
    if-gtz p2, :cond_2

    .line 3779
    const-string p1, ""

    .line 3784
    :cond_1
    :goto_0
    const/16 v0, 0xa

    const/16 v1, 0x20

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 3780
    :cond_2
    if-le v0, p2, :cond_1

    .line 3781
    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private a(FFFF)V
    .locals 2

    .prologue
    .line 3233
    iget-object v0, p0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    float-to-int v1, p1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 3234
    iget-object v0, p0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    float-to-int v1, p3

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 3235
    iget-object v0, p0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    float-to-int v1, p2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 3236
    iget-object v0, p0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    float-to-int v1, p4

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 3237
    return-void
.end method

.method private a(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1179
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    if-ge v0, v2, :cond_0

    .line 1180
    iget-object v2, p0, Lcom/android/calendar/e/g;->T:[I

    const/16 v3, 0x19

    aput v3, v2, v0

    .line 1179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1183
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v0, :cond_1

    .line 1184
    sget v0, Lcom/android/calendar/e/g;->bR:I

    sub-int v0, p2, v0

    div-int/lit8 v0, v0, 0x18

    sget v2, Lcom/android/calendar/e/g;->bF:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bF:I

    .line 1186
    :cond_1
    sget v0, Lcom/android/calendar/e/g;->bE:I

    sget v2, Lcom/android/calendar/e/g;->bF:I

    if-ge v0, v2, :cond_2

    .line 1187
    sget v0, Lcom/android/calendar/e/g;->bF:I

    sput v0, Lcom/android/calendar/e/g;->bE:I

    .line 1191
    :cond_2
    sget v0, Lcom/android/calendar/e/g;->bR:I

    iput v0, p0, Lcom/android/calendar/e/g;->bX:I

    .line 1193
    iput v1, p0, Lcom/android/calendar/e/g;->bQ:I

    .line 1195
    iget v0, p0, Lcom/android/calendar/e/g;->bX:I

    sub-int v0, p2, v0

    iput v0, p0, Lcom/android/calendar/e/g;->bD:I

    .line 1197
    iget v0, p0, Lcom/android/calendar/e/g;->bD:I

    sget v2, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v2, v2, 0x1

    div-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/e/g;->bV:I

    .line 1198
    iget-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    sget v2, Lcom/android/calendar/e/g;->bE:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/al;->c(F)V

    .line 1200
    sget v0, Lcom/android/calendar/e/g;->bF:I

    int-to-long v2, v0

    const-wide/32 v4, 0xea60

    mul-long/2addr v2, v4

    long-to-float v0, v2

    sget v2, Lcom/android/calendar/e/g;->bE:I

    int-to-float v2, v2

    const/high16 v3, 0x42700000    # 60.0f

    div-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-long v2, v0

    .line 1201
    iget-object v0, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-static {v0, v2, v3}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;J)V

    .line 1204
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    sget v2, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Lcom/android/calendar/e/g;->bC:I

    iget v3, p0, Lcom/android/calendar/e/g;->bW:I

    sub-int/2addr v2, v3

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/e/g;->bz:I

    .line 1205
    iget v0, p0, Lcom/android/calendar/e/g;->bz:I

    if-gez v0, :cond_3

    .line 1206
    iput v1, p0, Lcom/android/calendar/e/g;->bz:I

    .line 1208
    :cond_3
    sget v0, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x18

    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Lcom/android/calendar/e/g;->bD:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/e/g;->bA:I

    .line 1210
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iget v2, p0, Lcom/android/calendar/e/g;->bA:I

    if-lt v0, v2, :cond_4

    .line 1211
    iget v0, p0, Lcom/android/calendar/e/g;->bA:I

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    .line 1212
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 1213
    invoke-direct {p0}, Lcom/android/calendar/e/g;->z()V

    .line 1216
    :cond_4
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_5

    .line 1217
    invoke-direct {p0}, Lcom/android/calendar/e/g;->y()V

    .line 1218
    iput v1, p0, Lcom/android/calendar/e/g;->bZ:I

    .line 1225
    :cond_5
    iget v0, p0, Lcom/android/calendar/e/g;->bZ:I

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    if-lt v0, v1, :cond_6

    .line 1226
    sget v0, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/e/g;->bZ:I

    .line 1228
    :cond_6
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->bZ:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    .line 1230
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    sget v1, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    .line 1232
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v1, :cond_7

    iget-wide v2, p0, Lcom/android/calendar/e/g;->K:J

    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v4, v1, Lcom/android/calendar/dh;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 1233
    iget-object v1, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1235
    :cond_7
    iget-object v1, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    add-int/lit8 v0, v0, -0x14

    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 1236
    iget-object v0, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 1237
    sget v0, Lcom/android/calendar/e/g;->bE:I

    int-to-float v0, v0

    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->dK:I

    .line 1238
    div-int/lit8 v0, p2, 0x18

    .line 1239
    iget v1, p0, Lcom/android/calendar/e/g;->dK:I

    if-le v1, v0, :cond_8

    .line 1240
    iput v0, p0, Lcom/android/calendar/e/g;->dK:I

    .line 1243
    :cond_8
    return-void
.end method

.method private a(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 20

    .prologue
    .line 2626
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    .line 2627
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/calendar/e/g;->b(I)I

    move-result v2

    .line 2628
    add-int/lit8 v3, p2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/e/g;->b(I)I

    move-result v3

    sub-int/2addr v3, v2

    add-int/lit8 v6, v3, 0x1

    .line 2630
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->dO:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/e/g;->b(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 2631
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->i:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/calendar/e/g;->b(I)I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/g;->dO:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/calendar/e/g;->b(I)I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 2633
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/g;->dO:I

    move/from16 v0, p2

    if-ne v0, v5, :cond_2

    .line 2634
    int-to-float v2, v2

    int-to-float v5, v3

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v5, v7

    sub-float/2addr v2, v5

    float-to-int v2, v2

    .line 2635
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/g;->bx:I

    if-ge v2, v5, :cond_0

    .line 2636
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/g;->bx:I

    .line 2638
    :cond_0
    int-to-float v5, v6

    add-int/2addr v3, v4

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v5

    float-to-int v6, v3

    .line 2640
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->bC:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->bW:I

    sub-int/2addr v3, v4

    if-le v6, v3, :cond_11

    .line 2641
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->bC:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->bW:I

    sub-int v6, v3, v4

    move v4, v2

    .line 2649
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/g;->dO:I

    move/from16 v0, p2

    if-ne v2, v0, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/g;->dN:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    const/4 v8, 0x1

    .line 2651
    :goto_1
    sget v2, Lcom/android/calendar/e/g;->bE:I

    .line 2652
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cs:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_5

    .line 2723
    :cond_1
    :goto_2
    return-void

    .line 2643
    :cond_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/g;->dO:I

    move/from16 v0, p2

    if-ge v0, v5, :cond_3

    .line 2644
    int-to-float v2, v2

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move v4, v2

    goto :goto_0

    .line 2645
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->dO:I

    move/from16 v0, p2

    if-le v0, v3, :cond_11

    .line 2646
    int-to-float v2, v2

    int-to-float v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    move v4, v2

    goto :goto_0

    .line 2649
    :cond_4
    const/4 v8, 0x0

    goto :goto_1

    .line 2656
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/e/g;->av:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    .line 2657
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->aj:I

    add-int/lit8 v5, v2, 0x1

    mul-int/2addr v3, v5

    add-int v3, v3, p3

    move-object/from16 v0, v16

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 2658
    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    move-object/from16 v0, v16

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 2659
    move-object/from16 v0, v16

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 2660
    move-object/from16 v0, v16

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v6

    move-object/from16 v0, v16

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 2661
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    .line 2662
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    .line 2663
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/g;->by:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v5, v7

    sget v7, Lcom/android/calendar/e/g;->bR:I

    sub-int/2addr v5, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/e/g;->bQ:I

    sub-int v15, v5, v7

    .line 2664
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_6
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/calendar/dh;

    .line 2666
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/e/g;->dN:F

    move/from16 v3, p1

    move/from16 v5, p3

    invoke-virtual/range {v2 .. v9}, Lcom/android/calendar/e/al;->a(IIIILcom/android/calendar/dh;ZF)Z

    move-result v3

    if-nez v3, :cond_7

    iget-boolean v3, v7, Lcom/android/calendar/dh;->f:Z

    if-eqz v3, :cond_6

    .line 2671
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/e/g;->dz:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {v7, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2674
    :cond_8
    sget-boolean v3, Lcom/android/calendar/e/g;->p:Z

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/e/g;->dC:Z

    if-nez v3, :cond_6

    .line 2678
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->ai:I

    move/from16 v0, p1

    if-ne v0, v3, :cond_a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/e/g;->ak:Z

    if-nez v3, :cond_a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/e/g;->cl:Z

    if-eqz v3, :cond_a

    move-object/from16 v0, v16

    invoke-virtual {v2, v7, v0}, Lcom/android/calendar/e/al;->a(Lcom/android/calendar/dh;Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2680
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2683
    :cond_a
    sget v3, Lcom/android/calendar/e/g;->a:I

    if-ltz v3, :cond_1

    .line 2686
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->i:I

    const/4 v5, 0x1

    if-eq v3, v5, :cond_b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->i:I

    const/4 v5, 0x7

    if-eq v3, v5, :cond_b

    if-eqz v8, :cond_c

    .line 2687
    :cond_b
    invoke-virtual {v7}, Lcom/android/calendar/dh;->c()I

    move-result v3

    sget v5, Lcom/android/calendar/e/g;->a:I

    if-ne v3, v5, :cond_c

    .line 2688
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v7, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Canvas;)V

    .line 2691
    :cond_c
    invoke-virtual {v7}, Lcom/android/calendar/dh;->c()I

    move-result v3

    sget v5, Lcom/android/calendar/e/g;->a:I

    if-ge v3, v5, :cond_6

    .line 2694
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v3

    if-eqz v3, :cond_d

    iget-wide v10, v7, Lcom/android/calendar/dh;->q:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v0, v3, Lcom/android/calendar/dh;->q:J

    move-wide/from16 v18, v0

    cmp-long v3, v10, v18

    if-eqz v3, :cond_6

    .line 2697
    :cond_d
    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/calendar/e/g;->by:I

    move-object/from16 v9, p0

    move-object v10, v7

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    invoke-direct/range {v9 .. v15}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;II)Landroid/graphics/Rect;

    move-result-object v11

    .line 2698
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/calendar/e/g;->setupTextRect(Landroid/graphics/Rect;)V

    .line 2701
    iget v3, v11, Landroid/graphics/Rect;->top:I

    if-gt v3, v15, :cond_6

    iget v3, v11, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/g;->by:I

    if-lt v3, v5, :cond_6

    .line 2705
    iget v3, v7, Lcom/android/calendar/dh;->B:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_e

    .line 2706
    const/16 v3, 0x66

    invoke-virtual {v13, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2708
    :cond_e
    invoke-virtual {v7}, Lcom/android/calendar/dh;->h()Z

    move-result v3

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->g()I

    move-result v3

    const/4 v5, 0x2

    if-ne v3, v5, :cond_f

    .line 2709
    iget v3, v11, Landroid/graphics/Rect;->right:I

    sget v5, Lcom/android/calendar/e/g;->z:I

    sub-int/2addr v3, v5

    sget v5, Lcom/android/calendar/e/g;->A:I

    sub-int/2addr v3, v5

    sget v5, Lcom/android/calendar/e/g;->B:I

    sub-int/2addr v3, v5

    iput v3, v11, Landroid/graphics/Rect;->right:I

    .line 2710
    const/4 v14, 0x0

    move-object/from16 v9, p0

    move-object v10, v7

    move-object/from16 v12, p4

    invoke-direct/range {v9 .. v14}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V

    .line 2711
    iget v3, v11, Landroid/graphics/Rect;->right:I

    sget v5, Lcom/android/calendar/e/g;->z:I

    add-int/2addr v3, v5

    sget v5, Lcom/android/calendar/e/g;->A:I

    add-int/2addr v3, v5

    sget v5, Lcom/android/calendar/e/g;->B:I

    add-int/2addr v3, v5

    iput v3, v11, Landroid/graphics/Rect;->right:I

    .line 2715
    :goto_4
    sget-boolean v3, Lcom/android/calendar/dz;->d:Z

    if-eqz v3, :cond_6

    .line 2716
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v7, v1, v11}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    goto/16 :goto_3

    .line 2713
    :cond_f
    const/4 v14, 0x0

    move-object/from16 v9, p0

    move-object v10, v7

    move-object/from16 v12, p4

    invoke-direct/range {v9 .. v14}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V

    goto :goto_4

    .line 2720
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/g;->ai:I

    move/from16 v0, p1

    if-ne v0, v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/e/g;->ak:Z

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/g;->cP:I

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/e/g;->dy:Z

    if-nez v2, :cond_1

    .line 2721
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/e/g;->G()V

    goto/16 :goto_2

    :cond_11
    move v4, v2

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0xe

    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 738
    invoke-virtual {p0, v2}, Lcom/android/calendar/e/g;->setFocusable(Z)V

    .line 742
    invoke-virtual {p0, v2}, Lcom/android/calendar/e/g;->setFocusableInTouchMode(Z)V

    .line 743
    invoke-virtual {p0, v2}, Lcom/android/calendar/e/g;->setClickable(Z)V

    .line 745
    invoke-static {p1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->aE:I

    .line 746
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 747
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    .line 751
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 752
    iget-object v0, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 753
    iget-object v0, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->P:I

    .line 755
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b0031

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bt:I

    .line 756
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b0029

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bn:I

    .line 757
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b002f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bo:I

    .line 759
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b0030

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->br:I

    .line 761
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b002d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bp:I

    .line 763
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b0032

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bv:I

    .line 764
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b00c4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bl:I

    .line 765
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b002c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bm:I

    .line 766
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b00b6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bu:I

    .line 767
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b0072

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/e/g;->bq:I

    .line 769
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00ce

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->ds:I

    .line 770
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00d0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->dt:I

    .line 771
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00cf

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->du:I

    .line 772
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00cc

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->dv:I

    .line 773
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00cd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->dw:I

    .line 775
    iget-object v0, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/e/g;->aS:F

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 776
    iget-object v0, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 777
    iget-object v0, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 779
    iget-object v0, p0, Lcom/android/calendar/e/g;->az:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/e/g;->bj:I

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 780
    iget-object v0, p0, Lcom/android/calendar/e/g;->az:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 781
    iget-object v0, p0, Lcom/android/calendar/e/g;->az:Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/e/g;->bq:I

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 782
    iget-object v0, p0, Lcom/android/calendar/e/g;->az:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 784
    iget-object v0, p0, Lcom/android/calendar/e/g;->aA:Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/e/g;->bq:I

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 785
    iget-object v0, p0, Lcom/android/calendar/e/g;->aA:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 787
    iget-object v0, p0, Lcom/android/calendar/e/g;->aB:Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/e/g;->bp:I

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 788
    iget-object v0, p0, Lcom/android/calendar/e/g;->aB:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/DashPathEffect;

    new-array v4, v8, [F

    sget v5, Lcom/android/calendar/e/g;->u:F

    aput v5, v4, v1

    const/high16 v5, 0x40000000    # 2.0f

    sget v6, Lcom/android/calendar/e/g;->u:F

    mul-float/2addr v5, v6

    aput v5, v4, v2

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 789
    iget-object v0, p0, Lcom/android/calendar/e/g;->aB:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 790
    iget-object v0, p0, Lcom/android/calendar/e/g;->aB:Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/e/g;->u:F

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 792
    invoke-direct {p0}, Lcom/android/calendar/e/g;->L()V

    .line 794
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0b002e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 795
    iget-object v3, p0, Lcom/android/calendar/e/g;->ay:Landroid/graphics/Paint;

    .line 796
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 797
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 798
    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 800
    iget-object v3, p0, Lcom/android/calendar/e/g;->aw:Landroid/graphics/Paint;

    .line 801
    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 805
    new-array v0, v9, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cb:[Ljava/lang/String;

    .line 808
    new-array v0, v9, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cc:[Ljava/lang/String;

    move v0, v2

    .line 810
    :goto_1
    const/4 v4, 0x7

    if-gt v0, v4, :cond_2

    .line 811
    add-int/lit8 v4, v0, -0x1

    .line 813
    iget-object v5, p0, Lcom/android/calendar/e/g;->cb:[Ljava/lang/String;

    invoke-static {v0, v8}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    .line 814
    iget-object v5, p0, Lcom/android/calendar/e/g;->cb:[Ljava/lang/String;

    add-int/lit8 v6, v4, 0x7

    iget-object v7, p0, Lcom/android/calendar/e/g;->cb:[Ljava/lang/String;

    aget-object v7, v7, v4

    aput-object v7, v5, v6

    .line 816
    iget-object v5, p0, Lcom/android/calendar/e/g;->cc:[Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    .line 819
    iget-object v5, p0, Lcom/android/calendar/e/g;->cc:[Ljava/lang/String;

    aget-object v5, v5, v4

    iget-object v6, p0, Lcom/android/calendar/e/g;->cb:[Ljava/lang/String;

    aget-object v6, v6, v4

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 820
    iget-object v5, p0, Lcom/android/calendar/e/g;->cc:[Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    .line 823
    :cond_0
    iget-object v5, p0, Lcom/android/calendar/e/g;->cc:[Ljava/lang/String;

    add-int/lit8 v6, v4, 0x7

    iget-object v7, p0, Lcom/android/calendar/e/g;->cc:[Ljava/lang/String;

    aget-object v4, v7, v4

    aput-object v4, v5, v6

    .line 810
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 749
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    invoke-static {p1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    goto/16 :goto_0

    .line 826
    :cond_2
    invoke-static {v1}, Lcom/android/calendar/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->cB:Ljava/lang/String;

    .line 827
    invoke-static {v2}, Lcom/android/calendar/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->cC:Ljava/lang/String;

    .line 828
    sget v0, Lcom/android/calendar/e/g;->aU:F

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 829
    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 830
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->a()V

    .line 833
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 834
    const v3, 0x7f04001b

    invoke-virtual {v0, v3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    .line 835
    iget-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 838
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    .line 839
    iget-object v0, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 840
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 841
    const v3, 0x103000b

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 842
    new-array v3, v2, [I

    const v4, 0x1010054

    aput v4, v3, v1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 845
    iget-object v3, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 846
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 849
    iget-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 851
    invoke-virtual {p0, p0}, Lcom/android/calendar/e/g;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 853
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    invoke-static {p1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    .line 854
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 855
    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 857
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/calendar/e/g;->T:[I

    .line 863
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x19

    .line 865
    mul-int/lit8 v3, v0, 0x4

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/android/calendar/e/g;->aC:[F

    .line 866
    new-array v0, v0, [Landroid/graphics/Path;

    iput-object v0, p0, Lcom/android/calendar/e/g;->aD:[Landroid/graphics/Path;

    move v0, v1

    .line 867
    :goto_2
    iget-object v3, p0, Lcom/android/calendar/e/g;->aD:[Landroid/graphics/Path;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 868
    iget-object v3, p0, Lcom/android/calendar/e/g;->aD:[Landroid/graphics/Path;

    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    aput-object v4, v3, v0

    .line 867
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 870
    :cond_3
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gx;->a(Landroid/app/FragmentManager;)Landroid/util/LruCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->dP:Landroid/util/LruCache;

    .line 871
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0f043b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->aq:Ljava/lang/String;

    .line 872
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    if-ne v0, v2, :cond_4

    .line 873
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v1, 0x7f0c00bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->bW:I

    .line 874
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v1, 0x7f0c00bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->aa:I

    .line 875
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v1, 0x7f0c00c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->ab:I

    .line 881
    :goto_3
    return-void

    .line 877
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v2, 0x7f0c0398

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->bW:I

    .line 878
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v2, 0x7f0c039c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->aa:I

    .line 879
    iput v1, p0, Lcom/android/calendar/e/g;->ab:I

    goto :goto_3
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2755
    iget-object v0, p0, Lcom/android/calendar/e/g;->aw:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/g;->c(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 2756
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5

    .prologue
    const/16 v4, 0xc

    .line 2783
    sget v0, Lcom/android/calendar/e/g;->bn:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2784
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2785
    sget v0, Lcom/android/calendar/e/g;->aV:F

    const/high16 v1, 0x40a00000    # 5.0f

    sub-float/2addr v0, v1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2789
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2790
    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 2791
    iget-object v0, p0, Lcom/android/calendar/e/g;->cB:Ljava/lang/String;

    .line 2792
    invoke-direct {p0}, Lcom/android/calendar/e/g;->z()V

    .line 2793
    iget v1, p0, Lcom/android/calendar/e/g;->bY:I

    if-lt v1, v4, :cond_0

    .line 2794
    iget-object v0, p0, Lcom/android/calendar/e/g;->cC:Ljava/lang/String;

    .line 2797
    :cond_0
    iget v1, p0, Lcom/android/calendar/e/g;->bX:I

    iget v2, p0, Lcom/android/calendar/e/g;->bZ:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/e/g;->bP:I

    mul-int/lit8 v2, v2, 0x4

    div-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 2798
    iget v2, p0, Lcom/android/calendar/e/g;->bY:I

    if-nez v2, :cond_1

    .line 2799
    sget v2, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    .line 2802
    :cond_1
    sget v2, Lcom/android/calendar/e/g;->aQ:I

    .line 2804
    invoke-static {}, Lcom/android/calendar/hj;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2805
    sget v2, Lcom/android/calendar/e/g;->aQ:I

    add-int/lit8 v2, v2, 0x4

    .line 2807
    :cond_2
    int-to-float v3, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v3, v1, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2808
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    if-ge v0, v4, :cond_3

    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    iget v1, p0, Lcom/android/calendar/e/g;->bV:I

    add-int/2addr v0, v1

    if-le v0, v4, :cond_3

    .line 2810
    iget-object v0, p0, Lcom/android/calendar/e/g;->cC:Ljava/lang/String;

    .line 2811
    iget v1, p0, Lcom/android/calendar/e/g;->bX:I

    iget v3, p0, Lcom/android/calendar/e/g;->bZ:I

    add-int/2addr v1, v3

    iget v3, p0, Lcom/android/calendar/e/g;->bY:I

    rsub-int/lit8 v3, v3, 0xc

    sget v4, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v3, v4

    add-int/2addr v1, v3

    iget v3, p0, Lcom/android/calendar/e/g;->bP:I

    mul-int/lit8 v3, v3, 0x4

    div-int/lit8 v3, v3, 0x3

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, 0x1

    .line 2813
    int-to-float v2, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v2, v1, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2815
    :cond_3
    return-void

    .line 2787
    :cond_4
    sget v0, Lcom/android/calendar/e/g;->aV:F

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Rect;IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2842
    iget v0, p0, Lcom/android/calendar/e/g;->dO:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v0

    .line 2843
    iget v3, p0, Lcom/android/calendar/e/g;->i:I

    invoke-direct {p0, v3}, Lcom/android/calendar/e/g;->b(I)I

    move-result v3

    iget v4, p0, Lcom/android/calendar/e/g;->dO:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {p0, v4}, Lcom/android/calendar/e/g;->b(I)I

    move-result v4

    sub-int/2addr v3, v4

    .line 2844
    invoke-direct {p0, p2}, Lcom/android/calendar/e/g;->b(I)I

    move-result v4

    iget-object v5, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v6, 0x7f0c0340

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p1, Landroid/graphics/Rect;->left:I

    .line 2845
    add-int/lit8 v4, p2, 0x1

    invoke-direct {p0, v4}, Lcom/android/calendar/e/g;->b(I)I

    move-result v4

    iput v4, p1, Landroid/graphics/Rect;->right:I

    .line 2846
    iget v4, p0, Lcom/android/calendar/e/g;->dO:I

    if-ne p2, v4, :cond_5

    .line 2847
    iget v4, p1, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    int-to-float v0, v0

    iget v5, p0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v0, v5

    sub-float v0, v4, v0

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 2848
    iget v0, p1, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    int-to-float v3, v3

    iget v4, p0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 2856
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-nez v0, :cond_1

    .line 2857
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_1

    .line 2858
    iget v0, p1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 2862
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-nez v0, :cond_8

    .line 2863
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v3, 0x7f0c00c3

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2865
    :goto_1
    iget v3, p0, Lcom/android/calendar/e/g;->i:I

    if-ne v3, v2, :cond_2

    .line 2866
    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v5, 0x7f0c00c6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->left:I

    .line 2867
    iget v3, p1, Landroid/graphics/Rect;->right:I

    sget v4, Lcom/android/calendar/e/g;->ba:I

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->right:I

    .line 2869
    :cond_2
    div-int/lit8 v3, v0, 0x2

    sub-int v3, p3, v3

    iput v3, p1, Landroid/graphics/Rect;->top:I

    .line 2870
    iget v3, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v3

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 2871
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v3, :cond_3

    .line 2872
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 2874
    :cond_3
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 2875
    iget-object v0, p0, Lcom/android/calendar/e/g;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2876
    iget-object v0, p0, Lcom/android/calendar/e/g;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2878
    iget-object v0, p0, Lcom/android/calendar/e/g;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 2879
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(I)I

    move-result v1

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 2880
    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    if-ne v1, v2, :cond_4

    .line 2881
    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 2883
    :cond_4
    iget v1, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 2885
    iget-object v0, p0, Lcom/android/calendar/e/g;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 2886
    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 2887
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 2889
    iget-object v0, p0, Lcom/android/calendar/e/g;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2890
    iget-object v0, p0, Lcom/android/calendar/e/g;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2891
    return-void

    .line 2849
    :cond_5
    iget v4, p0, Lcom/android/calendar/e/g;->dO:I

    if-ge p2, v4, :cond_6

    .line 2850
    iget v3, p1, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    int-to-float v4, v0

    iget v5, p0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p1, Landroid/graphics/Rect;->left:I

    .line 2851
    iget v3, p1, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    int-to-float v0, v0

    iget v4, p0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v0, v4

    sub-float v0, v3, v0

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0

    .line 2852
    :cond_6
    iget v0, p0, Lcom/android/calendar/e/g;->dO:I

    if-le p2, v0, :cond_0

    .line 2853
    iget v0, p1, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    int-to-float v4, v3

    iget v5, p0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v4, v5

    add-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 2854
    iget v0, p1, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    int-to-float v3, v3

    iget v4, p0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 2874
    goto :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 12

    .prologue
    .line 2520
    invoke-virtual {p3}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v6

    .line 2522
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v0

    int-to-float v5, v0

    .line 2524
    sget v0, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v7, v0

    .line 2528
    sget v0, Lcom/android/calendar/e/g;->bo:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2529
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2530
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2531
    const/4 v3, 0x0

    .line 2532
    const/4 v2, 0x0

    .line 2535
    const/4 v1, 0x0

    .line 2537
    const/16 v8, 0x18

    .line 2539
    const/4 v0, 0x0

    move v4, v3

    move v11, v2

    move v2, v1

    move v1, v11

    :goto_0
    if-ge v0, v8, :cond_0

    .line 2540
    iget-object v3, p0, Lcom/android/calendar/e/g;->aC:[F

    add-int/lit8 v9, v1, 0x1

    const/4 v10, 0x0

    aput v10, v3, v1

    .line 2541
    iget-object v1, p0, Lcom/android/calendar/e/g;->aC:[F

    add-int/lit8 v3, v9, 0x1

    aput v4, v1, v9

    .line 2542
    iget-object v1, p0, Lcom/android/calendar/e/g;->aC:[F

    add-int/lit8 v9, v3, 0x1

    aput v5, v1, v3

    .line 2543
    iget-object v1, p0, Lcom/android/calendar/e/g;->aC:[F

    add-int/lit8 v3, v9, 0x1

    aput v4, v1, v9

    .line 2544
    iget-object v1, p0, Lcom/android/calendar/e/g;->aD:[Landroid/graphics/Path;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 2545
    iget-object v1, p0, Lcom/android/calendar/e/g;->aD:[Landroid/graphics/Path;

    aget-object v1, v1, v2

    const/4 v9, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    div-float v10, v7, v10

    add-float/2addr v10, v4

    invoke-virtual {v1, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2546
    iget-object v1, p0, Lcom/android/calendar/e/g;->aD:[Landroid/graphics/Path;

    aget-object v1, v1, v2

    const/high16 v9, 0x40000000    # 2.0f

    div-float v9, v7, v9

    add-float/2addr v9, v4

    invoke-virtual {v1, v5, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2547
    add-int/lit8 v1, v2, 0x1

    .line 2548
    add-float v2, v4, v7

    .line 2539
    add-int/lit8 v0, v0, 0x1

    move v4, v2

    move v2, v1

    move v1, v3

    goto :goto_0

    .line 2550
    :cond_0
    sget v0, Lcom/android/calendar/e/g;->br:I

    sget v3, Lcom/android/calendar/e/g;->bo:I

    if-eq v0, v3, :cond_1

    .line 2551
    iget-object v0, p0, Lcom/android/calendar/e/g;->aC:[F

    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3, v1, p3}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 2552
    sget v0, Lcom/android/calendar/e/g;->br:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2558
    :cond_1
    sget v0, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x18

    add-int/lit8 v0, v0, 0x1

    int-to-float v3, v0

    .line 2563
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->ce:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_4

    .line 2564
    const/4 v0, 0x0

    .line 2568
    :goto_1
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2569
    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(I)I

    move-result v1

    iget v5, p0, Lcom/android/calendar/e/g;->dO:I

    add-int/lit8 v5, v5, 0x1

    invoke-direct {p0, v5}, Lcom/android/calendar/e/g;->b(I)I

    move-result v5

    sub-int/2addr v1, v5

    int-to-float v1, v1

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v1, v5

    float-to-int v5, v1

    .line 2570
    iget v1, p0, Lcom/android/calendar/e/g;->dO:I

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v8, 0x3f800000    # 1.0f

    add-float/2addr v1, v8

    float-to-int v8, v1

    .line 2571
    :goto_2
    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    if-ge v0, v1, :cond_6

    .line 2572
    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v1

    int-to-float v1, v1

    .line 2573
    if-nez v0, :cond_2

    .line 2574
    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v1, v9

    .line 2576
    :cond_2
    iget v9, p0, Lcom/android/calendar/e/g;->dO:I

    if-gt v0, v9, :cond_5

    .line 2577
    int-to-float v9, v8

    iget v10, p0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v9, v10

    sub-float/2addr v1, v9

    float-to-int v1, v1

    iput v1, v4, Landroid/graphics/Rect;->left:I

    .line 2578
    iget v1, v4, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v1, v9

    float-to-int v1, v1

    iput v1, v4, Landroid/graphics/Rect;->right:I

    .line 2583
    :cond_3
    :goto_3
    const/4 v1, 0x0

    iput v1, v4, Landroid/graphics/Rect;->top:I

    .line 2584
    float-to-int v1, v3

    iput v1, v4, Landroid/graphics/Rect;->bottom:I

    .line 2585
    invoke-virtual {p2, v4, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2571
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2566
    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    .line 2579
    :cond_5
    iget v9, p0, Lcom/android/calendar/e/g;->dO:I

    if-le v0, v9, :cond_3

    .line 2580
    int-to-float v9, v5

    iget v10, p0, Lcom/android/calendar/e/g;->dN:F

    mul-float/2addr v9, v10

    add-float/2addr v1, v9

    float-to-int v1, v1

    iput v1, v4, Landroid/graphics/Rect;->left:I

    .line 2581
    iget v1, v4, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v1, v9

    float-to-int v1, v1

    iput v1, v4, Landroid/graphics/Rect;->right:I

    goto :goto_3

    .line 2587
    :cond_6
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_7

    .line 2588
    iget-object v1, p0, Lcom/android/calendar/e/g;->aD:[Landroid/graphics/Path;

    aget-object v1, v1, v0

    iget-object v3, p0, Lcom/android/calendar/e/g;->aB:Landroid/graphics/Paint;

    invoke-virtual {p2, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2587
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2593
    :cond_7
    iget v0, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v1, v0

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v3, v0

    const/high16 v0, 0x41c00000    # 24.0f

    mul-float v4, v7, v0

    move-object v0, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2597
    invoke-virtual {p3, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2598
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2599
    return-void
.end method

.method private a(Landroid/text/format/Time;Landroid/text/format/Time;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 4235
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getContext()Landroid/content/Context;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/app/Activity;

    .line 4236
    new-instance v0, Lcom/android/calendar/event/iv;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-virtual {p1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {p2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/event/iv;-><init>(Landroid/content/Context;JJZ)V

    .line 4237
    invoke-virtual {v7}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 4238
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 4239
    sget-object v3, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 4240
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 4242
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4248
    :cond_0
    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 4249
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 4253
    :goto_1
    return-void

    .line 4243
    :catch_0
    move-exception v1

    goto :goto_0

    .line 4250
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private static a(Lcom/android/calendar/as;Landroid/text/format/Time;Landroid/text/format/Time;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5498
    new-instance v4, Lcom/android/a/c;

    invoke-direct {v4}, Lcom/android/a/c;-><init>()V

    .line 5499
    iget-object v0, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 5501
    iget-object v0, v4, Lcom/android/a/c;->m:[I

    if-eqz v0, :cond_7

    iget v0, v4, Lcom/android/a/c;->o:I

    if-lez v0, :cond_7

    move v0, v1

    .line 5502
    :goto_0
    iget v3, v4, Lcom/android/a/c;->o:I

    if-ge v0, v3, :cond_6

    .line 5503
    iget-object v3, v4, Lcom/android/a/c;->m:[I

    aget v3, v3, v0

    iget v5, p2, Landroid/text/format/Time;->weekDay:I

    invoke-static {v5}, Lcom/android/a/c;->b(I)I

    move-result v5

    if-ne v3, v5, :cond_5

    .line 5504
    iget-object v3, v4, Lcom/android/a/c;->m:[I

    iget v5, p1, Landroid/text/format/Time;->weekDay:I

    invoke-static {v5}, Lcom/android/a/c;->b(I)I

    move-result v5

    aput v5, v3, v0

    .line 5505
    iget-object v0, v4, Lcom/android/a/c;->m:[I

    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    move v0, v2

    .line 5510
    :goto_1
    iget v3, v4, Lcom/android/a/c;->b:I

    const/4 v5, 0x6

    if-ne v3, v5, :cond_1

    .line 5512
    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v3, v3, -0x1

    div-int/lit8 v3, v3, 0x7

    add-int/lit8 v3, v3, 0x1

    .line 5513
    const/4 v5, 0x5

    if-ne v3, v5, :cond_0

    .line 5514
    const/4 v3, -0x1

    .line 5516
    :cond_0
    iget-object v5, v4, Lcom/android/a/c;->n:[I

    if-eqz v5, :cond_1

    iget-object v5, v4, Lcom/android/a/c;->n:[I

    array-length v5, v5

    if-lez v5, :cond_1

    .line 5517
    iget-object v0, v4, Lcom/android/a/c;->n:[I

    aput v3, v0, v1

    move v0, v2

    .line 5522
    :cond_1
    :goto_2
    iget-object v3, v4, Lcom/android/a/c;->p:[I

    if-eqz v3, :cond_2

    iget v3, v4, Lcom/android/a/c;->q:I

    if-lez v3, :cond_2

    .line 5523
    iget-object v0, v4, Lcom/android/a/c;->p:[I

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    aput v3, v0, v1

    move v0, v2

    .line 5526
    :cond_2
    iget-object v3, v4, Lcom/android/a/c;->r:[I

    if-eqz v3, :cond_3

    iget v3, v4, Lcom/android/a/c;->s:I

    if-lez v3, :cond_3

    .line 5527
    iget-object v0, v4, Lcom/android/a/c;->r:[I

    iget v3, p1, Landroid/text/format/Time;->yearDay:I

    aput v3, v0, v1

    move v0, v2

    .line 5530
    :cond_3
    if-eqz v0, :cond_4

    .line 5531
    invoke-virtual {v4}, Lcom/android/a/c;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 5533
    :cond_4
    return-void

    .line 5502
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method private a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 5237
    if-eqz p1, :cond_1

    .line 5238
    if-gez p4, :cond_0

    .line 5239
    iget p4, p0, Lcom/android/calendar/e/g;->ef:I

    .line 5242
    :cond_0
    sget v0, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x6

    .line 5243
    iget v1, p0, Lcom/android/calendar/e/g;->bX:I

    iget v2, p0, Lcom/android/calendar/e/g;->bQ:I

    add-int/2addr v1, v2

    .line 5244
    iget v2, p1, Lcom/android/calendar/dh;->F:F

    float-to-int v2, v2

    .line 5246
    sget v3, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v3, v3, 0x18

    add-int/lit8 v3, v3, 0x1

    iget v4, p0, Lcom/android/calendar/e/g;->bQ:I

    add-int/2addr v3, v4

    .line 5248
    iget v4, p0, Lcom/android/calendar/e/g;->by:I

    add-int/2addr v4, p4

    iget v5, p1, Lcom/android/calendar/dh;->E:F

    float-to-int v5, v5

    add-int/2addr v0, v5

    iget v5, p0, Lcom/android/calendar/e/g;->bQ:I

    add-int/2addr v0, v5

    if-ge v4, v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-ne v0, v4, :cond_4

    .line 5250
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;IZ)V

    .line 5259
    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    .line 5260
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 5262
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/e/g;->Q()V

    .line 5263
    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/android/calendar/dh;->t:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 5264
    invoke-direct {p0}, Lcom/android/calendar/e/g;->O()V

    .line 5266
    :cond_3
    return-void

    .line 5251
    :cond_4
    add-int v0, v2, v1

    if-le v0, v3, :cond_5

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_5

    .line 5252
    int-to-float v0, v3

    iget v1, p1, Lcom/android/calendar/dh;->F:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->bQ:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 5253
    iget v1, p1, Lcom/android/calendar/dh;->E:F

    int-to-float v0, v0

    add-float/2addr v0, v1

    iput v0, p1, Lcom/android/calendar/dh;->E:F

    .line 5254
    invoke-direct {p0, p1, p2, p3, v6}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;IZ)V

    goto :goto_0

    .line 5256
    :cond_5
    invoke-direct {p0, p1, p2, p3, v6}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;IZ)V

    goto :goto_0
.end method

.method private a(Lcom/android/calendar/dh;Landroid/database/Cursor;IZ)V
    .locals 20

    .prologue
    .line 5283
    new-instance v6, Lcom/android/calendar/as;

    invoke-direct {v6}, Lcom/android/calendar/as;-><init>()V

    .line 5284
    new-instance v7, Lcom/android/calendar/as;

    invoke-direct {v7}, Lcom/android/calendar/as;-><init>()V

    .line 5285
    move-object/from16 v0, p2

    invoke-static {v7, v0}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Landroid/database/Cursor;)V

    .line 5286
    move-object/from16 v0, p2

    invoke-static {v6, v0}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Landroid/database/Cursor;)V

    .line 5288
    invoke-direct/range {p0 .. p1}, Lcom/android/calendar/e/g;->b(Lcom/android/calendar/dh;)Lcom/android/calendar/dh;

    move-result-object v8

    .line 5289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    invoke-virtual {v2, v8}, Lcom/android/calendar/e/al;->a(Lcom/android/calendar/dh;)I

    move-result v4

    .line 5290
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/calendar/e/g;->dB:J

    .line 5291
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/e/g;->dB:J

    iget-wide v12, v8, Lcom/android/calendar/dh;->n:J

    iget-wide v14, v8, Lcom/android/calendar/dh;->m:J

    sub-long/2addr v12, v14

    add-long/2addr v12, v2

    .line 5292
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/e/g;->H:Z

    if-eqz v2, :cond_8

    const-string v2, "UTC"

    .line 5293
    :goto_0
    new-instance v9, Landroid/text/format/Time;

    iget-object v3, v8, Lcom/android/calendar/dh;->s:Ljava/lang/String;

    invoke-direct {v9, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 5294
    iget-wide v14, v8, Lcom/android/calendar/dh;->m:J

    invoke-virtual {v9, v14, v15}, Landroid/text/format/Time;->set(J)V

    .line 5295
    iput-object v2, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 5296
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 5297
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/e/g;->H:Z

    if-eqz v3, :cond_9

    const/4 v3, 0x0

    :goto_1
    iput v3, v9, Landroid/text/format/Time;->hour:I

    .line 5298
    rem-int/lit8 v3, v4, 0x3c

    .line 5299
    int-to-float v3, v3

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    mul-int/lit8 v3, v3, 0xa

    .line 5300
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/e/g;->H:Z

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    :cond_0
    iput v3, v9, Landroid/text/format/Time;->minute:I

    .line 5302
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-eq v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    if-eq v3, v4, :cond_1

    iget-boolean v3, v8, Lcom/android/calendar/dh;->f:Z

    if-eqz v3, :cond_2

    .line 5303
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/dh;)I

    move-result v3

    .line 5304
    iget v4, v9, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v3, v4

    iput v3, v9, Landroid/text/format/Time;->monthDay:I

    .line 5307
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v14

    .line 5311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-ne v3, v4, :cond_a

    .line 5312
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    invoke-virtual {v3, v8}, Lcom/android/calendar/e/al;->b(Lcom/android/calendar/dh;)I

    move-result v3

    .line 5313
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 5314
    iget-wide v0, v8, Lcom/android/calendar/dh;->n:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 5315
    div-int/lit8 v5, v3, 0x3c

    iput v5, v4, Landroid/text/format/Time;->hour:I

    .line 5316
    rem-int/lit8 v3, v3, 0x3c

    .line 5317
    int-to-float v3, v3

    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    mul-int/lit8 v3, v3, 0xa

    .line 5318
    iput v3, v4, Landroid/text/format/Time;->minute:I

    .line 5319
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v3, :cond_3

    .line 5320
    iget v3, v9, Landroid/text/format/Time;->monthDay:I

    iput v3, v4, Landroid/text/format/Time;->monthDay:I

    .line 5322
    :cond_3
    if-eqz p4, :cond_4

    .line 5323
    iget v3, v9, Landroid/text/format/Time;->monthDay:I

    iput v3, v4, Landroid/text/format/Time;->monthDay:I

    .line 5324
    iget v3, v9, Landroid/text/format/Time;->hour:I

    iget v5, v9, Landroid/text/format/Time;->minute:I

    add-int/lit8 v5, v5, 0x1e

    div-int/lit8 v5, v5, 0x3c

    add-int/2addr v3, v5

    iput v3, v4, Landroid/text/format/Time;->hour:I

    .line 5325
    iget v3, v9, Landroid/text/format/Time;->minute:I

    add-int/lit8 v3, v3, 0x1e

    rem-int/lit8 v3, v3, 0x3c

    iput v3, v4, Landroid/text/format/Time;->minute:I

    .line 5326
    const/4 v3, 0x0

    iput v3, v4, Landroid/text/format/Time;->second:I

    .line 5330
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v16

    .line 5331
    iget-wide v0, v9, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-static {v14, v15, v0, v1}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 5332
    iget-wide v0, v4, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v19}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v5

    .line 5333
    if-le v3, v5, :cond_5

    .line 5334
    iget v3, v9, Landroid/text/format/Time;->monthDay:I

    iput v3, v4, Landroid/text/format/Time;->monthDay:I

    .line 5335
    const/16 v3, 0x18

    iput v3, v4, Landroid/text/format/Time;->hour:I

    .line 5336
    const/4 v3, 0x0

    iput v3, v4, Landroid/text/format/Time;->minute:I

    .line 5337
    const/4 v3, 0x0

    iput v3, v4, Landroid/text/format/Time;->second:I

    .line 5340
    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 5373
    :cond_6
    :goto_2
    cmp-long v3, v10, v14

    if-nez v3, :cond_e

    cmp-long v3, v12, v4

    if-nez v3, :cond_e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/e/g;->I:Z

    if-nez v3, :cond_e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/e/g;->H:Z

    if-nez v3, :cond_e

    .line 5446
    :cond_7
    :goto_3
    return-void

    .line 5292
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 5297
    :cond_9
    div-int/lit8 v3, v4, 0x3c

    goto/16 :goto_1

    .line 5342
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v3, v4, :cond_c

    .line 5343
    iget-wide v4, v8, Lcom/android/calendar/dh;->n:J

    .line 5347
    :goto_4
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 5348
    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 5349
    iget v4, v9, Landroid/text/format/Time;->year:I

    iput v4, v3, Landroid/text/format/Time;->year:I

    .line 5350
    iget v4, v9, Landroid/text/format/Time;->month:I

    iput v4, v3, Landroid/text/format/Time;->month:I

    .line 5352
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/e/g;->H:Z

    if-eqz v4, :cond_d

    .line 5353
    iget v4, v9, Landroid/text/format/Time;->monthDay:I

    iget v5, v8, Lcom/android/calendar/dh;->j:I

    add-int/2addr v4, v5

    iget v5, v8, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Landroid/text/format/Time;->monthDay:I

    .line 5354
    const/4 v4, 0x0

    iput v4, v3, Landroid/text/format/Time;->hour:I

    .line 5358
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/e/g;->H:Z

    if-nez v4, :cond_b

    iget v4, v3, Landroid/text/format/Time;->hour:I

    if-nez v4, :cond_b

    iget v4, v3, Landroid/text/format/Time;->minute:I

    if-nez v4, :cond_b

    iget v4, v3, Landroid/text/format/Time;->second:I

    if-nez v4, :cond_b

    .line 5359
    const/16 v4, 0x18

    iput v4, v3, Landroid/text/format/Time;->hour:I

    .line 5361
    :cond_b
    invoke-virtual {v3, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 5362
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 5364
    cmp-long v16, v14, v4

    if-lez v16, :cond_6

    .line 5365
    iget v4, v9, Landroid/text/format/Time;->monthDay:I

    iput v4, v3, Landroid/text/format/Time;->monthDay:I

    .line 5366
    const/16 v4, 0x18

    iput v4, v3, Landroid/text/format/Time;->hour:I

    .line 5367
    const/4 v4, 0x0

    iput v4, v3, Landroid/text/format/Time;->minute:I

    .line 5368
    const/4 v4, 0x0

    iput v4, v3, Landroid/text/format/Time;->second:I

    .line 5369
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    goto/16 :goto_2

    .line 5345
    :cond_c
    iget-wide v4, v8, Lcom/android/calendar/dh;->n:J

    sub-long v16, v14, v10

    add-long v4, v4, v16

    goto :goto_4

    .line 5356
    :cond_d
    iget v4, v9, Landroid/text/format/Time;->monthDay:I

    iput v4, v3, Landroid/text/format/Time;->monthDay:I

    goto :goto_5

    .line 5376
    :cond_e
    sub-long v12, v4, v14

    const-wide/32 v16, 0x1b7740

    cmp-long v3, v12, v16

    if-gez v3, :cond_f

    .line 5377
    const-wide/32 v4, 0x1b7740

    add-long/2addr v4, v14

    .line 5379
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->V:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v6, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 5380
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->V:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 5381
    iget-object v3, v6, Lcom/android/calendar/as;->j:Ljava/lang/String;

    iput-object v3, v6, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    .line 5382
    iget-wide v12, v7, Lcom/android/calendar/as;->y:J

    cmp-long v3, v10, v12

    if-nez v3, :cond_11

    const/4 v3, 0x1

    :goto_6
    iput-boolean v3, v6, Lcom/android/calendar/as;->w:Z

    .line 5383
    iput-wide v14, v6, Lcom/android/calendar/as;->y:J

    .line 5384
    iput-wide v4, v6, Lcom/android/calendar/as;->B:J

    .line 5385
    iget-boolean v3, v8, Lcom/android/calendar/dh;->o:Z

    iput-boolean v3, v6, Lcom/android/calendar/as;->H:Z

    .line 5386
    iget-wide v8, v8, Lcom/android/calendar/dh;->b:J

    iput-wide v8, v6, Lcom/android/calendar/as;->b:J

    .line 5387
    iput-object v2, v6, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 5389
    new-instance v2, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 5390
    new-instance v3, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v3, v8}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 5391
    new-instance v8, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v8, v9}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 5392
    iget-wide v12, v6, Lcom/android/calendar/as;->y:J

    invoke-virtual {v2, v12, v13}, Landroid/text/format/Time;->set(J)V

    .line 5393
    invoke-virtual {v3, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 5394
    iget-wide v10, v6, Lcom/android/calendar/as;->B:J

    invoke-virtual {v8, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 5395
    invoke-static {v2}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v9

    if-nez v9, :cond_10

    invoke-static {v8}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v8

    if-eqz v8, :cond_12

    .line 5397
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->i()V

    .line 5398
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/e/g;->invalidate()V

    goto/16 :goto_3

    .line 5382
    :cond_11
    const/4 v3, 0x0

    goto :goto_6

    .line 5401
    :cond_12
    const/4 v8, 0x1

    move/from16 v0, p3

    if-ne v0, v8, :cond_13

    .line 5402
    const/4 v8, 0x0

    iput-object v8, v6, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 5403
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/e/g;->dB:J

    iput-wide v8, v6, Lcom/android/calendar/as;->x:J

    .line 5405
    :cond_13
    iget-object v8, v6, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_14

    .line 5406
    invoke-static {v6, v2, v3}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/as;Landroid/text/format/Time;Landroid/text/format/Time;)V

    .line 5407
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/e/g;->dB:J

    .line 5408
    const-string v8, "duration"

    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 5409
    iput-wide v2, v6, Lcom/android/calendar/as;->x:J

    .line 5410
    add-long/2addr v2, v8

    iput-wide v2, v6, Lcom/android/calendar/as;->A:J

    .line 5413
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v3, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-eq v2, v3, :cond_15

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/e/g;->I:Z

    if-eqz v2, :cond_16

    .line 5414
    :cond_15
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/e/g;->I:Z

    .line 5415
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/e/g;->H:Z

    iput-boolean v2, v6, Lcom/android/calendar/as;->G:Z

    .line 5418
    :cond_16
    iget-boolean v2, v6, Lcom/android/calendar/as;->G:Z

    if-nez v2, :cond_17

    .line 5419
    iget-wide v2, v6, Lcom/android/calendar/as;->B:J

    iget-wide v8, v6, Lcom/android/calendar/as;->y:J

    cmp-long v2, v2, v8

    if-ltz v2, :cond_17

    .line 5420
    iget-wide v2, v6, Lcom/android/calendar/as;->B:J

    iget-wide v8, v6, Lcom/android/calendar/as;->y:J

    sub-long/2addr v2, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v2, v8

    .line 5421
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "P"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "S"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Lcom/android/calendar/as;->D:Ljava/lang/String;

    .line 5425
    :cond_17
    new-instance v3, Lcom/android/calendar/event/av;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-direct {v3, v2, v7}, Lcom/android/calendar/event/av;-><init>(Landroid/content/Context;Lcom/android/calendar/as;)V

    .line 5426
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    move/from16 v0, p3

    invoke-virtual {v3, v6, v7, v0, v2}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;ILandroid/app/Activity;)Z

    .line 5430
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/e/g;->H:Z

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v2, :cond_1a

    .line 5431
    const/4 v2, 0x0

    move v3, v2

    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_1a

    .line 5432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {v2, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    iget-wide v6, v2, Lcom/android/calendar/dh;->b:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v8, v2, Lcom/android/calendar/dh;->b:J

    cmp-long v2, v6, v8

    if-nez v2, :cond_19

    .line 5433
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 5431
    :cond_19
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_7

    .line 5437
    :cond_1a
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/e/g;->dz:Z

    .line 5439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-object v2, v2, Lcom/android/calendar/dh;->t:Ljava/lang/String;

    if-eqz v2, :cond_1b

    .line 5440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const v3, 0x7f0f039a

    const/4 v6, 0x0

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 5442
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/e/g;->cX:Z

    if-eqz v2, :cond_7

    .line 5443
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2, v14, v15, v4, v5}, Lcom/android/calendar/e/am;->a(JJ)V

    .line 5444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/g;->cT:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->cU:I

    invoke-virtual {v2, v3, v4}, Lcom/android/calendar/e/am;->b(II)V

    goto/16 :goto_3
.end method

.method private a(Lcom/android/calendar/dh;Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 2726
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2727
    iget v1, p1, Lcom/android/calendar/dh;->E:F

    float-to-int v1, v1

    sget v2, Lcom/android/calendar/e/g;->aX:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 2728
    iget v1, p1, Lcom/android/calendar/dh;->F:F

    float-to-int v1, v1

    sget v2, Lcom/android/calendar/e/g;->aY:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 2729
    iget v1, p1, Lcom/android/calendar/dh;->C:F

    float-to-int v1, v1

    sget v2, Lcom/android/calendar/e/g;->aZ:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 2730
    iget v1, p1, Lcom/android/calendar/dh;->D:F

    float-to-int v1, v1

    sget v2, Lcom/android/calendar/e/g;->ba:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 2732
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    if-eqz v1, :cond_1

    iget v1, p1, Lcom/android/calendar/dh;->E:F

    iget-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->E:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    iget v1, p1, Lcom/android/calendar/dh;->i:I

    iget-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->i:I

    if-ne v1, v2, :cond_1

    .line 2733
    iget-object v1, p0, Lcom/android/calendar/e/g;->cw:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2734
    iget-object v1, p0, Lcom/android/calendar/e/g;->cw:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2735
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 2741
    :goto_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 2742
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2743
    iget v2, p0, Lcom/android/calendar/e/g;->cA:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2744
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2745
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2746
    const-string v2, "..."

    .line 2747
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    .line 2748
    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    div-float/2addr v4, v7

    div-float v5, v3, v7

    sub-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    .line 2749
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    iget v6, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    div-float/2addr v5, v7

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v6

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    iget v6, v0, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    .line 2750
    iget v6, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v6, v0

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_0

    .line 2751
    invoke-virtual {p2, v2, v4, v5, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2752
    :cond_0
    return-void

    .line 2737
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/g;->cv:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2738
    iget-object v1, p0, Lcom/android/calendar/e/g;->cv:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private a(Lcom/android/calendar/dh;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x2

    .line 3267
    invoke-virtual {p1}, Lcom/android/calendar/dh;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    if-le v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/android/calendar/dh;->d()I

    move-result v0

    if-ge v0, v6, :cond_3

    .line 3268
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/g;->dP:Landroid/util/LruCache;

    iget-wide v2, p1, Lcom/android/calendar/dh;->y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 3270
    if-nez v0, :cond_1

    .line 3272
    iget-object v0, p1, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    .line 3273
    invoke-static {v0, v4, v4}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3274
    if-eqz v0, :cond_1

    .line 3275
    iget-object v1, p0, Lcom/android/calendar/e/g;->dP:Landroid/util/LruCache;

    iget-wide v2, p1, Lcom/android/calendar/dh;->y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v4, v0

    .line 3279
    if-eqz v4, :cond_3

    .line 3280
    iget v0, p0, Lcom/android/calendar/e/g;->aa:I

    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    iget v2, p3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 3285
    iget-object v1, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->g()I

    move-result v1

    if-ne v1, v6, :cond_4

    .line 3286
    iget v0, p3, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/e/g;->bh:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    sget v1, Lcom/android/calendar/e/g;->A:I

    sub-int v3, v0, v1

    .line 3287
    sget v0, Lcom/android/calendar/e/g;->z:I

    sub-int v2, v3, v0

    .line 3288
    iget v0, p3, Landroid/graphics/Rect;->bottom:I

    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    iget v5, p3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->z:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 3289
    sget v0, Lcom/android/calendar/e/g;->z:I

    sub-int v0, v1, v0

    .line 3297
    :goto_0
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 3298
    invoke-virtual {v5, v2, v0, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 3300
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    .line 3301
    iget v1, p1, Lcom/android/calendar/dh;->B:I

    if-ne v1, v6, :cond_2

    .line 3302
    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 3305
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p2, v4, v1, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 3308
    :cond_3
    return-void

    .line 3291
    :cond_4
    iget v1, p3, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/e/g;->bh:F

    sub-float/2addr v1, v2

    float-to-int v3, v1

    .line 3292
    sub-int v2, v3, v0

    .line 3293
    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    sget v5, Lcom/android/calendar/e/g;->bh:F

    sub-float/2addr v1, v5

    float-to-int v1, v1

    .line 3294
    sub-int v0, v1, v0

    goto :goto_0
.end method

.method private a(Lcom/android/calendar/dh;Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V
    .locals 23

    .prologue
    .line 3788
    const/16 v2, 0x1f4

    new-array v0, v2, [F

    move-object/from16 v18, v0

    .line 3791
    sget v2, Lcom/android/calendar/e/g;->bm:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 3792
    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v3, v2

    .line 3793
    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v4

    int-to-float v5, v2

    .line 3795
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/e/g;->dM:I

    move/from16 v19, v0

    .line 3798
    sget v2, Lcom/android/calendar/e/g;->aW:I

    int-to-float v2, v2

    cmpg-float v2, v3, v2

    if-gez v2, :cond_1

    .line 3975
    :cond_0
    :goto_0
    return-void

    .line 3802
    :cond_1
    const/4 v2, 0x0

    .line 3803
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    if-eqz v4, :cond_1a

    .line 3804
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    const-string v4, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    const-string v4, "com.sec.android.app.snsaccountfacebook.account_type"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_2
    const/4 v2, 0x1

    :goto_1
    move/from16 v17, v2

    .line 3808
    :goto_2
    if-eqz v17, :cond_3

    .line 3809
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/g;->cy:I

    sget v4, Lcom/android/calendar/e/g;->bb:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v2, v4

    int-to-float v2, v2

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 3812
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->cx:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sget v6, Lcom/android/calendar/e/g;->bb:I

    sub-int/2addr v4, v6

    move-object/from16 v0, p2

    iget v6, v0, Landroid/graphics/Rect;->top:I

    sget v7, Lcom/android/calendar/e/g;->bb:I

    sub-int/2addr v6, v7

    move-object/from16 v0, p2

    iget v7, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/calendar/e/g;->cy:I

    add-int/2addr v7, v8

    sget v8, Lcom/android/calendar/e/g;->bb:I

    sub-int/2addr v7, v8

    move-object/from16 v0, p2

    iget v8, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/e/g;->cy:I

    add-int/2addr v8, v9

    sget v9, Lcom/android/calendar/e/g;->bb:I

    sub-int/2addr v8, v9

    invoke-virtual {v2, v4, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 3816
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->cx:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3820
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3821
    const/4 v4, 0x1

    new-array v4, v4, [F

    .line 3822
    const-string v6, "."

    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 3823
    const/4 v6, 0x0

    aget v4, v4, v6

    const/high16 v6, 0x40400000    # 3.0f

    mul-float v20, v4, v6

    .line 3824
    const/16 v4, 0x1f4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/android/calendar/e/g;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    .line 3825
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v10

    .line 3830
    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v12, v1}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 3832
    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->dL:I

    add-int/2addr v2, v4

    add-int v2, v2, p5

    int-to-float v4, v2

    .line 3840
    const/4 v6, 0x0

    .line 3841
    const/4 v8, 0x0

    .line 3842
    const/4 v7, 0x0

    .line 3843
    const/4 v2, 0x0

    move v14, v2

    move v15, v4

    move-object v11, v12

    move/from16 v16, v5

    .line 3846
    :goto_3
    if-ge v6, v10, :cond_16

    move/from16 v0, v19

    int-to-float v2, v0

    cmpl-float v2, v16, v2

    if-ltz v2, :cond_16

    .line 3847
    mul-int/lit8 v2, v19, 0x2

    int-to-float v2, v2

    cmpg-float v2, v16, v2

    if-gez v2, :cond_8

    const/4 v2, 0x1

    move v13, v2

    .line 3849
    :goto_4
    const/4 v5, 0x0

    .line 3851
    if-eqz v17, :cond_9

    if-nez v14, :cond_9

    .line 3852
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/g;->cy:I

    int-to-float v2, v2

    sub-float v2, v3, v2

    .line 3856
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/dh;->h()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3857
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->aa:I

    int-to-float v4, v4

    sub-float v4, v16, v4

    .line 3858
    move/from16 v0, v19

    int-to-float v9, v0

    cmpg-float v4, v4, v9

    if-ltz v4, :cond_4

    if-eqz v13, :cond_5

    .line 3859
    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/g;->aa:I

    int-to-float v4, v4

    sub-float/2addr v2, v4

    :cond_5
    move v9, v6

    move v4, v6

    .line 3862
    :goto_6
    if-ge v9, v10, :cond_19

    .line 3863
    invoke-virtual {v11, v9}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 3866
    const/16 v21, 0x20

    move/from16 v0, v21

    if-ne v5, v0, :cond_6

    .line 3867
    add-int/lit8 v4, v9, 0x1

    .line 3869
    :cond_6
    add-int/lit8 v5, v9, 0x1

    invoke-virtual {v11, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v5, v5

    .line 3873
    int-to-float v0, v5

    move/from16 v21, v0

    cmpl-float v21, v21, v2

    if-lez v21, :cond_b

    .line 3874
    if-le v4, v6, :cond_a

    if-nez v13, :cond_a

    .line 3876
    invoke-virtual {v11, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    move v9, v5

    move v5, v6

    move-object v6, v8

    .line 3892
    :goto_7
    int-to-float v8, v9

    cmpg-float v8, v8, v2

    if-gtz v8, :cond_18

    .line 3893
    invoke-virtual {v11, v4, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    move v8, v10

    .line 3898
    :goto_8
    if-eqz v13, :cond_10

    int-to-float v6, v9

    cmpl-float v6, v6, v2

    if-lez v6, :cond_10

    if-lez v8, :cond_10

    .line 3899
    add-int/lit8 v9, v8, -0x1

    .line 3900
    const/4 v6, 0x0

    move/from16 v22, v6

    move v6, v9

    move/from16 v9, v22

    .line 3901
    :goto_9
    cmpg-float v12, v9, v20

    if-gtz v12, :cond_c

    if-ltz v6, :cond_c

    .line 3902
    aget v12, v18, v6

    add-float/2addr v9, v12

    .line 3903
    add-int/lit8 v6, v6, -0x1

    goto :goto_9

    .line 3804
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 3847
    :cond_8
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_4

    :cond_9
    move v2, v3

    .line 3854
    goto :goto_5

    .line 3884
    :cond_a
    invoke-virtual {v11, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move/from16 v22, v5

    move v5, v6

    move-object v6, v4

    move v4, v9

    move/from16 v9, v22

    .line 3887
    goto :goto_7

    .line 3862
    :cond_b
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 3905
    :cond_c
    rem-int/lit8 v9, v6, 0x2

    if-eqz v9, :cond_d

    .line 3906
    add-int/lit8 v6, v6, 0x1

    .line 3908
    :cond_d
    if-gez v6, :cond_e

    .line 3909
    const/4 v6, 0x0

    .line 3911
    :cond_e
    if-le v4, v6, :cond_f

    .line 3912
    const/4 v4, 0x0

    .line 3914
    :cond_f
    invoke-static {v5}, Lcom/android/calendar/e/an;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3917
    :cond_10
    invoke-static {v5}, Lcom/android/calendar/e/an;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 3918
    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/calendar/dh;->B:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_11

    .line 3919
    const/16 v5, 0x66

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 3921
    :cond_11
    if-eqz v17, :cond_15

    if-nez v14, :cond_15

    .line 3922
    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/e/g;->cy:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v9, v5, v15, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 3926
    :goto_a
    move/from16 v0, v19

    int-to-float v5, v0

    add-float/2addr v15, v5

    .line 3927
    move/from16 v0, v19

    int-to-float v5, v0

    sub-float v16, v16, v5

    .line 3928
    add-int/lit8 v12, v14, 0x1

    .line 3930
    if-lt v8, v10, :cond_17

    if-nez v13, :cond_17

    if-nez v7, :cond_17

    .line 3931
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    if-eqz v5, :cond_17

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_17

    .line 3932
    const/16 v5, 0xb

    new-array v13, v5, [F

    .line 3933
    const-string v5, "agenda:A..."

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v13}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 3934
    const/4 v6, 0x0

    .line 3935
    const/4 v5, 0x0

    .line 3937
    :cond_12
    aget v14, v13, v5

    add-float/2addr v6, v14

    .line 3938
    add-int/lit8 v5, v5, 0x1

    .line 3939
    const/16 v14, 0xb

    if-lt v5, v14, :cond_12

    .line 3941
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/calendar/e/g;->ch:Z

    if-eqz v5, :cond_14

    .line 3942
    const-string v5, "A..."

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v13}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 3944
    const/4 v6, 0x0

    .line 3945
    const/4 v5, 0x0

    .line 3947
    :cond_13
    aget v14, v13, v5

    add-float/2addr v6, v14

    .line 3948
    add-int/lit8 v5, v5, 0x1

    .line 3949
    const/4 v14, 0x4

    if-lt v5, v14, :cond_13

    .line 3952
    :cond_14
    cmpg-float v2, v6, v2

    if-gez v2, :cond_17

    .line 3953
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3954
    const/16 v5, 0x1f4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Lcom/android/calendar/e/g;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 3955
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    .line 3960
    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v6, v1}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 3962
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v5, 0x7f0b002a

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 3964
    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 3965
    const/4 v5, 0x0

    .line 3966
    const/4 v2, 0x1

    move-object v8, v6

    :goto_b
    move v14, v12

    move v10, v7

    move-object v11, v8

    move-object v12, v6

    move v7, v2

    move v8, v4

    move v6, v5

    .line 3970
    goto/16 :goto_3

    .line 3924
    :cond_15
    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v9, v5, v15, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_a

    .line 3971
    :cond_16
    sget v2, Lcom/android/calendar/e/g;->bm:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 3972
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/dh;->B:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 3973
    const/16 v2, 0x66

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_0

    :cond_17
    move v2, v7

    move v5, v8

    move-object v6, v9

    move v7, v10

    move-object v8, v11

    goto :goto_b

    :cond_18
    move v8, v4

    move v4, v5

    move-object v5, v6

    goto/16 :goto_8

    :cond_19
    move v9, v5

    move v4, v6

    move v5, v8

    move-object v6, v12

    goto/16 :goto_7

    :cond_1a
    move/from16 v17, v2

    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/android/calendar/e/g;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->i(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/e/g;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->b(Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Lcom/android/calendar/dh;)V
    .locals 7

    .prologue
    .line 1993
    iget-wide v2, p2, Lcom/android/calendar/dh;->m:J

    iget-wide v4, p2, Lcom/android/calendar/dh;->n:J

    invoke-virtual {p2}, Lcom/android/calendar/dh;->e()Z

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/calendar/e/g;->a(JJZ)Ljava/lang/String;

    move-result-object v0

    .line 1994
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1995
    const-string v0, ". "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1996
    invoke-virtual {p2}, Lcom/android/calendar/dh;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1997
    const-string v0, ". "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1998
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1999
    const-string v0, ". "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2000
    return-void
.end method

.method private a(IIZ)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 5901
    const/4 v0, 0x0

    .line 5905
    if-eqz p3, :cond_6

    .line 5908
    iget-object v4, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 5909
    iget v3, p0, Lcom/android/calendar/e/g;->ai:I

    .line 5910
    iget v2, p0, Lcom/android/calendar/e/g;->aj:I

    .line 5911
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->ak:Z

    .line 5913
    :goto_0
    iget v5, p0, Lcom/android/calendar/e/g;->bx:I

    iget v6, p0, Lcom/android/calendar/e/g;->ab:I

    add-int/2addr v5, v6

    add-int/2addr v5, p1

    .line 5914
    iput v5, p0, Lcom/android/calendar/e/g;->ci:I

    .line 5915
    iput p2, p0, Lcom/android/calendar/e/g;->cj:I

    .line 5916
    iget v6, p0, Lcom/android/calendar/e/g;->bW:I

    if-ge v5, v6, :cond_0

    .line 5917
    iget v5, p0, Lcom/android/calendar/e/g;->bW:I

    .line 5920
    :cond_0
    iget v6, p0, Lcom/android/calendar/e/g;->bW:I

    sub-int v6, v5, v6

    sget v8, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v8, v8, 0x1

    div-int/2addr v6, v8

    .line 5921
    iget v8, p0, Lcom/android/calendar/e/g;->i:I

    if-lt v6, v8, :cond_1

    .line 5922
    iget v6, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v6, v6, -0x1

    .line 5924
    :cond_1
    iget v8, p0, Lcom/android/calendar/e/g;->R:I

    add-int/2addr v6, v8

    .line 5925
    invoke-direct {p0, v6}, Lcom/android/calendar/e/g;->setSelectedDay(I)V

    .line 5927
    sget v6, Lcom/android/calendar/e/g;->bR:I

    if-ge p2, v6, :cond_2

    .line 5928
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->c(Z)V

    .line 5959
    :goto_1
    return v1

    .line 5932
    :cond_2
    iget v6, p0, Lcom/android/calendar/e/g;->bY:I

    invoke-direct {p0, v6}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 5934
    iget v6, p0, Lcom/android/calendar/e/g;->bX:I

    if-ge p2, v6, :cond_4

    .line 5935
    iput-boolean v7, p0, Lcom/android/calendar/e/g;->ak:Z

    .line 5950
    :goto_2
    invoke-direct {p0, v5, p2}, Lcom/android/calendar/e/g;->b(II)V

    .line 5951
    invoke-direct {p0, v7}, Lcom/android/calendar/e/g;->c(Z)V

    .line 5953
    if-eqz p3, :cond_3

    .line 5954
    iput-object v4, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 5955
    iput v3, p0, Lcom/android/calendar/e/g;->ai:I

    .line 5956
    iput v2, p0, Lcom/android/calendar/e/g;->aj:I

    .line 5957
    iput-boolean v0, p0, Lcom/android/calendar/e/g;->ak:Z

    :cond_3
    move v1, v7

    .line 5959
    goto :goto_1

    .line 5938
    :cond_4
    iget v6, p0, Lcom/android/calendar/e/g;->bX:I

    sub-int v6, p2, v6

    .line 5940
    iget v8, p0, Lcom/android/calendar/e/g;->bZ:I

    if-ge v6, v8, :cond_5

    .line 5941
    iget v6, p0, Lcom/android/calendar/e/g;->aj:I

    add-int/lit8 v6, v6, -0x1

    invoke-direct {p0, v6}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 5947
    :goto_3
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->ak:Z

    goto :goto_2

    .line 5943
    :cond_5
    iget v8, p0, Lcom/android/calendar/e/g;->aj:I

    iget v9, p0, Lcom/android/calendar/e/g;->bZ:I

    sub-int/2addr v6, v9

    sget v9, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v9, v9, 0x1

    div-int/2addr v6, v9

    add-int/2addr v6, v8

    invoke-direct {p0, v6}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    goto :goto_3

    :cond_6
    move v2, v1

    move v3, v1

    move-object v4, v0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/e/g;IIZ)Z
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/e/g;->a(IIZ)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->J:Z

    return p1
.end method

.method private a(Ljava/util/ArrayList;Lcom/android/calendar/dh;)Z
    .locals 6

    .prologue
    .line 2402
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 2403
    iget-wide v2, v0, Lcom/android/calendar/dh;->q:J

    iget-wide v4, p2, Lcom/android/calendar/dh;->q:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 2404
    const/4 v0, 0x1

    .line 2407
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(F)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6340
    const/4 v0, 0x0

    .line 6341
    sget v2, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v2, v2, 0x1

    int-to-float v3, v2

    move v2, v0

    move v0, v1

    .line 6344
    :goto_0
    iget v4, p0, Lcom/android/calendar/e/g;->i:I

    if-ge v0, v4, :cond_1

    .line 6345
    add-float/2addr v2, v3

    .line 6346
    cmpg-float v4, p1, v2

    if-gez v4, :cond_0

    .line 6351
    :goto_1
    return v0

    .line 6344
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private b(I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2819
    sget v0, Lcom/android/calendar/e/g;->as:I

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    mul-int/2addr v0, v1

    .line 2820
    sget v1, Lcom/android/calendar/e/g;->as:I

    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    mul-int/2addr v1, v2

    sub-int v1, v0, v1

    .line 2821
    sget v0, Lcom/android/calendar/e/g;->as:I

    mul-int/2addr v0, p1

    .line 2823
    if-le p1, v4, :cond_0

    .line 2824
    add-int/lit8 v2, p1, -0x1

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2827
    :cond_0
    add-int v2, v1, p1

    iget v3, p0, Lcom/android/calendar/e/g;->i:I

    if-le v2, v3, :cond_1

    .line 2828
    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    sub-int v1, v2, v1

    sub-int v1, p1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2830
    :cond_1
    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    if-ne v1, v4, :cond_2

    .line 2831
    if-nez p1, :cond_3

    .line 2832
    iget v1, p0, Lcom/android/calendar/e/g;->ab:I

    add-int/2addr v0, v1

    .line 2837
    :cond_2
    :goto_0
    return v0

    .line 2833
    :cond_3
    if-ne p1, v4, :cond_2

    .line 2834
    iget v1, p0, Lcom/android/calendar/e/g;->ab:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/e/g;I)I
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/android/calendar/e/g;->cj:I

    return p1
.end method

.method private b(Lcom/android/calendar/dh;)Lcom/android/calendar/dh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5196
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_3

    .line 5197
    iget v0, p1, Lcom/android/calendar/dh;->F:F

    iget v1, p1, Lcom/android/calendar/dh;->E:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 5198
    iget v1, p0, Lcom/android/calendar/e/g;->dW:I

    iget v2, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, p1, Lcom/android/calendar/dh;->E:F

    .line 5200
    iget v1, p1, Lcom/android/calendar/dh;->E:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_0

    .line 5201
    iput v3, p1, Lcom/android/calendar/dh;->E:F

    .line 5203
    :cond_0
    iget v1, p1, Lcom/android/calendar/dh;->E:F

    int-to-float v2, v0

    add-float/2addr v1, v2

    iput v1, p1, Lcom/android/calendar/dh;->F:F

    .line 5204
    iget v1, p0, Lcom/android/calendar/e/g;->bA:I

    iget v2, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v1, v2

    sget v2, Lcom/android/calendar/e/g;->bR:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/e/g;->bQ:I

    sub-int/2addr v1, v2

    .line 5205
    iget v2, p1, Lcom/android/calendar/dh;->F:F

    int-to-float v3, v1

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 5206
    int-to-float v1, v1

    iput v1, p1, Lcom/android/calendar/dh;->F:F

    .line 5207
    iget v1, p1, Lcom/android/calendar/dh;->F:F

    int-to-float v0, v0

    sub-float v0, v1, v0

    iput v0, p1, Lcom/android/calendar/dh;->E:F

    .line 5210
    :cond_1
    iget v0, p1, Lcom/android/calendar/dh;->D:F

    iget v1, p1, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 5211
    iget v1, p0, Lcom/android/calendar/e/g;->dV:I

    iget v2, p0, Lcom/android/calendar/e/g;->dY:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, p1, Lcom/android/calendar/dh;->C:F

    .line 5212
    iget v1, p1, Lcom/android/calendar/dh;->C:F

    int-to-float v0, v0

    add-float/2addr v0, v1

    iput v0, p1, Lcom/android/calendar/dh;->D:F

    .line 5233
    :cond_2
    :goto_0
    return-object p1

    .line 5213
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_5

    .line 5214
    iget v0, p0, Lcom/android/calendar/e/g;->dX:I

    iget v1, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p1, Lcom/android/calendar/dh;->F:F

    .line 5215
    iget v0, p1, Lcom/android/calendar/dh;->F:F

    iget v1, p1, Lcom/android/calendar/dh;->E:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 5216
    sget v1, Lcom/android/calendar/e/g;->bF:I

    if-ge v0, v1, :cond_4

    .line 5217
    iget v0, p1, Lcom/android/calendar/dh;->E:F

    sget v1, Lcom/android/calendar/e/g;->bF:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p1, Lcom/android/calendar/dh;->F:F

    .line 5219
    :cond_4
    iget v0, p0, Lcom/android/calendar/e/g;->bA:I

    iget v1, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v0, v1

    .line 5220
    iget v1, p1, Lcom/android/calendar/dh;->F:F

    int-to-float v2, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 5221
    int-to-float v0, v0

    iput v0, p1, Lcom/android/calendar/dh;->F:F

    goto :goto_0

    .line 5223
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_2

    .line 5224
    iget v0, p0, Lcom/android/calendar/e/g;->dW:I

    iget v1, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p1, Lcom/android/calendar/dh;->E:F

    .line 5225
    iget v0, p1, Lcom/android/calendar/dh;->F:F

    iget v1, p1, Lcom/android/calendar/dh;->E:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 5226
    sget v1, Lcom/android/calendar/e/g;->bF:I

    if-ge v0, v1, :cond_6

    .line 5227
    iget v0, p1, Lcom/android/calendar/dh;->F:F

    sget v1, Lcom/android/calendar/e/g;->bF:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p1, Lcom/android/calendar/dh;->E:F

    .line 5229
    :cond_6
    iget v0, p1, Lcom/android/calendar/dh;->E:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    .line 5230
    iput v3, p1, Lcom/android/calendar/dh;->E:F

    goto :goto_0
.end method

.method private b(II)V
    .locals 13

    .prologue
    .line 5963
    iget v1, p0, Lcom/android/calendar/e/g;->ai:I

    .line 5964
    sget v4, Lcom/android/calendar/e/g;->as:I

    .line 5965
    iget-object v7, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    .line 5966
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 5967
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    iget v2, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v2

    .line 5968
    const/4 v3, 0x0

    .line 5969
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 5970
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    .line 5972
    iget-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 5973
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->ak:Z

    if-eqz v0, :cond_6

    .line 5975
    const v3, 0x461c4000    # 10000.0f

    .line 5976
    const/4 v2, 0x0

    .line 5977
    iget v0, p0, Lcom/android/calendar/e/g;->bQ:I

    int-to-float v6, v0

    .line 5978
    sget v0, Lcom/android/calendar/e/g;->bR:I

    sget v1, Lcom/android/calendar/e/g;->aN:I

    add-int v7, v0, v1

    .line 5979
    const/4 v0, 0x4

    .line 5980
    iget v1, p0, Lcom/android/calendar/e/g;->bS:I

    const/4 v4, 0x4

    if-le v1, v4, :cond_13

    .line 5982
    const/4 v0, 0x3

    move v1, v0

    .line 5984
    :goto_0
    iget-object v8, p0, Lcom/android/calendar/e/g;->af:Ljava/util/ArrayList;

    .line 5985
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 5986
    const/4 v0, 0x0

    move v5, v0

    :goto_1
    if-ge v5, v9, :cond_12

    .line 5987
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 5988
    invoke-virtual {v0}, Lcom/android/calendar/dh;->e()Z

    move-result v4

    if-eqz v4, :cond_11

    sget-boolean v4, Lcom/android/calendar/e/g;->bU:Z

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcom/android/calendar/dh;->c()I

    move-result v4

    if-lt v4, v1, :cond_0

    move-object v0, v2

    move v2, v3

    .line 5986
    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    move-object v2, v0

    goto :goto_1

    .line 5994
    :cond_0
    iget v4, v0, Lcom/android/calendar/dh;->i:I

    iget v10, p0, Lcom/android/calendar/e/g;->ai:I

    if-gt v4, v10, :cond_11

    iget v4, v0, Lcom/android/calendar/dh;->j:I

    iget v10, p0, Lcom/android/calendar/e/g;->ai:I

    if-lt v4, v10, :cond_11

    .line 5995
    sget-boolean v4, Lcom/android/calendar/e/g;->bU:Z

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/android/calendar/e/g;->bS:I

    int-to-float v4, v4

    .line 5997
    :goto_3
    div-float v4, v6, v4

    .line 5998
    sget v10, Lcom/android/calendar/e/g;->aO:I

    int-to-float v10, v10

    cmpl-float v10, v4, v10

    if-lez v10, :cond_1

    .line 5999
    sget v4, Lcom/android/calendar/e/g;->aO:I

    int-to-float v4, v4

    .line 6001
    :cond_1
    int-to-float v10, v7

    invoke-virtual {v0}, Lcom/android/calendar/dh;->c()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v11, v4

    add-float/2addr v10, v11

    .line 6002
    add-float/2addr v4, v10

    .line 6003
    int-to-float v11, p2

    cmpg-float v11, v10, v11

    if-gez v11, :cond_4

    int-to-float v11, p2

    cmpl-float v11, v4, v11

    if-lez v11, :cond_4

    .line 6006
    iget-object v1, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6023
    :goto_4
    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 6102
    :cond_2
    :goto_5
    return-void

    .line 5995
    :cond_3
    const/high16 v4, 0x40800000    # 4.0f

    goto :goto_3

    .line 6011
    :cond_4
    int-to-float v11, p2

    cmpl-float v11, v10, v11

    if-ltz v11, :cond_5

    .line 6012
    int-to-float v4, p2

    sub-float v4, v10, v4

    .line 6016
    :goto_6
    cmpg-float v10, v4, v3

    if-gez v10, :cond_11

    move v2, v4

    .line 6018
    goto :goto_2

    .line 6014
    :cond_5
    int-to-float v10, p2

    sub-float v4, v10, v4

    goto :goto_6

    .line 6028
    :cond_6
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iget v5, p0, Lcom/android/calendar/e/g;->bX:I

    sub-int/2addr v0, v5

    add-int v9, p2, v0

    .line 6031
    iget-object v10, p0, Lcom/android/calendar/e/g;->at:Landroid/graphics/Rect;

    .line 6032
    add-int/lit8 v0, p1, -0xa

    iget v5, p0, Lcom/android/calendar/e/g;->bW:I

    sub-int/2addr v0, v5

    iput v0, v10, Landroid/graphics/Rect;->left:I

    .line 6033
    add-int/lit8 v0, p1, 0xa

    iget v5, p0, Lcom/android/calendar/e/g;->bW:I

    sub-int/2addr v0, v5

    iput v0, v10, Landroid/graphics/Rect;->right:I

    .line 6034
    add-int/lit8 v0, v9, -0xa

    iput v0, v10, Landroid/graphics/Rect;->top:I

    .line 6035
    add-int/lit8 v0, v9, 0xa

    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    .line 6037
    iget-object v0, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    .line 6039
    const/4 v5, 0x0

    move v6, v5

    :goto_7
    if-ge v6, v8, :cond_9

    .line 6040
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/calendar/dh;

    .line 6042
    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/e/al;->a(IIIILcom/android/calendar/dh;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 6039
    :cond_7
    :goto_8
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_7

    .line 6048
    :cond_8
    invoke-virtual {v0, v5, v10}, Lcom/android/calendar/e/al;->a(Lcom/android/calendar/dh;Landroid/graphics/Rect;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 6049
    iget-object v11, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 6055
    :cond_9
    iget-object v1, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 6056
    const/4 v4, 0x0

    .line 6057
    iget v1, p0, Lcom/android/calendar/e/g;->bC:I

    iget v2, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v1, v2

    int-to-float v3, v1

    .line 6058
    iget-object v1, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/dh;

    .line 6059
    int-to-float v2, p1

    int-to-float v6, v9

    invoke-virtual {v0, v2, v6, v1}, Lcom/android/calendar/e/al;->a(FFLcom/android/calendar/dh;)F

    move-result v2

    .line 6060
    cmpg-float v6, v2, v3

    if-gez v6, :cond_10

    move v12, v2

    move-object v2, v1

    move v1, v12

    :goto_a
    move v3, v1

    move-object v4, v2

    .line 6064
    goto :goto_9

    .line 6067
    :cond_a
    if-eqz v4, :cond_2

    .line 6068
    invoke-direct {p0, v4}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 6073
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 6074
    iput-object v4, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    .line 6080
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v1, v0, Lcom/android/calendar/dh;->i:I

    .line 6081
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v2, v0, Lcom/android/calendar/dh;->j:I

    .line 6082
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    if-ge v0, v1, :cond_d

    .line 6083
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedDay(I)V

    .line 6088
    :cond_c
    :goto_b
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->k:I

    div-int/lit8 v3, v0, 0x3c

    .line 6090
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->k:I

    iget-object v4, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v4, v4, Lcom/android/calendar/dh;->l:I

    if-ge v0, v4, :cond_e

    .line 6091
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->l:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x3c

    .line 6096
    :goto_c
    iget v4, p0, Lcom/android/calendar/e/g;->aj:I

    if-ge v4, v3, :cond_f

    iget v4, p0, Lcom/android/calendar/e/g;->ai:I

    if-ne v4, v1, :cond_f

    .line 6097
    invoke-direct {p0, v3}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    goto/16 :goto_5

    .line 6084
    :cond_d
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    if-le v0, v2, :cond_c

    .line 6085
    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->setSelectedDay(I)V

    goto :goto_b

    .line 6093
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->l:I

    div-int/lit8 v0, v0, 0x3c

    goto :goto_c

    .line 6098
    :cond_f
    iget v1, p0, Lcom/android/calendar/e/g;->aj:I

    if-le v1, v0, :cond_2

    iget v1, p0, Lcom/android/calendar/e/g;->ai:I

    if-ne v1, v2, :cond_2

    .line 6099
    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    goto/16 :goto_5

    :cond_10
    move v1, v3

    move-object v2, v4

    goto :goto_a

    :cond_11
    move-object v0, v2

    move v2, v3

    goto/16 :goto_2

    :cond_12
    move-object v0, v2

    goto/16 :goto_4

    :cond_13
    move v1, v0

    goto/16 :goto_0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2759
    iget-object v0, p0, Lcom/android/calendar/e/g;->aw:Landroid/graphics/Paint;

    .line 2760
    iget-object v1, p0, Lcom/android/calendar/e/g;->at:Landroid/graphics/Rect;

    .line 2762
    invoke-direct {p0, v1, p1, v0}, Lcom/android/calendar/e/g;->b(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 2764
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->cf:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/e/g;->cd:Z

    if-nez v1, :cond_0

    .line 2765
    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/g;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 2767
    :cond_0
    return-void
.end method

.method private b(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 3049
    invoke-direct {p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v0, :cond_0

    .line 3050
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iget v1, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->bR:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->bQ:I

    sub-int v6, v0, v1

    .line 3051
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-object v4, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    iget v5, p0, Lcom/android/calendar/e/g;->by:I

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;II)Landroid/graphics/Rect;

    move-result-object v2

    .line 3053
    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->setupTextRect(Landroid/graphics/Rect;)V

    .line 3054
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {v0}, Lcom/android/calendar/dh;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 3055
    iget v0, v2, Landroid/graphics/Rect;->right:I

    sget v1, Lcom/android/calendar/e/g;->z:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->A:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->B:I

    sub-int/2addr v0, v1

    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 3056
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-object v4, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    move-object v0, p0

    move-object v3, p1

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V

    .line 3057
    iget v0, v2, Landroid/graphics/Rect;->right:I

    sget v1, Lcom/android/calendar/e/g;->z:I

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->A:I

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->B:I

    add-int/2addr v0, v1

    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 3061
    :goto_0
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-eqz v0, :cond_0

    .line 3062
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v0, p1, v2}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 3065
    :cond_0
    return-void

    .line 3059
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-object v4, p0, Lcom/android/calendar/e/g;->ax:Landroid/graphics/Paint;

    move-object v0, p0

    move-object v3, p1

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V

    goto :goto_0
.end method

.method private b(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 2770
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v0

    .line 2771
    iget v1, p0, Lcom/android/calendar/e/g;->bX:I

    add-int/lit8 v4, v1, -0x1

    .line 2773
    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2774
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2776
    sget v1, Lcom/android/calendar/e/g;->bo:I

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2777
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2778
    iget v1, p0, Lcom/android/calendar/e/g;->bW:I

    int-to-float v1, v1

    int-to-float v2, v4

    int-to-float v3, v0

    int-to-float v4, v4

    move-object v0, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2779
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2780
    return-void
.end method

.method private b(Landroid/text/format/Time;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1129
    iget v0, p1, Landroid/text/format/Time;->weekDay:I

    .line 1130
    iget v1, p0, Lcom/android/calendar/e/g;->aE:I

    sub-int/2addr v0, v1

    .line 1131
    if-eqz v0, :cond_1

    .line 1132
    if-gez v0, :cond_0

    .line 1133
    add-int/lit8 v0, v0, 0x7

    .line 1135
    :cond_0
    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    sub-int v0, v1, v0

    iput v0, p1, Landroid/text/format/Time;->monthDay:I

    .line 1136
    invoke-virtual {p1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 1137
    invoke-virtual {p1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->ch:Z

    if-eqz v0, :cond_1

    .line 1138
    invoke-static {p1}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 1139
    invoke-virtual {p1, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1142
    :cond_1
    return-void
.end method

.method private b(Landroid/view/MotionEvent;Lcom/android/calendar/dh;)V
    .locals 10

    .prologue
    .line 5053
    const/4 v0, 0x0

    .line 5054
    if-eqz p1, :cond_10

    .line 5055
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    move v6, v0

    .line 5057
    :goto_0
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 5058
    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 5059
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v0, v1}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 5060
    iget v1, p2, Lcom/android/calendar/dh;->i:I

    if-eq v1, v0, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/e/g;->I:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/e/g;->H:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 5061
    iget v1, p2, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v0, v1

    .line 5062
    iget v1, p2, Lcom/android/calendar/dh;->i:I

    add-int/2addr v1, v0

    iput v1, p2, Lcom/android/calendar/dh;->i:I

    .line 5063
    iget-wide v2, p2, Lcom/android/calendar/dh;->m:J

    int-to-long v4, v0

    const-wide/32 v8, 0x5265c00

    mul-long/2addr v4, v8

    add-long/2addr v2, v4

    iput-wide v2, p2, Lcom/android/calendar/dh;->m:J

    .line 5064
    iget v1, p2, Lcom/android/calendar/dh;->j:I

    add-int/2addr v1, v0

    iput v1, p2, Lcom/android/calendar/dh;->j:I

    .line 5065
    iget-wide v2, p2, Lcom/android/calendar/dh;->n:J

    int-to-long v0, v0

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p2, Lcom/android/calendar/dh;->n:J

    .line 5067
    :cond_0
    iput-object p2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 5068
    iput-object p2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 5069
    invoke-direct {p0}, Lcom/android/calendar/e/g;->F()Z

    move-result v0

    if-nez v0, :cond_2

    .line 5070
    sget-object v0, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 5163
    :cond_1
    :goto_1
    return-void

    .line 5073
    :cond_2
    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-nez v0, :cond_7

    .line 5074
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    sget-object v1, Lcom/android/calendar/e/ag;->c:Lcom/android/calendar/e/ag;

    if-eq v0, v1, :cond_3

    .line 5075
    iget v0, p2, Lcom/android/calendar/dh;->D:F

    iget v1, p2, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v0, v1

    .line 5076
    iget v1, p0, Lcom/android/calendar/e/g;->dV:I

    iget v2, p0, Lcom/android/calendar/e/g;->dY:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    .line 5077
    add-float v2, v1, v0

    .line 5078
    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 5079
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(F)I

    move-result v1

    .line 5080
    iget v2, p0, Lcom/android/calendar/e/g;->dV:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    add-float/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(F)I

    move-result v0

    .line 5082
    iget v2, p0, Lcom/android/calendar/e/g;->dZ:I

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    sget v3, Lcom/android/calendar/e/g;->bE:I

    div-int/lit8 v3, v3, 0x6

    if-ge v2, v3, :cond_3

    if-ne v1, v0, :cond_3

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->I:Z

    if-nez v0, :cond_3

    .line 5083
    sget-object v0, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 5084
    invoke-direct {p0}, Lcom/android/calendar/e/g;->Q()V

    .line 5085
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    goto :goto_1

    .line 5091
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_7

    .line 5092
    :cond_4
    iget v0, p0, Lcom/android/calendar/e/g;->dZ:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lcom/android/calendar/e/g;->bE:I

    div-int/lit8 v1, v1, 0x6

    if-ge v0, v1, :cond_5

    iget v0, p0, Lcom/android/calendar/e/g;->dY:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lcom/android/calendar/e/g;->as:I

    if-lt v0, v1, :cond_6

    :cond_5
    iget v0, p0, Lcom/android/calendar/e/g;->dU:I

    if-ge v6, v0, :cond_7

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->F:Z

    if-eqz v0, :cond_7

    .line 5094
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 5095
    sget-object v0, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 5096
    invoke-direct {p0}, Lcom/android/calendar/e/g;->Q()V

    .line 5097
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    goto/16 :goto_1

    .line 5103
    :cond_7
    iget-wide v0, p2, Lcom/android/calendar/dh;->b:J

    .line 5104
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 5105
    iput-object v1, p0, Lcom/android/calendar/e/g;->V:Landroid/net/Uri;

    .line 5108
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/event/av;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 5112
    if-eqz v3, :cond_8

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_9

    .line 5113
    :cond_8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 5114
    sget-object v0, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 5115
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 5116
    if-eqz v3, :cond_1

    .line 5117
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 5122
    :cond_9
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 5123
    const-string v0, "rrule"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 5124
    const-string v0, "_sync_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 5126
    iget-wide v4, p2, Lcom/android/calendar/dh;->m:J

    .line 5127
    const-string v0, "dtstart"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 5129
    cmp-long v0, v4, v8

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/android/calendar/e/g;->eh:Z

    .line 5130
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->eg:Z

    .line 5132
    const/4 v1, 0x0

    .line 5136
    if-eqz v2, :cond_f

    .line 5137
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->eg:Z

    if-eqz v0, :cond_d

    .line 5140
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->eh:Z

    if-eqz v0, :cond_c

    .line 5142
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    .line 5154
    :goto_3
    iget-boolean v2, p0, Lcom/android/calendar/e/g;->eh:Z

    if-nez v2, :cond_a

    .line 5155
    add-int/lit8 v2, v1, 0x1

    iget-object v4, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const v5, 0x7f0f0435

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v0, v1

    move v1, v2

    .line 5157
    :cond_a
    add-int/lit8 v2, v1, 0x1

    iget-object v2, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const v4, 0x7f0f0048

    invoke-virtual {v2, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v0, v1

    .line 5158
    invoke-virtual {p0, v0, v3}, Lcom/android/calendar/e/g;->a([Ljava/lang/CharSequence;Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 5129
    :cond_b
    const/4 v0, 0x0

    goto :goto_2

    .line 5144
    :cond_c
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    goto :goto_3

    .line 5147
    :cond_d
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->eh:Z

    if-eqz v0, :cond_e

    .line 5148
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    .line 5152
    :goto_4
    const/4 v2, 0x1

    iget-object v4, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const v5, 0x7f0f02e6

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v0, v1

    move v1, v2

    goto :goto_3

    .line 5150
    :cond_e
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    goto :goto_4

    .line 5161
    :cond_f
    const/4 v0, 0x3

    invoke-direct {p0, p2, v3, v0, v6}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V

    goto/16 :goto_1

    :cond_10
    move v6, v0

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/calendar/e/g;->r()V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/e/g;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->d(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 4195
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 4196
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 4197
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 4198
    iget v2, p0, Lcom/android/calendar/e/g;->ai:I

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 4199
    invoke-virtual {v0, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 4200
    const-wide/16 v8, -0x1

    cmp-long v2, v4, v8

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->ch:Z

    if-eqz v2, :cond_0

    .line 4201
    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v4

    .line 4203
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/al;->a(J)V

    .line 4204
    new-instance v0, Lcom/android/calendar/month/a;

    const/4 v6, 0x1

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/month/a;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;JZZ)V

    .line 4205
    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 4206
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 4208
    sget-object v3, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 4209
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_1

    .line 4211
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4218
    :cond_1
    :try_start_1
    sget-object v1, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 4219
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 4224
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/e/u;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/u;-><init>(Lcom/android/calendar/e/g;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4232
    :goto_0
    return-void

    .line 4212
    :catch_0
    move-exception v0

    goto :goto_0

    .line 4220
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private b(Ljava/util/ArrayList;Lcom/android/calendar/dh;)V
    .locals 8

    .prologue
    .line 2411
    const/4 v1, 0x0

    .line 2412
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 2413
    iget-wide v4, v0, Lcom/android/calendar/dh;->q:J

    iget-wide v6, p2, Lcom/android/calendar/dh;->q:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    :goto_1
    move-object v1, v0

    .line 2416
    goto :goto_0

    .line 2417
    :cond_0
    if-eqz v1, :cond_1

    .line 2418
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2420
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private b(Z)V
    .locals 12

    .prologue
    const-wide/32 v2, 0x40000000

    const/4 v8, 0x0

    const-wide/32 v10, 0x36ee80

    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    .line 1281
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 1283
    iget-object v1, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1284
    iput-wide v6, p0, Lcom/android/calendar/e/g;->K:J

    .line 1285
    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    if-le v1, v4, :cond_6

    .line 1290
    if-eqz p1, :cond_4

    .line 1291
    if-nez v0, :cond_1

    .line 1292
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v0

    .line 1293
    add-long/2addr v10, v0

    .line 1295
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1296
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1297
    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1298
    invoke-virtual {v5, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 1300
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 1356
    :cond_0
    :goto_0
    return-void

    .line 1302
    :cond_1
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->dp:Z

    if-eqz v1, :cond_2

    .line 1303
    iget-object v1, p0, Lcom/android/calendar/e/g;->do:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->interrupt()V

    .line 1305
    :cond_2
    invoke-virtual {v0}, Lcom/android/calendar/dh;->d()I

    move-result v1

    sget v2, Lcom/android/calendar/e/g;->a:I

    if-lt v1, v2, :cond_3

    .line 1306
    iget-wide v2, v0, Lcom/android/calendar/dh;->m:J

    iget-wide v0, v0, Lcom/android/calendar/dh;->n:J

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/android/calendar/e/g;->a(JJ)V

    goto :goto_0

    .line 1308
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1309
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1310
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1317
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 1318
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->dp:Z

    if-eqz v1, :cond_5

    .line 1319
    iget-object v1, p0, Lcom/android/calendar/e/g;->do:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->interrupt()V

    .line 1321
    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1322
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1323
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1333
    :cond_6
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    if-eqz v1, :cond_7

    .line 1334
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v0

    .line 1335
    add-long v2, v0, v10

    .line 1336
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/calendar/e/g;->a(JJ)V

    goto :goto_0

    .line 1337
    :cond_7
    if-nez v0, :cond_8

    .line 1338
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v0

    .line 1339
    add-long/2addr v10, v0

    .line 1341
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1342
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1343
    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1344
    invoke-virtual {v5, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 1346
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    .line 1348
    :cond_8
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->dp:Z

    if-eqz v1, :cond_9

    .line 1349
    iget-object v1, p0, Lcom/android/calendar/e/g;->do:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->interrupt()V

    .line 1351
    :cond_9
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1352
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1353
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->dC:Z

    return p1
.end method

.method private c(I)I
    .locals 6

    .prologue
    .line 6355
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 6356
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 6357
    const/4 v1, 0x2

    aget v2, v0, v1

    float-to-double v2, v2

    const-wide v4, 0x3fea8f5c28f5c28fL    # 0.83

    mul-double/2addr v2, v4

    double-to-float v2, v2

    aput v2, v0, v1

    .line 6358
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/e/g;I)I
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/android/calendar/e/g;->cP:I

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/e/g;)Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    return-object v0
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2894
    iget-object v5, p0, Lcom/android/calendar/e/g;->aw:Landroid/graphics/Paint;

    .line 2895
    iget-object v7, p0, Lcom/android/calendar/e/g;->at:Landroid/graphics/Rect;

    .line 2897
    sget v0, Lcom/android/calendar/e/g;->bs:I

    if-eqz v0, :cond_0

    .line 2898
    invoke-direct {p0, v7, p1, v5}, Lcom/android/calendar/e/g;->e(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 2900
    :cond_0
    invoke-direct {p0, v7, p1, v5}, Lcom/android/calendar/e/g;->d(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 2903
    iget v1, p0, Lcom/android/calendar/e/g;->R:I

    .line 2904
    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2905
    :goto_0
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    if-ge v2, v0, :cond_2

    move-object v0, p0

    move-object v4, p1

    .line 2908
    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/e/g;->a(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 2910
    iget v0, p0, Lcom/android/calendar/e/g;->P:I

    if-ne v1, v0, :cond_1

    .line 2911
    iget-object v0, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    sget v4, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    iget-object v4, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->minute:I

    sget v6, Lcom/android/calendar/e/g;->bE:I

    mul-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x3c

    add-int/2addr v0, v4

    add-int/lit8 v9, v0, 0x1

    .line 2915
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    if-lt v9, v0, :cond_1

    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iget v4, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x2

    if-ge v9, v0, :cond_1

    move-object v6, p0

    move v8, v2

    move-object v10, p1

    move-object v11, v5

    .line 2916
    invoke-direct/range {v6 .. v11}, Lcom/android/calendar/e/g;->a(Landroid/graphics/Rect;IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 2905
    :cond_1
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2921
    :cond_2
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->H:Z

    if-nez v0, :cond_3

    .line 2922
    invoke-direct {p0, p1, v5}, Lcom/android/calendar/e/g;->b(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 2924
    :cond_3
    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2925
    invoke-direct {p0, v7, p1, v5}, Lcom/android/calendar/e/g;->c(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->W:Z

    .line 2926
    invoke-direct {p0}, Lcom/android/calendar/e/g;->D()V

    .line 2927
    return-void
.end method

.method private c(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 3096
    invoke-direct {p0, p2}, Lcom/android/calendar/e/g;->setupHourTextPaint(Landroid/graphics/Paint;)V

    .line 3098
    iget v0, p0, Lcom/android/calendar/e/g;->bP:I

    add-int/lit8 v0, v0, 0x1

    sget v1, Lcom/android/calendar/e/g;->aP:I

    add-int/2addr v1, v0

    .line 3099
    invoke-static {}, Lcom/android/calendar/hj;->h()Z

    move-result v3

    .line 3100
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    const/16 v0, 0x18

    if-ge v1, v0, :cond_6

    .line 3101
    iget-object v0, p0, Lcom/android/calendar/e/g;->ca:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 3102
    iget-boolean v4, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/android/calendar/e/g;->cd:Z

    if-nez v4, :cond_1

    .line 3103
    const/16 v4, 0xc

    if-lt v1, v4, :cond_0

    const/16 v4, 0x17

    if-le v1, v4, :cond_4

    .line 3104
    :cond_0
    if-eqz v3, :cond_3

    .line 3105
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/calendar/e/g;->cB:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3117
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 3118
    sget v4, Lcom/android/calendar/e/g;->aQ:I

    int-to-float v4, v4

    int-to-float v5, v2

    invoke-virtual {p1, v0, v4, v5, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 3120
    :cond_2
    sget v0, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 3100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3107
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/calendar/e/g;->cB:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 3110
    :cond_4
    if-eqz v3, :cond_5

    .line 3111
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/calendar/e/g;->cC:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 3113
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/calendar/e/g;->cC:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 3122
    :cond_6
    return-void
.end method

.method private c(Landroid/text/format/Time;)V
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 6413
    invoke-static {p1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-nez v0, :cond_0

    .line 6414
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v6, p1

    move-object v12, v5

    move-object v13, v5

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 6419
    :goto_0
    return-void

    .line 6417
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    goto :goto_0
.end method

.method private c(Lcom/android/calendar/dh;)V
    .locals 12

    .prologue
    const-wide/32 v10, 0x1b7740

    const/4 v9, 0x0

    const/high16 v7, 0x41200000    # 10.0f

    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 5449
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->b(Lcom/android/calendar/dh;)Lcom/android/calendar/dh;

    move-result-object v0

    .line 5451
    iget-object v1, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    invoke-virtual {v1, v0}, Lcom/android/calendar/e/al;->a(Lcom/android/calendar/dh;)I

    move-result v1

    .line 5453
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v3, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 5454
    iget-wide v4, v0, Lcom/android/calendar/dh;->m:J

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 5455
    div-int/lit8 v3, v1, 0x3c

    iput v3, v2, Landroid/text/format/Time;->hour:I

    .line 5456
    rem-int/lit8 v1, v1, 0x3c

    .line 5457
    int-to-float v1, v1

    div-float/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    .line 5458
    iput v1, v2, Landroid/text/format/Time;->minute:I

    .line 5459
    invoke-virtual {v2, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 5460
    iput-wide v2, p0, Lcom/android/calendar/e/g;->dd:J

    .line 5464
    iget-object v1, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-ne v1, v4, :cond_1

    .line 5465
    iget-object v1, p0, Lcom/android/calendar/e/g;->h:Lcom/android/calendar/e/al;

    invoke-virtual {v1, v0}, Lcom/android/calendar/e/al;->b(Lcom/android/calendar/dh;)I

    move-result v1

    .line 5466
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v3, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 5467
    iget-wide v4, v0, Lcom/android/calendar/dh;->n:J

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 5468
    div-int/lit8 v0, v1, 0x3c

    iput v0, v2, Landroid/text/format/Time;->hour:I

    .line 5469
    rem-int/lit8 v0, v1, 0x3c

    .line 5470
    int-to-float v0, v0

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    .line 5471
    iput v0, v2, Landroid/text/format/Time;->minute:I

    .line 5473
    invoke-virtual {v2, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/e/g;->de:J

    .line 5474
    iget-wide v0, p0, Lcom/android/calendar/e/g;->de:J

    iget-wide v2, p0, Lcom/android/calendar/e/g;->dd:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v10

    if-gez v0, :cond_0

    .line 5475
    iget-wide v0, p0, Lcom/android/calendar/e/g;->dd:J

    add-long/2addr v0, v10

    iput-wide v0, p0, Lcom/android/calendar/e/g;->de:J

    .line 5495
    :cond_0
    :goto_0
    return-void

    .line 5478
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v1, v4, :cond_2

    .line 5479
    iget-wide v0, v0, Lcom/android/calendar/dh;->n:J

    .line 5483
    :goto_1
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v5, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 5484
    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 5485
    invoke-virtual {v4, v4}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 5486
    invoke-virtual {v4, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/calendar/e/g;->de:J

    .line 5488
    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 5489
    const/16 v0, 0x18

    iput v0, v4, Landroid/text/format/Time;->hour:I

    .line 5490
    iput v9, v4, Landroid/text/format/Time;->minute:I

    .line 5491
    iput v9, v4, Landroid/text/format/Time;->second:I

    .line 5492
    invoke-virtual {v4, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/e/g;->de:J

    goto :goto_0

    .line 5481
    :cond_2
    iget-wide v4, v0, Lcom/android/calendar/dh;->n:J

    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    sub-long v0, v2, v0

    add-long/2addr v0, v4

    goto :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/e/g;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->e(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private c(Z)V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1917
    invoke-direct {p0}, Lcom/android/calendar/e/g;->t()V

    .line 1918
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dp:Z

    if-nez v0, :cond_1

    .line 1986
    :cond_0
    :goto_0
    return-void

    .line 1921
    :cond_1
    iget v0, p0, Lcom/android/calendar/e/g;->ao:I

    iget v1, p0, Lcom/android/calendar/e/g;->al:I

    if-eq v0, v1, :cond_9

    move v1, v2

    .line 1922
    :goto_1
    iget v0, p0, Lcom/android/calendar/e/g;->ap:I

    iget v4, p0, Lcom/android/calendar/e/g;->am:I

    if-eq v0, v4, :cond_a

    move v4, v2

    .line 1923
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v0, :cond_b

    move v0, v2

    .line 1924
    :goto_3
    if-nez v1, :cond_2

    if-nez v4, :cond_2

    if-eqz v0, :cond_0

    .line 1925
    :cond_2
    iget v0, p0, Lcom/android/calendar/e/g;->al:I

    iput v0, p0, Lcom/android/calendar/e/g;->ao:I

    .line 1926
    iget v0, p0, Lcom/android/calendar/e/g;->am:I

    iput v0, p0, Lcom/android/calendar/e/g;->ap:I

    .line 1927
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1930
    if-eqz v1, :cond_3

    .line 1931
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0f0139

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1932
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " %A"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1933
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeForAccessibility()Landroid/text/format/Time;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1935
    :cond_3
    if-eqz v4, :cond_4

    .line 1936
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeForAccessibility()Landroid/text/format/Time;

    move-result-object v5

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cd:Z

    if-eqz v0, :cond_c

    const-string v0, "%k"

    :goto_4
    invoke-virtual {v5, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1937
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0f003f

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1939
    :cond_4
    if-nez v1, :cond_5

    if-eqz v4, :cond_6

    .line 1940
    :cond_5
    const-string v0, ". "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1943
    :cond_6
    if-eqz p1, :cond_f

    .line 1944
    iget-object v0, p0, Lcom/android/calendar/e/g;->U:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 1945
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const v5, 0x7f0f042d

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->U:Ljava/lang/String;

    .line 1949
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1950
    if-lez v8, :cond_f

    .line 1951
    iget-object v0, p0, Lcom/android/calendar/e/g;->an:Lcom/android/calendar/dh;

    if-nez v0, :cond_d

    .line 1954
    iget-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v5, v2

    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 1955
    if-le v8, v2, :cond_8

    .line 1957
    sget-object v6, Lcom/android/calendar/e/g;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1958
    sget-object v10, Lcom/android/calendar/e/g;->g:Ljava/util/Formatter;

    iget-object v11, p0, Lcom/android/calendar/e/g;->U:Ljava/lang/String;

    new-array v12, v13, [Ljava/lang/Object;

    add-int/lit8 v6, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v12, v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v12, v2

    invoke-virtual {v10, v11, v12}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1959
    const-string v5, " "

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v5, v6

    .line 1961
    :cond_8
    invoke-direct {p0, v7, v0}, Lcom/android/calendar/e/g;->a(Ljava/lang/StringBuilder;Lcom/android/calendar/dh;)V

    goto :goto_5

    :cond_9
    move v1, v3

    .line 1921
    goto/16 :goto_1

    :cond_a
    move v4, v3

    .line 1922
    goto/16 :goto_2

    :cond_b
    move v0, v3

    .line 1923
    goto/16 :goto_3

    .line 1936
    :cond_c
    const-string v0, "%l%p"

    goto/16 :goto_4

    .line 1964
    :cond_d
    if-le v8, v2, :cond_e

    .line 1966
    sget-object v0, Lcom/android/calendar/e/g;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1967
    sget-object v0, Lcom/android/calendar/e/g;->g:Ljava/util/Formatter;

    iget-object v5, p0, Lcom/android/calendar/e/g;->U:Ljava/lang/String;

    new-array v6, v13, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/android/calendar/e/g;->an:Lcom/android/calendar/dh;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-virtual {v0, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1969
    const-string v0, " "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1971
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/e/g;->an:Lcom/android/calendar/dh;

    invoke-direct {p0, v7, v0}, Lcom/android/calendar/e/g;->a(Ljava/lang/StringBuilder;Lcom/android/calendar/dh;)V

    .line 1976
    :cond_f
    if-nez v1, :cond_10

    if-nez v4, :cond_10

    if-eqz p1, :cond_0

    .line 1977
    :cond_10
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/g;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1978
    const v0, 0x8000

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 1980
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1981
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1982
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 1983
    invoke-virtual {p0, v0}, Lcom/android/calendar/e/g;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    goto/16 :goto_0
.end method

.method private c(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2930
    .line 2931
    iget-object v2, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 3041
    :cond_0
    :goto_0
    return v0

    .line 2934
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    instance-of v2, v2, Lcom/android/calendar/AllInOneActivity;

    if-eqz v2, :cond_0

    .line 2938
    invoke-direct {p0}, Lcom/android/calendar/e/g;->E()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2939
    iget-object v2, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    invoke-virtual {v2}, Lcom/android/calendar/e/ao;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2940
    iput v0, p0, Lcom/android/calendar/e/g;->cP:I

    goto :goto_0

    .line 2943
    :cond_2
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dD:Z

    .line 2944
    iget-object v2, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    invoke-virtual {v2}, Lcom/android/calendar/e/ao;->b()V

    .line 2945
    new-instance v2, Lcom/android/calendar/e/t;

    invoke-direct {v2, p0}, Lcom/android/calendar/e/t;-><init>(Lcom/android/calendar/e/g;)V

    const-wide/16 v4, 0x64

    invoke-virtual {p0, v2, v4, v5}, Lcom/android/calendar/e/g;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2951
    iget v2, p0, Lcom/android/calendar/e/g;->ai:I

    iget v3, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v2, v3

    .line 2952
    iget v3, p0, Lcom/android/calendar/e/g;->aj:I

    sget v4, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v3, v4

    sget v4, Lcom/android/calendar/e/g;->aX:I

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->top:I

    .line 2953
    iget v3, p1, Landroid/graphics/Rect;->top:I

    sget v4, Lcom/android/calendar/e/g;->bE:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    sget v4, Lcom/android/calendar/e/g;->aY:I

    sub-int/2addr v3, v4

    sget v4, Lcom/android/calendar/e/g;->aX:I

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 2955
    iget-object v3, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-ne v3, v4, :cond_6

    .line 2956
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 2958
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iget v4, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    .line 2959
    sget v4, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v4, v4, 0x1

    .line 2961
    if-ge v3, v4, :cond_3

    .line 2962
    iget v3, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 2965
    :cond_3
    iget v3, p0, Lcom/android/calendar/e/g;->bA:I

    iget v4, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v3, v4

    sget v4, Lcom/android/calendar/e/g;->bR:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/e/g;->bQ:I

    sub-int/2addr v3, v4

    .line 2966
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    if-le v4, v3, :cond_4

    .line 2967
    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 2983
    :cond_4
    :goto_1
    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->b(I)I

    move-result v3

    sget v4, Lcom/android/calendar/e/g;->aZ:I

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->left:I

    .line 2984
    add-int/lit8 v3, v2, 0x1

    invoke-direct {p0, v3}, Lcom/android/calendar/e/g;->b(I)I

    move-result v3

    sget v4, Lcom/android/calendar/e/g;->ba:I

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->right:I

    .line 2985
    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v2, :cond_5

    .line 2986
    iget v2, p1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 2988
    :cond_5
    iget v2, p1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget v4, p1, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/android/calendar/e/g;->a(FFFF)V

    .line 2990
    iget-object v2, p0, Lcom/android/calendar/e/g;->cs:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2991
    iget-object v2, p0, Lcom/android/calendar/e/g;->cs:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2997
    sget v2, Lcom/android/calendar/e/g;->bu:I

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2998
    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    if-le v2, v1, :cond_8

    .line 2999
    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3000
    sget v0, Lcom/android/calendar/e/g;->aR:I

    int-to-float v0, v0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 3001
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 3002
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3004
    iget-object v0, p0, Lcom/android/calendar/e/g;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 3005
    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget v4, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 3006
    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 3007
    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 3008
    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v3

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 3009
    iget-object v0, p0, Lcom/android/calendar/e/g;->ct:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3010
    iget-object v0, p0, Lcom/android/calendar/e/g;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :goto_2
    move v0, v1

    .line 3035
    goto/16 :goto_0

    .line 2969
    :cond_6
    iget-object v3, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v4, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v3, v4, :cond_4

    .line 2970
    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->top:I

    .line 2972
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iget v4, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    .line 2973
    sget v4, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v4, v4, 0x1

    .line 2975
    if-ge v3, v4, :cond_7

    .line 2976
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->top:I

    .line 2978
    :cond_7
    iget v3, p1, Landroid/graphics/Rect;->top:I

    if-gez v3, :cond_4

    .line 2979
    iput v0, p1, Landroid/graphics/Rect;->top:I

    goto/16 :goto_1

    .line 3012
    :cond_8
    sget v2, Lcom/android/calendar/e/g;->aT:F

    .line 3013
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3014
    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 3015
    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 3016
    const-string v2, "sec-roboto-regular"

    invoke-static {v2, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 3017
    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3019
    iget v0, p0, Lcom/android/calendar/e/g;->du:I

    .line 3020
    iget-boolean v2, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v2, :cond_9

    .line 3021
    sget v2, Lcom/android/calendar/e/g;->be:I

    add-int/2addr v0, v2

    .line 3023
    :cond_9
    sget v2, Lcom/android/calendar/e/g;->bE:I

    iget-object v3, p0, Lcom/android/calendar/e/g;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/android/calendar/e/g;->dt:I

    .line 3024
    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/android/calendar/e/g;->ds:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 3025
    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 3026
    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 3027
    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v3

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 3028
    iget-object v0, p0, Lcom/android/calendar/e/g;->ct:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3029
    iget-object v0, p0, Lcom/android/calendar/e/g;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3031
    iget-object v0, p0, Lcom/android/calendar/e/g;->dr:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/e/g;->cu:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/android/calendar/e/g;->dv:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-virtual {p3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Paint$FontMetrics;->bottom:F

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {p2, v0, v2, v3, p3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 3037
    :cond_a
    invoke-direct {p0}, Lcom/android/calendar/e/g;->F()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3038
    iput v0, p0, Lcom/android/calendar/e/g;->cP:I

    goto/16 :goto_0
.end method

.method private c(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x96

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 1656
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->d(Landroid/content/Context;)Z

    move-result v0

    .line 1657
    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->e(Landroid/content/Context;)Z

    move-result v1

    .line 1658
    iget-object v2, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/dz;->l(Landroid/content/Context;)Z

    move-result v2

    .line 1659
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    .line 1661
    if-ne v4, v10, :cond_1

    .line 1662
    if-nez v0, :cond_2

    move v6, v5

    .line 1811
    :cond_0
    :goto_0
    return v6

    .line 1665
    :cond_1
    if-ne v4, v6, :cond_2

    .line 1666
    if-nez v2, :cond_2

    move v6, v5

    .line 1667
    goto :goto_0

    .line 1671
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 1672
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 1673
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 1675
    iget-object v7, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v8, 0x7f0d0003

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    .line 1676
    iget-object v8, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v9, 0x7f0c0215

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 1677
    add-int v9, v7, v8

    .line 1680
    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1689
    :pswitch_1
    if-eqz v1, :cond_d

    if-ne v4, v10, :cond_d

    .line 1691
    iget v1, p0, Lcom/android/calendar/e/g;->bD:I

    sub-int/2addr v1, v7

    if-le v0, v1, :cond_4

    .line 1692
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 1693
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1695
    :cond_3
    const/16 v0, 0xf

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 1696
    new-instance v0, Lcom/android/calendar/e/ae;

    invoke-direct {v0, p0, v6}, Lcom/android/calendar/e/ae;-><init>(Lcom/android/calendar/e/g;Z)V

    invoke-virtual {p0, v0, v12, v13}, Lcom/android/calendar/e/g;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1683
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1684
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1698
    :cond_4
    if-ge v0, v7, :cond_6

    .line 1699
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_5

    .line 1700
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1702
    :cond_5
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 1703
    new-instance v0, Lcom/android/calendar/e/ae;

    invoke-direct {v0, p0, v5}, Lcom/android/calendar/e/ae;-><init>(Lcom/android/calendar/e/g;Z)V

    invoke-virtual {p0, v0, v12, v13}, Lcom/android/calendar/e/g;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1705
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-nez v0, :cond_7

    sget-boolean v0, Lcom/android/calendar/ek;->a:Z

    if-nez v0, :cond_7

    if-le v3, v7, :cond_7

    iget v0, p0, Lcom/android/calendar/e/g;->bC:I

    sub-int/2addr v0, v7

    if-ge v3, v0, :cond_7

    .line 1707
    invoke-static {v6}, Lcom/android/calendar/ek;->a(I)V

    goto/16 :goto_0

    .line 1709
    :cond_7
    iget v0, p0, Lcom/android/calendar/e/g;->bC:I

    sub-int/2addr v0, v7

    if-le v3, v0, :cond_a

    .line 1710
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_8

    .line 1711
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1713
    :cond_8
    const/16 v0, 0xd

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 1714
    iget v0, p0, Lcom/android/calendar/e/g;->cV:I

    iget v1, p0, Lcom/android/calendar/e/g;->cY:I

    if-le v0, v1, :cond_9

    .line 1715
    iput v5, p0, Lcom/android/calendar/e/g;->cV:I

    .line 1716
    iput v5, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1717
    invoke-direct {p0, v6}, Lcom/android/calendar/e/g;->e(Z)Landroid/text/format/Time;

    move-result-object v0

    .line 1718
    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->c(Landroid/text/format/Time;)V

    goto/16 :goto_0

    .line 1720
    :cond_9
    iget v0, p0, Lcom/android/calendar/e/g;->cV:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/e/g;->cV:I

    goto/16 :goto_0

    .line 1723
    :cond_a
    if-ge v8, v3, :cond_d

    if-ge v3, v9, :cond_d

    .line 1724
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_b

    .line 1725
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1727
    :cond_b
    const/16 v0, 0x11

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 1728
    iget v0, p0, Lcom/android/calendar/e/g;->cV:I

    iget v1, p0, Lcom/android/calendar/e/g;->cY:I

    if-le v0, v1, :cond_c

    .line 1729
    iput v5, p0, Lcom/android/calendar/e/g;->cV:I

    .line 1730
    invoke-direct {p0, v5}, Lcom/android/calendar/e/g;->e(Z)Landroid/text/format/Time;

    move-result-object v0

    .line 1731
    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->c(Landroid/text/format/Time;)V

    goto/16 :goto_0

    .line 1733
    :cond_c
    iget v0, p0, Lcom/android/calendar/e/g;->cV:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/e/g;->cV:I

    goto/16 :goto_0

    .line 1739
    :cond_d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1, v6}, Lcom/android/calendar/e/g;->a(IIZ)Z

    .line 1740
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 1742
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    if-eqz v0, :cond_16

    .line 1743
    iput-boolean v6, p0, Lcom/android/calendar/e/g;->cW:Z

    .line 1744
    if-ne v4, v6, :cond_10

    move v0, v6

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/e/g;->dQ:Z

    .line 1745
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_e

    .line 1746
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dS:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1748
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    if-nez v0, :cond_f

    .line 1749
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget-wide v0, v0, Lcom/android/calendar/dh;->q:J

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dc:J

    .line 1750
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    invoke-virtual {v0}, Lcom/android/calendar/dh;->c()I

    move-result v0

    sget v1, Lcom/android/calendar/e/g;->a:I

    if-lt v0, v1, :cond_11

    .line 1751
    new-instance v0, Lcom/android/calendar/month/bb;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget-wide v8, v2, Lcom/android/calendar/dh;->m:J

    iget-object v2, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget-wide v10, v2, Lcom/android/calendar/dh;->n:J

    invoke-virtual {p0, v8, v9, v10, v11}, Lcom/android/calendar/e/g;->b(JJ)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {v0, v1, v2, v4}, Lcom/android/calendar/month/bb;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    .line 1755
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_f

    .line 1756
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1757
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    const-wide/16 v8, 0x190

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1760
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 1761
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1762
    invoke-virtual {p0, v1}, Lcom/android/calendar/e/g;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1764
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->E:F

    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_12

    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v0, v0

    .line 1765
    :goto_3
    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    neg-int v2, v2

    sget v4, Lcom/android/calendar/e/g;->bR:I

    add-int/2addr v2, v4

    iget v4, p0, Lcom/android/calendar/e/g;->bQ:I

    add-int/2addr v2, v4

    int-to-float v2, v2

    .line 1766
    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    add-float v4, v0, v2

    .line 1767
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v0, :cond_13

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    :goto_4
    iget v1, p0, Lcom/android/calendar/e/g;->bW:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v1, v1

    sub-float v1, v0, v1

    .line 1769
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    instance-of v0, v0, Lcom/android/calendar/e/ai;

    if-eqz v0, :cond_14

    .line 1770
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    check-cast v0, Lcom/android/calendar/e/ai;

    float-to-int v1, v1

    float-to-int v2, v4

    sget v4, Lcom/android/calendar/e/g;->as:I

    iget v5, p0, Lcom/android/calendar/e/g;->i:I

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/e/ai;->a(IIIII)V

    goto/16 :goto_0

    :cond_10
    move v0, v5

    .line 1744
    goto/16 :goto_1

    .line 1753
    :cond_11
    new-instance v0, Lcom/android/calendar/e/ai;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget-boolean v4, p0, Lcom/android/calendar/e/g;->dQ:Z

    invoke-direct {v0, v1, v2, v4}, Lcom/android/calendar/e/ai;-><init>(Landroid/content/Context;Lcom/android/calendar/dh;Z)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    goto :goto_2

    .line 1764
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->E:F

    goto :goto_3

    .line 1767
    :cond_13
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->C:F

    iget-object v1, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v1, v1, Lcom/android/calendar/dh;->D:F

    iget-object v2, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_4

    .line 1771
    :cond_14
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    instance-of v0, v0, Lcom/android/calendar/month/bb;

    if-eqz v0, :cond_0

    .line 1772
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    if-ne v0, v6, :cond_15

    .line 1773
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->C:F

    iget-object v1, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v1, v1, Lcom/android/calendar/dh;->D:F

    iget-object v2, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v1, v2

    sub-float/2addr v0, v1

    move v2, v0

    .line 1777
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    check-cast v0, Lcom/android/calendar/month/bb;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    float-to-int v2, v2

    float-to-int v3, v4

    iget v4, p0, Lcom/android/calendar/e/g;->i:I

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;IIIZ)V

    goto/16 :goto_0

    .line 1775
    :cond_15
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->D:F

    sget v1, Lcom/android/calendar/e/g;->as:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    move v2, v0

    goto :goto_5

    .line 1780
    :cond_16
    iget-object v0, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    if-nez v0, :cond_0

    .line 1781
    iput-boolean v5, p0, Lcom/android/calendar/e/g;->cW:Z

    .line 1782
    iput-object v11, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    .line 1783
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1784
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1785
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dS:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1791
    :pswitch_3
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    if-nez v0, :cond_17

    sget-boolean v0, Lcom/android/calendar/ek;->a:Z

    if-nez v0, :cond_17

    .line 1792
    invoke-static {v6}, Lcom/android/calendar/ek;->a(I)V

    .line 1794
    :cond_17
    iput-boolean v5, p0, Lcom/android/calendar/e/g;->cW:Z

    .line 1795
    iput-object v11, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    .line 1796
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1797
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1798
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 1802
    :goto_6
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1803
    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    instance-of v0, v0, Lcom/android/calendar/e/ai;

    if-eqz v0, :cond_0

    .line 1804
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dS:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1800
    :cond_18
    invoke-static {v6}, Lcom/android/calendar/ek;->a(I)V

    goto :goto_6

    .line 1680
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic c(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->dD:Z

    return p1
.end method

.method private d(I)I
    .locals 6

    .prologue
    .line 6362
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v0, :cond_0

    .line 6363
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 6364
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 6365
    const/4 v1, 0x1

    aget v2, v0, v1

    float-to-double v2, v2

    const-wide v4, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v2, v4

    double-to-float v2, v2

    aput v2, v0, v1

    .line 6366
    const/4 v1, 0x2

    aget v2, v0, v1

    float-to-double v2, v2

    const-wide v4, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    aput v2, v0, v1

    .line 6367
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result p1

    .line 6369
    :cond_0
    return p1
.end method

.method private d(Lcom/android/calendar/dh;)I
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 6332
    iget v0, p1, Lcom/android/calendar/dh;->D:F

    iget v1, p1, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v0, v1

    .line 6333
    iget v1, p1, Lcom/android/calendar/dh;->C:F

    div-float v2, v0, v3

    add-float/2addr v1, v2

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(F)I

    move-result v1

    .line 6334
    iget v2, p0, Lcom/android/calendar/e/g;->dV:I

    int-to-float v2, v2

    div-float/2addr v0, v3

    add-float/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(F)I

    move-result v0

    .line 6336
    sub-int v0, v1, v0

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/e/g;I)I
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/android/calendar/e/g;->by:I

    return p1
.end method

.method private d(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 3134
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/e/g;->a(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 3135
    return-void
.end method

.method private d(Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    .line 4087
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 4088
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 4089
    iget v2, p0, Lcom/android/calendar/e/g;->ai:I

    .line 4090
    iget v3, p0, Lcom/android/calendar/e/g;->aj:I

    .line 4091
    const/4 v4, 0x0

    invoke-direct {p0, v0, v1, v4}, Lcom/android/calendar/e/g;->a(IIZ)Z

    move-result v0

    .line 4092
    iget-object v4, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 4093
    iget-object v5, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v6, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    if-ne v5, v6, :cond_1

    .line 4094
    iget-object v5, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v6, v5, Lcom/android/calendar/dh;->q:J

    iget-object v5, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v8, v5, Lcom/android/calendar/dh;->q:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 4096
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/g;->O()V

    .line 4097
    iput-object v4, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 4100
    :cond_1
    iget-object v4, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v5, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v4, :cond_2

    .line 4101
    sget-object v4, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    iput-object v4, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 4102
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 4103
    const/4 v4, 0x0

    sput-boolean v4, Lcom/android/calendar/e/g;->p:Z

    .line 4105
    :cond_2
    if-nez v0, :cond_4

    .line 4106
    sget v0, Lcom/android/calendar/e/g;->bR:I

    if-ge v1, v0, :cond_3

    .line 4107
    new-instance v6, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v6, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 4108
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    invoke-static {v6, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 4109
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    iput v0, v6, Landroid/text/format/Time;->hour:I

    .line 4110
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 4111
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x2

    const-wide/16 v10, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 4192
    :cond_3
    :goto_0
    return-void

    .line 4117
    :cond_4
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/e/g;->cP:I

    .line 4119
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v0, :cond_7

    .line 4120
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 4122
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/calendar/e/g;->playSoundEffect(I)V

    .line 4124
    sget v1, Lcom/android/calendar/e/g;->a:I

    if-ltz v1, :cond_3

    .line 4127
    invoke-virtual {v0}, Lcom/android/calendar/dh;->c()I

    move-result v1

    sget v2, Lcom/android/calendar/e/g;->a:I

    if-lt v1, v2, :cond_6

    .line 4128
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 4129
    iget-wide v2, v0, Lcom/android/calendar/dh;->m:J

    iget-wide v0, v0, Lcom/android/calendar/dh;->n:J

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/android/calendar/e/g;->a(JJ)V

    .line 4191
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    goto :goto_0

    .line 4131
    :cond_6
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 4132
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4133
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 4135
    :cond_7
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    if-ne v2, v0, :cond_c

    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    if-ne v3, v0, :cond_c

    .line 4138
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v0

    .line 4139
    const-wide/32 v2, 0x36ee80

    add-long/2addr v2, v0

    .line 4141
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 4142
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 4143
    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 4144
    invoke-virtual {v5, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 4145
    invoke-static {v4}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 4146
    if-nez v0, :cond_9

    iget-boolean v1, p0, Lcom/android/calendar/e/g;->W:Z

    if-eqz v1, :cond_9

    .line 4147
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4148
    invoke-direct {p0, v4, v5}, Lcom/android/calendar/e/g;->a(Landroid/text/format/Time;Landroid/text/format/Time;)V

    goto :goto_1

    .line 4150
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/32 v2, 0x40000000

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_1

    .line 4152
    :cond_9
    if-nez v0, :cond_a

    iget-boolean v1, p0, Lcom/android/calendar/e/g;->W:Z

    if-eqz v1, :cond_5

    .line 4153
    :cond_a
    iget-object v1, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->i()V

    .line 4154
    const/4 v1, -0x1

    if-ne v0, v1, :cond_b

    .line 4155
    const v0, 0x24dc87

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    .line 4159
    :goto_2
    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    .line 4160
    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 4161
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_1

    .line 4157
    :cond_b
    const v0, 0x259d23

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    goto :goto_2

    .line 4165
    :cond_c
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 4166
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 4167
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    iput v0, v4, Landroid/text/format/Time;->hour:I

    .line 4168
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 4170
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 4171
    iget v0, v5, Landroid/text/format/Time;->hour:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v5, Landroid/text/format/Time;->hour:I

    .line 4172
    invoke-static {v4}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 4173
    if-nez v0, :cond_d

    .line 4174
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_1

    .line 4179
    :cond_d
    const/4 v1, -0x1

    if-ne v0, v1, :cond_e

    .line 4180
    const v0, 0x24dc87

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    .line 4184
    :goto_3
    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    .line 4185
    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 4186
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    .line 4187
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_1

    .line 4182
    :cond_e
    const v0, 0x259d23

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    goto :goto_3
.end method

.method static synthetic d(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/calendar/e/g;->v()V

    return-void
.end method

.method private d(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2173
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    iget v1, p0, Lcom/android/calendar/e/g;->bY:I

    add-int/lit8 v1, v1, 0x1

    if-lt v0, v1, :cond_0

    if-eqz p1, :cond_2

    .line 2174
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 2175
    invoke-direct {p0, v5}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 2176
    iget-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2177
    iput-boolean v4, p0, Lcom/android/calendar/e/g;->cl:Z

    .line 2178
    iput-boolean v3, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 2186
    :cond_1
    :goto_0
    return-void

    .line 2179
    :cond_2
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    iget v1, p0, Lcom/android/calendar/e/g;->bY:I

    iget v2, p0, Lcom/android/calendar/e/g;->bV:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x3

    if-le v0, v1, :cond_1

    .line 2180
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 2181
    invoke-direct {p0, v5}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 2182
    iget-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2183
    iput-boolean v4, p0, Lcom/android/calendar/e/g;->cl:Z

    .line 2184
    iput-boolean v3, p0, Lcom/android/calendar/e/g;->cq:Z

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->cq:Z

    return p1
.end method

.method static synthetic e(Lcom/android/calendar/e/g;I)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/e/g;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    return-object v0
.end method

.method private e(Z)Landroid/text/format/Time;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 6393
    if-eqz p1, :cond_0

    move v0, v1

    .line 6394
    :goto_0
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 6395
    iget-object v3, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 6397
    iget-object v3, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->g()I

    move-result v3

    .line 6398
    packed-switch v3, :pswitch_data_0

    .line 6408
    :goto_1
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 6409
    return-object v2

    .line 6393
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 6400
    :pswitch_0
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    mul-int/lit8 v0, v0, 0x7

    add-int/2addr v0, v3

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    goto :goto_1

    .line 6403
    :pswitch_1
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v0, v3

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    goto :goto_1

    .line 6398
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private e(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 3143
    iget v0, p0, Lcom/android/calendar/e/g;->P:I

    iget v1, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v0, v1

    .line 3145
    iget-object v1, p0, Lcom/android/calendar/e/g;->au:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 3146
    iget-object v1, p0, Lcom/android/calendar/e/g;->au:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 3147
    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 3148
    iget v1, p0, Lcom/android/calendar/e/g;->bW:I

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 3149
    sget v1, Lcom/android/calendar/e/g;->bt:I

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 3150
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3151
    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3152
    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 3155
    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    if-ne v1, v4, :cond_1

    if-nez v0, :cond_1

    .line 3157
    iget-object v0, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->minute:I

    sget v2, Lcom/android/calendar/e/g;->bE:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x3c

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 3159
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    iget v2, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_0

    .line 3160
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 3161
    iget v1, p0, Lcom/android/calendar/e/g;->bW:I

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 3162
    iget v1, p0, Lcom/android/calendar/e/g;->bC:I

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 3163
    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 3164
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iget v1, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 3165
    sget v0, Lcom/android/calendar/e/g;->bs:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 3166
    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 3200
    :cond_0
    :goto_0
    invoke-virtual {p3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3201
    return-void

    .line 3168
    :cond_1
    if-ltz v0, :cond_3

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    if-ge v0, v1, :cond_3

    .line 3170
    iget-object v1, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->hour:I

    sget v2, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->minute:I

    sget v3, Lcom/android/calendar/e/g;->bE:I

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x3c

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 3172
    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    iget v3, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_2

    .line 3173
    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 3174
    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 3175
    add-int/lit8 v2, v0, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->b(I)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->right:I

    .line 3176
    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 3177
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    iget v2, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v1, v2

    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 3178
    sget v1, Lcom/android/calendar/e/g;->bs:I

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 3179
    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 3183
    :cond_2
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    if-ge v1, v2, :cond_0

    .line 3184
    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 3185
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 3186
    iget-object v0, p0, Lcom/android/calendar/e/g;->au:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 3187
    iget-object v0, p0, Lcom/android/calendar/e/g;->au:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 3188
    sget v0, Lcom/android/calendar/e/g;->bs:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 3189
    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 3191
    :cond_3
    if-gez v0, :cond_0

    .line 3193
    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 3194
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(I)I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 3195
    iget-object v0, p0, Lcom/android/calendar/e/g;->au:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 3196
    iget-object v0, p0, Lcom/android/calendar/e/g;->au:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 3197
    sget v0, Lcom/android/calendar/e/g;->bs:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 3198
    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method private e(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4256
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cQ:Z

    if-eqz v0, :cond_1

    .line 4277
    :cond_0
    :goto_0
    return-void

    .line 4261
    :cond_1
    iget v0, p0, Lcom/android/calendar/e/g;->bJ:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/calendar/e/g;->bI:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 4265
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 4266
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 4268
    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/e/g;->a(IIZ)Z

    move-result v0

    .line 4269
    if-eqz v0, :cond_0

    .line 4274
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/e/g;->cP:I

    .line 4275
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 4276
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->f(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->ec:Z

    return p1
.end method

.method static synthetic f(Lcom/android/calendar/e/g;I)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->ea:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/e/g;->ea:I

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/e/g;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->ej:Ljava/lang/Runnable;

    return-object v0
.end method

.method private f(Landroid/view/MotionEvent;)V
    .locals 13

    .prologue
    const-wide/32 v8, 0x36ee80

    const/4 v11, 0x0

    .line 4284
    invoke-direct {p0}, Lcom/android/calendar/e/g;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4285
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    invoke-static {v0}, Lcom/android/calendar/dh;->a(Lcom/android/calendar/dh;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_1

    .line 4287
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/g;->I()V

    .line 4288
    sget-object v0, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 4289
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->dT:I

    .line 4290
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lcom/android/calendar/e/g;->ea:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->dU:I

    .line 4291
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedEvent()Lcom/android/calendar/dh;

    move-result-object v0

    .line 4292
    iput-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 4293
    iget v1, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dV:I

    .line 4294
    iget v1, v0, Lcom/android/calendar/dh;->E:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dW:I

    .line 4295
    iget v1, v0, Lcom/android/calendar/dh;->F:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dX:I

    .line 4296
    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dB:J

    .line 4339
    :cond_1
    :goto_0
    return-void

    .line 4299
    :cond_2
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 4300
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTime()Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 4301
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v2

    .line 4302
    add-long v6, v2, v8

    .line 4304
    new-instance v4, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 4305
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 4306
    invoke-virtual {v4, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 4307
    invoke-virtual {v5, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 4308
    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 4309
    if-nez v0, :cond_3

    .line 4310
    invoke-direct {p0}, Lcom/android/calendar/e/g;->I()V

    .line 4311
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedEvent()Lcom/android/calendar/dh;

    move-result-object v0

    .line 4312
    invoke-direct {p0}, Lcom/android/calendar/e/g;->getCurrentSelectionPosition()Landroid/graphics/Rect;

    move-result-object v1

    .line 4313
    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iput v2, v0, Lcom/android/calendar/dh;->C:F

    .line 4314
    iget v2, v1, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iput v2, v0, Lcom/android/calendar/dh;->D:F

    .line 4315
    iget v2, v1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iput v2, v0, Lcom/android/calendar/dh;->E:F

    .line 4316
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iput v1, v0, Lcom/android/calendar/dh;->F:F

    .line 4318
    iput-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 4319
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dT:I

    .line 4320
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dU:I

    .line 4321
    iget v1, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dV:I

    .line 4322
    iget v1, v0, Lcom/android/calendar/dh;->E:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dW:I

    .line 4323
    iget v1, v0, Lcom/android/calendar/dh;->F:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dX:I

    .line 4324
    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dB:J

    .line 4325
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->dy:Z

    goto :goto_0

    .line 4327
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->i()V

    .line 4328
    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    .line 4329
    const v0, 0x24dc87

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    .line 4333
    :goto_1
    add-long/2addr v0, v8

    .line 4334
    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 4335
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x1

    move-object v1, p0

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_0

    .line 4331
    :cond_4
    const v0, 0x259d23

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    goto :goto_1
.end method

.method static synthetic f(Lcom/android/calendar/e/g;Z)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->d(Z)V

    return-void
.end method

.method private f(Z)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const-wide/32 v6, 0x5265c00

    .line 6438
    sget v0, Lcom/android/calendar/e/g;->as:I

    iget v1, p0, Lcom/android/calendar/e/g;->ar:I

    if-eq v0, v1, :cond_1

    .line 6469
    :cond_0
    :goto_0
    return-void

    .line 6442
    :cond_1
    if-eqz p1, :cond_5

    .line 6443
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    .line 6447
    :goto_1
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->I:Z

    if-eqz v1, :cond_2

    .line 6448
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iput-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 6450
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v1, :cond_0

    .line 6453
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v2, v1, Lcom/android/calendar/dh;->i:I

    add-int/2addr v2, v0

    iput v2, v1, Lcom/android/calendar/dh;->i:I

    .line 6454
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v2, v1, Lcom/android/calendar/dh;->m:J

    int-to-long v4, v0

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/android/calendar/dh;->m:J

    .line 6455
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v2, v1, Lcom/android/calendar/dh;->j:I

    add-int/2addr v2, v0

    iput v2, v1, Lcom/android/calendar/dh;->j:I

    .line 6456
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v2, v1, Lcom/android/calendar/dh;->n:J

    int-to-long v4, v0

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/android/calendar/dh;->n:J

    .line 6457
    sput-boolean v8, Lcom/android/calendar/e/g;->p:Z

    .line 6458
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 6459
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v2, v1, Lcom/android/calendar/dh;->i:I

    add-int/2addr v2, v0

    iput v2, v1, Lcom/android/calendar/dh;->i:I

    .line 6460
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v2, v1, Lcom/android/calendar/dh;->m:J

    int-to-long v4, v0

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/android/calendar/dh;->m:J

    .line 6461
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v2, v1, Lcom/android/calendar/dh;->j:I

    add-int/2addr v2, v0

    iput v2, v1, Lcom/android/calendar/dh;->j:I

    .line 6462
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v2, v1, Lcom/android/calendar/dh;->n:J

    int-to-long v4, v0

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/android/calendar/dh;->n:J

    .line 6464
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/a;

    if-eqz v0, :cond_4

    .line 6465
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    check-cast v0, Lcom/android/calendar/a;

    invoke-virtual {v0}, Lcom/android/calendar/a;->a()V

    .line 6467
    :cond_4
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->dC:Z

    .line 6468
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    iget v1, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/calendar/e/g;->bC:I

    int-to-float v2, v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/calendar/e/g;->a(ZIFF)Landroid/view/View;

    goto :goto_0

    .line 6445
    :cond_5
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    mul-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method static synthetic g(Lcom/android/calendar/e/g;I)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->dZ:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/e/g;->dZ:I

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/e/g;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->ek:Ljava/lang/Runnable;

    return-object v0
.end method

.method private g(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 4490
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 4491
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-object v3, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eq v2, v3, :cond_1

    .line 4500
    :cond_0
    :goto_0
    return v0

    .line 4494
    :cond_1
    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    add-float/2addr v2, v3

    sget v3, Lcom/android/calendar/e/g;->bR:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/g;->bQ:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 4496
    iget v1, v1, Lcom/android/calendar/dh;->E:F

    .line 4497
    iget v3, p0, Lcom/android/calendar/e/g;->bg:I

    int-to-float v3, v3

    sub-float v3, v1, v3

    cmpg-float v3, v3, v2

    if-gtz v3, :cond_0

    iget v3, p0, Lcom/android/calendar/e/g;->bg:I

    int-to-float v3, v3

    add-float/2addr v3, v1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 4498
    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->aK:Z

    return p1
.end method

.method private getCurrentSelectionPosition()Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 3240
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 3241
    iget v1, p0, Lcom/android/calendar/e/g;->aj:I

    sget v2, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 3242
    iget v1, v0, Landroid/graphics/Rect;->top:I

    sget v2, Lcom/android/calendar/e/g;->bE:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 3243
    iget v1, p0, Lcom/android/calendar/e/g;->ai:I

    iget v2, p0, Lcom/android/calendar/e/g;->R:I

    sub-int/2addr v1, v2

    .line 3244
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 3245
    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->b(I)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 3246
    return-object v0
.end method

.method private getSelectedMinutesSinceMidnight()I
    .locals 1

    .prologue
    .line 977
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    mul-int/lit8 v0, v0, 0x3c

    return v0
.end method

.method private getTimeFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2020
    const-string v0, "%H:%M"

    .line 2022
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%-l:%M %^p"

    goto :goto_0
.end method

.method static synthetic h(Lcom/android/calendar/e/g;I)I
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/android/calendar/e/g;->P:I

    return p1
.end method

.method static synthetic h(Lcom/android/calendar/e/g;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    return-object v0
.end method

.method private h(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 4511
    iget-object v1, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 4513
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-object v3, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eq v2, v3, :cond_1

    .line 4524
    :cond_0
    :goto_0
    return v0

    .line 4516
    :cond_1
    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    add-float/2addr v2, v3

    sget v3, Lcom/android/calendar/e/g;->bR:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/g;->bQ:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 4518
    iget v1, v1, Lcom/android/calendar/dh;->F:F

    .line 4520
    iget v3, p0, Lcom/android/calendar/e/g;->bg:I

    int-to-float v3, v3

    sub-float v3, v1, v3

    cmpg-float v3, v3, v2

    if-gtz v3, :cond_0

    iget v3, p0, Lcom/android/calendar/e/g;->bg:I

    int-to-float v3, v3

    add-float/2addr v3, v1

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 4521
    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    iget v3, p0, Lcom/android/calendar/e/g;->bB:I

    add-int/2addr v2, v3

    sget v3, Lcom/android/calendar/e/g;->bR:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/g;->bQ:I

    sub-int/2addr v2, v3

    .line 4522
    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic h(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->cI:Z

    return p1
.end method

.method static synthetic i(Lcom/android/calendar/e/g;)Lcom/android/calendar/e/d;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->dg:Lcom/android/calendar/e/d;

    return-object v0
.end method

.method private i(Landroid/view/MotionEvent;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x14

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 4865
    invoke-direct {p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/e/g;->N()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4903
    :goto_0
    return-void

    .line 4871
    :cond_0
    if-eqz p1, :cond_3

    .line 4872
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 4873
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 4874
    iput v1, p0, Lcom/android/calendar/e/g;->ee:I

    .line 4875
    iput v0, p0, Lcom/android/calendar/e/g;->ef:I

    .line 4884
    :goto_1
    sget v4, Lcom/android/calendar/e/g;->bE:I

    div-int/lit8 v4, v4, 0x2

    if-ge v0, v4, :cond_4

    iget v4, p0, Lcom/android/calendar/e/g;->by:I

    if-le v4, v2, :cond_4

    .line 4886
    iget-boolean v4, p0, Lcom/android/calendar/e/g;->cI:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/android/calendar/e/g;->H:Z

    if-nez v4, :cond_1

    .line 4887
    new-instance v4, Lcom/android/calendar/e/ae;

    invoke-direct {v4, p0, v3}, Lcom/android/calendar/e/ae;-><init>(Lcom/android/calendar/e/g;Z)V

    invoke-virtual {p0, v4, v6, v7}, Lcom/android/calendar/e/g;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4888
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->cI:Z

    .line 4898
    :cond_1
    :goto_2
    iget v3, p0, Lcom/android/calendar/e/g;->dT:I

    sub-int/2addr v1, v3

    iput v1, p0, Lcom/android/calendar/e/g;->dY:I

    .line 4899
    if-nez v2, :cond_2

    .line 4900
    iget v1, p0, Lcom/android/calendar/e/g;->dU:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->ea:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->dZ:I

    .line 4902
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    goto :goto_0

    .line 4877
    :cond_3
    iget v1, p0, Lcom/android/calendar/e/g;->ee:I

    .line 4878
    iget v0, p0, Lcom/android/calendar/e/g;->ef:I

    goto :goto_1

    .line 4890
    :cond_4
    iget v4, p0, Lcom/android/calendar/e/g;->bB:I

    sget v5, Lcom/android/calendar/e/g;->bE:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    if-le v0, v4, :cond_5

    iget v4, p0, Lcom/android/calendar/e/g;->by:I

    iget v5, p0, Lcom/android/calendar/e/g;->bA:I

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_5

    .line 4892
    iget-boolean v3, p0, Lcom/android/calendar/e/g;->cI:Z

    if-nez v3, :cond_1

    .line 4893
    new-instance v3, Lcom/android/calendar/e/ae;

    invoke-direct {v3, p0, v2}, Lcom/android/calendar/e/ae;-><init>(Lcom/android/calendar/e/g;Z)V

    invoke-virtual {p0, v3, v6, v7}, Lcom/android/calendar/e/g;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4894
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->cI:Z

    goto :goto_2

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method static synthetic i(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->G:Z

    return p1
.end method

.method static synthetic j(Lcom/android/calendar/e/g;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->ci:I

    return v0
.end method

.method static synthetic j(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->cQ:Z

    return p1
.end method

.method static synthetic k(Lcom/android/calendar/e/g;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->cj:I

    return v0
.end method

.method static synthetic k(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->di:Z

    return p1
.end method

.method static synthetic l(Lcom/android/calendar/e/g;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    return v0
.end method

.method static synthetic l(Lcom/android/calendar/e/g;Z)Z
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->dF:Z

    return p1
.end method

.method static synthetic m(Lcom/android/calendar/e/g;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->dK:I

    return v0
.end method

.method static synthetic m(Lcom/android/calendar/e/g;Z)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->f(Z)V

    return-void
.end method

.method static synthetic n(Lcom/android/calendar/e/g;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->bA:I

    return v0
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o()I
    .locals 1

    .prologue
    .line 129
    sget v0, Lcom/android/calendar/e/g;->cE:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/calendar/e/g;->cE:I

    return v0
.end method

.method static synthetic o(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/calendar/e/g;->z()V

    return-void
.end method

.method static synthetic p()I
    .locals 1

    .prologue
    .line 129
    sget v0, Lcom/android/calendar/e/g;->cE:I

    return v0
.end method

.method static synthetic p(Lcom/android/calendar/e/g;)Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->ec:Z

    return v0
.end method

.method static synthetic q()I
    .locals 1

    .prologue
    .line 129
    sget v0, Lcom/android/calendar/e/g;->bE:I

    return v0
.end method

.method static synthetic q(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/android/calendar/e/g;->O()V

    return-void
.end method

.method static synthetic r(Lcom/android/calendar/e/g;)Lcom/android/calendar/e/ao;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    return-object v0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 229
    new-instance v1, Lcom/android/calendar/e/o;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/o;-><init>(Lcom/android/calendar/e/g;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 236
    :cond_0
    return-void
.end method

.method private s()V
    .locals 8

    .prologue
    .line 915
    iget-object v0, p0, Lcom/android/calendar/e/g;->j:Landroid/content/res/Resources;

    const v1, 0x7f0c0205

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    sput v0, Lcom/android/calendar/e/g;->aU:F

    .line 916
    iget-object v0, p0, Lcom/android/calendar/e/g;->ca:[Ljava/lang/String;

    const/16 v1, 0xc

    aget-object v0, v0, v1

    .line 917
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->cd:Z

    if-nez v1, :cond_0

    .line 918
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/g;->cB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 920
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/e/g;->aw:Landroid/graphics/Paint;

    .line 921
    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v3

    .line 922
    sget v1, Lcom/android/calendar/e/g;->aU:F

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 923
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 924
    :goto_0
    iget v4, p0, Lcom/android/calendar/e/g;->bW:I

    iget v5, p0, Lcom/android/calendar/e/g;->bW:I

    sget v6, Lcom/android/calendar/e/g;->aQ:I

    sub-int/2addr v5, v6

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v1, v1, v4

    if-lez v1, :cond_1

    .line 925
    sget v1, Lcom/android/calendar/e/g;->aU:F

    float-to-double v4, v1

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v4, v6

    double-to-float v1, v4

    sput v1, Lcom/android/calendar/e/g;->aU:F

    .line 926
    sget v1, Lcom/android/calendar/e/g;->aU:F

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 927
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    goto :goto_0

    .line 929
    :cond_1
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 930
    return-void
.end method

.method static synthetic s(Lcom/android/calendar/e/g;)Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cQ:Z

    return v0
.end method

.method private setSelectedDay(I)V
    .locals 0

    .prologue
    .line 6235
    iput p1, p0, Lcom/android/calendar/e/g;->ai:I

    .line 6236
    iput p1, p0, Lcom/android/calendar/e/g;->al:I

    .line 6237
    return-void
.end method

.method private setSelectedEvent(Lcom/android/calendar/dh;)V
    .locals 0

    .prologue
    .line 6226
    iput-object p1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 6227
    iput-object p1, p0, Lcom/android/calendar/e/g;->an:Lcom/android/calendar/dh;

    .line 6228
    return-void
.end method

.method private setSelectedHour(I)V
    .locals 0

    .prologue
    .line 6231
    iput p1, p0, Lcom/android/calendar/e/g;->aj:I

    .line 6232
    iput p1, p0, Lcom/android/calendar/e/g;->am:I

    .line 6233
    return-void
.end method

.method private setupHourTextPaint(Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 3125
    sget v0, Lcom/android/calendar/e/g;->bv:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 3126
    sget v0, Lcom/android/calendar/e/g;->aU:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 3127
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 3128
    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 3129
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3130
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3131
    return-void
.end method

.method private setupTextRect(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 3250
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-le v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-gt v0, v1, :cond_2

    .line 3251
    :cond_0
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 3252
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 3264
    :cond_1
    :goto_0
    return-void

    .line 3256
    :cond_2
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->bc:I

    sget v2, Lcom/android/calendar/e/g;->bd:I

    add-int/2addr v1, v2

    if-le v0, v1, :cond_3

    .line 3257
    iget v0, p1, Landroid/graphics/Rect;->top:I

    sget v1, Lcom/android/calendar/e/g;->bc:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 3258
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sget v1, Lcom/android/calendar/e/g;->bd:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 3260
    :cond_3
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->be:I

    sget v2, Lcom/android/calendar/e/g;->bf:I

    add-int/2addr v1, v2

    if-le v0, v1, :cond_1

    .line 3261
    iget v0, p1, Landroid/graphics/Rect;->left:I

    sget v1, Lcom/android/calendar/e/g;->be:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 3262
    iget v0, p1, Landroid/graphics/Rect;->right:I

    sget v1, Lcom/android/calendar/e/g;->bf:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method

.method static synthetic t(Lcom/android/calendar/e/g;)Landroid/widget/OverScroller;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->dj:Landroid/widget/OverScroller;

    return-object v0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 933
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/e/g;->do:Landroid/view/accessibility/AccessibilityManager;

    .line 935
    iget-object v0, p0, Lcom/android/calendar/e/g;->do:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->do:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/e/g;->dp:Z

    .line 936
    invoke-direct {p0}, Lcom/android/calendar/e/g;->x()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->dq:Z

    .line 937
    return-void

    .line 935
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 4

    .prologue
    .line 1118
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 1119
    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(Landroid/text/format/Time;)V

    .line 1122
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->d(Landroid/text/format/Time;)V

    .line 1123
    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 1124
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->R:I

    .line 1125
    iget v0, p0, Lcom/android/calendar/e/g;->R:I

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/e/g;->S:I

    .line 1126
    return-void
.end method

.method static synthetic u(Lcom/android/calendar/e/g;)Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->di:Z

    return v0
.end method

.method static synthetic v(Lcom/android/calendar/e/g;)F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->dl:F

    return v0
.end method

.method private v()V
    .locals 4

    .prologue
    .line 1867
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1868
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1869
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1870
    sget-object v2, Lcom/android/calendar/e/ai;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 1871
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1873
    :try_start_0
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1879
    :cond_0
    :goto_0
    sget-object v2, Lcom/android/calendar/month/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1880
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1882
    :try_start_1
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1888
    :cond_1
    :goto_1
    :try_start_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1893
    :goto_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/g;->da:Landroid/app/DialogFragment;

    .line 1894
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dc:J

    .line 1895
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->cW:Z

    .line 1896
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 1897
    return-void

    .line 1874
    :catch_0
    move-exception v2

    .line 1875
    sget-object v2, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v3, "Fail to dismiss DialogFragment"

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1883
    :catch_1
    move-exception v0

    .line 1884
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v2, "Fail to remove MonthHoverEventFragment"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1889
    :catch_2
    move-exception v0

    .line 1890
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method static synthetic w(Lcom/android/calendar/e/g;)Landroid/widget/EdgeEffect;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->dm:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method private w()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1900
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    if-eqz v0, :cond_1

    .line 1901
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cW:Z

    .line 1902
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cX:Z

    .line 1903
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v0}, Lcom/android/calendar/e/am;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1904
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v0}, Lcom/android/calendar/e/am;->d()V

    .line 1906
    :cond_0
    iput-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    .line 1907
    iput-object v2, p0, Lcom/android/calendar/e/g;->db:Lcom/android/calendar/dh;

    .line 1908
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dc:J

    .line 1910
    :cond_1
    return-void
.end method

.method static synthetic x(Lcom/android/calendar/e/g;)Landroid/widget/EdgeEffect;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->dn:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method private x()Z
    .locals 1

    .prologue
    .line 1913
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->do:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic y(Lcom/android/calendar/e/g;)I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/android/calendar/e/g;->bH:I

    return v0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 2189
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    iget v1, p0, Lcom/android/calendar/e/g;->bV:I

    div-int/lit8 v1, v1, 0x5

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->bY:I

    .line 2190
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    if-gez v0, :cond_1

    .line 2191
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/e/g;->bY:I

    .line 2195
    :cond_0
    :goto_0
    return-void

    .line 2192
    :cond_1
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    iget v1, p0, Lcom/android/calendar/e/g;->bV:I

    add-int/2addr v0, v1

    const/16 v1, 0x18

    if-le v0, v1, :cond_0

    .line 2193
    iget v0, p0, Lcom/android/calendar/e/g;->bV:I

    rsub-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/android/calendar/e/g;->bY:I

    goto :goto_0
.end method

.method static synthetic z(Lcom/android/calendar/e/g;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 2203
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, -0x1

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    div-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->bY:I

    .line 2204
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->bZ:I

    .line 2205
    return-void
.end method


# virtual methods
.method public a(Landroid/text/format/Time;)I
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1066
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v3, v2, Landroid/text/format/Time;->hour:I

    .line 1067
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v4, v2, Landroid/text/format/Time;->minute:I

    .line 1068
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v5, v2, Landroid/text/format/Time;->second:I

    .line 1070
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iput v0, v2, Landroid/text/format/Time;->hour:I

    .line 1071
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iput v0, v2, Landroid/text/format/Time;->minute:I

    .line 1072
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iput v0, v2, Landroid/text/format/Time;->second:I

    .line 1074
    sget-boolean v2, Lcom/android/calendar/e/g;->s:Z

    if-eqz v2, :cond_0

    .line 1075
    sget-object v2, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Begin "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v7}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1076
    sget-object v2, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Diff  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1080
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-static {p1, v2}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v2

    .line 1081
    if-ltz v2, :cond_4

    .line 1083
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v6, v2, Landroid/text/format/Time;->monthDay:I

    iget v7, p0, Lcom/android/calendar/e/g;->i:I

    add-int/2addr v6, v7

    iput v6, v2, Landroid/text/format/Time;->monthDay:I

    .line 1084
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1085
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/hj;->d(Landroid/text/format/Time;)V

    .line 1086
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-static {p1, v2}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v2

    .line 1088
    sget-boolean v6, Lcom/android/calendar/e/g;->s:Z

    if-eqz v6, :cond_1

    .line 1089
    sget-object v6, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "End   "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v8}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    :cond_1
    iget-object v6, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v7, v6, Landroid/text/format/Time;->monthDay:I

    iget v8, p0, Lcom/android/calendar/e/g;->i:I

    sub-int/2addr v7, v8

    iput v7, v6, Landroid/text/format/Time;->monthDay:I

    .line 1093
    iget-object v6, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v6, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1095
    if-gez v2, :cond_3

    .line 1105
    :goto_0
    sget-boolean v1, Lcom/android/calendar/e/g;->s:Z

    if-eqz v1, :cond_2

    .line 1106
    sget-object v1, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Diff: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iput v3, v1, Landroid/text/format/Time;->hour:I

    .line 1110
    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iput v4, v1, Landroid/text/format/Time;->minute:I

    .line 1111
    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iput v5, v1, Landroid/text/format/Time;->second:I

    .line 1112
    return v0

    .line 1098
    :cond_3
    if-nez v2, :cond_4

    move v0, v1

    .line 1100
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 896
    invoke-direct {p0}, Lcom/android/calendar/e/g;->t()V

    .line 897
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->cd:Z

    .line 898
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->ce:Z

    .line 899
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cd:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/ar;->b:[Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/android/calendar/e/g;->ca:[Ljava/lang/String;

    move v0, v1

    .line 900
    :goto_1
    const/16 v2, 0x18

    if-ge v0, v2, :cond_1

    .line 901
    iget-object v2, p0, Lcom/android/calendar/e/g;->ca:[Ljava/lang/String;

    const-string v3, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/calendar/e/g;->ca:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 900
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 899
    :cond_0
    sget-object v0, Lcom/android/calendar/ar;->a:[Ljava/lang/String;

    goto :goto_0

    .line 903
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cf:Z

    if-eqz v0, :cond_2

    .line 904
    invoke-direct {p0}, Lcom/android/calendar/e/g;->s()V

    .line 906
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->aE:I

    .line 907
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 908
    iput v1, p0, Lcom/android/calendar/e/g;->ao:I

    .line 909
    iput v1, p0, Lcom/android/calendar/e/g;->ap:I

    .line 910
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->n(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->dG:Z

    .line 911
    invoke-direct {p0}, Lcom/android/calendar/e/g;->R()V

    .line 912
    return-void
.end method

.method public a(F)V
    .locals 4

    .prologue
    .line 5543
    iget v1, p0, Lcom/android/calendar/e/g;->bW:I

    .line 5544
    sget v0, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v2, v0, 0x1

    .line 5546
    int-to-float v0, v1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 5547
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/e/g;->q:I

    .line 5550
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lcom/android/calendar/e/g;->i:I

    if-ge v0, v3, :cond_1

    .line 5551
    add-int/2addr v1, v2

    .line 5552
    int-to-float v3, v1

    cmpg-float v3, p1, v3

    if-gez v3, :cond_2

    .line 5553
    iput v0, p0, Lcom/android/calendar/e/g;->q:I

    .line 5557
    :cond_1
    return-void

    .line 5550
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 4372
    if-ltz p1, :cond_0

    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-eqz v0, :cond_1

    .line 4382
    :cond_0
    :goto_0
    return-void

    .line 4375
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->cG:Lcom/android/calendar/e/af;

    if-nez v0, :cond_2

    .line 4376
    new-instance v0, Lcom/android/calendar/e/af;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/e/af;-><init>(Lcom/android/calendar/e/g;Lcom/android/calendar/e/h;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->cG:Lcom/android/calendar/e/af;

    .line 4378
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->cG:Lcom/android/calendar/e/af;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/af;->a(I)V

    .line 4379
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 4380
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->cG:Lcom/android/calendar/e/af;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x3

    const/4 v1, -0x1

    .line 4999
    if-nez p1, :cond_3

    .line 5000
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->eg:Z

    if-eqz v0, :cond_2

    .line 5001
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->eh:Z

    if-nez v0, :cond_1

    .line 5002
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v0, p2, v3, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V

    .line 5022
    :cond_0
    :goto_0
    return-void

    .line 5004
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v0, p2, v2, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V

    goto :goto_0

    .line 5007
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v0, p2, v4, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V

    goto :goto_0

    .line 5009
    :cond_3
    if-ne p1, v4, :cond_6

    .line 5010
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->eh:Z

    if-nez v0, :cond_5

    .line 5011
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->eg:Z

    if-eqz v0, :cond_4

    .line 5012
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v0, p2, v2, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V

    goto :goto_0

    .line 5014
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v0, p2, v3, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V

    goto :goto_0

    .line 5017
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v0, p2, v2, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V

    goto :goto_0

    .line 5019
    :cond_6
    if-ne p1, v3, :cond_0

    .line 5020
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v0, p2, v2, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;Landroid/database/Cursor;II)V

    goto :goto_0
.end method

.method public a(JJ)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 6613
    iget-object v0, p0, Lcom/android/calendar/e/g;->cJ:Landroid/content/AsyncQueryHandler;

    if-nez v0, :cond_0

    .line 6614
    new-instance v0, Lcom/android/calendar/e/y;

    iget-object v3, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/e/y;-><init>(Lcom/android/calendar/e/g;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->cJ:Landroid/content/AsyncQueryHandler;

    .line 6616
    :cond_0
    iput-wide p1, p0, Lcom/android/calendar/e/g;->cK:J

    .line 6617
    iput-wide p3, p0, Lcom/android/calendar/e/g;->cL:J

    .line 6618
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 6619
    iget-wide v4, p0, Lcom/android/calendar/e/g;->cK:J

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 6620
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->cM:I

    .line 6621
    iget-object v0, p0, Lcom/android/calendar/e/g;->cJ:Landroid/content/AsyncQueryHandler;

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 6622
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 6623
    iget-wide v4, p0, Lcom/android/calendar/e/g;->cK:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 6624
    iget-wide v4, p0, Lcom/android/calendar/e/g;->cL:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 6625
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 6626
    iget-object v0, p0, Lcom/android/calendar/e/g;->cJ:Landroid/content/AsyncQueryHandler;

    invoke-static {}, Lcom/android/calendar/dh;->j()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/calendar/e/g;->m()Ljava/lang/String;

    move-result-object v5

    const-string v7, "startDay,startMinute,title"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6627
    return-void
.end method

.method public a(Landroid/text/format/Time;ZZ)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 989
    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 990
    if-nez p2, :cond_0

    .line 991
    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 993
    :cond_0
    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-nez v0, :cond_1

    .line 994
    invoke-direct {p0, v3}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 996
    :cond_1
    iput-object v3, p0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    .line 997
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 999
    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 1000
    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->setSelectedDay(I)V

    .line 1001
    iget-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1002
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->cl:Z

    .line 1004
    invoke-direct {p0}, Lcom/android/calendar/e/g;->u()V

    .line 1007
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1008
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1009
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 1010
    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1011
    iget-object v3, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget-object v3, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iput-object v3, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1012
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 1014
    :cond_2
    const-wide/16 v8, 0x2710

    sub-long v8, v4, v8

    cmp-long v0, v8, v6

    if-gez v0, :cond_4

    cmp-long v0, v6, v4

    if-gez v0, :cond_4

    move v0, v2

    .line 1015
    :goto_0
    if-nez v0, :cond_3

    if-nez p2, :cond_3

    if-eqz p3, :cond_5

    :cond_3
    move v0, v1

    :goto_1
    iput v0, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1016
    iput-boolean v2, p0, Lcom/android/calendar/e/g;->aK:Z

    .line 1017
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 1018
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->c(Z)V

    .line 1019
    return-void

    :cond_4
    move v0, v1

    .line 1014
    goto :goto_0

    .line 1015
    :cond_5
    const/4 v0, 0x2

    goto :goto_1
.end method

.method a(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 4065
    sget-object v0, Lcom/android/calendar/e/ag;->b:Lcom/android/calendar/e/ag;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 4066
    iput-boolean v5, p0, Lcom/android/calendar/e/g;->C:Z

    .line 4067
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 4068
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->M:Lcom/android/calendar/e/x;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4070
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 4071
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 4076
    iget-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 4077
    iget v3, p0, Lcom/android/calendar/e/g;->ai:I

    .line 4078
    iget v4, p0, Lcom/android/calendar/e/g;->aj:I

    .line 4079
    invoke-direct {p0, v0, v1, v5}, Lcom/android/calendar/e/g;->a(IIZ)Z

    .line 4080
    iput-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 4081
    iput v3, p0, Lcom/android/calendar/e/g;->ai:I

    .line 4082
    iput v4, p0, Lcom/android/calendar/e/g;->aj:I

    .line 4083
    iget-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->b()V

    .line 4084
    return-void
.end method

.method public a(Landroid/view/MotionEvent;ILcom/android/calendar/dh;IJI)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5026
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 5027
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5028
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5031
    :cond_0
    iput-boolean v4, p0, Lcom/android/calendar/e/g;->I:Z

    .line 5032
    iput p4, p0, Lcom/android/calendar/e/g;->dV:I

    .line 5033
    iput-wide p5, p0, Lcom/android/calendar/e/g;->dB:J

    .line 5034
    iput p7, p0, Lcom/android/calendar/e/g;->dY:I

    .line 5035
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    int-to-float v1, p2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 5036
    iput-boolean v4, p0, Lcom/android/calendar/e/g;->H:Z

    .line 5038
    :cond_1
    iget-wide v0, p3, Lcom/android/calendar/dh;->m:J

    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    iput-wide v0, p3, Lcom/android/calendar/dh;->n:J

    .line 5042
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v0, :cond_2

    .line 5043
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->i:I

    iput v0, p3, Lcom/android/calendar/dh;->i:I

    .line 5044
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->j:I

    iput v0, p3, Lcom/android/calendar/dh;->j:I

    .line 5045
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v0, p3, Lcom/android/calendar/dh;->m:J

    .line 5046
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-wide v0, v0, Lcom/android/calendar/dh;->n:J

    iput-wide v0, p3, Lcom/android/calendar/dh;->n:J

    .line 5048
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/android/calendar/e/g;->b(Landroid/view/MotionEvent;Lcom/android/calendar/dh;)V

    .line 5049
    return-void
.end method

.method a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 4528
    invoke-direct {p0}, Lcom/android/calendar/e/g;->K()V

    .line 4529
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->D:Z

    if-eqz v0, :cond_0

    .line 4530
    iput v5, p0, Lcom/android/calendar/e/g;->cR:F

    .line 4531
    iput v5, p0, Lcom/android/calendar/e/g;->cS:F

    .line 4534
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/g;->cR:F

    add-float/2addr v0, p3

    iput v0, p0, Lcom/android/calendar/e/g;->cR:F

    .line 4535
    iget v0, p0, Lcom/android/calendar/e/g;->cS:F

    add-float/2addr v0, p4

    iput v0, p0, Lcom/android/calendar/e/g;->cS:F

    .line 4536
    iget v0, p0, Lcom/android/calendar/e/g;->cR:F

    float-to-int v0, v0

    .line 4537
    iget v1, p0, Lcom/android/calendar/e/g;->cS:F

    float-to-int v1, v1

    .line 4540
    iget-object v2, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    sget-object v3, Lcom/android/calendar/e/ag;->b:Lcom/android/calendar/e/ag;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v3, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v3, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    if-ne v2, v3, :cond_2

    .line 4541
    :cond_1
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 4542
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 4543
    iget v4, p0, Lcom/android/calendar/e/g;->bx:I

    iput v4, p0, Lcom/android/calendar/e/g;->bG:I

    .line 4544
    iget v4, p0, Lcom/android/calendar/e/g;->by:I

    iput v4, p0, Lcom/android/calendar/e/g;->bH:I

    .line 4545
    if-le v2, v3, :cond_6

    .line 4546
    sget-object v2, Lcom/android/calendar/e/ag;->d:Lcom/android/calendar/e/ag;

    iput-object v2, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 4551
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    sget-object v3, Lcom/android/calendar/e/ag;->d:Lcom/android/calendar/e/ag;

    if-ne v2, v3, :cond_3

    .line 4552
    iget v2, p0, Lcom/android/calendar/e/g;->bI:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_3

    .line 4553
    iget v2, p0, Lcom/android/calendar/e/g;->bG:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/e/g;->bx:I

    .line 4554
    iget v0, p0, Lcom/android/calendar/e/g;->bx:I

    if-gez v0, :cond_7

    .line 4555
    iput v6, p0, Lcom/android/calendar/e/g;->bx:I

    .line 4562
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    sget-object v2, Lcom/android/calendar/e/ag;->c:Lcom/android/calendar/e/ag;

    if-ne v0, v2, :cond_5

    .line 4563
    iget v0, p0, Lcom/android/calendar/e/g;->bH:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    .line 4564
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    if-gez v0, :cond_8

    .line 4565
    iput v6, p0, Lcom/android/calendar/e/g;->by:I

    .line 4569
    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/android/calendar/e/g;->z()V

    .line 4571
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->cQ:Z

    .line 4572
    iput v6, p0, Lcom/android/calendar/e/g;->cP:I

    .line 4573
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 4574
    return-void

    .line 4548
    :cond_6
    sget-object v2, Lcom/android/calendar/e/ag;->c:Lcom/android/calendar/e/ag;

    iput-object v2, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    goto :goto_0

    .line 4556
    :cond_7
    iget v0, p0, Lcom/android/calendar/e/g;->bx:I

    iget v2, p0, Lcom/android/calendar/e/g;->bz:I

    if-le v0, v2, :cond_3

    .line 4557
    iget v0, p0, Lcom/android/calendar/e/g;->bz:I

    iput v0, p0, Lcom/android/calendar/e/g;->bx:I

    goto :goto_1

    .line 4566
    :cond_8
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iget v1, p0, Lcom/android/calendar/e/g;->bA:I

    if-le v0, v1, :cond_4

    .line 4567
    iget v0, p0, Lcom/android/calendar/e/g;->bA:I

    iput v0, p0, Lcom/android/calendar/e/g;->by:I

    goto :goto_2
.end method

.method public a(Landroid/view/MotionEvent;Lcom/android/calendar/dh;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 6538
    iput-boolean v7, p0, Lcom/android/calendar/e/g;->H:Z

    .line 6539
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-nez v0, :cond_0

    .line 6540
    iput-boolean v6, p0, Lcom/android/calendar/e/g;->I:Z

    .line 6541
    const/4 v1, 0x0

    .line 6543
    :try_start_0
    invoke-virtual {p2}, Lcom/android/calendar/dh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6547
    :goto_0
    iget-wide v2, p2, Lcom/android/calendar/dh;->b:J

    iput-wide v2, v0, Lcom/android/calendar/dh;->b:J

    .line 6548
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v1, v1

    iput v1, v0, Lcom/android/calendar/dh;->E:F

    .line 6549
    iget v1, v0, Lcom/android/calendar/dh;->E:F

    sget v2, Lcom/android/calendar/e/g;->bE:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Lcom/android/calendar/dh;->F:F

    .line 6550
    iget v1, p2, Lcom/android/calendar/dh;->C:F

    iput v1, v0, Lcom/android/calendar/dh;->C:F

    .line 6551
    iget v1, p2, Lcom/android/calendar/dh;->D:F

    iput v1, v0, Lcom/android/calendar/dh;->D:F

    .line 6553
    iget-wide v2, v0, Lcom/android/calendar/dh;->m:J

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/android/calendar/dh;->n:J

    .line 6554
    iput-boolean v6, v0, Lcom/android/calendar/dh;->f:Z

    .line 6555
    iget v1, v0, Lcom/android/calendar/dh;->i:I

    iput v1, p0, Lcom/android/calendar/e/g;->ai:I

    .line 6556
    iget-object v1, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6557
    iput-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 6558
    iput-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 6559
    sget-object v0, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 6560
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->dT:I

    .line 6561
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->dU:I

    .line 6562
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->dV:I

    .line 6563
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->E:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->dW:I

    .line 6564
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->F:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->dX:I

    .line 6565
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dB:J

    .line 6566
    iput-boolean v7, p0, Lcom/android/calendar/e/g;->ec:Z

    .line 6567
    iput-boolean v6, p0, Lcom/android/calendar/e/g;->D:Z

    .line 6568
    iput-boolean v6, p0, Lcom/android/calendar/e/g;->dx:Z

    .line 6569
    iput-boolean v6, p0, Lcom/android/calendar/e/g;->bO:Z

    .line 6571
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/e/g;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 6572
    return-void

    .line 6544
    :catch_0
    move-exception v0

    .line 6545
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v2, "Fail to clone event"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/dh;II)V
    .locals 6

    .prologue
    .line 6382
    new-instance v0, Lcom/android/calendar/e/am;

    sget v1, Lcom/android/calendar/e/g;->as:I

    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/e/am;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    .line 6384
    if-eqz p1, :cond_0

    .line 6385
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v0, p2, p3}, Lcom/android/calendar/e/am;->a(II)V

    .line 6386
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    iget-wide v2, p1, Lcom/android/calendar/dh;->m:J

    iget-wide v4, p1, Lcom/android/calendar/dh;->n:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/calendar/e/am;->a(JJ)V

    .line 6387
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v0}, Lcom/android/calendar/e/am;->b()V

    .line 6388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->cX:Z

    .line 6390
    :cond_0
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2300
    iput-object p1, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    .line 2302
    iput-boolean v6, p0, Lcom/android/calendar/e/g;->dz:Z

    .line 2304
    iget-object v0, p0, Lcom/android/calendar/e/g;->af:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 2305
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/g;->af:Ljava/util/ArrayList;

    .line 2310
    :goto_0
    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v0, :cond_2

    .line 2311
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 2312
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-wide v2, v2, Lcom/android/calendar/dh;->q:J

    iget-wide v4, v0, Lcom/android/calendar/dh;->q:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 2313
    iput-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    goto :goto_1

    .line 2307
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->af:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 2318
    :cond_2
    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-eqz v0, :cond_4

    .line 2319
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v0, :cond_4

    .line 2320
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/g;->a(Ljava/util/ArrayList;Lcom/android/calendar/dh;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2321
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/g;->b(Ljava/util/ArrayList;Lcom/android/calendar/dh;)V

    .line 2323
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2328
    :cond_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 2329
    invoke-virtual {v0}, Lcom/android/calendar/dh;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2330
    iget-object v2, p0, Lcom/android/calendar/e/g;->af:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2335
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/g;->ag:[Landroid/text/StaticLayout;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/e/g;->ag:[Landroid/text/StaticLayout;

    array-length v0, v0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 2336
    :cond_7
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/text/StaticLayout;

    iput-object v0, p0, Lcom/android/calendar/e/g;->ag:[Landroid/text/StaticLayout;

    .line 2341
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->ah:[Landroid/text/StaticLayout;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/e/g;->ah:[Landroid/text/StaticLayout;

    array-length v0, v0

    iget-object v1, p0, Lcom/android/calendar/e/g;->af:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_c

    .line 2342
    :cond_8
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/text/StaticLayout;

    iput-object v0, p0, Lcom/android/calendar/e/g;->ah:[Landroid/text/StaticLayout;

    .line 2347
    :goto_4
    invoke-direct {p0}, Lcom/android/calendar/e/g;->C()V

    .line 2348
    invoke-direct {p0}, Lcom/android/calendar/e/g;->L()V

    .line 2349
    invoke-direct {p0}, Lcom/android/calendar/e/g;->E()Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/android/calendar/e/g;->ci:I

    if-lez v0, :cond_9

    iget v0, p0, Lcom/android/calendar/e/g;->cj:I

    if-lez v0, :cond_9

    .line 2350
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_9

    .line 2351
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/e/s;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/s;-><init>(Lcom/android/calendar/e/g;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2362
    :cond_9
    iput-boolean v7, p0, Lcom/android/calendar/e/g;->aK:Z

    .line 2363
    iput-boolean v7, p0, Lcom/android/calendar/e/g;->cl:Z

    .line 2364
    invoke-direct {p0}, Lcom/android/calendar/e/g;->u()V

    .line 2365
    invoke-direct {p0}, Lcom/android/calendar/e/g;->B()V

    .line 2366
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 2367
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cH:Z

    if-eqz v0, :cond_a

    .line 2368
    iput-boolean v6, p0, Lcom/android/calendar/e/g;->cH:Z

    .line 2369
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getFirstVisibleHour()I

    move-result v0

    .line 2370
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getScrollHour()I

    move-result v1

    .line 2371
    const/4 v2, -0x1

    if-ne v1, v2, :cond_d

    .line 2374
    :goto_5
    invoke-virtual {p0, v0}, Lcom/android/calendar/e/g;->a(I)V

    .line 2376
    :cond_a
    sput-boolean v6, Lcom/android/calendar/e/g;->p:Z

    .line 2377
    return-void

    .line 2338
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/e/g;->ag:[Landroid/text/StaticLayout;

    invoke-static {v0, v8}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3

    .line 2344
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/e/g;->ah:[Landroid/text/StaticLayout;

    invoke-static {v0, v8}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    :cond_d
    move v0, v1

    goto :goto_5
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 2131
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->cH:Z

    .line 2132
    return-void
.end method

.method public a([Ljava/lang/CharSequence;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 5166
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f01d0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5167
    new-instance v1, Lcom/android/calendar/e/i;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/i;-><init>(Lcom/android/calendar/e/g;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 5177
    new-instance v1, Lcom/android/calendar/e/j;

    invoke-direct {v1, p0, p2}, Lcom/android/calendar/e/j;-><init>(Lcom/android/calendar/e/g;Landroid/database/Cursor;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5184
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 5185
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 5186
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 5187
    return-void
.end method

.method public a(Landroid/view/ScaleGestureDetector;)Z
    .locals 5

    .prologue
    .line 4724
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->bO:Z

    .line 4725
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v0

    sget v1, Lcom/android/calendar/e/g;->bR:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->bQ:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 4726
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    .line 4727
    sget v2, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/calendar/e/g;->i:I

    mul-int/2addr v2, v3

    int-to-float v2, v2

    .line 4728
    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v4

    add-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 4729
    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    iput v2, p0, Lcom/android/calendar/e/g;->bM:F

    .line 4733
    :goto_0
    iget v2, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    sget v2, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/e/g;->bM:F

    .line 4734
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->bN:F

    .line 4736
    sget v0, Lcom/android/calendar/e/g;->y:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->bI:F

    .line 4737
    sget v0, Lcom/android/calendar/e/g;->y:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->bJ:F

    .line 4739
    sget v0, Lcom/android/calendar/e/g;->as:I

    iput v0, p0, Lcom/android/calendar/e/g;->bL:I

    .line 4740
    sget v0, Lcom/android/calendar/e/g;->bE:I

    iput v0, p0, Lcom/android/calendar/e/g;->bK:I

    .line 4742
    sget-boolean v0, Lcom/android/calendar/e/g;->t:Z

    if-eqz v0, :cond_0

    .line 4743
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 4744
    sget-object v1, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onScaleBegin: mGestureCenterHour:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/calendar/e/g;->bN:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tViewStartHour: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\tmViewStartY:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\tmCellHeight:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/android/calendar/e/g;->bE:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " SpanY:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 4749
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 4731
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/calendar/e/g;->bM:F

    goto/16 :goto_0
.end method

.method public b(JJ)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 6640
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 6641
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 6642
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v0, v1}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/g;->cM:I

    .line 6643
    iget-object v0, p0, Lcom/android/calendar/e/g;->cJ:Landroid/content/AsyncQueryHandler;

    invoke-virtual {v0, v6}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 6644
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 6645
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 6646
    invoke-static {v0, p3, p4}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 6647
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 6649
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/dh;->j()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/calendar/e/g;->m()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "startDay,startMinute,title"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 6651
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6652
    iget-object v2, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget v3, p0, Lcom/android/calendar/e/g;->cM:I

    iget v4, p0, Lcom/android/calendar/e/g;->cM:I

    move v5, v6

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;IIZ)V

    .line 6653
    invoke-virtual {v0}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    .line 6654
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 6655
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 6656
    iget-wide v4, v2, Lcom/android/calendar/dh;->n:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_1

    iget-wide v4, v2, Lcom/android/calendar/dh;->n:J

    iget-wide v6, v2, Lcom/android/calendar/dh;->m:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    :cond_1
    invoke-virtual {v2}, Lcom/android/calendar/dh;->e()Z

    move-result v4

    if-nez v4, :cond_2

    iget-wide v4, v2, Lcom/android/calendar/dh;->m:J

    cmp-long v4, v4, p3

    if-nez v4, :cond_0

    iget-wide v4, v2, Lcom/android/calendar/dh;->n:J

    iget-wide v6, v2, Lcom/android/calendar/dh;->m:J

    cmp-long v2, v4, v6

    if-eqz v2, :cond_0

    .line 6657
    :cond_2
    invoke-interface {v3}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 6660
    :cond_3
    if-eqz v1, :cond_4

    .line 6661
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6663
    :cond_4
    return-object v0
.end method

.method public b()V
    .locals 14

    .prologue
    const-wide/16 v7, -0x1

    const/4 v2, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1045
    new-instance v6, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v6, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1046
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1047
    invoke-virtual {v6, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 1048
    new-instance v5, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v5, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1049
    iget v0, v5, Landroid/text/format/Time;->monthDay:I

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 1051
    iput-boolean v9, v5, Landroid/text/format/Time;->allDay:Z

    .line 1052
    iget v0, v5, Landroid/text/format/Time;->minute:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v5, Landroid/text/format/Time;->minute:I

    .line 1053
    invoke-virtual {v5, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 1055
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x400

    move-object v1, p0

    move-wide v10, v7

    move-object v12, v4

    move-object v13, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1057
    return-void
.end method

.method public b(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 6575
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->H:Z

    .line 6576
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v0}, Lcom/android/calendar/e/am;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6577
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v0}, Lcom/android/calendar/e/am;->d()V

    .line 6579
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 6580
    return-void
.end method

.method b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 11

    .prologue
    .line 4620
    invoke-direct {p0}, Lcom/android/calendar/e/g;->N()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4720
    :cond_0
    :goto_0
    return-void

    .line 4623
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    sub-int v1, v0, v1

    .line 4624
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 4625
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v0, v3

    .line 4626
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 4627
    const/4 v0, 0x1

    .line 4628
    iget-boolean v4, p0, Lcom/android/calendar/e/g;->cg:Z

    if-eqz v4, :cond_5

    .line 4629
    const/16 v4, 0x4b

    if-lt v2, v4, :cond_5

    if-le v2, v3, :cond_5

    .line 4630
    if-lez v1, :cond_2

    iget v4, p0, Lcom/android/calendar/e/g;->bx:I

    if-eqz v4, :cond_2

    .line 4631
    const/4 v0, 0x0

    .line 4633
    :cond_2
    if-gtz v1, :cond_3

    iget v4, p0, Lcom/android/calendar/e/g;->bx:I

    iget v5, p0, Lcom/android/calendar/e/g;->bz:I

    if-eq v4, v5, :cond_3

    .line 4634
    const/4 v0, 0x0

    .line 4636
    :cond_3
    iget v4, p0, Lcom/android/calendar/e/g;->bI:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_4

    .line 4637
    const/4 v0, 0x0

    .line 4639
    :cond_4
    iget v4, p0, Lcom/android/calendar/e/g;->bx:I

    iget v5, p0, Lcom/android/calendar/e/g;->bG:I

    if-eq v4, v5, :cond_5

    .line 4640
    const/4 v0, 0x0

    .line 4644
    :cond_5
    sget-object v4, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    iput-object v4, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 4645
    const/4 v4, 0x0

    iput v4, p0, Lcom/android/calendar/e/g;->cP:I

    .line 4646
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/calendar/e/g;->C:Z

    .line 4647
    sget-boolean v4, Lcom/android/calendar/e/g;->s:Z

    if-eqz v4, :cond_6

    .line 4648
    sget-object v4, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doFling: deltaX "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", HORIZONTAL_FLING_THRESHOLD "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x4b

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 4651
    :cond_6
    iget v4, p0, Lcom/android/calendar/e/g;->i:I

    .line 4653
    if-eqz v0, :cond_9

    const/16 v0, 0x4b

    if-lt v2, v0, :cond_9

    if-le v2, v3, :cond_9

    .line 4654
    const/4 v0, 0x0

    .line 4655
    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_d

    .line 4656
    if-gez v1, :cond_c

    .line 4657
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7f4

    if-gt v2, v3, :cond_7

    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7f4

    if-ne v2, v3, :cond_8

    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->month:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_8

    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v2, v4

    const/16 v3, 0x1f

    if-lt v2, v3, :cond_8

    .line 4660
    :cond_7
    const/4 v0, 0x1

    .line 4676
    :cond_8
    :goto_1
    if-eqz v0, :cond_10

    .line 4677
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    .line 4694
    :cond_9
    sget-object v0, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 4695
    sget-boolean v0, Lcom/android/calendar/e/g;->s:Z

    if-eqz v0, :cond_a

    .line 4696
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doFling: mViewStartY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " velocityY "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 4699
    :cond_a
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/e/g;->ea:I

    .line 4700
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/e/g;->dZ:I

    .line 4702
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->cQ:Z

    .line 4703
    iget-object v0, p0, Lcom/android/calendar/e/g;->dj:Landroid/widget/OverScroller;

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    const/4 v3, 0x0

    neg-float v4, p4

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v8, p0, Lcom/android/calendar/e/g;->bA:I

    iget v9, p0, Lcom/android/calendar/e/g;->dk:I

    iget v10, p0, Lcom/android/calendar/e/g;->dk:I

    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    .line 4709
    const/4 v0, 0x0

    cmpl-float v0, p4, v0

    if-lez v0, :cond_13

    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    if-eqz v0, :cond_13

    .line 4710
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->di:Z

    .line 4717
    :cond_b
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 4718
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->M:Lcom/android/calendar/e/x;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 4663
    :cond_c
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x76e

    if-ge v2, v3, :cond_8

    .line 4664
    const/4 v0, 0x1

    goto :goto_1

    .line 4668
    :cond_d
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7f4

    if-ne v2, v3, :cond_e

    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->month:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_e

    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0x1f

    if-ne v2, v3, :cond_e

    if-ltz v1, :cond_f

    :cond_e
    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x76e

    if-ne v2, v3, :cond_8

    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->month:I

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    if-lez v1, :cond_8

    .line 4672
    :cond_f
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 4679
    :cond_10
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->G:Z

    if-eqz v0, :cond_0

    .line 4682
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_11

    .line 4683
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/e/g;->ed:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4684
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/e/g;->ed:Ljava/lang/Runnable;

    const-wide/16 v6, 0x1c2

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4686
    :cond_11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->G:Z

    .line 4687
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 4688
    if-gez v1, :cond_12

    const/4 v0, 0x1

    :goto_3
    iget v1, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/calendar/e/g;->bC:I

    int-to-float v2, v2

    invoke-direct {p0, v0, v4, v1, v2}, Lcom/android/calendar/e/g;->a(ZIFF)Landroid/view/View;

    goto/16 :goto_0

    :cond_12
    const/4 v0, 0x0

    goto :goto_3

    .line 4714
    :cond_13
    const/4 v0, 0x0

    cmpg-float v0, p4, v0

    if-gez v0, :cond_b

    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iget v1, p0, Lcom/android/calendar/e/g;->bA:I

    if-eq v0, v1, :cond_b

    .line 4715
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->di:Z

    goto/16 :goto_2
.end method

.method public b(Landroid/view/ScaleGestureDetector;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4754
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cg:Z

    if-eqz v0, :cond_2

    .line 4755
    sget v0, Lcom/android/calendar/e/g;->y:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 4756
    iget v1, p0, Lcom/android/calendar/e/g;->bL:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/android/calendar/e/g;->bI:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/e/g;->as:I

    .line 4758
    sget v1, Lcom/android/calendar/e/g;->as:I

    iget v2, p0, Lcom/android/calendar/e/g;->ar:I

    if-ge v1, v2, :cond_6

    .line 4759
    iput v0, p0, Lcom/android/calendar/e/g;->bI:F

    .line 4760
    iget v0, p0, Lcom/android/calendar/e/g;->ar:I

    sput v0, Lcom/android/calendar/e/g;->as:I

    .line 4761
    iget v0, p0, Lcom/android/calendar/e/g;->ar:I

    iput v0, p0, Lcom/android/calendar/e/g;->bL:I

    .line 4768
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    .line 4769
    iget v1, p0, Lcom/android/calendar/e/g;->bM:F

    sget v2, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->bx:I

    .line 4770
    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    sget v2, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/e/g;->bC:I

    iget v3, p0, Lcom/android/calendar/e/g;->bW:I

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/e/g;->bz:I

    .line 4771
    iget v1, p0, Lcom/android/calendar/e/g;->bz:I

    if-gez v1, :cond_1

    .line 4772
    iput v5, p0, Lcom/android/calendar/e/g;->bz:I

    .line 4774
    :cond_1
    iget v1, p0, Lcom/android/calendar/e/g;->bx:I

    if-gez v1, :cond_7

    .line 4775
    iput v5, p0, Lcom/android/calendar/e/g;->bx:I

    .line 4776
    iget v1, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->bM:F

    .line 4783
    :cond_2
    :goto_1
    sget v0, Lcom/android/calendar/e/g;->y:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 4785
    iget v1, p0, Lcom/android/calendar/e/g;->bK:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/android/calendar/e/g;->bJ:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/e/g;->bE:I

    .line 4787
    sget v1, Lcom/android/calendar/e/g;->bE:I

    sget v2, Lcom/android/calendar/e/g;->bF:I

    if-ge v1, v2, :cond_8

    .line 4790
    iput v0, p0, Lcom/android/calendar/e/g;->bJ:F

    .line 4791
    sget v0, Lcom/android/calendar/e/g;->bF:I

    sput v0, Lcom/android/calendar/e/g;->bE:I

    .line 4792
    sget v0, Lcom/android/calendar/e/g;->bF:I

    iput v0, p0, Lcom/android/calendar/e/g;->bK:I

    .line 4799
    :cond_3
    :goto_2
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v0

    float-to-int v0, v0

    sget v1, Lcom/android/calendar/e/g;->bR:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->bQ:I

    sub-int/2addr v0, v1

    .line 4800
    iget v1, p0, Lcom/android/calendar/e/g;->bN:F

    sget v2, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/android/calendar/e/g;->by:I

    .line 4801
    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x18

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/android/calendar/e/g;->bD:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/e/g;->bA:I

    .line 4803
    sget-boolean v1, Lcom/android/calendar/e/g;->t:Z

    if-eqz v1, :cond_4

    .line 4804
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 4805
    sget-object v2, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onScale: mGestureCenterHour:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/calendar/e/g;->bN:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tViewStartHour: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\tmViewStartY:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/calendar/e/g;->by:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\tmCellHeight:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/android/calendar/e/g;->bE:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " SpanY:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanY()F

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 4810
    :cond_4
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    if-gez v1, :cond_9

    .line 4811
    iput v5, p0, Lcom/android/calendar/e/g;->by:I

    .line 4812
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->bN:F

    .line 4818
    :cond_5
    :goto_3
    invoke-direct {p0}, Lcom/android/calendar/e/g;->z()V

    .line 4820
    iput-boolean v6, p0, Lcom/android/calendar/e/g;->aK:Z

    .line 4821
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 4822
    return v6

    .line 4762
    :cond_6
    sget v1, Lcom/android/calendar/e/g;->as:I

    iget v2, p0, Lcom/android/calendar/e/g;->x:I

    if-le v1, v2, :cond_0

    .line 4763
    iput v0, p0, Lcom/android/calendar/e/g;->bI:F

    .line 4764
    iget v0, p0, Lcom/android/calendar/e/g;->x:I

    sput v0, Lcom/android/calendar/e/g;->as:I

    .line 4765
    iget v0, p0, Lcom/android/calendar/e/g;->x:I

    iput v0, p0, Lcom/android/calendar/e/g;->bL:I

    goto/16 :goto_0

    .line 4777
    :cond_7
    iget v1, p0, Lcom/android/calendar/e/g;->bx:I

    iget v2, p0, Lcom/android/calendar/e/g;->bz:I

    if-le v1, v2, :cond_2

    .line 4778
    iget v1, p0, Lcom/android/calendar/e/g;->bz:I

    iput v1, p0, Lcom/android/calendar/e/g;->bx:I

    .line 4779
    iget v1, p0, Lcom/android/calendar/e/g;->bx:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    sget v1, Lcom/android/calendar/e/g;->as:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->bM:F

    goto/16 :goto_1

    .line 4793
    :cond_8
    sget v1, Lcom/android/calendar/e/g;->bE:I

    sget v2, Lcom/android/calendar/e/g;->w:I

    if-le v1, v2, :cond_3

    .line 4794
    iput v0, p0, Lcom/android/calendar/e/g;->bJ:F

    .line 4795
    sget v0, Lcom/android/calendar/e/g;->w:I

    sput v0, Lcom/android/calendar/e/g;->bE:I

    .line 4796
    sget v0, Lcom/android/calendar/e/g;->w:I

    iput v0, p0, Lcom/android/calendar/e/g;->bK:I

    goto/16 :goto_2

    .line 4813
    :cond_9
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    iget v2, p0, Lcom/android/calendar/e/g;->bA:I

    if-le v1, v2, :cond_5

    .line 4814
    iget v1, p0, Lcom/android/calendar/e/g;->bA:I

    iput v1, p0, Lcom/android/calendar/e/g;->by:I

    .line 4815
    iget v1, p0, Lcom/android/calendar/e/g;->by:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/e/g;->bE:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/g;->bN:F

    goto :goto_3
.end method

.method c()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2269
    iget-object v0, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2271
    invoke-direct {p0, v5}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 2272
    iput-object v5, p0, Lcom/android/calendar/e/g;->co:Lcom/android/calendar/dh;

    .line 2273
    iget-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2274
    iput-boolean v3, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 2277
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/e/g;->ad:Ljava/lang/Runnable;

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2278
    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 2279
    iput v3, v0, Landroid/text/format/Time;->hour:I

    .line 2280
    iput v3, v0, Landroid/text/format/Time;->minute:I

    .line 2281
    iput v3, v0, Landroid/text/format/Time;->second:I

    .line 2282
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 2285
    invoke-direct {p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297
    :goto_0
    return-void

    .line 2290
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2291
    iget-object v0, p0, Lcom/android/calendar/e/g;->aL:Lcom/android/calendar/dj;

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    iget v3, p0, Lcom/android/calendar/e/g;->R:I

    new-instance v4, Lcom/android/calendar/e/r;

    invoke-direct {v4, p0, v2}, Lcom/android/calendar/e/r;-><init>(Lcom/android/calendar/e/g;Ljava/util/ArrayList;)V

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/dj;->a(ILjava/util/ArrayList;ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public c(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4827
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    iput v0, p0, Lcom/android/calendar/e/g;->bH:I

    .line 4828
    iget v0, p0, Lcom/android/calendar/e/g;->bx:I

    iput v0, p0, Lcom/android/calendar/e/g;->bG:I

    .line 4829
    iput v1, p0, Lcom/android/calendar/e/g;->cS:F

    .line 4830
    iput v1, p0, Lcom/android/calendar/e/g;->cR:F

    .line 4831
    iput v1, p0, Lcom/android/calendar/e/g;->bJ:F

    .line 4832
    iput v1, p0, Lcom/android/calendar/e/g;->bI:F

    .line 4833
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 3045
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dD:Z

    return v0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 714
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-nez v0, :cond_0

    .line 715
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 716
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 717
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 719
    :cond_0
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 720
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 4953
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->dx:Z

    .line 4954
    sget-object v0, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 4955
    iput-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 4956
    iput v1, p0, Lcom/android/calendar/e/g;->dT:I

    .line 4957
    iput v1, p0, Lcom/android/calendar/e/g;->dU:I

    .line 4958
    iput v1, p0, Lcom/android/calendar/e/g;->dV:I

    .line 4959
    iput v1, p0, Lcom/android/calendar/e/g;->dW:I

    .line 4960
    iput v1, p0, Lcom/android/calendar/e/g;->dX:I

    .line 4961
    iput v1, p0, Lcom/android/calendar/e/g;->ea:I

    .line 4962
    iput v1, p0, Lcom/android/calendar/e/g;->dY:I

    .line 4963
    iput v1, p0, Lcom/android/calendar/e/g;->dZ:I

    .line 4964
    iput-object v2, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 4965
    iput v1, p0, Lcom/android/calendar/e/g;->cP:I

    .line 4966
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 4967
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->F:Z

    .line 4968
    sput-boolean v1, Lcom/android/calendar/e/g;->p:Z

    .line 4969
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dB:J

    .line 4970
    invoke-direct {p0}, Lcom/android/calendar/e/g;->J()V

    .line 4971
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 4974
    invoke-direct {p0}, Lcom/android/calendar/e/g;->O()V

    .line 4975
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->dy:Z

    .line 4976
    iget-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_0

    .line 4977
    iget-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->h()V

    .line 4979
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 4980
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 5539
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/e/g;->q:I

    .line 5540
    return-void
.end method

.method public getCellHeight()I
    .locals 1

    .prologue
    .line 2610
    sget v0, Lcom/android/calendar/e/g;->bE:I

    return v0
.end method

.method public getEditMode()Lcom/android/calendar/e/ab;
    .locals 1

    .prologue
    .line 6506
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    return-object v0
.end method

.method getFirstVisibleHour()I
    .locals 1

    .prologue
    .line 981
    iget v0, p0, Lcom/android/calendar/e/g;->bY:I

    return v0
.end method

.method public getIsFromAllDay()Z
    .locals 1

    .prologue
    .line 6583
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->I:Z

    return v0
.end method

.method getNewEvent()Lcom/android/calendar/dh;
    .locals 4

    .prologue
    .line 3217
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/android/calendar/e/g;->getSelectedMinutesSinceMidnight()I

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/android/calendar/e/g;->a(IJI)Lcom/android/calendar/dh;

    move-result-object v0

    return-object v0
.end method

.method public getScrollHour()I
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 2143
    iget v0, p0, Lcom/android/calendar/e/g;->R:I

    iget v2, p0, Lcom/android/calendar/e/g;->P:I

    if-ne v0, v2, :cond_1

    .line 2144
    iget-object v0, p0, Lcom/android/calendar/e/g;->N:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    add-int/lit8 v0, v0, -0x1

    .line 2166
    :cond_0
    :goto_0
    return v0

    .line 2147
    :cond_1
    const/4 v2, 0x0

    .line 2148
    iget-object v0, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 2149
    goto :goto_0

    .line 2152
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->ae:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 2153
    invoke-virtual {v0}, Lcom/android/calendar/dh;->e()Z

    move-result v4

    if-nez v4, :cond_4

    iget v4, v0, Lcom/android/calendar/dh;->i:I

    iget v5, p0, Lcom/android/calendar/e/g;->R:I

    if-ne v4, v5, :cond_4

    .line 2159
    :goto_1
    if-nez v0, :cond_5

    move v0, v1

    .line 2160
    goto :goto_0

    .line 2162
    :cond_5
    iget v0, v0, Lcom/android/calendar/dh;->k:I

    div-int/lit8 v0, v0, 0x3c

    add-int/lit8 v0, v0, -0x1

    .line 2163
    if-gez v0, :cond_0

    .line 2164
    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    move-object v0, v2

    goto :goto_1
.end method

.method public getSelectedDay()Landroid/text/format/Time;
    .locals 2

    .prologue
    .line 1033
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1034
    iget v1, p0, Lcom/android/calendar/e/g;->ai:I

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 1035
    iget v1, p0, Lcom/android/calendar/e/g;->aj:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 1040
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1041
    return-object v0
.end method

.method getSelectedEvent()Lcom/android/calendar/dh;
    .locals 4

    .prologue
    .line 3204
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v0, :cond_0

    .line 3206
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/android/calendar/e/g;->getSelectedMinutesSinceMidnight()I

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/android/calendar/e/g;->a(IJI)Lcom/android/calendar/dh;

    move-result-object v0

    .line 3209
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    goto :goto_0
.end method

.method getSelectedTime()Landroid/text/format/Time;
    .locals 2

    .prologue
    .line 949
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 950
    iget v1, p0, Lcom/android/calendar/e/g;->ai:I

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 951
    iget v1, p0, Lcom/android/calendar/e/g;->aj:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 956
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 957
    return-object v0
.end method

.method getSelectedTimeForAccessibility()Landroid/text/format/Time;
    .locals 2

    .prologue
    .line 961
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 962
    iget v1, p0, Lcom/android/calendar/e/g;->al:I

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 963
    iget v1, p0, Lcom/android/calendar/e/g;->am:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 968
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 969
    return-object v0
.end method

.method getSelectedTimeInMillis()J
    .locals 2

    .prologue
    .line 945
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTime()Landroid/text/format/Time;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectionMode()I
    .locals 1

    .prologue
    .line 6374
    iget v0, p0, Lcom/android/calendar/e/g;->cP:I

    return v0
.end method

.method public getTouchMode()Lcom/android/calendar/e/ag;
    .locals 1

    .prologue
    .line 6502
    iget-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    return-object v0
.end method

.method public getViewScrollY()I
    .locals 1

    .prologue
    .line 2617
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    return v0
.end method

.method public getZoomLevel()I
    .locals 3

    .prologue
    .line 4852
    sget v0, Lcom/android/calendar/e/g;->w:I

    sget v1, Lcom/android/calendar/e/g;->bF:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x4

    .line 4853
    sget v1, Lcom/android/calendar/e/g;->bE:I

    sget v2, Lcom/android/calendar/e/g;->bF:I

    sub-int/2addr v1, v2

    div-int v0, v1, v0

    return v0
.end method

.method public h()V
    .locals 3

    .prologue
    .line 6153
    iget-object v0, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 6154
    iget-object v0, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 6156
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->c:Z

    .line 6157
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/e/g;->K:J

    .line 6158
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 6159
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aI:Lcom/android/calendar/e/z;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6160
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->O:Lcom/android/calendar/e/ah;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6161
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6162
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dS:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6163
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->cG:Lcom/android/calendar/e/af;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6165
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/e/g;->v()V

    .line 6166
    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-nez v0, :cond_2

    .line 6167
    invoke-direct {p0}, Lcom/android/calendar/e/g;->J()V

    .line 6170
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    const-string v1, "preferences_default_cell_height"

    sget v2, Lcom/android/calendar/e/g;->bE:I

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 6173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->aK:Z

    .line 6174
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 6185
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 6186
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->dR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6188
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/g;->S()V

    .line 6189
    invoke-direct {p0}, Lcom/android/calendar/e/g;->w()V

    .line 6190
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 6243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/g;->c:Z

    .line 6244
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 6245
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->O:Lcom/android/calendar/e/ah;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6246
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->O:Lcom/android/calendar/e/ah;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 6248
    :cond_0
    return-void
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 6514
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->D:Z

    return v0
.end method

.method public l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6522
    sget v0, Lcom/android/calendar/e/g;->v:I

    sput v0, Lcom/android/calendar/e/g;->bE:I

    .line 6523
    iget v0, p0, Lcom/android/calendar/e/g;->ar:I

    sput v0, Lcom/android/calendar/e/g;->as:I

    .line 6524
    iput v1, p0, Lcom/android/calendar/e/g;->bz:I

    .line 6525
    iput v1, p0, Lcom/android/calendar/e/g;->bx:I

    .line 6526
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 6527
    return-void
.end method

.method protected m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6630
    const-string v0, "visible=1"

    .line 6631
    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6632
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6635
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6636
    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 724
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 725
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    .line 726
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->O:Lcom/android/calendar/e/ah;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 730
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 731
    instance-of v1, v0, Lcom/android/calendar/e/be;

    if-eqz v1, :cond_1

    .line 732
    check-cast v0, Lcom/android/calendar/e/be;

    .line 733
    invoke-virtual {v0}, Lcom/android/calendar/e/be;->getWeekAllDayView()Lcom/android/calendar/e/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    .line 735
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 888
    iget-object v0, p0, Lcom/android/calendar/e/g;->aG:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 891
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(Z)V

    .line 893
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 6252
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->h()V

    .line 6253
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 6254
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2480
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->aK:Z

    if-eqz v0, :cond_0

    .line 2481
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/e/g;->a(II)V

    .line 2482
    iput-boolean v4, p0, Lcom/android/calendar/e/g;->aK:Z

    .line 2484
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2485
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 2486
    sget v0, Lcom/android/calendar/e/g;->b:I

    sput v0, Lcom/android/calendar/e/g;->a:I

    .line 2490
    :goto_0
    iget v0, p0, Lcom/android/calendar/e/g;->by:I

    neg-int v0, v0

    sget v1, Lcom/android/calendar/e/g;->bR:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/g;->bQ:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    .line 2491
    iget v1, p0, Lcom/android/calendar/e/g;->bx:I

    neg-int v1, v1

    iget v2, p0, Lcom/android/calendar/e/g;->bW:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    .line 2493
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2495
    iget-object v2, p0, Lcom/android/calendar/e/g;->au:Landroid/graphics/Rect;

    .line 2496
    iget v3, p0, Lcom/android/calendar/e/g;->bX:I

    int-to-float v3, v3

    sub-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 2497
    iget v3, p0, Lcom/android/calendar/e/g;->bB:I

    int-to-float v3, v3

    sub-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 2498
    iget v3, p0, Lcom/android/calendar/e/g;->bW:I

    int-to-float v3, v3

    sub-float/2addr v3, v1

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 2499
    iget v3, p0, Lcom/android/calendar/e/g;->bC:I

    int-to-float v3, v3

    sub-float/2addr v3, v1

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 2500
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2501
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 2502
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->c(Landroid/graphics/Canvas;)V

    .line 2504
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2506
    neg-float v1, v1

    invoke-virtual {p1, v1, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2507
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->a(Landroid/graphics/Canvas;)V

    .line 2508
    neg-float v0, v0

    invoke-virtual {p1, v5, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2510
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->b(Landroid/graphics/Canvas;)V

    .line 2511
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cl:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cm:Z

    if-eqz v0, :cond_1

    .line 2512
    invoke-direct {p0}, Lcom/android/calendar/e/g;->H()V

    .line 2513
    iput-boolean v4, p0, Lcom/android/calendar/e/g;->cm:Z

    .line 2515
    :cond_1
    iput-boolean v4, p0, Lcom/android/calendar/e/g;->cl:Z

    .line 2516
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2517
    return-void

    .line 2488
    :cond_2
    sget v0, Lcom/android/calendar/e/g;->b:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/calendar/e/g;->a:I

    goto :goto_0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1603
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 1604
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1624
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1607
    :pswitch_0
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    .line 1608
    cmpl-float v2, v1, v3

    if-eqz v2, :cond_0

    .line 1610
    const/high16 v2, -0x40800000    # -1.0f

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getVerticalScrollFactor()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 1612
    iput-boolean v0, p0, Lcom/android/calendar/e/g;->D:Z

    .line 1613
    sget-object v2, Lcom/android/calendar/e/ag;->b:Lcom/android/calendar/e/ag;

    iput-object v2, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 1614
    int-to-float v1, v1

    invoke-virtual {p0, p1, p1, v3, v1}, Lcom/android/calendar/e/g;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 1616
    sget-object v1, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    iput-object v1, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    goto :goto_0

    .line 1604
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1631
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 1632
    iget-object v1, p0, Lcom/android/calendar/e/g;->dS:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1652
    :cond_0
    :goto_0
    return v0

    .line 1635
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/e/g;->ac:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->f()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1638
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dq:Z

    if-eqz v0, :cond_2

    .line 1639
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    .line 1640
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, v0, v2, v1}, Lcom/android/calendar/e/g;->a(IIZ)Z

    .line 1641
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    move v0, v1

    .line 1642
    goto :goto_0

    .line 1645
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1646
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->c(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 1648
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dq:Z

    if-nez v0, :cond_4

    .line 1649
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1652
    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v10, 0x1

    .line 1411
    const/16 v0, 0x42

    if-eq p1, v0, :cond_0

    const/16 v0, 0x16

    if-eq p1, v0, :cond_0

    const/16 v0, 0x15

    if-eq p1, v0, :cond_0

    const/16 v0, 0x13

    if-eq p1, v0, :cond_0

    const/16 v0, 0x14

    if-eq p1, v0, :cond_0

    const/16 v0, 0x17

    if-ne p1, v0, :cond_1

    .line 1414
    :cond_0
    invoke-virtual {p0, v8}, Lcom/android/calendar/e/g;->playSoundEffect(I)V

    .line 1416
    :cond_1
    iget v0, p0, Lcom/android/calendar/e/g;->cP:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v0, :cond_5

    .line 1417
    const/16 v0, 0x42

    if-eq p1, v0, :cond_2

    const/16 v0, 0x16

    if-eq p1, v0, :cond_2

    const/16 v0, 0x15

    if-eq p1, v0, :cond_2

    const/16 v0, 0x13

    if-eq p1, v0, :cond_2

    const/16 v0, 0x14

    if-ne p1, v0, :cond_4

    .line 1422
    :cond_2
    iput v3, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1423
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    move v8, v10

    .line 1598
    :cond_3
    :goto_0
    return v8

    .line 1425
    :cond_4
    const/16 v0, 0x17

    if-ne p1, v0, :cond_5

    .line 1428
    iput v10, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1429
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    move v8, v10

    .line 1430
    goto :goto_0

    .line 1433
    :cond_5
    iget v1, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1434
    iput v3, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1435
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->cQ:Z

    .line 1437
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    .line 1439
    sparse-switch p1, :sswitch_data_0

    .line 1560
    iput v1, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1561
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v8

    goto :goto_0

    .line 1442
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 1443
    if-eqz v0, :cond_3

    .line 1446
    iget-object v1, p0, Lcom/android/calendar/e/g;->aF:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1447
    iput-wide v6, p0, Lcom/android/calendar/e/g;->K:J

    .line 1449
    iget-wide v2, v0, Lcom/android/calendar/dh;->m:J

    .line 1450
    iget-wide v4, v0, Lcom/android/calendar/dh;->n:J

    .line 1451
    iget-wide v6, v0, Lcom/android/calendar/dh;->b:J

    .line 1452
    iget-object v1, p0, Lcom/android/calendar/e/g;->cD:Lcom/android/calendar/cj;

    const/4 v8, -0x1

    invoke-virtual/range {v1 .. v8}, Lcom/android/calendar/cj;->a(JJJI)V

    move v8, v10

    .line 1453
    goto :goto_0

    .line 1456
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_6

    .line 1457
    invoke-direct {p0, v10}, Lcom/android/calendar/e/g;->b(Z)V

    :cond_6
    move v8, v10

    .line 1459
    goto :goto_0

    .line 1461
    :sswitch_2
    iput v1, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1462
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_7

    .line 1463
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    move v8, v10

    .line 1464
    goto :goto_0

    .line 1466
    :cond_7
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v8

    goto :goto_0

    .line 1468
    :sswitch_3
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    if-eqz v1, :cond_9

    .line 1469
    iget-object v1, p0, Lcom/android/calendar/e/g;->cp:Lcom/android/calendar/dh;

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 1470
    iput-object v2, p0, Lcom/android/calendar/e/g;->cp:Lcom/android/calendar/dh;

    .line 1471
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->cq:Z

    move v9, v0

    .line 1564
    :goto_1
    iget v0, p0, Lcom/android/calendar/e/g;->R:I

    if-lt v9, v0, :cond_8

    iget v0, p0, Lcom/android/calendar/e/g;->S:I

    if-le v9, v0, :cond_17

    .line 1566
    :cond_8
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1567
    iget v0, p0, Lcom/android/calendar/e/g;->R:I

    if-ge v9, v0, :cond_16

    .line 1568
    iget v0, v4, Landroid/text/format/Time;->monthDay:I

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    sub-int/2addr v0, v1

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    .line 1572
    :goto_2
    invoke-virtual {v4, v10}, Landroid/text/format/Time;->normalize(Z)J

    .line 1573
    invoke-direct {p0, v9}, Lcom/android/calendar/e/g;->setSelectedDay(I)V

    .line 1575
    invoke-direct {p0, p0}, Lcom/android/calendar/e/g;->H(Lcom/android/calendar/e/g;)V

    .line 1577
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1578
    iget v0, v5, Landroid/text/format/Time;->monthDay:I

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 1579
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    move v8, v10

    .line 1580
    goto/16 :goto_0

    .line 1475
    :cond_9
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v1, :cond_a

    .line 1476
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-object v1, v1, Lcom/android/calendar/dh;->H:Lcom/android/calendar/dh;

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 1478
    :cond_a
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v1, :cond_b

    .line 1479
    iput-wide v6, p0, Lcom/android/calendar/e/g;->K:J

    .line 1480
    add-int/lit8 v0, v0, -0x1

    :cond_b
    move v9, v0

    .line 1483
    goto :goto_1

    .line 1486
    :sswitch_4
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->cq:Z

    if-eqz v1, :cond_c

    .line 1487
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 1488
    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 1489
    iput-object v2, p0, Lcom/android/calendar/e/g;->cp:Lcom/android/calendar/dh;

    .line 1490
    iput-wide v6, p0, Lcom/android/calendar/e/g;->K:J

    .line 1491
    add-int/lit8 v0, v0, 0x1

    move v9, v0

    .line 1493
    goto :goto_1

    .line 1496
    :cond_c
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v1, :cond_e

    .line 1497
    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    if-ne v1, v10, :cond_d

    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    invoke-virtual {v1}, Lcom/android/calendar/dh;->d()I

    move-result v1

    sget v2, Lcom/android/calendar/e/g;->a:I

    if-le v1, v2, :cond_d

    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    invoke-virtual {v1}, Lcom/android/calendar/dh;->c()I

    move-result v1

    sget v2, Lcom/android/calendar/e/g;->a:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_d

    .line 1500
    iput-boolean v10, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 1501
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iput-object v1, p0, Lcom/android/calendar/e/g;->cp:Lcom/android/calendar/dh;

    .line 1502
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-object v1, v1, Lcom/android/calendar/dh;->G:Lcom/android/calendar/dh;

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 1503
    iput-wide v6, p0, Lcom/android/calendar/e/g;->K:J

    move v9, v0

    .line 1505
    goto/16 :goto_1

    .line 1507
    :cond_d
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-object v1, v1, Lcom/android/calendar/dh;->G:Lcom/android/calendar/dh;

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 1509
    :cond_e
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v1, :cond_f

    .line 1510
    iput-wide v6, p0, Lcom/android/calendar/e/g;->K:J

    .line 1511
    add-int/lit8 v0, v0, 0x1

    :cond_f
    move v9, v0

    .line 1514
    goto/16 :goto_1

    .line 1517
    :sswitch_5
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 1518
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v1, :cond_10

    .line 1519
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-object v1, v1, Lcom/android/calendar/dh;->I:Lcom/android/calendar/dh;

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 1521
    :cond_10
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v1, :cond_12

    .line 1522
    iput-wide v6, p0, Lcom/android/calendar/e/g;->K:J

    .line 1523
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->ak:Z

    if-nez v1, :cond_12

    .line 1524
    iget v1, p0, Lcom/android/calendar/e/g;->aj:I

    if-nez v1, :cond_11

    .line 1525
    const/16 v1, 0x21

    invoke-virtual {p0, v1}, Lcom/android/calendar/e/g;->focusSearch(I)Landroid/view/View;

    move-result-object v1

    .line 1526
    if-eqz v1, :cond_11

    .line 1527
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 1528
    iput v8, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1531
    :cond_11
    iget v1, p0, Lcom/android/calendar/e/g;->aj:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 1532
    invoke-direct {p0}, Lcom/android/calendar/e/g;->A()V

    .line 1533
    iget-object v1, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1534
    iput-boolean v10, p0, Lcom/android/calendar/e/g;->cl:Z

    :cond_12
    move v9, v0

    .line 1538
    goto/16 :goto_1

    .line 1541
    :sswitch_6
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->cq:Z

    .line 1542
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-eqz v1, :cond_13

    .line 1543
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    iget-object v1, v1, Lcom/android/calendar/dh;->J:Lcom/android/calendar/dh;

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedEvent(Lcom/android/calendar/dh;)V

    .line 1545
    :cond_13
    iget-object v1, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v1, :cond_14

    .line 1546
    iput-wide v6, p0, Lcom/android/calendar/e/g;->K:J

    .line 1547
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->ak:Z

    if-eqz v1, :cond_15

    .line 1548
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->ak:Z

    :cond_14
    :goto_3
    move v9, v0

    .line 1557
    goto/16 :goto_1

    .line 1550
    :cond_15
    iget v1, p0, Lcom/android/calendar/e/g;->aj:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->setSelectedHour(I)V

    .line 1551
    invoke-direct {p0}, Lcom/android/calendar/e/g;->A()V

    .line 1552
    iget-object v1, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1553
    iput-boolean v10, p0, Lcom/android/calendar/e/g;->cl:Z

    goto :goto_3

    .line 1570
    :cond_16
    iget v0, v4, Landroid/text/format/Time;->monthDay:I

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    add-int/2addr v0, v1

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    goto/16 :goto_2

    .line 1582
    :cond_17
    iget v0, p0, Lcom/android/calendar/e/g;->ai:I

    if-eq v0, v9, :cond_18

    .line 1583
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1584
    invoke-virtual {v4, v9}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1585
    iget v0, p0, Lcom/android/calendar/e/g;->aj:I

    iput v0, v4, Landroid/text/format/Time;->hour:I

    .line 1586
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 1588
    :cond_18
    invoke-direct {p0, v9}, Lcom/android/calendar/e/g;->setSelectedDay(I)V

    .line 1589
    iget-object v0, p0, Lcom/android/calendar/e/g;->ck:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1590
    iput-boolean v10, p0, Lcom/android/calendar/e/g;->cl:Z

    .line 1591
    iput-boolean v10, p0, Lcom/android/calendar/e/g;->cm:Z

    .line 1594
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    move v8, v10

    .line 1595
    goto/16 :goto_0

    .line 1439
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x13 -> :sswitch_5
        0x14 -> :sswitch_6
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
        0x17 -> :sswitch_1
        0x42 -> :sswitch_1
        0x43 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1360
    iput-boolean v1, p0, Lcom/android/calendar/e/g;->cQ:Z

    .line 1361
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 1363
    sparse-switch p1, :sswitch_data_0

    .line 1406
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 1365
    :sswitch_0
    iget v1, p0, Lcom/android/calendar/e/g;->cP:I

    if-eqz v1, :cond_0

    .line 1370
    iget v1, p0, Lcom/android/calendar/e/g;->cP:I

    if-ne v1, v0, :cond_1

    .line 1375
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1376
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    goto :goto_0

    .line 1381
    :cond_1
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 1382
    iget-object v1, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    instance-of v1, v1, Lcom/android/calendar/AllInOneActivity;

    if-eqz v1, :cond_0

    .line 1383
    invoke-direct {p0, v0}, Lcom/android/calendar/e/g;->b(Z)V

    goto :goto_0

    .line 1386
    :cond_2
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/e/g;->cP:I

    .line 1387
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 1388
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->performLongClick()Z

    goto :goto_0

    .line 1392
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v2, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-eq v1, v2, :cond_0

    .line 1393
    sget-object v1, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    iput-object v1, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 1394
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    goto :goto_1

    .line 1401
    :sswitch_2
    invoke-direct {p0, v1}, Lcom/android/calendar/e/g;->c(Z)V

    goto :goto_0

    .line 1363
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x17 -> :sswitch_0
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 6328
    const/4 v0, 0x1

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    .line 1146
    iput p1, p0, Lcom/android/calendar/e/g;->bC:I

    .line 1147
    iput p2, p0, Lcom/android/calendar/e/g;->bB:I

    .line 1148
    iget v0, p0, Lcom/android/calendar/e/g;->bW:I

    sub-int v1, p1, v0

    .line 1149
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    mul-int/lit8 v0, v0, 0x1

    sub-int v0, v1, v0

    iget v2, p0, Lcom/android/calendar/e/g;->i:I

    div-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/e/g;->ar:I

    .line 1150
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    mul-int/lit8 v0, v0, 0x1

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/e/g;->x:I

    .line 1151
    if-eq p1, p3, :cond_3

    const/4 v0, 0x1

    .line 1152
    :goto_0
    sget v2, Lcom/android/calendar/e/g;->as:I

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->cg:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_1

    .line 1153
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/g;->i:I

    mul-int/lit8 v0, v0, 0x1

    sub-int v0, v1, v0

    iget v1, p0, Lcom/android/calendar/e/g;->i:I

    div-int/2addr v0, v1

    sput v0, Lcom/android/calendar/e/g;->as:I

    .line 1155
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cg:Z

    if-eqz v0, :cond_2

    .line 1156
    sget v0, Lcom/android/calendar/e/g;->as:I

    iget v1, p0, Lcom/android/calendar/e/g;->x:I

    if-le v0, v1, :cond_4

    .line 1157
    iget v0, p0, Lcom/android/calendar/e/g;->x:I

    sput v0, Lcom/android/calendar/e/g;->as:I

    .line 1164
    :cond_2
    :goto_1
    div-int/lit8 v0, p1, 0x7

    sput v0, Lcom/android/calendar/e/g;->L:I

    .line 1166
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 1167
    sget v1, Lcom/android/calendar/e/g;->aU:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1168
    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/g;->bP:I

    .line 1169
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/e/g;->a(II)V

    .line 1170
    return-void

    .line 1151
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1158
    :cond_4
    sget v0, Lcom/android/calendar/e/g;->as:I

    iget v1, p0, Lcom/android/calendar/e/g;->ar:I

    if-ge v0, v1, :cond_2

    .line 1159
    iget v0, p0, Lcom/android/calendar/e/g;->ar:I

    sput v0, Lcom/android/calendar/e/g;->as:I

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v13, 0x1

    const/4 v8, 0x0

    .line 5561
    if-nez p1, :cond_1

    .line 5885
    :cond_0
    :goto_0
    return v8

    .line 5564
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 5565
    invoke-direct {p0}, Lcom/android/calendar/e/g;->P()Z

    move-result v1

    if-eqz v1, :cond_2b

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2b

    move v1, v13

    .line 5568
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 5570
    if-eqz v0, :cond_0

    .line 5574
    sparse-switch v1, :sswitch_data_0

    .line 5885
    iget-object v0, p0, Lcom/android/calendar/e/g;->dh:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    move v8, v13

    goto :goto_0

    .line 5576
    :sswitch_0
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->ec:Z

    .line 5577
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->D:Z

    .line 5579
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/g;->a(F)V

    .line 5580
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->dx:Z

    .line 5581
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->bO:Z

    .line 5582
    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5583
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->W:Z

    if-eqz v0, :cond_3

    .line 5584
    iput v13, p0, Lcom/android/calendar/e/g;->cP:I

    .line 5587
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_4

    .line 5588
    iget-object v0, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->h()V

    .line 5590
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/g;->dh:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 5591
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_6

    .line 5592
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1, v8}, Lcom/android/calendar/e/g;->a(IIZ)Z

    .line 5593
    iget-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    if-nez v0, :cond_5

    .line 5594
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cn:Lcom/android/calendar/dh;

    .line 5597
    :cond_5
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->h(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 5598
    sget-object v0, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 5599
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 5600
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dT:I

    .line 5601
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dU:I

    .line 5602
    iget v1, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dV:I

    .line 5603
    iget v1, v0, Lcom/android/calendar/dh;->E:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dW:I

    .line 5604
    iget v1, v0, Lcom/android/calendar/dh;->F:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dX:I

    .line 5605
    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dB:J

    :cond_6
    :goto_2
    move v8, v13

    .line 5618
    goto/16 :goto_0

    .line 5606
    :cond_7
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->g(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5607
    sget-object v0, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 5608
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 5609
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dT:I

    .line 5610
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dU:I

    .line 5611
    iget v1, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dV:I

    .line 5612
    iget v1, v0, Lcom/android/calendar/dh;->E:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dW:I

    .line 5613
    iget v1, v0, Lcom/android/calendar/dh;->F:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dX:I

    .line 5614
    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dB:J

    goto :goto_2

    .line 5622
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 5623
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 5624
    invoke-direct {p0, v0, v1, v8}, Lcom/android/calendar/e/g;->a(IIZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5627
    invoke-direct {p0}, Lcom/android/calendar/e/g;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5630
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cQ:Z

    if-nez v0, :cond_0

    .line 5634
    iget v0, p0, Lcom/android/calendar/e/g;->bJ:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/calendar/e/g;->bI:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_0

    .line 5637
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->ec:Z

    .line 5638
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->D:Z

    .line 5639
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->dx:Z

    .line 5640
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->bO:Z

    .line 5642
    iput v2, p0, Lcom/android/calendar/e/g;->cP:I

    .line 5644
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTime()Landroid/text/format/Time;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 5645
    if-nez v0, :cond_8

    .line 5646
    invoke-direct {p0}, Lcom/android/calendar/e/g;->I()V

    .line 5647
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedEvent()Lcom/android/calendar/dh;

    move-result-object v0

    .line 5648
    invoke-direct {p0}, Lcom/android/calendar/e/g;->getCurrentSelectionPosition()Landroid/graphics/Rect;

    move-result-object v1

    .line 5649
    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iput v2, v0, Lcom/android/calendar/dh;->C:F

    .line 5650
    iget v2, v1, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iput v2, v0, Lcom/android/calendar/dh;->D:F

    .line 5651
    iget v2, v1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iput v2, v0, Lcom/android/calendar/dh;->E:F

    .line 5652
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iput v1, v0, Lcom/android/calendar/dh;->F:F

    .line 5654
    iput-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    .line 5655
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dT:I

    .line 5656
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dU:I

    .line 5657
    iget v1, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dV:I

    .line 5658
    iget v1, v0, Lcom/android/calendar/dh;->E:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dW:I

    .line 5659
    iget v1, v0, Lcom/android/calendar/dh;->F:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/g;->dX:I

    .line 5660
    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v0, p0, Lcom/android/calendar/e/g;->dB:J

    .line 5661
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->dy:Z

    .line 5681
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/e/g;->dh:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move v8, v13

    .line 5682
    goto/16 :goto_0

    .line 5663
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->i()V

    .line 5664
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v2

    .line 5665
    const-wide/32 v4, 0x36ee80

    add-long v6, v2, v4

    .line 5667
    new-instance v4, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 5668
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 5669
    invoke-virtual {v4, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 5670
    invoke-virtual {v5, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 5671
    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    .line 5672
    const v0, 0x24dc87

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    .line 5676
    :goto_4
    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    .line 5677
    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 5678
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const-wide/16 v9, 0x1

    move-object v1, p0

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_3

    .line 5674
    :cond_9
    const v0, 0x259d23

    invoke-static {v4, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    goto :goto_4

    .line 5686
    :sswitch_2
    iget-boolean v1, p0, Lcom/android/calendar/e/g;->dy:Z

    if-eqz v1, :cond_f

    .line 5687
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v2, v1

    .line 5688
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    .line 5690
    iget-object v3, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->l()Z

    move-result v3

    if-eqz v3, :cond_2a

    iget-object v3, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->m()Z

    move-result v3

    if-nez v3, :cond_2a

    .line 5691
    invoke-static {v0}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v0

    .line 5692
    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    .line 5693
    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v1, v0

    move v1, v2

    .line 5696
    :goto_5
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v2, :cond_0

    .line 5699
    iget v2, p0, Lcom/android/calendar/e/g;->by:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    add-float/2addr v2, v3

    sget v3, Lcom/android/calendar/e/g;->bR:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/g;->bQ:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 5700
    iget-object v3, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v3, v3, Lcom/android/calendar/dh;->F:F

    cmpl-float v3, v2, v3

    if-lez v3, :cond_c

    .line 5701
    sget-object v2, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    iput-object v2, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    .line 5706
    :cond_a
    :goto_6
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->i(Landroid/view/MotionEvent;)V

    .line 5708
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v2, :cond_d

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->cX:Z

    if-nez v2, :cond_d

    .line 5709
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {p0, v2, v1, v0}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;II)V

    .line 5792
    :cond_b
    :goto_7
    iget-object v0, p0, Lcom/android/calendar/e/g;->dh:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move v8, v13

    .line 5793
    goto/16 :goto_0

    .line 5702
    :cond_c
    iget-object v3, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v3, v3, Lcom/android/calendar/dh;->E:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_a

    .line 5703
    sget-object v2, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    iput-object v2, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    goto :goto_6

    .line 5710
    :cond_d
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v2, :cond_b

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->cX:Z

    if-eqz v2, :cond_b

    .line 5711
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget-object v3, p0, Lcom/android/calendar/e/g;->cr:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iput v3, v2, Lcom/android/calendar/dh;->E:F

    .line 5712
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/dh;)V

    .line 5713
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    iget-wide v4, p0, Lcom/android/calendar/e/g;->dd:J

    iget-wide v6, p0, Lcom/android/calendar/e/g;->de:J

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/android/calendar/e/am;->a(JJ)V

    .line 5714
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2}, Lcom/android/calendar/e/am;->e()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/android/calendar/e/g;->bD:I

    sub-int/2addr v2, v3

    if-ge v0, v2, :cond_e

    iget-object v2, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->l()Z

    move-result v2

    if-nez v2, :cond_e

    .line 5715
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2}, Lcom/android/calendar/e/am;->e()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/android/calendar/e/g;->bD:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/e/am;->b(II)V

    goto :goto_7

    .line 5717
    :cond_e
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2, v1, v0}, Lcom/android/calendar/e/am;->b(II)V

    goto :goto_7

    .line 5720
    :cond_f
    invoke-direct {p0}, Lcom/android/calendar/e/g;->P()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 5721
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v2, v1

    .line 5722
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    .line 5724
    iget-object v3, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->l()Z

    move-result v3

    if-eqz v3, :cond_29

    iget-object v3, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->m()Z

    move-result v3

    if-nez v3, :cond_29

    .line 5725
    invoke-static {v0}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v0

    .line 5726
    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    .line 5727
    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v1, v0

    move v1, v2

    .line 5729
    :goto_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_13

    invoke-direct {p0}, Lcom/android/calendar/e/g;->M()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 5730
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->H:Z

    .line 5731
    iget-boolean v2, p0, Lcom/android/calendar/e/g;->I:Z

    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    if-eqz v2, :cond_10

    .line 5732
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->D:F

    iget-object v3, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v3, v3, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v2, v3

    .line 5733
    iget v3, p0, Lcom/android/calendar/e/g;->dV:I

    iget v4, p0, Lcom/android/calendar/e/g;->dY:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    .line 5734
    add-float/2addr v2, v3

    .line 5735
    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 5736
    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->b(F)I

    move-result v2

    .line 5737
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 5738
    iget-object v4, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v3, v4}, Lcom/android/calendar/e/g;->a(Landroid/graphics/Rect;Lcom/android/calendar/dh;)Landroid/graphics/Rect;

    .line 5739
    iget-object v4, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v5, v4, Lcom/android/calendar/dh;->C:F

    iget v6, v3, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, v4, Lcom/android/calendar/dh;->C:F

    .line 5740
    iget-object v4, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    iget v5, v4, Lcom/android/calendar/dh;->D:F

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    add-float/2addr v3, v5

    iput v3, v4, Lcom/android/calendar/dh;->D:F

    .line 5741
    iget-object v3, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    iget-object v4, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {v3, p1, v4, v2}, Lcom/android/calendar/e/ao;->a(Landroid/view/MotionEvent;Lcom/android/calendar/dh;I)V

    .line 5744
    :cond_10
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2}, Lcom/android/calendar/e/am;->a()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 5745
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2}, Lcom/android/calendar/e/am;->d()V

    .line 5758
    :cond_11
    :goto_9
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v2, :cond_12

    .line 5759
    invoke-direct {p0, p1}, Lcom/android/calendar/e/g;->i(Landroid/view/MotionEvent;)V

    .line 5762
    :cond_12
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v2, :cond_17

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->cX:Z

    if-nez v2, :cond_17

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->H:Z

    if-nez v2, :cond_17

    .line 5763
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {p0, v2, v1, v0}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;II)V

    goto/16 :goto_7

    .line 5749
    :cond_13
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2}, Lcom/android/calendar/e/am;->a()Z

    move-result v2

    if-nez v2, :cond_15

    .line 5750
    :cond_14
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-virtual {p0, v2, v1, v0}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/dh;II)V

    .line 5752
    :cond_15
    iget-boolean v2, p0, Lcom/android/calendar/e/g;->H:Z

    if-eqz v2, :cond_16

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->I:Z

    if-nez v2, :cond_16

    .line 5753
    iget-object v2, p0, Lcom/android/calendar/e/g;->aH:Lcom/android/calendar/e/ao;

    invoke-virtual {v2, p1}, Lcom/android/calendar/e/ao;->c(Landroid/view/MotionEvent;)V

    .line 5755
    :cond_16
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->H:Z

    goto :goto_9

    .line 5764
    :cond_17
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    if-eqz v2, :cond_b

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->cX:Z

    if-eqz v2, :cond_b

    iget-boolean v2, p0, Lcom/android/calendar/e/g;->H:Z

    if-nez v2, :cond_b

    .line 5765
    iget-object v2, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, v2}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/dh;)V

    .line 5766
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    iget-wide v4, p0, Lcom/android/calendar/e/g;->dd:J

    iget-wide v6, p0, Lcom/android/calendar/e/g;->de:J

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/android/calendar/e/am;->a(JJ)V

    .line 5767
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2}, Lcom/android/calendar/e/am;->e()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/android/calendar/e/g;->bD:I

    sub-int/2addr v2, v3

    if-ge v0, v2, :cond_18

    iget-object v2, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->l()Z

    move-result v2

    if-nez v2, :cond_18

    .line 5768
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2}, Lcom/android/calendar/e/am;->e()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/android/calendar/e/g;->bD:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/e/am;->b(II)V

    .line 5769
    iget-object v0, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v0}, Lcom/android/calendar/e/am;->e()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/android/calendar/e/g;->bD:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/e/g;->cU:I

    .line 5774
    :goto_a
    iput v1, p0, Lcom/android/calendar/e/g;->cT:I

    .line 5775
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_b

    .line 5776
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 5777
    iget-object v1, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v1, :cond_b

    .line 5778
    iget v1, p0, Lcom/android/calendar/e/g;->bC:I

    iget v2, p0, Lcom/android/calendar/e/g;->dJ:I

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_19

    .line 5779
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ej:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5780
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ek:Ljava/lang/Runnable;

    iget v2, p0, Lcom/android/calendar/e/g;->dI:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_7

    .line 5771
    :cond_18
    iget-object v2, p0, Lcom/android/calendar/e/g;->cZ:Lcom/android/calendar/e/am;

    invoke-virtual {v2, v1, v0}, Lcom/android/calendar/e/am;->b(II)V

    .line 5772
    iput v0, p0, Lcom/android/calendar/e/g;->cU:I

    goto :goto_a

    .line 5781
    :cond_19
    iget v1, p0, Lcom/android/calendar/e/g;->dJ:I

    if-ge v0, v1, :cond_1a

    .line 5782
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ek:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5783
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ej:Ljava/lang/Runnable;

    iget v2, p0, Lcom/android/calendar/e/g;->dI:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_7

    .line 5785
    :cond_1a
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ej:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5786
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ek:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/16 :goto_7

    .line 5797
    :sswitch_3
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->dy:Z

    if-eqz v0, :cond_1f

    .line 5798
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/g;->e:Landroid/text/format/Time;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 5799
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 5800
    iget-wide v0, p0, Lcom/android/calendar/e/g;->dd:J

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 5801
    iget-wide v0, p0, Lcom/android/calendar/e/g;->de:J

    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 5803
    invoke-static {v4}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-nez v0, :cond_1b

    iget-boolean v0, p0, Lcom/android/calendar/e/g;->W:Z

    if-eqz v0, :cond_1b

    .line 5804
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v2, :cond_1e

    iget-object v0, p0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 5805
    invoke-direct {p0, v4, v5}, Lcom/android/calendar/e/g;->a(Landroid/text/format/Time;Landroid/text/format/Time;)V

    .line 5818
    :cond_1b
    :goto_b
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_1c

    .line 5819
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ej:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5820
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->ek:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5822
    :cond_1c
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->D:Z

    .line 5823
    iget-object v0, p0, Lcom/android/calendar/e/g;->dh:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 5824
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cX:Z

    if-eqz v0, :cond_1d

    .line 5825
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_1d

    .line 5826
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5827
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5831
    :cond_1d
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->bO:Z

    if-nez v0, :cond_21

    .line 5832
    iput-boolean v13, p0, Lcom/android/calendar/e/g;->bO:Z

    .line 5833
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    move v8, v13

    .line 5834
    goto/16 :goto_0

    .line 5807
    :cond_1e
    iget-object v0, p0, Lcom/android/calendar/e/g;->df:Lcom/android/calendar/al;

    const-wide/32 v2, 0x40000000

    const-wide/16 v6, -0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 5808
    invoke-direct {p0}, Lcom/android/calendar/e/g;->O()V

    .line 5809
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->dy:Z

    goto :goto_b

    .line 5813
    :cond_1f
    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_20

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    if-eq v0, v1, :cond_20

    iget-object v0, p0, Lcom/android/calendar/e/g;->cO:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_1b

    .line 5814
    :cond_20
    iget-object v0, p0, Lcom/android/calendar/e/g;->dA:Lcom/android/calendar/dh;

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/g;->b(Landroid/view/MotionEvent;Lcom/android/calendar/dh;)V

    goto :goto_b

    .line 5836
    :cond_21
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->C:Z

    if-eqz v0, :cond_22

    move v8, v13

    .line 5837
    goto/16 :goto_0

    .line 5839
    :cond_22
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->dx:Z

    .line 5840
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->g()V

    .line 5842
    iget-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    sget-object v1, Lcom/android/calendar/e/ag;->d:Lcom/android/calendar/e/ag;

    if-ne v0, v1, :cond_26

    .line 5843
    sget-object v0, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    iput-object v0, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 5844
    iget v0, p0, Lcom/android/calendar/e/g;->bx:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lcom/android/calendar/e/g;->L:I

    if-le v0, v1, :cond_24

    .line 5846
    sget-boolean v0, Lcom/android/calendar/e/g;->s:Z

    if-eqz v0, :cond_23

    .line 5847
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v1, "- horizontal scroll: switch views"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    :cond_23
    move v8, v13

    .line 5850
    goto/16 :goto_0

    .line 5855
    :cond_24
    sget-boolean v0, Lcom/android/calendar/e/g;->s:Z

    if-eqz v0, :cond_25

    .line 5856
    sget-object v0, Lcom/android/calendar/e/g;->r:Ljava/lang/String;

    const-string v1, "- horizontal scroll: snap back"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 5858
    :cond_25
    invoke-direct {p0}, Lcom/android/calendar/e/g;->u()V

    .line 5859
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 5865
    :cond_26
    iget-boolean v0, p0, Lcom/android/calendar/e/g;->cQ:Z

    if-eqz v0, :cond_27

    .line 5866
    iput-boolean v8, p0, Lcom/android/calendar/e/g;->cQ:Z

    .line 5867
    invoke-direct {p0, v8}, Lcom/android/calendar/e/g;->d(Z)V

    .line 5868
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    :cond_27
    move v8, v13

    .line 5871
    goto/16 :goto_0

    .line 5875
    :sswitch_4
    iget-object v0, p0, Lcom/android/calendar/e/g;->dh:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 5877
    invoke-direct {p0, v8}, Lcom/android/calendar/e/g;->d(Z)V

    .line 5878
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    if-eqz v0, :cond_28

    .line 5879
    iget-object v0, p0, Lcom/android/calendar/e/g;->E:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/g;->aJ:Lcom/android/calendar/e/aa;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_28
    move v8, v13

    .line 5882
    goto/16 :goto_0

    :cond_29
    move v0, v1

    move v1, v2

    goto/16 :goto_8

    :cond_2a
    move v0, v1

    move v1, v2

    goto/16 :goto_5

    :cond_2b
    move v1, v0

    goto/16 :goto_1

    .line 5574
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_2
        0x3 -> :sswitch_4
        0x5 -> :sswitch_1
        0xd3 -> :sswitch_1
        0xd4 -> :sswitch_3
        0xd5 -> :sswitch_2
    .end sparse-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 6178
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    .line 6179
    if-nez p1, :cond_0

    .line 6180
    invoke-direct {p0}, Lcom/android/calendar/e/g;->J()V

    .line 6182
    :cond_0
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 6531
    const/16 v0, 0x40

    if-ne p1, v0, :cond_0

    .line 6532
    iget-object v0, p0, Lcom/android/calendar/e/g;->aq:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/g;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 6534
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public setAnimtingDay(I)V
    .locals 0

    .prologue
    .line 2606
    iput p1, p0, Lcom/android/calendar/e/g;->dO:I

    .line 2607
    return-void
.end method

.method public setAnimtionValue(F)V
    .locals 0

    .prologue
    .line 2602
    iput p1, p0, Lcom/android/calendar/e/g;->dN:F

    .line 2603
    return-void
.end method

.method public setCellHeight(I)V
    .locals 0

    .prologue
    .line 2613
    sput p1, Lcom/android/calendar/e/g;->bE:I

    .line 2614
    return-void
.end method

.method setFirstVisibleHour(I)V
    .locals 0

    .prologue
    .line 985
    invoke-virtual {p0, p1}, Lcom/android/calendar/e/g;->a(I)V

    .line 986
    return-void
.end method

.method public setPause(Z)V
    .locals 0

    .prologue
    .line 6497
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->c:Z

    .line 6498
    return-void
.end method

.method public setSelectionMode(I)V
    .locals 0

    .prologue
    .line 6378
    iput p1, p0, Lcom/android/calendar/e/g;->cP:I

    .line 6379
    return-void
.end method

.method public setStartingScroll(Z)V
    .locals 0

    .prologue
    .line 6518
    iput-boolean p1, p0, Lcom/android/calendar/e/g;->D:Z

    .line 6519
    return-void
.end method

.method public setTouchMode(Lcom/android/calendar/e/ag;)V
    .locals 0

    .prologue
    .line 6510
    iput-object p1, p0, Lcom/android/calendar/e/g;->cN:Lcom/android/calendar/e/ag;

    .line 6511
    return-void
.end method

.method public setViewScrollY(I)V
    .locals 0

    .prologue
    .line 2621
    iput p1, p0, Lcom/android/calendar/e/g;->by:I

    .line 2622
    invoke-direct {p0}, Lcom/android/calendar/e/g;->z()V

    .line 2623
    return-void
.end method

.method public setViewStartY(I)V
    .locals 1

    .prologue
    .line 1022
    iget v0, p0, Lcom/android/calendar/e/g;->bA:I

    if-le p1, v0, :cond_0

    .line 1023
    iget p1, p0, Lcom/android/calendar/e/g;->bA:I

    .line 1026
    :cond_0
    iput p1, p0, Lcom/android/calendar/e/g;->by:I

    .line 1028
    invoke-direct {p0}, Lcom/android/calendar/e/g;->z()V

    .line 1029
    invoke-virtual {p0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 1030
    return-void
.end method
