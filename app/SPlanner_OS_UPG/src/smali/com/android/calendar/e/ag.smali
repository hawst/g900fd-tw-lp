.class public final enum Lcom/android/calendar/e/ag;
.super Ljava/lang/Enum;
.source "DayView.java"


# static fields
.field public static final enum a:Lcom/android/calendar/e/ag;

.field public static final enum b:Lcom/android/calendar/e/ag;

.field public static final enum c:Lcom/android/calendar/e/ag;

.field public static final enum d:Lcom/android/calendar/e/ag;

.field private static final synthetic e:[Lcom/android/calendar/e/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 476
    new-instance v0, Lcom/android/calendar/e/ag;

    const-string v1, "INITIAL_STATE"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/e/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    .line 477
    new-instance v0, Lcom/android/calendar/e/ag;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/e/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ag;->b:Lcom/android/calendar/e/ag;

    .line 478
    new-instance v0, Lcom/android/calendar/e/ag;

    const-string v1, "VSCROLL"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/e/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ag;->c:Lcom/android/calendar/e/ag;

    .line 479
    new-instance v0, Lcom/android/calendar/e/ag;

    const-string v1, "HSCROLL"

    invoke-direct {v0, v1, v5}, Lcom/android/calendar/e/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ag;->d:Lcom/android/calendar/e/ag;

    .line 475
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/calendar/e/ag;

    sget-object v1, Lcom/android/calendar/e/ag;->a:Lcom/android/calendar/e/ag;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/e/ag;->b:Lcom/android/calendar/e/ag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/e/ag;->c:Lcom/android/calendar/e/ag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/calendar/e/ag;->d:Lcom/android/calendar/e/ag;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/e/ag;->e:[Lcom/android/calendar/e/ag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 475
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/e/ag;
    .locals 1

    .prologue
    .line 475
    const-class v0, Lcom/android/calendar/e/ag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/e/ag;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/e/ag;
    .locals 1

    .prologue
    .line 475
    sget-object v0, Lcom/android/calendar/e/ag;->e:[Lcom/android/calendar/e/ag;

    invoke-virtual {v0}, [Lcom/android/calendar/e/ag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/e/ag;

    return-object v0
.end method
