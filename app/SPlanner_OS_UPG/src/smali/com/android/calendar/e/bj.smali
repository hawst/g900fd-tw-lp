.class Lcom/android/calendar/e/bj;
.super Landroid/content/AsyncQueryHandler;
.source "WeekLayout.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/e/be;


# direct methods
.method public constructor <init>(Lcom/android/calendar/e/be;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    .line 171
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 172
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 176
    invoke-super {p0, p1, p2, p3}, Landroid/content/AsyncQueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 177
    if-nez p3, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 231
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 182
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->c(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_3

    .line 183
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/android/calendar/e/be;->a(Lcom/android/calendar/e/be;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 188
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->a(Lcom/android/calendar/e/be;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->c(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v1}, Lcom/android/calendar/e/be;->a(Lcom/android/calendar/e/be;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v2}, Lcom/android/calendar/e/be;->d(Lcom/android/calendar/e/be;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;Landroid/content/Context;I)V

    .line 195
    :goto_3
    new-instance v2, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->a(Lcom/android/calendar/e/be;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v1}, Lcom/android/calendar/e/be;->e(Lcom/android/calendar/e/be;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    iget-object v0, v0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 197
    invoke-static {v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 198
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 199
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v3}, Lcom/android/calendar/e/be;->f(Lcom/android/calendar/e/be;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 200
    invoke-static {v2}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 203
    :cond_2
    sget-object v3, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    .line 204
    const-wide/32 v4, 0x5265c00

    invoke-static {}, Lcom/android/calendar/e/be;->g()I

    move-result v2

    int-to-long v6, v2

    mul-long/2addr v4, v6

    add-long/2addr v4, v0

    .line 205
    iget-object v2, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v2}, Lcom/android/calendar/e/be;->a(Lcom/android/calendar/e/be;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1, v4, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v5

    .line 206
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->g(Lcom/android/calendar/e/be;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 207
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->g(Lcom/android/calendar/e/be;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/hc;->o:[Ljava/lang/String;

    const-string v7, " due_date ASC, importance DESC"

    move v1, v8

    move-object v4, v9

    move-object v6, v9

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->c(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_2

    .line 191
    :cond_4
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 192
    invoke-interface {p3}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 193
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->c(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v1}, Lcom/android/calendar/e/be;->a(Lcom/android/calendar/e/be;)Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v1}, Lcom/android/calendar/e/be;->d(Lcom/android/calendar/e/be;)I

    move-result v3

    iget-object v1, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v1}, Lcom/android/calendar/e/be;->d(Lcom/android/calendar/e/be;)I

    move-result v1

    invoke-static {}, Lcom/android/calendar/e/be;->g()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v4, v1, -0x1

    const/4 v5, 0x0

    move-object v1, p3

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;IIZ)V

    goto/16 :goto_3

    .line 212
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->h(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 213
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->h(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 217
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->a(Lcom/android/calendar/e/be;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 218
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-virtual {v0}, Lcom/android/calendar/e/be;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v1}, Lcom/android/calendar/e/be;->h(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v2}, Lcom/android/calendar/e/be;->d(Lcom/android/calendar/e/be;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hc;->a(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 222
    :goto_5
    if-eqz p3, :cond_5

    .line 223
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 225
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->i(Lcom/android/calendar/e/be;)V

    goto/16 :goto_1

    .line 215
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/android/calendar/e/be;->b(Lcom/android/calendar/e/be;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    goto :goto_4

    .line 220
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v0}, Lcom/android/calendar/e/be;->a(Lcom/android/calendar/e/be;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/bj;->a:Lcom/android/calendar/e/be;

    invoke-static {v1}, Lcom/android/calendar/e/be;->h(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, p3, v1}, Lcom/android/calendar/hc;->a(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/ArrayList;)V

    goto :goto_5

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
