.class public Lcom/android/calendar/e/be;
.super Lcom/android/calendar/e/a;
.source "WeekLayout.java"


# static fields
.field private static F:Z

.field private static final o:Ljava/lang/String;

.field private static p:I


# instance fields
.field private A:Ljava/util/ArrayList;

.field private B:Landroid/content/AsyncQueryHandler;

.field private C:Lcom/android/calendar/e/ao;

.field private D:Landroid/content/Context;

.field private E:Lcom/android/calendar/al;

.field private G:I

.field private final H:Z

.field private I:Ljava/lang/Runnable;

.field protected n:[I

.field private q:Landroid/text/format/Time;

.field private r:Landroid/text/format/Time;

.field private s:I

.field private t:I

.field private u:I

.field private v:Ljava/util/ArrayList;

.field private w:Ljava/util/ArrayList;

.field private x:Ljava/util/ArrayList;

.field private y:Ljava/util/ArrayList;

.field private z:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/android/calendar/e/be;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/e/be;->o:Ljava/lang/String;

    .line 58
    const/4 v0, 0x7

    sput v0, Lcom/android/calendar/e/be;->p:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/al;Lcom/android/calendar/dj;Lcom/android/calendar/e/d;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 118
    invoke-direct/range {p0 .. p5}, Lcom/android/calendar/e/a;-><init>(Landroid/content/Context;Lcom/android/calendar/al;Lcom/android/calendar/dj;Lcom/android/calendar/e/d;I)V

    .line 66
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/be;->r:Landroid/text/format/Time;

    .line 70
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/e/be;->u:I

    .line 73
    iput-object v3, p0, Lcom/android/calendar/e/be;->v:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/be;->w:Ljava/util/ArrayList;

    .line 75
    iput-object v3, p0, Lcom/android/calendar/e/be;->x:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/be;->y:Ljava/util/ArrayList;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/be;->A:Ljava/util/ArrayList;

    .line 88
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/e/be;->H:Z

    .line 91
    new-instance v0, Lcom/android/calendar/e/bf;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/bf;-><init>(Lcom/android/calendar/e/be;)V

    iput-object v0, p0, Lcom/android/calendar/e/be;->I:Ljava/lang/Runnable;

    .line 119
    iput-object p1, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    .line 120
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/be;->E:Lcom/android/calendar/al;

    .line 121
    sput p5, Lcom/android/calendar/e/be;->p:I

    .line 122
    invoke-virtual {p0}, Lcom/android/calendar/e/be;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 123
    const v0, 0x7f0d0001

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/be;->u:I

    .line 124
    sget v0, Lcom/android/calendar/e/be;->p:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/calendar/e/be;->n:[I

    .line 125
    iget-object v0, p0, Lcom/android/calendar/e/be;->n:[I

    invoke-static {v0, v5}, Ljava/util/Arrays;->fill([II)V

    .line 127
    invoke-virtual {p0, v2}, Lcom/android/calendar/e/be;->setOrientation(I)V

    .line 129
    invoke-virtual {p0}, Lcom/android/calendar/e/be;->getDayViewType()Lcom/android/calendar/e/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    .line 130
    new-instance v0, Lcom/android/calendar/e/ao;

    iget-object v2, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-direct {v0, v2, v4, p5}, Lcom/android/calendar/e/ao;-><init>(Landroid/content/Context;Lcom/android/calendar/e/g;I)V

    iput-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    .line 131
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/be;->addView(Landroid/view/View;)V

    .line 132
    sget-boolean v0, Lcom/android/calendar/dz;->b:Z

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    const v2, 0x7f0b011f

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/ao;->setBackgroundResource(I)V

    .line 134
    iget-object v0, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    const v2, 0x7f0b0101

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/g;->setBackgroundResource(I)V

    .line 136
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/be;->getSelectedTime()Landroid/text/format/Time;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/be;->q:Landroid/text/format/Time;

    .line 138
    iget-object v0, p0, Lcom/android/calendar/e/be;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 139
    :goto_1
    sget v2, Lcom/android/calendar/e/be;->p:I

    if-ge v0, v2, :cond_3

    .line 140
    iget-object v2, p0, Lcom/android/calendar/e/be;->y:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 88
    goto :goto_0

    .line 143
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 144
    :goto_2
    sget v2, Lcom/android/calendar/e/be;->p:I

    if-ge v0, v2, :cond_4

    .line 145
    iget-object v2, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 148
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/be;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 149
    :goto_3
    sget v2, Lcom/android/calendar/e/be;->p:I

    if-ge v0, v2, :cond_5

    .line 150
    iget-object v2, p0, Lcom/android/calendar/e/be;->A:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 154
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 155
    new-instance v2, Lcom/android/calendar/e/bj;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/e/bj;-><init>(Lcom/android/calendar/e/be;Landroid/content/ContentResolver;)V

    iput-object v2, p0, Lcom/android/calendar/e/be;->B:Landroid/content/AsyncQueryHandler;

    .line 157
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 158
    iget-object v2, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 159
    new-instance v2, Landroid/view/View;

    iget-object v4, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 160
    const v4, 0x7f0201f5

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 161
    const v4, 0x7f0c034a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 162
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v5, v3, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v2, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 164
    invoke-virtual {p0, v0}, Lcom/android/calendar/e/be;->addView(Landroid/view/View;)V

    .line 166
    invoke-virtual {p0}, Lcom/android/calendar/e/be;->setListHeight()I

    .line 167
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/e/be;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/e/be;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/android/calendar/e/be;->v:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(Ljava/util/ArrayList;Z)V
    .locals 1

    .prologue
    .line 532
    if-nez p1, :cond_0

    .line 537
    :goto_0
    return-void

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/g;->a(Ljava/util/ArrayList;)V

    .line 536
    iget-object v0, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->invalidate()V

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 54
    sput-boolean p0, Lcom/android/calendar/e/be;->F:Z

    return p0
.end method

.method static synthetic b(Lcom/android/calendar/e/be;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/e/be;->q:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/e/be;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/android/calendar/e/be;->x:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic c(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/e/be;->v:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/e/be;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/android/calendar/e/be;->s:I

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/e/be;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/e/be;->I:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/e/be;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/android/calendar/e/be;->H:Z

    return v0
.end method

.method static synthetic g()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/android/calendar/e/be;->p:I

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/e/be;)Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/e/be;->B:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/e/be;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/e/be;->x:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 288
    sget v1, Lcom/android/calendar/e/be;->p:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lcom/android/calendar/e/be;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/calendar/e/be;->l()V

    return-void
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 292
    invoke-static {}, Lcom/android/calendar/e/be;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Lcom/android/calendar/e/be;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/android/calendar/e/be;->G:I

    return v0
.end method

.method private j()Landroid/net/Uri;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 296
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 297
    cmp-long v2, v0, v6

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/e/be;->H:Z

    if-eqz v2, :cond_0

    .line 298
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 300
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 301
    const v1, 0x24dc87

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/android/calendar/e/be;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 302
    iget v0, p0, Lcom/android/calendar/e/be;->t:I

    if-nez v0, :cond_3

    .line 303
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    const v1, 0x24dc84

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 309
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 310
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 312
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/android/calendar/e/be;->H:Z

    if-eqz v1, :cond_2

    .line 313
    iget-object v1, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    const/16 v2, 0x7b2

    if-eq v1, v2, :cond_2

    .line 314
    iget-object v1, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iput-boolean v5, v1, Landroid/text/format/Time;->allDay:Z

    .line 315
    iget-object v1, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iput v5, v1, Landroid/text/format/Time;->hour:I

    .line 318
    :cond_2
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 319
    iget-object v2, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-static {v2, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 320
    iget-object v2, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-virtual {v2, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 321
    sget v4, Lcom/android/calendar/e/be;->p:I

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x1

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 322
    invoke-virtual {v1, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 323
    sget-object v4, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 324
    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 325
    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 326
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 304
    :cond_3
    iget v0, p0, Lcom/android/calendar/e/be;->t:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    .line 305
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    const v1, 0x24dc83

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    goto :goto_0

    .line 307
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    const v1, 0x24dc85

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/calendar/e/be;)Lcom/android/calendar/e/ao;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 330
    const-string v0, "visible=1"

    .line 331
    iget-object v1, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v1

    .line 332
    if-eqz v1, :cond_0

    .line 333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 337
    return-object v0
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 340
    iget-object v0, p0, Lcom/android/calendar/e/be;->x:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/be;->setTasks(Ljava/util/ArrayList;)V

    .line 341
    iget-object v0, p0, Lcom/android/calendar/e/be;->v:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/be;->setEvents(Ljava/util/ArrayList;)V

    .line 342
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    iget-object v1, p0, Lcom/android/calendar/e/be;->y:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/e/be;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/calendar/e/ao;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/be;->w:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v5}, Lcom/android/calendar/e/be;->a(Ljava/util/ArrayList;Z)V

    .line 346
    invoke-virtual {p0}, Lcom/android/calendar/e/be;->setListHeight()I

    .line 348
    sget-boolean v0, Lcom/android/calendar/e/d;->f:Z

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/ao;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-direct {p0}, Lcom/android/calendar/e/be;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/ao;->b(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/be;->G:I

    .line 352
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    const-string v1, "height"

    new-array v2, v6, [F

    const/4 v3, 0x0

    aput v3, v2, v5

    const/4 v3, 0x1

    iget v4, p0, Lcom/android/calendar/e/be;->G:I

    int-to-float v4, v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 354
    new-array v1, v6, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 356
    new-instance v2, Lcom/android/calendar/e/bg;

    invoke-direct {v2, p0}, Lcom/android/calendar/e/bg;-><init>(Lcom/android/calendar/e/be;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 380
    new-instance v2, Landroid/animation/LayoutTransition;

    invoke-direct {v2}, Landroid/animation/LayoutTransition;-><init>()V

    .line 381
    invoke-virtual {v2, v6, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 382
    invoke-virtual {v2, v5, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 383
    new-instance v0, Lcom/android/calendar/e/bh;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/bh;-><init>(Lcom/android/calendar/e/be;)V

    invoke-virtual {v2, v0}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 400
    invoke-virtual {p0, v2}, Lcom/android/calendar/e/be;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 402
    new-instance v0, Lcom/android/calendar/e/bi;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/bi;-><init>(Lcom/android/calendar/e/be;)V

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/calendar/e/be;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 409
    :cond_1
    return-void

    .line 354
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private m()I
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 498
    .line 501
    iget-object v0, p0, Lcom/android/calendar/e/be;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    move v1, v3

    .line 502
    :goto_0
    if-ge v4, v5, :cond_1

    .line 503
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->getDraggingEventDay()I

    move-result v6

    .line 504
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0, v4}, Lcom/android/calendar/e/ao;->a(I)I

    move-result v0

    .line 505
    if-ne v6, v4, :cond_0

    .line 506
    add-int/lit8 v0, v0, 0x1

    .line 508
    :cond_0
    if-le v0, v1, :cond_6

    .line 502
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    .line 513
    :cond_1
    if-nez v1, :cond_3

    .line 514
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    if-eqz v0, :cond_5

    move v1, v2

    .line 528
    :cond_2
    :goto_2
    return v1

    .line 517
    :cond_3
    iget v0, p0, Lcom/android/calendar/e/be;->u:I

    if-gt v1, v0, :cond_4

    .line 518
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    if-eqz v0, :cond_2

    if-ge v1, v2, :cond_2

    move v1, v2

    .line 519
    goto :goto_2

    .line 523
    :cond_4
    iget v0, p0, Lcom/android/calendar/e/be;->u:I

    if-le v1, v0, :cond_2

    .line 524
    iget v1, p0, Lcom/android/calendar/e/be;->u:I

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private setEvents(Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 441
    if-nez p1, :cond_1

    .line 485
    :cond_0
    return-void

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 445
    const-wide/16 v4, -0x1

    cmp-long v2, v0, v4

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/android/calendar/e/be;->H:Z

    if-eqz v2, :cond_2

    .line 446
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 448
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iget-wide v4, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v5

    .line 449
    iget-object v0, p0, Lcom/android/calendar/e/be;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 450
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 453
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 454
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 457
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/be;->w:Ljava/util/ArrayList;

    if-nez v0, :cond_6

    .line 458
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/be;->w:Ljava/util/ArrayList;

    .line 463
    :goto_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    .line 464
    :goto_3
    if-ge v4, v6, :cond_0

    .line 465
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 466
    iget v7, v0, Lcom/android/calendar/dh;->i:I

    .line 467
    iget v8, v0, Lcom/android/calendar/dh;->j:I

    .line 468
    invoke-virtual {v0}, Lcom/android/calendar/dh;->e()Z

    move-result v1

    if-nez v1, :cond_7

    .line 469
    iget-object v1, p0, Lcom/android/calendar/e/be;->w:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    iget-object v1, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v2, v3

    .line 471
    :goto_4
    if-ge v2, v9, :cond_9

    .line 472
    add-int v1, v5, v2

    if-gt v7, v1, :cond_5

    add-int v1, v5, v2

    if-lt v8, v1, :cond_5

    .line 473
    iget-object v1, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 460
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/be;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    .line 477
    :cond_7
    iget-object v1, p0, Lcom/android/calendar/e/be;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v2, v3

    .line 478
    :goto_5
    if-ge v2, v9, :cond_9

    .line 479
    add-int v1, v5, v2

    if-gt v7, v1, :cond_8

    add-int v1, v5, v2

    if-lt v8, v1, :cond_8

    .line 480
    iget-object v1, p0, Lcom/android/calendar/e/be;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 478
    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 464
    :cond_9
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3
.end method

.method private setSelectedDay(Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/android/calendar/e/be;->q:Landroid/text/format/Time;

    .line 254
    invoke-direct {p0, p1}, Lcom/android/calendar/e/be;->setWeekStartTime(Landroid/text/format/Time;)V

    .line 255
    return-void
.end method

.method private setTasks(Ljava/util/ArrayList;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 412
    if-nez p1, :cond_1

    .line 438
    :cond_0
    return-void

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 416
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/android/calendar/e/be;->H:Z

    if-eqz v3, :cond_2

    .line 417
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 419
    :cond_2
    iget-object v3, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iget-wide v4, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v4

    .line 421
    iget-object v0, p0, Lcom/android/calendar/e/be;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 422
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 425
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    .line 428
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 429
    :goto_1
    if-ge v3, v5, :cond_0

    .line 430
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/hc;

    iget-wide v0, v0, Lcom/android/calendar/hc;->c:J

    invoke-static {v0, v1, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v8

    .line 431
    iget-object v0, p0, Lcom/android/calendar/e/be;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v1, v2

    .line 432
    :goto_2
    if-ge v1, v9, :cond_5

    .line 433
    add-int v0, v4, v1

    if-ne v0, v8, :cond_4

    .line 434
    iget-object v0, p0, Lcom/android/calendar/e/be;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 429
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method private setWeekStartTime(Landroid/text/format/Time;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 258
    .line 259
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 260
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 261
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 262
    iget-object v3, p0, Lcom/android/calendar/e/be;->D:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v3

    iput v3, p0, Lcom/android/calendar/e/be;->t:I

    .line 263
    iget v3, p0, Lcom/android/calendar/e/be;->t:I

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;IZ)V

    .line 264
    sget v2, Lcom/android/calendar/e/be;->p:I

    if-ne v2, v5, :cond_1

    .line 267
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 268
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 269
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/e/be;->H:Z

    if-eqz v1, :cond_0

    .line 270
    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 272
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/e/be;->i:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/be;->s:I

    .line 273
    return-void

    :cond_0
    move-wide v0, v2

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/text/format/Time;ZZ)V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/e/g;->a(Landroid/text/format/Time;ZZ)V

    .line 245
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/e/ao;->a(Landroid/text/format/Time;ZZ)V

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->b()V

    .line 249
    invoke-direct {p0, p1}, Lcom/android/calendar/e/be;->setSelectedDay(Landroid/text/format/Time;)V

    .line 250
    return-void
.end method

.method public a(ZZ)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 277
    sget-boolean v0, Lcom/android/calendar/e/be;->F:Z

    if-eqz v0, :cond_0

    .line 285
    :goto_0
    return-void

    .line 280
    :cond_0
    if-eqz p2, :cond_1

    .line 281
    invoke-virtual {p0}, Lcom/android/calendar/e/be;->c()V

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/be;->B:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/android/calendar/e/be;->j()Landroid/net/Uri;

    move-result-object v3

    invoke-static {}, Lcom/android/calendar/dh;->j()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/android/calendar/e/be;->k()Ljava/lang/String;

    move-result-object v5

    const-string v7, "startDay,startMinute,title"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/calendar/e/ao;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 586
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/calendar/e/a;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 587
    return-void
.end method

.method protected c()V
    .locals 3

    .prologue
    .line 103
    invoke-super {p0}, Lcom/android/calendar/e/a;->c()V

    .line 104
    iget-object v0, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 106
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 107
    iget-object v0, p0, Lcom/android/calendar/e/be;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 106
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->e()V

    .line 113
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/be;->setListHeight()I

    .line 114
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->c()V

    .line 548
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 591
    invoke-super {p0}, Lcom/android/calendar/e/a;->e()V

    .line 592
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->f()V

    .line 595
    :cond_0
    return-void
.end method

.method f()V
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->d()V

    .line 610
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 611
    return-void
.end method

.method protected getAnimatedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    return-object v0
.end method

.method public getSelectedDay()Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/android/calendar/e/be;->r:Landroid/text/format/Time;

    return-object v0
.end method

.method public getSelectedEventMemo()I
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedTime()Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getSelectedTime()Landroid/text/format/Time;

    move-result-object v0

    return-object v0
.end method

.method public getWeekAllDayView()Lcom/android/calendar/e/ao;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    return-object v0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    .line 566
    invoke-super {p0, p1}, Lcom/android/calendar/e/a;->onScale(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    .line 567
    iget-object v1, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v1, :cond_0

    .line 568
    iget-object v1, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v1, p1}, Lcom/android/calendar/e/ao;->b(Landroid/view/ScaleGestureDetector;)Z

    .line 570
    :cond_0
    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    .line 557
    invoke-super {p0, p1}, Lcom/android/calendar/e/a;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    .line 558
    iget-object v1, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v1, :cond_0

    .line 559
    iget-object v1, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v1, p1}, Lcom/android/calendar/e/ao;->a(Landroid/view/ScaleGestureDetector;)Z

    .line 561
    :cond_0
    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 1

    .prologue
    .line 575
    invoke-super {p0, p1}, Lcom/android/calendar/e/a;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 576
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/ao;->c(Landroid/view/ScaleGestureDetector;)V

    .line 579
    :cond_0
    return-void
.end method

.method public setListHeight()I
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    if-nez v0, :cond_0

    .line 489
    const/4 v0, -0x1

    .line 494
    :goto_0
    return v0

    .line 491
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/be;->m()I

    move-result v0

    .line 492
    iget-object v1, p0, Lcom/android/calendar/e/be;->C:Lcom/android/calendar/e/ao;

    invoke-virtual {v1, v0}, Lcom/android/calendar/e/ao;->b(I)I

    move-result v0

    .line 493
    invoke-virtual {p0, v0}, Lcom/android/calendar/e/be;->setListHeight(I)V

    goto :goto_0
.end method
