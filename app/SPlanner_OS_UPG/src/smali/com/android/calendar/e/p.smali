.class Lcom/android/calendar/e/p;
.super Ljava/lang/Object;
.source "DayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 1814
    iput-object p1, p0, Lcom/android/calendar/e/p;->a:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1817
    iget-object v0, p0, Lcom/android/calendar/e/p;->a:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1818
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1819
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1822
    sget-object v2, Lcom/android/calendar/e/ai;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 1823
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1825
    :try_start_0
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1833
    :cond_0
    sget-object v2, Lcom/android/calendar/month/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1834
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1836
    :try_start_1
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1844
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/p;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/e/g;)Landroid/app/DialogFragment;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/e/p;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/e/g;)Landroid/app/DialogFragment;

    move-result-object v0

    instance-of v0, v0, Lcom/android/calendar/e/ai;

    if-eqz v0, :cond_3

    .line 1845
    iget-object v0, p0, Lcom/android/calendar/e/p;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/e/g;)Landroid/app/DialogFragment;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/e/ai;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1851
    :cond_2
    :goto_0
    :try_start_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1856
    :goto_1
    return-void

    .line 1826
    :catch_0
    move-exception v0

    .line 1828
    invoke-static {}, Lcom/android/calendar/e/g;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1837
    :catch_1
    move-exception v0

    .line 1839
    invoke-static {}, Lcom/android/calendar/e/g;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1846
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/p;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/e/g;)Landroid/app/DialogFragment;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/p;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/e/g;)Landroid/app/DialogFragment;

    move-result-object v0

    instance-of v0, v0, Lcom/android/calendar/month/bb;

    if-eqz v0, :cond_2

    .line 1847
    iget-object v0, p0, Lcom/android/calendar/e/p;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/e/g;)Landroid/app/DialogFragment;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/month/bb;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 1852
    :catch_2
    move-exception v0

    .line 1854
    invoke-static {}, Lcom/android/calendar/e/g;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to show DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
