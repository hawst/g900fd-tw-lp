.class Lcom/android/calendar/e/ac;
.super Ljava/lang/Object;
.source "DayView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;

.field private final b:I

.field private final c:Landroid/text/format/Time;

.field private final d:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Lcom/android/calendar/e/g;Landroid/text/format/Time;Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 2031
    iput-object p1, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2032
    invoke-static {}, Lcom/android/calendar/e/g;->o()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ac;->b:I

    .line 2033
    iput-object p2, p0, Lcom/android/calendar/e/ac;->c:Landroid/text/format/Time;

    .line 2034
    iput-object p3, p0, Lcom/android/calendar/e/ac;->d:Landroid/text/format/Time;

    .line 2035
    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 2039
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    iget-object v0, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    check-cast v0, Lcom/android/calendar/a;

    invoke-virtual {v0}, Lcom/android/calendar/a;->c()V

    .line 2040
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2041
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->f(Lcom/android/calendar/e/g;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2042
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->g(Lcom/android/calendar/e/g;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2044
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    new-instance v1, Lcom/android/calendar/e/ad;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/ad;-><init>(Lcom/android/calendar/e/ac;)V

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/calendar/e/g;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2050
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2054
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 14

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 2058
    iget v0, p0, Lcom/android/calendar/e/ac;->b:I

    invoke-static {}, Lcom/android/calendar/e/g;->p()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2059
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->h(Lcom/android/calendar/e/g;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x20

    iget-object v4, p0, Lcom/android/calendar/e/ac;->c:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/e/ac;->d:Landroid/text/format/Time;

    const-wide/16 v7, -0x1

    const-wide/16 v10, 0x1

    move-object v1, p0

    move-object v12, v6

    move-object v13, v6

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2062
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;Z)Z

    .line 2063
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, v9}, Lcom/android/calendar/e/g;->b(Lcom/android/calendar/e/g;Z)Z

    .line 2064
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->i(Lcom/android/calendar/e/g;)Lcom/android/calendar/e/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/e/d;->b()Lcom/android/calendar/e/a;

    move-result-object v0

    .line 2065
    invoke-virtual {v0, v9, v9}, Lcom/android/calendar/e/a;->a(ZZ)V

    .line 2066
    iget-object v0, p0, Lcom/android/calendar/e/ac;->a:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->j()V

    .line 2067
    return-void
.end method
