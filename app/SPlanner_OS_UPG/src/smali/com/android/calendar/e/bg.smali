.class Lcom/android/calendar/e/bg;
.super Ljava/lang/Object;
.source "WeekLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field a:I

.field b:Landroid/view/ViewGroup;

.field c:Z

.field d:Landroid/view/ViewGroup$LayoutParams;

.field final synthetic e:Lcom/android/calendar/e/be;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/be;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 365
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 366
    iget-boolean v0, p0, Lcom/android/calendar/e/bg;->c:Z

    if-nez v0, :cond_0

    .line 367
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/bg;->c:Z

    .line 368
    iget-object v0, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    invoke-virtual {v0}, Lcom/android/calendar/e/be;->getBottom()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/bg;->a:I

    .line 369
    iget-object v0, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    iget-object v0, v0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/calendar/e/bg;->b:Landroid/view/ViewGroup;

    .line 370
    iget-object v0, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    iget-object v0, v0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/bg;->d:Landroid/view/ViewGroup$LayoutParams;

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/bg;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    invoke-static {v2}, Lcom/android/calendar/e/be;->j(Lcom/android/calendar/e/be;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTop(I)V

    .line 373
    iget-object v0, p0, Lcom/android/calendar/e/bg;->d:Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Lcom/android/calendar/e/bg;->a:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    invoke-static {v3}, Lcom/android/calendar/e/be;->j(Lcom/android/calendar/e/be;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    sub-float v1, v2, v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 374
    iget-object v0, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    iget-object v0, v0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    iget-object v1, p0, Lcom/android/calendar/e/bg;->d:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/g;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 375
    iget-object v0, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    iget-object v0, v0, Lcom/android/calendar/e/be;->j:Lcom/android/calendar/e/g;

    iget v1, p0, Lcom/android/calendar/e/bg;->a:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/g;->setBottom(I)V

    .line 376
    iget-object v0, p0, Lcom/android/calendar/e/bg;->e:Lcom/android/calendar/e/be;

    invoke-virtual {v0}, Lcom/android/calendar/e/be;->invalidate()V

    .line 377
    return-void
.end method
