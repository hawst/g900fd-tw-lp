.class Lcom/android/calendar/e/l;
.super Ljava/lang/Object;
.source "DayView.java"

# interfaces
.implements Lcom/samsung/android/service/gesture/GestureListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 6421
    iput-object p1, p0, Lcom/android/calendar/e/l;->a:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 6424
    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v0

    .line 6425
    invoke-static {}, Lcom/android/calendar/e/g;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onGestureEvent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 6426
    iget-object v1, p0, Lcom/android/calendar/e/l;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->D(Lcom/android/calendar/e/g;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eq v0, v4, :cond_0

    if-nez v0, :cond_1

    .line 6428
    :cond_0
    if-nez v0, :cond_2

    .line 6429
    iget-object v0, p0, Lcom/android/calendar/e/l;->a:Lcom/android/calendar/e/g;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->m(Lcom/android/calendar/e/g;Z)V

    .line 6434
    :cond_1
    :goto_0
    return-void

    .line 6431
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/l;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, v4}, Lcom/android/calendar/e/g;->m(Lcom/android/calendar/e/g;Z)V

    goto :goto_0
.end method
