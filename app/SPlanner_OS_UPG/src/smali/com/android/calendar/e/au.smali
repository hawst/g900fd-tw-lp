.class Lcom/android/calendar/e/au;
.super Ljava/lang/Object;
.source "WeekAllDayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/ao;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/ao;)V
    .locals 0

    .prologue
    .line 2830
    iput-object p1, p0, Lcom/android/calendar/e/au;->a:Lcom/android/calendar/e/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2833
    iget-object v0, p0, Lcom/android/calendar/e/au;->a:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2834
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 2835
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 2838
    sget-object v2, Lcom/android/calendar/month/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 2839
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2841
    :try_start_0
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2848
    :cond_0
    sget-object v2, Lcom/android/calendar/e/ai;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 2849
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2851
    :try_start_1
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2858
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/e/au;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->d(Lcom/android/calendar/e/ao;)Landroid/app/DialogFragment;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/au;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->d(Lcom/android/calendar/e/ao;)Landroid/app/DialogFragment;

    move-result-object v0

    instance-of v0, v0, Lcom/android/calendar/month/bb;

    if-eqz v0, :cond_2

    .line 2859
    iget-object v0, p0, Lcom/android/calendar/e/au;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->d(Lcom/android/calendar/e/ao;)Landroid/app/DialogFragment;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/month/bb;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2861
    :cond_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2865
    :goto_0
    return-void

    .line 2842
    :catch_0
    move-exception v0

    .line 2843
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to remove Month DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2852
    :catch_1
    move-exception v0

    .line 2853
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to remove Month DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2862
    :catch_2
    move-exception v0

    .line 2863
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to add Month DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
