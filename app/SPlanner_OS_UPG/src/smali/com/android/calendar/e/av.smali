.class Lcom/android/calendar/e/av;
.super Ljava/lang/Object;
.source "WeekAllDayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/ao;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/ao;)V
    .locals 0

    .prologue
    .line 2868
    iput-object p1, p0, Lcom/android/calendar/e/av;->a:Lcom/android/calendar/e/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2871
    iget-object v0, p0, Lcom/android/calendar/e/av;->a:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2872
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 2873
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 2874
    sget-object v2, Lcom/android/calendar/e/ai;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 2875
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2877
    :try_start_0
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2883
    :cond_0
    :goto_0
    sget-object v2, Lcom/android/calendar/month/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 2884
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2886
    :try_start_1
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2892
    :cond_1
    :goto_1
    :try_start_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2896
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/av;->a:Lcom/android/calendar/e/ao;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/e/ao;->a(Lcom/android/calendar/e/ao;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2897
    return-void

    .line 2878
    :catch_0
    move-exception v2

    .line 2879
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Fail to dismiss DayViewHoverFragment"

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2887
    :catch_1
    move-exception v0

    .line 2888
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Fail to remove MonthHoverEventFragment"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2893
    :catch_2
    move-exception v0

    .line 2894
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
