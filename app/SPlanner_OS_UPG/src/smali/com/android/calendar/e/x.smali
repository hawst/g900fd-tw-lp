.class Lcom/android/calendar/e/x;
.super Ljava/lang/Object;
.source "DayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;


# direct methods
.method private constructor <init>(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 6107
    iput-object p1, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/e/g;Lcom/android/calendar/e/h;)V
    .locals 0

    .prologue
    .line 6107
    invoke-direct {p0, p1}, Lcom/android/calendar/e/x;-><init>(Lcom/android/calendar/e/g;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6111
    iget-object v2, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->s(Lcom/android/calendar/e/g;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->t(Lcom/android/calendar/e/g;)Landroid/widget/OverScroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0}, Lcom/android/calendar/e/g;->j(Lcom/android/calendar/e/g;Z)Z

    .line 6112
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->s(Lcom/android/calendar/e/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    iget-boolean v0, v0, Lcom/android/calendar/e/g;->c:Z

    if-eqz v0, :cond_2

    .line 6113
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->f(Lcom/android/calendar/e/g;Z)V

    .line 6114
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 6145
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 6111
    goto :goto_0

    .line 6118
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    iget-object v2, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->t(Lcom/android/calendar/e/g;)Landroid/widget/OverScroller;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v2

    invoke-static {v0, v2}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    .line 6120
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->u(Lcom/android/calendar/e/g;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6121
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v0

    if-gez v0, :cond_8

    .line 6122
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->w(Lcom/android/calendar/e/g;)Landroid/widget/EdgeEffect;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->v(Lcom/android/calendar/e/g;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    .line 6123
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->k(Lcom/android/calendar/e/g;Z)Z

    .line 6128
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    iget-object v2, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->t(Lcom/android/calendar/e/g;)Landroid/widget/OverScroller;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/OverScroller;->getCurrVelocity()F

    move-result v2

    invoke-static {v0, v2}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;F)F

    .line 6131
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->y(Lcom/android/calendar/e/g;)I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->y(Lcom/android/calendar/e/g;)I

    move-result v0

    iget-object v2, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v2

    if-ne v0, v2, :cond_6

    .line 6133
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v0

    if-gez v0, :cond_9

    .line 6134
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    .line 6140
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->o(Lcom/android/calendar/e/g;)V

    .line 6141
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 6142
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 6144
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->invalidate()V

    goto/16 :goto_1

    .line 6124
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v0

    iget-object v2, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v2

    if-le v0, v2, :cond_3

    .line 6125
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->x(Lcom/android/calendar/e/g;)Landroid/widget/EdgeEffect;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->v(Lcom/android/calendar/e/g;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    .line 6126
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->k(Lcom/android/calendar/e/g;Z)Z

    goto :goto_2

    .line 6135
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v1

    if-le v0, v1, :cond_6

    .line 6136
    iget-object v0, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    iget-object v1, p0, Lcom/android/calendar/e/x;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    goto :goto_3
.end method
