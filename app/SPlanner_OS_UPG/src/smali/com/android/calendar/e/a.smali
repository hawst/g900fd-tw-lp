.class public abstract Lcom/android/calendar/e/a;
.super Landroid/widget/LinearLayout;
.source "AbstractTimelineLayout.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# static fields
.field protected static e:Lcom/android/calendar/dh;

.field protected static f:Lcom/android/calendar/hc;


# instance fields
.field protected a:[I

.field protected b:[I

.field protected c:I

.field protected final d:Lcom/android/calendar/dj;

.field protected g:Landroid/util/LruCache;

.field protected h:Landroid/util/SparseIntArray;

.field protected i:Landroid/text/format/Time;

.field protected j:Lcom/android/calendar/e/g;

.field protected k:Lcom/android/calendar/al;

.field protected l:I

.field protected m:Landroid/content/Context;

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private final t:Landroid/view/GestureDetector;

.field private u:Landroid/view/ScaleGestureDetector;

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/al;Lcom/android/calendar/dj;Lcom/android/calendar/e/d;I)V
    .locals 2

    .prologue
    .line 164
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/a;->v:Z

    .line 79
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/a;->i:Landroid/text/format/Time;

    .line 166
    iput-object p1, p0, Lcom/android/calendar/e/a;->m:Landroid/content/Context;

    .line 167
    iput-object p2, p0, Lcom/android/calendar/e/a;->k:Lcom/android/calendar/al;

    .line 168
    iput-object p3, p0, Lcom/android/calendar/e/a;->d:Lcom/android/calendar/dj;

    .line 169
    new-instance v0, Lcom/android/calendar/e/g;

    invoke-direct {v0, p1, p2, p3, p5}, Lcom/android/calendar/e/g;-><init>(Landroid/content/Context;Lcom/android/calendar/al;Lcom/android/calendar/dj;I)V

    iput-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    .line 170
    iput p5, p0, Lcom/android/calendar/e/a;->l:I

    .line 171
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/android/calendar/e/c;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/c;-><init>(Lcom/android/calendar/e/a;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/e/a;->t:Landroid/view/GestureDetector;

    .line 172
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/android/calendar/e/a;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/e/a;->u:Landroid/view/ScaleGestureDetector;

    .line 173
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gx;->a(Landroid/app/FragmentManager;)Landroid/util/LruCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/a;->g:Landroid/util/LruCache;

    .line 174
    invoke-virtual {p0}, Lcom/android/calendar/e/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 175
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/a;->n:I

    .line 176
    const v1, 0x7f0b0121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/a;->o:I

    .line 177
    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/a;->p:I

    .line 178
    const v1, 0x7f0b00e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/a;->q:I

    .line 179
    const v1, 0x7f0b00cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/a;->r:I

    .line 180
    const v1, 0x7f0b00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/a;->s:I

    .line 181
    const v1, 0x7f0b00c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/a;->c:I

    .line 182
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/android/calendar/e/a;->m:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gx;->b(Landroid/app/FragmentManager;)Landroid/util/SparseIntArray;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/a;->h:Landroid/util/SparseIntArray;

    .line 185
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/e/a;->b()V

    .line 186
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/e/a;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/calendar/e/a;->v:Z

    return p1
.end method


# virtual methods
.method public a(Landroid/text/format/Time;)I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/g;->a(Landroid/text/format/Time;)I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->h()V

    .line 98
    return-void
.end method

.method public abstract a(Landroid/text/format/Time;ZZ)V
.end method

.method public abstract a(ZZ)V
.end method

.method public a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/calendar/e/g;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 190
    const/4 v0, 0x1

    return v0
.end method

.method protected final b()V
    .locals 7

    .prologue
    const/4 v0, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 129
    new-array v2, v0, [I

    .line 130
    new-array v3, v0, [I

    .line 132
    const-string v0, "XXXXXXR"

    .line 134
    iget v1, p0, Lcom/android/calendar/e/a;->n:I

    aput v1, v2, v4

    .line 135
    iget v1, p0, Lcom/android/calendar/e/a;->r:I

    aput v1, v2, v5

    .line 136
    iget v1, p0, Lcom/android/calendar/e/a;->p:I

    aput v1, v2, v6

    .line 138
    iget v1, p0, Lcom/android/calendar/e/a;->o:I

    aput v1, v3, v4

    .line 139
    iget v1, p0, Lcom/android/calendar/e/a;->s:I

    aput v1, v3, v5

    .line 140
    iget v1, p0, Lcom/android/calendar/e/a;->q:I

    aput v1, v3, v6

    .line 142
    invoke-static {}, Lcom/android/calendar/dz;->e()Ljava/lang/String;

    move-result-object v1

    .line 143
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 146
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/e/a;->m:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v1

    .line 147
    invoke-static {v0, v2, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/e/a;->a:[I

    .line 148
    invoke-static {v0, v3, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/a;->b:[I

    .line 149
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/calendar/e/g;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 196
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/g;->setStartingScroll(Z)V

    .line 200
    :cond_0
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method public abstract d()V
.end method

.method public e()V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->l()V

    .line 357
    :cond_0
    return-void
.end method

.method f()V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method protected abstract getAnimatedView()Landroid/view/View;
.end method

.method public getDayViewType()Lcom/android/calendar/e/g;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    return-object v0
.end method

.method public getFirstVisibleHour()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getFirstVisibleHour()I

    move-result v0

    return v0
.end method

.method public abstract getSelectedDay()Landroid/text/format/Time;
.end method

.method public getSelectedEvent()Lcom/android/calendar/dh;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/android/calendar/e/a;->e:Lcom/android/calendar/dh;

    return-object v0
.end method

.method public abstract getSelectedEventMemo()I
.end method

.method public getSelectedTask()Lcom/android/calendar/hc;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/android/calendar/e/a;->f:Lcom/android/calendar/hc;

    return-object v0
.end method

.method public getStartTime()Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/android/calendar/e/a;->i:Landroid/text/format/Time;

    return-object v0
.end method

.method public getTimelineView()Lcom/android/calendar/e/g;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 227
    invoke-virtual {p0}, Lcom/android/calendar/e/a;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 228
    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getTouchMode()Lcom/android/calendar/e/ag;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/e/ag;->d:Lcom/android/calendar/e/ag;

    if-eq v0, v2, :cond_0

    .line 230
    iget-object v0, p0, Lcom/android/calendar/e/a;->u:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 231
    iget-object v0, p0, Lcom/android/calendar/e/a;->u:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getEditMode()Lcom/android/calendar/e/ab;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 245
    :goto_0
    return v0

    .line 238
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/e/a;->v:Z

    if-nez v0, :cond_1

    .line 239
    iget-object v0, p0, Lcom/android/calendar/e/a;->t:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getEditMode()Lcom/android/calendar/e/ab;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 241
    goto :goto_0

    .line 245
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/g;->b(Landroid/view/ScaleGestureDetector;)Z

    .line 337
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 325
    iput-boolean v1, p0, Lcom/android/calendar/e/a;->v:Z

    .line 326
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/g;->a(Landroid/view/ScaleGestureDetector;)Z

    .line 329
    :cond_0
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 4

    .prologue
    .line 342
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/g;->c(Landroid/view/ScaleGestureDetector;)V

    .line 345
    :cond_0
    new-instance v0, Lcom/android/calendar/e/b;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/b;-><init>(Lcom/android/calendar/e/a;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/calendar/e/a;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 351
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 204
    invoke-virtual {p0}, Lcom/android/calendar/e/a;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 205
    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getTouchMode()Lcom/android/calendar/e/ag;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/e/ag;->d:Lcom/android/calendar/e/ag;

    if-eq v0, v2, :cond_0

    .line 207
    iget-object v0, p0, Lcom/android/calendar/e/a;->u:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 208
    iget-object v0, p0, Lcom/android/calendar/e/a;->u:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getEditMode()Lcom/android/calendar/e/ab;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 222
    :goto_0
    return v0

    .line 215
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/e/a;->v:Z

    if-nez v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/android/calendar/e/a;->t:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getEditMode()Lcom/android/calendar/e/ab;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 218
    goto :goto_0

    .line 222
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setFirstVisibleHour(I)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/android/calendar/e/a;->j:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/g;->setFirstVisibleHour(I)V

    .line 114
    return-void
.end method

.method public abstract setListHeight()I
.end method

.method public setListHeight(I)V
    .locals 2

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/android/calendar/e/a;->getAnimatedView()Landroid/view/View;

    move-result-object v0

    .line 271
    if-nez v0, :cond_0

    .line 277
    :goto_0
    return-void

    .line 274
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 275
    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 276
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
