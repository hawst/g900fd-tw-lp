.class Lcom/android/calendar/e/e;
.super Ljava/lang/Object;
.source "DayFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/d;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/d;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/android/calendar/e/e;->a:Lcom/android/calendar/e/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/e/e;->a:Lcom/android/calendar/e/d;

    invoke-virtual {v0}, Lcom/android/calendar/e/d;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/e;->a:Lcom/android/calendar/e/d;

    invoke-virtual {v0}, Lcom/android/calendar/e/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/e;->a:Lcom/android/calendar/e/d;

    invoke-static {v1}, Lcom/android/calendar/e/d;->a(Lcom/android/calendar/e/d;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lcom/android/calendar/e/e;->a:Lcom/android/calendar/e/d;

    invoke-static {v1}, Lcom/android/calendar/e/d;->b(Lcom/android/calendar/e/d;)Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/android/calendar/e/e;->a:Lcom/android/calendar/e/d;

    invoke-static {v0}, Lcom/android/calendar/e/d;->b(Lcom/android/calendar/e/d;)Landroid/text/format/Time;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->d(Landroid/text/format/Time;)V

    .line 94
    iget-object v0, p0, Lcom/android/calendar/e/e;->a:Lcom/android/calendar/e/d;

    invoke-static {v0}, Lcom/android/calendar/e/d;->b(Lcom/android/calendar/e/d;)Landroid/text/format/Time;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    goto :goto_0
.end method
