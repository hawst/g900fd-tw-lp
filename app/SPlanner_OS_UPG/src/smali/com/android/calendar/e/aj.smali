.class Lcom/android/calendar/e/aj;
.super Ljava/lang/Object;
.source "DayViewHoverFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/ai;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/ai;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x6

    const/high16 v9, 0x40400000    # 3.0f

    const/4 v8, 0x0

    .line 158
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-virtual {v0}, Lcom/android/calendar/e/ai;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_1

    .line 161
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 162
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 163
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 164
    if-eqz v2, :cond_0

    if-nez v3, :cond_2

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->b(Lcom/android/calendar/e/ai;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v1}, Lcom/android/calendar/e/ai;->a(Lcom/android/calendar/e/ai;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 224
    :cond_1
    :goto_0
    return-void

    .line 168
    :cond_2
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 169
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->c(Lcom/android/calendar/e/ai;)I

    move-result v0

    div-int/lit8 v5, v3, 0x2

    sub-int/2addr v0, v5

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 170
    iget v0, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    if-gez v0, :cond_3

    .line 171
    iput v8, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 173
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->e(Lcom/android/calendar/e/ai;)Lcom/android/calendar/al;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->e(Lcom/android/calendar/e/ai;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->m()Z

    move-result v0

    if-nez v0, :cond_4

    .line 177
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_4

    .line 179
    iget v5, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v5, v0

    .line 180
    iget v5, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v5, v3

    if-le v5, v0, :cond_4

    .line 181
    sub-int/2addr v0, v3

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 185
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0c00bd

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 186
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->e(Lcom/android/calendar/e/ai;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 187
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_6

    .line 189
    iget v6, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v6, v0

    .line 190
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v6, v3

    if-le v6, v0, :cond_5

    .line 191
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    sub-int v7, v0, v3

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 193
    :cond_5
    iget-object v6, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    iget-object v7, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v7}, Lcom/android/calendar/e/ai;->f(Lcom/android/calendar/e/ai;)I

    move-result v7

    sub-int/2addr v7, v5

    invoke-static {v6, v7}, Lcom/android/calendar/e/ai;->a(Lcom/android/calendar/e/ai;I)I

    move-result v6

    if-lt v6, v10, :cond_6

    .line 194
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    sub-int/2addr v0, v3

    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    sub-int/2addr v0, v7

    sub-int v0, v6, v0

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 198
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v6, v0, Landroid/util/DisplayMetrics;->density:F

    .line 199
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->g(Lcom/android/calendar/e/ai;)I

    move-result v0

    sub-int/2addr v0, v2

    int-to-float v0, v0

    const/high16 v2, 0x41000000    # 8.0f

    mul-float/2addr v2, v6

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 200
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->c(Lcom/android/calendar/e/ai;)I

    move-result v0

    iget v2, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->h(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    .line 201
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    iget-object v7, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v7}, Lcom/android/calendar/e/ai;->f(Lcom/android/calendar/e/ai;)I

    move-result v7

    sub-int v5, v7, v5

    invoke-static {v2, v5}, Lcom/android/calendar/e/ai;->a(Lcom/android/calendar/e/ai;I)I

    move-result v2

    if-lt v2, v10, :cond_7

    .line 202
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->e(Lcom/android/calendar/e/ai;)Lcom/android/calendar/al;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/al;->m()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 203
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->i(Lcom/android/calendar/e/ai;)I

    move-result v0

    sub-int v0, v3, v0

    int-to-float v0, v0

    .line 205
    :cond_7
    iget v2, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    if-gez v2, :cond_8

    .line 206
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-virtual {v2}, Lcom/android/calendar/e/ai;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c03e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 207
    iget-object v3, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v3}, Lcom/android/calendar/e/ai;->g(Lcom/android/calendar/e/ai;)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    mul-float v3, v6, v9

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 208
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->h(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/view/View;->setVisibility(I)V

    .line 209
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->j(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 210
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->j(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setX(F)V

    .line 211
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->j(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v0

    mul-float v2, v6, v9

    invoke-virtual {v0, v2}, Landroid/view/View;->setY(F)V

    .line 217
    :goto_1
    invoke-virtual {v1, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 219
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->k(Lcom/android/calendar/e/ai;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->o(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->l(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->playSoundEffect(I)V

    .line 221
    iget-object v0, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v0}, Lcom/android/calendar/e/ai;->l(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    goto/16 :goto_0

    .line 213
    :cond_8
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->j(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/view/View;->setVisibility(I)V

    .line 214
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->h(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v2, p0, Lcom/android/calendar/e/aj;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->h(Lcom/android/calendar/e/ai;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setX(F)V

    goto :goto_1
.end method
