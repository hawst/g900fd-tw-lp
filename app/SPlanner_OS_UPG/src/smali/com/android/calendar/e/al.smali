.class public Lcom/android/calendar/e/al;
.super Ljava/lang/Object;
.source "EventGeometry.java"


# instance fields
.field private a:I

.field private b:I

.field private c:F

.field private d:F

.field private e:F

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v0, p0, Lcom/android/calendar/e/al;->a:I

    .line 28
    iput v0, p0, Lcom/android/calendar/e/al;->b:I

    return-void
.end method


# virtual methods
.method a(FFLcom/android/calendar/dh;)F
    .locals 5

    .prologue
    .line 169
    iget v0, p3, Lcom/android/calendar/dh;->C:F

    .line 170
    iget v1, p3, Lcom/android/calendar/dh;->D:F

    .line 171
    iget v2, p3, Lcom/android/calendar/dh;->E:F

    .line 172
    iget v3, p3, Lcom/android/calendar/dh;->F:F

    .line 174
    cmpl-float v4, p1, v0

    if-ltz v4, :cond_5

    .line 175
    cmpg-float v0, p1, v1

    if-gtz v0, :cond_3

    .line 176
    cmpl-float v0, p2, v2

    if-ltz v0, :cond_2

    .line 177
    cmpg-float v0, p2, v3

    if-gtz v0, :cond_1

    .line 179
    const/4 v0, 0x0

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 182
    :cond_1
    sub-float v0, p2, v3

    goto :goto_0

    .line 185
    :cond_2
    sub-float v0, v2, p2

    goto :goto_0

    .line 189
    :cond_3
    sub-float v0, p1, v1

    .line 190
    cmpg-float v1, p2, v2

    if-gez v1, :cond_4

    .line 192
    sub-float v1, v2, p2

    .line 193
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0

    .line 195
    :cond_4
    cmpl-float v1, p2, v3

    if-lez v1, :cond_0

    .line 197
    sub-float v1, p2, v3

    .line 198
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0

    .line 204
    :cond_5
    sub-float/2addr v0, p1

    .line 205
    cmpg-float v1, p2, v2

    if-gez v1, :cond_6

    .line 207
    sub-float v1, v2, p2

    .line 208
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    goto :goto_0

    .line 210
    :cond_6
    cmpl-float v1, p2, v3

    if-lez v1, :cond_0

    .line 212
    sub-float v1, p2, v3

    .line 213
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    goto :goto_0
.end method

.method a(Lcom/android/calendar/dh;)I
    .locals 4

    .prologue
    .line 220
    iget v0, p1, Lcom/android/calendar/dh;->E:F

    iget v1, p0, Lcom/android/calendar/e/al;->c:F

    iget v2, p0, Lcom/android/calendar/e/al;->d:F

    const/high16 v3, 0x42700000    # 60.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/android/calendar/e/al;->d:F

    .line 43
    return-void
.end method

.method a(I)V
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Lcom/android/calendar/e/al;->a:I

    .line 39
    return-void
.end method

.method public a(IIIILcom/android/calendar/dh;)Z
    .locals 8

    .prologue
    .line 68
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/e/al;->a(IIIILcom/android/calendar/dh;ZF)Z

    move-result v0

    return v0
.end method

.method public a(IIIILcom/android/calendar/dh;ZF)Z
    .locals 12

    .prologue
    .line 72
    invoke-virtual/range {p5 .. p5}, Lcom/android/calendar/dh;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    const/4 v1, 0x0

    .line 151
    :goto_0
    return v1

    .line 76
    :cond_0
    iget v4, p0, Lcom/android/calendar/e/al;->c:F

    .line 77
    move-object/from16 v0, p5

    iget v3, v0, Lcom/android/calendar/dh;->i:I

    .line 78
    move-object/from16 v0, p5

    iget v5, v0, Lcom/android/calendar/dh;->j:I

    .line 80
    if-gt v3, p1, :cond_1

    if-ge v5, p1, :cond_2

    .line 81
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 84
    :cond_2
    move-object/from16 v0, p5

    iget v2, v0, Lcom/android/calendar/dh;->k:I

    .line 85
    move-object/from16 v0, p5

    iget v1, v0, Lcom/android/calendar/dh;->l:I

    .line 89
    if-ge v3, p1, :cond_c

    .line 90
    const/4 v2, 0x0

    move v3, v2

    .line 95
    :goto_1
    if-le v5, p1, :cond_3

    .line 96
    const/16 v1, 0x5a0

    .line 99
    :cond_3
    invoke-virtual/range {p5 .. p5}, Lcom/android/calendar/dh;->c()I

    move-result v5

    .line 100
    invoke-virtual/range {p5 .. p5}, Lcom/android/calendar/dh;->d()I

    move-result v6

    .line 101
    div-int/lit8 v2, v3, 0x3c

    .line 102
    div-int/lit8 v7, v1, 0x3c

    .line 107
    int-to-float v8, p3

    move-object/from16 v0, p5

    iput v8, v0, Lcom/android/calendar/dh;->E:F

    .line 108
    move-object/from16 v0, p5

    iget v8, v0, Lcom/android/calendar/dh;->E:F

    int-to-float v9, v3

    mul-float/2addr v9, v4

    float-to-int v9, v9

    int-to-float v9, v9

    add-float/2addr v8, v9

    move-object/from16 v0, p5

    iput v8, v0, Lcom/android/calendar/dh;->E:F

    .line 109
    move-object/from16 v0, p5

    iget v8, v0, Lcom/android/calendar/dh;->E:F

    int-to-float v9, v2

    iget v10, p0, Lcom/android/calendar/e/al;->d:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    move-object/from16 v0, p5

    iput v8, v0, Lcom/android/calendar/dh;->E:F

    .line 111
    sub-int v2, v7, v2

    .line 112
    const/4 v7, 0x2

    if-gt v2, v7, :cond_4

    .line 113
    const/4 v2, 0x0

    .line 116
    :cond_4
    move-object/from16 v0, p5

    iget v7, v0, Lcom/android/calendar/dh;->E:F

    sub-int/2addr v1, v3

    int-to-float v1, v1

    mul-float/2addr v1, v4

    add-float/2addr v1, v7

    move-object/from16 v0, p5

    iput v1, v0, Lcom/android/calendar/dh;->F:F

    .line 117
    move-object/from16 v0, p5

    iget v1, v0, Lcom/android/calendar/dh;->F:F

    int-to-float v2, v2

    iget v3, p0, Lcom/android/calendar/e/al;->d:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    move-object/from16 v0, p5

    iput v1, v0, Lcom/android/calendar/dh;->F:F

    .line 120
    move-object/from16 v0, p5

    iget v1, v0, Lcom/android/calendar/dh;->F:F

    move-object/from16 v0, p5

    iget v2, v0, Lcom/android/calendar/dh;->E:F

    iget v3, p0, Lcom/android/calendar/e/al;->e:F

    add-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_5

    .line 121
    move-object/from16 v0, p5

    iget v1, v0, Lcom/android/calendar/dh;->E:F

    iget v2, p0, Lcom/android/calendar/e/al;->e:F

    add-float/2addr v1, v2

    move-object/from16 v0, p5

    iput v1, v0, Lcom/android/calendar/dh;->F:F

    .line 123
    :cond_5
    move-object/from16 v0, p5

    iget v1, v0, Lcom/android/calendar/dh;->F:F

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    move-object/from16 v0, p5

    iput v1, v0, Lcom/android/calendar/dh;->F:F

    .line 124
    iget v1, p0, Lcom/android/calendar/e/al;->g:I

    int-to-float v1, v1

    .line 126
    if-eqz p6, :cond_6

    .line 127
    iget v1, p0, Lcom/android/calendar/e/al;->g:I

    int-to-float v1, v1

    mul-float v1, v1, p7

    .line 130
    :cond_6
    iget v2, p0, Lcom/android/calendar/e/al;->a:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p4, v2

    int-to-float v2, v2

    int-to-float v3, v6

    div-float/2addr v2, v3

    .line 131
    sget v3, Lcom/android/calendar/e/g;->a:I

    if-le v6, v3, :cond_b

    .line 132
    iget v2, p0, Lcom/android/calendar/e/al;->a:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p4, v2

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/e/g;->a:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 133
    iget v3, p0, Lcom/android/calendar/e/al;->f:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_8

    .line 135
    iget v1, p0, Lcom/android/calendar/e/al;->a:I

    int-to-float v1, v1

    sget v3, Lcom/android/calendar/e/g;->a:I

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    div-float/2addr v1, v3

    .line 136
    sget v3, Lcom/android/calendar/e/g;->a:I

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    div-float v3, v2, v3

    add-float/2addr v1, v3

    sub-float v1, v2, v1

    .line 141
    :goto_2
    sget v3, Lcom/android/calendar/e/g;->a:I

    if-ne v5, v3, :cond_a

    iget v3, p0, Lcom/android/calendar/e/al;->f:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    .line 142
    iget v3, p0, Lcom/android/calendar/e/al;->a:I

    add-int/2addr v3, p2

    int-to-float v3, v3

    int-to-float v4, v5

    mul-float/2addr v1, v4

    add-float/2addr v1, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v1, v3

    move-object/from16 v0, p5

    iput v1, v0, Lcom/android/calendar/dh;->C:F

    .line 143
    move-object/from16 v0, p5

    iget v1, v0, Lcom/android/calendar/dh;->C:F

    add-float/2addr v1, v2

    move-object/from16 v0, p5

    iput v1, v0, Lcom/android/calendar/dh;->D:F

    .line 151
    :cond_7
    :goto_3
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 137
    :cond_8
    iget v3, p0, Lcom/android/calendar/e/al;->f:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_9

    if-eqz p6, :cond_b

    .line 138
    :cond_9
    sget v3, Lcom/android/calendar/e/g;->a:I

    int-to-float v3, v3

    div-float v3, v1, v3

    sub-float/2addr v2, v3

    move v11, v2

    move v2, v1

    move v1, v11

    goto :goto_2

    .line 145
    :cond_a
    iget v2, p0, Lcom/android/calendar/e/al;->a:I

    add-int/2addr v2, p2

    int-to-float v2, v2

    int-to-float v3, v5

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    move-object/from16 v0, p5

    iput v2, v0, Lcom/android/calendar/dh;->C:F

    .line 146
    move-object/from16 v0, p5

    iget v2, v0, Lcom/android/calendar/dh;->C:F

    add-float/2addr v1, v2

    move-object/from16 v0, p5

    iput v1, v0, Lcom/android/calendar/dh;->D:F

    .line 147
    add-int/lit8 v1, v6, -0x1

    if-ge v5, v1, :cond_7

    sget v1, Lcom/android/calendar/e/g;->a:I

    if-ge v5, v1, :cond_7

    .line 148
    move-object/from16 v0, p5

    iget v1, v0, Lcom/android/calendar/dh;->D:F

    iget v2, p0, Lcom/android/calendar/e/al;->b:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    move-object/from16 v0, p5

    iput v1, v0, Lcom/android/calendar/dh;->D:F

    goto :goto_3

    :cond_b
    move v11, v2

    move v2, v1

    move v1, v11

    goto :goto_2

    :cond_c
    move v3, v2

    goto/16 :goto_1
.end method

.method a(Lcom/android/calendar/dh;Landroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 158
    iget v0, p1, Lcom/android/calendar/dh;->C:F

    iget v1, p2, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p1, Lcom/android/calendar/dh;->D:F

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/android/calendar/dh;->E:F

    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p1, Lcom/android/calendar/dh;->F:F

    iget v1, p2, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 160
    const/4 v0, 0x1

    .line 162
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Lcom/android/calendar/dh;)I
    .locals 4

    .prologue
    .line 224
    iget v0, p1, Lcom/android/calendar/dh;->F:F

    iget v1, p0, Lcom/android/calendar/e/al;->c:F

    iget v2, p0, Lcom/android/calendar/e/al;->d:F

    const/high16 v3, 0x42700000    # 60.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public b(F)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/android/calendar/e/al;->e:F

    .line 51
    return-void
.end method

.method b(I)V
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lcom/android/calendar/e/al;->b:I

    .line 47
    return-void
.end method

.method public c(F)V
    .locals 1

    .prologue
    .line 54
    const/high16 v0, 0x42700000    # 60.0f

    div-float v0, p1, v0

    iput v0, p0, Lcom/android/calendar/e/al;->c:F

    .line 55
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/android/calendar/e/al;->g:I

    .line 59
    return-void
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 62
    iput p1, p0, Lcom/android/calendar/e/al;->f:I

    .line 63
    return-void
.end method
