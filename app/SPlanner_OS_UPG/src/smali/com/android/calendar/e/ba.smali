.class Lcom/android/calendar/e/ba;
.super Ljava/lang/Object;
.source "WeekAllDayView.java"


# instance fields
.field public a:I

.field public b:I

.field final synthetic c:Lcom/android/calendar/e/ao;

.field private d:I

.field private e:I

.field private f:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Lcom/android/calendar/e/ao;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 411
    iput-object p1, p0, Lcom/android/calendar/e/ba;->c:Lcom/android/calendar/e/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412
    const v0, 0x7f0c027d

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ba;->d:I

    .line 413
    const v0, 0x7f0c027c

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ba;->e:I

    .line 414
    invoke-static {p1}, Lcom/android/calendar/e/ao;->b(Lcom/android/calendar/e/ao;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ba;->a:I

    .line 415
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/calendar/e/ba;->b:I

    .line 417
    invoke-direct {p0}, Lcom/android/calendar/e/ba;->b()Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ba;->f:Landroid/animation/ValueAnimator;

    .line 418
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/e/ba;)I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lcom/android/calendar/e/ba;->e:I

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/e/ba;)I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lcom/android/calendar/e/ba;->d:I

    return v0
.end method

.method private b()Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 421
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 422
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 423
    new-instance v1, Lcom/android/calendar/e/bb;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/bb;-><init>(Lcom/android/calendar/e/ba;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 439
    new-instance v1, Lcom/android/calendar/e/bc;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/bc;-><init>(Lcom/android/calendar/e/ba;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 456
    return-object v0

    .line 421
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/android/calendar/e/ba;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 461
    return-void
.end method
