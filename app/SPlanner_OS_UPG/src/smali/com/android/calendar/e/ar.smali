.class Lcom/android/calendar/e/ar;
.super Ljava/lang/Object;
.source "WeekAllDayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/ao;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/ao;)V
    .locals 0

    .prologue
    .line 2761
    iput-object p1, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2764
    iget-object v0, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2765
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 2766
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 2769
    sget-object v2, Lcom/android/calendar/month/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 2770
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2772
    :try_start_0
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2780
    :cond_0
    sget-object v2, Lcom/android/calendar/e/ai;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 2781
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2783
    :try_start_1
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2790
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->d(Lcom/android/calendar/e/ao;)Landroid/app/DialogFragment;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->d(Lcom/android/calendar/e/ao;)Landroid/app/DialogFragment;

    move-result-object v0

    instance-of v0, v0, Lcom/android/calendar/e/ai;

    if-eqz v0, :cond_2

    .line 2792
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->d(Lcom/android/calendar/e/ao;)Landroid/app/DialogFragment;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/e/ai;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2796
    iget-object v0, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->e(Lcom/android/calendar/e/ao;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2797
    iget-object v0, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->f(Lcom/android/calendar/e/ao;)V

    .line 2802
    :cond_2
    :goto_0
    :try_start_3
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3

    .line 2807
    :goto_1
    return-void

    .line 2773
    :catch_0
    move-exception v0

    .line 2775
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2784
    :catch_1
    move-exception v0

    .line 2786
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2793
    :catch_2
    move-exception v0

    .line 2794
    :try_start_4
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Fail to add DialogFragment"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2796
    iget-object v0, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->e(Lcom/android/calendar/e/ao;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2797
    iget-object v0, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->f(Lcom/android/calendar/e/ao;)V

    goto :goto_0

    .line 2796
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v1}, Lcom/android/calendar/e/ao;->e(Lcom/android/calendar/e/ao;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2797
    iget-object v1, p0, Lcom/android/calendar/e/ar;->a:Lcom/android/calendar/e/ao;

    invoke-static {v1}, Lcom/android/calendar/e/ao;->f(Lcom/android/calendar/e/ao;)V

    :cond_3
    throw v0

    .line 2803
    :catch_3
    move-exception v0

    .line 2805
    invoke-static {}, Lcom/android/calendar/e/ao;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to show DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
