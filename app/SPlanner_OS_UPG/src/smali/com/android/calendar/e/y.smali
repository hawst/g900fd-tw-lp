.class Lcom/android/calendar/e/y;
.super Landroid/content/AsyncQueryHandler;
.source "DayView.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;


# direct methods
.method public constructor <init>(Lcom/android/calendar/e/g;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 6587
    iput-object p1, p0, Lcom/android/calendar/e/y;->a:Lcom/android/calendar/e/g;

    .line 6588
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 6589
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    .line 6593
    invoke-super {p0, p1, p2, p3}, Landroid/content/AsyncQueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 6594
    if-nez p3, :cond_0

    .line 6609
    :goto_0
    return-void

    .line 6598
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6599
    iget-object v1, p0, Lcom/android/calendar/e/y;->a:Lcom/android/calendar/e/g;

    iget-object v2, v1, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/e/y;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->F(Lcom/android/calendar/e/g;)I

    move-result v3

    iget-object v1, p0, Lcom/android/calendar/e/y;->a:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->F(Lcom/android/calendar/e/g;)I

    move-result v4

    const/4 v5, 0x0

    move-object v1, p3

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;IIZ)V

    .line 6600
    invoke-virtual {v0}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 6601
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 6602
    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/dh;

    .line 6603
    iget-wide v4, v1, Lcom/android/calendar/dh;->n:J

    iget-object v3, p0, Lcom/android/calendar/e/y;->a:Lcom/android/calendar/e/g;

    invoke-static {v3}, Lcom/android/calendar/e/g;->G(Lcom/android/calendar/e/g;)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, v1, Lcom/android/calendar/dh;->n:J

    iget-wide v6, v1, Lcom/android/calendar/dh;->m:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    :cond_2
    invoke-virtual {v1}, Lcom/android/calendar/dh;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6604
    :cond_3
    invoke-interface {v2}, Ljava/util/ListIterator;->remove()V

    goto :goto_1

    .line 6607
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/e/y;->a:Lcom/android/calendar/e/g;

    invoke-static {v1, v0}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;Ljava/util/ArrayList;)V

    .line 6608
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method
