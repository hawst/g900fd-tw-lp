.class Lcom/android/calendar/e/ah;
.super Ljava/lang/Object;
.source "DayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;


# direct methods
.method private constructor <init>(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 6273
    iput-object p1, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/e/g;Lcom/android/calendar/e/h;)V
    .locals 0

    .prologue
    .line 6273
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ah;-><init>(Lcom/android/calendar/e/g;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/32 v6, 0x493e0

    .line 6276
    iget-object v0, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    iget-object v0, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6277
    iget-object v0, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;Landroid/text/format/Time;)Landroid/text/format/Time;

    .line 6281
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 6282
    iget-object v2, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;)Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 6284
    iget-object v2, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    iget-boolean v2, v2, Lcom/android/calendar/e/g;->c:Z

    if-nez v2, :cond_0

    .line 6285
    iget-object v2, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 6286
    iget-object v2, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    invoke-static {v2}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    invoke-static {v3}, Lcom/android/calendar/e/g;->C(Lcom/android/calendar/e/g;)Lcom/android/calendar/e/ah;

    move-result-object v3

    rem-long v4, v0, v6

    sub-long v4, v6, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 6290
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    iget-object v3, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    invoke-static {v3}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;)Landroid/text/format/Time;

    move-result-object v3

    iget-wide v4, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    invoke-static {v2, v0}, Lcom/android/calendar/e/g;->h(Lcom/android/calendar/e/g;I)I

    .line 6291
    iget-object v0, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 6292
    return-void

    .line 6279
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    iget-object v2, v2, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/calendar/e/ah;->a:Lcom/android/calendar/e/g;

    invoke-static {v3}, Lcom/android/calendar/e/g;->B(Lcom/android/calendar/e/g;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->a(Lcom/android/calendar/e/g;Landroid/text/format/Time;)Landroid/text/format/Time;

    goto :goto_0
.end method
