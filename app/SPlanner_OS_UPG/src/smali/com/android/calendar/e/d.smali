.class public Lcom/android/calendar/e/d;
.super Landroid/app/Fragment;
.source "DayFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;
.implements Lcom/android/calendar/h/f;


# static fields
.field static c:Z

.field static d:I

.field static e:I

.field static f:Z

.field private static final g:Ljava/lang/String;

.field private static o:I


# instance fields
.field a:Landroid/view/animation/Animation;

.field b:Landroid/view/animation/Animation;

.field private h:Lcom/android/calendar/al;

.field private i:Lcom/android/calendar/a;

.field private j:Lcom/android/calendar/dj;

.field private k:Lcom/android/calendar/e/a;

.field private l:I

.field private m:Lcom/android/calendar/h/c;

.field private n:Z

.field private p:I

.field private q:Landroid/text/format/Time;

.field private r:Landroid/text/format/Time;

.field private final s:Ljava/lang/Runnable;

.field private t:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/android/calendar/e/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/e/d;->g:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/e/d;->o:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 70
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/e/d;->l:I

    .line 73
    iput-boolean v1, p0, Lcom/android/calendar/e/d;->n:Z

    .line 75
    iput v1, p0, Lcom/android/calendar/e/d;->p:I

    .line 77
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    .line 85
    new-instance v0, Lcom/android/calendar/e/e;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/e;-><init>(Lcom/android/calendar/e/d;)V

    iput-object v0, p0, Lcom/android/calendar/e/d;->s:Ljava/lang/Runnable;

    .line 343
    new-instance v0, Lcom/android/calendar/e/f;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/f;-><init>(Lcom/android/calendar/e/d;)V

    iput-object v0, p0, Lcom/android/calendar/e/d;->t:Landroid/view/animation/Animation$AnimationListener;

    .line 99
    iget-object v0, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 100
    return-void
.end method

.method public constructor <init>(JI)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 102
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 70
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/e/d;->l:I

    .line 73
    iput-boolean v4, p0, Lcom/android/calendar/e/d;->n:Z

    .line 75
    iput v4, p0, Lcom/android/calendar/e/d;->p:I

    .line 77
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    .line 85
    new-instance v0, Lcom/android/calendar/e/e;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/e;-><init>(Lcom/android/calendar/e/d;)V

    iput-object v0, p0, Lcom/android/calendar/e/d;->s:Ljava/lang/Runnable;

    .line 343
    new-instance v0, Lcom/android/calendar/e/f;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/f;-><init>(Lcom/android/calendar/e/d;)V

    iput-object v0, p0, Lcom/android/calendar/e/d;->t:Landroid/view/animation/Animation$AnimationListener;

    .line 103
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 104
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 105
    iput p3, p0, Lcom/android/calendar/e/d;->l:I

    .line 106
    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    .line 108
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 109
    iget-object v0, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 118
    :goto_0
    return-void

    .line 111
    :cond_0
    if-gez v1, :cond_2

    .line 112
    const/4 v1, 0x1

    const/16 v2, 0x76e

    invoke-virtual {v0, v1, v4, v2}, Landroid/text/format/Time;->set(III)V

    .line 116
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto :goto_0

    .line 113
    :cond_2
    if-lez v1, :cond_1

    .line 114
    const/16 v1, 0x1f

    const/16 v2, 0xb

    const/16 v3, 0x7f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/format/Time;->set(III)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/e/d;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/e/d;->s:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(Landroid/text/format/Time;ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 263
    iget-object v0, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 265
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    if-nez v0, :cond_1

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/e/d;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    .line 277
    invoke-virtual {v0, p1}, Lcom/android/calendar/e/a;->a(Landroid/text/format/Time;)I

    move-result v1

    .line 278
    if-nez v1, :cond_3

    .line 280
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/e/a;->a(Landroid/text/format/Time;ZZ)V

    .line 281
    if-eqz p3, :cond_2

    .line 282
    iget-object v1, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v1}, Lcom/android/calendar/e/a;->e()V

    .line 283
    iget-object v1, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v1}, Lcom/android/calendar/e/a;->setListHeight()I

    .line 284
    iget v1, p1, Landroid/text/format/Time;->hour:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/a;->setFirstVisibleHour(I)V

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/e/d;->l:I

    if-le v0, v3, :cond_0

    .line 287
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    invoke-virtual {v0}, Lcom/android/calendar/a;->invalidateOptionsMenu()V

    goto :goto_0

    .line 290
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    if-eqz v0, :cond_0

    .line 295
    iget v0, p0, Lcom/android/calendar/e/d;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/e/d;->p:I

    .line 296
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0}, Lcom/android/calendar/e/a;->invalidate()V

    .line 298
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    invoke-virtual {v0}, Lcom/android/calendar/a;->a()V

    .line 299
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    invoke-virtual {v0}, Lcom/android/calendar/a;->invalidateOptionsMenu()V

    .line 300
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    invoke-virtual {v0, v2}, Lcom/android/calendar/a;->a(I)V

    .line 301
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0}, Lcom/android/calendar/e/a;->e()V

    .line 302
    if-lez v1, :cond_6

    .line 303
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    invoke-virtual {v0}, Lcom/android/calendar/a;->b()V

    .line 304
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    iget-object v1, p0, Lcom/android/calendar/e/d;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/android/calendar/a;->a(Landroid/view/animation/Animation;)V

    .line 305
    iget-object v0, p0, Lcom/android/calendar/e/d;->a:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/calendar/e/d;->t:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 310
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_4

    .line 312
    invoke-virtual {v0}, Lcom/android/calendar/e/g;->h()V

    .line 315
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/g;->setPause(Z)V

    .line 316
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    .line 317
    invoke-virtual {v0}, Lcom/android/calendar/e/a;->c()V

    .line 318
    if-eqz p3, :cond_5

    .line 319
    iget v1, p1, Landroid/text/format/Time;->hour:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/a;->setFirstVisibleHour(I)V

    .line 321
    :cond_5
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/e/a;->a(Landroid/text/format/Time;ZZ)V

    .line 322
    if-nez p3, :cond_0

    .line 323
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    .line 324
    invoke-virtual {v0, v3}, Lcom/android/calendar/e/g;->a(Z)V

    goto/16 :goto_0

    .line 307
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    iget-object v1, p0, Lcom/android/calendar/e/d;->b:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/android/calendar/a;->b(Landroid/view/animation/Animation;)V

    .line 308
    iget-object v0, p0, Lcom/android/calendar/e/d;->b:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/calendar/e/d;->t:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/android/calendar/e/d;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/e/d;)Lcom/android/calendar/e/a;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/e/d;)I
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lcom/android/calendar/e/d;->p:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/calendar/e/d;->p:I

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/e/d;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/android/calendar/e/d;->p:I

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/e/d;)Lcom/android/calendar/a;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 205
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 208
    invoke-direct {p0}, Lcom/android/calendar/e/d;->h()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Z)V

    .line 209
    invoke-direct {p0}, Lcom/android/calendar/e/d;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    sput-boolean v2, Lcom/android/calendar/e/ao;->b:Z

    .line 214
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 208
    goto :goto_0
.end method

.method private h()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 217
    iget-object v1, p0, Lcom/android/calendar/e/d;->r:Landroid/text/format/Time;

    if-nez v1, :cond_1

    .line 223
    :cond_0
    :goto_0
    return v0

    .line 220
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/d;->r:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 221
    iget-object v1, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 222
    iget-object v2, p0, Lcom/android/calendar/e/d;->r:Landroid/text/format/Time;

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/e/d;->r:Landroid/text/format/Time;

    iget-wide v4, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 223
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private i()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 330
    iget-object v1, p0, Lcom/android/calendar/e/d;->h:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->g()I

    move-result v1

    .line 331
    iget v2, p0, Lcom/android/calendar/e/d;->l:I

    if-ne v2, v0, :cond_1

    .line 332
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 340
    :cond_0
    :goto_0
    return v0

    .line 335
    :cond_1
    iget v2, p0, Lcom/android/calendar/e/d;->l:I

    const/4 v3, 0x7

    if-ne v2, v3, :cond_2

    .line 336
    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 340
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/android/calendar/e/g;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x0

    .line 230
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0}, Lcom/android/calendar/e/a;->getTimelineView()Lcom/android/calendar/e/g;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 401
    iget-wide v4, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v6, 0x20

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 405
    iget-object v3, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    iget-wide v4, p1, Lcom/android/calendar/aq;->p:J

    const-wide/16 v6, 0x1

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-wide v4, p1, Lcom/android/calendar/aq;->p:J

    const-wide/16 v6, 0x8

    and-long/2addr v4, v6

    cmp-long v4, v4, v8

    if-eqz v4, :cond_2

    :goto_1
    invoke-direct {p0, v3, v0, v1}, Lcom/android/calendar/e/d;->a(Landroid/text/format/Time;ZZ)V

    .line 410
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 405
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 407
    :cond_3
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 408
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->d()V

    goto :goto_2
.end method

.method public b()Lcom/android/calendar/e/a;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    return-object v0
.end method

.method public c()J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 372
    iget-object v2, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    if-nez v2, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-wide v0

    .line 375
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v2

    .line 376
    if-eqz v2, :cond_0

    .line 379
    invoke-virtual {v2}, Lcom/android/calendar/e/g;->getSelectedTimeInMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 384
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-void

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0, v1, v1}, Lcom/android/calendar/e/a;->a(ZZ)V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0}, Lcom/android/calendar/e/a;->f()V

    .line 427
    :cond_0
    return-void
.end method

.method public g()J
    .locals 4

    .prologue
    .line 392
    const-wide/16 v0, 0xa0

    .line 393
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->getId()I

    move-result v2

    const v3, 0x7f12006d

    if-ne v2, v3, :cond_0

    .line 394
    const-wide/32 v2, 0x20000000

    or-long/2addr v0, v2

    .line 396
    :cond_0
    return-wide v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 414
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 417
    sget-object v0, Lcom/android/calendar/e/d;->g:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown request code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 157
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 158
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/d;->h:Lcom/android/calendar/al;

    .line 159
    instance-of v0, p1, Lcom/android/calendar/a;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 160
    check-cast v0, Lcom/android/calendar/a;

    iput-object v0, p0, Lcom/android/calendar/e/d;->i:Lcom/android/calendar/a;

    .line 162
    :cond_0
    invoke-static {p1}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/d;->r:Landroid/text/format/Time;

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/e/d;->r:Landroid/text/format/Time;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 123
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 124
    const v1, 0x7f05000f

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/d;->a:Landroid/view/animation/Animation;

    .line 125
    const v1, 0x7f050010

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/d;->b:Landroid/view/animation/Animation;

    .line 126
    new-instance v1, Lcom/android/calendar/dj;

    invoke-direct {v1, v0}, Lcom/android/calendar/dj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/e/d;->j:Lcom/android/calendar/dj;

    .line 127
    if-eqz p1, :cond_1

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/d;->n:Z

    .line 133
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/d;->m:Lcom/android/calendar/h/c;

    .line 135
    iget-object v0, p0, Lcom/android/calendar/e/d;->m:Lcom/android/calendar/h/c;

    invoke-virtual {v0, p0}, Lcom/android/calendar/h/c;->a(Lcom/android/calendar/h/f;)V

    .line 137
    :cond_0
    return-void

    .line 130
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/d;->n:Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 141
    iget-object v0, p0, Lcom/android/calendar/e/d;->s:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 142
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 143
    if-eqz p3, :cond_0

    .line 144
    const-string v0, "key_number_of_day"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "key_number_of_day"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/d;->l:I

    .line 148
    :cond_0
    new-instance v0, Lcom/android/calendar/e/be;

    invoke-virtual {p0}, Lcom/android/calendar/e/d;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/e/d;->j:Lcom/android/calendar/dj;

    iget v5, p0, Lcom/android/calendar/e/d;->l:I

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/e/be;-><init>(Landroid/content/Context;Lcom/android/calendar/al;Lcom/android/calendar/dj;Lcom/android/calendar/e/d;I)V

    iput-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    .line 149
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    iget-object v1, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    invoke-virtual {v0, v1, v6, v6}, Lcom/android/calendar/e/a;->a(Landroid/text/format/Time;ZZ)V

    .line 150
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0}, Lcom/android/calendar/e/a;->e()V

    .line 151
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/d;->setHasOptionsMenu(Z)V

    .line 152
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0}, Lcom/android/calendar/e/a;->a()V

    .line 257
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->i()V

    .line 258
    iget-object v0, p0, Lcom/android/calendar/e/d;->j:Lcom/android/calendar/dj;

    invoke-virtual {v0}, Lcom/android/calendar/dj;->b()V

    .line 259
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 260
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 200
    invoke-super {p0, p1}, Landroid/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 201
    invoke-direct {p0}, Lcom/android/calendar/e/d;->f()V

    .line 202
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 171
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 173
    iget-object v0, p0, Lcom/android/calendar/e/d;->j:Lcom/android/calendar/dj;

    invoke-virtual {v0}, Lcom/android/calendar/dj;->a()V

    .line 174
    iget-object v0, p0, Lcom/android/calendar/e/d;->s:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 175
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->d()V

    .line 176
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0}, Lcom/android/calendar/e/a;->d()V

    .line 178
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v2

    .line 179
    invoke-virtual {v2}, Lcom/android/calendar/e/g;->a()V

    .line 180
    invoke-virtual {v2}, Lcom/android/calendar/e/g;->j()V

    .line 181
    iget-boolean v0, p0, Lcom/android/calendar/e/d;->n:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 182
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/e/d;->q:Landroid/text/format/Time;

    invoke-virtual {v2, v3, v0, v1}, Lcom/android/calendar/e/g;->a(Landroid/text/format/Time;ZZ)V

    .line 183
    invoke-virtual {v2}, Lcom/android/calendar/e/g;->b()V

    .line 184
    sget-boolean v0, Lcom/android/calendar/e/d;->c:Z

    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    sget v1, Lcom/android/calendar/e/d;->d:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/g;->setCellHeight(I)V

    .line 186
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    sget v1, Lcom/android/calendar/e/d;->e:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/e/g;->setViewScrollY(I)V

    .line 190
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/d;->r:Landroid/text/format/Time;

    .line 195
    :goto_2
    invoke-direct {p0}, Lcom/android/calendar/e/d;->f()V

    .line 196
    return-void

    :cond_0
    move v0, v1

    .line 181
    goto :goto_0

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/d;->k:Lcom/android/calendar/e/a;

    invoke-virtual {v0}, Lcom/android/calendar/e/a;->e()V

    goto :goto_1

    .line 193
    :cond_2
    new-instance v0, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/e/d;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/e/d;->r:Landroid/text/format/Time;

    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 239
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 241
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->c()J

    move-result-wide v0

    .line 242
    iget-object v2, p0, Lcom/android/calendar/e/d;->h:Lcom/android/calendar/al;

    invoke-virtual {v2, v0, v1}, Lcom/android/calendar/al;->a(J)V

    .line 243
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 244
    const-string v2, "key_restore_time"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 246
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    if-nez v0, :cond_1

    .line 247
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/e/d;->o:I

    .line 251
    :goto_0
    const-string v0, "key_number_of_day"

    iget v1, p0, Lcom/android/calendar/e/d;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 252
    return-void

    .line 249
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->getSelectionMode()I

    move-result v0

    sput v0, Lcom/android/calendar/e/d;->o:I

    goto :goto_0
.end method
