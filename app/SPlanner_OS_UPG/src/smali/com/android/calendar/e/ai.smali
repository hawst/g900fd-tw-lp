.class public Lcom/android/calendar/e/ai;
.super Landroid/app/DialogFragment;
.source "DayViewHoverFragment.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/content/Context;

.field private f:Lcom/android/calendar/al;

.field private g:Z

.field private h:Lcom/android/calendar/dh;

.field private i:Lcom/android/calendar/hc;

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Landroid/os/Handler;

.field private q:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/android/calendar/e/ai;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/e/ai;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 92
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 78
    iput-boolean v2, p0, Lcom/android/calendar/e/ai;->g:Z

    .line 79
    new-instance v0, Lcom/android/calendar/dh;

    invoke-direct {v0}, Lcom/android/calendar/dh;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    .line 80
    new-instance v0, Lcom/android/calendar/hc;

    invoke-direct {v0}, Lcom/android/calendar/hc;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    .line 81
    iput-boolean v2, p0, Lcom/android/calendar/e/ai;->j:Z

    .line 83
    iput v1, p0, Lcom/android/calendar/e/ai;->k:I

    .line 84
    iput v1, p0, Lcom/android/calendar/e/ai;->l:I

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->p:Landroid/os/Handler;

    .line 155
    new-instance v0, Lcom/android/calendar/e/aj;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/aj;-><init>(Lcom/android/calendar/e/ai;)V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->q:Ljava/lang/Runnable;

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/dh;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 95
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 78
    iput-boolean v2, p0, Lcom/android/calendar/e/ai;->g:Z

    .line 79
    new-instance v0, Lcom/android/calendar/dh;

    invoke-direct {v0}, Lcom/android/calendar/dh;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    .line 80
    new-instance v0, Lcom/android/calendar/hc;

    invoke-direct {v0}, Lcom/android/calendar/hc;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    .line 81
    iput-boolean v2, p0, Lcom/android/calendar/e/ai;->j:Z

    .line 83
    iput v1, p0, Lcom/android/calendar/e/ai;->k:I

    .line 84
    iput v1, p0, Lcom/android/calendar/e/ai;->l:I

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->p:Landroid/os/Handler;

    .line 155
    new-instance v0, Lcom/android/calendar/e/aj;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/aj;-><init>(Lcom/android/calendar/e/ai;)V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->q:Ljava/lang/Runnable;

    .line 96
    iput-object p1, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    .line 97
    iget-object v0, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ai;->f:Lcom/android/calendar/al;

    .line 98
    iput-object p2, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/ai;->g:Z

    .line 100
    iput-boolean p3, p0, Lcom/android/calendar/e/ai;->j:Z

    .line 101
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->a()V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/hc;Z)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 78
    iput-boolean v1, p0, Lcom/android/calendar/e/ai;->g:Z

    .line 79
    new-instance v0, Lcom/android/calendar/dh;

    invoke-direct {v0}, Lcom/android/calendar/dh;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    .line 80
    new-instance v0, Lcom/android/calendar/hc;

    invoke-direct {v0}, Lcom/android/calendar/hc;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    .line 81
    iput-boolean v1, p0, Lcom/android/calendar/e/ai;->j:Z

    .line 83
    iput v2, p0, Lcom/android/calendar/e/ai;->k:I

    .line 84
    iput v2, p0, Lcom/android/calendar/e/ai;->l:I

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->p:Landroid/os/Handler;

    .line 155
    new-instance v0, Lcom/android/calendar/e/aj;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/aj;-><init>(Lcom/android/calendar/e/ai;)V

    iput-object v0, p0, Lcom/android/calendar/e/ai;->q:Ljava/lang/Runnable;

    .line 105
    iput-object p1, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    .line 106
    iget-object v0, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ai;->f:Lcom/android/calendar/al;

    .line 107
    iput-object p2, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    .line 108
    iput-boolean v1, p0, Lcom/android/calendar/e/ai;->g:Z

    .line 109
    iput-boolean p3, p0, Lcom/android/calendar/e/ai;->j:Z

    .line 110
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->a()V

    .line 111
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 228
    iget v0, p0, Lcom/android/calendar/e/ai;->n:I

    add-int/lit8 v0, v0, 0x1

    div-int v0, p1, v0

    .line 230
    iget v1, p0, Lcom/android/calendar/e/ai;->o:I

    if-lt v0, v1, :cond_0

    .line 231
    iget v0, p0, Lcom/android/calendar/e/ai;->o:I

    add-int/lit8 v0, v0, -0x1

    .line 233
    :cond_0
    if-gez v0, :cond_1

    .line 234
    const/4 v0, 0x0

    .line 236
    :cond_1
    return v0
.end method

.method static synthetic a(Lcom/android/calendar/e/ai;I)I
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ai;->a(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/e/ai;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->q:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(JJZ)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 518
    const-string v0, ""

    .line 519
    iget-object v1, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    invoke-virtual {v1}, Lcom/android/calendar/dh;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 533
    :cond_0
    :goto_0
    return-object v0

    .line 522
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    .line 523
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 524
    if-eqz p5, :cond_2

    .line 525
    iget-object v0, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 526
    :cond_2
    cmp-long v1, p1, v4

    if-eqz v1, :cond_0

    cmp-long v1, p3, v4

    if-eqz v1, :cond_0

    .line 527
    invoke-virtual {v2, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 528
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 529
    invoke-virtual {v2, p3, p4}, Landroid/text/format/Time;->set(J)V

    .line 530
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 531
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ~ "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(JLandroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x2000

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 339
    invoke-virtual {p0}, Lcom/android/calendar/e/ai;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-object v3

    .line 343
    :cond_1
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 345
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 346
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "description"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 349
    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 351
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v6, :cond_2

    .line 352
    const/4 v0, 0x0

    const/16 v2, 0x2000

    invoke-virtual {v3, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 356
    :cond_2
    if-eqz v1, :cond_0

    .line 357
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 356
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 357
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    if-nez v0, :cond_0

    .line 117
    :cond_0
    return-void
.end method

.method private a(Lcom/android/calendar/dh;Lcom/android/calendar/hc;Z)V
    .locals 7

    .prologue
    .line 304
    if-eqz p3, :cond_3

    .line 305
    iget-object v0, p1, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->a(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p1}, Lcom/android/calendar/dh;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    iget-wide v2, p1, Lcom/android/calendar/dh;->m:J

    iget-wide v4, p1, Lcom/android/calendar/dh;->n:J

    iget-boolean v6, p1, Lcom/android/calendar/dh;->f:Z

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/calendar/e/ai;->a(JJZ)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->b(Ljava/lang/String;)V

    .line 309
    :cond_0
    iget-object v0, p1, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 310
    iget-object v0, p1, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->c(Ljava/lang/String;)V

    .line 312
    :cond_1
    iget-wide v0, p1, Lcom/android/calendar/dh;->b:J

    iget-object v2, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/e/ai;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 313
    if-eqz v0, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 314
    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->a(Ljava/lang/CharSequence;)V

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->c:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->b(I)V

    .line 317
    iget-boolean v0, p1, Lcom/android/calendar/dh;->o:Z

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->a(Z)V

    .line 318
    iget-boolean v0, p1, Lcom/android/calendar/dh;->p:Z

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->b(Z)V

    .line 319
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->e()V

    .line 336
    :goto_0
    return-void

    .line 321
    :cond_3
    iget-object v0, p2, Lcom/android/calendar/hc;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 322
    iget-object v0, p2, Lcom/android/calendar/hc;->h:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->a(Ljava/lang/String;)V

    .line 324
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->d()V

    .line 325
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/android/calendar/hc;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 326
    iget-object v0, p2, Lcom/android/calendar/hc;->l:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->b(Ljava/lang/String;)V

    .line 328
    :cond_5
    iget-object v0, p2, Lcom/android/calendar/hc;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 329
    iget-object v0, p2, Lcom/android/calendar/hc;->n:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->c(Ljava/lang/String;)V

    .line 331
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    iget v0, v0, Lcom/android/calendar/hc;->m:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->b(I)V

    .line 332
    iget-boolean v0, p2, Lcom/android/calendar/hc;->d:Z

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->a(Z)V

    .line 333
    iget v0, p2, Lcom/android/calendar/hc;->g:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ai;->c(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 399
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f12020e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 400
    if-eqz v0, :cond_0

    .line 401
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 403
    :cond_0
    const v0, 0x7f12020f

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/ai;->a(Ljava/lang/CharSequence;I)V

    .line 404
    return-void
.end method

.method private a(Ljava/lang/CharSequence;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 407
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 408
    if-eqz v0, :cond_0

    .line 409
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 410
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 411
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 412
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 367
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f120203

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 368
    if-eqz v0, :cond_1

    .line 369
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 370
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 371
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 372
    iget-boolean v1, p0, Lcom/android/calendar/e/ai;->g:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    iget-boolean v1, v1, Lcom/android/calendar/hc;->f:Z

    if-eqz v1, :cond_0

    .line 373
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 375
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 380
    :cond_1
    :goto_0
    return-void

    .line 377
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 427
    if-eqz p1, :cond_0

    .line 429
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    const v0, 0x7f12020d

    .line 434
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 435
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 436
    const v1, 0x7f02010f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 438
    :cond_0
    return-void

    .line 432
    :cond_1
    const v0, 0x7f120208

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/e/ai;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->p:Landroid/os/Handler;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/android/calendar/e/ai;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    .line 141
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 142
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 143
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 144
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 145
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 147
    const v2, 0x7f100045

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 148
    const v2, 0x800033

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 149
    iget v2, p0, Lcom/android/calendar/e/ai;->l:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 150
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 151
    iget-object v0, p0, Lcom/android/calendar/e/ai;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ai;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 153
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f120201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_0

    .line 422
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 424
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f120205

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 384
    if-eqz v0, :cond_0

    .line 385
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 387
    :cond_0
    const v0, 0x7f120206

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/ai;->a(Ljava/lang/CharSequence;I)V

    .line 388
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 441
    if-eqz p1, :cond_0

    .line 443
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 444
    const v0, 0x7f12020c

    .line 448
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 449
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 450
    const v1, 0x7f020116

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 452
    :cond_0
    return-void

    .line 446
    :cond_1
    const v0, 0x7f120207

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/e/ai;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/e/ai;->k:I

    return v0
.end method

.method private c(I)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 466
    if-eq p1, v1, :cond_0

    if-eqz p1, :cond_0

    .line 483
    :goto_0
    return-void

    .line 469
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 470
    const v0, 0x7f12020c

    move v2, v0

    .line 475
    :goto_1
    if-ne p1, v1, :cond_2

    .line 476
    const v0, 0x7f02013b

    move v1, v0

    .line 480
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 481
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 482
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 472
    :cond_1
    const v0, 0x7f120207

    move v2, v0

    goto :goto_1

    .line 478
    :cond_2
    const v0, 0x7f02013c

    move v1, v0

    goto :goto_2
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f12020a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_0

    .line 393
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 395
    :cond_0
    const v0, 0x7f12020b

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/ai;->a(Ljava/lang/CharSequence;I)V

    .line 396
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f12020b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 456
    if-eqz v0, :cond_0

    .line 457
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 458
    const/4 v0, 0x1

    .line 461
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/e/ai;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 495
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f120202

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 496
    if-nez v0, :cond_0

    .line 507
    :goto_0
    return-void

    .line 499
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v2, 0x7f120203

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 500
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 501
    iget-object v2, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    iget-boolean v2, v2, Lcom/android/calendar/hc;->f:Z

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 502
    iget-object v2, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    iget-boolean v2, v2, Lcom/android/calendar/hc;->f:Z

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 503
    iget-object v2, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    iget-boolean v2, v2, Lcom/android/calendar/hc;->f:Z

    if-eqz v2, :cond_1

    .line 504
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setPaintFlags(I)V

    .line 506
    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/e/ai;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->f:Lcom/android/calendar/al;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f120204

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 511
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    invoke-virtual {v1}, Lcom/android/calendar/dh;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    iget-object v1, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    iget-object v1, v1, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 513
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 515
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/e/ai;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/e/ai;->m:I

    return v0
.end method

.method private f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    const-string v0, "%H:%M"

    .line 540
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%-l:%M %^p"

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/calendar/e/ai;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/e/ai;->l:I

    return v0
.end method

.method static synthetic h(Lcom/android/calendar/e/ai;)Landroid/view/View;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/e/ai;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/e/ai;->n:I

    return v0
.end method

.method static synthetic j(Lcom/android/calendar/e/ai;)Landroid/view/View;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/e/ai;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/android/calendar/e/ai;->j:Z

    return v0
.end method

.method static synthetic l(Lcom/android/calendar/e/ai;)Landroid/view/View;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/e/ai;)Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/android/calendar/e/ai;->g:Z

    return v0
.end method

.method static synthetic n(Lcom/android/calendar/e/ai;)Lcom/android/calendar/dh;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    return-object v0
.end method

.method static synthetic o(Lcom/android/calendar/e/ai;)Lcom/android/calendar/hc;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    return-object v0
.end method


# virtual methods
.method public a(IIIII)V
    .locals 0

    .prologue
    .line 240
    iput p1, p0, Lcom/android/calendar/e/ai;->k:I

    .line 241
    iput p2, p0, Lcom/android/calendar/e/ai;->l:I

    .line 242
    iput p3, p0, Lcom/android/calendar/e/ai;->m:I

    .line 243
    iput p4, p0, Lcom/android/calendar/e/ai;->n:I

    .line 244
    iput p5, p0, Lcom/android/calendar/e/ai;->o:I

    .line 245
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 130
    invoke-virtual {p0}, Lcom/android/calendar/e/ai;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    sget-object v0, Lcom/android/calendar/hj;->r:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/calendar/e/ai;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 134
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/ai;->b()V

    .line 135
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const v1, 0x103013b

    .line 121
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 122
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/e/ai;->setStyle(II)V

    .line 123
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/e/ai;->setStyle(II)V

    .line 124
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 249
    const v0, 0x7f040066

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    .line 250
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 275
    :goto_0
    return-object v0

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f120211

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ai;->c:Landroid/view/View;

    .line 254
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    const v1, 0x7f120210

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ai;->d:Landroid/view/View;

    .line 256
    iget-object v0, p0, Lcom/android/calendar/e/ai;->h:Lcom/android/calendar/dh;

    iget-object v1, p0, Lcom/android/calendar/e/ai;->i:Lcom/android/calendar/hc;

    iget-boolean v2, p0, Lcom/android/calendar/e/ai;->g:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/e/ai;->a(Lcom/android/calendar/dh;Lcom/android/calendar/hc;Z)V

    .line 258
    iget-object v0, p0, Lcom/android/calendar/e/ai;->e:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/e/ak;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/ak;-><init>(Lcom/android/calendar/e/ai;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ai;->b:Landroid/view/View;

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/android/calendar/e/ai;->j:Z

    if-nez v0, :cond_0

    .line 297
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 300
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 301
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 280
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 281
    iget-boolean v0, p0, Lcom/android/calendar/e/ai;->j:Z

    if-nez v0, :cond_0

    .line 282
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 284
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/android/calendar/e/ai;->p:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/android/calendar/e/ai;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ai;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 291
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 292
    return-void
.end method
