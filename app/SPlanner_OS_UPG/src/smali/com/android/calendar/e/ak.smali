.class Lcom/android/calendar/e/ak;
.super Ljava/lang/Object;
.source "DayViewHoverFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/ai;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/ai;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 18

    .prologue
    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->d(Lcom/android/calendar/e/ai;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 272
    :goto_0
    return-void

    .line 265
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->m(Lcom/android/calendar/e/ai;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->e(Lcom/android/calendar/e/ai;)Lcom/android/calendar/al;

    move-result-object v2

    const-wide/16 v4, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-static {v3}, Lcom/android/calendar/e/ai;->n(Lcom/android/calendar/e/ai;)Lcom/android/calendar/dh;

    move-result-object v3

    iget-wide v6, v3, Lcom/android/calendar/dh;->b:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-static {v3}, Lcom/android/calendar/e/ai;->n(Lcom/android/calendar/e/ai;)Lcom/android/calendar/dh;

    move-result-object v3

    iget-wide v8, v3, Lcom/android/calendar/dh;->m:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-static {v3}, Lcom/android/calendar/e/ai;->n(Lcom/android/calendar/e/ai;)Lcom/android/calendar/dh;

    move-result-object v3

    iget-wide v10, v3, Lcom/android/calendar/dh;->n:J

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    .line 271
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-virtual {v2}, Lcom/android/calendar/e/ai;->dismiss()V

    goto :goto_0

    .line 269
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-static {v2}, Lcom/android/calendar/e/ai;->e(Lcom/android/calendar/e/ai;)Lcom/android/calendar/al;

    move-result-object v2

    const-wide/16 v4, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ak;->a:Lcom/android/calendar/e/ai;

    invoke-static {v3}, Lcom/android/calendar/e/ai;->o(Lcom/android/calendar/e/ai;)Lcom/android/calendar/hc;

    move-result-object v3

    iget-wide v8, v3, Lcom/android/calendar/hc;->a:J

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v11}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    goto :goto_1
.end method
