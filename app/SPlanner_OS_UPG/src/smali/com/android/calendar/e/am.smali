.class public Lcom/android/calendar/e/am;
.super Ljava/lang/Object;
.source "TimeCuePopupWindow.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/PopupWindow;

.field private f:Z

.field private g:I

.field private h:Landroid/view/ViewGroup$LayoutParams;

.field private i:Landroid/graphics/Rect;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/android/calendar/e/am;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/e/am;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    .line 67
    iput-object p1, p0, Lcom/android/calendar/e/am;->c:Landroid/view/View;

    .line 68
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/am;->b:Landroid/content/Context;

    .line 69
    iput p2, p0, Lcom/android/calendar/e/am;->n:I

    .line 70
    const/4 v0, 0x7

    if-eq p3, v0, :cond_0

    .line 71
    iget v0, p0, Lcom/android/calendar/e/am;->n:I

    div-int/lit8 v0, v0, 0x7

    iput v0, p0, Lcom/android/calendar/e/am;->n:I

    .line 73
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/am;->f()V

    .line 74
    return-void
.end method

.method private a(Landroid/view/View;III)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    const/4 v1, 0x0

    .line 170
    const/4 v0, 0x0

    .line 172
    iget-object v2, p0, Lcom/android/calendar/e/am;->b:Landroid/content/Context;

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 173
    iget-object v0, p0, Lcom/android/calendar/e/am;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 176
    :cond_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    if-nez v2, :cond_2

    .line 209
    :cond_1
    :goto_0
    return-void

    .line 180
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/e/am;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 182
    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v0}, Lcom/android/calendar/hj;->d(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 183
    :cond_3
    invoke-static {v0}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    .line 198
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/e/am;->h:Landroid/view/ViewGroup$LayoutParams;

    if-nez v0, :cond_6

    .line 199
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 201
    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 205
    :goto_2
    iget-object v2, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 206
    iget-object v0, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/am;->l:I

    .line 207
    iget-object v0, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/am;->m:I

    .line 208
    invoke-direct {p0, p3, p4}, Lcom/android/calendar/e/am;->c(II)V

    goto :goto_0

    .line 185
    :cond_4
    if-eqz p1, :cond_5

    .line 188
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 189
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 190
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v4, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v0, Landroid/graphics/Rect;->right:I

    .line 191
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 192
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v4, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 185
    :cond_5
    iget-object p1, p0, Lcom/android/calendar/e/am;->c:Landroid/view/View;

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private b(Landroid/view/View;III)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 243
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    if-nez v0, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/e/am;->a(Landroid/view/View;III)V

    .line 249
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 255
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    iget v1, p0, Lcom/android/calendar/e/am;->j:I

    iget v2, p0, Lcom/android/calendar/e/am;->k:I

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/PopupWindow;->update(IIII)V

    goto :goto_0

    .line 258
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/calendar/e/am;->j:I

    iget v3, p0, Lcom/android/calendar/e/am;->k:I

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method

.method private c(II)V
    .locals 6

    .prologue
    .line 212
    iput p1, p0, Lcom/android/calendar/e/am;->j:I

    .line 213
    iput p2, p0, Lcom/android/calendar/e/am;->k:I

    .line 214
    iget v0, p0, Lcom/android/calendar/e/am;->j:I

    int-to-double v0, v0

    iget v2, p0, Lcom/android/calendar/e/am;->n:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/android/calendar/e/am;->l:I

    int-to-double v4, v4

    add-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/am;->j:I

    .line 216
    iget v0, p0, Lcom/android/calendar/e/am;->j:I

    iget-object v1, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0xa

    if-ge v0, v1, :cond_0

    .line 217
    iget v0, p0, Lcom/android/calendar/e/am;->j:I

    int-to-double v0, v0

    iget v2, p0, Lcom/android/calendar/e/am;->n:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/android/calendar/e/am;->l:I

    int-to-double v4, v4

    add-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/am;->j:I

    .line 219
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/am;->j:I

    iget-object v1, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/android/calendar/e/am;->l:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0xa

    if-le v0, v1, :cond_1

    .line 220
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/android/calendar/e/am;->l:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/android/calendar/e/am;->j:I

    .line 223
    :cond_1
    iget v0, p0, Lcom/android/calendar/e/am;->m:I

    sub-int v0, p2, v0

    iput v0, p0, Lcom/android/calendar/e/am;->k:I

    .line 224
    iget v0, p0, Lcom/android/calendar/e/am;->k:I

    iget-object v1, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/android/calendar/e/am;->m:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0xa

    if-le v0, v1, :cond_2

    .line 225
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/android/calendar/e/am;->m:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/android/calendar/e/am;->k:I

    .line 227
    :cond_2
    iget v0, p0, Lcom/android/calendar/e/am;->k:I

    iget-object v1, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/android/calendar/e/am;->m:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0xa

    if-ge v0, v1, :cond_3

    .line 228
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/android/calendar/e/am;->m:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/android/calendar/e/am;->k:I

    .line 230
    :cond_3
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 80
    iget-object v0, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/android/calendar/e/am;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 82
    const v1, 0x7f0400b3

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    const v1, 0x7f1202ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/e/am;->o:Landroid/widget/TextView;

    .line 86
    :cond_0
    iput-object v3, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/am;->f:Z

    .line 89
    iput v2, p0, Lcom/android/calendar/e/am;->g:I

    .line 90
    iput v2, p0, Lcom/android/calendar/e/am;->j:I

    .line 91
    iput v2, p0, Lcom/android/calendar/e/am;->k:I

    .line 92
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 139
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/am;->h()Landroid/widget/PopupWindow;

    .line 140
    invoke-virtual {p0}, Lcom/android/calendar/e/am;->c()V

    .line 141
    return-void
.end method

.method private h()Landroid/widget/PopupWindow;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 150
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/android/calendar/e/am;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    .line 152
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 153
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 154
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 155
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 156
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 157
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/android/calendar/e/am;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    const-string v0, "%H:%M"

    .line 321
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%-l:%M %^p"

    goto :goto_0
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 113
    iput p1, p0, Lcom/android/calendar/e/am;->j:I

    .line 114
    iput p2, p0, Lcom/android/calendar/e/am;->k:I

    .line 115
    return-void
.end method

.method public a(JJ)V
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 299
    const-string v0, ""

    .line 300
    iget-object v1, p0, Lcom/android/calendar/e/am;->b:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    .line 301
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 302
    cmp-long v1, p1, v4

    if-eqz v1, :cond_0

    cmp-long v1, p3, v4

    if-eqz v1, :cond_0

    .line 303
    invoke-virtual {v2, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 304
    invoke-direct {p0}, Lcom/android/calendar/e/am;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 305
    invoke-virtual {v2, p3, p4}, Landroid/text/format/Time;->set(J)V

    .line 306
    invoke-direct {p0}, Lcom/android/calendar/e/am;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 307
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ~ "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/e/am;->o:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 311
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 312
    iget-object v1, p0, Lcom/android/calendar/e/am;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 103
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/android/calendar/e/am;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/e/am;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/e/am;->g()V

    goto :goto_0
.end method

.method public b(II)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 263
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 264
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/e/am;->c(II)V

    .line 265
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    iget v1, p0, Lcom/android/calendar/e/am;->j:I

    iget v2, p0, Lcom/android/calendar/e/am;->k:I

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 267
    :cond_0
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 236
    iget-object v0, p0, Lcom/android/calendar/e/am;->c:Landroid/view/View;

    iget v1, p0, Lcom/android/calendar/e/am;->g:I

    iget v2, p0, Lcom/android/calendar/e/am;->j:I

    iget v3, p0, Lcom/android/calendar/e/am;->k:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/calendar/e/am;->b(Landroid/view/View;III)V

    .line 237
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/e/am;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/am;->d:Landroid/view/View;

    .line 283
    :cond_1
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 277
    sget-object v0, Lcom/android/calendar/e/am;->a:Ljava/lang/String;

    const-string v1, "Fail to dismiss PopupWindow"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public e()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/android/calendar/e/am;->i:Landroid/graphics/Rect;

    return-object v0
.end method
