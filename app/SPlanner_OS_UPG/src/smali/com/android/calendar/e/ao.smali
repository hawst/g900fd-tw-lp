.class public Lcom/android/calendar/e/ao;
.super Landroid/view/View;
.source "WeekAllDayView.java"


# static fields
.field private static aL:I

.field private static ap:I

.field private static aq:I

.field private static at:I

.field public static b:Z

.field public static c:Z

.field private static cH:I

.field private static cj:I

.field private static ck:F

.field private static final d:Ljava/lang/String;

.field private static e:F


# instance fields
.field private A:Landroid/util/LruCache;

.field private B:Landroid/util/SparseIntArray;

.field private C:I

.field private D:F

.field private E:F

.field private F:F

.field private G:F

.field private H:F

.field private I:F

.field private J:F

.field private K:F

.field private L:F

.field private M:F

.field private N:I

.field private O:I

.field private P:I

.field private Q:I

.field private R:I

.field private S:I

.field private T:I

.field private U:I

.field private V:I

.field private W:I

.field public a:F

.field private aA:I

.field private aB:[I

.field private aC:[I

.field private aD:I

.field private aE:I

.field private aF:I

.field private aG:I

.field private aH:I

.field private aI:I

.field private aJ:I

.field private aK:I

.field private aM:Landroid/graphics/drawable/Drawable;

.field private aN:Landroid/graphics/drawable/Drawable;

.field private aO:Landroid/graphics/drawable/Drawable;

.field private aP:Landroid/graphics/drawable/Drawable;

.field private aQ:Landroid/graphics/drawable/Drawable;

.field private aR:Landroid/graphics/drawable/Drawable;

.field private aS:Landroid/graphics/drawable/Drawable;

.field private aT:Landroid/graphics/drawable/Drawable;

.field private aU:Landroid/graphics/drawable/Drawable;

.field private aV:I

.field private aW:I

.field private aX:Landroid/text/format/Time;

.field private aY:Landroid/text/format/Time;

.field private aZ:Landroid/text/format/Time;

.field private aa:I

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:I

.field private ag:I

.field private ah:I

.field private ai:F

.field private aj:F

.field private ak:I

.field private al:I

.field private am:I

.field private an:I

.field private ao:I

.field private ar:I

.field private as:I

.field private au:I

.field private av:I

.field private aw:I

.field private ax:I

.field private ay:I

.field private az:I

.field private bA:Z

.field private bB:Z

.field private bC:Z

.field private bD:Landroid/view/GestureDetector;

.field private bE:Lcom/android/calendar/e/bd;

.field private bF:I

.field private bG:I

.field private bH:I

.field private bI:Ljava/lang/Object;

.field private bJ:Ljava/lang/Object;

.field private bK:I

.field private bL:I

.field private bM:Ljava/lang/Object;

.field private bN:Ljava/lang/Object;

.field private bO:Landroid/app/DialogFragment;

.field private bP:Lcom/android/calendar/AllInOneActivity;

.field private bQ:I

.field private bR:I

.field private bS:Ljava/lang/Object;

.field private bT:Ljava/lang/Object;

.field private bU:Landroid/view/accessibility/AccessibilityManager;

.field private bV:Landroid/content/Context;

.field private bW:Lcom/android/calendar/al;

.field private bX:Landroid/os/Handler;

.field private bY:Lcom/android/calendar/d/g;

.field private bZ:Lcom/android/calendar/ej;

.field private ba:Landroid/text/format/Time;

.field private bb:I

.field private bc:[Landroid/graphics/Bitmap;

.field private bd:Lcom/android/calendar/h/c;

.field private be:Ljava/lang/String;

.field private bf:Ljava/util/ArrayList;

.field private bg:Ljava/util/ArrayList;

.field private bh:Ljava/util/ArrayList;

.field private bi:Ljava/util/Set;

.field private bj:Ljava/util/Set;

.field private bk:[[Z

.field private bl:[[Ljava/lang/Object;

.field private bm:[I

.field private bn:[Z

.field private bo:[Ljava/lang/String;

.field private bp:Z

.field private bq:Z

.field private br:Z

.field private bs:Z

.field private bt:Z

.field private bu:Z

.field private bv:Z

.field private bw:Z

.field private bx:Z

.field private by:Z

.field private bz:Z

.field private cA:I

.field private cB:Lcom/android/calendar/dh;

.field private cC:Z

.field private cD:I

.field private cE:I

.field private cF:I

.field private cG:I

.field private cI:Lcom/android/calendar/hc;

.field private cJ:Lcom/android/calendar/e/ba;

.field private final cK:Z

.field private final cL:Ljava/lang/Runnable;

.field private final cM:Ljava/util/regex/Pattern;

.field private cN:Ljava/lang/Runnable;

.field private cO:Ljava/lang/Runnable;

.field private cP:Ljava/lang/Runnable;

.field private cQ:Ljava/util/Comparator;

.field private ca:Ljava/lang/String;

.field private cb:Ljava/lang/String;

.field private cc:Ljava/lang/String;

.field private cd:I

.field private ce:I

.field private cf:Z

.field private cg:F

.field private ch:F

.field private ci:F

.field private cl:I

.field private cm:I

.field private cn:I

.field private co:I

.field private cp:I

.field private cq:I

.field private cr:I

.field private cs:F

.field private ct:I

.field private cu:Lcom/android/calendar/e/g;

.field private cv:Lcom/android/calendar/e/ab;

.field private cw:I

.field private cx:I

.field private cy:I

.field private cz:J

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/Paint;

.field private k:Landroid/graphics/Paint;

.field private l:Landroid/text/TextPaint;

.field private m:Landroid/graphics/Paint;

.field private n:Landroid/graphics/Paint;

.field private o:Landroid/graphics/Paint;

.field private p:Landroid/graphics/Paint;

.field private q:Landroid/graphics/Paint;

.field private r:Landroid/graphics/Paint;

.field private final s:Landroid/graphics/Paint;

.field private t:Landroid/graphics/Rect;

.field private u:Landroid/graphics/Rect;

.field private v:Landroid/graphics/Rect;

.field private w:Landroid/graphics/Rect;

.field private x:Landroid/graphics/Rect;

.field private y:Landroid/graphics/RectF;

.field private z:[F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 93
    const-class v0, Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/e/ao;->d:Ljava/lang/String;

    .line 103
    sput v1, Lcom/android/calendar/e/ao;->e:F

    .line 170
    const/16 v0, 0x76e

    sput v0, Lcom/android/calendar/e/ao;->ap:I

    .line 171
    const/16 v0, 0x7f4

    sput v0, Lcom/android/calendar/e/ao;->aq:I

    .line 298
    const/16 v0, 0x64

    sput v0, Lcom/android/calendar/e/ao;->cj:I

    .line 299
    sput v1, Lcom/android/calendar/e/ao;->ck:F

    .line 329
    sput-boolean v2, Lcom/android/calendar/e/ao;->b:Z

    .line 3446
    sput-boolean v2, Lcom/android/calendar/e/ao;->c:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/e/g;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x7

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 345
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 106
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    .line 107
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    .line 108
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    .line 109
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->i:Landroid/graphics/Paint;

    .line 110
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    .line 111
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    .line 112
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    .line 113
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    .line 114
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->n:Landroid/graphics/Paint;

    .line 115
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->o:Landroid/graphics/Paint;

    .line 116
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->p:Landroid/graphics/Paint;

    .line 117
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->q:Landroid/graphics/Paint;

    .line 118
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->r:Landroid/graphics/Paint;

    .line 119
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->s:Landroid/graphics/Paint;

    .line 120
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->t:Landroid/graphics/Rect;

    .line 121
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->u:Landroid/graphics/Rect;

    .line 122
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    .line 123
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->w:Landroid/graphics/Rect;

    .line 124
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->x:Landroid/graphics/Rect;

    .line 125
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->y:Landroid/graphics/RectF;

    .line 166
    const/4 v2, 0x3

    iput v2, p0, Lcom/android/calendar/e/ao;->am:I

    .line 167
    iput v0, p0, Lcom/android/calendar/e/ao;->an:I

    .line 229
    new-array v2, v4, [I

    iput-object v2, p0, Lcom/android/calendar/e/ao;->bm:[I

    .line 230
    new-array v2, v4, [Z

    iput-object v2, p0, Lcom/android/calendar/e/ao;->bn:[Z

    .line 241
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bt:Z

    .line 242
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bu:Z

    .line 243
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bv:Z

    .line 244
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bw:Z

    .line 245
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bx:Z

    .line 246
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->by:Z

    .line 247
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bz:Z

    .line 248
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bA:Z

    .line 249
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bB:Z

    .line 250
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bC:Z

    .line 259
    sget-object v2, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v2, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 279
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/calendar/e/ao;->bU:Landroid/view/accessibility/AccessibilityManager;

    .line 284
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    .line 290
    iput v4, p0, Lcom/android/calendar/e/ao;->cd:I

    .line 294
    iput-boolean v1, p0, Lcom/android/calendar/e/ao;->cf:Z

    .line 295
    iput v3, p0, Lcom/android/calendar/e/ao;->cg:F

    .line 296
    iput v3, p0, Lcom/android/calendar/e/ao;->ch:F

    .line 297
    iput v3, p0, Lcom/android/calendar/e/ao;->ci:F

    .line 314
    sget-object v2, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    iput-object v2, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    .line 321
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->cC:Z

    .line 334
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->cK:Z

    .line 336
    new-instance v0, Lcom/android/calendar/e/ap;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/ap;-><init>(Lcom/android/calendar/e/ao;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cL:Ljava/lang/Runnable;

    .line 1343
    const-string v0, "[\t\n],"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cM:Ljava/util/regex/Pattern;

    .line 2761
    new-instance v0, Lcom/android/calendar/e/ar;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/ar;-><init>(Lcom/android/calendar/e/ao;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cN:Ljava/lang/Runnable;

    .line 2830
    new-instance v0, Lcom/android/calendar/e/au;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/au;-><init>(Lcom/android/calendar/e/ao;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cO:Ljava/lang/Runnable;

    .line 2868
    new-instance v0, Lcom/android/calendar/e/av;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/av;-><init>(Lcom/android/calendar/e/ao;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cP:Ljava/lang/Runnable;

    .line 3255
    new-instance v0, Lcom/android/calendar/e/ax;

    invoke-direct {v0, p0}, Lcom/android/calendar/e/ax;-><init>(Lcom/android/calendar/e/ao;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cQ:Ljava/util/Comparator;

    .line 346
    iput p3, p0, Lcom/android/calendar/e/ao;->cd:I

    .line 347
    iput-object p2, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    .line 348
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->a(Landroid/content/Context;)V

    .line 349
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    .line 3237
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 3253
    :cond_0
    return-void

    .line 3241
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3242
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_2

    .line 3251
    iget-object v2, p0, Lcom/android/calendar/e/ao;->cQ:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private B()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3267
    move v1, v0

    .line 3268
    :goto_0
    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v0, v2, :cond_1

    .line 3269
    invoke-virtual {p0, v0}, Lcom/android/calendar/e/ao;->a(I)I

    move-result v2

    .line 3270
    if-le v2, v1, :cond_0

    move v1, v2

    .line 3268
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3272
    :cond_1
    iput v1, p0, Lcom/android/calendar/e/ao;->an:I

    .line 3273
    return-void
.end method

.method private C()Z
    .locals 2

    .prologue
    .line 3468
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->cC:Z

    if-eqz v0, :cond_0

    .line 3469
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_0

    .line 3470
    const/4 v0, 0x1

    .line 3473
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/e/ao;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    return-object v0
.end method

.method private a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;IIIIZ)Lcom/android/calendar/e/ay;
    .locals 8

    .prologue
    .line 1228
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bi:Ljava/util/Set;

    iget-wide v2, p2, Lcom/android/calendar/dh;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1229
    sget-object v0, Lcom/android/calendar/e/ay;->b:Lcom/android/calendar/e/ay;

    .line 1340
    :goto_0
    return-object v0

    .line 1231
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bi:Ljava/util/Set;

    iget-wide v2, p2, Lcom/android/calendar/dh;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1233
    iget v0, p2, Lcom/android/calendar/dh;->j:I

    iget v1, p2, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 1234
    iget v1, p0, Lcom/android/calendar/e/ao;->aV:I

    iget v2, p2, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v1, v2

    .line 1235
    iget v2, p0, Lcom/android/calendar/e/ao;->aV:I

    iget v3, p2, Lcom/android/calendar/dh;->i:I

    if-le v2, v3, :cond_1

    .line 1236
    sub-int/2addr v0, v1

    .line 1241
    :cond_1
    iget v1, p0, Lcom/android/calendar/e/ao;->aV:I

    add-int/2addr v1, p6

    .line 1242
    iget v2, p0, Lcom/android/calendar/e/ao;->aV:I

    iget v3, p2, Lcom/android/calendar/dh;->i:I

    if-ge v2, v3, :cond_6

    iget v2, p2, Lcom/android/calendar/dh;->i:I

    if-ge v2, v1, :cond_6

    .line 1243
    iget v2, p2, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 1248
    :cond_2
    :goto_1
    add-int v1, p6, v0

    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v1, v2, :cond_3

    .line 1249
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    sub-int/2addr v0, p6

    .line 1252
    :cond_3
    iget v1, p0, Lcom/android/calendar/e/ao;->W:I

    add-int v2, p4, v1

    .line 1253
    const/4 v1, 0x0

    .line 1254
    :goto_2
    iget-object v3, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v3, v3, p6

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_7

    iget-object v3, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v3, v3, p6

    aget-boolean v3, v3, v1

    if-eqz v3, :cond_7

    .line 1255
    iget-boolean v3, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v3, :cond_4

    rem-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_5

    .line 1256
    :cond_4
    iget v3, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v4, p0, Lcom/android/calendar/e/ao;->W:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 1258
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1244
    :cond_6
    if-ltz p6, :cond_2

    iget v1, p0, Lcom/android/calendar/e/ao;->aV:I

    iget v2, p2, Lcom/android/calendar/dh;->i:I

    if-le v1, v2, :cond_2

    .line 1245
    sub-int/2addr v0, p6

    goto :goto_1

    .line 1264
    :cond_7
    const/4 v3, 0x1

    if-le v0, v3, :cond_a

    iget v3, p0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v3, v3, -0x1

    if-ge p6, v3, :cond_a

    .line 1265
    add-int v3, p6, v0

    .line 1266
    iget v4, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v3, v4, :cond_8

    .line 1267
    iget v3, p0, Lcom/android/calendar/e/ao;->cd:I

    .line 1269
    :cond_8
    add-int/lit8 v4, p6, 0x1

    :goto_3
    if-ge v4, v3, :cond_9

    .line 1270
    iget-object v5, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v5, v5, v4

    aget-boolean v5, v5, v1

    if-eqz v5, :cond_c

    .line 1271
    add-int/lit8 v1, v1, 0x1

    .line 1272
    iget v3, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v4, p0, Lcom/android/calendar/e/ao;->W:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 1277
    :cond_9
    add-int/lit8 v3, p6, 0x1

    :goto_4
    iget v4, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v3, v4, :cond_a

    .line 1278
    iget v4, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_d

    invoke-direct {p0, v3}, Lcom/android/calendar/e/ao;->f(I)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1279
    sub-int v0, v3, p6

    .line 1285
    :cond_a
    iget-boolean v3, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v3, v3, p6

    if-eqz v3, :cond_e

    iget v3, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v4, 0x1

    if-le v3, v4, :cond_e

    const/4 v3, 0x1

    .line 1286
    :goto_5
    if-eqz v3, :cond_f

    const/4 v3, 0x1

    .line 1287
    :goto_6
    add-int/lit8 v4, v1, 0x1

    iget v5, p0, Lcom/android/calendar/e/ao;->am:I

    if-ne v4, v5, :cond_10

    const/4 v4, 0x0

    invoke-direct {p0, p6, v4}, Lcom/android/calendar/e/ao;->b(IZ)I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/e/ao;->am:I

    if-le v3, v4, :cond_10

    invoke-direct {p0}, Lcom/android/calendar/e/ao;->C()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget-wide v4, v3, Lcom/android/calendar/dh;->q:J

    iget-wide v6, p2, Lcom/android/calendar/dh;->q:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_10

    .line 1288
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bi:Ljava/util/Set;

    iget-wide v2, p2, Lcom/android/calendar/dh;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1289
    sget-object v0, Lcom/android/calendar/e/ay;->c:Lcom/android/calendar/e/ay;

    goto/16 :goto_0

    .line 1269
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1277
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1285
    :cond_e
    const/4 v3, 0x0

    goto :goto_5

    .line 1286
    :cond_f
    const/4 v3, 0x0

    goto :goto_6

    .line 1292
    :cond_10
    iget-object v3, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v3, v3, p6

    array-length v3, v3

    if-ge v1, v3, :cond_12

    .line 1293
    const/4 v3, 0x0

    :goto_7
    if-ge v3, v0, :cond_12

    .line 1294
    add-int v4, p6, v3

    iget v5, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v4, v5, :cond_11

    if-eqz p7, :cond_11

    .line 1295
    add-int v4, p6, v3

    invoke-direct {p0, v4, v1, p2}, Lcom/android/calendar/e/ao;->a(IILjava/lang/Object;)V

    .line 1293
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1300
    :cond_12
    iget v3, p2, Lcom/android/calendar/dh;->c:I

    .line 1301
    iget-object v4, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1302
    iget-object v3, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    const/16 v4, 0xb2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1304
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/lit8 v4, p3, 0x1

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 1305
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iput v2, v3, Landroid/graphics/Rect;->top:I

    .line 1306
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v4, p0, Lcom/android/calendar/e/ao;->Q:I

    add-int/2addr v2, v4

    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 1307
    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/2addr v0, p6

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v0

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v0, v3

    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 1308
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v0, :cond_13

    .line 1309
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/e/ao;->a(IZ)V

    .line 1311
    :cond_13
    iget v0, p2, Lcom/android/calendar/dh;->c:I

    invoke-static {v0}, Lcom/android/calendar/hj;->d(I)I

    move-result v0

    .line 1312
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v2, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    if-eq v1, v2, :cond_14

    .line 1313
    iget v0, p0, Lcom/android/calendar/e/ao;->aE:I

    .line 1315
    :cond_14
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    iget-boolean v1, p0, Lcom/android/calendar/e/ao;->bu:Z

    if-eqz v1, :cond_15

    .line 1316
    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->g(I)I

    move-result v0

    .line 1318
    :cond_15
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->C()Z

    move-result v1

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget-wide v2, v1, Lcom/android/calendar/dh;->q:J

    iget-wide v4, p2, Lcom/android/calendar/dh;->q:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_16

    .line 1320
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->E:F

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1321
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->F:F

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1322
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->C:F

    float-to-int v2, v2

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1323
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->D:F

    float-to-int v2, v2

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1325
    :cond_16
    iget-object v1, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1326
    iget-object v1, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1327
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1328
    iget v0, p2, Lcom/android/calendar/dh;->B:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_17

    .line 1329
    iget-object v0, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1331
    :cond_17
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->C()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget-wide v0, v0, Lcom/android/calendar/dh;->q:J

    iget-wide v2, p2, Lcom/android/calendar/dh;->q:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_18

    .line 1333
    iget-object v0, p0, Lcom/android/calendar/e/ao;->s:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->ao:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1334
    iget-object v0, p0, Lcom/android/calendar/e/ao;->y:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1335
    iget-object v0, p0, Lcom/android/calendar/e/ao;->y:Landroid/graphics/RectF;

    sget v1, Lcom/android/calendar/e/ao;->e:F

    sget v2, Lcom/android/calendar/e/ao;->e:F

    iget-object v3, p0, Lcom/android/calendar/e/ao;->s:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1338
    :cond_18
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/calendar/e/ao;->a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;Lcom/android/calendar/hc;)V

    .line 1339
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/e/ao;->a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;)V

    .line 1340
    sget-object v0, Lcom/android/calendar/e/ay;->a:Lcom/android/calendar/e/ay;

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/e/ao;Lcom/android/calendar/hc;)Lcom/android/calendar/hc;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/android/calendar/e/ao;->cI:Lcom/android/calendar/hc;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/e/ao;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/android/calendar/e/ao;->bN:Ljava/lang/Object;

    return-object p1
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1346
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cM:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1347
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1348
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1351
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 1352
    if-gtz p2, :cond_2

    .line 1353
    const-string p1, ""

    .line 1358
    :cond_1
    :goto_0
    const/16 v0, 0xa

    const/16 v1, 0x20

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1354
    :cond_2
    if-le v0, p2, :cond_1

    .line 1355
    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 682
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    sget v1, Lcom/android/calendar/e/ao;->at:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/ao;->ar:I

    iget v2, p0, Lcom/android/calendar/e/ao;->C:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/ao;->cr:I

    .line 683
    iget v0, p0, Lcom/android/calendar/e/ao;->cr:I

    if-gez v0, :cond_0

    .line 684
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/e/ao;->cr:I

    .line 686
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 687
    instance-of v1, v0, Lcom/android/calendar/e/be;

    if-eqz v1, :cond_1

    .line 688
    check-cast v0, Lcom/android/calendar/e/be;

    .line 689
    invoke-virtual {v0}, Lcom/android/calendar/e/be;->setListHeight()I

    .line 691
    :cond_1
    return-void
.end method

.method private a(IILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v0, v0, p1

    const/4 v1, 0x1

    aput-boolean v1, v0, p2

    .line 1171
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    aget-object v0, v0, p1

    aput-object p3, v0, p2

    .line 1172
    return-void
.end method

.method private a(IZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1662
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1686
    :cond_0
    :goto_0
    return-void

    .line 1669
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x1

    if-ne v0, v1, :cond_2

    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 1671
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cl:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 1672
    :cond_2
    rem-int/lit8 v0, p1, 0x2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1674
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cl:I

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    iget v4, p0, Lcom/android/calendar/e/ao;->aa:I

    sub-int/2addr v3, v4

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1676
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cl:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1677
    if-nez p2, :cond_0

    .line 1678
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    iget v3, p0, Lcom/android/calendar/e/ao;->aa:I

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 1681
    :cond_3
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 1684
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method

.method private a(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2370
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 2371
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/al;->a(J)V

    .line 2372
    new-instance v0, Lcom/android/calendar/month/a;

    const/4 v6, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/month/a;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;JZZ)V

    .line 2373
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 2374
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 2376
    sget-object v3, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 2377
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2379
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2386
    :cond_0
    :try_start_1
    sget-object v1, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2387
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2392
    :goto_0
    return-void

    .line 2380
    :catch_0
    move-exception v0

    goto :goto_0

    .line 2388
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 352
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/ao;->setFocusable(Z)V

    .line 353
    iput-object p1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    .line 354
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bP:Lcom/android/calendar/AllInOneActivity;

    .line 355
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    .line 356
    const/16 v0, 0x1c

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    .line 357
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cL:Ljava/lang/Runnable;

    invoke-static {p1, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    .line 358
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 359
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    .line 360
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->aZ:Landroid/text/format/Time;

    .line 361
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->ba:Landroid/text/format/Time;

    .line 362
    iget-object v0, p0, Lcom/android/calendar/e/ao;->ba:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 364
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bi:Ljava/util/Set;

    .line 365
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bj:Ljava/util/Set;

    .line 366
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bp:Z

    .line 367
    invoke-static {p1}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bq:Z

    .line 368
    invoke-static {p1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ao;->bb:I

    .line 369
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->o()V

    .line 371
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    new-array v0, v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    .line 372
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 373
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 374
    invoke-static {v0}, Lcom/android/calendar/gx;->a(Landroid/app/FragmentManager;)Landroid/util/LruCache;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->A:Landroid/util/LruCache;

    .line 375
    invoke-static {v0}, Lcom/android/calendar/gx;->b(Landroid/app/FragmentManager;)Landroid/util/SparseIntArray;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->B:Landroid/util/SparseIntArray;

    .line 378
    :cond_0
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/android/calendar/e/az;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/e/az;-><init>(Lcom/android/calendar/e/ao;Lcom/android/calendar/e/ap;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bD:Landroid/view/GestureDetector;

    .line 379
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bY:Lcom/android/calendar/d/g;

    .line 381
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bY:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    invoke-static {p1}, Lcom/android/calendar/ej;->a(Landroid/content/Context;)Lcom/android/calendar/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bZ:Lcom/android/calendar/ej;

    .line 385
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0474

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->ca:Ljava/lang/String;

    .line 386
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cb:Ljava/lang/String;

    .line 387
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bv:Z

    .line 388
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->n()V

    .line 389
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->k()V

    .line 390
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->l()V

    .line 391
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->m()V

    .line 392
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    iget v1, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v1, v1, 0x1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    .line 393
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    iget v1, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v1, v1, 0x1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    .line 394
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bB:Z

    .line 396
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->j()V

    .line 397
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 719
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 720
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->b(Landroid/graphics/Canvas;)V

    .line 721
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->c(Landroid/graphics/Canvas;)V

    .line 722
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->d(Landroid/graphics/Canvas;)V

    .line 726
    :goto_0
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->f(Landroid/graphics/Canvas;)V

    .line 727
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->o(Landroid/graphics/Canvas;)V

    .line 728
    return-void

    .line 724
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->e(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 954
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-nez v0, :cond_1

    .line 987
    :cond_0
    :goto_0
    return-void

    .line 957
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bg:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 961
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bg:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 964
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 965
    invoke-virtual {v0}, Lcom/android/calendar/dh;->i()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 966
    iget-object v1, p0, Lcom/android/calendar/e/ao;->B:Landroid/util/SparseIntArray;

    iget-wide v2, v0, Lcom/android/calendar/dh;->y:J

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    .line 967
    if-nez v1, :cond_3

    .line 968
    iget-object v1, v0, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/calendar/gx;->a(Ljava/lang/String;)I

    move-result v1

    .line 969
    iget-object v2, p0, Lcom/android/calendar/e/ao;->B:Landroid/util/SparseIntArray;

    iget-wide v4, v0, Lcom/android/calendar/dh;->y:J

    long-to-int v0, v4

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    :cond_3
    move v0, v1

    .line 971
    const/4 v2, 0x1

    .line 975
    :goto_1
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 976
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 977
    if-nez p2, :cond_4

    .line 978
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 980
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/lit8 v2, p2, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 981
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/e/ao;->D:F

    iget v3, p0, Lcom/android/calendar/e/ao;->E:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/ao;->G:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 982
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/e/ao;->D:F

    iget v3, p0, Lcom/android/calendar/e/ao;->E:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/ao;->H:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 983
    iget-object v1, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 984
    iget-object v0, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 985
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method private a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1507
    invoke-virtual {p2}, Lcom/android/calendar/dh;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1508
    iget-object v0, p0, Lcom/android/calendar/e/ao;->A:Landroid/util/LruCache;

    iget-wide v2, p2, Lcom/android/calendar/dh;->y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1509
    if-nez v0, :cond_0

    .line 1510
    iget-object v0, p2, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    .line 1511
    invoke-static {v0, v4, v4}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1512
    if-eqz v0, :cond_0

    .line 1513
    iget-object v1, p0, Lcom/android/calendar/e/ao;->A:Landroid/util/LruCache;

    iget-wide v2, p2, Lcom/android/calendar/dh;->y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516
    :cond_0
    if-eqz v0, :cond_1

    .line 1517
    iget-object v1, p0, Lcom/android/calendar/e/ao;->w:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/calendar/e/ao;->ai:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1518
    iget-object v1, p0, Lcom/android/calendar/e/ao;->w:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1519
    iget-object v1, p0, Lcom/android/calendar/e/ao;->w:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/calendar/e/ao;->Q:I

    int-to-float v3, v3

    iget v4, p0, Lcom/android/calendar/e/ao;->aj:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1520
    iget-object v1, p0, Lcom/android/calendar/e/ao;->w:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->w:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/calendar/e/ao;->aj:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1521
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/calendar/e/ao;->w:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1524
    :cond_1
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;Lcom/android/calendar/hc;)V
    .locals 19

    .prologue
    .line 1362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    .line 1363
    if-eqz p2, :cond_8

    const/4 v1, 0x1

    .line 1364
    :goto_0
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    .line 1365
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v12

    .line 1366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v2, v2

    .line 1367
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    int-to-float v2, v2

    add-float/2addr v2, v3

    float-to-int v13, v2

    .line 1368
    div-int v14, v12, v13

    .line 1369
    new-array v15, v14, [Ljava/lang/String;

    .line 1370
    const/4 v10, 0x0

    .line 1372
    if-eqz v1, :cond_9

    .line 1373
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1f4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/calendar/e/ao;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 1377
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    .line 1378
    const/4 v8, 0x0

    .line 1379
    const/4 v4, 0x0

    .line 1380
    const/4 v7, 0x0

    .line 1383
    :goto_2
    add-int/lit8 v3, v14, -0x1

    if-lt v10, v3, :cond_a

    const/4 v3, 0x1

    .line 1385
    :goto_3
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v11, 0x1

    if-ne v6, v11, :cond_1b

    .line 1386
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/e/ao;->V:I

    sub-int v6, v9, v6

    .line 1388
    :goto_4
    if-eqz v1, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/android/calendar/dh;->h()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1389
    int-to-float v6, v6

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/calendar/e/ao;->ai:F

    sub-float/2addr v6, v11

    float-to-int v6, v6

    .line 1391
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 1392
    if-le v4, v5, :cond_1

    move v4, v5

    .line 1395
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    invoke-virtual {v11, v2, v8, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v11

    .line 1396
    int-to-float v0, v6

    move/from16 v16, v0

    cmpl-float v11, v11, v16

    if-lez v11, :cond_1a

    .line 1397
    add-int/lit8 v11, v4, -0x1

    invoke-virtual {v2, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v15, v10

    .line 1398
    aget-object v11, v15, v10

    invoke-static {v11}, Lcom/android/calendar/e/an;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v15, v10

    .line 1399
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    aget-object v16, v15, v10

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v11

    int-to-float v0, v6

    move/from16 v16, v0

    cmpl-float v11, v11, v16

    if-lez v11, :cond_2

    .line 1400
    add-int/lit8 v4, v4, -0x1

    .line 1401
    add-int/lit8 v11, v4, -0x1

    invoke-virtual {v2, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v15, v10

    :cond_2
    move v8, v4

    .line 1403
    add-int/lit8 v4, v10, 0x1

    .line 1404
    if-nez v3, :cond_3

    if-ne v8, v5, :cond_b

    .line 1405
    :cond_3
    const/4 v3, 0x1

    move v5, v4

    .line 1420
    :goto_5
    if-eqz v1, :cond_d

    sget-boolean v4, Lcom/android/calendar/dz;->c:Z

    if-eqz v4, :cond_d

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    .line 1421
    :goto_6
    if-ge v5, v14, :cond_18

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_18

    .line 1422
    const/4 v6, 0x0

    .line 1423
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1f4

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/android/calendar/e/ao;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    .line 1424
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v4

    .line 1425
    const/4 v3, 0x0

    move v11, v3

    move v7, v3

    move v8, v5

    .line 1427
    :goto_7
    add-int/lit8 v3, v14, -0x1

    if-lt v8, v3, :cond_e

    const/4 v3, 0x1

    move v10, v3

    .line 1429
    :goto_8
    if-eqz v1, :cond_17

    invoke-virtual/range {p2 .. p2}, Lcom/android/calendar/dh;->h()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1430
    int-to-float v3, v9

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->ai:F

    sub-float/2addr v3, v5

    float-to-int v5, v3

    .line 1432
    :goto_9
    add-int/lit8 v3, v11, 0x1

    .line 1433
    if-le v3, v4, :cond_4

    move v3, v4

    .line 1436
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    move-object/from16 v0, v16

    invoke-virtual {v11, v0, v7, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v11

    .line 1437
    int-to-float v0, v5

    move/from16 v17, v0

    cmpl-float v11, v11, v17

    if-lez v11, :cond_10

    .line 1438
    add-int/lit8 v11, v3, -0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v15, v8

    .line 1439
    aget-object v11, v15, v8

    invoke-static {v11}, Lcom/android/calendar/e/an;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v15, v8

    .line 1440
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    aget-object v17, v15, v8

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v11

    int-to-float v0, v5

    move/from16 v17, v0

    cmpl-float v11, v11, v17

    if-lez v11, :cond_5

    .line 1441
    add-int/lit8 v3, v3, -0x1

    .line 1442
    add-int/lit8 v11, v3, -0x1

    invoke-virtual {v2, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v15, v8

    :cond_5
    move v7, v3

    .line 1444
    add-int/lit8 v3, v8, 0x1

    .line 1445
    if-nez v10, :cond_6

    if-ne v7, v4, :cond_f

    .line 1446
    :cond_6
    const/4 v2, 0x1

    move v4, v3

    move v3, v5

    .line 1460
    :goto_a
    if-eqz v1, :cond_7

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/dh;->B:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_7

    .line 1461
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    const/16 v6, 0x66

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 1463
    :cond_7
    if-eqz v2, :cond_13

    .line 1464
    add-int/lit8 v2, v4, -0x1

    aget-object v2, v15, v2

    invoke-static {v2}, Lcom/android/calendar/e/an;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1465
    :goto_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    invoke-virtual {v5, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    int-to-float v6, v3

    cmpl-float v5, v5, v6

    if-lez v5, :cond_12

    .line 1466
    add-int/lit8 v2, v4, -0x1

    add-int/lit8 v5, v4, -0x1

    aget-object v5, v15, v5

    const/4 v6, 0x0

    add-int/lit8 v7, v4, -0x1

    aget-object v7, v15, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v15, v2

    .line 1467
    add-int/lit8 v2, v4, -0x1

    aget-object v2, v15, v2

    invoke-static {v2}, Lcom/android/calendar/e/an;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_b

    .line 1363
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1375
    :cond_9
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/hc;->h:Ljava/lang/String;

    const/16 v3, 0x1f4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/calendar/e/ao;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1383
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 1408
    :cond_b
    add-int/lit8 v3, v8, -0x1

    move v8, v3

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    .line 1411
    :goto_c
    if-ne v4, v5, :cond_c

    .line 1412
    if-eqz v15, :cond_19

    array-length v5, v15

    if-lez v5, :cond_19

    .line 1413
    invoke-virtual {v2, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v15, v3

    .line 1414
    aget-object v4, v15, v3

    invoke-static {v4}, Lcom/android/calendar/e/an;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v15, v3

    .line 1415
    add-int/lit8 v5, v3, 0x1

    move v3, v7

    goto/16 :goto_5

    :cond_c
    move v10, v3

    .line 1419
    goto/16 :goto_2

    .line 1420
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 1427
    :cond_e
    const/4 v3, 0x0

    move v10, v3

    goto/16 :goto_8

    .line 1449
    :cond_f
    add-int/lit8 v7, v7, -0x1

    move v8, v3

    move v3, v7

    .line 1452
    :cond_10
    if-ne v3, v4, :cond_11

    .line 1453
    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v15, v8

    .line 1454
    aget-object v2, v15, v8

    invoke-static {v2}, Lcom/android/calendar/e/an;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v15, v8

    .line 1455
    add-int/lit8 v2, v8, 0x1

    move v3, v5

    move v4, v2

    move v2, v6

    .line 1456
    goto/16 :goto_a

    :cond_11
    move v11, v3

    .line 1458
    goto/16 :goto_7

    .line 1469
    :cond_12
    add-int/lit8 v3, v4, -0x1

    aput-object v2, v15, v3

    .line 1471
    :cond_13
    if-nez v1, :cond_14

    move-object/from16 v0, p3

    iget-boolean v1, v0, Lcom/android/calendar/hc;->f:Z

    if-eqz v1, :cond_14

    .line 1472
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStrikeThruText(Z)V

    .line 1473
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/ao;->aD:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 1475
    :cond_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    mul-int v2, v13, v4

    sub-int v2, v12, v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v3, v1

    .line 1476
    const/4 v1, 0x0

    .line 1477
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_15

    .line 1478
    const/4 v1, 0x1

    const/high16 v2, 0x41300000    # 11.0f

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v1, v2, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 1480
    :cond_15
    const/4 v2, 0x0

    :goto_d
    if-ge v2, v4, :cond_16

    .line 1481
    aget-object v5, v15, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v1

    int-to-float v6, v6

    int-to-float v7, v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1482
    add-int/2addr v3, v13

    .line 1480
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 1484
    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStrikeThruText(Z)V

    .line 1485
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 1486
    return-void

    :cond_17
    move v5, v9

    goto/16 :goto_9

    :cond_18
    move v2, v3

    move v4, v5

    move v3, v6

    goto/16 :goto_a

    :cond_19
    move v5, v3

    move v3, v7

    goto/16 :goto_5

    :cond_1a
    move v3, v10

    goto/16 :goto_c

    :cond_1b
    move v6, v9

    goto/16 :goto_4
.end method

.method private a(Landroid/view/MotionEvent;Ljava/lang/Long;)V
    .locals 19

    .prologue
    .line 2396
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2397
    new-instance v2, Lcom/android/calendar/event/iv;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/android/calendar/event/iv;-><init>(Landroid/content/Context;JJZ)V

    .line 2398
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    .line 2399
    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 2400
    sget-object v5, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    .line 2401
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/app/Fragment;->isRemoving()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2403
    :try_start_0
    invoke-virtual {v4, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2409
    :cond_0
    :goto_0
    :try_start_1
    sget-object v3, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2410
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2420
    :goto_1
    return-void

    .line 2415
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    const-wide/16 v4, 0x1

    const-wide/16 v6, -0x1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x10

    const-wide/16 v16, -0x1

    const/16 v18, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v18}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJZ)V

    goto :goto_1

    .line 2404
    :catch_0
    move-exception v3

    goto :goto_0

    .line 2411
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method private a(Lcom/android/calendar/dh;)V
    .locals 3

    .prologue
    .line 3454
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_1

    .line 3455
    iget v0, p1, Lcom/android/calendar/dh;->F:F

    iget v1, p1, Lcom/android/calendar/dh;->E:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 3456
    iget v1, p0, Lcom/android/calendar/e/ao;->cA:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cG:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, p1, Lcom/android/calendar/dh;->E:F

    .line 3457
    iget v1, p1, Lcom/android/calendar/dh;->E:F

    iget v2, p0, Lcom/android/calendar/e/ao;->G:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 3458
    iget v1, p0, Lcom/android/calendar/e/ao;->G:F

    iput v1, p1, Lcom/android/calendar/dh;->E:F

    .line 3460
    :cond_0
    iget v1, p1, Lcom/android/calendar/dh;->E:F

    int-to-float v0, v0

    add-float/2addr v0, v1

    iput v0, p1, Lcom/android/calendar/dh;->F:F

    .line 3461
    iget v0, p1, Lcom/android/calendar/dh;->D:F

    iget v1, p1, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 3462
    iget v1, p0, Lcom/android/calendar/e/ao;->cy:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cF:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, p1, Lcom/android/calendar/dh;->C:F

    .line 3463
    iget v1, p1, Lcom/android/calendar/dh;->C:F

    int-to-float v0, v0

    add-float/2addr v0, v1

    iput v0, p1, Lcom/android/calendar/dh;->D:F

    .line 3465
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/e/ao;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->f(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private a(Ljava/lang/Object;Landroid/view/MotionEvent;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2256
    if-nez p2, :cond_1

    .line 2315
    :cond_0
    :goto_0
    return-void

    .line 2259
    :cond_1
    iput-boolean v2, p0, Lcom/android/calendar/e/ao;->bC:Z

    .line 2260
    if-nez p1, :cond_2

    .line 2261
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->d(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 2263
    :cond_2
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2264
    instance-of v0, p1, Lcom/android/calendar/dh;

    if-eqz v0, :cond_3

    .line 2265
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2266
    check-cast p1, Lcom/android/calendar/dh;

    .line 2267
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 2268
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2269
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2270
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2271
    invoke-direct {p0, v0, v3, v1}, Lcom/android/calendar/e/ao;->a(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 2273
    :cond_3
    instance-of v0, p1, Lcom/android/calendar/hc;

    if-eqz v0, :cond_8

    .line 2274
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2276
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 2277
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 2279
    check-cast p1, Lcom/android/calendar/hc;

    .line 2280
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 2281
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2282
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2283
    invoke-static {p1}, Lcom/android/calendar/task/aa;->a(Lcom/android/calendar/hc;)Lcom/android/calendar/task/aa;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2284
    invoke-direct {p0, v3, v4}, Lcom/android/calendar/e/ao;->c(II)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lcom/android/calendar/hc;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2285
    iget-boolean v0, p1, Lcom/android/calendar/hc;->f:Z

    if-nez v0, :cond_4

    move v0, v1

    .line 2286
    :goto_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 2287
    const-string v4, "complete"

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2288
    const-string v0, "date_completed"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2289
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    iget-wide v4, p1, Lcom/android/calendar/hc;->a:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 2290
    iget-object v4, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v0, v3, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2293
    iget-boolean v0, p1, Lcom/android/calendar/hc;->f:Z

    if-nez v0, :cond_6

    :goto_3
    iput-boolean v1, p1, Lcom/android/calendar/hc;->f:Z

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 2285
    goto :goto_1

    :cond_5
    move v0, v2

    .line 2287
    goto :goto_2

    :cond_6
    move v1, v2

    .line 2293
    goto :goto_3

    .line 2295
    :cond_7
    iput-object p1, p0, Lcom/android/calendar/e/ao;->cI:Lcom/android/calendar/hc;

    .line 2296
    invoke-direct {p0, v0, v5, v6}, Lcom/android/calendar/e/ao;->a(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 2299
    :cond_8
    instance-of v0, p1, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/ao;->a(I)I

    move-result v0

    if-ne v0, v1, :cond_9

    .line 2300
    sget-object v0, Lcom/android/calendar/e/bd;->c:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2301
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->d(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 2302
    :cond_9
    instance-of v0, p1, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2303
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2306
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2307
    iput-boolean v1, p0, Lcom/android/calendar/e/ao;->bC:Z

    .line 2308
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 2309
    new-instance v2, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    iget v3, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2310
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    iget v3, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/android/calendar/task/aa;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2311
    invoke-direct {p0, v0, v2, v1}, Lcom/android/calendar/e/ao;->a(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;ZZLandroid/graphics/Canvas;Z)V
    .locals 8

    .prologue
    .line 846
    invoke-direct {p0, p6, p2}, Lcom/android/calendar/e/ao;->a(Landroid/graphics/Canvas;I)V

    .line 847
    if-eqz p4, :cond_1

    .line 848
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->cC:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-eqz v0, :cond_0

    .line 849
    invoke-direct {p0, p6, p2}, Lcom/android/calendar/e/ao;->b(Landroid/graphics/Canvas;I)V

    .line 851
    :cond_0
    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-nez v0, :cond_1

    .line 852
    invoke-direct {p0, p6, p2}, Lcom/android/calendar/e/ao;->c(Landroid/graphics/Canvas;I)V

    .line 856
    :cond_1
    if-eqz p5, :cond_2

    .line 857
    invoke-direct {p0, p6, p2}, Lcom/android/calendar/e/ao;->d(Landroid/graphics/Canvas;I)V

    .line 859
    :cond_2
    const/4 v0, 0x0

    .line 860
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "JAPAN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 861
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bY:Lcom/android/calendar/d/g;

    invoke-virtual {v1}, Lcom/android/calendar/d/g;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 862
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 863
    iget v2, v1, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v2, p2

    iput v2, v1, Landroid/text/format/Time;->monthDay:I

    .line 864
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 865
    iget-object v2, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    iget v3, v1, Landroid/text/format/Time;->month:I

    iget v4, v1, Landroid/text/format/Time;->monthDay:I

    invoke-static {v2, v3, v4}, Lcom/android/calendar/hj;->a(III)I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->weekDay:I

    .line 866
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bZ:Lcom/android/calendar/ej;

    invoke-virtual {v2, v1}, Lcom/android/calendar/ej;->a(Landroid/text/format/Time;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bZ:Lcom/android/calendar/ej;

    invoke-virtual {v2, v1}, Lcom/android/calendar/ej;->b(Landroid/text/format/Time;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 868
    :cond_3
    const/4 v0, 0x1

    .line 873
    :cond_4
    if-eqz v0, :cond_8

    .line 874
    iget-object v1, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/e/ao;->ay:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 878
    :goto_0
    if-eqz p5, :cond_9

    .line 879
    iget-object v0, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 889
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->K:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 890
    sget v0, Lcom/android/calendar/e/ao;->at:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 891
    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 892
    iget v2, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    .line 893
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 894
    iget v3, p0, Lcom/android/calendar/e/ao;->bF:I

    if-ge p2, v3, :cond_b

    .line 895
    int-to-float v0, v0

    int-to-float v3, v2

    iget v4, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v3, v4

    sub-float/2addr v0, v3

    float-to-int v0, v0

    .line 899
    :cond_5
    :goto_2
    iget v3, p0, Lcom/android/calendar/e/ao;->G:F

    iget v4, p0, Lcom/android/calendar/e/ao;->K:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 900
    iget v4, p0, Lcom/android/calendar/e/ao;->G:F

    sub-float v3, v4, v3

    .line 901
    iget-object v4, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    const/4 v5, 0x0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v4, p3, v5, v6, v7}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 902
    int-to-float v0, v0

    iget-object v4, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    invoke-virtual {p6, p1, v0, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 905
    iget-object v0, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/e/ao;->J:F

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 906
    iget-object v0, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    .line 907
    iget v0, p0, Lcom/android/calendar/e/ao;->H:F

    iget v4, p0, Lcom/android/calendar/e/ao;->G:F

    sub-float/2addr v0, v4

    iget v4, p0, Lcom/android/calendar/e/ao;->J:F

    sub-float/2addr v0, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v0, v4

    .line 908
    iget v4, p0, Lcom/android/calendar/e/ao;->H:F

    iget v5, p0, Lcom/android/calendar/e/ao;->D:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/e/ao;->E:F

    sub-float/2addr v4, v5

    sub-float/2addr v4, v0

    .line 909
    sget v0, Lcom/android/calendar/e/ao;->at:I

    int-to-float v0, v0

    iget-object v5, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {v5, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    sub-float/2addr v0, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v0, v5

    .line 910
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v0, v5

    .line 911
    iget v5, p0, Lcom/android/calendar/e/ao;->bF:I

    if-ge p2, v5, :cond_c

    .line 912
    int-to-float v1, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 916
    :cond_6
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v6, p0, Lcom/android/calendar/e/ao;->u:Landroid/graphics/Rect;

    invoke-virtual {v1, p3, v2, v5, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 917
    if-eqz p7, :cond_7

    .line 918
    iget-object v1, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    const/16 v2, 0x66

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 920
    :cond_7
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v3, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {p6, p3, v0, v4, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 921
    return-void

    .line 876
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->aC:[I

    aget v2, v2, p2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 881
    :cond_9
    if-eqz v0, :cond_a

    .line 882
    iget-object v0, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->ax:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_1

    .line 884
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aB:[I

    aget v1, v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_1

    .line 896
    :cond_b
    iget v3, p0, Lcom/android/calendar/e/ao;->bF:I

    if-le p2, v3, :cond_5

    .line 897
    int-to-float v0, v0

    int-to-float v3, v1

    iget v4, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    goto/16 :goto_2

    .line 913
    :cond_c
    iget v2, p0, Lcom/android/calendar/e/ao;->bF:I

    if-le p2, v2, :cond_6

    .line 914
    int-to-float v1, v1

    iget v2, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_3
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2619
    instance-of v0, p2, Lcom/android/calendar/dh;

    if-eqz v0, :cond_1

    .line 2620
    check-cast p2, Lcom/android/calendar/dh;

    .line 2621
    invoke-virtual {p2}, Lcom/android/calendar/dh;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2630
    :cond_0
    :goto_0
    return-void

    .line 2622
    :cond_1
    instance-of v0, p2, Lcom/android/calendar/hc;

    if-eqz v0, :cond_2

    .line 2623
    check-cast p2, Lcom/android/calendar/hc;

    .line 2624
    iget-object v0, p2, Lcom/android/calendar/hc;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2625
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2626
    const-string v0, "+"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bm:[I

    iget v2, p0, Lcom/android/calendar/e/ao;->bQ:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2627
    :cond_3
    instance-of v0, p2, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2628
    iget-object v0, p0, Lcom/android/calendar/e/ao;->be:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 2591
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bt:Z

    if-nez v0, :cond_1

    .line 2616
    :cond_0
    :goto_0
    return-void

    .line 2594
    :cond_1
    iget v0, p0, Lcom/android/calendar/e/ao;->bR:I

    iget v1, p0, Lcom/android/calendar/e/ao;->bQ:I

    if-eq v0, v1, :cond_6

    const/4 v0, 0x1

    .line 2595
    :goto_1
    if-nez v0, :cond_2

    if-eqz p1, :cond_0

    .line 2596
    :cond_2
    iget v1, p0, Lcom/android/calendar/e/ao;->bQ:I

    iput v1, p0, Lcom/android/calendar/e/ao;->bR:I

    .line 2597
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2598
    if-eqz v0, :cond_3

    .line 2599
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0139

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2600
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " %A"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2601
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getSelectedTimeForAccessbility()Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2603
    :cond_3
    if-eqz p1, :cond_5

    .line 2604
    if-eqz v0, :cond_4

    .line 2605
    const-string v0, ". "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2607
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/e/ao;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 2609
    :cond_5
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/ao;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2610
    const v0, 0x8000

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 2611
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2612
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2613
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 2614
    invoke-virtual {p0, v0}, Lcom/android/calendar/e/ao;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 2594
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/text/format/Time;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1161
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1162
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 1163
    invoke-virtual {v1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    .line 1164
    invoke-virtual {p1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 1166
    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/e/ao;Z)Z
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/android/calendar/e/ao;->bA:Z

    return p1
.end method

.method private b(IZ)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3291
    .line 3292
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3293
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3294
    if-eqz p2, :cond_2

    .line 3296
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3297
    iget-object v1, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 3298
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    .line 3304
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3305
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 3307
    :cond_0
    add-int v0, v1, v2

    .line 3308
    iget v1, p0, Lcom/android/calendar/e/ao;->ce:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 3309
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/e/ao;->bz:Z

    .line 3310
    iget v1, p0, Lcom/android/calendar/e/ao;->am:I

    if-ge v0, v1, :cond_1

    .line 3311
    rem-int/lit8 v1, v0, 0x2

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 3314
    :cond_1
    return v0

    .line 3300
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/e/ao;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/android/calendar/e/ao;->P:I

    return v0
.end method

.method private b(II)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2160
    iget v0, p0, Lcom/android/calendar/e/ao;->cq:I

    iget v1, p0, Lcom/android/calendar/e/ao;->C:I

    sub-int/2addr v0, v1

    add-int v1, p1, v0

    .line 2161
    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->h(I)I

    move-result v4

    .line 2162
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->i(I)I

    move-result v0

    .line 2163
    iget-boolean v3, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v3, :cond_0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 2164
    iget v3, p0, Lcom/android/calendar/e/ao;->cl:I

    div-int/lit8 v3, v3, 0x2

    if-le v1, v3, :cond_3

    .line 2165
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v3, v0, 0x1

    .line 2166
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    move v1, v2

    .line 2167
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 2168
    :goto_1
    add-int/lit8 v0, v3, 0x1

    add-int/2addr v1, v2

    if-le v0, v1, :cond_5

    .line 2169
    add-int/lit8 v0, v3, -0x1

    .line 2175
    :cond_0
    :goto_2
    iput v4, p0, Lcom/android/calendar/e/ao;->bF:I

    .line 2176
    iput v0, p0, Lcom/android/calendar/e/ao;->bH:I

    .line 2177
    if-ltz v0, :cond_4

    iget v1, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_4

    if-ltz v4, :cond_4

    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v1, v1, -0x1

    if-gt v4, v1, :cond_4

    .line 2178
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    aget-object v1, v1, v4

    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    .line 2182
    :goto_3
    return-void

    .line 2166
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 2167
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto :goto_1

    .line 2172
    :cond_3
    mul-int/lit8 v0, v0, 0x2

    goto :goto_2

    .line 2180
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_2
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 731
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->p()V

    .line 732
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bp:Z

    if-nez v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/android/calendar/e/ao;->t:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->r:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 734
    iget-object v0, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/e/ao;->cH:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 735
    iget-object v0, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 737
    :cond_0
    return-void
.end method

.method private b(Landroid/graphics/Canvas;I)V
    .locals 3

    .prologue
    .line 990
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 991
    if-nez p2, :cond_0

    .line 992
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 994
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/lit8 v1, p2, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 995
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/e/ao;->D:F

    iget v2, p0, Lcom/android/calendar/e/ao;->E:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->G:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 996
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/e/ao;->D:F

    iget v2, p0, Lcom/android/calendar/e/ao;->E:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->H:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 998
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 999
    return-void
.end method

.method private static b(Landroid/text/format/Time;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3597
    iget v0, p0, Landroid/text/format/Time;->month:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/text/format/Time;->monthDay:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 3598
    iget v0, p0, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/text/format/Time;->year:I

    .line 3599
    iput v2, p0, Landroid/text/format/Time;->month:I

    .line 3600
    iput v3, p0, Landroid/text/format/Time;->monthDay:I

    .line 3601
    iput-boolean v2, p0, Landroid/text/format/Time;->allDay:Z

    .line 3602
    iput v3, p0, Landroid/text/format/Time;->hour:I

    .line 3603
    iget v0, p0, Landroid/text/format/Time;->year:I

    const/16 v1, 0x78c

    if-ne v0, v1, :cond_0

    .line 3604
    const/4 v0, 0x5

    iput v0, p0, Landroid/text/format/Time;->weekDay:I

    .line 3607
    :cond_0
    return-void
.end method

.method private c(I)I
    .locals 4

    .prologue
    .line 781
    sget v0, Lcom/android/calendar/e/ao;->at:I

    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    mul-int/2addr v0, v1

    .line 782
    sget v1, Lcom/android/calendar/e/ao;->at:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    mul-int/2addr v1, v2

    sub-int v1, v0, v1

    .line 783
    sget v0, Lcom/android/calendar/e/ao;->at:I

    mul-int/2addr v0, p1

    .line 785
    const/4 v2, 0x1

    if-le p1, v2, :cond_0

    .line 786
    add-int/lit8 v2, p1, -0x1

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 789
    :cond_0
    add-int v2, v1, p1

    iget v3, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v2, v3, :cond_1

    .line 790
    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    sub-int v1, v2, v1

    sub-int v1, p1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 792
    :cond_1
    return v0
.end method

.method static synthetic c(Lcom/android/calendar/e/ao;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/e/ao;->q:Landroid/graphics/Paint;

    return-object v0
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 749
    iget-boolean v1, p0, Lcom/android/calendar/e/ao;->bp:Z

    if-eqz v1, :cond_0

    .line 751
    iget-object v1, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/e/ao;->F:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 752
    iget-object v1, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v0

    .line 753
    iget-object v1, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v3, 0x2

    iget v4, p0, Lcom/android/calendar/e/ao;->as:I

    int-to-float v4, v4

    iget v5, p0, Lcom/android/calendar/e/ao;->F:F

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    aput v4, v1, v2

    .line 754
    iget-object v1, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v2, 0x3

    iget v4, p0, Lcom/android/calendar/e/ao;->ar:I

    int-to-float v4, v4

    aput v4, v1, v3

    .line 755
    iget-object v1, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v3, 0x4

    iget v4, p0, Lcom/android/calendar/e/ao;->as:I

    int-to-float v4, v4

    iget v5, p0, Lcom/android/calendar/e/ao;->F:F

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    aput v4, v1, v2

    .line 756
    iget-object v1, p0, Lcom/android/calendar/e/ao;->z:[F

    iget-object v2, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v3, v2}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 760
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 761
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 762
    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    iget v3, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 763
    iget v3, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-direct {p0, v3}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v3

    .line 764
    :goto_0
    iget v4, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v0, v4, :cond_3

    .line 765
    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v4

    .line 767
    iget v5, p0, Lcom/android/calendar/e/ao;->bF:I

    if-gt v0, v5, :cond_2

    .line 768
    int-to-float v4, v4

    int-to-float v5, v3

    iget v6, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v1, Landroid/graphics/Rect;->left:I

    .line 769
    iget v4, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Landroid/graphics/Rect;->right:I

    .line 774
    :cond_1
    :goto_1
    iget v4, p0, Lcom/android/calendar/e/ao;->D:F

    iget v5, p0, Lcom/android/calendar/e/ao;->G:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/e/ao;->E:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v1, Landroid/graphics/Rect;->top:I

    .line 775
    iget v4, p0, Lcom/android/calendar/e/ao;->as:I

    iput v4, v1, Landroid/graphics/Rect;->bottom:I

    .line 776
    iget-object v4, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 764
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 770
    :cond_2
    iget v5, p0, Lcom/android/calendar/e/ao;->bF:I

    if-le v0, v5, :cond_1

    .line 771
    int-to-float v4, v4

    int-to-float v5, v2

    iget v6, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v1, Landroid/graphics/Rect;->left:I

    .line 772
    iget v4, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Landroid/graphics/Rect;->right:I

    goto :goto_1

    .line 778
    :cond_3
    return-void
.end method

.method private c(Landroid/graphics/Canvas;I)V
    .locals 4

    .prologue
    .line 1026
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->e(I)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    .line 1027
    iget-object v0, p0, Lcom/android/calendar/e/ao;->q:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cJ:Lcom/android/calendar/e/ba;

    iget v1, v1, Lcom/android/calendar/e/ba;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1028
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/android/calendar/e/ao;->P:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/android/calendar/e/ao;->P:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/calendar/e/ao;->cJ:Lcom/android/calendar/e/ba;

    iget v2, v2, Lcom/android/calendar/e/ba;->a:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/calendar/e/ao;->q:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1030
    return-void
.end method

.method private c(II)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2353
    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v2, v0, :cond_0

    .line 2365
    :goto_0
    return v1

    .line 2356
    :cond_0
    iget v2, p0, Lcom/android/calendar/e/ao;->ae:I

    iget v3, p0, Lcom/android/calendar/e/ao;->ac:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/ao;->ak:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v2, v3

    .line 2357
    iget-boolean v3, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v3, :cond_1

    .line 2358
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getNumberOfEvents()I

    move-result v3

    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getNumbetOfTasks()I

    move-result v4

    add-int/2addr v3, v4

    .line 2359
    rem-int/lit8 v4, v3, 0x2

    if-nez v4, :cond_2

    iget v4, p0, Lcom/android/calendar/e/ao;->bH:I

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_2

    .line 2360
    iget v3, p0, Lcom/android/calendar/e/ao;->cl:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 2365
    :cond_1
    :goto_1
    if-lt p1, v2, :cond_3

    iget v3, p0, Lcom/android/calendar/e/ao;->af:I

    add-int/2addr v2, v3

    if-gt p1, v2, :cond_3

    :goto_2
    move v1, v0

    goto :goto_0

    .line 2361
    :cond_2
    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/android/calendar/e/ao;->bH:I

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    .line 2362
    iget v3, p0, Lcom/android/calendar/e/ao;->cl:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2365
    goto :goto_2
.end method

.method private d(I)I
    .locals 3

    .prologue
    .line 796
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getEventStartYPos()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/e/ao;->W:I

    add-int/2addr v0, v1

    .line 797
    iget v1, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v2, p0, Lcom/android/calendar/e/ao;->W:I

    add-int/2addr v1, v2

    mul-int/2addr v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/e/ao;)Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    return-object v0
.end method

.method private d(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2575
    iget v0, p0, Lcom/android/calendar/e/ao;->cq:I

    iget v1, p0, Lcom/android/calendar/e/ao;->C:I

    sub-int/2addr v0, v1

    add-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->h(I)I

    move-result v0

    .line 2576
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->i(I)I

    move-result v1

    .line 2577
    iput v0, p0, Lcom/android/calendar/e/ao;->bQ:I

    .line 2578
    if-ltz v1, :cond_1

    iget v2, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_1

    if-ltz v0, :cond_1

    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v2, v2, -0x1

    if-gt v0, v2, :cond_1

    .line 2579
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    aget-object v0, v2, v0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    .line 2583
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    .line 2584
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    .line 2585
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    .line 2588
    :cond_0
    return-void

    .line 2581
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    goto :goto_0
.end method

.method private d(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 801
    iget-object v0, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 802
    iget-object v0, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 804
    new-instance v10, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-direct {v10, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 809
    const/16 v0, 0xc

    iput v0, v10, Landroid/text/format/Time;->hour:I

    .line 810
    iput-boolean v8, v10, Landroid/text/format/Time;->allDay:Z

    .line 811
    invoke-virtual {v10, v9}, Landroid/text/format/Time;->normalize(Z)J

    .line 813
    iget-object v0, p0, Lcom/android/calendar/e/ao;->ba:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 814
    iget-object v0, p0, Lcom/android/calendar/e/ao;->ba:Landroid/text/format/Time;

    invoke-virtual {v0, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/e/ao;->ba:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 815
    iget v1, p0, Lcom/android/calendar/e/ao;->aV:I

    sub-int v11, v0, v1

    move v2, v8

    .line 816
    :goto_0
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v2, v0, :cond_7

    .line 817
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->cK:Z

    if-eqz v0, :cond_0

    .line 818
    invoke-static {v10}, Lcom/android/calendar/e/ao;->b(Landroid/text/format/Time;)V

    .line 820
    :cond_0
    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bn:[Z

    aget-boolean v4, v0, v2

    .line 821
    :goto_1
    if-ne v11, v2, :cond_5

    move v5, v9

    .line 822
    :goto_2
    iget v0, v10, Landroid/text/format/Time;->monthDay:I

    const/4 v1, 0x4

    invoke-virtual {v10, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    if-le v0, v1, :cond_1

    .line 823
    invoke-virtual {v10, v9}, Landroid/text/format/Time;->normalize(Z)J

    .line 825
    :cond_1
    const/high16 v0, -0x1000000

    .line 826
    iget v1, v10, Landroid/text/format/Time;->weekDay:I

    .line 827
    iget-object v3, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 828
    iget-object v3, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 830
    iget v0, v10, Landroid/text/format/Time;->year:I

    sget v3, Lcom/android/calendar/e/ao;->ap:I

    if-lt v0, v3, :cond_2

    iget v0, v10, Landroid/text/format/Time;->year:I

    sget v3, Lcom/android/calendar/e/ao;->aq:I

    if-le v0, v3, :cond_8

    :cond_2
    move v7, v9

    .line 834
    :goto_3
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bB:Z

    if-eqz v0, :cond_6

    .line 835
    const-string v0, "%d"

    new-array v3, v9, [Ljava/lang/Object;

    iget v6, v10, Landroid/text/format/Time;->monthDay:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v8

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 839
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bo:[Ljava/lang/String;

    aget-object v1, v0, v1

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/e/ao;->a(Ljava/lang/String;ILjava/lang/String;ZZLandroid/graphics/Canvas;Z)V

    .line 840
    iget v0, v10, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v10, Landroid/text/format/Time;->monthDay:I

    .line 841
    iget v0, v10, Landroid/text/format/Time;->weekDay:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v10, Landroid/text/format/Time;->weekDay:I

    .line 816
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 820
    :cond_3
    iget v0, p0, Lcom/android/calendar/e/ao;->aW:I

    iget v1, p0, Lcom/android/calendar/e/ao;->aV:I

    sub-int/2addr v0, v1

    if-ne v2, v0, :cond_4

    move v4, v9

    goto :goto_1

    :cond_4
    move v4, v8

    goto :goto_1

    :cond_5
    move v5, v8

    .line 821
    goto :goto_2

    .line 837
    :cond_6
    iget v0, v10, Landroid/text/format/Time;->monthDay:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 843
    :cond_7
    return-void

    :cond_8
    move v7, v8

    goto :goto_3
.end method

.method private d(Landroid/graphics/Canvas;I)V
    .locals 4

    .prologue
    .line 1033
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->e(I)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    .line 1034
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/android/calendar/e/ao;->P:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/android/calendar/e/ao;->P:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/android/calendar/e/ao;->P:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/calendar/e/ao;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1036
    return-void
.end method

.method private d(Landroid/view/MotionEvent;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 2318
    iget v0, p0, Lcom/android/calendar/e/ao;->bG:I

    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    if-ne v0, v1, :cond_2

    move v0, v6

    .line 2319
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bJ:Ljava/lang/Object;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    if-nez v1, :cond_3

    move v1, v6

    .line 2320
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    instance-of v2, v2, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-virtual {p0, v2}, Lcom/android/calendar/e/ao;->a(I)I

    move-result v2

    if-ne v2, v6, :cond_4

    move v2, v6

    .line 2322
    :goto_2
    if-eqz v0, :cond_5

    if-nez v1, :cond_0

    if-eqz v2, :cond_5

    :cond_0
    move v0, v6

    .line 2324
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v2, Lcom/android/calendar/e/bd;->c:Lcom/android/calendar/e/bd;

    if-ne v1, v2, :cond_7

    if-eqz v0, :cond_7

    .line 2325
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2326
    new-instance v0, Lcom/android/calendar/event/iv;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-virtual {v2, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-virtual {v4, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/event/iv;-><init>(Landroid/content/Context;JJZ)V

    .line 2327
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 2328
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 2329
    sget-object v3, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 2330
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2332
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2338
    :cond_1
    :goto_4
    :try_start_1
    sget-object v1, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2339
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2349
    :goto_5
    return-void

    :cond_2
    move v0, v8

    .line 2318
    goto :goto_0

    :cond_3
    move v1, v8

    .line 2319
    goto :goto_1

    :cond_4
    move v2, v8

    .line 2320
    goto :goto_2

    :cond_5
    move v0, v8

    .line 2322
    goto :goto_3

    .line 2344
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    const-wide/16 v2, 0x1

    iget-object v4, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    const-wide/16 v6, -0x1

    const-wide/16 v9, 0x10

    move-object v1, p0

    move-object v11, v5

    move-object v12, v5

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_5

    .line 2347
    :cond_7
    sget-object v0, Lcom/android/calendar/e/bd;->c:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    goto :goto_5

    .line 2333
    :catch_0
    move-exception v1

    goto :goto_4

    .line 2340
    :catch_1
    move-exception v0

    goto :goto_5
.end method

.method static synthetic e(Lcom/android/calendar/e/ao;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    return-object v0
.end method

.method private e(I)Landroid/graphics/Rect;
    .locals 7

    .prologue
    .line 1002
    iget v1, p0, Lcom/android/calendar/e/ao;->P:I

    .line 1004
    sget v0, Lcom/android/calendar/e/ao;->at:I

    sub-int/2addr v0, v1

    div-int/lit8 v2, v0, 0x2

    .line 1005
    iget v0, p0, Lcom/android/calendar/e/ao;->H:F

    iget v3, p0, Lcom/android/calendar/e/ao;->G:F

    sub-float/2addr v0, v3

    int-to-float v3, v1

    sub-float/2addr v0, v3

    float-to-int v0, v0

    div-int/lit8 v3, v0, 0x2

    .line 1006
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v0

    iget v4, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {p0, v4}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v4

    sub-int v4, v0, v4

    .line 1007
    iget v0, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v5

    .line 1009
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v0

    .line 1010
    iget v6, p0, Lcom/android/calendar/e/ao;->bF:I

    if-ge p1, v6, :cond_1

    .line 1011
    int-to-float v0, v0

    int-to-float v4, v5

    iget v5, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v4, v5

    sub-float/2addr v0, v4

    float-to-int v0, v0

    .line 1016
    :cond_0
    :goto_0
    add-int/2addr v0, v2

    .line 1017
    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    sub-int v2, v0, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 1018
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v1

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 1019
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/e/ao;->D:F

    iget v4, p0, Lcom/android/calendar/e/ao;->G:F

    add-float/2addr v2, v4

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 1020
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1022
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    return-object v0

    .line 1012
    :cond_1
    iget v5, p0, Lcom/android/calendar/e/ao;->bF:I

    if-le p1, v5, :cond_0

    .line 1013
    int-to-float v0, v0

    int-to-float v4, v4

    iget v5, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v4, v5

    add-float/2addr v0, v4

    float-to-int v0, v0

    goto :goto_0
.end method

.method private e(II)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 2901
    iget v0, p0, Lcom/android/calendar/e/ao;->cq:I

    iget v1, p0, Lcom/android/calendar/e/ao;->C:I

    sub-int/2addr v0, v1

    add-int v4, p1, v0

    .line 2902
    invoke-direct {p0, v4}, Lcom/android/calendar/e/ao;->h(I)I

    move-result v5

    .line 2903
    invoke-direct {p0, p2}, Lcom/android/calendar/e/ao;->i(I)I

    move-result v0

    .line 2904
    iget-boolean v1, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v1, :cond_0

    .line 2905
    iget v1, p0, Lcom/android/calendar/e/ao;->cl:I

    div-int/lit8 v1, v1, 0x2

    if-le v4, v1, :cond_4

    .line 2906
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v3, v0, 0x1

    .line 2907
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    move v1, v2

    .line 2908
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    move v0, v2

    .line 2909
    :goto_1
    add-int/lit8 v6, v3, 0x1

    add-int/2addr v0, v1

    if-le v6, v0, :cond_6

    .line 2910
    add-int/lit8 v0, v3, -0x1

    .line 2916
    :cond_0
    :goto_2
    iput v5, p0, Lcom/android/calendar/e/ao;->bK:I

    .line 2917
    iput v0, p0, Lcom/android/calendar/e/ao;->bL:I

    .line 2918
    if-ltz v0, :cond_5

    iget v1, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_5

    if-ltz v5, :cond_5

    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v1, v1, -0x1

    if-gt v5, v1, :cond_5

    .line 2919
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    aget-object v1, v1, v5

    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    .line 2923
    :goto_3
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bv:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bu:Z

    if-eqz v0, :cond_1

    .line 2924
    sget v0, Lcom/android/calendar/e/ao;->at:I

    div-int/lit8 v0, v0, 0x2

    .line 2925
    invoke-direct {p0, v7}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    sub-int/2addr v1, v0

    .line 2926
    const/4 v3, 0x3

    invoke-direct {p0, v3}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v3

    add-int/2addr v3, v0

    .line 2927
    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v5

    sub-int/2addr v5, v0

    .line 2928
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v6

    add-int/2addr v0, v6

    .line 2929
    if-ge v1, v4, :cond_1

    if-ge v4, v3, :cond_1

    if-ge v5, p2, :cond_1

    if-ge p2, v0, :cond_1

    .line 2930
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    aget-object v0, v0, v7

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    .line 2931
    iput v7, p0, Lcom/android/calendar/e/ao;->bK:I

    .line 2932
    iput v2, p0, Lcom/android/calendar/e/ao;->bL:I

    .line 2935
    :cond_1
    return-void

    .line 2907
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 2908
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_1

    .line 2913
    :cond_4
    mul-int/lit8 v0, v0, 0x2

    goto :goto_2

    .line 2921
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    goto :goto_3

    :cond_6
    move v0, v3

    goto :goto_2
.end method

.method private e(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 924
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 925
    invoke-virtual {v0, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 926
    iget v1, v0, Landroid/text/format/Time;->weekDay:I

    .line 927
    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    .line 928
    const-string v2, "%d"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 931
    iget-object v2, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/e/ao;->L:F

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 932
    iget-object v2, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/e/ao;->aI:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 933
    iget-object v2, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 934
    iget-object v2, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    .line 935
    iget v3, p0, Lcom/android/calendar/e/ao;->I:F

    iget-object v4, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->ascent()F

    move-result v4

    iget-object v5, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v7

    .line 936
    iget v4, p0, Lcom/android/calendar/e/ao;->I:F

    sub-float v3, v4, v3

    .line 937
    iget v4, p0, Lcom/android/calendar/e/ao;->N:I

    int-to-float v4, v4

    .line 938
    invoke-direct {p0, v8}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v5, v4

    .line 939
    div-float v6, v2, v7

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v3, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 942
    iget-object v0, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/e/ao;->M:F

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 943
    iget-object v0, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/e/ao;->aJ:I

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 944
    iget-object v0, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 945
    add-float v0, v4, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->O:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    .line 946
    invoke-direct {p0, v8}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v0, v2

    .line 947
    iget v2, p0, Lcom/android/calendar/e/ao;->I:F

    iget-object v3, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    iget-object v4, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->descent()F

    move-result v4

    add-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v7

    .line 948
    iget v3, p0, Lcom/android/calendar/e/ao;->I:F

    sub-float v2, v3, v2

    .line 949
    iget-object v3, p0, Lcom/android/calendar/e/ao;->bo:[Ljava/lang/String;

    aget-object v1, v3, v1

    iget-object v3, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 950
    return-void
.end method

.method private e(Landroid/view/MotionEvent;)V
    .locals 19

    .prologue
    .line 2423
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/ao;->cq:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->C:I

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 2424
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/e/ao;->h(I)I

    move-result v3

    .line 2425
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/ao;->bF:I

    if-ge v3, v2, :cond_1

    move v2, v3

    .line 2426
    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bF:I

    if-le v3, v4, :cond_2

    .line 2428
    :goto_1
    new-instance v8, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-direct {v8, v4}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2429
    iget v4, v8, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v2, v4

    iput v2, v8, Landroid/text/format/Time;->monthDay:I

    .line 2430
    const-string v2, "UTC"

    iput-object v2, v8, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2431
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 2433
    new-instance v10, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-direct {v10, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2434
    iget v2, v10, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v2, v3

    iput v2, v10, Landroid/text/format/Time;->monthDay:I

    .line 2435
    const-string v2, "UTC"

    iput-object v2, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2436
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 2438
    invoke-static {v8}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v10}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v2

    if-eqz v2, :cond_3

    .line 2440
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->i()V

    .line 2441
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/e/ao;->h()V

    .line 2469
    :goto_2
    return-void

    .line 2425
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/ao;->bF:I

    goto :goto_0

    .line 2426
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/ao;->bF:I

    goto :goto_1

    .line 2445
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2446
    new-instance v2, Lcom/android/calendar/event/iv;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v8, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-virtual {v10, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/android/calendar/event/iv;-><init>(Landroid/content/Context;JJZ)V

    .line 2447
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    .line 2448
    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 2449
    sget-object v5, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    .line 2450
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/app/Fragment;->isRemoving()Z

    move-result v5

    if-nez v5, :cond_4

    .line 2452
    :try_start_0
    invoke-virtual {v4, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2458
    :cond_4
    :goto_3
    :try_start_1
    sget-object v3, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2459
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 2460
    :catch_0
    move-exception v2

    goto :goto_2

    .line 2464
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    const-wide/16 v4, 0x1

    const-wide/16 v6, -0x1

    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x10

    const-wide/16 v16, -0x1

    const/16 v18, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v18}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJZ)V

    goto :goto_2

    .line 2453
    :catch_1
    move-exception v3

    goto :goto_3
.end method

.method private f(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1039
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v0, :cond_0

    .line 1040
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 1041
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->g(Landroid/graphics/Canvas;)V

    .line 1047
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1062
    :cond_1
    :goto_1
    return-void

    .line 1043
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->h(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1050
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v0, :cond_4

    .line 1051
    sget v0, Lcom/android/calendar/e/ao;->at:I

    div-int/lit8 v0, v0, 0x2

    sput v0, Lcom/android/calendar/e/ao;->at:I

    .line 1053
    :cond_4
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->m(Landroid/graphics/Canvas;)V

    .line 1054
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->i(Landroid/graphics/Canvas;)V

    .line 1055
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->k(Landroid/graphics/Canvas;)V

    .line 1056
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->l(Landroid/graphics/Canvas;)V

    .line 1057
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->n(Landroid/graphics/Canvas;)V

    .line 1058
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->j(Landroid/graphics/Canvas;)V

    .line 1059
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v0, :cond_1

    .line 1060
    sget v0, Lcom/android/calendar/e/ao;->at:I

    mul-int/lit8 v0, v0, 0x2

    sput v0, Lcom/android/calendar/e/ao;->at:I

    goto :goto_1
.end method

.method private f(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2472
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    if-eqz v0, :cond_0

    .line 2473
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->e()V

    .line 2475
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/ao;->cg:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 2518
    :cond_1
    :goto_0
    return-void

    .line 2478
    :cond_2
    iget v0, p0, Lcom/android/calendar/e/ao;->bH:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v0, v2, :cond_3

    .line 2479
    sput-boolean v2, Lcom/android/calendar/e/ao;->b:Z

    goto :goto_0

    .line 2482
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    if-eqz v0, :cond_6

    .line 2483
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/calendar/dh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/dh;

    invoke-static {v0}, Lcom/android/calendar/dh;->a(Lcom/android/calendar/dh;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_1

    .line 2487
    sget-object v0, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    .line 2488
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2489
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/ao;->cw:I

    .line 2490
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/ao;->cx:I

    .line 2492
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/dh;

    .line 2493
    iget v1, p0, Lcom/android/calendar/e/ao;->cx:I

    int-to-float v1, v1

    iput v1, v0, Lcom/android/calendar/dh;->E:F

    .line 2495
    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    .line 2496
    :cond_4
    if-lez v1, :cond_5

    .line 2497
    add-int/lit8 v1, v1, -0x1

    .line 2498
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    aget-object v2, v2, v1

    iget v3, p0, Lcom/android/calendar/e/ao;->bH:I

    aget-object v2, v2, v3

    .line 2499
    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2500
    add-int/lit8 v1, v1, 0x1

    .line 2504
    :cond_5
    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Lcom/android/calendar/dh;->C:F

    .line 2505
    iget v1, v0, Lcom/android/calendar/dh;->C:F

    sget v2, Lcom/android/calendar/e/ao;->at:I

    iget v3, v0, Lcom/android/calendar/dh;->j:I

    add-int/lit8 v3, v3, 0x1

    iget v4, v0, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v3, v4

    mul-int/2addr v2, v3

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Lcom/android/calendar/dh;->D:F

    .line 2506
    iget v1, v0, Lcom/android/calendar/dh;->E:F

    iget v2, p0, Lcom/android/calendar/e/ao;->Q:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Lcom/android/calendar/dh;->F:F

    .line 2508
    iput-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    .line 2509
    iget v1, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->cy:I

    .line 2510
    iget v1, v0, Lcom/android/calendar/dh;->E:F

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->cA:I

    .line 2511
    iget-wide v0, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v0, p0, Lcom/android/calendar/e/ao;->cz:J

    goto/16 :goto_0

    .line 2514
    :cond_6
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bw:Z

    if-nez v0, :cond_1

    .line 2515
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/e/ao;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->x()V

    return-void
.end method

.method private f(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1499
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/e/ao;->b(IZ)I

    move-result v0

    .line 1500
    iget-boolean v2, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v2, v2, p1

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v2, v1, :cond_0

    .line 1501
    add-int/lit8 v0, v0, 0x1

    .line 1503
    :cond_0
    iget v2, p0, Lcom/android/calendar/e/ao;->am:I

    if-le v0, v2, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(I)I
    .locals 6

    .prologue
    .line 1527
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 1528
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 1529
    const/4 v1, 0x2

    aget v2, v0, v1

    float-to-double v2, v2

    const-wide v4, 0x3fea8f5c28f5c28fL    # 0.83

    mul-double/2addr v2, v4

    double-to-float v2, v2

    aput v2, v0, v1

    .line 1530
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method private g(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1083
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->r()V

    .line 1084
    sget v0, Lcom/android/calendar/e/ao;->at:I

    iget v2, p0, Lcom/android/calendar/e/ao;->ag:I

    sub-int/2addr v0, v2

    div-int/lit8 v3, v0, 0x2

    .line 1085
    iget v0, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v2, p0, Lcom/android/calendar/e/ao;->ah:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    iget v2, p0, Lcom/android/calendar/e/ao;->D:F

    float-to-int v2, v2

    add-int/2addr v0, v2

    .line 1086
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getEventStartYPos()I

    move-result v2

    iget v4, p0, Lcom/android/calendar/e/ao;->W:I

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    int-to-float v4, v0

    .line 1088
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v0

    iget v2, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    sub-int v5, v0, v2

    .line 1089
    iget v0, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v6

    move v0, v1

    .line 1091
    :goto_0
    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v0, v2, :cond_3

    .line 1092
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v7, v2, v0

    .line 1093
    if-nez v7, :cond_0

    .line 1091
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1096
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/e/ao;->a(IILjava/lang/Object;)V

    .line 1097
    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    add-int/2addr v2, v3

    int-to-float v2, v2

    .line 1100
    iget v8, p0, Lcom/android/calendar/e/ao;->bF:I

    if-ge v0, v8, :cond_2

    .line 1101
    int-to-float v8, v6

    iget v9, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v8, v9

    sub-float/2addr v2, v8

    .line 1105
    :cond_1
    :goto_2
    iget-object v8, p0, Lcom/android/calendar/e/ao;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v2, v4, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1102
    :cond_2
    iget v8, p0, Lcom/android/calendar/e/ao;->bF:I

    if-le v0, v8, :cond_1

    .line 1103
    int-to-float v8, v5

    iget v9, p0, Lcom/android/calendar/e/ao;->a:F

    mul-float/2addr v8, v9

    add-float/2addr v2, v8

    goto :goto_2

    .line 1107
    :cond_3
    return-void
.end method

.method private g(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2545
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 2546
    packed-switch v2, :pswitch_data_0

    .line 2571
    :goto_0
    :pswitch_0
    return v0

    .line 2549
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 2550
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 2551
    invoke-direct {p0, v2, v3}, Lcom/android/calendar/e/ao;->d(II)V

    .line 2552
    int-to-float v2, v3

    iget v3, p0, Lcom/android/calendar/e/ao;->G:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 2553
    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->a(Z)V

    :goto_1
    move v0, v1

    .line 2571
    goto :goto_0

    .line 2556
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bT:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2558
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bT:Ljava/lang/Object;

    .line 2559
    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->a(Z)V

    .line 2561
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    goto :goto_1

    .line 2564
    :pswitch_2
    iput-object v3, p0, Lcom/android/calendar/e/ao;->bS:Ljava/lang/Object;

    .line 2565
    iput-object v3, p0, Lcom/android/calendar/e/ao;->bT:Ljava/lang/Object;

    goto :goto_1

    .line 2546
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic g(Lcom/android/calendar/e/ao;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bC:Z

    return v0
.end method

.method private getEventStartYPos()I
    .locals 3

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/android/calendar/e/ao;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/e/ao;->H:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1218
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->p()V

    .line 1220
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 1221
    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    if-eqz v1, :cond_1

    .line 1222
    iget v1, p0, Lcom/android/calendar/e/ao;->S:I

    add-int/2addr v0, v1

    .line 1224
    :cond_1
    return v0
.end method

.method private getNumberOfEvents()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3181
    .line 3182
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 3183
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move v3, v2

    .line 3184
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 3185
    add-int/lit8 v4, v1, 0x1

    .line 3184
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_0

    :cond_1
    move v1, v2

    .line 3189
    :cond_2
    return v1
.end method

.method private getNumbetOfTasks()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3193
    .line 3194
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 3195
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move v3, v2

    .line 3196
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 3197
    add-int/lit8 v4, v1, 0x1

    .line 3196
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_0

    :cond_1
    move v1, v2

    .line 3201
    :cond_2
    return v1
.end method

.method private getSelectedTimeForAccessbility()Landroid/text/format/Time;
    .locals 3

    .prologue
    .line 2633
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2634
    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    iget v2, p0, Lcom/android/calendar/e/ao;->bQ:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 2635
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 2636
    return-object v0
.end method

.method private h(I)I
    .locals 2

    .prologue
    .line 2185
    sget v0, Lcom/android/calendar/e/ao;->at:I

    add-int/lit8 v0, v0, 0x1

    div-int v0, p1, v0

    .line 2186
    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    if-lt v0, v1, :cond_0

    .line 2187
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v0, v0, -0x1

    .line 2189
    :cond_0
    if-gez v0, :cond_1

    .line 2190
    const/4 v0, 0x0

    .line 2192
    :cond_1
    return v0
.end method

.method static synthetic h(Lcom/android/calendar/e/ao;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/android/calendar/e/ao;->bF:I

    return v0
.end method

.method private h(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/16 v8, 0x21

    const/16 v7, 0xa

    const/4 v6, 0x1

    .line 1110
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    if-nez v0, :cond_0

    .line 1111
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    .line 1113
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/h/c;->a(Landroid/content/res/Resources;Landroid/text/format/Time;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1114
    if-nez v1, :cond_1

    .line 1158
    :goto_0
    return-void

    .line 1117
    :cond_1
    const-string v0, ""

    .line 1118
    iget-object v2, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->a(Landroid/text/format/Time;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1119
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    invoke-virtual {v0}, Lcom/android/calendar/h/c;->d()Ljava/lang/String;

    move-result-object v0

    .line 1121
    :cond_2
    if-eqz v0, :cond_3

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1122
    :cond_3
    const-string v0, ""

    .line 1125
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1126
    iget-object v3, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1127
    iget-object v3, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    const v4, 0x7f0c0066

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1128
    iget-object v2, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    const-string v3, "sec-roboto-light"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1129
    iget-object v2, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/e/ao;->aK:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1130
    iget-object v2, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    .line 1131
    invoke-direct {p0, v6}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v3

    int-to-float v3, v3

    sub-float v2, v3, v2

    iget v3, p0, Lcom/android/calendar/e/ao;->T:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 1132
    iget v3, p0, Lcom/android/calendar/e/ao;->R:I

    iget-object v4, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->ascent()F

    move-result v4

    iget-object v5, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    .line 1133
    iget v4, p0, Lcom/android/calendar/e/ao;->R:I

    int-to-float v4, v4

    sub-float v3, v4, v3

    .line 1134
    iget-object v4, p0, Lcom/android/calendar/e/ao;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1136
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/android/calendar/e/ao;->U:I

    add-int/2addr v0, v3

    int-to-float v0, v0

    sub-float v0, v2, v0

    .line 1137
    iget v2, p0, Lcom/android/calendar/e/ao;->R:I

    iget v3, p0, Lcom/android/calendar/e/ao;->ah:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 1138
    iget-object v3, p0, Lcom/android/calendar/e/ao;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1141
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->a(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1142
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    invoke-virtual {v0}, Lcom/android/calendar/h/c;->d()Ljava/lang/String;

    move-result-object v0

    .line 1143
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1144
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-le v2, v6, :cond_6

    .line 1145
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1146
    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v2, v7}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v2, v3, v0, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1151
    :cond_5
    new-instance v0, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v0, v7}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1154
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    invoke-virtual {v1}, Lcom/android/calendar/h/c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->be:Ljava/lang/String;

    goto/16 :goto_0

    .line 1156
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Lcom/android/calendar/h/c;->b(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->be:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private h(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/16 v4, 0x12c

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    const/4 v10, -0x1

    const/4 v12, 0x1

    const/4 v5, 0x0

    .line 2640
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->d(Landroid/content/Context;)Z

    move-result v0

    .line 2641
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->l(Landroid/content/Context;)Z

    move-result v1

    .line 2642
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    .line 2643
    iput-boolean v5, p0, Lcom/android/calendar/e/ao;->bu:Z

    .line 2644
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2645
    if-nez v0, :cond_2

    .line 2751
    :cond_0
    :goto_0
    return v5

    .line 2648
    :cond_1
    if-ne v2, v12, :cond_2

    .line 2649
    if-eqz v1, :cond_0

    .line 2652
    iput-boolean v12, p0, Lcom/android/calendar/e/ao;->bu:Z

    .line 2655
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 2656
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 2657
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    .line 2658
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2661
    :pswitch_1
    invoke-direct {p0, v3, v0}, Lcom/android/calendar/e/ao;->e(II)V

    .line 2662
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bu:Z

    if-eqz v0, :cond_3

    .line 2663
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 2665
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    .line 2666
    :cond_4
    invoke-direct {p0, v4}, Lcom/android/calendar/e/ao;->j(I)V

    move v5, v12

    .line 2667
    goto :goto_0

    .line 2669
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cP:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2670
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bN:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2671
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bN:Ljava/lang/Object;

    .line 2672
    if-ne v2, v12, :cond_8

    move v2, v12

    .line 2673
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/calendar/dh;

    if-eqz v0, :cond_9

    .line 2674
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/dh;

    .line 2675
    new-instance v1, Lcom/android/calendar/e/ai;

    iget-object v4, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-direct {v1, v4, v0, v2}, Lcom/android/calendar/e/ai;-><init>(Landroid/content/Context;Lcom/android/calendar/dh;Z)V

    iput-object v1, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    .line 2676
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2677
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cN:Ljava/lang/Runnable;

    const-wide/16 v8, 0x190

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2692
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    if-eqz v0, :cond_7

    .line 2693
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 2694
    invoke-virtual {p0, v4}, Lcom/android/calendar/e/ao;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2695
    iget v2, p0, Lcom/android/calendar/e/ao;->bL:I

    .line 2697
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v0, :cond_d

    .line 2698
    iget v0, p0, Lcom/android/calendar/e/ao;->am:I

    if-ge v2, v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v0, v0, v5

    add-int/lit8 v1, v2, 0x1

    aget-boolean v0, v0, v1

    if-nez v0, :cond_b

    move-wide v0, v6

    .line 2703
    :goto_3
    div-int/lit8 v2, v2, 0x2

    .line 2707
    :goto_4
    iget v6, p0, Lcom/android/calendar/e/ao;->bK:I

    invoke-direct {p0, v6}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v6

    sget v7, Lcom/android/calendar/e/ao;->at:I

    int-to-double v8, v7

    mul-double/2addr v0, v8

    double-to-int v0, v0

    add-int/2addr v0, v6

    iget v1, p0, Lcom/android/calendar/e/ao;->C:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/e/ao;->cq:I

    sub-int v1, v0, v1

    .line 2709
    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v0

    iget v2, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->W:I

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->W:I

    sub-int v2, v0, v2

    .line 2710
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    instance-of v0, v0, Lcom/android/calendar/e/ai;

    if-eqz v0, :cond_e

    .line 2711
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    check-cast v0, Lcom/android/calendar/e/ai;

    sget v4, Lcom/android/calendar/e/ao;->at:I

    iget v5, p0, Lcom/android/calendar/e/ao;->cd:I

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/e/ai;->a(IIIII)V

    :cond_7
    :goto_5
    move v5, v12

    .line 2729
    goto/16 :goto_0

    :cond_8
    move v2, v5

    .line 2672
    goto :goto_1

    .line 2678
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/calendar/hc;

    if-eqz v0, :cond_a

    .line 2679
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/hc;

    .line 2680
    new-instance v1, Lcom/android/calendar/e/ai;

    iget-object v4, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-direct {v1, v4, v0, v2}, Lcom/android/calendar/e/ai;-><init>(Landroid/content/Context;Lcom/android/calendar/hc;Z)V

    iput-object v1, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    .line 2681
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2682
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cN:Ljava/lang/Runnable;

    const-wide/16 v8, 0x190

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 2683
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2684
    new-instance v4, Lcom/android/calendar/month/bb;

    iget-object v8, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/e/ao;->bK:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    iget v9, p0, Lcom/android/calendar/e/ao;->bK:I

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-direct {v4, v8, v0, v1, v5}, Lcom/android/calendar/month/bb;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    iput-object v4, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    .line 2686
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2687
    const-string v1, "finger_hover"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2688
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 2689
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2690
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cO:Ljava/lang/Runnable;

    const-wide/16 v8, 0xc8

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 2701
    :cond_b
    rem-int/lit8 v0, v2, 0x2

    if-nez v0, :cond_c

    const-wide/high16 v0, 0x3fd0000000000000L    # 0.25

    goto/16 :goto_3

    :cond_c
    const-wide/high16 v0, 0x3fe8000000000000L    # 0.75

    goto/16 :goto_3

    :cond_d
    move-wide v0, v6

    .line 2705
    goto/16 :goto_4

    .line 2712
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    instance-of v0, v0, Lcom/android/calendar/month/bb;

    if-eqz v0, :cond_7

    .line 2713
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ne v0, v12, :cond_15

    move v8, v3

    .line 2716
    :goto_6
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bu:Z

    if-eqz v0, :cond_f

    .line 2717
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    check-cast v0, Lcom/android/calendar/month/bb;

    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v1

    move v2, v8

    move v3, v10

    move v4, v10

    move v5, v12

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;IIIZ)V

    goto/16 :goto_5

    .line 2719
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2720
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    check-cast v0, Lcom/android/calendar/month/bb;

    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v1

    move v2, v8

    move v3, v10

    move v4, v10

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;IIIZ)V

    goto/16 :goto_5

    .line 2722
    :cond_10
    iget-object v6, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    check-cast v6, Lcom/android/calendar/month/bb;

    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v7

    move v9, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;IIIZ)V

    goto/16 :goto_5

    .line 2732
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    if-eqz v0, :cond_12

    .line 2733
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bO:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2734
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 2737
    :goto_7
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bu:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 2738
    :cond_11
    invoke-direct {p0, v4}, Lcom/android/calendar/e/ao;->j(I)V

    .line 2744
    :goto_8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    .line 2746
    :cond_12
    iput-boolean v5, p0, Lcom/android/calendar/e/ao;->bu:Z

    .line 2747
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    move v5, v12

    .line 2748
    goto/16 :goto_0

    .line 2736
    :cond_13
    invoke-static {v12}, Lcom/android/calendar/ek;->a(I)V

    goto :goto_7

    .line 2740
    :cond_14
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2741
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2742
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bN:Ljava/lang/Object;

    goto :goto_8

    :cond_15
    move v8, v1

    goto/16 :goto_6

    .line 2658
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private i(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 2196
    .line 2198
    iget-object v1, p0, Lcom/android/calendar/e/ao;->u:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/e/ao;->t:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v1, v2

    if-le p1, v1, :cond_0

    .line 2199
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getEventStartYPos()I

    move-result v1

    sub-int v1, p1, v1

    .line 2200
    if-ltz v1, :cond_0

    .line 2201
    iget v0, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v2, p0, Lcom/android/calendar/e/ao;->W:I

    add-int/2addr v0, v2

    div-int v0, v1, v0

    .line 2206
    :cond_0
    iget v1, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    .line 2207
    iget v0, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v0, v0, -0x1

    .line 2209
    :cond_1
    return v0
.end method

.method static synthetic i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/calendar/e/ao;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/e/ao;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    return-object v0
.end method

.method private i(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 1175
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bi:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1176
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ne v0, v7, :cond_1

    .line 1177
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->r()V

    .line 1180
    :cond_1
    const/4 v6, -0x1

    .line 1181
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getEventStartYPos()I

    move-result v4

    .line 1182
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1183
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1184
    add-int/lit8 v6, v6, 0x1

    .line 1185
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_2

    .line 1188
    invoke-direct {p0, v6}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int v3, v1, v2

    .line 1189
    add-int/lit8 v1, v6, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int v5, v1, v2

    .line 1191
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 1192
    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1193
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 1194
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    if-eq v2, v0, :cond_3

    move-object v0, p0

    move-object v1, p1

    .line 1195
    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/e/ao;->a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;IIIIZ)Lcom/android/calendar/e/ay;

    move-result-object v0

    .line 1196
    sget-object v1, Lcom/android/calendar/e/ay;->c:Lcom/android/calendar/e/ay;

    if-ne v0, v1, :cond_3

    goto :goto_0

    .line 1203
    :cond_4
    return-void
.end method

.method private i(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 3477
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->C()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3494
    :goto_0
    return-void

    .line 3482
    :cond_0
    if-eqz p1, :cond_1

    .line 3483
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 3484
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 3485
    iput v1, p0, Lcom/android/calendar/e/ao;->cD:I

    .line 3486
    iput v0, p0, Lcom/android/calendar/e/ao;->cE:I

    .line 3491
    :goto_1
    iget v2, p0, Lcom/android/calendar/e/ao;->cw:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/e/ao;->cF:I

    .line 3492
    iget v1, p0, Lcom/android/calendar/e/ao;->cx:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/ao;->cG:I

    .line 3493
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    goto :goto_0

    .line 3488
    :cond_1
    iget v1, p0, Lcom/android/calendar/e/ao;->cD:I

    .line 3489
    iget v0, p0, Lcom/android/calendar/e/ao;->cE:I

    goto :goto_1
.end method

.method static synthetic j(Lcom/android/calendar/e/ao;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 400
    new-instance v0, Lcom/android/calendar/e/ba;

    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/e/ba;-><init>(Lcom/android/calendar/e/ao;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cJ:Lcom/android/calendar/e/ba;

    .line 401
    return-void
.end method

.method private j(I)V
    .locals 4

    .prologue
    .line 2756
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2757
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2758
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cP:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2759
    return-void
.end method

.method private j(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 1206
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getEventStartYPos()I

    move-result v4

    .line 1209
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bx:Z

    if-nez v0, :cond_0

    .line 1210
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->D:F

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v1, v1, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v1, v1, Lcom/android/calendar/dh;->C:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->h(I)I

    move-result v6

    .line 1211
    add-int/lit8 v0, v6, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int v5, v0, v1

    .line 1212
    iget-object v2, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v3, v0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/e/ao;->a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;IIIIZ)Lcom/android/calendar/e/ay;

    .line 1214
    :cond_0
    return-void
.end method

.method private j(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 3511
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lcom/android/calendar/e/ao;->cq:I

    iget v2, p0, Lcom/android/calendar/e/ao;->C:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 3512
    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->h(I)I

    move-result v1

    .line 3513
    iget v0, p0, Lcom/android/calendar/e/ao;->bF:I

    if-ge v1, v0, :cond_0

    move v0, v1

    .line 3514
    :goto_0
    iget v2, p0, Lcom/android/calendar/e/ao;->bF:I

    if-le v1, v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    .line 3516
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bn:[Z

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([ZZ)V

    .line 3517
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bn:[Z

    const/4 v3, 0x1

    invoke-static {v2, v0, v1, v3}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 3518
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3519
    return-void

    .line 3513
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/ao;->bF:I

    goto :goto_0

    .line 3514
    :cond_1
    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private k(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3205
    rem-int/lit8 v0, p1, 0xa

    .line 3206
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 3208
    packed-switch v0, :pswitch_data_0

    .line 3222
    const v0, 0x7f0f001d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3226
    :goto_0
    return-object v0

    .line 3210
    :pswitch_0
    const v0, 0x7f0f001b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3214
    :pswitch_1
    const v0, 0x7f0f000f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3218
    :pswitch_2
    const v0, 0x7f0f0013

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3208
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private k()V
    .locals 5

    .prologue
    const v4, 0x7f0c03e5

    const/4 v3, 0x1

    .line 465
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 466
    iget-object v1, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/e/ao;->au:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 468
    iget-object v1, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 469
    iget-object v1, p0, Lcom/android/calendar/e/ao;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 471
    iget-object v1, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 472
    iget-object v1, p0, Lcom/android/calendar/e/ao;->h:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 473
    iget-object v1, p0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 474
    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ne v1, v3, :cond_0

    .line 475
    iget-object v1, p0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    const v2, 0x7f0c03e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 476
    iget-object v1, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    const v2, 0x7f0c0067

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 482
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 484
    iget-object v1, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 485
    iget-object v1, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 486
    iget-object v1, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 488
    iget-object v1, p0, Lcom/android/calendar/e/ao;->n:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 489
    iget-object v1, p0, Lcom/android/calendar/e/ao;->n:Landroid/graphics/Paint;

    const v2, 0x7f0c0391

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 490
    iget-object v1, p0, Lcom/android/calendar/e/ao;->n:Landroid/graphics/Paint;

    const v2, 0x7f0b011c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 491
    iget-object v0, p0, Lcom/android/calendar/e/ao;->n:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 493
    iget-object v0, p0, Lcom/android/calendar/e/ao;->o:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 494
    iget-object v0, p0, Lcom/android/calendar/e/ao;->o:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->aF:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 495
    iget-object v0, p0, Lcom/android/calendar/e/ao;->o:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 497
    iget-object v0, p0, Lcom/android/calendar/e/ao;->p:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 498
    iget-object v0, p0, Lcom/android/calendar/e/ao;->p:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->aG:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 499
    iget-object v0, p0, Lcom/android/calendar/e/ao;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 501
    iget-object v0, p0, Lcom/android/calendar/e/ao;->q:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->aF:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 502
    iget-object v0, p0, Lcom/android/calendar/e/ao;->q:Landroid/graphics/Paint;

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 503
    iget-object v0, p0, Lcom/android/calendar/e/ao;->q:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 504
    iget-object v0, p0, Lcom/android/calendar/e/ao;->q:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 506
    iget-object v0, p0, Lcom/android/calendar/e/ao;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 507
    iget-object v0, p0, Lcom/android/calendar/e/ao;->r:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->aH:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 508
    iget-object v0, p0, Lcom/android/calendar/e/ao;->r:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 510
    iget-object v0, p0, Lcom/android/calendar/e/ao;->s:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->ao:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 511
    iget-object v0, p0, Lcom/android/calendar/e/ao;->s:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 512
    iget-object v0, p0, Lcom/android/calendar/e/ao;->s:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/e/ao;->aL:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 513
    iget-object v0, p0, Lcom/android/calendar/e/ao;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 515
    iget-object v0, p0, Lcom/android/calendar/e/ao;->i:Landroid/graphics/Paint;

    const/16 v1, 0xa5

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 516
    return-void

    .line 478
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 479
    iget-object v1, p0, Lcom/android/calendar/e/ao;->l:Landroid/text/TextPaint;

    const/4 v2, 0x0

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 480
    iget-object v1, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    goto/16 :goto_0
.end method

.method private k(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bj:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1535
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1559
    :cond_0
    return-void

    .line 1539
    :cond_1
    const/4 v6, -0x1

    .line 1540
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1541
    add-int/lit8 v6, v6, 0x1

    .line 1542
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_2

    .line 1546
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getEventStartYPos()I

    move-result v4

    .line 1547
    invoke-direct {p0, v6}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int v3, v1, v2

    .line 1548
    add-int/lit8 v1, v6, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int v5, v1, v2

    .line 1549
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1551
    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1552
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/hc;

    move-object v0, p0

    move-object v1, p1

    .line 1553
    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/e/ao;->a(Landroid/graphics/Canvas;Lcom/android/calendar/hc;IIII)Lcom/android/calendar/e/ay;

    move-result-object v0

    .line 1554
    sget-object v1, Lcom/android/calendar/e/ay;->c:Lcom/android/calendar/e/ay;

    if-ne v0, v1, :cond_3

    goto :goto_0
.end method

.method private l()V
    .locals 9

    .prologue
    const v5, 0x7f0d0001

    const v4, 0x7f0c00b6

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 519
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 520
    const v1, 0x7f0c0390

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->D:F

    .line 521
    const v1, 0x7f0c038d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->E:F

    .line 522
    const v1, 0x7f0c0389

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->F:F

    .line 523
    const v1, 0x7f0c0342

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ao:I

    .line 524
    const v1, 0x7f0c0344

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    sput v1, Lcom/android/calendar/e/ao;->e:F

    .line 525
    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ne v1, v2, :cond_2

    .line 526
    iput v3, p0, Lcom/android/calendar/e/ao;->C:I

    .line 527
    iput v8, p0, Lcom/android/calendar/e/ao;->G:F

    .line 528
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->H:F

    .line 529
    const v1, 0x7f0c03e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->Q:I

    .line 530
    const v1, 0x7f0c0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->R:I

    .line 531
    const v1, 0x7f0c0064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->T:I

    .line 532
    const v1, 0x7f0c0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->U:I

    .line 533
    const v1, 0x7f0c005b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->S:I

    .line 534
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/android/calendar/e/ao;->ce:I

    .line 535
    const v1, 0x7f0c00c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->W:I

    .line 536
    const v1, 0x7f0c00c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ac:I

    .line 537
    const v1, 0x7f0c00c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ad:I

    .line 538
    const v1, 0x7f0c0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ai:F

    .line 539
    const v1, 0x7f0c005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aj:F

    .line 540
    const v1, 0x7f0c00c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ak:I

    .line 541
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->I:F

    .line 542
    const v1, 0x7f0c00ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->L:F

    .line 543
    const v1, 0x7f0c00b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->M:F

    .line 544
    const v1, 0x7f0c00b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->N:I

    .line 545
    const v1, 0x7f0c00b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->O:I

    .line 546
    const v1, 0x7f0c0061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ae:I

    .line 547
    const v1, 0x7f0c00c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ab:I

    .line 548
    iget v1, p0, Lcom/android/calendar/e/ao;->ce:I

    if-ne v1, v2, :cond_1

    .line 549
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->am:I

    .line 571
    :goto_0
    const v1, 0x7f0c00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aa:I

    .line 573
    const v1, 0x7f0c0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->af:I

    .line 574
    const v1, 0x7f0c0392

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->J:F

    .line 575
    const v1, 0x7f0c0422

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->K:F

    .line 576
    const v1, 0x7f0c005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->V:I

    .line 577
    iget v1, p0, Lcom/android/calendar/e/ao;->Q:I

    iput v1, p0, Lcom/android/calendar/e/ao;->cm:I

    .line 578
    iget v1, p0, Lcom/android/calendar/e/ao;->cm:I

    int-to-double v2, v1

    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    int-to-double v4, v1

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, p0, Lcom/android/calendar/e/ao;->cn:I

    .line 579
    const v1, 0x7f0c006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ag:I

    .line 580
    const v1, 0x7f0c006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ah:I

    .line 581
    sget v1, Lcom/android/calendar/e/ao;->ck:F

    cmpl-float v1, v1, v8

    if-nez v1, :cond_0

    .line 582
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/android/calendar/e/ao;->ck:F

    .line 583
    sget v0, Lcom/android/calendar/e/ao;->ck:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 584
    sget v0, Lcom/android/calendar/e/ao;->cj:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/e/ao;->ck:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/e/ao;->cj:I

    .line 588
    :cond_0
    return-void

    .line 551
    :cond_1
    const/4 v1, 0x6

    iput v1, p0, Lcom/android/calendar/e/ao;->am:I

    .line 552
    iput-boolean v2, p0, Lcom/android/calendar/e/ao;->bz:Z

    goto :goto_0

    .line 555
    :cond_2
    const v1, 0x7f0c0398

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->C:I

    .line 556
    const v1, 0x7f0c038a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->G:F

    .line 557
    const v1, 0x7f0c038b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->H:F

    .line 558
    const v1, 0x7f0c038f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->P:I

    .line 559
    const v1, 0x7f0c03e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->Q:I

    .line 560
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->am:I

    .line 561
    const v1, 0x7f0c038e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->W:I

    .line 562
    const v1, 0x7f0c039b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ac:I

    .line 563
    const v1, 0x7f0c039a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ad:I

    .line 564
    const v1, 0x7f0c0069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ai:F

    .line 565
    const v1, 0x7f0c0068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aj:F

    .line 566
    iput v3, p0, Lcom/android/calendar/e/ao;->ak:I

    .line 567
    iput v8, p0, Lcom/android/calendar/e/ao;->I:F

    .line 568
    const v1, 0x7f0c03e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ae:I

    .line 569
    const v1, 0x7f0c038c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ab:I

    goto/16 :goto_0
.end method

.method private l(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1689
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->s()V

    .line 1693
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v0, :cond_1

    .line 1694
    iget v0, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v2

    .line 1695
    sget v0, Lcom/android/calendar/e/ao;->at:I

    .line 1699
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iput v2, v3, Landroid/graphics/Rect;->top:I

    .line 1700
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/android/calendar/e/ao;->Q:I

    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    move v3, v1

    .line 1701
    :goto_1
    iget v4, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v3, v4, :cond_3

    .line 1702
    iget-object v4, p0, Lcom/android/calendar/e/ao;->bm:[I

    aget v4, v4, v3

    if-lez v4, :cond_0

    .line 1703
    iget-boolean v4, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v4, :cond_2

    .line 1704
    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-direct {p0, v3}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v5

    add-int/2addr v5, v0

    iget v6, p0, Lcom/android/calendar/e/ao;->aa:I

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 1710
    :goto_2
    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/lit8 v5, v3, 0x1

    invoke-direct {p0, v5}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v5

    add-int/2addr v5, v0

    iget v6, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/graphics/Rect;->right:I

    .line 1711
    iget-object v4, p0, Lcom/android/calendar/e/ao;->aO:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1712
    iget-object v4, p0, Lcom/android/calendar/e/ao;->aO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1714
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "+ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/calendar/e/ao;->bm:[I

    aget v7, v7, v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1715
    invoke-direct {p0, v3}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v5

    add-int/2addr v5, v0

    sget v6, Lcom/android/calendar/e/ao;->at:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    .line 1716
    iget-object v6, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->descent()F

    move-result v6

    iget-object v7, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    invoke-virtual {v7}, Landroid/graphics/Paint;->ascent()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    float-to-int v6, v6

    .line 1717
    iget v7, p0, Lcom/android/calendar/e/ao;->Q:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v2

    .line 1718
    int-to-float v5, v5

    int-to-float v6, v6

    iget-object v7, p0, Lcom/android/calendar/e/ao;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1719
    iget v4, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v4, v4, -0x1

    iget-object v5, p0, Lcom/android/calendar/e/ao;->aO:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v3, v4, v5}, Lcom/android/calendar/e/ao;->a(IILjava/lang/Object;)V

    .line 1701
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 1697
    :cond_1
    iget v0, p0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v0

    move v2, v0

    move v0, v1

    goto/16 :goto_0

    .line 1707
    :cond_2
    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-direct {p0, v3}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v5

    add-int/2addr v5, v0

    iget v6, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Landroid/graphics/Rect;->left:I

    goto/16 :goto_2

    .line 1722
    :cond_3
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 592
    iget-boolean v1, p0, Lcom/android/calendar/e/ao;->bp:Z

    if-eqz v1, :cond_0

    .line 593
    const v1, 0x7f020194

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aM:Landroid/graphics/drawable/Drawable;

    .line 594
    const v1, 0x7f020193

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aN:Landroid/graphics/drawable/Drawable;

    .line 604
    :goto_0
    const v1, 0x7f020089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aO:Landroid/graphics/drawable/Drawable;

    .line 605
    const v1, 0x7f020134

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aP:Landroid/graphics/drawable/Drawable;

    .line 606
    const v1, 0x7f020141

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aQ:Landroid/graphics/drawable/Drawable;

    .line 608
    iget-object v1, p0, Lcom/android/calendar/e/ao;->aQ:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aR:Landroid/graphics/drawable/Drawable;

    .line 609
    const v1, 0x7f020065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aS:Landroid/graphics/drawable/Drawable;

    .line 610
    const v1, 0x7f0201f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aT:Landroid/graphics/drawable/Drawable;

    .line 611
    const v1, 0x7f02008f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->aU:Landroid/graphics/drawable/Drawable;

    .line 612
    return-void

    .line 596
    :cond_0
    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 597
    const v1, 0x7f020076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aM:Landroid/graphics/drawable/Drawable;

    .line 598
    const v1, 0x7f020075

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aN:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 600
    :cond_1
    const v1, 0x7f0200c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aM:Landroid/graphics/drawable/Drawable;

    .line 601
    const v1, 0x7f0200c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aN:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private m(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1740
    iget-boolean v2, p0, Lcom/android/calendar/e/ao;->bu:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    if-nez v2, :cond_1

    .line 1765
    :cond_0
    :goto_0
    return-void

    .line 1743
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    instance-of v2, v2, Lcom/android/calendar/dh;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bM:Ljava/lang/Object;

    instance-of v2, v2, Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 1744
    iget v3, p0, Lcom/android/calendar/e/ao;->bL:I

    .line 1745
    iget v2, p0, Lcom/android/calendar/e/ao;->bK:I

    .line 1747
    iget-boolean v4, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v4, :cond_4

    .line 1748
    rem-int/lit8 v2, v3, 0x2

    if-nez v2, :cond_3

    .line 1750
    iget v2, p0, Lcom/android/calendar/e/ao;->am:I

    if-ge v3, v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v2, v2, v1

    add-int/lit8 v4, v3, 0x1

    aget-boolean v2, v2, v4

    if-nez v2, :cond_2

    .line 1751
    const/4 v0, 0x2

    .line 1756
    :cond_2
    :goto_1
    div-int/lit8 v2, v3, 0x2

    .line 1758
    :goto_2
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v4

    iget v5, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 1759
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v0, v1

    iput v0, v3, Landroid/graphics/Rect;->right:I

    .line 1760
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1761
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/android/calendar/e/ao;->Q:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1762
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aR:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1763
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aR:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_3
    move v1, v0

    .line 1754
    goto :goto_1

    :cond_4
    move v1, v2

    move v2, v3

    goto :goto_2
.end method

.method private n()V
    .locals 2

    .prologue
    .line 615
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 616
    const v1, 0x7f0b0122

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->av:I

    .line 617
    const v1, 0x7f0b0121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aw:I

    .line 618
    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ax:I

    .line 619
    const v1, 0x7f0b00e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->ay:I

    .line 620
    const v1, 0x7f0b00cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->az:I

    .line 621
    const v1, 0x7f0b00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aA:I

    .line 622
    const v1, 0x7f0b011d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aD:I

    .line 623
    const v1, 0x7f0b00c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aE:I

    .line 624
    const v1, 0x7f0b002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/calendar/e/ao;->cH:I

    .line 625
    const v1, 0x7f0b011e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aF:I

    .line 626
    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aG:I

    .line 627
    const/high16 v1, 0x7f0b0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aH:I

    .line 628
    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aI:I

    .line 629
    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aJ:I

    .line 630
    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/e/ao;->aK:I

    .line 631
    const v1, 0x7f0b0072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/calendar/e/ao;->aL:I

    .line 632
    iget-boolean v1, p0, Lcom/android/calendar/e/ao;->bp:Z

    if-eqz v1, :cond_0

    .line 633
    const v1, 0x7f0b00a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ao;->au:I

    .line 637
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->w()V

    .line 638
    return-void

    .line 635
    :cond_0
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/android/calendar/e/ao;->au:I

    goto :goto_0
.end method

.method private n(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1768
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v1, Lcom/android/calendar/e/bd;->d:Lcom/android/calendar/e/bd;

    if-eq v0, v1, :cond_1

    .line 1779
    :cond_0
    :goto_0
    return-void

    .line 1771
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/calendar/dh;

    if-nez v0, :cond_0

    .line 1772
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1773
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1774
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/e/ao;->bH:I

    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1775
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/android/calendar/e/ao;->Q:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1776
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aP:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1777
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aP:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private o()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 643
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bo:[Ljava/lang/String;

    .line 644
    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x7

    if-gt v0, v1, :cond_2

    .line 645
    add-int/lit8 v1, v0, -0x1

    .line 647
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 648
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bo:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 656
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bo:[Ljava/lang/String;

    add-int/lit8 v3, v1, 0x7

    iget-object v4, p0, Lcom/android/calendar/e/ao;->bo:[Ljava/lang/String;

    aget-object v1, v4, v1

    aput-object v1, v2, v3

    .line 644
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 650
    :cond_0
    iget-boolean v2, p0, Lcom/android/calendar/e/ao;->bp:Z

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/android/calendar/hj;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 651
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bo:[Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    goto :goto_1

    .line 653
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bo:[Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    goto :goto_1

    .line 658
    :cond_2
    return-void
.end method

.method private o(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 1782
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v2, Lcom/android/calendar/e/bd;->c:Lcom/android/calendar/e/bd;

    if-eq v1, v2, :cond_1

    .line 1821
    :cond_0
    :goto_0
    return-void

    .line 1785
    :cond_1
    iget v1, p0, Lcom/android/calendar/e/ao;->bH:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/calendar/e/ao;->bH:I

    iget v2, p0, Lcom/android/calendar/e/ao;->an:I

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_0

    .line 1788
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-virtual {p0, v1}, Lcom/android/calendar/e/ao;->a(I)I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 1791
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    invoke-virtual {v1}, Lcom/android/calendar/e/g;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1792
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    goto :goto_0

    .line 1795
    :cond_3
    iput-boolean v4, p0, Lcom/android/calendar/e/ao;->bA:Z

    .line 1796
    new-instance v1, Lcom/android/calendar/e/aq;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/aq;-><init>(Lcom/android/calendar/e/ao;)V

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/calendar/e/ao;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1802
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1803
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v2

    iget v3, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1804
    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-virtual {p0, v1}, Lcom/android/calendar/e/ao;->a(I)I

    move-result v1

    .line 1805
    if-ne v1, v4, :cond_4

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    iget v3, p0, Lcom/android/calendar/e/ao;->bF:I

    aget-object v2, v2, v3

    aget-object v2, v2, v0

    instance-of v2, v2, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/android/calendar/e/ao;->an:I

    if-ne v2, v4, :cond_4

    .line 1808
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v0

    iget v2, p0, Lcom/android/calendar/e/ao;->W:I

    sub-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1809
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/e/ao;->as:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1810
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aT:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1811
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aT:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1813
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aS:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1814
    iget-object v1, p0, Lcom/android/calendar/e/ao;->aS:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1815
    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v1

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 1816
    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v3

    iput v1, v2, Landroid/graphics/Rect;->bottom:I

    .line 1817
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1818
    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1819
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aS:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1820
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aS:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private p()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 740
    sget v0, Lcom/android/calendar/e/ao;->at:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    mul-int/2addr v0, v1

    .line 741
    iget v1, p0, Lcom/android/calendar/e/ao;->G:F

    iget v2, p0, Lcom/android/calendar/e/ao;->D:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->E:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 742
    iget v2, p0, Lcom/android/calendar/e/ao;->H:F

    iget v3, p0, Lcom/android/calendar/e/ao;->D:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/ao;->E:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 743
    iget-object v3, p0, Lcom/android/calendar/e/ao;->t:Landroid/graphics/Rect;

    invoke-virtual {v3, v4, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 744
    iget-object v3, p0, Lcom/android/calendar/e/ao;->u:Landroid/graphics/Rect;

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 745
    return-void
.end method

.method private p(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1837
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bp:Z

    if-eqz v0, :cond_0

    .line 1838
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->r(Landroid/graphics/Canvas;)V

    .line 1842
    :goto_0
    return-void

    .line 1840
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->q(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private q(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1845
    iget v0, p0, Lcom/android/calendar/e/ao;->C:I

    .line 1846
    iget v2, p0, Lcom/android/calendar/e/ao;->G:F

    iget v3, p0, Lcom/android/calendar/e/ao;->D:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/ao;->E:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 1847
    int-to-float v3, v0

    int-to-float v4, v2

    iget-object v5, p0, Lcom/android/calendar/e/ao;->r:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1848
    return-void
.end method

.method private q()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1065
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1066
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1067
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1079
    :goto_0
    return v1

    .line 1072
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 1073
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1074
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 1079
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private r()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1489
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1490
    invoke-static {v4, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 1489
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1492
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 1493
    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1492
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1495
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bm:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 1496
    return-void
.end method

.method private r(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1851
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bp:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bq:Z

    if-nez v0, :cond_1

    .line 1856
    :cond_0
    :goto_0
    return-void

    .line 1854
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->s(Landroid/graphics/Canvas;)V

    .line 1855
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->t(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private s()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1725
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v0, v2, :cond_3

    .line 1727
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v4, v2, v0

    array-length v5, v4

    move v3, v1

    move v2, v1

    :goto_1
    if-ge v3, v5, :cond_1

    aget-boolean v6, v4, v3

    .line 1728
    if-eqz v6, :cond_0

    .line 1729
    add-int/lit8 v2, v2, 0x1

    .line 1727
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1732
    :cond_1
    iget-boolean v3, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v3, v7, :cond_2

    .line 1733
    add-int/lit8 v2, v2, -0x1

    .line 1735
    :cond_2
    iget-object v3, p0, Lcom/android/calendar/e/ao;->bm:[I

    invoke-direct {p0, v0, v7}, Lcom/android/calendar/e/ao;->b(IZ)I

    move-result v4

    sub-int v2, v4, v2

    aput v2, v3, v0

    .line 1725
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1737
    :cond_3
    return-void
.end method

.method private s(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1859
    .line 1860
    iget-object v0, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->D:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1861
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    aput v6, v0, v5

    .line 1862
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    iget v1, p0, Lcom/android/calendar/e/ao;->D:F

    div-float/2addr v1, v7

    aput v1, v0, v8

    .line 1863
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v1, 0x3

    aput v6, v0, v9

    .line 1864
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v2, 0x4

    iget v3, p0, Lcom/android/calendar/e/ao;->D:F

    div-float/2addr v3, v7

    aput v3, v0, v1

    .line 1865
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    iget-object v1, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v2, v1}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 1868
    iget-object v0, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->E:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1869
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    aput v6, v0, v5

    .line 1870
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    iget v1, p0, Lcom/android/calendar/e/ao;->D:F

    iget v2, p0, Lcom/android/calendar/e/ao;->G:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->E:F

    div-float/2addr v2, v7

    add-float/2addr v1, v2

    aput v1, v0, v8

    .line 1871
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v1, 0x3

    aput v6, v0, v9

    .line 1872
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v2, 0x4

    iget v3, p0, Lcom/android/calendar/e/ao;->D:F

    iget v4, p0, Lcom/android/calendar/e/ao;->G:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/e/ao;->E:F

    div-float/2addr v4, v7

    add-float/2addr v3, v4

    aput v3, v0, v1

    .line 1873
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    iget-object v1, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v2, v1}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 1876
    iget-object v0, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/e/ao;->E:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1877
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    aput v6, v0, v5

    .line 1878
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    iget v1, p0, Lcom/android/calendar/e/ao;->as:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/calendar/e/ao;->E:F

    div-float/2addr v2, v7

    sub-float/2addr v1, v2

    aput v1, v0, v8

    .line 1879
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v1, 0x3

    aput v6, v0, v9

    .line 1880
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v2, 0x4

    iget v3, p0, Lcom/android/calendar/e/ao;->as:I

    int-to-float v3, v3

    iget v4, p0, Lcom/android/calendar/e/ao;->E:F

    div-float/2addr v4, v7

    sub-float/2addr v3, v4

    aput v3, v0, v1

    .line 1881
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    iget-object v1, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v2, v1}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 1884
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/high16 v1, -0x40800000    # -1.0f

    aput v1, v0, v5

    .line 1885
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    iget v1, p0, Lcom/android/calendar/e/ao;->D:F

    aput v1, v0, v8

    .line 1886
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v1, 0x3

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v0, v9

    .line 1887
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    const/4 v2, 0x4

    iget v3, p0, Lcom/android/calendar/e/ao;->as:I

    int-to-float v3, v3

    aput v3, v0, v1

    .line 1888
    iget-object v0, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1889
    iget-object v0, p0, Lcom/android/calendar/e/ao;->z:[F

    iget-object v1, p0, Lcom/android/calendar/e/ao;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v2, v1}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 1890
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 1938
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bU:Landroid/view/accessibility/AccessibilityManager;

    .line 1939
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bU:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bU:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bt:Z

    .line 1940
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->u()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bs:Z

    .line 1941
    return-void

    .line 1939
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 1893
    iget-object v1, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {}, Lcom/android/calendar/dz;->k()Z

    move-result v4

    invoke-static {v2, v3, v1, v4}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;Z)I

    move-result v1

    .line 1895
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/e/ao;->ca:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1896
    iget-object v2, p0, Lcom/android/calendar/e/ao;->n:Landroid/graphics/Paint;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1897
    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    .line 1898
    iget v3, p0, Lcom/android/calendar/e/ao;->D:F

    iget v4, p0, Lcom/android/calendar/e/ao;->G:F

    sub-float/2addr v4, v2

    div-float/2addr v4, v9

    add-float/2addr v3, v4

    add-float/2addr v3, v2

    float-to-int v3, v3

    .line 1899
    iget v4, p0, Lcom/android/calendar/e/ao;->C:I

    int-to-float v4, v4

    div-float/2addr v4, v9

    int-to-float v3, v3

    iget-object v5, p0, Lcom/android/calendar/e/ao;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4, v3, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1900
    iget-object v1, p0, Lcom/android/calendar/e/ao;->cb:Ljava/lang/String;

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1901
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getEventStartYPos()I

    move-result v1

    .line 1903
    array-length v4, v3

    int-to-float v4, v4

    mul-float/2addr v4, v2

    iget v5, p0, Lcom/android/calendar/e/ao;->W:I

    array-length v6, v3

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v5, v6

    int-to-float v5, v5

    add-float/2addr v4, v5

    .line 1905
    iget v5, p0, Lcom/android/calendar/e/ao;->as:I

    sub-int/2addr v5, v1

    int-to-float v5, v5

    .line 1906
    sub-float v4, v5, v4

    div-float/2addr v4, v9

    float-to-int v4, v4

    add-int/2addr v1, v4

    .line 1907
    array-length v4, v3

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 1908
    int-to-float v1, v1

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 1909
    iget v6, p0, Lcom/android/calendar/e/ao;->C:I

    int-to-float v6, v6

    div-float/2addr v6, v9

    int-to-float v7, v1

    iget-object v8, p0, Lcom/android/calendar/e/ao;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1910
    iget v5, p0, Lcom/android/calendar/e/ao;->W:I

    add-int/2addr v1, v5

    .line 1907
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1912
    :cond_0
    return-void
.end method

.method private u()Z
    .locals 1

    .prologue
    .line 1944
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bt:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bU:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1948
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->aZ:Landroid/text/format/Time;

    iget v3, p0, Lcom/android/calendar/e/ao;->bb:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;IZ)V

    .line 1949
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 1950
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/e/ao;->cK:Z

    if-eqz v2, :cond_0

    .line 1951
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 1953
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ao;->aV:I

    .line 1954
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ao;->aW:I

    .line 1955
    return-void
.end method

.method private w()V
    .locals 7

    .prologue
    const/4 v0, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1958
    new-array v2, v0, [I

    .line 1959
    new-array v3, v0, [I

    .line 1961
    const-string v0, "XXXXXXR"

    .line 1963
    iget v1, p0, Lcom/android/calendar/e/ao;->av:I

    aput v1, v2, v4

    .line 1964
    iget v1, p0, Lcom/android/calendar/e/ao;->az:I

    aput v1, v2, v5

    .line 1965
    iget v1, p0, Lcom/android/calendar/e/ao;->ax:I

    aput v1, v2, v6

    .line 1967
    iget v1, p0, Lcom/android/calendar/e/ao;->aw:I

    aput v1, v3, v4

    .line 1968
    iget v1, p0, Lcom/android/calendar/e/ao;->aA:I

    aput v1, v3, v5

    .line 1969
    iget v1, p0, Lcom/android/calendar/e/ao;->ay:I

    aput v1, v3, v6

    .line 1971
    invoke-static {}, Lcom/android/calendar/dz;->e()Ljava/lang/String;

    move-result-object v1

    .line 1972
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1975
    :goto_0
    iget v1, p0, Lcom/android/calendar/e/ao;->bb:I

    invoke-static {v0, v2, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/e/ao;->aB:[I

    .line 1976
    iget v1, p0, Lcom/android/calendar/e/ao;->bb:I

    invoke-static {v0, v3, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->aC:[I

    .line 1977
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private x()V
    .locals 4

    .prologue
    .line 2811
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/e/as;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/as;-><init>(Lcom/android/calendar/e/ao;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2822
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bX:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/e/at;

    invoke-direct {v1, p0}, Lcom/android/calendar/e/at;-><init>(Lcom/android/calendar/e/ao;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2828
    return-void
.end method

.method private y()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3105
    .line 3106
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cI:Lcom/android/calendar/hc;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 3107
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3109
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    move v1, v3

    .line 3111
    goto :goto_0

    .line 3114
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/hc;

    .line 3115
    iget-wide v0, v0, Lcom/android/calendar/hc;->a:J

    iget-object v6, p0, Lcom/android/calendar/e/ao;->cI:Lcom/android/calendar/hc;

    iget-wide v6, v6, Lcom/android/calendar/hc;->a:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    move v0, v2

    :goto_2
    move v1, v0

    .line 3122
    goto :goto_0

    :cond_2
    move v1, v3

    .line 3121
    goto :goto_1

    :cond_3
    move v2, v1

    .line 3124
    :cond_4
    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/android/calendar/e/ao;->cI:Lcom/android/calendar/hc;

    if-nez v0, :cond_6

    :cond_5
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bC:Z

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_7

    .line 3126
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 3128
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 3129
    if-eqz v0, :cond_7

    instance-of v1, v0, Lcom/android/calendar/month/a;

    if-eqz v1, :cond_7

    .line 3131
    check-cast v0, Lcom/android/calendar/month/a;

    .line 3132
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/android/calendar/month/a;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/android/calendar/month/a;->isRemoving()Z

    move-result v1

    if-nez v1, :cond_7

    .line 3134
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/android/calendar/e/aw;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/e/aw;-><init>(Lcom/android/calendar/e/ao;Lcom/android/calendar/month/a;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3151
    :cond_7
    return-void

    :cond_8
    move v0, v1

    goto :goto_2
.end method

.method private z()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 3154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3155
    iget-object v1, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {}, Lcom/android/calendar/dz;->k()Z

    move-result v4

    invoke-static {v2, v3, v1, v4}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;Z)I

    move-result v1

    .line 3156
    invoke-direct {p0, v1}, Lcom/android/calendar/e/ao;->k(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3157
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3158
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 3159
    const v2, 0x7f0f0045

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3160
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getNumberOfEvents()I

    move-result v2

    .line 3161
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getNumbetOfTasks()I

    move-result v3

    .line 3162
    if-nez v2, :cond_1

    if-nez v3, :cond_1

    .line 3163
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3164
    const v2, 0x7f0f02d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3177
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cc:Ljava/lang/String;

    .line 3178
    return-void

    .line 3166
    :cond_1
    if-lez v2, :cond_2

    .line 3167
    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3168
    const v4, 0x7f0e0001

    invoke-virtual {v1, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 3169
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3171
    :cond_2
    if-lez v3, :cond_0

    .line 3172
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3173
    const v2, 0x7f0e0003

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3174
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3276
    .line 3277
    iget-boolean v2, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v2, :cond_1

    .line 3278
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v2, v2, p1

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v2, v0, :cond_1

    .line 3283
    :goto_0
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/e/ao;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3284
    iget v1, p0, Lcom/android/calendar/e/ao;->am:I

    if-le v0, v1, :cond_0

    .line 3285
    iget v0, p0, Lcom/android/calendar/e/ao;->am:I

    .line 3287
    :cond_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;Lcom/android/calendar/hc;IIII)Lcom/android/calendar/e/ay;
    .locals 9

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bj:Ljava/util/Set;

    iget-wide v2, p2, Lcom/android/calendar/hc;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1563
    sget-object v0, Lcom/android/calendar/e/ay;->b:Lcom/android/calendar/e/ay;

    .line 1658
    :goto_0
    return-object v0

    .line 1565
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bj:Ljava/util/Set;

    iget-wide v2, p2, Lcom/android/calendar/hc;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1567
    iget v0, p0, Lcom/android/calendar/e/ao;->W:I

    add-int v1, p4, v0

    .line 1568
    const/4 v0, 0x0

    .line 1571
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v2, v2, p6

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v2, v2, p6

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_3

    .line 1572
    iget-boolean v2, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v2, :cond_1

    rem-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_2

    .line 1573
    :cond_1
    iget v2, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v3, p0, Lcom/android/calendar/e/ao;->W:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 1575
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1578
    :cond_3
    iget-boolean v2, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aget-object v2, v2, p6

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_4

    const/4 v2, 0x1

    .line 1579
    :goto_2
    if-eqz v2, :cond_5

    const/4 v2, 0x1

    .line 1580
    :goto_3
    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcom/android/calendar/e/ao;->am:I

    if-ne v3, v4, :cond_6

    const/4 v3, 0x0

    invoke-direct {p0, p6, v3}, Lcom/android/calendar/e/ao;->b(IZ)I

    move-result v3

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/e/ao;->am:I

    if-le v2, v3, :cond_6

    .line 1582
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bj:Ljava/util/Set;

    iget-wide v2, p2, Lcom/android/calendar/hc;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1583
    sget-object v0, Lcom/android/calendar/e/ay;->c:Lcom/android/calendar/e/ay;

    goto :goto_0

    .line 1578
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 1579
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 1586
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bk:[[Z

    aget-object v2, v2, p6

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 1587
    invoke-direct {p0, p6, v0, p2}, Lcom/android/calendar/e/ao;->a(IILjava/lang/Object;)V

    .line 1591
    :cond_7
    iget v2, p2, Lcom/android/calendar/hc;->k:I

    if-gez v2, :cond_b

    .line 1592
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v2

    .line 1597
    :goto_4
    iget-object v3, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v4, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    if-eq v3, v4, :cond_c

    const/4 v3, 0x1

    move v4, v3

    .line 1598
    :goto_5
    if-eqz v4, :cond_d

    iget v3, p0, Lcom/android/calendar/e/ao;->aE:I

    .line 1600
    :goto_6
    iget-object v5, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/lit8 v6, p3, 0x1

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 1601
    iget-object v5, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/lit8 v6, p6, 0x1

    invoke-direct {p0, v6}, Lcom/android/calendar/e/ao;->c(I)I

    move-result v6

    iget v7, p0, Lcom/android/calendar/e/ao;->ab:I

    sub-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->right:I

    .line 1602
    iget-object v5, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iput v1, v5, Landroid/graphics/Rect;->top:I

    .line 1603
    iget-object v5, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/android/calendar/e/ao;->Q:I

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    .line 1605
    iget-boolean v5, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v5, :cond_8

    .line 1606
    const/4 v5, 0x1

    invoke-direct {p0, v0, v5}, Lcom/android/calendar/e/ao;->a(IZ)V

    .line 1608
    :cond_8
    iget-object v5, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    .line 1609
    iget-object v6, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1610
    iget-object v3, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1611
    if-eqz v4, :cond_e

    .line 1612
    iget-object v3, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    const/16 v4, 0xff

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1617
    :goto_7
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1619
    iget v3, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v4, p0, Lcom/android/calendar/e/ao;->ad:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    .line 1620
    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/lit8 v6, p3, 0x1

    iget v7, p0, Lcom/android/calendar/e/ao;->ak:I

    add-int/2addr v6, v7

    iput v6, v4, Landroid/graphics/Rect;->left:I

    .line 1621
    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lcom/android/calendar/e/ao;->ac:I

    add-int/2addr v6, v7

    iput v6, v4, Landroid/graphics/Rect;->right:I

    .line 1622
    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/2addr v3, v1

    iput v3, v4, Landroid/graphics/Rect;->top:I

    .line 1623
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget v6, p0, Lcom/android/calendar/e/ao;->ad:I

    add-int/2addr v4, v6

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 1624
    iget-boolean v3, p0, Lcom/android/calendar/e/ao;->bz:Z

    if-eqz v3, :cond_9

    .line 1625
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/android/calendar/e/ao;->a(IZ)V

    .line 1628
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1629
    iget-object v0, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1630
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1633
    iget-boolean v0, p2, Lcom/android/calendar/hc;->f:Z

    if-eqz v0, :cond_f

    .line 1634
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aM:Landroid/graphics/drawable/Drawable;

    .line 1638
    :goto_8
    iget v3, p0, Lcom/android/calendar/e/ao;->af:I

    .line 1639
    iget v2, p0, Lcom/android/calendar/e/ao;->af:I

    .line 1640
    iget v4, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v6, 0x1

    if-le v4, v6, :cond_a

    .line 1641
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 1642
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 1644
    :cond_a
    iget v4, p0, Lcom/android/calendar/e/ao;->Q:I

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    .line 1645
    iget-object v6, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget v8, p0, Lcom/android/calendar/e/ao;->ae:I

    add-int/2addr v7, v8

    iput v7, v6, Landroid/graphics/Rect;->left:I

    .line 1646
    iget-object v6, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v7

    iput v3, v6, Landroid/graphics/Rect;->right:I

    .line 1647
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    add-int/2addr v4, v1

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 1648
    iget-object v3, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v4

    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 1649
    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1650
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1652
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 1653
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 1654
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1655
    iget-object v0, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/android/calendar/e/ao;->Q:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1656
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/android/calendar/e/ao;->a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;Lcom/android/calendar/hc;)V

    .line 1658
    sget-object v0, Lcom/android/calendar/e/ay;->a:Lcom/android/calendar/e/ay;

    goto/16 :goto_0

    .line 1594
    :cond_b
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    iget v3, p2, Lcom/android/calendar/hc;->k:I

    invoke-static {v2, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v2

    goto/16 :goto_4

    .line 1597
    :cond_c
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_5

    :cond_d
    move v3, v2

    .line 1598
    goto/16 :goto_6

    .line 1614
    :cond_e
    iget-object v3, p0, Lcom/android/calendar/e/ao;->j:Landroid/graphics/Paint;

    const/16 v4, 0x33

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_7

    .line 1636
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aN:Landroid/graphics/drawable/Drawable;

    goto :goto_8
.end method

.method public a(Landroid/text/format/Time;ZZ)V
    .locals 2

    .prologue
    .line 3067
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v1, Lcom/android/calendar/e/bd;->c:Lcom/android/calendar/e/bd;

    if-ne v0, v1, :cond_0

    .line 3068
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 3069
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/e/ao;->bG:I

    .line 3071
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 3072
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->v()V

    .line 3073
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->br:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/e/ao;->aZ:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->after(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3074
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->d()V

    .line 3076
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cJ:Lcom/android/calendar/e/ba;

    invoke-virtual {v0}, Lcom/android/calendar/e/ba;->a()V

    .line 3077
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3078
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 2132
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 2133
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2134
    invoke-direct {p0, v0, v1}, Lcom/android/calendar/e/ao;->b(II)V

    .line 2135
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 2136
    sget-object v0, Lcom/android/calendar/e/bd;->c:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2143
    :goto_0
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v0, v2, :cond_3

    .line 2144
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2145
    iget v0, v4, Landroid/text/format/Time;->monthDay:I

    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/2addr v0, v1

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    .line 2146
    invoke-virtual {v4, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 2150
    :goto_1
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->cK:Z

    if-eqz v0, :cond_0

    .line 2151
    invoke-static {v4}, Lcom/android/calendar/e/ao;->b(Landroid/text/format/Time;)V

    .line 2153
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v7, -0x1

    const-wide/16 v10, 0x1

    move-object v1, p0

    move-object v6, v4

    move-object v12, v5

    move-object v13, v5

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2155
    sput-boolean v9, Lcom/android/calendar/e/ao;->b:Z

    .line 2156
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 2157
    return-void

    .line 2137
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/calendar/e/ao;->bF:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/ao;->a(I)I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 2138
    sget-object v0, Lcom/android/calendar/e/bd;->c:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    goto :goto_0

    .line 2140
    :cond_2
    sget-object v0, Lcom/android/calendar/e/bd;->b:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    goto :goto_0

    .line 2148
    :cond_3
    iget-object v4, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    goto :goto_1
.end method

.method a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3388
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3389
    iput v3, p0, Lcom/android/calendar/e/ao;->cs:F

    .line 3392
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/ao;->cs:F

    add-float/2addr v0, p3

    iput v0, p0, Lcom/android/calendar/e/ao;->cs:F

    .line 3393
    iget v0, p0, Lcom/android/calendar/e/ao;->cs:F

    float-to-int v0, v0

    .line 3397
    iget-object v1, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    invoke-virtual {v1}, Lcom/android/calendar/e/g;->getTouchMode()Lcom/android/calendar/e/ag;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/e/ag;->b:Lcom/android/calendar/e/ag;

    if-ne v1, v2, :cond_1

    .line 3398
    iget v1, p0, Lcom/android/calendar/e/ao;->cq:I

    iput v1, p0, Lcom/android/calendar/e/ao;->ct:I

    .line 3401
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    invoke-virtual {v1}, Lcom/android/calendar/e/g;->getTouchMode()Lcom/android/calendar/e/ag;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/e/ag;->d:Lcom/android/calendar/e/ag;

    if-ne v1, v2, :cond_2

    .line 3402
    iget v1, p0, Lcom/android/calendar/e/ao;->cg:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_2

    .line 3403
    iget v1, p0, Lcom/android/calendar/e/ao;->ct:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/ao;->cq:I

    .line 3404
    iget v0, p0, Lcom/android/calendar/e/ao;->cq:I

    if-gez v0, :cond_3

    .line 3405
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/e/ao;->cq:I

    .line 3411
    :cond_2
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bw:Z

    .line 3412
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 3413
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3414
    return-void

    .line 3406
    :cond_3
    iget v0, p0, Lcom/android/calendar/e/ao;->cq:I

    iget v1, p0, Lcom/android/calendar/e/ao;->cr:I

    if-le v0, v1, :cond_2

    .line 3407
    iget v0, p0, Lcom/android/calendar/e/ao;->cr:I

    iput v0, p0, Lcom/android/calendar/e/ao;->cq:I

    goto :goto_0
.end method

.method public a(Landroid/view/MotionEvent;Lcom/android/calendar/dh;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3529
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bx:Z

    .line 3530
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    if-nez v0, :cond_0

    .line 3531
    iput-boolean v4, p0, Lcom/android/calendar/e/ao;->by:Z

    .line 3532
    const/4 v1, 0x0

    .line 3534
    :try_start_0
    invoke-virtual {p2}, Lcom/android/calendar/dh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 3538
    :goto_0
    invoke-virtual {p2}, Lcom/android/calendar/dh;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/calendar/dh;->a(I)V

    .line 3539
    iget-wide v2, p2, Lcom/android/calendar/dh;->b:J

    iput-wide v2, v1, Lcom/android/calendar/dh;->b:J

    .line 3540
    iput-object v1, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    .line 3541
    iput-object v1, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    .line 3542
    sget-object v0, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    .line 3543
    iput-boolean v4, p0, Lcom/android/calendar/e/ao;->cC:Z

    .line 3544
    iget v0, p2, Lcom/android/calendar/dh;->C:F

    iput v0, v1, Lcom/android/calendar/dh;->C:F

    .line 3545
    iget v0, p2, Lcom/android/calendar/dh;->D:F

    iput v0, v1, Lcom/android/calendar/dh;->D:F

    .line 3546
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->F:F

    iget-object v2, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v2, v2, Lcom/android/calendar/dh;->E:F

    sub-float/2addr v0, v2

    float-to-int v0, v0

    div-int/lit8 v3, v0, 0x2

    .line 3547
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 3549
    const/4 v2, -0x1

    .line 3550
    instance-of v4, v0, Lcom/android/calendar/e/be;

    if-eqz v4, :cond_1

    .line 3551
    check-cast v0, Lcom/android/calendar/e/be;

    .line 3552
    invoke-virtual {v0}, Lcom/android/calendar/e/be;->setListHeight()I

    move-result v0

    .line 3554
    :goto_1
    sub-int/2addr v0, v3

    int-to-float v0, v0

    iput v0, v1, Lcom/android/calendar/dh;->F:F

    .line 3555
    iget v0, v1, Lcom/android/calendar/dh;->F:F

    iget v2, p0, Lcom/android/calendar/e/ao;->Q:I

    int-to-float v2, v2

    sub-float/2addr v0, v2

    iput v0, v1, Lcom/android/calendar/dh;->E:F

    .line 3556
    iput p3, v1, Lcom/android/calendar/dh;->i:I

    .line 3557
    iget v0, p2, Lcom/android/calendar/dh;->j:I

    iget v2, p2, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v0, v2

    add-int/2addr v0, p3

    iput v0, v1, Lcom/android/calendar/dh;->j:I

    .line 3558
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/ao;->cw:I

    .line 3559
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/ao;->cx:I

    .line 3560
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->C:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/ao;->cy:I

    .line 3561
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->E:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/ao;->cA:I

    .line 3563
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/e/ao;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 3564
    return-void

    .line 3535
    :catch_0
    move-exception v0

    .line 3536
    sget-object v0, Lcom/android/calendar/e/ao;->d:Ljava/lang/String;

    const-string v2, "Fail to clone event"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 3082
    iput-object p1, p0, Lcom/android/calendar/e/ao;->bf:Ljava/util/ArrayList;

    .line 3083
    iput-object p2, p0, Lcom/android/calendar/e/ao;->bg:Ljava/util/ArrayList;

    .line 3084
    iput-object p3, p0, Lcom/android/calendar/e/ao;->bh:Ljava/util/ArrayList;

    .line 3085
    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-nez v0, :cond_0

    .line 3086
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    .line 3088
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->z()V

    .line 3089
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->r()V

    .line 3090
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->A()V

    .line 3091
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->B()V

    .line 3092
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 3093
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_1

    sget-boolean v0, Lcom/android/calendar/e/g;->p:Z

    if-nez v0, :cond_1

    .line 3094
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->g()V

    .line 3097
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3098
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3099
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->y()V

    .line 3101
    :cond_2
    return-void
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 1824
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bA:Z

    return v0
.end method

.method public a(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    .line 3318
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    .line 3319
    iget v1, p0, Lcom/android/calendar/e/ao;->cq:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    sget v1, Lcom/android/calendar/e/ao;->at:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/ao;->ci:F

    .line 3320
    sget v0, Lcom/android/calendar/e/ao;->cj:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ao;->cg:F

    .line 3321
    sget v0, Lcom/android/calendar/e/ao;->cj:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ao;->ch:F

    .line 3322
    sget v0, Lcom/android/calendar/e/ao;->at:I

    iput v0, p0, Lcom/android/calendar/e/ao;->co:I

    .line 3323
    iget v0, p0, Lcom/android/calendar/e/ao;->Q:I

    iput v0, p0, Lcom/android/calendar/e/ao;->cp:I

    .line 3324
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 3425
    if-nez p1, :cond_0

    .line 3426
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->getEventStartYPos()I

    move-result v0

    .line 3428
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->d(I)I

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1828
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v1, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    if-eq v0, v1, :cond_0

    .line 1829
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 1830
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/e/ao;->bG:I

    .line 1831
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bJ:Ljava/lang/Object;

    .line 1832
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 1834
    :cond_0
    return-void
.end method

.method public b(Landroid/view/MotionEvent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 2213
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 2214
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2215
    invoke-direct {p0, v0, v1}, Lcom/android/calendar/e/ao;->b(II)V

    .line 2216
    iget v0, p0, Lcom/android/calendar/e/ao;->bH:I

    .line 2217
    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v1, v4, :cond_0

    .line 2218
    invoke-virtual {p0, v6}, Lcom/android/calendar/e/ao;->playSoundEffect(I)V

    .line 2220
    :cond_0
    new-instance v2, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-direct {v2, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 2221
    iget v1, v2, Landroid/text/format/Time;->monthDay:I

    iget v3, p0, Lcom/android/calendar/e/ao;->bF:I

    add-int/2addr v1, v3

    iput v1, v2, Landroid/text/format/Time;->monthDay:I

    .line 2222
    invoke-virtual {v2, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 2223
    iget-boolean v1, p0, Lcom/android/calendar/e/ao;->cK:Z

    if-eqz v1, :cond_1

    .line 2224
    invoke-static {v2}, Lcom/android/calendar/e/ao;->b(Landroid/text/format/Time;)V

    .line 2226
    :cond_1
    invoke-static {v2}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    .line 2227
    if-nez v1, :cond_6

    .line 2228
    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-nez v0, :cond_4

    .line 2229
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v0, v4, :cond_3

    .line 2230
    iget v0, p0, Lcom/android/calendar/e/ao;->bG:I

    iget v1, p0, Lcom/android/calendar/e/ao;->bF:I

    if-ne v0, v1, :cond_3

    .line 2231
    invoke-virtual {v2, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 2232
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/android/calendar/e/ao;->cK:Z

    if-eqz v3, :cond_2

    .line 2233
    invoke-static {v2}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 2235
    :cond_2
    invoke-virtual {p0, v6}, Lcom/android/calendar/e/ao;->playSoundEffect(I)V

    .line 2236
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/e/ao;->a(Landroid/view/MotionEvent;Ljava/lang/Long;)V

    .line 2250
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bJ:Ljava/lang/Object;

    .line 2251
    iget v0, p0, Lcom/android/calendar/e/ao;->bF:I

    iput v0, p0, Lcom/android/calendar/e/ao;->bG:I

    .line 2252
    return-void

    .line 2240
    :cond_4
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ne v0, v4, :cond_5

    .line 2241
    invoke-virtual {p0, v6}, Lcom/android/calendar/e/ao;->playSoundEffect(I)V

    .line 2243
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    invoke-direct {p0, v0, p1}, Lcom/android/calendar/e/ao;->a(Ljava/lang/Object;Landroid/view/MotionEvent;)V

    .line 2244
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    goto :goto_0

    .line 2247
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    goto :goto_0
.end method

.method public b(Landroid/view/ScaleGestureDetector;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 3328
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ne v0, v4, :cond_0

    .line 3378
    :goto_0
    return v4

    .line 3331
    :cond_0
    sget v0, Lcom/android/calendar/e/ao;->cj:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 3333
    iget v1, p0, Lcom/android/calendar/e/ao;->co:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/android/calendar/e/ao;->cg:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/e/ao;->at:I

    .line 3335
    sget v1, Lcom/android/calendar/e/ao;->at:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cl:I

    if-ge v1, v2, :cond_5

    .line 3336
    iput v0, p0, Lcom/android/calendar/e/ao;->cg:F

    .line 3337
    iget v0, p0, Lcom/android/calendar/e/ao;->cl:I

    sput v0, Lcom/android/calendar/e/ao;->at:I

    .line 3338
    iget v0, p0, Lcom/android/calendar/e/ao;->cl:I

    iput v0, p0, Lcom/android/calendar/e/ao;->co:I

    .line 3345
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    .line 3346
    iget v1, p0, Lcom/android/calendar/e/ao;->ci:F

    sget v2, Lcom/android/calendar/e/ao;->at:I

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->cq:I

    .line 3347
    iget v1, p0, Lcom/android/calendar/e/ao;->cd:I

    sget v2, Lcom/android/calendar/e/ao;->at:I

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/android/calendar/e/ao;->ar:I

    iget v3, p0, Lcom/android/calendar/e/ao;->C:I

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/e/ao;->cr:I

    .line 3349
    iget v1, p0, Lcom/android/calendar/e/ao;->cr:I

    if-gez v1, :cond_2

    .line 3350
    iput v5, p0, Lcom/android/calendar/e/ao;->cr:I

    .line 3353
    :cond_2
    iget v1, p0, Lcom/android/calendar/e/ao;->cq:I

    if-gez v1, :cond_6

    .line 3354
    iput v5, p0, Lcom/android/calendar/e/ao;->cq:I

    .line 3355
    iget v1, p0, Lcom/android/calendar/e/ao;->cq:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    sget v1, Lcom/android/calendar/e/ao;->at:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/ao;->ci:F

    .line 3361
    :cond_3
    :goto_2
    sget v0, Lcom/android/calendar/e/ao;->cj:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 3363
    iget v1, p0, Lcom/android/calendar/e/ao;->cp:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/android/calendar/e/ao;->ch:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/e/ao;->Q:I

    .line 3365
    iget v1, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cm:I

    if-ge v1, v2, :cond_7

    .line 3368
    iput v0, p0, Lcom/android/calendar/e/ao;->ch:F

    .line 3369
    iget v0, p0, Lcom/android/calendar/e/ao;->cm:I

    iput v0, p0, Lcom/android/calendar/e/ao;->Q:I

    .line 3370
    iget v0, p0, Lcom/android/calendar/e/ao;->cm:I

    iput v0, p0, Lcom/android/calendar/e/ao;->cp:I

    .line 3376
    :cond_4
    :goto_3
    iput-boolean v4, p0, Lcom/android/calendar/e/ao;->cf:Z

    .line 3377
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    goto/16 :goto_0

    .line 3339
    :cond_5
    sget v1, Lcom/android/calendar/e/ao;->at:I

    iget v2, p0, Lcom/android/calendar/e/ao;->al:I

    if-le v1, v2, :cond_1

    .line 3340
    iput v0, p0, Lcom/android/calendar/e/ao;->cg:F

    .line 3341
    iget v0, p0, Lcom/android/calendar/e/ao;->al:I

    sput v0, Lcom/android/calendar/e/ao;->at:I

    .line 3342
    iget v0, p0, Lcom/android/calendar/e/ao;->al:I

    iput v0, p0, Lcom/android/calendar/e/ao;->co:I

    goto :goto_1

    .line 3356
    :cond_6
    iget v1, p0, Lcom/android/calendar/e/ao;->cq:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cr:I

    if-le v1, v2, :cond_3

    .line 3357
    iget v1, p0, Lcom/android/calendar/e/ao;->cr:I

    iput v1, p0, Lcom/android/calendar/e/ao;->cq:I

    .line 3358
    iget v1, p0, Lcom/android/calendar/e/ao;->cq:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    sget v1, Lcom/android/calendar/e/ao;->at:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/ao;->ci:F

    goto :goto_2

    .line 3371
    :cond_7
    iget v1, p0, Lcom/android/calendar/e/ao;->Q:I

    iget v2, p0, Lcom/android/calendar/e/ao;->cn:I

    if-le v1, v2, :cond_4

    .line 3372
    iput v0, p0, Lcom/android/calendar/e/ao;->ch:F

    .line 3373
    iget v0, p0, Lcom/android/calendar/e/ao;->cn:I

    iput v0, p0, Lcom/android/calendar/e/ao;->Q:I

    .line 3374
    iget v0, p0, Lcom/android/calendar/e/ao;->cn:I

    iput v0, p0, Lcom/android/calendar/e/ao;->cp:I

    goto :goto_3
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1915
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->t()V

    .line 1916
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/ao;->bb:I

    .line 1917
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bp:Z

    if-eqz v0, :cond_0

    .line 1918
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bq:Z

    .line 1920
    :cond_0
    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-eqz v0, :cond_1

    .line 1921
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->h()V

    .line 1923
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    .line 1924
    if-eqz v0, :cond_2

    .line 1925
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->ba:Landroid/text/format/Time;

    .line 1929
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->v()V

    .line 1930
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->w()V

    .line 1931
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->d()V

    .line 1932
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 1933
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/e/ao;->bG:I

    .line 1934
    iput-object v2, p0, Lcom/android/calendar/e/ao;->bJ:Ljava/lang/Object;

    .line 1935
    return-void

    .line 1927
    :cond_2
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/e/ao;->ba:Landroid/text/format/Time;

    goto :goto_0
.end method

.method public c(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 3576
    iget-object v1, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    if-eqz v1, :cond_2

    .line 3578
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getDraggingEventDay()I

    move-result v1

    if-ltz v1, :cond_3

    move v1, v0

    .line 3581
    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->bx:Z

    .line 3583
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 3584
    instance-of v3, v0, Lcom/android/calendar/e/be;

    if-eqz v3, :cond_0

    .line 3585
    check-cast v0, Lcom/android/calendar/e/be;

    .line 3586
    invoke-virtual {v0}, Lcom/android/calendar/e/be;->setListHeight()I

    move-result v2

    .line 3588
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/ao;->as:I

    sub-int v0, v2, v0

    .line 3589
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 3590
    iget v1, p0, Lcom/android/calendar/e/ao;->cA:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/ao;->cA:I

    .line 3592
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3594
    :cond_2
    return-void

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public c(Landroid/view/ScaleGestureDetector;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3382
    iput v0, p0, Lcom/android/calendar/e/ao;->cg:F

    .line 3383
    iput v0, p0, Lcom/android/calendar/e/ao;->ch:F

    .line 3384
    iput v0, p0, Lcom/android/calendar/e/ao;->cs:F

    .line 3385
    return-void
.end method

.method d()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 1980
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bv:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/e/ao;->br:Z

    .line 1981
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->br:Z

    if-nez v0, :cond_1

    .line 2011
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1980
    goto :goto_0

    .line 1984
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    if-nez v0, :cond_2

    .line 1985
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    .line 1987
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    invoke-static {v0, v7}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1989
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v0, v1, :cond_3

    .line 1990
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/e/ao;->aX:Landroid/text/format/Time;

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1995
    :goto_2
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v4, v7}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1996
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 1998
    iget-object v3, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1999
    :goto_3
    iget v4, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ge v2, v4, :cond_5

    .line 2000
    iget-object v4, p0, Lcom/android/calendar/e/ao;->bd:Lcom/android/calendar/h/c;

    invoke-virtual {v4, v3, v0}, Lcom/android/calendar/h/c;->a(Landroid/content/res/Resources;Landroid/text/format/Time;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2001
    if-nez v4, :cond_4

    .line 2002
    iget-object v4, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aput-object v7, v4, v2

    .line 2007
    :goto_4
    iget v4, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/text/format/Time;->monthDay:I

    .line 2008
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1999
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1992
    :cond_3
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    goto :goto_2

    .line 2004
    :cond_4
    iget v5, p0, Lcom/android/calendar/e/ao;->ag:I

    iget v6, p0, Lcom/android/calendar/e/ao;->ah:I

    invoke-static {v4, v5, v6, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2005
    iget-object v5, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    aput-object v4, v5, v2

    goto :goto_4

    .line 2010
    :cond_5
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    goto :goto_1
.end method

.method public e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3231
    invoke-virtual {p0, v1, v1, v1}, Lcom/android/calendar/e/ao;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3232
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bc:[Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3233
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3234
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3417
    iget v0, p0, Lcom/android/calendar/e/ao;->cl:I

    sput v0, Lcom/android/calendar/e/ao;->at:I

    .line 3418
    iget v0, p0, Lcom/android/calendar/e/ao;->cm:I

    iput v0, p0, Lcom/android/calendar/e/ao;->Q:I

    .line 3419
    iput v1, p0, Lcom/android/calendar/e/ao;->cr:I

    .line 3420
    iput v1, p0, Lcom/android/calendar/e/ao;->cq:I

    .line 3421
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3422
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3497
    iput-boolean v2, p0, Lcom/android/calendar/e/ao;->cC:Z

    .line 3498
    sget-object v0, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    .line 3499
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    .line 3500
    iput v2, p0, Lcom/android/calendar/e/ao;->cw:I

    .line 3501
    iput v2, p0, Lcom/android/calendar/e/ao;->cx:I

    .line 3502
    iput v2, p0, Lcom/android/calendar/e/ao;->cy:I

    .line 3503
    iput v2, p0, Lcom/android/calendar/e/ao;->cA:I

    .line 3504
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/e/ao;->cz:J

    .line 3505
    iput v2, p0, Lcom/android/calendar/e/ao;->cF:I

    .line 3506
    iput v2, p0, Lcom/android/calendar/e/ao;->cG:I

    .line 3507
    iput-boolean v2, p0, Lcom/android/calendar/e/ao;->by:Z

    .line 3508
    return-void
.end method

.method public getDraggingEventDay()I
    .locals 2

    .prologue
    .line 3567
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    if-eqz v0, :cond_0

    .line 3568
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bx:Z

    if-nez v0, :cond_0

    .line 3569
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v0, v0, Lcom/android/calendar/dh;->D:F

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v1, v1, Lcom/android/calendar/dh;->C:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v1, v1, Lcom/android/calendar/dh;->C:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->h(I)I

    move-result v0

    .line 3572
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3522
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bn:[Z

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 3523
    sput-boolean v1, Lcom/android/calendar/e/ao;->b:Z

    .line 3524
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 3525
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3526
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 695
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->cf:Z

    if-eqz v0, :cond_0

    .line 696
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/e/ao;->a(II)V

    .line 697
    iput-boolean v2, p0, Lcom/android/calendar/e/ao;->cf:Z

    .line 699
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 700
    iget v0, p0, Lcom/android/calendar/e/ao;->cq:I

    neg-int v0, v0

    iget v1, p0, Lcom/android/calendar/e/ao;->C:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    .line 701
    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 702
    iget-object v1, p0, Lcom/android/calendar/e/ao;->x:Landroid/graphics/Rect;

    .line 703
    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 704
    iget v2, p0, Lcom/android/calendar/e/ao;->as:I

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 705
    iget v2, p0, Lcom/android/calendar/e/ao;->C:I

    int-to-float v2, v2

    sub-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 706
    iget v2, p0, Lcom/android/calendar/e/ao;->ar:I

    int-to-float v2, v2

    sub-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 707
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 708
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 709
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->a(Landroid/graphics/Canvas;)V

    .line 710
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 711
    neg-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 712
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 713
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->p(Landroid/graphics/Canvas;)V

    .line 715
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 716
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x0

    .line 2524
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 2525
    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->j(I)V

    .line 2541
    :cond_0
    :goto_0
    return v0

    .line 2528
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/ao;->bP:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v1}, Lcom/android/calendar/AllInOneActivity;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2531
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bs:Z

    if-eqz v0, :cond_2

    .line 2532
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->g(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 2534
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bV:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2535
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->h(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 2537
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->bs:Z

    if-nez v0, :cond_4

    .line 2538
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 2541
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 17

    .prologue
    .line 2939
    const/16 v2, 0x16

    move/from16 v0, p1

    if-eq v0, v2, :cond_0

    const/16 v2, 0x15

    move/from16 v0, p1

    if-eq v0, v2, :cond_0

    const/16 v2, 0x13

    move/from16 v0, p1

    if-eq v0, v2, :cond_0

    const/16 v2, 0x14

    move/from16 v0, p1

    if-ne v0, v2, :cond_1

    .line 2941
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/calendar/e/ao;->playSoundEffect(I)V

    .line 2943
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2944
    sget-object v2, Lcom/android/calendar/e/bd;->d:Lcom/android/calendar/e/bd;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2947
    const/4 v2, 0x0

    .line 2948
    const/4 v3, 0x0

    .line 2949
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    iget-wide v8, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v5

    .line 2951
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/e/ao;->aV:I

    sub-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/e/ao;->bF:I

    .line 2952
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->bF:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v6, v6, -0x1

    if-le v5, v6, :cond_2

    .line 2953
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/e/ao;->bF:I

    .line 2955
    :cond_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->bF:I

    if-gez v5, :cond_3

    .line 2956
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/calendar/e/ao;->bF:I

    .line 2958
    :cond_3
    packed-switch p1, :pswitch_data_0

    .line 2995
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2996
    invoke-super/range {p0 .. p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 3026
    :goto_0
    return v2

    .line 2960
    :pswitch_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/e/ao;->bF:I

    .line 2961
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/ao;->bF:I

    if-gez v3, :cond_4

    .line 2962
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/e/ao;->bF:I

    .line 2963
    const/4 v2, -0x1

    .line 2965
    :cond_4
    const/4 v3, -0x1

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 2999
    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bH:I

    if-ltz v4, :cond_8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bH:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v5, v5, -0x1

    if-gt v4, v5, :cond_8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bF:I

    if-ltz v4, :cond_8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bF:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v5, v5, -0x1

    if-gt v4, v5, :cond_8

    .line 3000
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/e/ao;->bl:[[Ljava/lang/Object;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->bF:I

    aget-object v4, v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->bH:I

    aget-object v4, v4, v5

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    .line 3005
    :goto_2
    if-eqz v3, :cond_9

    .line 3006
    new-instance v6, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {v6, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 3007
    iget v2, v6, Landroid/text/format/Time;->monthDay:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->cd:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, v6, Landroid/text/format/Time;->monthDay:I

    .line 3008
    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 3010
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    const-wide/16 v4, 0x20

    const/4 v7, 0x0

    const-wide/16 v9, -0x1

    const/4 v11, 0x0

    const-wide/16 v12, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v3, p0

    move-object v8, v6

    invoke-virtual/range {v2 .. v15}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 3012
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2970
    :pswitch_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/ao;->bF:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/e/ao;->bF:I

    .line 2971
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/e/ao;->bF:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v4, v4, -0x1

    if-le v3, v4, :cond_5

    .line 2972
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/e/ao;->cd:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/e/ao;->bF:I

    .line 2973
    const/4 v2, 0x1

    .line 2975
    :cond_5
    const/4 v3, 0x1

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 2977
    goto/16 :goto_1

    .line 2980
    :pswitch_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bH:I

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/e/ao;->bH:I

    .line 2981
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bH:I

    if-gez v4, :cond_6

    .line 2982
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/e/ao;->bH:I

    :cond_6
    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 2985
    goto/16 :goto_1

    .line 2988
    :pswitch_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bH:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/e/ao;->bH:I

    .line 2989
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->bH:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v5, v5, -0x1

    if-le v4, v5, :cond_7

    .line 2990
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/e/ao;->am:I

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/e/ao;->bH:I

    :cond_7
    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 2993
    goto/16 :goto_1

    .line 3002
    :cond_8
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    goto/16 :goto_2

    .line 3014
    :cond_9
    if-eqz v2, :cond_a

    .line 3015
    new-instance v6, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/e/ao;->aY:Landroid/text/format/Time;

    invoke-direct {v6, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 3016
    iget v3, v6, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v2, v3

    iput v2, v6, Landroid/text/format/Time;->monthDay:I

    .line 3017
    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 3018
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/e/ao;->bW:Lcom/android/calendar/al;

    const-wide/16 v4, 0x20

    const/4 v7, 0x0

    const-wide/16 v9, -0x1

    const/4 v11, 0x0

    const-wide/16 v12, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v3, p0

    move-object v8, v6

    invoke-virtual/range {v2 .. v15}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 3022
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3023
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2958
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    .line 3031
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 3033
    sparse-switch p1, :sswitch_data_0

    .line 3063
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 3036
    :sswitch_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/calendar/e/ao;->playSoundEffect(I)V

    .line 3037
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v3, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    if-eq v2, v3, :cond_0

    .line 3042
    iget-object v2, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v3, Lcom/android/calendar/e/bd;->b:Lcom/android/calendar/e/bd;

    if-ne v2, v3, :cond_1

    .line 3047
    sget-object v0, Lcom/android/calendar/e/bd;->d:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 3048
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    goto :goto_0

    .line 3053
    :cond_1
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 3054
    sget-object v0, Lcom/android/calendar/e/bd;->e:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 3055
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 3056
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->performLongClick()Z

    goto :goto_0

    .line 3033
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 662
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 663
    iput p2, p0, Lcom/android/calendar/e/ao;->as:I

    .line 664
    iput p1, p0, Lcom/android/calendar/e/ao;->ar:I

    .line 665
    iget v0, p0, Lcom/android/calendar/e/ao;->ar:I

    iget v2, p0, Lcom/android/calendar/e/ao;->C:I

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    mul-int/lit8 v2, v2, 0x1

    sub-int/2addr v0, v2

    .line 666
    iget v2, p0, Lcom/android/calendar/e/ao;->cd:I

    div-int v2, v0, v2

    iput v2, p0, Lcom/android/calendar/e/ao;->cl:I

    .line 667
    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/e/ao;->al:I

    .line 668
    if-eq p1, p3, :cond_3

    move v0, v1

    .line 669
    :goto_0
    sget v2, Lcom/android/calendar/e/ao;->at:I

    if-eqz v2, :cond_0

    if-eqz v0, :cond_1

    .line 670
    :cond_0
    iget v0, p0, Lcom/android/calendar/e/ao;->cl:I

    sput v0, Lcom/android/calendar/e/ao;->at:I

    .line 672
    :cond_1
    sget v0, Lcom/android/calendar/e/ao;->at:I

    iget v2, p0, Lcom/android/calendar/e/ao;->al:I

    if-le v0, v2, :cond_4

    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v0, v1, :cond_4

    .line 673
    iget v0, p0, Lcom/android/calendar/e/ao;->al:I

    sput v0, Lcom/android/calendar/e/ao;->at:I

    .line 678
    :cond_2
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/e/ao;->a(II)V

    .line 679
    return-void

    .line 668
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 674
    :cond_4
    sget v0, Lcom/android/calendar/e/ao;->at:I

    iget v1, p0, Lcom/android/calendar/e/ao;->cl:I

    if-ge v0, v1, :cond_2

    .line 675
    iget v0, p0, Lcom/android/calendar/e/ao;->cl:I

    sput v0, Lcom/android/calendar/e/ao;->at:I

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 2015
    if-nez p1, :cond_1

    move v10, v9

    .line 2108
    :cond_0
    :goto_0
    return v10

    .line 2018
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2019
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->C()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    move v0, v10

    .line 2022
    :cond_2
    sparse-switch v0, :sswitch_data_0

    .line 2108
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bD:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v9, v10

    :cond_4
    move v10, v9

    goto :goto_0

    .line 2024
    :sswitch_0
    iput-boolean v10, p0, Lcom/android/calendar/e/ao;->cC:Z

    .line 2025
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bD:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 2029
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cv:Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    if-ne v0, v1, :cond_5

    .line 2030
    iget-object v1, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    iget v3, p0, Lcom/android/calendar/e/ao;->as:I

    iget-object v4, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    iget v5, p0, Lcom/android/calendar/e/ao;->cy:I

    iget-wide v6, p0, Lcom/android/calendar/e/ao;->cz:J

    iget v8, p0, Lcom/android/calendar/e/ao;->cF:I

    move-object v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/android/calendar/e/g;->a(Landroid/view/MotionEvent;ILcom/android/calendar/dh;IJI)V

    .line 2032
    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-ne v0, v10, :cond_5

    .line 2033
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->g()V

    .line 2036
    :cond_5
    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-eqz v0, :cond_6

    .line 2037
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->e(Landroid/view/MotionEvent;)V

    .line 2039
    :cond_6
    iput-boolean v9, p0, Lcom/android/calendar/e/ao;->cC:Z

    .line 2040
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    sget-object v1, Lcom/android/calendar/e/bd;->b:Lcom/android/calendar/e/bd;

    if-ne v0, v1, :cond_7

    .line 2041
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2043
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bD:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2044
    iput-boolean v9, p0, Lcom/android/calendar/e/ao;->bw:Z

    .line 2045
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    goto :goto_0

    .line 2049
    :sswitch_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    .line 2050
    sget-object v0, Lcom/android/calendar/e/bd;->a:Lcom/android/calendar/e/bd;

    iput-object v0, p0, Lcom/android/calendar/e/ao;->bE:Lcom/android/calendar/e/bd;

    .line 2051
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->invalidate()V

    goto :goto_0

    .line 2055
    :sswitch_3
    invoke-direct {p0}, Lcom/android/calendar/e/ao;->C()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2056
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/android/calendar/e/ao;->as:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/android/calendar/e/ao;->G:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b

    .line 2057
    iput-boolean v10, p0, Lcom/android/calendar/e/ao;->bx:Z

    .line 2058
    iget-boolean v0, p0, Lcom/android/calendar/e/ao;->by:Z

    if-nez v0, :cond_8

    .line 2060
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/android/calendar/e/ao;->G:F

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 2061
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    if-eqz v0, :cond_8

    .line 2062
    iget-object v1, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    iget-object v0, p0, Lcom/android/calendar/e/ao;->bI:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/dh;

    invoke-virtual {v1, p1, v0}, Lcom/android/calendar/e/g;->a(Landroid/view/MotionEvent;Lcom/android/calendar/dh;)V

    .line 2069
    :cond_8
    :goto_1
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->i(Landroid/view/MotionEvent;)V

    .line 2070
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cB:Lcom/android/calendar/dh;

    invoke-direct {p0, v0}, Lcom/android/calendar/e/ao;->a(Lcom/android/calendar/dh;)V

    .line 2071
    invoke-virtual {p0}, Lcom/android/calendar/e/ao;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2073
    instance-of v1, v0, Lcom/android/calendar/e/be;

    if-eqz v1, :cond_c

    .line 2074
    check-cast v0, Lcom/android/calendar/e/be;

    .line 2075
    invoke-virtual {v0}, Lcom/android/calendar/e/be;->setListHeight()I

    move-result v0

    .line 2077
    :goto_2
    iget v1, p0, Lcom/android/calendar/e/ao;->as:I

    sub-int/2addr v0, v1

    .line 2078
    if-eqz v0, :cond_9

    iget-boolean v1, p0, Lcom/android/calendar/e/ao;->by:Z

    if-eqz v1, :cond_9

    .line 2079
    iget v1, p0, Lcom/android/calendar/e/ao;->cA:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/ao;->cA:I

    .line 2082
    :cond_9
    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-eqz v0, :cond_a

    .line 2083
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->j(Landroid/view/MotionEvent;)V

    .line 2086
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/e/ao;->bD:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_0

    .line 2066
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cu:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/g;->b(Landroid/view/MotionEvent;)V

    .line 2067
    iput-boolean v9, p0, Lcom/android/calendar/e/ao;->bx:Z

    goto :goto_1

    .line 2089
    :sswitch_4
    invoke-virtual {p0, p1}, Lcom/android/calendar/e/ao;->a(Landroid/view/MotionEvent;)V

    .line 2090
    iget v0, p0, Lcom/android/calendar/e/ao;->bH:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/e/ao;->cd:I

    if-le v0, v10, :cond_0

    .line 2091
    sput-boolean v10, Lcom/android/calendar/e/ao;->b:Z

    goto/16 :goto_0

    .line 2096
    :sswitch_5
    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-eqz v0, :cond_0

    .line 2097
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 2102
    :sswitch_6
    sget-boolean v0, Lcom/android/calendar/e/ao;->b:Z

    if-eqz v0, :cond_0

    .line 2103
    invoke-direct {p0, p1}, Lcom/android/calendar/e/ao;->j(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    :cond_c
    move v0, v9

    goto :goto_2

    .line 2022
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0xd3 -> :sswitch_4
        0xd4 -> :sswitch_5
        0xd5 -> :sswitch_6
    .end sparse-switch
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 3434
    const/16 v0, 0x40

    if-ne p1, v0, :cond_0

    .line 3435
    iget-object v0, p0, Lcom/android/calendar/e/ao;->cc:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/e/ao;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3437
    sget-boolean v0, Lcom/android/calendar/e/ao;->c:Z

    if-eqz v0, :cond_0

    .line 3438
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/e/ao;->c:Z

    .line 3439
    const/4 v0, 0x1

    .line 3442
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method
