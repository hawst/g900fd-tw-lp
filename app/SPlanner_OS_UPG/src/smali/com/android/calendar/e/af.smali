.class Lcom/android/calendar/e/af;
.super Ljava/lang/Object;
.source "DayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Z

.field final synthetic e:Lcom/android/calendar/e/g;


# direct methods
.method private constructor <init>(Lcom/android/calendar/e/g;)V
    .locals 1

    .prologue
    .line 4389
    iput-object p1, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4393
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/e/af;->d:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/e/g;Lcom/android/calendar/e/h;)V
    .locals 0

    .prologue
    .line 4389
    invoke-direct {p0, p1}, Lcom/android/calendar/e/af;-><init>(Lcom/android/calendar/e/g;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 4396
    iput p1, p0, Lcom/android/calendar/e/af;->c:I

    .line 4397
    iget v0, p0, Lcom/android/calendar/e/af;->c:I

    invoke-static {}, Lcom/android/calendar/e/g;->q()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/e/af;->b:I

    .line 4398
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;Z)Z

    .line 4399
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4403
    iget-boolean v0, p0, Lcom/android/calendar/e/af;->d:Z

    if-eqz v0, :cond_0

    .line 4404
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/e/af;->b:I

    if-ge v0, v1, :cond_1

    .line 4405
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->m(Lcom/android/calendar/e/g;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/e/af;->a:I

    .line 4409
    :goto_0
    iput-boolean v2, p0, Lcom/android/calendar/e/af;->d:Z

    .line 4411
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->l(Lcom/android/calendar/e/g;)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/e/af;->a:I

    add-int/2addr v0, v1

    .line 4413
    if-gez v0, :cond_2

    .line 4414
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0, v2}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    .line 4415
    iput v2, p0, Lcom/android/calendar/e/af;->a:I

    .line 4427
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->o(Lcom/android/calendar/e/g;)V

    .line 4428
    iget v0, p0, Lcom/android/calendar/e/af;->a:I

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->p(Lcom/android/calendar/e/g;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4429
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p0}, Lcom/android/calendar/e/g;->post(Ljava/lang/Runnable;)Z

    .line 4435
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0, v3}, Lcom/android/calendar/e/g;->g(Lcom/android/calendar/e/g;Z)Z

    .line 4436
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-virtual {v0}, Lcom/android/calendar/e/g;->invalidate()V

    .line 4437
    return-void

    .line 4407
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0}, Lcom/android/calendar/e/g;->m(Lcom/android/calendar/e/g;)I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lcom/android/calendar/e/af;->a:I

    goto :goto_0

    .line 4416
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v1

    if-le v0, v1, :cond_3

    .line 4417
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    iget-object v1, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v1}, Lcom/android/calendar/e/g;->n(Lcom/android/calendar/e/g;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    .line 4418
    iput v2, p0, Lcom/android/calendar/e/af;->a:I

    goto :goto_1

    .line 4419
    :cond_3
    iget v1, p0, Lcom/android/calendar/e/af;->a:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/android/calendar/e/af;->b:I

    if-ge v0, v1, :cond_5

    :cond_4
    iget v1, p0, Lcom/android/calendar/e/af;->a:I

    if-gez v1, :cond_6

    iget v1, p0, Lcom/android/calendar/e/af;->b:I

    if-ge v0, v1, :cond_6

    .line 4421
    :cond_5
    iput v2, p0, Lcom/android/calendar/e/af;->a:I

    .line 4422
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    iget v1, p0, Lcom/android/calendar/e/af;->b:I

    invoke-static {v0, v1}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    goto :goto_1

    .line 4424
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v1, v0}, Lcom/android/calendar/e/g;->d(Lcom/android/calendar/e/g;I)I

    goto :goto_1

    .line 4431
    :cond_7
    iput-boolean v3, p0, Lcom/android/calendar/e/af;->d:Z

    .line 4432
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0, v2}, Lcom/android/calendar/e/g;->e(Lcom/android/calendar/e/g;Z)Z

    .line 4433
    iget-object v0, p0, Lcom/android/calendar/e/af;->e:Lcom/android/calendar/e/g;

    invoke-static {v0, v3}, Lcom/android/calendar/e/g;->f(Lcom/android/calendar/e/g;Z)V

    goto :goto_2
.end method
