.class public final enum Lcom/android/calendar/e/ab;
.super Ljava/lang/Enum;
.source "DayView.java"


# static fields
.field public static final enum a:Lcom/android/calendar/e/ab;

.field public static final enum b:Lcom/android/calendar/e/ab;

.field public static final enum c:Lcom/android/calendar/e/ab;

.field public static final enum d:Lcom/android/calendar/e/ab;

.field public static final enum e:Lcom/android/calendar/e/ab;

.field private static final synthetic f:[Lcom/android/calendar/e/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 485
    new-instance v0, Lcom/android/calendar/e/ab;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    .line 486
    new-instance v0, Lcom/android/calendar/e/ab;

    const-string v1, "DRAG"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    .line 487
    new-instance v0, Lcom/android/calendar/e/ab;

    const-string v1, "RESIZE_READY"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    .line 488
    new-instance v0, Lcom/android/calendar/e/ab;

    const-string v1, "RESIZING_UP"

    invoke-direct {v0, v1, v5}, Lcom/android/calendar/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    .line 489
    new-instance v0, Lcom/android/calendar/e/ab;

    const-string v1, "RESIZING_DOWN"

    invoke-direct {v0, v1, v6}, Lcom/android/calendar/e/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    .line 484
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/calendar/e/ab;

    sget-object v1, Lcom/android/calendar/e/ab;->a:Lcom/android/calendar/e/ab;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/e/ab;->b:Lcom/android/calendar/e/ab;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/e/ab;->c:Lcom/android/calendar/e/ab;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/calendar/e/ab;->d:Lcom/android/calendar/e/ab;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/calendar/e/ab;->e:Lcom/android/calendar/e/ab;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/calendar/e/ab;->f:[Lcom/android/calendar/e/ab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/e/ab;
    .locals 1

    .prologue
    .line 484
    const-class v0, Lcom/android/calendar/e/ab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/e/ab;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/e/ab;
    .locals 1

    .prologue
    .line 484
    sget-object v0, Lcom/android/calendar/e/ab;->f:[Lcom/android/calendar/e/ab;

    invoke-virtual {v0}, [Lcom/android/calendar/e/ab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/e/ab;

    return-object v0
.end method
