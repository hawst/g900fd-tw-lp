.class Lcom/android/calendar/e/w;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "DayView.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/e/g;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/g;)V
    .locals 0

    .prologue
    .line 6295
    iput-object p1, p0, Lcom/android/calendar/e/w;->a:Lcom/android/calendar/e/g;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 6321
    iget-object v0, p0, Lcom/android/calendar/e/w;->a:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1}, Lcom/android/calendar/e/g;->a(Landroid/view/MotionEvent;)V

    .line 6322
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 6316
    iget-object v0, p0, Lcom/android/calendar/e/w;->a:Lcom/android/calendar/e/g;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/calendar/e/g;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 6317
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 6308
    iget-object v0, p0, Lcom/android/calendar/e/w;->a:Lcom/android/calendar/e/g;

    iget-object v0, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-nez v0, :cond_0

    .line 6312
    :goto_0
    return-void

    .line 6311
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/w;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, p1}, Lcom/android/calendar/e/g;->c(Lcom/android/calendar/e/g;Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 6298
    iget-object v0, p0, Lcom/android/calendar/e/w;->a:Lcom/android/calendar/e/g;

    iget-object v0, v0, Lcom/android/calendar/e/g;->d:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-nez v0, :cond_0

    .line 6299
    const/4 v0, 0x0

    .line 6303
    :goto_0
    return v0

    .line 6302
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/e/w;->a:Lcom/android/calendar/e/g;

    invoke-static {v0, p1}, Lcom/android/calendar/e/g;->b(Lcom/android/calendar/e/g;Landroid/view/MotionEvent;)V

    .line 6303
    const/4 v0, 0x1

    goto :goto_0
.end method
