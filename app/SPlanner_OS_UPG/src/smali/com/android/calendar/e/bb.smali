.class Lcom/android/calendar/e/bb;
.super Ljava/lang/Object;
.source "WeekAllDayView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/e/ba;


# direct methods
.method constructor <init>(Lcom/android/calendar/e/ba;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/android/calendar/e/bb;->a:Lcom/android/calendar/e/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    .line 426
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 427
    iget-object v1, p0, Lcom/android/calendar/e/bb;->a:Lcom/android/calendar/e/ba;

    iget-object v2, p0, Lcom/android/calendar/e/bb;->a:Lcom/android/calendar/e/ba;

    iget-object v2, v2, Lcom/android/calendar/e/ba;->c:Lcom/android/calendar/e/ao;

    invoke-static {v2}, Lcom/android/calendar/e/ao;->b(Lcom/android/calendar/e/ao;)I

    move-result v2

    add-int/lit8 v2, v2, -0x5

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Lcom/android/calendar/e/ba;->a:I

    .line 428
    float-to-double v2, v0

    const-wide v4, 0x3fe3333333333333L    # 0.6

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 429
    iget-object v0, p0, Lcom/android/calendar/e/bb;->a:Lcom/android/calendar/e/ba;

    iget-object v0, v0, Lcom/android/calendar/e/ba;->c:Lcom/android/calendar/e/ao;

    invoke-static {v0}, Lcom/android/calendar/e/ao;->c(Lcom/android/calendar/e/ao;)Landroid/graphics/Paint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 436
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/e/bb;->a:Lcom/android/calendar/e/ba;

    iget-object v0, v0, Lcom/android/calendar/e/ba;->c:Lcom/android/calendar/e/ao;

    invoke-virtual {v0}, Lcom/android/calendar/e/ao;->invalidate()V

    .line 437
    return-void

    .line 431
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/e/bb;->a:Lcom/android/calendar/e/ba;

    iget-object v1, v1, Lcom/android/calendar/e/ba;->c:Lcom/android/calendar/e/ao;

    invoke-static {v1}, Lcom/android/calendar/e/ao;->c(Lcom/android/calendar/e/ao;)Landroid/graphics/Paint;

    move-result-object v1

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 432
    float-to-double v0, v0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    rem-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/android/calendar/e/bb;->a:Lcom/android/calendar/e/ba;

    iget v1, v0, Lcom/android/calendar/e/ba;->b:I

    iget-object v2, p0, Lcom/android/calendar/e/bb;->a:Lcom/android/calendar/e/ba;

    invoke-static {v2}, Lcom/android/calendar/e/ba;->a(Lcom/android/calendar/e/ba;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/android/calendar/e/ba;->b:I

    goto :goto_0
.end method
