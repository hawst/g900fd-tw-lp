.class Lcom/android/calendar/er;
.super Lcom/android/calendar/ew;
.source "LinkActionChooserFragment.java"


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Lcom/android/calendar/eo;


# direct methods
.method constructor <init>(Lcom/android/calendar/eo;Landroid/app/Activity;IZZ)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/android/calendar/er;->c:Lcom/android/calendar/eo;

    iput-boolean p4, p0, Lcom/android/calendar/er;->a:Z

    iput-boolean p5, p0, Lcom/android/calendar/er;->b:Z

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/ew;-><init>(Landroid/app/Activity;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 135
    .line 136
    const/4 v0, -0x1

    .line 137
    iget-boolean v2, p0, Lcom/android/calendar/er;->a:Z

    if-eqz v2, :cond_1

    .line 138
    const-string v0, "tel"

    invoke-virtual {p2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 139
    invoke-virtual {p2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v4

    invoke-static {p1, v0}, Lcom/android/calendar/eo;->a(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    .line 146
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/er;->c:Lcom/android/calendar/eo;

    invoke-static {v2}, Lcom/android/calendar/eo;->a(Lcom/android/calendar/eo;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 147
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 149
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 161
    :goto_1
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_2
    return-void

    .line 140
    :cond_1
    iget-boolean v2, p0, Lcom/android/calendar/er;->b:Z

    if-eqz v2, :cond_0

    .line 141
    const-string v0, "mailto"

    invoke-virtual {p2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 142
    invoke-virtual {p2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v4

    invoke-static {p1, v0}, Lcom/android/calendar/eo;->b(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 151
    :cond_2
    invoke-static {}, Lcom/android/calendar/dz;->o()Ljava/lang/String;

    move-result-object v2

    .line 152
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 153
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.contacts.action.SHOW_OR_CREATE_CONTACT"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 157
    :goto_3
    const-string v1, "com.android.contacts.action.FORCE_CREATE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    .line 155
    :cond_3
    new-instance v0, Landroid/content/Intent;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".action.SHOW_OR_CREATE_CONTACT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_3

    .line 162
    :catch_0
    move-exception v0

    .line 163
    iget-object v1, p0, Lcom/android/calendar/er;->c:Lcom/android/calendar/eo;

    invoke-virtual {v1}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0f005f

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 164
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_2
.end method
