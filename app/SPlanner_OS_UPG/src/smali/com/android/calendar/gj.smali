.class public Lcom/android/calendar/gj;
.super Ljava/lang/Object;
.source "SDImportExportUtils.java"


# static fields
.field private static final a:Z

.field private static b:Lcom/android/calendar/gj;

.field private static c:Landroid/os/storage/StorageManager;

.field private static d:Ljava/lang/String;

.field private static e:Z

.field private static f:Z

.field private static g:Ljava/lang/String;

.field private static h:Z

.field private static i:Z


# instance fields
.field private j:Ljava/io/File;

.field private k:Ljava/io/File;

.field private l:Ljava/io/File;

.field private m:Ljava/io/File;

.field private n:Ljava/io/File;

.field private o:Ljava/util/ArrayList;

.field private p:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/android/calendar/gj;->a:Z

    .line 45
    sput-object v3, Lcom/android/calendar/gj;->b:Lcom/android/calendar/gj;

    .line 49
    sput-object v3, Lcom/android/calendar/gj;->c:Landroid/os/storage/StorageManager;

    .line 52
    const-string v0, ""

    sput-object v0, Lcom/android/calendar/gj;->d:Ljava/lang/String;

    .line 54
    sput-boolean v1, Lcom/android/calendar/gj;->e:Z

    .line 56
    sput-boolean v1, Lcom/android/calendar/gj;->f:Z

    .line 59
    const-string v0, ""

    sput-object v0, Lcom/android/calendar/gj;->g:Ljava/lang/String;

    .line 61
    sput-boolean v1, Lcom/android/calendar/gj;->h:Z

    .line 63
    sput-boolean v1, Lcom/android/calendar/gj;->i:Z

    return-void

    :cond_0
    move v0, v1

    .line 43
    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object v0, p0, Lcom/android/calendar/gj;->j:Ljava/io/File;

    .line 68
    iput-object v0, p0, Lcom/android/calendar/gj;->k:Ljava/io/File;

    .line 70
    iput-object v0, p0, Lcom/android/calendar/gj;->l:Ljava/io/File;

    .line 72
    iput-object v0, p0, Lcom/android/calendar/gj;->m:Ljava/io/File;

    .line 74
    iput-object v0, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    .line 76
    iput-object v0, p0, Lcom/android/calendar/gj;->o:Ljava/util/ArrayList;

    .line 82
    return-void
.end method

.method public static a()Lcom/android/calendar/gj;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/android/calendar/gj;->b:Lcom/android/calendar/gj;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/android/calendar/gj;

    invoke-direct {v0}, Lcom/android/calendar/gj;-><init>()V

    sput-object v0, Lcom/android/calendar/gj;->b:Lcom/android/calendar/gj;

    .line 88
    :cond_0
    sget-object v0, Lcom/android/calendar/gj;->b:Lcom/android/calendar/gj;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 184
    const/4 v0, 0x1

    .line 186
    :goto_0
    invoke-static {p1, v0}, Lcom/android/calendar/gj;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/calendar/gj;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    const-string v1, "SDImportExportUtils"

    const-string v2, "getRecordFileName : !checkFileNumber"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    add-int/lit8 v0, v0, 0x1

    .line 189
    goto :goto_0

    .line 193
    :cond_0
    invoke-static {p1, v0}, Lcom/android/calendar/gj;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 222
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 231
    :goto_0
    return-object p0

    .line 224
    :cond_0
    const/16 v0, 0xb

    if-ge p1, v0, :cond_1

    .line 225
    add-int/lit8 v0, p1, -0x1

    .line 226
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 228
    :cond_1
    add-int/lit8 v0, p1, -0x1

    .line 229
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 116
    const-string v0, ""

    sput-object v0, Lcom/android/calendar/gj;->d:Ljava/lang/String;

    .line 117
    sput-boolean v1, Lcom/android/calendar/gj;->e:Z

    .line 118
    sput-boolean v1, Lcom/android/calendar/gj;->f:Z

    .line 120
    const-string v0, ""

    sput-object v0, Lcom/android/calendar/gj;->g:Ljava/lang/String;

    .line 121
    sput-boolean v1, Lcom/android/calendar/gj;->h:Z

    .line 122
    sput-boolean v1, Lcom/android/calendar/gj;->i:Z

    .line 124
    sget-object v0, Lcom/android/calendar/gj;->c:Landroid/os/storage/StorageManager;

    if-nez v0, :cond_0

    .line 125
    const-string v0, "storage"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    sput-object v0, Lcom/android/calendar/gj;->c:Landroid/os/storage/StorageManager;

    .line 128
    :cond_0
    sget-object v0, Lcom/android/calendar/gj;->c:Landroid/os/storage/StorageManager;

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v2

    .line 129
    array-length v3, v2

    move v0, v1

    .line 130
    :goto_0
    if-ge v0, v3, :cond_7

    .line 131
    aget-object v4, v2, v0

    .line 132
    sget-object v5, Lcom/android/calendar/gj;->c:Landroid/os/storage/StorageManager;

    invoke-virtual {v5, v4}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 133
    aget-object v6, v2, v1

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 134
    sput-object v4, Lcom/android/calendar/gj;->d:Ljava/lang/String;

    .line 135
    const-string v4, "mounted"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 136
    sput-boolean v8, Lcom/android/calendar/gj;->e:Z

    .line 142
    :goto_1
    :try_start_0
    new-instance v4, Landroid/os/StatFs;

    sget-object v5, Lcom/android/calendar/gj;->d:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v6

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v4

    mul-long/2addr v4, v6

    cmp-long v4, v4, v10

    if-lez v4, :cond_3

    .line 144
    const/4 v4, 0x0

    sput-boolean v4, Lcom/android/calendar/gj;->f:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_2
    sput-boolean v1, Lcom/android/calendar/gj;->e:Z

    goto :goto_1

    .line 146
    :cond_3
    const/4 v4, 0x1

    :try_start_1
    sput-boolean v4, Lcom/android/calendar/gj;->f:Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 148
    :catch_0
    move-exception v4

    .line 149
    sput-boolean v8, Lcom/android/calendar/gj;->f:Z

    goto :goto_2

    .line 151
    :cond_4
    aget-object v6, v2, v8

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 152
    sput-object v4, Lcom/android/calendar/gj;->g:Ljava/lang/String;

    .line 153
    const-string v4, "mounted"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 154
    sput-boolean v8, Lcom/android/calendar/gj;->h:Z

    .line 160
    :goto_3
    :try_start_2
    new-instance v4, Landroid/os/StatFs;

    sget-object v5, Lcom/android/calendar/gj;->g:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v6

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v4

    mul-long/2addr v4, v6

    cmp-long v4, v4, v10

    if-lez v4, :cond_6

    .line 162
    const/4 v4, 0x0

    sput-boolean v4, Lcom/android/calendar/gj;->i:Z
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 166
    :catch_1
    move-exception v4

    .line 167
    sput-boolean v8, Lcom/android/calendar/gj;->i:Z

    goto :goto_2

    .line 156
    :cond_5
    sput-boolean v1, Lcom/android/calendar/gj;->h:Z

    goto :goto_3

    .line 164
    :cond_6
    const/4 v4, 0x1

    :try_start_3
    sput-boolean v4, Lcom/android/calendar/gj;->i:Z
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 173
    :cond_7
    sget-boolean v0, Lcom/android/calendar/gj;->a:Z

    if-eqz v0, :cond_8

    .line 174
    const-string v0, "SDImportExportUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkStorageState : mSDInternalStoragePath = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/gj;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const-string v0, "SDImportExportUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkStorageState : mSDInternalMounted = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/calendar/gj;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const-string v0, "SDImportExportUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkStorageState : mSDInternalIsFull = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/calendar/gj;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    const-string v0, "SDImportExportUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkStorageState : mSDExternalStoragePath = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/gj;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const-string v0, "SDImportExportUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkStorageState : mSDExternalMounted = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/calendar/gj;->h:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const-string v0, "SDImportExportUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkStorageState : mSDExternalIsFull = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/calendar/gj;->i:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :cond_8
    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 6

    .prologue
    .line 357
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 358
    if-eqz v1, :cond_3

    .line 359
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 360
    invoke-virtual {v3}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    .line 361
    iget-object v5, p0, Lcom/android/calendar/gj;->p:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 359
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 365
    :cond_1
    iget-object v5, p0, Lcom/android/calendar/gj;->p:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 367
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 368
    invoke-direct {p0, v3}, Lcom/android/calendar/gj;->a(Ljava/io/File;)V

    goto :goto_1

    .line 369
    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".vcs"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 370
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 371
    iget-object v4, p0, Lcom/android/calendar/gj;->o:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 376
    :cond_3
    return-void
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 96
    sget-boolean v0, Lcom/android/calendar/gj;->e:Z

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 197
    .line 198
    new-instance v0, Lcom/android/calendar/gk;

    invoke-direct {v0, p1}, Lcom/android/calendar/gk;-><init>(Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    invoke-virtual {v1, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 108
    sget-boolean v0, Lcom/android/calendar/gj;->h:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;I)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 321
    invoke-static {p1}, Lcom/android/calendar/gj;->a(Landroid/app/Activity;)V

    .line 323
    iget-object v0, p0, Lcom/android/calendar/gj;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 324
    iget-object v0, p0, Lcom/android/calendar/gj;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/android/calendar/gj;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 331
    :cond_0
    :goto_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_4

    .line 332
    iget-object v0, p0, Lcom/android/calendar/gj;->l:Ljava/io/File;

    if-nez v0, :cond_1

    .line 333
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/calendar/gj;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/gj;->l:Ljava/io/File;

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/gj;->l:Ljava/io/File;

    iput-object v0, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    .line 345
    :cond_2
    :goto_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/gj;->p:Ljava/util/Set;

    .line 347
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/android/calendar/gj;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/gj;->o:Ljava/util/ArrayList;

    return-object v0

    .line 328
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/gj;->o:Ljava/util/ArrayList;

    goto :goto_0

    .line 337
    :cond_4
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 338
    iget-object v0, p0, Lcom/android/calendar/gj;->m:Ljava/io/File;

    if-nez v0, :cond_5

    .line 339
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/calendar/gj;->g:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/gj;->m:Ljava/io/File;

    .line 342
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/gj;->m:Ljava/io/File;

    iput-object v0, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    goto :goto_1

    .line 348
    :catch_0
    move-exception v0

    .line 349
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public a(Landroid/app/Activity;Landroid/net/Uri;I)Z
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 235
    sget-boolean v2, Lcom/android/calendar/gj;->a:Z

    if-eqz v2, :cond_0

    .line 236
    const-string v2, "SDImportExportUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "saveVCalendar : uri = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_0
    new-instance v2, Lcom/android/calendar/vcal/u;

    invoke-direct {v2, p1, p2}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 240
    invoke-virtual {v2}, Lcom/android/calendar/vcal/u;->a()Ljava/lang/String;

    move-result-object v4

    .line 241
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 242
    :cond_1
    sget-boolean v1, Lcom/android/calendar/gj;->a:Z

    if-eqz v1, :cond_2

    .line 243
    const-string v1, "SDImportExportUtils"

    const-string v2, "saveVCalendar : vcsData has no values"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_2
    :goto_0
    return v0

    .line 248
    :cond_3
    invoke-static {p1}, Lcom/android/calendar/gj;->a(Landroid/app/Activity;)V

    .line 250
    if-ne p3, v1, :cond_4

    sget-boolean v3, Lcom/android/calendar/gj;->e:Z

    if-nez v3, :cond_4

    .line 251
    sget-boolean v1, Lcom/android/calendar/gj;->a:Z

    if-eqz v1, :cond_2

    .line 252
    const-string v1, "SDImportExportUtils"

    const-string v2, "saveVCalendar : internal not mounted"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 255
    :cond_4
    if-ne p3, v5, :cond_5

    sget-boolean v3, Lcom/android/calendar/gj;->h:Z

    if-nez v3, :cond_5

    .line 256
    sget-boolean v1, Lcom/android/calendar/gj;->a:Z

    if-eqz v1, :cond_2

    .line 257
    const-string v1, "SDImportExportUtils"

    const-string v2, "saveVCalendar : external not mounted"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 262
    :cond_5
    if-ne p3, v1, :cond_6

    sget-boolean v3, Lcom/android/calendar/gj;->f:Z

    if-eqz v3, :cond_6

    .line 263
    sget-boolean v1, Lcom/android/calendar/gj;->a:Z

    if-eqz v1, :cond_2

    .line 264
    const-string v1, "SDImportExportUtils"

    const-string v2, "saveVCalendar : internal memory full"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 267
    :cond_6
    if-ne p3, v5, :cond_7

    sget-boolean v3, Lcom/android/calendar/gj;->i:Z

    if-eqz v3, :cond_7

    .line 268
    sget-boolean v1, Lcom/android/calendar/gj;->a:Z

    if-eqz v1, :cond_2

    .line 269
    const-string v1, "SDImportExportUtils"

    const-string v2, "saveVCalendar : external memory full"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 274
    :cond_7
    if-ne p3, v1, :cond_c

    .line 275
    iget-object v3, p0, Lcom/android/calendar/gj;->j:Ljava/io/File;

    if-nez v3, :cond_8

    .line 276
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/android/calendar/gj;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/Backup/vCalendar"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/calendar/gj;->j:Ljava/io/File;

    .line 279
    :cond_8
    iget-object v3, p0, Lcom/android/calendar/gj;->j:Ljava/io/File;

    iput-object v3, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    .line 288
    :cond_9
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_a

    .line 289
    iget-object v3, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 290
    const-string v3, "SDImportExportUtils"

    const-string v5, "saveVCalendar : back up folder does not exist"

    invoke-static {v3, v5}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_a
    const/4 v3, 0x0

    .line 295
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    .line 297
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    invoke-virtual {v2}, Lcom/android/calendar/vcal/u;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/calendar/gj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 298
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    :try_start_1
    invoke-virtual {v2, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 300
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 307
    if-eqz v2, :cond_b

    .line 309
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_b
    :goto_2
    move v0, v1

    .line 312
    goto/16 :goto_0

    .line 280
    :cond_c
    if-ne p3, v5, :cond_9

    .line 281
    iget-object v3, p0, Lcom/android/calendar/gj;->k:Ljava/io/File;

    if-nez v3, :cond_d

    .line 282
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/android/calendar/gj;->g:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/Backup/vCalendar"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/calendar/gj;->k:Ljava/io/File;

    .line 285
    :cond_d
    iget-object v3, p0, Lcom/android/calendar/gj;->k:Ljava/io/File;

    iput-object v3, p0, Lcom/android/calendar/gj;->n:Ljava/io/File;

    goto :goto_1

    .line 310
    :catch_0
    move-exception v0

    .line 311
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 302
    :catch_1
    move-exception v1

    move-object v2, v3

    .line 303
    :goto_3
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 307
    if-eqz v2, :cond_2

    .line 309
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 310
    :catch_2
    move-exception v1

    .line 311
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 304
    :catch_3
    move-exception v1

    .line 305
    :goto_4
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 307
    if-eqz v3, :cond_2

    .line 309
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_0

    .line 310
    :catch_4
    move-exception v1

    .line 311
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 307
    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v3, :cond_e

    .line 309
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 312
    :cond_e
    :goto_6
    throw v0

    .line 310
    :catch_5
    move-exception v1

    .line 311
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 307
    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_5

    .line 304
    :catch_6
    move-exception v1

    move-object v3, v2

    goto :goto_4

    .line 302
    :catch_7
    move-exception v1

    goto :goto_3
.end method
