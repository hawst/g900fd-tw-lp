.class Lcom/android/calendar/af;
.super Landroid/os/AsyncTask;
.source "AllInOneActivity.java"


# instance fields
.field a:Ljava/lang/String;

.field final synthetic b:Lcom/android/calendar/AllInOneActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/AllInOneActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3251
    iput-object p1, p0, Lcom/android/calendar/af;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 3252
    iput-object p2, p0, Lcom/android/calendar/af;->a:Ljava/lang/String;

    .line 3253
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Long;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 3

    .prologue
    .line 3257
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 3258
    iget-object v2, p0, Lcom/android/calendar/af;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;J)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 2

    .prologue
    .line 3264
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/af;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/af;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1}, Lcom/android/calendar/AllInOneActivity;->u(Lcom/android/calendar/AllInOneActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3265
    iget-object v0, p0, Lcom/android/calendar/af;->b:Lcom/android/calendar/AllInOneActivity;

    iget-object v0, v0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 3266
    iget-object v0, p0, Lcom/android/calendar/af;->b:Lcom/android/calendar/AllInOneActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    .line 3270
    :goto_0
    return-void

    .line 3268
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/af;->b:Lcom/android/calendar/AllInOneActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3247
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/android/calendar/af;->a([Ljava/lang/Long;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 3247
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0, p1}, Lcom/android/calendar/af;->a(Landroid/graphics/drawable/BitmapDrawable;)V

    return-void
.end method
