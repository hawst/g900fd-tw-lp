.class public Lcom/android/calendar/fk;
.super Landroid/widget/BaseAdapter;
.source "RecipientAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/android/ex/a/a;


# instance fields
.field private a:Lcom/android/calendar/fj;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Landroid/text/SpannableStringBuilder;

.field private e:Landroid/content/Context;

.field private f:Landroid/content/ContentResolver;

.field private g:Landroid/view/LayoutInflater;

.field private h:Landroid/accounts/Account;

.field private i:I

.field private j:Landroid/os/Handler;

.field private k:Ljava/util/LinkedHashMap;

.field private l:Ljava/util/List;

.field private m:Ljava/util/Set;

.field private n:Ljava/util/List;

.field private o:I

.field private p:Ljava/lang/CharSequence;

.field private q:Landroid/util/LruCache;

.field private final r:Lcom/android/calendar/fp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 512
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 445
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/fk;->j:Landroid/os/Handler;

    .line 507
    new-instance v0, Lcom/android/calendar/fp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/fp;-><init>(Lcom/android/calendar/fk;Lcom/android/calendar/fl;)V

    iput-object v0, p0, Lcom/android/calendar/fk;->r:Lcom/android/calendar/fp;

    .line 513
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/calendar/fk;->a(Landroid/content/Context;II)V

    .line 514
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/fk;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/fk;->i:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/fk;Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/fk;->a(Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 796
    iget-object v0, p0, Lcom/android/calendar/fk;->a:Lcom/android/calendar/fj;

    invoke-virtual {v0}, Lcom/android/calendar/fj;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    add-int/lit8 v2, p2, 0x5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 800
    if-eqz p3, :cond_0

    .line 801
    const-string v0, "directory"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 804
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/fk;->h:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    .line 805
    const-string v0, "name_for_primary_account"

    iget-object v2, p0, Lcom/android/calendar/fk;->h:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 806
    const-string v0, "type_for_primary_account"

    iget-object v2, p0, Lcom/android/calendar/fk;->h:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 808
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 809
    iget-object v0, p0, Lcom/android/calendar/fk;->f:Landroid/content/ContentResolver;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/fk;->a:Lcom/android/calendar/fj;

    invoke-virtual {v2}, Lcom/android/calendar/fj;->a()[Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 811
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 818
    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/fk;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/calendar/fk;->p:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/fk;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/calendar/fk;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/fk;Ljava/util/LinkedHashMap;)Ljava/util/LinkedHashMap;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/calendar/fk;->k:Ljava/util/LinkedHashMap;

    return-object p1
.end method

.method private a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 570
    iget-object v0, p0, Lcom/android/calendar/fk;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 571
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 572
    const/4 v1, 0x0

    .line 573
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 574
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 578
    const-wide/16 v8, 0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_0

    .line 582
    new-instance v0, Lcom/android/calendar/fs;

    invoke-direct {v0}, Lcom/android/calendar/fs;-><init>()V

    .line 583
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 584
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 585
    iput-wide v6, v0, Lcom/android/calendar/fs;->a:J

    .line 586
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/fs;->c:Ljava/lang/String;

    .line 587
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/fs;->d:Ljava/lang/String;

    .line 588
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/fs;->e:Ljava/lang/String;

    .line 589
    if-eqz v5, :cond_1

    if-eqz v8, :cond_1

    .line 591
    :try_start_0
    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 593
    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/fs;->b:Ljava/lang/String;

    .line 594
    iget-object v2, v0, Lcom/android/calendar/fs;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 595
    const-string v2, "RecipientAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot resolve directory name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "@"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 607
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/fk;->h:Landroid/accounts/Account;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/fk;->h:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, v0, Lcom/android/calendar/fs;->d:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/fk;->h:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v5, v0, Lcom/android/calendar/fs;->e:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    move-object v1, v0

    .line 613
    goto/16 :goto_0

    .line 598
    :catch_0
    move-exception v2

    .line 599
    const-string v6, "RecipientAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot resolve directory name: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "@"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 611
    :cond_2
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_2

    .line 615
    :cond_3
    if-eqz v1, :cond_4

    .line 616
    invoke-interface {v4, v10, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 619
    :cond_4
    return-object v4
.end method

.method static synthetic a(Lcom/android/calendar/fk;Landroid/database/Cursor;)Ljava/util/List;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/android/calendar/fk;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/fk;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/calendar/fk;->l:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/fk;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)Ljava/util/List;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/fk;->a(ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 691
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 693
    invoke-virtual {p2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 694
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 695
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    move v4, v2

    move v3, v1

    .line 696
    :goto_1
    if-ge v4, v7, :cond_0

    .line 697
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/ex/a/b;

    .line 698
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 699
    invoke-direct {p0, v1}, Lcom/android/calendar/fk;->a(Lcom/android/ex/a/b;)V

    .line 700
    add-int/lit8 v3, v3, 0x1

    .line 696
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 702
    :cond_0
    iget v0, p0, Lcom/android/calendar/fk;->i:I

    if-le v3, v0, :cond_2

    .line 706
    :goto_2
    iget v0, p0, Lcom/android/calendar/fk;->i:I

    if-gt v3, v0, :cond_1

    .line 707
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/a/b;

    .line 708
    iget v2, p0, Lcom/android/calendar/fk;->i:I

    if-le v3, v2, :cond_3

    .line 718
    :cond_1
    return-object v5

    :cond_2
    move v1, v3

    .line 705
    goto :goto_0

    .line 711
    :cond_3
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 712
    invoke-direct {p0, v0}, Lcom/android/calendar/fk;->a(Lcom/android/ex/a/b;)V

    .line 714
    add-int/lit8 v3, v3, 0x1

    .line 715
    goto :goto_3

    :cond_4
    move v3, v1

    goto :goto_2
.end method

.method static synthetic a(Lcom/android/calendar/fk;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/calendar/fk;->m:Ljava/util/Set;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/fk;Lcom/android/calendar/fu;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct/range {p0 .. p5}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fu;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/fk;Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/fk;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    return-void
.end method

.method private a(Lcom/android/calendar/fu;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V
    .locals 15

    .prologue
    .line 650
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/fu;->b:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 679
    :goto_0
    return-void

    .line 654
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/fu;->b:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 656
    if-nez p2, :cond_1

    .line 657
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/fu;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/fu;->h:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/android/calendar/fu;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/calendar/fu;->c:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/fu;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/fu;->e:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/fu;->f:J

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/android/calendar/fu;->g:Ljava/lang/String;

    const/4 v13, 0x1

    const/4 v14, 0x1

    invoke-static/range {v3 .. v14}, Lcom/android/ex/a/b;->a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;JJLjava/lang/String;ZZ)Lcom/android/ex/a/b;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 662
    :cond_1
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/fu;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 664
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/fu;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 665
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/fu;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/fu;->h:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/android/calendar/fu;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/calendar/fu;->c:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/fu;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/fu;->e:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/fu;->f:J

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/android/calendar/fu;->g:Ljava/lang/String;

    const/4 v13, 0x1

    const/4 v14, 0x1

    invoke-static/range {v3 .. v14}, Lcom/android/ex/a/b;->b(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;JJLjava/lang/String;ZZ)Lcom/android/ex/a/b;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 671
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 672
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/fu;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/fu;->h:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/android/calendar/fu;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/calendar/fu;->c:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/fu;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/fu;->e:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/fu;->f:J

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/android/calendar/fu;->g:Ljava/lang/String;

    const/4 v13, 0x1

    const/4 v14, 0x1

    invoke-static/range {v3 .. v14}, Lcom/android/ex/a/b;->a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;JJLjava/lang/String;ZZ)Lcom/android/ex/a/b;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 677
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/fu;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private a(Lcom/android/ex/a/b;)V
    .locals 2

    .prologue
    .line 728
    invoke-virtual {p1}, Lcom/android/ex/a/b;->g()Landroid/net/Uri;

    move-result-object v1

    .line 729
    if-eqz v1, :cond_0

    .line 730
    iget-object v0, p0, Lcom/android/calendar/fk;->q:Landroid/util/LruCache;

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 731
    if-eqz v0, :cond_1

    .line 732
    invoke-virtual {p1, v0}, Lcom/android/ex/a/b;->a([B)V

    .line 742
    :cond_0
    :goto_0
    return-void

    .line 739
    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/fk;->a(Lcom/android/ex/a/b;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private a(Lcom/android/ex/a/b;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 745
    new-instance v0, Lcom/android/calendar/fl;

    invoke-direct {v0, p0, p2, p1}, Lcom/android/calendar/fl;-><init>(Lcom/android/calendar/fk;Landroid/net/Uri;Lcom/android/ex/a/b;)V

    .line 771
    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 772
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 4

    .prologue
    .line 628
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 630
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 631
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/fs;

    .line 632
    iput-object p1, v0, Lcom/android/calendar/fs;->f:Ljava/lang/CharSequence;

    .line 633
    iget-object v3, v0, Lcom/android/calendar/fs;->g:Lcom/android/calendar/fq;

    if-nez v3, :cond_0

    .line 634
    new-instance v3, Lcom/android/calendar/fq;

    invoke-direct {v3, p0, v0}, Lcom/android/calendar/fq;-><init>(Lcom/android/calendar/fk;Lcom/android/calendar/fs;)V

    iput-object v3, v0, Lcom/android/calendar/fs;->g:Lcom/android/calendar/fq;

    .line 636
    :cond_0
    iget-object v3, v0, Lcom/android/calendar/fs;->g:Lcom/android/calendar/fq;

    invoke-virtual {v3, p3}, Lcom/android/calendar/fq;->a(I)V

    .line 637
    iget-object v0, v0, Lcom/android/calendar/fs;->g:Lcom/android/calendar/fq;

    invoke-virtual {v0, p1}, Lcom/android/calendar/fq;->filter(Ljava/lang/CharSequence;)V

    .line 630
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 642
    :cond_1
    add-int/lit8 v0, v2, -0x1

    iput v0, p0, Lcom/android/calendar/fk;->o:I

    .line 643
    iget-object v0, p0, Lcom/android/calendar/fk;->r:Lcom/android/calendar/fp;

    invoke-virtual {v0}, Lcom/android/calendar/fp;->a()V

    .line 644
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 723
    iput-object p1, p0, Lcom/android/calendar/fk;->n:Ljava/util/List;

    .line 724
    invoke-virtual {p0}, Lcom/android/calendar/fk;->notifyDataSetChanged()V

    .line 725
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/fk;)Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->f:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/fk;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/android/calendar/fk;->a(Ljava/util/List;)V

    return-void
.end method

.method static synthetic c(Lcom/android/calendar/fk;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/fk;)Lcom/android/calendar/fp;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->r:Lcom/android/calendar/fp;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/fk;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->p:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/fk;)Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->k:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/fk;)Ljava/util/List;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/fk;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->m:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/fk;)I
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/fk;->o:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/calendar/fk;->o:I

    return v0
.end method

.method static synthetic j(Lcom/android/calendar/fk;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/fk;->o:I

    return v0
.end method

.method static synthetic k(Lcom/android/calendar/fk;)Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->q:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/fk;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/fk;->j:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 945
    const v0, 0x7f04001f

    return v0
.end method

.method public a(Landroid/content/Context;II)V
    .locals 3

    .prologue
    .line 529
    iput-object p1, p0, Lcom/android/calendar/fk;->e:Landroid/content/Context;

    .line 530
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/fk;->f:Landroid/content/ContentResolver;

    .line 531
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/fk;->g:Landroid/view/LayoutInflater;

    .line 532
    iput p2, p0, Lcom/android/calendar/fk;->i:I

    .line 533
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/fk;->q:Landroid/util/LruCache;

    .line 534
    iput p3, p0, Lcom/android/calendar/fk;->b:I

    .line 535
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    .line 536
    if-nez p3, :cond_0

    .line 537
    sget-object v0, Lcom/android/calendar/fg;->b:Lcom/android/calendar/fj;

    iput-object v0, p0, Lcom/android/calendar/fk;->a:Lcom/android/calendar/fj;

    .line 544
    :goto_0
    return-void

    .line 538
    :cond_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    .line 539
    sget-object v0, Lcom/android/calendar/fg;->a:Lcom/android/calendar/fj;

    iput-object v0, p0, Lcom/android/calendar/fk;->a:Lcom/android/calendar/fj;

    goto :goto_0

    .line 541
    :cond_1
    sget-object v0, Lcom/android/calendar/fg;->b:Lcom/android/calendar/fj;

    iput-object v0, p0, Lcom/android/calendar/fk;->a:Lcom/android/calendar/fj;

    .line 542
    const-string v0, "RecipientAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported query type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 953
    const v0, 0x7f020100

    return v0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 961
    const v0, 0x1020016

    return v0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 970
    const v0, 0x1020014

    return v0
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 978
    const v0, 0x1020015

    return v0
.end method

.method protected f()I
    .locals 1

    .prologue
    .line 986
    const v0, 0x1020006

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lcom/android/calendar/fk;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/fk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 566
    new-instance v0, Lcom/android/calendar/fn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/fn;-><init>(Lcom/android/calendar/fk;Lcom/android/calendar/fl;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 837
    iget-object v0, p0, Lcom/android/calendar/fk;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 842
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 852
    iget-object v0, p0, Lcom/android/calendar/fk;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/a/b;

    invoke-virtual {v0}, Lcom/android/ex/a/b;->a()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    .line 862
    iget-object v0, p0, Lcom/android/calendar/fk;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/a/b;

    .line 863
    invoke-virtual {v0}, Lcom/android/ex/a/b;->b()Ljava/lang/String;

    move-result-object v2

    .line 864
    invoke-virtual {v0}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v1

    .line 865
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 870
    :cond_0
    invoke-virtual {v0}, Lcom/android/ex/a/b;->f()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 871
    const/4 v2, 0x0

    move-object v5, v2

    move-object v6, v1

    .line 875
    :goto_0
    if-eqz p2, :cond_5

    .line 877
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/fk;->c()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 879
    invoke-virtual {p0}, Lcom/android/calendar/fk;->d()I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 880
    invoke-virtual {p0}, Lcom/android/calendar/fk;->e()I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 882
    invoke-virtual {p0}, Lcom/android/calendar/fk;->f()I

    move-result v4

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 883
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 885
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/android/calendar/fk;->c:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 886
    iget-object v7, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 887
    iget-object v7, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    const/4 v8, 0x0

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 888
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/calendar/fk;->c:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 889
    iget-object v7, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    const v9, -0xffff01

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v9, p0, Lcom/android/calendar/fk;->c:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v6

    const/16 v10, 0x21

    invoke-virtual {v7, v8, v6, v9, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 890
    iget-object v6, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 893
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 894
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 895
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 896
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/calendar/fk;->c:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 897
    iget-object v6, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 898
    iget-object v6, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    const/4 v7, 0x0

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 899
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/calendar/fk;->c:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 900
    iget-object v6, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    const v8, -0xffff01

    invoke-direct {v7, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v8, p0, Lcom/android/calendar/fk;->c:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v5

    const/16 v9, 0x21

    invoke-virtual {v6, v7, v5, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 901
    iget-object v5, p0, Lcom/android/calendar/fk;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 907
    :cond_2
    :goto_2
    if-eqz v3, :cond_3

    .line 908
    iget-object v2, p0, Lcom/android/calendar/fk;->a:Lcom/android/calendar/fj;

    iget-object v5, p0, Lcom/android/calendar/fk;->e:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0}, Lcom/android/ex/a/b;->d()I

    move-result v6

    invoke-virtual {v0}, Lcom/android/ex/a/b;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v5, v6, v7}, Lcom/android/calendar/fj;->a(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 912
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 915
    :cond_3
    invoke-virtual {v0}, Lcom/android/ex/a/b;->f()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 916
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 917
    if-eqz v4, :cond_4

    .line 918
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 919
    invoke-virtual {v0}, Lcom/android/ex/a/b;->h()[B

    move-result-object v0

    .line 920
    if-eqz v0, :cond_7

    if-eqz v4, :cond_7

    .line 921
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 923
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 934
    :cond_4
    :goto_3
    return-object p2

    .line 875
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/fk;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/android/calendar/fk;->a()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_1

    .line 904
    :cond_6
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 905
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 925
    :cond_7
    invoke-virtual {p0}, Lcom/android/calendar/fk;->b()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 929
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 930
    if-eqz v4, :cond_4

    .line 931
    const/4 v0, 0x4

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    :cond_9
    move-object v5, v1

    move-object v6, v1

    goto/16 :goto_0

    :cond_a
    move-object v5, v1

    move-object v6, v2

    goto/16 :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 847
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 857
    iget-object v0, p0, Lcom/android/calendar/fk;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/a/b;

    invoke-virtual {v0}, Lcom/android/ex/a/b;->i()Z

    move-result v0

    return v0
.end method
