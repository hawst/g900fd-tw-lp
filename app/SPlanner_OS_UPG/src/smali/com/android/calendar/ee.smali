.class Lcom/android/calendar/ee;
.super Ljava/lang/Object;
.source "FestivalEffectManager.java"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 247
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "kor_holiday_new_years_day"

    aput-object v1, v0, v3

    const-string v1, "kor_holiday_lunar_newyear"

    aput-object v1, v0, v4

    const-string v1, "kor_holiday_buddha"

    aput-object v1, v0, v5

    const-string v1, "kor_holiday_korean_thanksgiving"

    aput-object v1, v0, v6

    const-string v1, "kor_holiday_christmas"

    aput-object v1, v0, v7

    sput-object v0, Lcom/android/calendar/ee;->a:[Ljava/lang/String;

    .line 253
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "splanner_monthlist_icon_newyearsday_kor"

    aput-object v1, v0, v3

    const-string v1, "splanner_monthlist_icon_lunar_newyear_kor"

    aput-object v1, v0, v4

    const-string v1, "splanner_monthlist_icon_buddha_kor"

    aput-object v1, v0, v5

    const-string v1, "splanner_monthlist_icon_korean_thanksgiving_kor"

    aput-object v1, v0, v6

    const-string v1, "splanner_monthlist_icon_christmas_kor"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "splanner_monthlist_icon_birthday_kor"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/ee;->b:[Ljava/lang/String;

    .line 260
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "chn_holiday_new_years_day"

    aput-object v1, v0, v3

    const-string v1, "chn_holiday_chinese_new_years_day"

    aput-object v1, v0, v4

    const-string v1, "chn_holiday_valentines_day"

    aput-object v1, v0, v5

    const-string v1, "chn_holiday_double_seventh_day"

    aput-object v1, v0, v6

    const-string v1, "chn_holiday_lantern_festival"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "chn_holiday_dragon_boat_festival"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "chn_holiday_mid_autumn_festival"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "chn_holiday_christmas"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/ee;->c:[Ljava/lang/String;

    .line 267
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "splanner_monthlist_icon_new_year"

    aput-object v1, v0, v3

    const-string v1, "splanner_monthlist_icon_chinese_new_year"

    aput-object v1, v0, v4

    const-string v1, "splanner_monthlist_icon_valentine"

    aput-object v1, v0, v5

    const-string v1, "splanner_monthlist_icon_chn_valentine"

    aput-object v1, v0, v6

    const-string v1, "splanner_monthlist_icon_lantern"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "splanner_monthlist_icon_dragon_boat"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "splanner_monthlist_icon_mid_autumn"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "splanner_monthlist_icon_christmas"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "splanner_monthlist_icon_birthday"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/ee;->d:[Ljava/lang/String;

    .line 275
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "splanner_monthlist_icon_new_year_set2"

    aput-object v1, v0, v3

    const-string v1, "splanner_monthlist_icon_chinese_new_year_set2"

    aput-object v1, v0, v4

    const-string v1, "splanner_monthlist_icon_valentine_set2"

    aput-object v1, v0, v5

    const-string v1, "splanner_monthlist_icon_chn_valentine_set2"

    aput-object v1, v0, v6

    const-string v1, "splanner_monthlist_icon_lantern_set2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "splanner_monthlist_icon_dragon_boat_set2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "splanner_monthlist_icon_mid_autumn_set2"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "splanner_monthlist_icon_christmas_set2"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "splanner_monthlist_icon_birthday_set2"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/ee;->e:[Ljava/lang/String;

    .line 284
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "jpn_holiday_new_year_day"

    aput-object v1, v0, v3

    const-string v1, "jpn_holiday_boys_festival"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/ee;->f:[Ljava/lang/String;

    .line 288
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "splanner_monthlist_icon_jpn_new_year"

    aput-object v1, v0, v3

    const-string v1, "splanner_monthlist_icon_jpn_boysday"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/ee;->g:[Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 230
    invoke-static {}, Lcom/android/calendar/dz;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    sget-object v0, Lcom/android/calendar/ee;->b:[Ljava/lang/String;

    aget-object v0, v0, p1

    .line 243
    :goto_0
    return-object v0

    .line 232
    :cond_0
    invoke-static {}, Lcom/android/calendar/dz;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    sget-object v0, Lcom/android/calendar/ee;->g:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    .line 236
    :cond_1
    invoke-static {p0}, Lcom/android/calendar/dz;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    .line 237
    const-string v1, "FestivalEffectManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCurrentThemeName is ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    const-string v1, "com.bst.festivalrespreload1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    sget-object v0, Lcom/android/calendar/ee;->d:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0

    .line 243
    :cond_2
    sget-object v0, Lcom/android/calendar/ee;->e:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public static a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    invoke-static {}, Lcom/android/calendar/dz;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    sget-object v0, Lcom/android/calendar/ee;->f:[Ljava/lang/String;

    .line 226
    :goto_0
    return-object v0

    .line 223
    :cond_0
    invoke-static {}, Lcom/android/calendar/dz;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    sget-object v0, Lcom/android/calendar/ee;->a:[Ljava/lang/String;

    goto :goto_0

    .line 226
    :cond_1
    sget-object v0, Lcom/android/calendar/ee;->c:[Ljava/lang/String;

    goto :goto_0
.end method
