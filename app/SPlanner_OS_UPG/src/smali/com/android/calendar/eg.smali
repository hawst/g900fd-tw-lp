.class public Lcom/android/calendar/eg;
.super Landroid/app/DialogFragment;
.source "GotoDialogFragment.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Lcom/android/calendar/al;

.field private d:Landroid/os/Handler;

.field private e:Lcom/android/calendar/ei;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/android/calendar/eg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/eg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/eg;->d:Landroid/os/Handler;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/eg;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/calendar/eg;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private a(III)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/calendar/eg;->e:Lcom/android/calendar/ei;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/android/calendar/ei;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/ei;-><init>(Lcom/android/calendar/eg;Lcom/android/calendar/eh;)V

    iput-object v0, p0, Lcom/android/calendar/eg;->e:Lcom/android/calendar/ei;

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/eg;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/eg;->e:Lcom/android/calendar/ei;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 86
    iget-object v0, p0, Lcom/android/calendar/eg;->e:Lcom/android/calendar/ei;

    iput p1, v0, Lcom/android/calendar/ei;->a:I

    .line 87
    iget-object v0, p0, Lcom/android/calendar/eg;->e:Lcom/android/calendar/ei;

    iput p2, v0, Lcom/android/calendar/ei;->b:I

    .line 88
    iget-object v0, p0, Lcom/android/calendar/eg;->e:Lcom/android/calendar/ei;

    iput p3, v0, Lcom/android/calendar/ei;->c:I

    .line 89
    iget-object v0, p0, Lcom/android/calendar/eg;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/eg;->e:Lcom/android/calendar/ei;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 90
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/eg;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/calendar/eg;->c:Lcom/android/calendar/al;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 50
    iput-object p1, p0, Lcom/android/calendar/eg;->b:Landroid/app/Activity;

    .line 51
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/eg;->c:Lcom/android/calendar/al;

    .line 52
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/android/calendar/eg;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    :goto_0
    return-object v0

    .line 59
    :cond_0
    new-instance v2, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/eg;->b:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/android/calendar/eg;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 63
    iget-object v0, p0, Lcom/android/calendar/eg;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f03ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    .line 65
    new-instance v0, Lcom/android/calendar/common/extension/a;

    iget-object v1, p0, Lcom/android/calendar/eg;->b:Landroid/app/Activity;

    iget v3, v2, Landroid/text/format/Time;->year:I

    iget v4, v2, Landroid/text/format/Time;->month:I

    iget v5, v2, Landroid/text/format/Time;->monthDay:I

    move-object v2, p0

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/common/extension/a;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;IIILjava/lang/String;ZZ)V

    .line 68
    instance-of v1, v0, Landroid/content/DialogInterface$OnClickListener;

    if-eqz v1, :cond_1

    .line 69
    const/4 v2, -0x1

    iget-object v1, p0, Lcom/android/calendar/eg;->b:Landroid/app/Activity;

    const v3, 0x7f0f0210

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v1, v0

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 72
    :cond_1
    invoke-virtual {v0, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p2, p3, p4}, Lcom/android/calendar/eg;->a(III)V

    .line 79
    return-void
.end method
