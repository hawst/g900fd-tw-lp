.class Lcom/android/calendar/gi;
.super Ljava/lang/Object;
.source "SDImportExportDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ArrayAdapter;

.field final synthetic b:Lcom/android/calendar/gg;


# direct methods
.method constructor <init>(Lcom/android/calendar/gg;Landroid/widget/ArrayAdapter;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/android/calendar/gi;->b:Lcom/android/calendar/gg;

    iput-object p2, p0, Lcom/android/calendar/gi;->a:Landroid/widget/ArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 103
    iget-object v0, p0, Lcom/android/calendar/gi;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 104
    sparse-switch v0, :sswitch_data_0

    .line 131
    invoke-static {}, Lcom/android/calendar/gg;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    const-string v1, "SDImportExportDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/gi;->b:Lcom/android/calendar/gg;

    invoke-virtual {v3}, Lcom/android/calendar/gg;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 139
    return-void

    .line 107
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/gi;->b:Lcom/android/calendar/gg;

    invoke-static {v0}, Lcom/android/calendar/gg;->a(Lcom/android/calendar/gg;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/32 v2, 0x400000

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    .line 113
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/gi;->b:Lcom/android/calendar/gg;

    invoke-static {v0}, Lcom/android/calendar/gg;->a(Lcom/android/calendar/gg;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/32 v2, 0x800000

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    .line 119
    :sswitch_2
    iget-object v0, p0, Lcom/android/calendar/gi;->b:Lcom/android/calendar/gg;

    invoke-static {v0}, Lcom/android/calendar/gg;->a(Lcom/android/calendar/gg;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/32 v2, 0x1000000

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    .line 125
    :sswitch_3
    iget-object v0, p0, Lcom/android/calendar/gi;->b:Lcom/android/calendar/gg;

    invoke-static {v0}, Lcom/android/calendar/gg;->a(Lcom/android/calendar/gg;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/32 v2, 0x2000000

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    .line 104
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f01e4 -> :sswitch_3
        0x7f0f01e5 -> :sswitch_2
        0x7f0f0240 -> :sswitch_1
        0x7f0f0241 -> :sswitch_0
    .end sparse-switch
.end method
