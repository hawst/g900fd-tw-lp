.class public Lcom/android/calendar/CalendarSearchActivity;
.super Lcom/android/calendar/b;
.source "CalendarSearchActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Landroid/widget/SearchView$OnSuggestionListener;
.implements Lcom/android/calendar/ap;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Lcom/android/calendar/al;

.field private d:Landroid/content/ContentResolver;

.field private e:Ljava/lang/String;

.field private f:Lcom/android/calendar/CalendarSearchView;

.field private g:I

.field private h:J

.field private i:Ljava/lang/String;

.field private j:I

.field private k:I

.field private l:Landroid/app/Activity;

.field private final m:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/android/calendar/CalendarSearchActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/CalendarSearchActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    .line 417
    new-instance v0, Lcom/android/calendar/av;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/av;-><init>(Lcom/android/calendar/CalendarSearchActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->m:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/CalendarSearchActivity;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    return-object v0
.end method

.method private a(JLjava/lang/String;)V
    .locals 5

    .prologue
    const v3, 0x7f12006b

    .line 224
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 227
    new-instance v1, Lcom/android/calendar/agenda/z;

    const/4 v2, 0x1

    invoke-direct {v1, p1, p2, v2}, Lcom/android/calendar/agenda/z;-><init>(JZ)V

    .line 228
    invoke-virtual {v0, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 229
    iget-object v2, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v2, v3, v1}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 231
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 232
    invoke-direct {p0, p3}, Lcom/android/calendar/CalendarSearchActivity;->a(Ljava/lang/String;)V

    .line 233
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 407
    if-eqz p1, :cond_0

    .line 408
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 409
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    .line 411
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 251
    iput-object p1, p0, Lcom/android/calendar/CalendarSearchActivity;->e:Ljava/lang/String;

    .line 252
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    const-wide/16 v2, 0x100

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v12

    move-object v1, p0

    move-object v5, v4

    move-object v11, p1

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 254
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 202
    const v1, 0x7f040083

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 203
    const v0, 0x7f120045

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarSearchView;

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    .line 205
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0, p0}, Lcom/android/calendar/CalendarSearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 208
    :cond_0
    iget v0, p0, Lcom/android/calendar/CalendarSearchActivity;->g:I

    if-nez v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->getInputType()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/CalendarSearchActivity;->g:I

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    .line 213
    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 214
    const/4 v0, 0x0

    .line 218
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    if-eqz v1, :cond_2

    .line 219
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/CalendarSearchView;->setInputType(I)V

    .line 221
    :cond_2
    return-void

    .line 216
    :cond_3
    iget v0, p0, Lcom/android/calendar/CalendarSearchActivity;->g:I

    goto :goto_0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 316
    if-nez v0, :cond_0

    .line 320
    :goto_0
    return-void

    .line 319
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 311
    iput p1, p0, Lcom/android/calendar/CalendarSearchActivity;->k:I

    .line 312
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 395
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide v2, 0x200000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p1, Lcom/android/calendar/aq;->p:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-direct {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->a(Landroid/view/View;)V

    .line 398
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->b(I)V

    .line 400
    :cond_0
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 390
    const-wide v0, 0x200000000L

    return-wide v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 268
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 269
    const/4 v0, -0x1

    if-eq p2, v0, :cond_4

    .line 270
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 271
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0, v2}, Lcom/android/calendar/al;->a(I)V

    .line 273
    invoke-direct {p0, v3}, Lcom/android/calendar/CalendarSearchActivity;->b(I)V

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    iget v1, p0, Lcom/android/calendar/CalendarSearchActivity;->k:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(I)V

    .line 276
    iget v0, p0, Lcom/android/calendar/CalendarSearchActivity;->k:I

    if-ne v0, v2, :cond_2

    .line 277
    invoke-direct {p0, v3}, Lcom/android/calendar/CalendarSearchActivity;->b(I)V

    goto :goto_0

    .line 278
    :cond_2
    iget v0, p0, Lcom/android/calendar/CalendarSearchActivity;->k:I

    if-ne v0, v3, :cond_0

    .line 279
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->i:Ljava/lang/String;

    invoke-static {v0, v4, v4}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 280
    if-nez v0, :cond_3

    .line 281
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/calendar/CalendarSearchView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0, v2}, Lcom/android/calendar/al;->a(I)V

    .line 283
    invoke-direct {p0, v3}, Lcom/android/calendar/CalendarSearchActivity;->b(I)V

    goto :goto_0

    .line 285
    :cond_3
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->b(I)V

    goto :goto_0

    .line 292
    :cond_4
    const/16 v0, 0x67

    if-ne p1, v0, :cond_0

    .line 293
    const-string v0, "_id"

    invoke-virtual {p3, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    .line 294
    const-string v0, "filepath"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->i:Ljava/lang/String;

    .line 295
    const-string v0, "sticker_group"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/CalendarSearchActivity;->j:I

    .line 296
    iget-wide v0, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_5

    .line 297
    iget-wide v0, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 298
    invoke-direct {p0, v2}, Lcom/android/calendar/CalendarSearchActivity;->b(I)V

    .line 299
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarSearchView;->requestFocus()Z

    .line 300
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    iget-wide v2, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    iget-object v4, p0, Lcom/android/calendar/CalendarSearchActivity;->i:Ljava/lang/String;

    iget v5, p0, Lcom/android/calendar/CalendarSearchActivity;->j:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/calendar/CalendarSearchView;->a(JLjava/lang/String;I)V

    .line 301
    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->onQueryTextChange(Ljava/lang/String;)Z

    goto :goto_0

    .line 303
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/calendar/CalendarSearchView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0, v2}, Lcom/android/calendar/al;->a(I)V

    .line 305
    invoke-direct {p0, v3}, Lcom/android/calendar/CalendarSearchActivity;->b(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 165
    sget-object v0, Lcom/android/calendar/CalendarSearchActivity;->b:Ljava/lang/String;

    const-string v1, "CalendarSearchActivity, onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 168
    iget v0, p1, Landroid/content/res/Configuration;->densityDpi:I

    mul-int/lit8 v0, v0, 0x4

    div-int/lit16 v1, v0, 0xa0

    .line 170
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 171
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 172
    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    sub-int v1, v2, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 173
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/CalendarSearchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarSearchView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v2}, Lcom/android/calendar/CalendarSearchView;->getPaddingRight()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Lcom/android/calendar/CalendarSearchView;->setPadding(IIII)V

    .line 181
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 177
    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    add-int/2addr v2, v1

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 178
    iget-object v2, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/CalendarSearchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    iget-object v2, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v2}, Lcom/android/calendar/CalendarSearchView;->getPaddingLeft()I

    move-result v2

    div-int/lit8 v3, v1, 0x2

    iget-object v4, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v4}, Lcom/android/calendar/CalendarSearchView;->getPaddingRight()I

    move-result v4

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/android/calendar/CalendarSearchView;->setPadding(IIII)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 82
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 83
    iput-object p0, p0, Lcom/android/calendar/CalendarSearchActivity;->l:Landroid/app/Activity;

    .line 85
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 87
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v4, v4, 0x100

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 88
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 89
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 90
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 92
    invoke-static {p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    .line 93
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->d:Landroid/content/ContentResolver;

    .line 94
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->setContentView(I)V

    .line 95
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->setDefaultKeyMode(I)V

    .line 96
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 97
    const-string v0, "key_time"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    const-string v1, "key_time"

    invoke-virtual {v4, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/android/calendar/al;->a(J)V

    .line 101
    :cond_0
    if-eqz p1, :cond_3

    .line 103
    const-string v0, "key_restore_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 105
    :goto_0
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 106
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    .line 108
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/CalendarSearchActivity;->b()V

    .line 109
    const-string v2, "android.intent.action.SEARCH"

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 110
    const-string v2, "query"

    invoke-virtual {v4, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 111
    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/CalendarSearchActivity;->a(JLjava/lang/String;)V

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->requestFocus()Z

    .line 114
    return-void

    :cond_3
    move-wide v0, v2

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 119
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 121
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 122
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 123
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/CalendarSearchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->a()V

    .line 158
    invoke-static {p0}, Lcom/android/calendar/al;->b(Landroid/content/Context;)V

    .line 159
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/CalendarSearchView;->setCursorVisibility(Z)V

    .line 160
    invoke-super {p0}, Lcom/android/calendar/b;->onDestroy()V

    .line 161
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 185
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 187
    const v1, 0x7f120347

    if-ne v0, v1, :cond_0

    .line 188
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/CalendarSearchActivity;->k:I

    .line 189
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(I)V

    .line 190
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-direct {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->a(Landroid/view/View;)V

    .line 191
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 192
    const-class v1, Lcom/android/calendar/event/SelectStickerActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 193
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 194
    const-string v1, "caller"

    const-string v2, "search"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const/16 v1, 0x67

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/CalendarSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 197
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    const v1, 0x7f120045

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(Ljava/lang/Integer;)V

    .line 151
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->d:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->m:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 152
    invoke-super {p0}, Lcom/android/calendar/b;->onPause()V

    .line 153
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const-string v0, "0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 244
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/CalendarSearchActivity;->a(Ljava/lang/String;)V

    .line 246
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x4

    .line 345
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 346
    const-string v0, "key_search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    const-string v0, "key_search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 348
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 349
    if-ne v0, v2, :cond_0

    .line 350
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/calendar/CalendarSearchView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(I)V

    .line 359
    :cond_1
    :goto_0
    const-string v0, "key_previous_search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 360
    const-string v0, "key_previous_search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/CalendarSearchActivity;->k:I

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    if-eqz v0, :cond_3

    const-string v0, "key_search_view"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "key_search_view"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 363
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    const-string v1, "key_search_view"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/CalendarSearchView;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 366
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    .line 367
    if-ne v0, v2, :cond_4

    .line 368
    const-string v0, "sticker_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "sticker_path"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 370
    const-string v0, "sticker_id"

    invoke-virtual {p1, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    .line 371
    const-string v0, "sticker_path"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->i:Ljava/lang/String;

    .line 372
    const-string v0, "sticker_group"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/CalendarSearchActivity;->j:I

    .line 373
    iget-wide v0, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    if-eqz v0, :cond_4

    .line 374
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    iget-wide v2, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->i:Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/CalendarSearchActivity;->j:I

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/android/calendar/CalendarSearchView;->a(JLjava/lang/String;I)V

    .line 375
    iget-wide v0, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 376
    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->onQueryTextChange(Ljava/lang/String;)Z

    .line 380
    :cond_4
    return-void

    .line 356
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v1, v0}, Lcom/android/calendar/al;->a(I)V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 130
    invoke-super {p0}, Lcom/android/calendar/b;->onResume()V

    .line 132
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    const v1, 0x7f120045

    invoke-virtual {v0, v1, p0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 133
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/calendar/CalendarSearchActivity;->m:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 134
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->l:Landroid/app/Activity;

    const v1, 0x7f10000a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTheme(I)V

    .line 135
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0, v3}, Lcom/android/calendar/al;->a(I)V

    .line 137
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->requestFocus()Z

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 141
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/calendar/CalendarSearchActivity;->b(I)V

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->invalidateOptionsMenu()V

    .line 145
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gx;->a(Landroid/content/ContentResolver;)V

    .line 146
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 324
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    .line 325
    const-string v1, "key_search_mode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 326
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 327
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    const-string v0, "sticker_id"

    iget-wide v2, p0, Lcom/android/calendar/CalendarSearchActivity;->h:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 329
    const-string v0, "sticker_path"

    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v0, "sticker_group"

    iget v1, p0, Lcom/android/calendar/CalendarSearchActivity;->j:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 333
    :cond_0
    const-string v0, "key_previous_search_mode"

    iget v1, p0, Lcom/android/calendar/CalendarSearchActivity;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 335
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    if-eqz v0, :cond_1

    .line 336
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 337
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/CalendarSearchView;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 338
    const-string v1, "key_search_view"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 340
    :cond_1
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 341
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchActivity;->f:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSearchView;->requestFocus()Z

    .line 385
    invoke-super {p0}, Lcom/android/calendar/b;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method public onSuggestionClick(I)Z
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    return v0
.end method

.method public onSuggestionSelect(I)Z
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return v0
.end method
