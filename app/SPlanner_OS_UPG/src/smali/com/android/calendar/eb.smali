.class public Lcom/android/calendar/eb;
.super Ljava/lang/Object;
.source "FestivalEffectManager.java"


# static fields
.field private static a:Lcom/android/calendar/eb;

.field private static final b:Ljava/lang/Object;

.field private static c:Ljava/util/HashMap;

.field private static e:J

.field private static final f:[Ljava/lang/String;

.field private static final g:Landroid/net/Uri;


# instance fields
.field private final d:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/eb;->a:Lcom/android/calendar/eb;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/calendar/eb;->b:Ljava/lang/Object;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/calendar/eb;->c:Ljava/util/HashMap;

    .line 301
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/calendar/eb;->e:J

    .line 303
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "startDay"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/eb;->f:[Ljava/lang/String;

    .line 307
    const-string v0, "content://com.sec.android.chinaholiday/holidays_with_workingday"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/eb;->g:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/eb;->d:Ljava/util/ArrayList;

    .line 58
    return-void
.end method

.method private a(Landroid/content/ContentResolver;J)J
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 163
    const-wide/16 v8, 0x0

    .line 164
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 167
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "contact_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "contactEventType"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 171
    if-nez v2, :cond_1

    .line 172
    const-wide/16 v0, -0x1

    .line 181
    if-eqz v2, :cond_0

    .line 182
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 185
    :cond_0
    :goto_0
    return-wide v0

    .line 175
    :cond_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 176
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 177
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 181
    :goto_1
    if-eqz v2, :cond_0

    .line 182
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 181
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_2

    .line 182
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 181
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :cond_3
    move-wide v0, v8

    goto :goto_1
.end method

.method private a(Landroid/content/Context;)Landroid/content/res/Resources;
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 70
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.eventnotification"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    .line 71
    :catch_0
    move-exception v1

    .line 72
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    .line 133
    sget-object v1, Lcom/android/calendar/eb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_0
    sget-object v0, Lcom/android/calendar/eb;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 135
    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable$ConstantState;

    .line 137
    if-eqz v0, :cond_0

    .line 138
    const-string v2, "FestivalEffectManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Get cached drawable state for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    monitor-exit v1

    .line 152
    :goto_0
    return-object v0

    .line 148
    :cond_0
    sget-object v0, Lcom/android/calendar/eb;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    :cond_1
    monitor-exit v1

    .line 152
    const/4 v0, 0x0

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static declared-synchronized a()Lcom/android/calendar/eb;
    .locals 2

    .prologue
    .line 61
    const-class v1, Lcom/android/calendar/eb;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/calendar/eb;->a:Lcom/android/calendar/eb;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/android/calendar/eb;

    invoke-direct {v0}, Lcom/android/calendar/eb;-><init>()V

    sput-object v0, Lcom/android/calendar/eb;->a:Lcom/android/calendar/eb;

    .line 64
    :cond_0
    sget-object v0, Lcom/android/calendar/eb;->a:Lcom/android/calendar/eb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 359
    if-nez p1, :cond_1

    .line 360
    const-string v0, "FestivalEffectManager"

    const-string v1, "china holiday cursor is null"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 365
    :cond_1
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 366
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 371
    :cond_2
    new-instance v0, Lcom/android/calendar/ed;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/ed;-><init>(Lcom/android/calendar/eb;Lcom/android/calendar/ec;)V

    .line 372
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/ed;->a:I

    .line 373
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/ed;->b:Ljava/lang/String;

    .line 374
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/ed;->c:I

    .line 375
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/ed;->d:I

    .line 377
    iget-object v1, p0, Lcom/android/calendar/eb;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 381
    :cond_3
    if-eqz p1, :cond_0

    .line 382
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 381
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_4

    .line 382
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method static synthetic a(Lcom/android/calendar/eb;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/android/calendar/eb;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 156
    sget-object v1, Lcom/android/calendar/eb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 157
    :try_start_0
    sget-object v0, Lcom/android/calendar/eb;->c:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    monitor-exit v1

    .line 160
    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(J)Z
    .locals 3

    .prologue
    .line 190
    const-wide/16 v0, 0x3

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v1, -0x1

    .line 78
    invoke-direct {p0, p1}, Lcom/android/calendar/eb;->a(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v2

    .line 79
    if-nez v2, :cond_1

    move v0, v1

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 84
    :cond_1
    invoke-static {}, Lcom/android/calendar/ee;->a()[Ljava/lang/String;

    move-result-object v3

    .line 85
    const/4 v0, 0x0

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_3

    .line 86
    aget-object v4, v3, v0

    const-string v5, "string"

    const-string v6, "com.sec.android.app.eventnotification"

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 87
    if-eqz v4, :cond_2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 85
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 92
    goto :goto_0
.end method

.method private c(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-direct {p0, p1}, Lcom/android/calendar/eb;->a(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v1

    .line 97
    if-nez v1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-object v0

    .line 103
    :cond_1
    const-string v2, "drawable"

    const-string v3, "com.sec.android.app.eventnotification"

    invoke-virtual {v1, p2, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 104
    if-eqz v2, :cond_0

    .line 106
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 107
    invoke-direct {p0, p2, v0}, Lcom/android/calendar/eb;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/ContentResolver;Landroid/text/format/Time;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 388
    invoke-virtual {p0, p1}, Lcom/android/calendar/eb;->a(Landroid/content/ContentResolver;)V

    .line 390
    invoke-virtual {p2, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iget-wide v4, p2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 392
    iget-object v0, p0, Lcom/android/calendar/eb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/ed;

    .line 393
    iget v4, v0, Lcom/android/calendar/ed;->d:I

    if-ne v2, v4, :cond_0

    .line 394
    const-string v2, "WorkingDay"

    iget-object v0, v0, Lcom/android/calendar/ed;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 395
    const/4 v0, 0x2

    .line 402
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 397
    goto :goto_0

    .line 402
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 194
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 195
    invoke-direct {p0, v2, p2, p3}, Lcom/android/calendar/eb;->a(Landroid/content/ContentResolver;J)J

    move-result-wide v4

    .line 197
    invoke-direct {p0, v4, v5}, Lcom/android/calendar/eb;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 213
    :goto_0
    return-object v0

    .line 201
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/eb;->a(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v3

    .line 202
    if-nez v3, :cond_1

    move-object v0, v1

    .line 203
    goto :goto_0

    .line 206
    :cond_1
    invoke-static {}, Lcom/android/calendar/dz;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    .line 207
    :goto_1
    invoke-static {v2, v0}, Lcom/android/calendar/ee;->a(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    .line 208
    const-string v2, "drawable"

    const-string v4, "com.sec.android.app.eventnotification"

    invoke-virtual {v3, v0, v2, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 209
    if-eqz v0, :cond_3

    .line 210
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 206
    :cond_2
    const/16 v0, 0x8

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 213
    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 113
    const-string v0, "FestivalEffectManager"

    const-string v1, "getHolidayImageForMonthList"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/eb;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 116
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 117
    const/4 v0, 0x0

    .line 129
    :cond_0
    :goto_0
    return-object v0

    .line 120
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 121
    invoke-static {v1, v0}, Lcom/android/calendar/ee;->a(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-direct {p0, v1}, Lcom/android/calendar/eb;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 125
    if-nez v0, :cond_0

    .line 129
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/eb;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/ContentResolver;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 328
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/eb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 334
    const-string v3, "FestivalEffectManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateHolidayList "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-wide v6, Lcom/android/calendar/eb;->e:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    sget-wide v4, Lcom/android/calendar/eb;->e:J

    sub-long v4, v0, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 336
    const-string v0, "FestivalEffectManager"

    const-string v1, "updateHolidayList : too already update request"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 339
    :cond_2
    sput-wide v0, Lcom/android/calendar/eb;->e:J

    .line 341
    new-instance v0, Lcom/android/calendar/ef;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/ef;-><init>(Lcom/android/calendar/eb;Landroid/content/ContentResolver;)V

    .line 342
    const/4 v1, -0x1

    sget-object v3, Lcom/android/calendar/eb;->g:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/eb;->f:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ef;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 318
    const-string v0, "FestivalEffectManager"

    const-string v1, "clearHolidayList"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v0, p0, Lcom/android/calendar/eb;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/android/calendar/eb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 324
    :cond_0
    sget-object v0, Lcom/android/calendar/eb;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 325
    return-void
.end method

.method public b(Landroid/content/ContentResolver;Landroid/text/format/Time;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 406
    invoke-virtual {p0, p1}, Lcom/android/calendar/eb;->a(Landroid/content/ContentResolver;)V

    .line 408
    invoke-virtual {p2, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iget-wide v4, p2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 409
    iget-object v0, p0, Lcom/android/calendar/eb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/ed;

    .line 410
    iget v0, v0, Lcom/android/calendar/ed;->d:I

    if-gt v2, v0, :cond_0

    .line 411
    const-string v0, "FestivalEffectManager"

    const-string v2, "find next year china holiday"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 415
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
