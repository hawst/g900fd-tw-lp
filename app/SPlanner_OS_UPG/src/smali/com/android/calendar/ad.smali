.class Lcom/android/calendar/ad;
.super Landroid/database/ContentObserver;
.source "AllInOneActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/AllInOneActivity;

.field private b:Landroid/content/Context;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/AllInOneActivity;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 303
    iput-object p1, p0, Lcom/android/calendar/ad;->a:Lcom/android/calendar/AllInOneActivity;

    .line 304
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 305
    iput-object p2, p0, Lcom/android/calendar/ad;->b:Landroid/content/Context;

    .line 306
    iget-object v0, p0, Lcom/android/calendar/ad;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "easy_mode_splanner"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/ad;->c:I

    .line 308
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4

    .prologue
    .line 312
    iget v0, p0, Lcom/android/calendar/ad;->c:I

    iget-object v1, p0, Lcom/android/calendar/ad;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_mode_splanner"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 314
    iget-object v0, p0, Lcom/android/calendar/ad;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->recreate()V

    .line 315
    iget-object v0, p0, Lcom/android/calendar/ad;->a:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, p0, Lcom/android/calendar/ad;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;Z)V

    .line 317
    :cond_0
    return-void
.end method
