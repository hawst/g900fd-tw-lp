.class Lcom/android/calendar/s;
.super Ljava/lang/Object;
.source "AllInOneActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Landroid/text/format/Time;

.field final synthetic c:Lcom/android/calendar/AllInOneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/AllInOneActivity;ILandroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lcom/android/calendar/s;->c:Lcom/android/calendar/AllInOneActivity;

    iput p2, p0, Lcom/android/calendar/s;->a:I

    iput-object p3, p0, Lcom/android/calendar/s;->b:Landroid/text/format/Time;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v9, -0x1

    .line 456
    iget v0, p0, Lcom/android/calendar/s;->a:I

    if-eq v0, v9, :cond_3

    iget v0, p0, Lcom/android/calendar/s;->a:I

    iget-object v1, p0, Lcom/android/calendar/s;->c:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1}, Lcom/android/calendar/AllInOneActivity;->d(Lcom/android/calendar/AllInOneActivity;)I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 457
    iget-object v0, p0, Lcom/android/calendar/s;->c:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 458
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 459
    const-string v2, "EventInfoFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 460
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 461
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 463
    :cond_0
    const-string v2, "TaskInfoFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 464
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 465
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 467
    :cond_1
    sget-object v2, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 468
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/Fragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 469
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 471
    :cond_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 473
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/s;->c:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x20

    iget-object v4, p0, Lcom/android/calendar/s;->b:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/s;->b:Landroid/text/format/Time;

    iget-object v6, p0, Lcom/android/calendar/s;->b:Landroid/text/format/Time;

    const-wide/16 v7, -0x1

    iget v1, p0, Lcom/android/calendar/s;->a:I

    if-ne v1, v9, :cond_4

    const/4 v9, 0x0

    :goto_0
    const-wide/16 v10, 0x1

    const-string v12, ""

    const/4 v13, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 475
    return-void

    .line 473
    :cond_4
    iget v9, p0, Lcom/android/calendar/s;->a:I

    goto :goto_0
.end method
