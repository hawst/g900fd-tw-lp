.class Lcom/android/calendar/dn;
.super Ljava/lang/Thread;
.source "EventLoader.java"


# instance fields
.field a:Ljava/util/concurrent/LinkedBlockingQueue;

.field b:Lcom/android/calendar/dj;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/LinkedBlockingQueue;Lcom/android/calendar/dj;)V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 164
    iput-object p1, p0, Lcom/android/calendar/dn;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 165
    iput-object p2, p0, Lcom/android/calendar/dn;->b:Lcom/android/calendar/dj;

    .line 166
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/dn;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v1, Lcom/android/calendar/do;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/android/calendar/do;-><init>(Lcom/android/calendar/dk;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    return-void

    .line 171
    :catch_0
    move-exception v0

    .line 175
    const-string v0, "Cal"

    const-string v1, "LoaderThread.shutdown() interrupted!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 181
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 185
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/dn;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dm;

    .line 189
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/dn;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/android/calendar/dn;->b:Lcom/android/calendar/dj;

    invoke-interface {v0, v1}, Lcom/android/calendar/dm;->b(Lcom/android/calendar/dj;)V

    .line 194
    iget-object v0, p0, Lcom/android/calendar/dn;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dm;

    goto :goto_1

    .line 197
    :cond_0
    instance-of v1, v0, Lcom/android/calendar/do;

    if-eqz v1, :cond_1

    .line 198
    return-void

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/dn;->b:Lcom/android/calendar/dj;

    invoke-interface {v0, v1}, Lcom/android/calendar/dm;->a(Lcom/android/calendar/dj;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    const-string v0, "Cal"

    const-string v1, "background LoaderThread interrupted!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
