.class final Lcom/android/calendar/fn;
.super Landroid/widget/Filter;
.source "RecipientAdapter.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/fk;


# direct methods
.method private constructor <init>(Lcom/android/calendar/fk;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/fk;Lcom/android/calendar/fl;)V
    .locals 0

    .prologue
    .line 213
    invoke-direct {p0, p1}, Lcom/android/calendar/fn;-><init>(Lcom/android/calendar/fk;)V

    return-void
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 321
    check-cast p1, Lcom/android/ex/a/b;

    .line 322
    invoke-virtual {p1}, Lcom/android/ex/a/b;->b()Ljava/lang/String;

    move-result-object v1

    .line 323
    invoke-virtual {p1}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 324
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 327
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Landroid/text/util/Rfc822Token;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, v3}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 16

    .prologue
    .line 222
    new-instance v13, Landroid/widget/Filter$FilterResults;

    invoke-direct {v13}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 223
    const/4 v2, 0x0

    .line 224
    const/4 v14, 0x0

    .line 226
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v13

    .line 291
    :goto_0
    return-object v1

    .line 230
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/lang/String;)Ljava/lang/String;

    .line 232
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    invoke-static {v3}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;)I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v1, v0, v3, v4}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v15

    .line 234
    if-nez v15, :cond_3

    .line 284
    :goto_1
    if-eqz v15, :cond_1

    .line 285
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 287
    :cond_1
    if-eqz v14, :cond_2

    .line 288
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v1, v13

    .line 291
    goto :goto_0

    .line 242
    :cond_3
    :try_start_1
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 244
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 246
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 248
    :goto_2
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 251
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    new-instance v2, Lcom/android/calendar/fu;

    invoke-direct {v2, v15}, Lcom/android/calendar/fu;-><init>(Landroid/database/Cursor;)V

    const/4 v3, 0x1

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Lcom/android/calendar/fu;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 284
    :catchall_0
    move-exception v1

    move-object v2, v14

    move-object v3, v15

    :goto_3
    if-eqz v3, :cond_4

    .line 285
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 287
    :cond_4
    if-eqz v2, :cond_5

    .line 288
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1

    .line 256
    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    const/4 v2, 0x0

    invoke-static {v1, v2, v4, v5, v6}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v3

    .line 261
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    invoke-static {v1}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;)I

    move-result v1

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v2

    sub-int/2addr v1, v2

    .line 263
    if-lez v1, :cond_7

    .line 269
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    invoke-static {v1}, Lcom/android/calendar/fk;->b(Lcom/android/calendar/fk;)Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/android/calendar/fr;->a:Landroid/net/Uri;

    sget-object v9, Lcom/android/calendar/fr;->b:[Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 272
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    invoke-static {v1, v2}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Landroid/database/Cursor;)Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v7

    move-object v8, v2

    .line 278
    :goto_4
    :try_start_4
    new-instance v2, Lcom/android/calendar/fo;

    invoke-direct/range {v2 .. v7}, Lcom/android/calendar/fo;-><init>(Ljava/util/List;Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;Ljava/util/List;)V

    iput-object v2, v13, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 281
    const/4 v1, 0x1

    iput v1, v13, Landroid/widget/Filter$FilterResults;->count:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-object v14, v8

    goto :goto_1

    .line 275
    :cond_7
    const/4 v7, 0x0

    move-object v8, v14

    goto :goto_4

    .line 284
    :catchall_1
    move-exception v1

    move-object v3, v2

    move-object v2, v14

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v3, v15

    goto :goto_3

    :catchall_3
    move-exception v1

    move-object v2, v8

    move-object v3, v15

    goto :goto_3
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    invoke-static {v0, p1}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 301
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/fo;

    .line 303
    iget-object v1, p0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    iget-object v2, v0, Lcom/android/calendar/fo;->b:Ljava/util/LinkedHashMap;

    invoke-static {v1, v2}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/util/LinkedHashMap;)Ljava/util/LinkedHashMap;

    .line 304
    iget-object v1, p0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    iget-object v2, v0, Lcom/android/calendar/fo;->c:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/util/List;)Ljava/util/List;

    .line 305
    iget-object v1, p0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    iget-object v2, v0, Lcom/android/calendar/fo;->d:Ljava/util/Set;

    invoke-static {v1, v2}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/util/Set;)Ljava/util/Set;

    .line 307
    iget-object v1, p0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    iget-object v2, v0, Lcom/android/calendar/fo;->a:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/android/calendar/fk;->b(Lcom/android/calendar/fk;Ljava/util/List;)V

    .line 310
    iget-object v1, v0, Lcom/android/calendar/fo;->e:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 311
    iget-object v1, p0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    invoke-static {v1}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;)I

    move-result v1

    iget-object v2, v0, Lcom/android/calendar/fo;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    sub-int/2addr v1, v2

    .line 313
    iget-object v2, p0, Lcom/android/calendar/fn;->a:Lcom/android/calendar/fk;

    iget-object v0, v0, Lcom/android/calendar/fo;->e:Ljava/util/List;

    invoke-static {v2, p1, v0, v1}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/lang/CharSequence;Ljava/util/List;I)V

    .line 317
    :cond_0
    return-void
.end method
