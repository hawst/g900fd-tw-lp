.class Lcom/android/calendar/cl;
.super Lcom/android/calendar/ag;
.source "DeleteEventHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/cj;


# direct methods
.method public constructor <init>(Lcom/android/calendar/cj;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    .line 134
    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 135
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 139
    if-nez p3, :cond_0

    .line 158
    :goto_0
    return-void

    .line 142
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 144
    :pswitch_0
    invoke-static {p3}, Lcom/android/calendar/cj;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 146
    iget-object v11, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    new-instance v0, Lcom/android/calendar/ca;

    iget-object v1, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v1}, Lcom/android/calendar/cj;->a(Lcom/android/calendar/cj;)Lcom/android/calendar/as;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v2}, Lcom/android/calendar/cj;->b(Lcom/android/calendar/cj;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v4}, Lcom/android/calendar/cj;->c(Lcom/android/calendar/cj;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v6}, Lcom/android/calendar/cj;->d(Lcom/android/calendar/cj;)Lcom/android/calendar/cq;

    move-result-object v6

    iget-object v7, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v7}, Lcom/android/calendar/cj;->e(Lcom/android/calendar/cj;)Ljava/lang/Runnable;

    move-result-object v7

    iget-object v8, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v8}, Lcom/android/calendar/cj;->f(Lcom/android/calendar/cj;)Ljava/lang/Runnable;

    move-result-object v8

    iget-object v9, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v9}, Lcom/android/calendar/cj;->g(Lcom/android/calendar/cj;)Z

    move-result v9

    iget-object v10, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v10}, Lcom/android/calendar/cj;->h(Lcom/android/calendar/cj;)Z

    move-result v10

    invoke-direct/range {v0 .. v10}, Lcom/android/calendar/ca;-><init>(Lcom/android/calendar/as;JJLcom/android/calendar/cq;Ljava/lang/Runnable;Ljava/lang/Runnable;ZZ)V

    iput-object v0, v11, Lcom/android/calendar/cj;->a:Landroid/app/DialogFragment;

    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    iget-object v0, v0, Lcom/android/calendar/cj;->a:Landroid/app/DialogFragment;

    iget-object v1, p0, Lcom/android/calendar/cl;->a:Lcom/android/calendar/cj;

    invoke-static {v1}, Lcom/android/calendar/cj;->i(Lcom/android/calendar/cj;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "DeleteEvent"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
