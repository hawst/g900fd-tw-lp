.class public Lcom/android/calendar/ca;
.super Landroid/app/DialogFragment;
.source "DeleteEventDialogFragment.java"


# static fields
.field private static b:Landroid/app/AlertDialog;

.field private static t:Z


# instance fields
.field private A:Landroid/content/DialogInterface$OnClickListener;

.field private B:Landroid/content/DialogInterface$OnClickListener;

.field a:Landroid/database/Cursor;

.field private c:Landroid/content/AsyncQueryHandler;

.field private d:Lcom/android/calendar/as;

.field private e:J

.field private f:J

.field private g:I

.field private h:I

.field private i:Z

.field private j:Ljava/util/ArrayList;

.field private k:Lcom/android/calendar/cq;

.field private l:Ljava/lang/Runnable;

.field private m:Ljava/lang/Runnable;

.field private n:Z

.field private o:Z

.field private p:Landroid/widget/LinearLayout;

.field private q:Ljava/util/ArrayList;

.field private r:Z

.field private s:Landroid/widget/CheckedTextView;

.field private u:Landroid/app/Activity;

.field private v:I

.field private w:Lcom/android/calendar/detail/bj;

.field private x:Landroid/content/DialogInterface$OnClickListener;

.field private y:Landroid/content/DialogInterface$OnClickListener;

.field private z:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 99
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 75
    iput v1, p0, Lcom/android/calendar/ca;->g:I

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/ca;->i:Z

    .line 81
    iput-object v2, p0, Lcom/android/calendar/ca;->k:Lcom/android/calendar/cq;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/ca;->q:Ljava/util/ArrayList;

    .line 93
    iput-object v2, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    .line 96
    iput v1, p0, Lcom/android/calendar/ca;->v:I

    .line 97
    sget-object v0, Lcom/android/calendar/detail/bj;->a:Lcom/android/calendar/detail/bj;

    iput-object v0, p0, Lcom/android/calendar/ca;->w:Lcom/android/calendar/detail/bj;

    .line 382
    new-instance v0, Lcom/android/calendar/ce;

    invoke-direct {v0, p0}, Lcom/android/calendar/ce;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->x:Landroid/content/DialogInterface$OnClickListener;

    .line 437
    new-instance v0, Lcom/android/calendar/cf;

    invoke-direct {v0, p0}, Lcom/android/calendar/cf;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->y:Landroid/content/DialogInterface$OnClickListener;

    .line 451
    new-instance v0, Lcom/android/calendar/cg;

    invoke-direct {v0, p0}, Lcom/android/calendar/cg;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->z:Landroid/content/DialogInterface$OnClickListener;

    .line 485
    new-instance v0, Lcom/android/calendar/ch;

    invoke-direct {v0, p0}, Lcom/android/calendar/ch;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->A:Landroid/content/DialogInterface$OnClickListener;

    .line 515
    new-instance v0, Lcom/android/calendar/ci;

    invoke-direct {v0, p0}, Lcom/android/calendar/ci;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->B:Landroid/content/DialogInterface$OnClickListener;

    .line 100
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/ca;->setRetainInstance(Z)V

    .line 101
    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/as;JJLcom/android/calendar/cq;Ljava/lang/Runnable;Ljava/lang/Runnable;ZZ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 104
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 75
    iput v1, p0, Lcom/android/calendar/ca;->g:I

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/ca;->i:Z

    .line 81
    iput-object v2, p0, Lcom/android/calendar/ca;->k:Lcom/android/calendar/cq;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/ca;->q:Ljava/util/ArrayList;

    .line 93
    iput-object v2, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    .line 96
    iput v1, p0, Lcom/android/calendar/ca;->v:I

    .line 97
    sget-object v0, Lcom/android/calendar/detail/bj;->a:Lcom/android/calendar/detail/bj;

    iput-object v0, p0, Lcom/android/calendar/ca;->w:Lcom/android/calendar/detail/bj;

    .line 382
    new-instance v0, Lcom/android/calendar/ce;

    invoke-direct {v0, p0}, Lcom/android/calendar/ce;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->x:Landroid/content/DialogInterface$OnClickListener;

    .line 437
    new-instance v0, Lcom/android/calendar/cf;

    invoke-direct {v0, p0}, Lcom/android/calendar/cf;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->y:Landroid/content/DialogInterface$OnClickListener;

    .line 451
    new-instance v0, Lcom/android/calendar/cg;

    invoke-direct {v0, p0}, Lcom/android/calendar/cg;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->z:Landroid/content/DialogInterface$OnClickListener;

    .line 485
    new-instance v0, Lcom/android/calendar/ch;

    invoke-direct {v0, p0}, Lcom/android/calendar/ch;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->A:Landroid/content/DialogInterface$OnClickListener;

    .line 515
    new-instance v0, Lcom/android/calendar/ci;

    invoke-direct {v0, p0}, Lcom/android/calendar/ci;-><init>(Lcom/android/calendar/ca;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->B:Landroid/content/DialogInterface$OnClickListener;

    .line 105
    iput-object p1, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    .line 106
    iput-wide p2, p0, Lcom/android/calendar/ca;->e:J

    .line 107
    iput-wide p4, p0, Lcom/android/calendar/ca;->f:J

    .line 108
    iput-object p6, p0, Lcom/android/calendar/ca;->k:Lcom/android/calendar/cq;

    .line 109
    iput-object p7, p0, Lcom/android/calendar/ca;->l:Ljava/lang/Runnable;

    .line 110
    iput-object p8, p0, Lcom/android/calendar/ca;->m:Ljava/lang/Runnable;

    .line 111
    iput-boolean p9, p0, Lcom/android/calendar/ca;->n:Z

    .line 112
    iput-boolean p10, p0, Lcom/android/calendar/ca;->o:Z

    .line 113
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/ca;->setRetainInstance(Z)V

    .line 114
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/ca;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/android/calendar/ca;->g:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/ca;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/android/calendar/ca;->h:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/ca;Lcom/android/calendar/detail/bj;)Lcom/android/calendar/detail/bj;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/calendar/ca;->w:Lcom/android/calendar/detail/bj;

    return-object p1
.end method

.method private a(I)V
    .locals 14

    .prologue
    const-wide/16 v12, -0x1

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 560
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 561
    iget-object v4, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-boolean v4, v4, Lcom/android/calendar/as;->G:Z

    .line 562
    iget-object v5, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-wide v6, v5, Lcom/android/calendar/as;->y:J

    .line 563
    iget-object v5, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-wide v8, v5, Lcom/android/calendar/as;->b:J

    .line 565
    packed-switch p1, :pswitch_data_0

    .line 651
    :cond_0
    :goto_0
    return-void

    .line 570
    :pswitch_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 574
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 575
    const-string v6, "title"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 578
    iget-object v6, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-wide v6, v6, Lcom/android/calendar/as;->c:J

    .line 579
    const-string v10, "eventTimezone"

    invoke-virtual {v5, v10, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    const-string v10, "allDay"

    if-eqz v4, :cond_1

    move v0, v3

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v10, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 581
    const-string v0, "originalAllDay"

    if-eqz v4, :cond_2

    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 582
    const-string v0, "calendar_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 583
    const-string v0, "dtstart"

    iget-wide v6, p0, Lcom/android/calendar/ca;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 584
    const-string v0, "dtend"

    iget-wide v6, p0, Lcom/android/calendar/ca;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 585
    const-string v0, "original_sync_id"

    iget-object v3, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    const-string v0, "original_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 587
    const-string v0, "originalInstanceTime"

    iget-wide v6, p0, Lcom/android/calendar/ca;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 588
    const-string v0, "eventStatus"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 590
    iget-object v0, p0, Lcom/android/calendar/ca;->c:Landroid/content/AsyncQueryHandler;

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/content/AsyncQueryHandler;->startInsert(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 591
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    const-string v0, "GATE"

    const-string v1, "<GATE-M>EVENT_DELETED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    move v0, v1

    .line 580
    goto :goto_1

    :cond_2
    move v3, v1

    .line 581
    goto :goto_2

    .line 597
    :pswitch_1
    cmp-long v0, v8, v12

    if-eqz v0, :cond_0

    .line 598
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 599
    iget-object v0, p0, Lcom/android/calendar/ca;->c:Landroid/content/AsyncQueryHandler;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 600
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    const-string v0, "GATE"

    const-string v1, "<GATE-M>EVENT_DELETED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 609
    :pswitch_2
    iget-wide v10, p0, Lcom/android/calendar/ca;->e:J

    cmp-long v3, v6, v10

    if-nez v3, :cond_3

    .line 610
    cmp-long v0, v8, v12

    if-eqz v0, :cond_0

    .line 611
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 612
    iget-object v0, p0, Lcom/android/calendar/ca;->c:Landroid/content/AsyncQueryHandler;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 613
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    const-string v0, "GATE"

    const-string v1, "<GATE-M>EVENT_DELETED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 621
    :cond_3
    new-instance v3, Lcom/android/a/c;

    invoke-direct {v3}, Lcom/android/a/c;-><init>()V

    .line 622
    invoke-virtual {v3, v0}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 623
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 624
    if-eqz v4, :cond_4

    .line 625
    const-string v4, "UTC"

    iput-object v4, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 627
    :cond_4
    iget-wide v4, p0, Lcom/android/calendar/ca;->e:J

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 629
    iget v4, v3, Lcom/android/a/c;->b:I

    const/4 v5, 0x4

    if-lt v4, v5, :cond_5

    .line 630
    iget v4, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Landroid/text/format/Time;->monthDay:I

    .line 635
    :goto_3
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 639
    const-string v4, "UTC"

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 640
    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 641
    iput v1, v3, Lcom/android/a/c;->d:I

    .line 643
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 644
    const-string v0, "dtstart"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 645
    const-string v0, "rrule"

    invoke-virtual {v3}, Lcom/android/a/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 647
    iget-object v0, p0, Lcom/android/calendar/ca;->c:Landroid/content/AsyncQueryHandler;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 632
    :cond_5
    iget v4, v0, Landroid/text/format/Time;->second:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Landroid/text/format/Time;->second:I

    goto :goto_3

    .line 565
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 347
    .line 350
    iget-wide v0, p0, Lcom/android/calendar/ca;->e:J

    iget-object v2, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-wide v2, v2, Lcom/android/calendar/as;->y:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 351
    invoke-static {p1, p2, v4}, Lcom/android/calendar/ca;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 353
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 354
    invoke-static {p1, p2, v5}, Lcom/android/calendar/ca;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    iput v4, p0, Lcom/android/calendar/ca;->v:I

    .line 359
    iget-boolean v0, p0, Lcom/android/calendar/ca;->r:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->v:Z

    if-nez v0, :cond_3

    .line 360
    :cond_2
    invoke-static {p1, p2, v4}, Lcom/android/calendar/ca;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 361
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/ca;->v:I

    .line 364
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 365
    invoke-static {p1, p2, v5}, Lcom/android/calendar/ca;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 370
    iget v0, p0, Lcom/android/calendar/ca;->v:I

    if-ne v0, v4, :cond_0

    .line 371
    iput v5, p0, Lcom/android/calendar/ca;->v:I

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 336
    invoke-virtual {p0, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 337
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 338
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/ca;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/android/calendar/ca;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/ca;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/android/calendar/ca;->g:I

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/ca;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/calendar/ca;->s:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/ca;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/calendar/ca;->f()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/ca;->q:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    iget-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    return-void
.end method

.method static synthetic d(Lcom/android/calendar/ca;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/calendar/ca;->g()V

    return-void
.end method

.method static synthetic e(Lcom/android/calendar/ca;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/calendar/ca;->m:Ljava/lang/Runnable;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/android/calendar/ca;->k:Lcom/android/calendar/cq;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/android/calendar/ca;->k:Lcom/android/calendar/cq;

    invoke-interface {v0}, Lcom/android/calendar/cq;->a()V

    .line 380
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/ca;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/calendar/ca;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 398
    invoke-direct {p0}, Lcom/android/calendar/ca;->e()V

    .line 399
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->b:J

    .line 400
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    .line 401
    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 402
    iget-object v0, p0, Lcom/android/calendar/ca;->c:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 404
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const-string v0, "GATE"

    const-string v1, "<GATE-M>EVENT_DELETED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/ca;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 409
    iget-object v0, p0, Lcom/android/calendar/ca;->l:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 412
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    if-nez v0, :cond_2

    .line 435
    :goto_0
    return-void

    .line 415
    :cond_2
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 416
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 419
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 420
    const-string v1, "EventInfoFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 421
    check-cast v0, Lcom/android/calendar/detail/EventInfoFragment;

    .line 422
    if-eqz v0, :cond_4

    .line 423
    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 424
    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->dismiss()V

    .line 428
    :cond_4
    iget-boolean v0, p0, Lcom/android/calendar/ca;->n:Z

    if-eqz v0, :cond_5

    .line 429
    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 434
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/calendar/ca;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/android/calendar/ca;->v:I

    return v0
.end method

.method private g()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 461
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->b:J

    .line 463
    invoke-direct {p0}, Lcom/android/calendar/ca;->e()V

    .line 466
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 467
    const-string v3, "eventStatus"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 470
    iget-object v0, p0, Lcom/android/calendar/ca;->c:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x0

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 472
    iget-object v0, p0, Lcom/android/calendar/ca;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/android/calendar/ca;->l:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 476
    :cond_0
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 477
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 480
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/ca;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 481
    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 483
    :cond_2
    return-void
.end method

.method static synthetic h(Lcom/android/calendar/ca;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/calendar/ca;->p:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 532
    invoke-direct {p0}, Lcom/android/calendar/ca;->e()V

    .line 533
    iget v0, p0, Lcom/android/calendar/ca;->h:I

    invoke-direct {p0, v0}, Lcom/android/calendar/ca;->a(I)V

    .line 534
    iget-object v0, p0, Lcom/android/calendar/ca;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/android/calendar/ca;->l:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 538
    :cond_0
    sget-boolean v0, Lcom/android/calendar/ca;->t:Z

    if-eqz v0, :cond_1

    .line 539
    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 540
    const-string v1, "EventInfoFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 541
    check-cast v0, Lcom/android/calendar/detail/EventInfoFragment;

    .line 542
    if-eqz v0, :cond_1

    .line 543
    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 544
    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->dismiss()V

    .line 550
    :cond_1
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 551
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 554
    :cond_2
    iget-boolean v0, p0, Lcom/android/calendar/ca;->n:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    if-eqz v0, :cond_3

    .line 555
    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 557
    :cond_3
    return-void
.end method

.method static synthetic i(Lcom/android/calendar/ca;)Lcom/android/calendar/as;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/ca;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/android/calendar/ca;->h:I

    return v0
.end method

.method static synthetic k(Lcom/android/calendar/ca;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/calendar/ca;->h()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 152
    iget-object v0, p0, Lcom/android/calendar/ca;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lcom/android/calendar/ca;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 155
    array-length v2, v1

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    const-string v0, "EmailDebug"

    const-string v1, "Sending decline response from DeleteEventDialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.email.intent.action.CALENDAR_MEETING_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 160
    const-string v1, "account_email"

    iget-object v2, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    const-string v1, "event_id"

    iget-object v2, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-wide v2, v2, Lcom/android/calendar/as;->b:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 162
    const-string v1, "meeting_response"

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 164
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/ca;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    return-void

    .line 165
    :catch_0
    move-exception v0

    .line 166
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/android/calendar/ca;->w:Lcom/android/calendar/detail/bj;

    sget-object v1, Lcom/android/calendar/detail/bj;->c:Lcom/android/calendar/detail/bj;

    if-ne v0, v1, :cond_1

    .line 686
    invoke-direct {p0}, Lcom/android/calendar/ca;->h()V

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/ca;->w:Lcom/android/calendar/detail/bj;

    sget-object v1, Lcom/android/calendar/detail/bj;->b:Lcom/android/calendar/detail/bj;

    if-ne v0, v1, :cond_2

    .line 688
    invoke-direct {p0}, Lcom/android/calendar/ca;->f()V

    goto :goto_0

    .line 689
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/ca;->w:Lcom/android/calendar/detail/bj;

    sget-object v1, Lcom/android/calendar/detail/bj;->d:Lcom/android/calendar/detail/bj;

    if-ne v0, v1, :cond_0

    .line 690
    invoke-direct {p0}, Lcom/android/calendar/ca;->g()V

    goto :goto_0
.end method

.method public c()Lcom/android/calendar/detail/bj;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/android/calendar/ca;->w:Lcom/android/calendar/detail/bj;

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 119
    new-instance v0, Lcom/android/calendar/cb;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/cb;-><init>(Lcom/android/calendar/ca;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->c:Landroid/content/AsyncQueryHandler;

    .line 121
    iput-object p1, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    .line 123
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 124
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 127
    :cond_0
    invoke-static {}, Lcom/android/calendar/cj;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 128
    if-nez v0, :cond_2

    .line 142
    :cond_1
    :goto_0
    return-void

    .line 132
    :cond_2
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    .line 134
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    const-string v1, "com.android.exchange"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/ca;->r:Z

    .line 139
    iget-boolean v0, p0, Lcom/android/calendar/ca;->r:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 140
    invoke-direct {p0}, Lcom/android/calendar/ca;->d()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14

    .prologue
    const v13, 0x7f0f0159

    const v12, 0x7f0f0149

    const/high16 v11, 0x1040000

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 173
    iget-object v0, p0, Lcom/android/calendar/ca;->u:Landroid/app/Activity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/ca;->t:Z

    .line 175
    invoke-virtual {p0}, Lcom/android/calendar/ca;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    const/4 v0, 0x0

    .line 331
    :goto_0
    return-object v0

    .line 178
    :cond_0
    if-eqz p1, :cond_7

    .line 179
    const-string v0, "event_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    const-string v0, "event_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/as;

    iput-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    .line 182
    :cond_1
    const-string v0, "which"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    const-string v0, "which"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/ca;->g:I

    .line 185
    :cond_2
    const-string v0, "which_delete"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 186
    const-string v0, "which_delete"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/ca;->h:I

    .line 188
    :cond_3
    const-string v0, "start_millis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189
    const-string v0, "start_millis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/ca;->e:J

    .line 191
    :cond_4
    const-string v0, "end_millis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 192
    const-string v0, "end_millis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/ca;->f:J

    .line 195
    :cond_5
    const-string v0, "checked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 196
    const-string v0, "checked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/ca;->i:Z

    .line 199
    :cond_6
    const-string v0, "exit_when_done"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 200
    const-string v0, "exit_when_done"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/ca;->n:Z

    .line 203
    :cond_7
    invoke-virtual {p0}, Lcom/android/calendar/ca;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 207
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v5, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v6, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    .line 209
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-object v7, v0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    .line 211
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/ca;->p:Landroid/widget/LinearLayout;

    .line 212
    const-string v0, "layout_inflater"

    invoke-virtual {v4, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 214
    const v1, 0x7f040033

    iget-object v2, p0, Lcom/android/calendar/ca;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 215
    iget-object v0, p0, Lcom/android/calendar/ca;->p:Landroid/widget/LinearLayout;

    const v1, 0x7f1200b9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 216
    iget-object v1, p0, Lcom/android/calendar/ca;->p:Landroid/widget/LinearLayout;

    const v2, 0x7f12002d

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 217
    iget-object v2, p0, Lcom/android/calendar/ca;->p:Landroid/widget/LinearLayout;

    const v8, 0x7f1200b8

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 219
    iget-object v2, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-boolean v2, v2, Lcom/android/calendar/as;->v:Z

    if-eqz v2, :cond_a

    const v2, 0x7f0f03d0

    .line 221
    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(I)V

    .line 222
    iget-boolean v2, p0, Lcom/android/calendar/ca;->i:Z

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 223
    iput-object v0, p0, Lcom/android/calendar/ca;->s:Landroid/widget/CheckedTextView;

    .line 224
    iget-object v2, p0, Lcom/android/calendar/ca;->s:Landroid/widget/CheckedTextView;

    new-instance v9, Lcom/android/calendar/cc;

    invoke-direct {v9, p0, v0}, Lcom/android/calendar/cc;-><init>(Lcom/android/calendar/ca;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v2, v9}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 233
    iget-boolean v0, p0, Lcom/android/calendar/ca;->o:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/android/calendar/ca;->r:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->v:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gt v0, v10, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->v:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v0, v10, :cond_b

    .line 235
    :cond_9
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    .line 237
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v13}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ca;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ca;->z:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v11, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    .line 253
    :goto_2
    if-nez v7, :cond_c

    .line 255
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v4, v12}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/ca;->x:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 331
    :goto_3
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 219
    :cond_a
    const v2, 0x7f0f03d1

    goto :goto_1

    .line 245
    :cond_b
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v13}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0156

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ca;->z:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v11, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    goto :goto_2

    .line 260
    :cond_c
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v4, v12}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/ca;->y:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_3

    .line 267
    :cond_d
    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 268
    new-instance v2, Ljava/util/ArrayList;

    const v1, 0x7f09000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 271
    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    .line 272
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 273
    array-length v6, v1

    move v0, v3

    :goto_4
    if-ge v0, v6, :cond_e

    aget v7, v1, v0

    .line 274
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 277
    :cond_e
    invoke-direct {p0, v2, v5}, Lcom/android/calendar/ca;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 279
    iget v0, p0, Lcom/android/calendar/ca;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_f

    .line 281
    iget v0, p0, Lcom/android/calendar/ca;->g:I

    iget v1, p0, Lcom/android/calendar/ca;->h:I

    if-eq v0, v1, :cond_11

    .line 282
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v3

    .line 283
    :goto_5
    if-ge v1, v6, :cond_f

    .line 284
    iget v3, p0, Lcom/android/calendar/ca;->h:I

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v3, v0, :cond_10

    .line 285
    iput v1, p0, Lcom/android/calendar/ca;->g:I

    .line 293
    :cond_f
    :goto_6
    iput-object v5, p0, Lcom/android/calendar/ca;->j:Ljava/util/ArrayList;

    .line 295
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f04009b

    invoke-direct {v0, v4, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 297
    iget-boolean v1, p0, Lcom/android/calendar/ca;->o:Z

    if-eqz v1, :cond_12

    iget-boolean v1, p0, Lcom/android/calendar/ca;->r:Z

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-le v1, v10, :cond_12

    .line 298
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v13}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/ca;->g:I

    iget-object v3, p0, Lcom/android/calendar/ca;->A:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ca;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ca;->B:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v12, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ca;->z:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v11, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    .line 305
    iget-object v0, p0, Lcom/android/calendar/ca;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 315
    :goto_7
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 316
    new-instance v1, Lcom/android/calendar/cd;

    invoke-direct {v1, p0}, Lcom/android/calendar/cd;-><init>(Lcom/android/calendar/ca;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_3

    .line 283
    :cond_10
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 290
    :cond_11
    iget v0, p0, Lcom/android/calendar/ca;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/ca;->g:I

    goto :goto_6

    .line 307
    :cond_12
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v13}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/ca;->g:I

    iget-object v3, p0, Lcom/android/calendar/ca;->A:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ca;->B:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v12, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/ca;->z:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v11, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    goto :goto_7
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 667
    invoke-virtual {p0}, Lcom/android/calendar/ca;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/ca;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668
    invoke-virtual {p0}, Lcom/android/calendar/ca;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 670
    :cond_0
    sput-object v1, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    .line 671
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 672
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 676
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 677
    sget-object v0, Lcom/android/calendar/ca;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 679
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 680
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 655
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 656
    const-string v0, "event_model"

    iget-object v1, p0, Lcom/android/calendar/ca;->d:Lcom/android/calendar/as;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 657
    const-string v0, "which"

    iget v1, p0, Lcom/android/calendar/ca;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 658
    const-string v0, "which_delete"

    iget v1, p0, Lcom/android/calendar/ca;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 659
    const-string v0, "start_millis"

    iget-wide v2, p0, Lcom/android/calendar/ca;->e:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 660
    const-string v0, "end_millis"

    iget-wide v2, p0, Lcom/android/calendar/ca;->f:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 661
    const-string v0, "checked"

    iget-boolean v1, p0, Lcom/android/calendar/ca;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 662
    const-string v0, "exit_when_done"

    iget-boolean v1, p0, Lcom/android/calendar/ca;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 663
    return-void
.end method
