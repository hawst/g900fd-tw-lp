.class public Lcom/android/calendar/AsyncQueryServiceHelper;
.super Landroid/app/IntentService;
.source "AsyncQueryServiceHelper.java"


# static fields
.field private static final b:Ljava/util/PriorityQueue;


# instance fields
.field protected a:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    sput-object v0, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 236
    const-string v0, "AsyncQueryServiceHelper"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 48
    const-class v0, Lcom/android/calendar/ag;

    iput-object v0, p0, Lcom/android/calendar/AsyncQueryServiceHelper;->a:Ljava/lang/Class;

    .line 237
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 232
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 48
    const-class v0, Lcom/android/calendar/ag;

    iput-object v0, p0, Lcom/android/calendar/AsyncQueryServiceHelper;->a:Ljava/lang/Class;

    .line 233
    return-void
.end method

.method public static a(I)I
    .locals 4

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    sget-object v2, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    monitor-enter v2

    .line 216
    :try_start_0
    sget-object v1, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    .line 217
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/ai;

    iget v0, v0, Lcom/android/calendar/ai;->a:I

    if-ne v0, p0, :cond_0

    .line 219
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 220
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 223
    :cond_1
    monitor-exit v2

    .line 228
    return v1

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcom/android/calendar/ai;)V
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p1}, Lcom/android/calendar/ai;->a()V

    .line 160
    sget-object v1, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    monitor-enter v1

    .line 161
    :try_start_0
    sget-object v0, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 162
    sget-object v0, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 163
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/AsyncQueryServiceHelper;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 166
    return-void

    .line 163
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public onCreate()V
    .locals 0

    .prologue
    .line 363
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 364
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 371
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 372
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 246
    sget-object v3, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    monitor-enter v3

    .line 252
    :cond_0
    :try_start_0
    sget-object v2, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 253
    monitor-exit v3

    .line 348
    :cond_1
    :goto_0
    return-void

    .line 254
    :cond_2
    sget-object v2, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_3

    .line 255
    sget-object v2, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/ai;

    .line 256
    if-eqz v2, :cond_4

    .line 257
    invoke-static {v2}, Lcom/android/calendar/ai;->a(Lcom/android/calendar/ai;)J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 259
    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_3

    .line 261
    :try_start_1
    sget-object v2, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    :cond_3
    :goto_1
    :try_start_2
    sget-object v2, Lcom/android/calendar/AsyncQueryServiceHelper;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/android/calendar/ai;

    move-object v8, v0

    .line 271
    if-eqz v8, :cond_0

    .line 276
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 282
    iget-object v2, v8, Lcom/android/calendar/ai;->c:Landroid/content/ContentResolver;

    .line 283
    if-eqz v2, :cond_1

    .line 285
    iget v3, v8, Lcom/android/calendar/ai;->b:I

    packed-switch v3, :pswitch_data_0

    .line 337
    :goto_2
    iget-object v2, v8, Lcom/android/calendar/ai;->f:Landroid/os/Handler;

    iget v3, v8, Lcom/android/calendar/ai;->a:I

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 338
    iput-object v8, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 339
    iget v3, v8, Lcom/android/calendar/ai;->b:I

    iput v3, v2, Landroid/os/Message;->arg1:I

    .line 346
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 266
    :cond_4
    :try_start_3
    monitor-exit v3

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 289
    :pswitch_0
    :try_start_4
    iget-object v3, v8, Lcom/android/calendar/ai;->d:Landroid/net/Uri;

    iget-object v4, v8, Lcom/android/calendar/ai;->g:[Ljava/lang/String;

    iget-object v5, v8, Lcom/android/calendar/ai;->h:Ljava/lang/String;

    iget-object v6, v8, Lcom/android/calendar/ai;->i:[Ljava/lang/String;

    iget-object v7, v8, Lcom/android/calendar/ai;->j:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 296
    if-eqz v2, :cond_5

    .line 297
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 304
    :cond_5
    :goto_3
    iput-object v2, v8, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    goto :goto_2

    .line 299
    :catch_0
    move-exception v2

    .line 300
    const-string v3, "AsyncQuery"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v9

    .line 301
    goto :goto_3

    .line 308
    :pswitch_1
    iget-object v3, v8, Lcom/android/calendar/ai;->d:Landroid/net/Uri;

    iget-object v4, v8, Lcom/android/calendar/ai;->m:Landroid/content/ContentValues;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, v8, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    goto :goto_2

    .line 312
    :pswitch_2
    iget-object v3, v8, Lcom/android/calendar/ai;->d:Landroid/net/Uri;

    iget-object v4, v8, Lcom/android/calendar/ai;->m:Landroid/content/ContentValues;

    iget-object v5, v8, Lcom/android/calendar/ai;->h:Ljava/lang/String;

    iget-object v6, v8, Lcom/android/calendar/ai;->i:[Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v8, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    goto :goto_2

    .line 317
    :pswitch_3
    iget-object v3, v8, Lcom/android/calendar/ai;->d:Landroid/net/Uri;

    iget-object v4, v8, Lcom/android/calendar/ai;->h:Ljava/lang/String;

    iget-object v5, v8, Lcom/android/calendar/ai;->i:[Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v8, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    goto :goto_2

    .line 322
    :pswitch_4
    :try_start_5
    iget-object v3, v8, Lcom/android/calendar/ai;->e:Ljava/lang/String;

    iget-object v4, v8, Lcom/android/calendar/ai;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v2

    iput-object v2, v8, Lcom/android/calendar/ai;->k:Ljava/lang/Object;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 323
    :catch_1
    move-exception v2

    .line 324
    const-string v3, "AsyncQuery"

    invoke-virtual {v2}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iput-object v9, v8, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    goto :goto_2

    .line 326
    :catch_2
    move-exception v2

    .line 327
    const-string v3, "AsyncQuery"

    invoke-virtual {v2}, Landroid/content/OperationApplicationException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    iput-object v9, v8, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    goto/16 :goto_2

    .line 262
    :catch_3
    move-exception v2

    goto/16 :goto_1

    .line 285
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 355
    invoke-super {p0, p1, p2}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    .line 356
    return-void
.end method
