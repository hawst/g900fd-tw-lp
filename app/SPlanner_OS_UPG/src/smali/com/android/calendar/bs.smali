.class Lcom/android/calendar/bs;
.super Landroid/content/AsyncQueryHandler;
.source "CopyEventToSolarHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/bm;


# direct methods
.method public constructor <init>(Lcom/android/calendar/bm;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    .line 244
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 245
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 249
    if-nez p3, :cond_0

    .line 292
    :goto_0
    return-void

    .line 253
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 291
    :cond_1
    :goto_1
    :pswitch_0
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 255
    :pswitch_1
    invoke-static {v4}, Lcom/android/calendar/bm;->a(I)I

    .line 256
    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/bm;->a(Lcom/android/calendar/bm;Landroid/database/MatrixCursor;)Landroid/database/MatrixCursor;

    .line 257
    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v0}, Lcom/android/calendar/bm;->e(Lcom/android/calendar/bm;)Landroid/database/MatrixCursor;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v0}, Lcom/android/calendar/bm;->e(Lcom/android/calendar/bm;)Landroid/database/MatrixCursor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/MatrixCursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/calendar/bm;->a(Lcom/android/calendar/bm;I)V

    goto :goto_1

    .line 259
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v0}, Lcom/android/calendar/bm;->e(Lcom/android/calendar/bm;)Landroid/database/MatrixCursor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v0, v5}, Lcom/android/calendar/bm;->a(Lcom/android/calendar/bm;Lcom/android/calendar/bs;)Lcom/android/calendar/bs;

    .line 261
    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v0}, Lcom/android/calendar/bm;->e(Lcom/android/calendar/bm;)Landroid/database/MatrixCursor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/MatrixCursor;->close()V

    goto :goto_1

    .line 266
    :pswitch_2
    new-instance v0, Lcom/android/calendar/as;

    invoke-direct {v0}, Lcom/android/calendar/as;-><init>()V

    .line 267
    invoke-static {v0, p3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Landroid/database/Cursor;)V

    .line 268
    iget-object v1, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v1}, Lcom/android/calendar/bm;->f(Lcom/android/calendar/bm;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->c:J

    .line 269
    iget-object v1, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v1}, Lcom/android/calendar/bm;->g(Lcom/android/calendar/bm;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 271
    new-instance v1, Lcom/android/calendar/event/av;

    iget-object v2, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v2}, Lcom/android/calendar/bm;->b(Lcom/android/calendar/bm;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/calendar/event/av;-><init>(Landroid/content/Context;Lcom/android/calendar/as;)V

    .line 272
    invoke-virtual {v1, v0, v5, v4}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;I)Z

    .line 274
    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v0}, Lcom/android/calendar/bm;->b(Lcom/android/calendar/bm;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v0}, Lcom/android/calendar/bm;->b(Lcom/android/calendar/bm;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/android/calendar/bs;->a:Lcom/android/calendar/bm;

    invoke-static {v0}, Lcom/android/calendar/bm;->b(Lcom/android/calendar/bm;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/bt;

    invoke-direct {v1, p0}, Lcom/android/calendar/bt;-><init>(Lcom/android/calendar/bs;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    .line 253
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
