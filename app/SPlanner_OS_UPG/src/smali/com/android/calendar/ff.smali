.class Lcom/android/calendar/ff;
.super Landroid/os/AsyncTask;
.source "NfcImportActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/NfcImportActivity;


# direct methods
.method private constructor <init>(Lcom/android/calendar/NfcImportActivity;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/NfcImportActivity;Lcom/android/calendar/fe;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/android/calendar/ff;-><init>(Lcom/android/calendar/NfcImportActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/nfc/NdefRecord;)Lcom/android/calendar/vcal/w;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 123
    .line 124
    aget-object v1, p1, v0

    .line 125
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 126
    const/4 v1, 0x0

    .line 129
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v2, v3}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 130
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 131
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v4

    .line 132
    if-lez v4, :cond_1

    .line 133
    new-array v4, v4, [B

    .line 136
    :goto_0
    invoke-virtual {v2, v4}, Ljava/io/ByteArrayInputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 137
    invoke-virtual {v3, v4, v0, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 138
    add-int/2addr v0, v5

    goto :goto_0

    .line 140
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-direct {v0, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v0

    .line 148
    :cond_1
    if-eqz v2, :cond_2

    .line 149
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 155
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-virtual {v0}, Lcom/android/calendar/NfcImportActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-static {v2}, Lcom/android/calendar/NfcImportActivity;->a(Lcom/android/calendar/NfcImportActivity;)Z

    move-result v2

    iget-object v3, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-static {v3}, Lcom/android/calendar/NfcImportActivity;->b(Lcom/android/calendar/NfcImportActivity;)I

    move-result v3

    iget-object v4, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-static {v4}, Lcom/android/calendar/NfcImportActivity;->c(Lcom/android/calendar/NfcImportActivity;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-static {v5}, Lcom/android/calendar/NfcImportActivity;->d(Lcom/android/calendar/NfcImportActivity;)I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/vcal/x;->a(Landroid/content/Context;Ljava/lang/String;ZILjava/lang/String;I)Lcom/android/calendar/vcal/w;

    move-result-object v0

    .line 158
    return-object v0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 148
    if-eqz v2, :cond_2

    .line 149
    :try_start_3
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 151
    :catch_1
    move-exception v0

    goto :goto_1

    .line 144
    :catch_2
    move-exception v0

    .line 145
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 148
    if-eqz v2, :cond_2

    .line 149
    :try_start_5
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 151
    :catch_3
    move-exception v0

    goto :goto_1

    .line 147
    :catchall_0
    move-exception v0

    .line 148
    if-eqz v2, :cond_3

    .line 149
    :try_start_6
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 152
    :cond_3
    :goto_2
    throw v0

    .line 151
    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_2
.end method

.method protected a(Lcom/android/calendar/vcal/w;)V
    .locals 4

    .prologue
    .line 163
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 165
    if-eqz p1, :cond_0

    .line 166
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lcom/android/calendar/vcal/w;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 167
    iget-object v1, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-static {v1}, Lcom/android/calendar/NfcImportActivity;->a(Lcom/android/calendar/NfcImportActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    const-string v1, "com.android.calendar"

    const-string v2, "com.android.calendar.detail.TaskInfoActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-virtual {v1, v0}, Lcom/android/calendar/NfcImportActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/ff;->a:Lcom/android/calendar/NfcImportActivity;

    invoke-virtual {v0}, Lcom/android/calendar/NfcImportActivity;->finish()V

    .line 183
    return-void

    .line 171
    :cond_1
    const-string v1, "com.android.calendar"

    const-string v2, "com.android.calendar.detail.EventInfoActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string v1, "beginTime"

    invoke-virtual {p1}, Lcom/android/calendar/vcal/w;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 174
    const-string v1, "endTime"

    invoke-virtual {p1}, Lcom/android/calendar/vcal/w;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0

    .line 178
    :catch_0
    move-exception v0

    .line 179
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    check-cast p1, [Landroid/nfc/NdefRecord;

    invoke-virtual {p0, p1}, Lcom/android/calendar/ff;->a([Landroid/nfc/NdefRecord;)Lcom/android/calendar/vcal/w;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 119
    check-cast p1, Lcom/android/calendar/vcal/w;

    invoke-virtual {p0, p1}, Lcom/android/calendar/ff;->a(Lcom/android/calendar/vcal/w;)V

    return-void
.end method
