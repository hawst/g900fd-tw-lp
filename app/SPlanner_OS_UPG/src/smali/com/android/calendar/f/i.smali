.class public Lcom/android/calendar/f/i;
.super Lcom/android/calendar/f/a;
.source "MonthTitleFragment.java"


# static fields
.field public static n:Z

.field private static final o:Ljava/lang/String;


# instance fields
.field protected k:Landroid/widget/TextView;

.field protected l:Landroid/view/View;

.field protected m:Landroid/view/View;

.field private p:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/android/calendar/f/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/f/i;->o:Ljava/lang/String;

    .line 152
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/f/i;->n:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/calendar/f/a;-><init>()V

    .line 219
    new-instance v0, Lcom/android/calendar/f/j;

    invoke-direct {v0, p0}, Lcom/android/calendar/f/j;-><init>(Lcom/android/calendar/f/i;)V

    iput-object v0, p0, Lcom/android/calendar/f/i;->p:Ljava/lang/Runnable;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/f/i;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/f/i;->p:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method protected a(Z)Landroid/text/format/Time;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 193
    if-eqz p1, :cond_0

    move v0, v1

    .line 195
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/f/i;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_1

    .line 196
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 200
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/f/i;->a:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 201
    iget v3, v2, Landroid/text/format/Time;->month:I

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v3

    iput v0, v2, Landroid/text/format/Time;->month:I

    .line 202
    invoke-static {v2}, Lcom/android/calendar/f/m;->a(Landroid/text/format/Time;)V

    .line 203
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 204
    invoke-static {v2}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;)V

    .line 205
    return-object v2

    .line 193
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 198
    :cond_1
    new-instance v2, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/f/i;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected a()Landroid/view/View;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/calendar/f/i;->m:Landroid/view/View;

    return-object v0
.end method

.method protected a(Landroid/text/format/Time;)V
    .locals 13

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x1

    .line 162
    invoke-static {p1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-nez v0, :cond_1

    .line 163
    sget-boolean v0, Lcom/android/calendar/month/k;->r:Z

    if-nez v0, :cond_0

    .line 174
    :goto_0
    return-void

    .line 166
    :cond_0
    sput-boolean v1, Lcom/android/calendar/month/k;->z:Z

    .line 168
    invoke-static {v1}, Lcom/android/calendar/month/k;->b(Z)V

    .line 169
    iget-object v0, p0, Lcom/android/calendar/f/i;->a:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x10

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/f/i;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    goto :goto_0
.end method

.method protected b()Landroid/view/View;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/calendar/f/i;->l:Landroid/view/View;

    return-object v0
.end method

.method protected b(Lcom/android/calendar/aq;)V
    .locals 9

    .prologue
    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 81
    invoke-super {p0, p1}, Lcom/android/calendar/f/a;->b(Lcom/android/calendar/aq;)V

    .line 82
    invoke-virtual {p0}, Lcom/android/calendar/f/i;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    .line 88
    iget-object v1, p0, Lcom/android/calendar/f/i;->b:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 89
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 90
    invoke-virtual {p0}, Lcom/android/calendar/f/i;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v3, v0, Landroid/text/format/Time;->year:I

    invoke-static {v1, v3}, Lcom/android/calendar/gm;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 92
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 93
    const-string v1, "ml"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 94
    :cond_1
    iget v0, v0, Landroid/text/format/Time;->month:I

    const/4 v1, 0x3

    invoke-static {v0, v1, v2}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 108
    :goto_1
    invoke-static {}, Lcom/android/calendar/hj;->j()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 109
    iget-object v1, p0, Lcom/android/calendar/f/i;->d:Ljava/lang/String;

    const-string v3, "YMD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 110
    new-instance v3, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 125
    :goto_2
    sget-boolean v4, Lcom/android/calendar/dz;->b:Z

    if-eqz v4, :cond_8

    .line 126
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/f/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :cond_2
    const-string v1, "el"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 96
    iget v0, v0, Landroid/text/format/Time;->month:I

    invoke-static {v0, v6, v8}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-static {v0}, Lcom/android/calendar/f/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 98
    :cond_3
    invoke-static {}, Lcom/android/calendar/hj;->r()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 99
    iget v0, v0, Landroid/text/format/Time;->month:I

    invoke-static {v0, v6, v2}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-static {v0}, Lcom/android/calendar/f/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 102
    :cond_4
    iget v0, v0, Landroid/text/format/Time;->month:I

    invoke-static {v0, v8, v8}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 103
    invoke-static {v0}, Lcom/android/calendar/f/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 113
    :cond_5
    new-instance v1, Landroid/text/SpannableStringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v1

    move v1, v2

    .line 114
    goto :goto_2

    .line 117
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/f/i;->d:Ljava/lang/String;

    const-string v3, "YMD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 118
    new-instance v3, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 119
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 121
    :cond_7
    new-instance v1, Landroid/text/SpannableStringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v1

    move v1, v2

    .line 122
    goto/16 :goto_2

    .line 128
    :cond_8
    iget-object v4, p0, Lcom/android/calendar/f/i;->k:Landroid/widget/TextView;

    .line 129
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 130
    const/16 v4, 0x21

    .line 132
    invoke-virtual {p0}, Lcom/android/calendar/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0367

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 133
    new-instance v7, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v7, v6}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v3, v7, v2, v6, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 134
    sget-boolean v2, Lcom/android/calendar/dz;->c:Z

    if-eqz v2, :cond_9

    .line 135
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v3, v2, v1, v5, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 136
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget v5, p0, Lcom/android/calendar/f/i;->f:I

    invoke-direct {v2, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v3, v2, v1, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 138
    :cond_9
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget v6, p0, Lcom/android/calendar/f/i;->i:I

    invoke-direct {v2, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v3, v2, v1, v6, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 139
    sget-boolean v2, Landroid/graphics/Typeface;->isFlipFontUsed:Z

    if-nez v2, :cond_a

    .line 140
    const-string v2, "or"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 141
    new-instance v2, Landroid/text/style/TypefaceSpan;

    const-string v5, "sans-serif"

    invoke-direct {v2, v5}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v2, v1, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 146
    :cond_a
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/f/i;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 143
    :cond_b
    new-instance v2, Landroid/text/style/TypefaceSpan;

    const-string v5, "sans-serif"

    invoke-direct {v2, v5}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v3, v2, v1, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3
.end method

.method protected c()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/calendar/f/i;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method protected e()V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/k;->w:Z

    .line 157
    invoke-super {p0}, Lcom/android/calendar/f/a;->e()V

    .line 158
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 59
    sget-boolean v0, Lcom/android/calendar/dz;->b:Z

    if-eqz v0, :cond_0

    .line 60
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/f/a;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    .line 62
    :cond_0
    const v0, 0x7f0400b4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 63
    invoke-virtual {p0}, Lcom/android/calendar/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/f/i;->j:I

    .line 64
    const v0, 0x7f1202f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/f/i;->k:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f1202f1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/f/i;->l:Landroid/view/View;

    .line 66
    const v0, 0x7f1202f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/f/i;->m:Landroid/view/View;

    .line 67
    new-instance v0, Lcom/android/calendar/f/k;

    invoke-direct {v0, p0}, Lcom/android/calendar/f/k;-><init>(Lcom/android/calendar/f/i;)V

    .line 68
    iget-object v2, p0, Lcom/android/calendar/f/i;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v2, p0, Lcom/android/calendar/f/i;->l:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v2, p0, Lcom/android/calendar/f/i;->m:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/android/calendar/f/i;->l:Landroid/view/View;

    const v2, 0x7f0f0340

    invoke-virtual {p0, v2}, Lcom/android/calendar/f/i;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/android/calendar/f/i;->m:Landroid/view/View;

    const v2, 0x7f0f02cc

    invoke-virtual {p0, v2}, Lcom/android/calendar/f/i;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/android/calendar/f/i;->l:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/f/i;->a(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/android/calendar/f/i;->m:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/f/i;->a(Landroid/view/View;)V

    move-object v0, v1

    .line 76
    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/android/calendar/f/i;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/f/i;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 230
    invoke-super {p0}, Lcom/android/calendar/f/a;->onDestroy()V

    .line 231
    return-void
.end method
