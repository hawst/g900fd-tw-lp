.class public abstract Lcom/android/calendar/f/l;
.super Lcom/android/calendar/f/a;
.source "NormalTitleFragment.java"


# static fields
.field private static final n:Ljava/lang/String;


# instance fields
.field protected k:Landroid/widget/TextView;

.field protected l:Landroid/view/View;

.field protected m:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/android/calendar/f/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/f/l;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/android/calendar/f/a;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/f/l;->m:Landroid/view/View;

    return-object v0
.end method

.method protected b()Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/calendar/f/l;->l:Landroid/view/View;

    return-object v0
.end method

.method protected c()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/f/l;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 42
    sget-boolean v0, Lcom/android/calendar/dz;->b:Z

    if-eqz v0, :cond_0

    .line 43
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/f/a;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    .line 45
    :cond_0
    const v0, 0x7f0400b6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 46
    const v0, 0x7f1202f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/f/l;->k:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f1202f1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/f/l;->l:Landroid/view/View;

    .line 48
    const v0, 0x7f1202f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/f/l;->m:Landroid/view/View;

    .line 49
    new-instance v0, Lcom/android/calendar/f/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/f/d;-><init>(Lcom/android/calendar/f/a;)V

    .line 50
    iget-object v2, p0, Lcom/android/calendar/f/l;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v2, p0, Lcom/android/calendar/f/l;->l:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v2, p0, Lcom/android/calendar/f/l;->m:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 53
    goto :goto_0
.end method
