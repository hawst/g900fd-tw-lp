.class public Lcom/android/calendar/f/o;
.super Lcom/android/calendar/f/l;
.source "YearTitleFragment.java"


# static fields
.field public static n:Z

.field private static final o:Ljava/lang/String;


# instance fields
.field private p:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/android/calendar/f/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/f/o;->o:Ljava/lang/String;

    .line 42
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/f/o;->n:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/android/calendar/f/l;-><init>()V

    .line 124
    new-instance v0, Lcom/android/calendar/f/p;

    invoke-direct {v0, p0}, Lcom/android/calendar/f/p;-><init>(Lcom/android/calendar/f/o;)V

    iput-object v0, p0, Lcom/android/calendar/f/o;->p:Ljava/lang/Runnable;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/f/o;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/calendar/f/o;->p:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method protected a(Z)Landroid/text/format/Time;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 99
    if-eqz p1, :cond_0

    move v0, v1

    .line 101
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/f/o;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_1

    .line 102
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 106
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/f/o;->a:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 107
    iget v3, v2, Landroid/text/format/Time;->year:I

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v3

    iput v0, v2, Landroid/text/format/Time;->year:I

    .line 108
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 109
    invoke-static {v2}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;)V

    .line 110
    return-object v2

    .line 99
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 104
    :cond_1
    new-instance v2, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/f/o;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected a(Landroid/text/format/Time;)V
    .locals 14

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 85
    invoke-static {p1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-nez v0, :cond_1

    .line 86
    sget-boolean v0, Lcom/android/calendar/f/o;->n:Z

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 89
    :cond_0
    sput-boolean v9, Lcom/android/calendar/f/o;->n:Z

    .line 90
    iget-object v0, p0, Lcom/android/calendar/f/o;->a:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v7, -0x1

    const-wide/16 v10, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v6, p1

    move-object v12, v5

    move-object v13, v5

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/f/o;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    goto :goto_0
.end method

.method protected b(Lcom/android/calendar/aq;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    invoke-super {p0, p1}, Lcom/android/calendar/f/l;->b(Lcom/android/calendar/aq;)V

    .line 64
    invoke-virtual {p0}, Lcom/android/calendar/f/o;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    :goto_0
    return-void

    .line 67
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 68
    invoke-virtual {p0}, Lcom/android/calendar/f/o;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v0, v0, Landroid/text/format/Time;->year:I

    invoke-static {v1, v0}, Lcom/android/calendar/gm;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 69
    sget-boolean v1, Lcom/android/calendar/dz;->b:Z

    if-eqz v1, :cond_1

    .line 70
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/f/o;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 72
    :cond_1
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {p0}, Lcom/android/calendar/f/o;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c036d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 74
    const/16 v2, 0x21

    .line 75
    new-instance v3, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v3, v0}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {v1, v3, v4, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 76
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    if-eqz v0, :cond_2

    .line 77
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v3, p0, Lcom/android/calendar/f/o;->f:I

    invoke-direct {v0, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v0, v4, v3, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/f/o;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 49
    sget-boolean v0, Lcom/android/calendar/dz;->b:Z

    if-eqz v0, :cond_0

    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/f/l;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    .line 52
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/f/l;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/android/calendar/f/o;->l:Landroid/view/View;

    const v2, 0x7f0f0343

    invoke-virtual {p0, v2}, Lcom/android/calendar/f/o;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v1, p0, Lcom/android/calendar/f/o;->m:Landroid/view/View;

    const v2, 0x7f0f02cf

    invoke-virtual {p0, v2}, Lcom/android/calendar/f/o;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 55
    new-instance v1, Lcom/android/calendar/f/q;

    invoke-direct {v1, p0}, Lcom/android/calendar/f/q;-><init>(Lcom/android/calendar/f/o;)V

    .line 56
    iget-object v2, p0, Lcom/android/calendar/f/o;->l:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v2, p0, Lcom/android/calendar/f/o;->m:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/calendar/f/o;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/f/o;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 135
    invoke-super {p0}, Lcom/android/calendar/f/l;->onDestroy()V

    .line 136
    return-void
.end method
