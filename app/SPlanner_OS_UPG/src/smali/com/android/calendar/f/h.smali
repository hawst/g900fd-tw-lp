.class public Lcom/android/calendar/f/h;
.super Lcom/android/calendar/f/i;
.source "EasyModeTitleFragment.java"


# static fields
.field private static final s:Ljava/lang/String;


# instance fields
.field private o:I

.field private p:I

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/android/calendar/f/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/f/h;->s:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/calendar/f/i;-><init>()V

    .line 55
    return-void
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/android/calendar/f/h;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method protected b()Landroid/view/View;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/android/calendar/f/h;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method protected b(Lcom/android/calendar/aq;)V
    .locals 12

    .prologue
    const/4 v7, 0x3

    const/4 v6, -0x1

    const v3, 0x3f733333    # 0.95f

    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 83
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/android/calendar/f/h;->b:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 86
    iget-object v1, p0, Lcom/android/calendar/f/h;->b:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 88
    invoke-virtual {p0}, Lcom/android/calendar/f/h;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 93
    iget-object v4, p0, Lcom/android/calendar/f/h;->k:Landroid/widget/TextView;

    .line 94
    invoke-virtual {v4}, Landroid/widget/TextView;->clearAnimation()V

    .line 95
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 97
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    .line 99
    iget-object v1, p0, Lcom/android/calendar/f/h;->b:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 100
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 103
    sget-boolean v1, Lcom/android/calendar/dz;->c:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/android/calendar/dz;->b:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/android/calendar/f/h;->j:I

    if-ne v1, v8, :cond_4

    .line 104
    :cond_2
    const-string v1, "vi"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 105
    iget-object v1, p0, Lcom/android/calendar/f/h;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 106
    iget-object v1, p0, Lcom/android/calendar/f/h;->r:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 108
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/f/h;->q:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-static {v3, v6}, Lcom/android/calendar/f/m;->a(Landroid/text/format/Time;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v1, p0, Lcom/android/calendar/f/h;->r:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-static {v3, v8}, Lcom/android/calendar/f/m;->a(Landroid/text/format/Time;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/f/h;->r:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-static {v3, v8}, Lcom/android/calendar/f/m;->b(Landroid/text/format/Time;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v1, p0, Lcom/android/calendar/f/h;->q:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-static {v3, v6}, Lcom/android/calendar/f/m;->b(Landroid/text/format/Time;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 115
    new-instance v1, Landroid/text/format/Time;

    iget-object v3, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-direct {v1, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 116
    invoke-virtual {p0}, Lcom/android/calendar/f/h;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget v6, v1, Landroid/text/format/Time;->year:I

    invoke-static {v3, v6}, Lcom/android/calendar/gm;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 118
    const-string v3, "vi"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 119
    :cond_5
    iget v0, v1, Landroid/text/format/Time;->month:I

    invoke-static {v0, v7, v2}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 132
    :goto_1
    iget v1, p0, Lcom/android/calendar/f/h;->j:I

    if-ne v1, v8, :cond_b

    .line 133
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 134
    new-instance v3, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "\n"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 135
    const/16 v7, 0x21

    .line 136
    const v1, 0x7f0c0404

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 137
    const v1, 0x7f0c0403

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 140
    invoke-virtual {v4}, Landroid/widget/TextView;->getWidth()I

    move-result v9

    .line 141
    if-lez v9, :cond_9

    .line 142
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 143
    int-to-float v4, v1

    invoke-virtual {v10, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 144
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    .line 145
    :goto_2
    if-le v4, v9, :cond_9

    if-lez v1, :cond_9

    .line 146
    add-int/lit8 v4, v1, -0x5

    .line 147
    int-to-float v1, v4

    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 148
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-int v1, v1

    move v11, v1

    move v1, v4

    move v4, v11

    goto :goto_2

    .line 120
    :cond_6
    const-string v3, "el"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 121
    iget v0, v1, Landroid/text/format/Time;->month:I

    const/4 v1, 0x2

    invoke-static {v0, v1, v8}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-static {v0}, Lcom/android/calendar/f/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 123
    :cond_7
    const-string v3, "ml"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 124
    iget v0, v1, Landroid/text/format/Time;->month:I

    invoke-static {v0, v8, v2}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 126
    :cond_8
    iget v0, v1, Landroid/text/format/Time;->month:I

    invoke-static {v0, v7, v8}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-static {v0}, Lcom/android/calendar/f/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    move v0, v1

    .line 153
    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v1, v8}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v1, v2, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 154
    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v1, v0}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v3, v1, v0, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 155
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, p0, Lcom/android/calendar/f/h;->o:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v3, v0, v2, v1, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 156
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, p0, Lcom/android/calendar/f/h;->p:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v3, v0, v1, v2, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 158
    sget-boolean v0, Landroid/graphics/Typeface;->isFlipFontUsed:Z

    if-nez v0, :cond_a

    .line 159
    new-instance v0, Landroid/text/style/TypefaceSpan;

    const-string v1, "sans-serif"

    invoke-direct {v0, v1}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v3, v0, v1, v2, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 194
    :cond_a
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/f/h;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/android/calendar/f/h;->k:Landroid/widget/TextView;

    const v1, 0x7f0b00b1

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 162
    :cond_b
    invoke-static {}, Lcom/android/calendar/hj;->j()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 163
    iget-object v1, p0, Lcom/android/calendar/f/h;->d:Ljava/lang/String;

    const-string v3, "YMD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 164
    new-instance v3, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 165
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    .line 180
    :goto_4
    const/16 v4, 0x21

    .line 181
    const v6, 0x7f0c0368

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 182
    new-instance v7, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v7, v6}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v3, v7, v2, v6, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 183
    sget-boolean v2, Lcom/android/calendar/dz;->c:Z

    if-eqz v2, :cond_f

    .line 184
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v3, v2, v1, v6, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 185
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget v6, p0, Lcom/android/calendar/f/h;->f:I

    invoke-direct {v2, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v3, v2, v1, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3

    .line 167
    :cond_c
    new-instance v1, Landroid/text/SpannableStringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v1

    move v1, v2

    .line 168
    goto :goto_4

    .line 171
    :cond_d
    iget-object v1, p0, Lcom/android/calendar/f/h;->d:Ljava/lang/String;

    const-string v3, "YMD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 172
    new-instance v3, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 173
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 175
    :cond_e
    new-instance v1, Landroid/text/SpannableStringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v1

    move v1, v2

    .line 176
    goto/16 :goto_4

    .line 187
    :cond_f
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget v6, p0, Lcom/android/calendar/f/h;->i:I

    invoke-direct {v2, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v3, v2, v1, v6, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 188
    sget-boolean v2, Landroid/graphics/Typeface;->isFlipFontUsed:Z

    if-nez v2, :cond_a

    .line 189
    new-instance v2, Landroid/text/style/TypefaceSpan;

    const-string v6, "sans-serif"

    invoke-direct {v2, v6}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v3, v2, v1, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3
.end method

.method protected c()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/android/calendar/f/h;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/android/calendar/f/i;->onAttach(Landroid/app/Activity;)V

    .line 60
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 64
    const v0, 0x7f0400b5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 65
    invoke-virtual {p0}, Lcom/android/calendar/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/f/h;->j:I

    .line 66
    const v0, 0x7f1202f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/f/h;->k:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f1202f7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/f/h;->q:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f1202fa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/f/h;->r:Landroid/widget/TextView;

    .line 69
    new-instance v0, Lcom/android/calendar/f/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/f/d;-><init>(Lcom/android/calendar/f/a;)V

    .line 70
    iget-object v2, p0, Lcom/android/calendar/f/h;->q:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v2, p0, Lcom/android/calendar/f/h;->r:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lcom/android/calendar/f/h;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/android/calendar/f/h;->a(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/android/calendar/f/h;->r:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/android/calendar/f/h;->a(Landroid/view/View;)V

    .line 75
    invoke-virtual {p0}, Lcom/android/calendar/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0065

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/f/h;->o:I

    .line 76
    invoke-virtual {p0}, Lcom/android/calendar/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0064

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/f/h;->p:I

    .line 77
    return-object v1
.end method
