.class public Lcom/android/calendar/f/d;
.super Ljava/lang/Object;
.source "AbsTitleFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/f/a;


# direct methods
.method protected constructor <init>(Lcom/android/calendar/f/a;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x4b0

    .line 209
    iget-object v0, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    invoke-virtual {v0}, Lcom/android/calendar/f/a;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    invoke-virtual {v0}, Lcom/android/calendar/f/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 213
    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 214
    iget-object v1, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    invoke-virtual {v1}, Lcom/android/calendar/f/a;->b()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 215
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    iget-object v0, v0, Lcom/android/calendar/f/a;->c:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/f/e;

    invoke-direct {v1, p0}, Lcom/android/calendar/f/e;-><init>(Lcom/android/calendar/f/d;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    invoke-virtual {v0}, Lcom/android/calendar/f/a;->d()V

    goto :goto_0

    .line 225
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    invoke-virtual {v1}, Lcom/android/calendar/f/a;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 227
    iget-object v0, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    iget-object v0, v0, Lcom/android/calendar/f/a;->c:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/f/f;

    invoke-direct {v1, p0}, Lcom/android/calendar/f/f;-><init>(Lcom/android/calendar/f/d;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 234
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/f/d;->a:Lcom/android/calendar/f/a;

    invoke-virtual {v0}, Lcom/android/calendar/f/a;->e()V

    goto :goto_0
.end method
