.class public Lcom/android/calendar/f/m;
.super Ljava/lang/Object;
.source "TitleUtils.java"


# direct methods
.method static a(Landroid/text/format/Time;I)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 45
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 46
    const/16 v0, 0xf

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 47
    iget v0, v1, Landroid/text/format/Time;->month:I

    add-int/2addr v0, p1

    iput v0, v1, Landroid/text/format/Time;->month:I

    .line 48
    invoke-virtual {v1, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 49
    iget v0, v1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x2

    invoke-static {v0, v2, v4}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 51
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 53
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ml"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kn"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 55
    :cond_1
    iget v0, v1, Landroid/text/format/Time;->month:I

    invoke-static {v0, v6, v4}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 57
    :cond_2
    invoke-static {}, Lcom/android/calendar/hj;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 58
    iget v0, v1, Landroid/text/format/Time;->month:I

    invoke-static {v0, v5, v4}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 60
    :cond_3
    invoke-static {v0}, Lcom/android/calendar/f/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 35
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    :goto_0
    return-object p0

    .line 37
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 38
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Landroid/text/format/Time;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 72
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 73
    iget v1, p0, Landroid/text/format/Time;->month:I

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 74
    iput v2, v0, Landroid/text/format/Time;->monthDay:I

    .line 75
    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 79
    const/4 v1, 0x4

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->getActualMaximum(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 84
    :goto_0
    iget v1, p0, Landroid/text/format/Time;->monthDay:I

    if-le v1, v0, :cond_0

    .line 85
    iput v0, p0, Landroid/text/format/Time;->monthDay:I

    .line 87
    :cond_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    const/16 v0, 0x64

    goto :goto_0
.end method

.method static b(Landroid/text/format/Time;I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 64
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 65
    const/16 v1, 0xf

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 66
    iget v1, v0, Landroid/text/format/Time;->month:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 67
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 68
    iget v0, v0, Landroid/text/format/Time;->month:I

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
