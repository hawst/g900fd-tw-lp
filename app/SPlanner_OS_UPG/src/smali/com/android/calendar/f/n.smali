.class public Lcom/android/calendar/f/n;
.super Lcom/android/calendar/f/l;
.source "WeekTitleFragment.java"


# static fields
.field private static final n:Ljava/lang/String;


# instance fields
.field private o:Landroid/text/format/Time;

.field private p:Landroid/text/format/Time;

.field private q:Landroid/text/format/Time;

.field private r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/android/calendar/f/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/f/n;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/android/calendar/f/l;-><init>()V

    .line 45
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    const v0, 0x7f0f0476

    invoke-virtual {p0, v0}, Lcom/android/calendar/f/n;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 119
    const-string v0, "%d"

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 120
    const-string v4, "%s"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 121
    if-ge v4, v0, :cond_0

    move v0, v1

    .line 122
    :goto_0
    const v4, 0x7f0f0474

    invoke-virtual {p0, v4}, Lcom/android/calendar/f/n;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 123
    if-eqz v0, :cond_1

    .line 124
    new-array v0, v5, [Ljava/lang/Object;

    aput-object v4, v0, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 126
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 121
    goto :goto_0

    .line 126
    :cond_1
    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v2

    aput-object v4, v0, v1

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method protected a(Z)Landroid/text/format/Time;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 132
    if-eqz p1, :cond_0

    move v0, v1

    .line 134
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/f/n;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_1

    .line 135
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 139
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/f/n;->a:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 140
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    mul-int/lit8 v0, v0, 0x7

    add-int/2addr v0, v3

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    .line 141
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 142
    invoke-static {v2}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;)V

    .line 143
    return-object v2

    .line 132
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 137
    :cond_1
    new-instance v2, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/f/n;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected a(Landroid/text/format/Time;)V
    .locals 14

    .prologue
    const/16 v2, 0x7f4

    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v9, 0x0

    .line 148
    invoke-static {p1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    .line 151
    if-nez v1, :cond_1

    move v0, v9

    .line 164
    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    .line 165
    iget-object v0, p0, Lcom/android/calendar/f/n;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    .line 170
    :goto_1
    return-void

    .line 153
    :cond_1
    if-ne v1, v0, :cond_3

    .line 154
    iget-object v1, p0, Lcom/android/calendar/f/n;->o:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/calendar/f/n;->o:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/calendar/f/n;->o:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/calendar/f/n;->o:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x7

    const/16 v2, 0x1f

    if-ge v1, v2, :cond_0

    :cond_2
    move v0, v9

    goto :goto_0

    .line 159
    :cond_3
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 160
    iget-object v1, p0, Lcom/android/calendar/f/n;->o:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    const/16 v2, 0x76e

    if-ge v1, v2, :cond_2

    goto :goto_0

    .line 167
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/f/n;->a:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v7, -0x1

    const-wide/16 v10, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v6, p1

    move-object v12, v5

    move-object v13, v5

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_1
.end method

.method protected b(Lcom/android/calendar/aq;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 61
    invoke-super {p0, p1}, Lcom/android/calendar/f/l;->b(Lcom/android/calendar/aq;)V

    .line 62
    invoke-virtual {p0}, Lcom/android/calendar/f/n;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/f/n;->q:Landroid/text/format/Time;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/f/n;->q:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget-object v2, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/calendar/f/n;->q:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->yearDay:I

    iget-object v2, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->yearDay:I

    if-eq v0, v2, :cond_0

    .line 69
    :cond_2
    new-instance v2, Landroid/text/format/Time;

    iget-object v0, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 70
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/f/n;->q:Landroid/text/format/Time;

    .line 71
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/f/n;->o:Landroid/text/format/Time;

    .line 72
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/f/n;->p:Landroid/text/format/Time;

    .line 73
    invoke-virtual {p0}, Lcom/android/calendar/f/n;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 74
    invoke-static {v3}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    .line 75
    iget-object v4, p0, Lcom/android/calendar/f/n;->o:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/f/n;->p:Landroid/text/format/Time;

    invoke-static {v2, v4, v5, v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;IZ)V

    .line 76
    iget-object v0, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {}, Lcom/android/calendar/dz;->k()Z

    move-result v0

    invoke-static {v4, v5, v3, v0}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;Z)I

    move-result v4

    .line 79
    invoke-static {}, Lcom/android/calendar/hj;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 80
    const/4 v0, 0x3

    .line 88
    :goto_1
    invoke-static {}, Lcom/android/calendar/hj;->r()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 89
    const/4 v1, 0x0

    .line 93
    :cond_3
    iget v5, v2, Landroid/text/format/Time;->month:I

    invoke-static {v5, v0, v1}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-static {v0}, Lcom/android/calendar/f/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-virtual {p0}, Lcom/android/calendar/f/n;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v2, v2, Landroid/text/format/Time;->year:I

    invoke-static {v0, v2}, Lcom/android/calendar/gm;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 98
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    const-string v0, ""

    .line 100
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 101
    const/16 v0, 0x202b

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    .line 104
    :cond_4
    invoke-static {v3}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 105
    invoke-direct {p0, v4}, Lcom/android/calendar/f/n;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 106
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :goto_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/f/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 81
    :cond_5
    invoke-static {}, Lcom/android/calendar/hj;->r()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 82
    const/4 v0, 0x2

    goto :goto_1

    :cond_6
    move v0, v1

    .line 84
    goto :goto_1

    .line 108
    :cond_7
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/android/calendar/hj;->h()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/android/calendar/hj;->j()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 109
    :cond_8
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 111
    :cond_9
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/f/l;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 50
    sget-boolean v1, Lcom/android/calendar/dz;->b:Z

    if-eqz v1, :cond_0

    .line 56
    :goto_0
    return-object v0

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/f/n;->l:Landroid/view/View;

    const v2, 0x7f0f0342

    invoke-virtual {p0, v2}, Lcom/android/calendar/f/n;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v1, p0, Lcom/android/calendar/f/n;->m:Landroid/view/View;

    const v2, 0x7f0f02ce

    invoke-virtual {p0, v2}, Lcom/android/calendar/f/n;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 55
    const v1, 0x7f0f0036

    invoke-virtual {p0, v1}, Lcom/android/calendar/f/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/f/n;->r:Ljava/lang/String;

    goto :goto_0
.end method
