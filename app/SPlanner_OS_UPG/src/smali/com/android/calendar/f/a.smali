.class public abstract Lcom/android/calendar/f/a;
.super Landroid/app/Fragment;
.source "AbsTitleFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field protected a:Lcom/android/calendar/al;

.field protected b:Landroid/text/format/Time;

.field protected c:Landroid/os/Handler;

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:I

.field protected g:I

.field protected h:I

.field protected i:I

.field protected j:I

.field private l:Lcom/android/calendar/aq;

.field private m:I

.field private final n:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/android/calendar/f/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/f/a;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/f/a;->c:Landroid/os/Handler;

    .line 156
    new-instance v0, Lcom/android/calendar/f/b;

    invoke-direct {v0, p0}, Lcom/android/calendar/f/b;-><init>(Lcom/android/calendar/f/a;)V

    iput-object v0, p0, Lcom/android/calendar/f/a;->n:Ljava/lang/Runnable;

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/f/a;)Lcom/android/calendar/aq;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/calendar/f/a;->l:Lcom/android/calendar/aq;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Z)Landroid/text/format/Time;
.end method

.method protected abstract a()Landroid/view/View;
.end method

.method protected a(Landroid/text/format/Time;)V
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 198
    invoke-static {p1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/android/calendar/f/a;->a:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v6, p1

    move-object v12, v5

    move-object v13, v5

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 204
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/f/a;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 242
    new-instance v1, Lcom/android/calendar/f/c;

    invoke-direct {v1, p0, v0, p1}, Lcom/android/calendar/f/c;-><init>(Lcom/android/calendar/f/a;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 254
    return-void
.end method

.method protected a(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 153
    const v0, 0x8000

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    .line 154
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 6

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/android/calendar/f/a;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 113
    iput-object p1, p0, Lcom/android/calendar/f/a;->l:Lcom/android/calendar/aq;

    .line 114
    iget-object v0, p0, Lcom/android/calendar/f/a;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/f/a;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 115
    iget-object v0, p0, Lcom/android/calendar/f/a;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/f/a;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/f/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_2

    instance-of v1, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v1, :cond_2

    .line 121
    iget-object v1, p0, Lcom/android/calendar/f/a;->a:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->g()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 126
    :cond_2
    iget-wide v2, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x400

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    iget-wide v2, p1, Lcom/android/calendar/aq;->a:J

    const-wide/32 v4, 0x10000

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 127
    :cond_3
    const/4 v1, 0x0

    .line 128
    sget-boolean v2, Lcom/android/calendar/dz;->b:Z

    if-eqz v2, :cond_5

    .line 129
    if-eqz v0, :cond_6

    instance-of v2, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v2, :cond_6

    .line 130
    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 131
    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->e()Landroid/widget/TextView;

    move-result-object v0

    .line 138
    :goto_1
    const-string v1, ""

    .line 139
    if-eqz v0, :cond_4

    .line 140
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 142
    :cond_4
    invoke-virtual {p0, p1}, Lcom/android/calendar/f/a;->b(Lcom/android/calendar/aq;)V

    .line 143
    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 145
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    invoke-virtual {p0, v0}, Lcom/android/calendar/f/a;->a(Landroid/widget/TextView;)V

    goto :goto_0

    .line 135
    :cond_5
    invoke-virtual {p0}, Lcom/android/calendar/f/a;->c()Landroid/widget/TextView;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/android/calendar/f/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v1, :cond_0

    .line 172
    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 173
    iget-object v1, p0, Lcom/android/calendar/f/a;->a:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->g()I

    move-result v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/calendar/AllInOneActivity;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_0
    return-void
.end method

.method protected abstract b()Landroid/view/View;
.end method

.method protected b(Lcom/android/calendar/aq;)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method protected abstract c()Landroid/widget/TextView;
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/f/a;->a(Z)Landroid/text/format/Time;

    move-result-object v0

    .line 189
    invoke-virtual {p0, v0}, Lcom/android/calendar/f/a;->a(Landroid/text/format/Time;)V

    .line 190
    return-void
.end method

.method protected e()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/f/a;->a(Z)Landroid/text/format/Time;

    move-result-object v0

    .line 194
    invoke-virtual {p0, v0}, Lcom/android/calendar/f/a;->a(Landroid/text/format/Time;)V

    .line 195
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 107
    const-wide/32 v0, 0x10400

    return-wide v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 76
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/f/a;->a:Lcom/android/calendar/al;

    .line 77
    new-instance v0, Landroid/text/format/Time;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/f/a;->b:Landroid/text/format/Time;

    .line 78
    iget-object v0, p0, Lcom/android/calendar/f/a;->b:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 79
    const v0, 0x7f0f012b

    invoke-virtual {p0, v0}, Lcom/android/calendar/f/a;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/f/a;->e:Ljava/lang/String;

    .line 80
    invoke-virtual {p0}, Lcom/android/calendar/f/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 81
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/hj;->e()[C

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/f/a;->d:Ljava/lang/String;

    .line 82
    sget-boolean v1, Lcom/android/calendar/dz;->c:Z

    if-eqz v1, :cond_0

    .line 83
    const v1, 0x7f0b00e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/f/a;->f:I

    .line 84
    const v1, 0x7f0b00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/f/a;->g:I

    .line 85
    const v1, 0x7f0b00e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/f/a;->h:I

    .line 87
    :cond_0
    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/f/a;->i:I

    .line 88
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/android/calendar/f/a;->j:I

    .line 89
    const v1, 0x7f0c0361

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/f/a;->m:I

    .line 90
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/calendar/f/a;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/f/a;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 102
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 103
    return-void
.end method

.method public onModeChanged(Z)V
    .locals 3

    .prologue
    .line 258
    sget-boolean v0, Lcom/android/calendar/dz;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/f/a;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/f/a;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 262
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 263
    instance-of v2, v1, Landroid/app/ActionBar$LayoutParams;

    if-nez v2, :cond_0

    .line 266
    check-cast v1, Landroid/app/ActionBar$LayoutParams;

    .line 267
    if-eqz p1, :cond_2

    .line 268
    const/4 v2, -0x1

    iput v2, v1, Landroid/app/ActionBar$LayoutParams;->width:I

    .line 269
    const/4 v2, 0x0

    iput v2, v1, Landroid/app/ActionBar$LayoutParams;->gravity:I

    .line 274
    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 271
    :cond_2
    iget v2, p0, Lcom/android/calendar/f/a;->m:I

    iput v2, v1, Landroid/app/ActionBar$LayoutParams;->width:I

    .line 272
    const/16 v2, 0x11

    iput v2, v1, Landroid/app/ActionBar$LayoutParams;->gravity:I

    goto :goto_1
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 96
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/hj;->e()[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/f/a;->d:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 0

    .prologue
    .line 279
    return-void
.end method
