.class public Lcom/android/calendar/timezone/t;
.super Ljava/lang/Object;
.source "CalendarTimezoneCity.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/TimeZone;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/timezone/t;->h:Ljava/util/TimeZone;

    .line 283
    iput-object p1, p0, Lcom/android/calendar/timezone/t;->a:Ljava/lang/String;

    .line 284
    iput-object p2, p0, Lcom/android/calendar/timezone/t;->b:Ljava/lang/String;

    .line 285
    iput-object p3, p0, Lcom/android/calendar/timezone/t;->c:Ljava/lang/String;

    .line 286
    iput p4, p0, Lcom/android/calendar/timezone/t;->d:I

    .line 287
    iput p5, p0, Lcom/android/calendar/timezone/t;->e:I

    .line 288
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/android/calendar/timezone/t;->f:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/android/calendar/timezone/t;->f:Ljava/lang/String;

    .line 300
    iput-object p2, p0, Lcom/android/calendar/timezone/t;->g:Ljava/lang/String;

    .line 301
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/android/calendar/timezone/t;->g:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    const v2, 0x36ee80

    .line 304
    const-string v0, "GMT"

    .line 305
    iget v1, p0, Lcom/android/calendar/timezone/t;->d:I

    div-int/2addr v1, v2

    if-ltz v1, :cond_0

    .line 306
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 308
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/timezone/t;->d:I

    div-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 309
    iget v1, p0, Lcom/android/calendar/timezone/t;->d:I

    rem-int/2addr v1, v2

    if-gtz v1, :cond_1

    iget v1, p0, Lcom/android/calendar/timezone/t;->d:I

    rem-int/2addr v1, v2

    if-gez v1, :cond_2

    .line 310
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/timezone/t;->d:I

    rem-int/2addr v1, v2

    const v2, 0xea60

    div-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 312
    :cond_2
    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/android/calendar/timezone/t;->e:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/android/calendar/timezone/t;->a:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/android/calendar/timezone/t;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Lcom/android/calendar/timezone/t;->d:I

    return v0
.end method
