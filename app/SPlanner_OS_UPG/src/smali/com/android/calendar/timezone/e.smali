.class Lcom/android/calendar/timezone/e;
.super Ljava/lang/Object;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwIndexScrollView$OnIndexSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 982
    iput-object p1, p0, Lcom/android/calendar/timezone/e;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIndexSelected(I)V
    .locals 4

    .prologue
    .line 986
    rsub-int/lit8 v0, p1, 0xd

    const v1, 0x36ee80

    mul-int v2, v0, v1

    .line 987
    iget-object v0, p0, Lcom/android/calendar/timezone/e;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v3

    .line 988
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 989
    iget-object v0, p0, Lcom/android/calendar/timezone/e;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    .line 990
    if-eqz v0, :cond_0

    .line 991
    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->f()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 988
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 995
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/timezone/e;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 998
    :cond_1
    return-void
.end method
