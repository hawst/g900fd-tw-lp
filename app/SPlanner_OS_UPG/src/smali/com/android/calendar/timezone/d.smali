.class Lcom/android/calendar/timezone/d;
.super Ljava/lang/Object;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwIndexScrollView$OnIndexSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 933
    iput-object p1, p0, Lcom/android/calendar/timezone/d;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIndexSelected(I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 935
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 937
    :cond_0
    add-int/lit8 v3, p1, 0x3

    .line 938
    iget-object v0, p0, Lcom/android/calendar/timezone/d;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v4

    move v1, v2

    .line 939
    :goto_0
    if-ge v1, v4, :cond_2

    .line 941
    iget-object v0, p0, Lcom/android/calendar/timezone/d;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v5, 0x2

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 939
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 945
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/timezone/d;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 951
    :cond_2
    :goto_1
    return-void

    .line 949
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/timezone/d;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_1
.end method
