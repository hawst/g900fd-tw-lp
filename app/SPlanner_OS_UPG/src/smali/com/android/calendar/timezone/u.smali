.class public Lcom/android/calendar/timezone/u;
.super Ljava/lang/Object;
.source "TimeZoneFinder.java"


# static fields
.field public static a:Lcom/android/calendar/timezone/r;

.field public static b:I

.field private static h:Ljava/lang/String;

.field private static i:Lcom/android/calendar/timezone/p;

.field private static j:Ljava/util/Timer;

.field private static k:Ljava/util/LinkedList;


# instance fields
.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/util/Map;

.field private n:Ljava/util/Map;

.field private o:Ljava/util/Map;

.field private p:Landroid/content/Context;

.field private q:Landroid/telephony/TelephonyManager;

.field private r:Landroid/location/LocationManager;

.field private s:Landroid/net/ConnectivityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    const-string v0, "undefined"

    sput-object v0, Lcom/android/calendar/timezone/u;->h:Ljava/lang/String;

    .line 60
    sput-object v1, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    .line 61
    sput-object v1, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    .line 62
    sput-object v1, Lcom/android/calendar/timezone/u;->k:Ljava/util/LinkedList;

    .line 64
    sput-object v1, Lcom/android/calendar/timezone/u;->a:Lcom/android/calendar/timezone/r;

    .line 65
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/timezone/u;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/calendar/timezone/u;->c:I

    .line 54
    const-string v0, "http://maps.google.com/maps/api/geocode/json?latlng="

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->d:Ljava/lang/String;

    .line 55
    const-string v0, "&sensor=true"

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->e:Ljava/lang/String;

    .line 56
    const-string v0, "&language=en"

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->f:Ljava/lang/String;

    .line 57
    const-string v0, "&language=kr"

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->g:Ljava/lang/String;

    .line 66
    const-string v0, "undefined"

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    .line 67
    iput-object v1, p0, Lcom/android/calendar/timezone/u;->m:Ljava/util/Map;

    .line 68
    iput-object v1, p0, Lcom/android/calendar/timezone/u;->n:Ljava/util/Map;

    .line 69
    iput-object v1, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    .line 70
    iput-object v1, p0, Lcom/android/calendar/timezone/u;->p:Landroid/content/Context;

    .line 71
    iput-object v1, p0, Lcom/android/calendar/timezone/u;->q:Landroid/telephony/TelephonyManager;

    .line 72
    iput-object v1, p0, Lcom/android/calendar/timezone/u;->r:Landroid/location/LocationManager;

    .line 73
    iput-object v1, p0, Lcom/android/calendar/timezone/u;->s:Landroid/net/ConnectivityManager;

    .line 80
    sget-object v0, Lcom/android/calendar/timezone/u;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    .line 81
    iput-object p1, p0, Lcom/android/calendar/timezone/u;->p:Landroid/content/Context;

    .line 83
    new-instance v0, Lcom/android/calendar/timezone/r;

    invoke-direct {v0, p1}, Lcom/android/calendar/timezone/r;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/calendar/timezone/u;->a:Lcom/android/calendar/timezone/r;

    .line 84
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->q:Landroid/telephony/TelephonyManager;

    .line 85
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->r:Landroid/location/LocationManager;

    .line 86
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->s:Landroid/net/ConnectivityManager;

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->n:Ljava/util/Map;

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->m:Ljava/util/Map;

    .line 92
    invoke-direct {p0}, Lcom/android/calendar/timezone/u;->i()V

    .line 93
    return-void
.end method

.method private a(Ljava/net/URL;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 369
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 370
    const/4 v2, 0x0

    .line 372
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 373
    if-eqz v0, :cond_1

    .line 375
    const/16 v1, 0x1b58

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 376
    const/16 v1, 0x1b58

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 377
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 378
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 379
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setAllowUserInteraction(Z)V

    .line 380
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    const-string v5, "utf-8"

    invoke-direct {v4, v0, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    const/16 v0, 0x1f40

    invoke-direct {v1, v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    :try_start_1
    const-string v0, ""

    .line 382
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 383
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 386
    :catch_0
    move-exception v0

    .line 387
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 388
    const-string v0, "NETWORK_ERROR"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390
    if-eqz v1, :cond_0

    .line 392
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 397
    :cond_0
    :goto_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, v2

    .line 390
    :cond_2
    if-eqz v1, :cond_0

    .line 392
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 393
    :catch_1
    move-exception v0

    goto :goto_2

    .line 390
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v2, :cond_3

    .line 392
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 394
    :cond_3
    :goto_4
    throw v0

    .line 393
    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_4

    .line 390
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 386
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Z)Ljava/util/ArrayList;
    .locals 13

    .prologue
    .line 401
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 402
    const-string v1, "NETWORK_ERROR"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 403
    new-instance v1, Landroid/location/Address;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 404
    const-string v2, "NETWORK_ERROR"

    invoke-virtual {v1, v2}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 405
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 487
    :cond_0
    :goto_0
    return-object v0

    .line 409
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 410
    const-string v2, "results"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 411
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v1

    .line 412
    if-eqz p2, :cond_15

    .line 413
    const/4 v1, 0x1

    move v3, v1

    .line 415
    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 416
    new-instance v1, Landroid/location/Address;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 417
    const-string v2, "NODATA_ERROR"

    invoke-virtual {v1, v2}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 418
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 479
    :catch_0
    move-exception v1

    .line 480
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 481
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 484
    new-instance v1, Landroid/location/Address;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 485
    const-string v2, "PARSING_ERROR"

    invoke-virtual {v1, v2}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 486
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 421
    :cond_2
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_0

    .line 422
    :try_start_1
    new-instance v5, Landroid/location/Address;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 423
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v6, "address_components"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 424
    const/4 v1, 0x0

    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v8, "formatted_address"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v1, v7}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    .line 426
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v7, "geometry"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v7, "location"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v7, "lat"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Landroid/location/Address;->setLatitude(D)V

    .line 428
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v7, "geometry"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v7, "location"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v7, "lng"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Landroid/location/Address;->setLongitude(D)V

    .line 431
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 432
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v7, :cond_14

    .line 433
    invoke-virtual {v6, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 434
    const-string v9, "types"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 435
    const-string v10, "short_name"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 436
    const-string v11, "long_name"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 438
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v12, "country"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 439
    invoke-virtual {v5, v8}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 440
    invoke-virtual {v5, v10}, Landroid/location/Address;->setCountryCode(Ljava/lang/String;)V

    .line 432
    :cond_3
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 441
    :cond_4
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "administrative_area_level_1"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 442
    invoke-virtual {v5, v8}, Landroid/location/Address;->setAdminArea(Ljava/lang/String;)V

    goto :goto_4

    .line 443
    :cond_5
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "administrative_area_level_2"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 444
    invoke-virtual {v5, v8}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    goto :goto_4

    .line 445
    :cond_6
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "administrative_area_level_3"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 446
    invoke-virtual {v5, v8}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    goto :goto_4

    .line 447
    :cond_7
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "colloquial_area"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 448
    invoke-virtual {v5, v8}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    goto :goto_4

    .line 449
    :cond_8
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "locality"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 450
    invoke-virtual {v5, v8}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    goto :goto_4

    .line 451
    :cond_9
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "sublocality"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 452
    invoke-virtual {v5, v8}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    goto :goto_4

    .line 453
    :cond_a
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "neighborhood"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 454
    invoke-virtual {v5, v8}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    goto :goto_4

    .line 455
    :cond_b
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "premise"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 456
    invoke-virtual {v5, v8}, Landroid/location/Address;->setPremises(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 457
    :cond_c
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "subpremise"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 458
    invoke-virtual {v5, v8}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 459
    :cond_d
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "postal_code"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 460
    invoke-virtual {v5, v8}, Landroid/location/Address;->setPostalCode(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 461
    :cond_e
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "airport"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 462
    invoke-virtual {v5, v8}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 463
    :cond_f
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "park"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 464
    invoke-virtual {v5, v8}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 465
    :cond_10
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "point_of_interest"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 466
    invoke-virtual {v5, v8}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 467
    :cond_11
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "street_number"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 468
    invoke-virtual {v5, v8}, Landroid/location/Address;->setThoroughfare(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 469
    :cond_12
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "route"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_13

    .line 470
    invoke-virtual {v5, v8}, Landroid/location/Address;->setSubThoroughfare(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 471
    :cond_13
    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "intersection"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 472
    invoke-virtual {v5, v8}, Landroid/location/Address;->setSubThoroughfare(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 475
    :cond_14
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 421
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_2

    :cond_15
    move v3, v1

    goto/16 :goto_1
.end method

.method private g()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 122
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->p:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 127
    new-instance v1, Landroid/location/Criteria;

    invoke-direct {v1}, Landroid/location/Criteria;-><init>()V

    .line 128
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 129
    invoke-virtual {v1, v3}, Landroid/location/Criteria;->setAltitudeRequired(Z)V

    .line 130
    invoke-virtual {v1, v3}, Landroid/location/Criteria;->setBearingRequired(Z)V

    .line 131
    invoke-virtual {v1, v3}, Landroid/location/Criteria;->setSpeedRequired(Z)V

    .line 133
    invoke-virtual {v0, v1, v4}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v1

    .line 134
    if-nez v1, :cond_0

    .line 135
    const-string v1, "network"

    .line 138
    :cond_0
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 140
    if-nez v1, :cond_2

    .line 142
    :try_start_0
    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    .line 143
    if-eqz v2, :cond_1

    .line 144
    const-string v4, "network"

    invoke-virtual {v0, v4}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 157
    :cond_1
    :goto_0
    if-nez v2, :cond_2

    .line 159
    :try_start_1
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    .line 160
    if-eqz v2, :cond_2

    .line 161
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v0

    .line 172
    :goto_1
    if-eqz v0, :cond_3

    .line 173
    new-instance v1, Landroid/location/Geocoder;

    iget-object v2, p0, Lcom/android/calendar/timezone/u;->p:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 176
    :try_start_2
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v0

    .line 177
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 178
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    invoke-virtual {v0}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    move-result-object v0

    .line 184
    :goto_2
    return-object v0

    .line 146
    :catch_0
    move-exception v2

    .line 148
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move v2, v3

    .line 155
    goto :goto_0

    .line 149
    :catch_1
    move-exception v2

    .line 151
    invoke-virtual {v2}, Ljava/lang/SecurityException;->printStackTrace()V

    move v2, v3

    .line 155
    goto :goto_0

    .line 152
    :catch_2
    move-exception v2

    .line 154
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->printStackTrace()V

    move v2, v3

    goto :goto_0

    .line 163
    :catch_3
    move-exception v0

    .line 164
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move-object v0, v1

    .line 169
    goto :goto_1

    .line 165
    :catch_4
    move-exception v0

    .line 166
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    move-object v0, v1

    .line 169
    goto :goto_1

    .line 167
    :catch_5
    move-exception v0

    .line 168
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 180
    :catch_6
    move-exception v0

    .line 181
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 184
    :cond_3
    sget-object v0, Lcom/android/calendar/timezone/u;->h:Ljava/lang/String;

    goto :goto_2
.end method

.method private h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 234
    .line 236
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->q:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 251
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/calendar/timezone/u;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method private i()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 255
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->p:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v9

    .line 256
    if-nez v9, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    :try_start_0
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 263
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_1

    const-string v1, "City"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v10

    move v1, v2

    move-object v0, v3

    move-object v4, v3

    move v5, v6

    move v7, v2

    .line 267
    :goto_1
    if-ge v1, v10, :cond_1

    .line 268
    invoke-interface {v9, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v8

    .line 270
    if-eqz v8, :cond_3

    const-string v11, "isoCode"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 271
    invoke-interface {v9, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v4

    move v8, v7

    move v7, v5

    move-object v5, v4

    move-object v4, v0

    .line 280
    :goto_2
    if-eqz v5, :cond_2

    if-eq v7, v6, :cond_2

    if-eqz v4, :cond_2

    .line 281
    packed-switch v8, :pswitch_data_0

    .line 267
    :cond_2
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v4

    move-object v4, v5

    move v5, v7

    move v7, v8

    goto :goto_1

    .line 272
    :cond_3
    if-eqz v8, :cond_4

    const-string v11, "id"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 273
    invoke-interface {v9, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    move v8, v7

    move v7, v5

    move-object v5, v4

    move-object v4, v0

    goto :goto_2

    .line 274
    :cond_4
    if-eqz v8, :cond_5

    const-string v11, "type"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 275
    invoke-interface {v9, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move v8, v7

    move v7, v5

    move-object v5, v4

    move-object v4, v0

    goto :goto_2

    .line 276
    :cond_5
    if-eqz v8, :cond_8

    const-string v11, "name"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 277
    invoke-interface {v9, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    move v8, v7

    move v7, v5

    move-object v5, v4

    move-object v4, v0

    goto :goto_2

    .line 283
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->n:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v0, v5, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->m:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v0, v4, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 320
    :catch_0
    move-exception v0

    .line 321
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 288
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 289
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 290
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 297
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->m:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v0, v4, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 292
    :cond_6
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 293
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 294
    iget-object v11, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    invoke-interface {v11, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 301
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 302
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 303
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 310
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->m:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v0, v4, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 305
    :cond_7
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 306
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 307
    iget-object v11, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    invoke-interface {v11, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    :cond_8
    move v8, v7

    move v7, v5

    move-object v5, v4

    move-object v4, v0

    goto/16 :goto_2

    .line 281
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    return-object v0
.end method

.method public a(DD)Ljava/lang/String;
    .locals 9

    .prologue
    .line 188
    .line 190
    const/4 v6, 0x1

    :try_start_0
    const-string v7, "EN"

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/timezone/u;->a(DDZLjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 191
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 192
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 193
    invoke-virtual {v0}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    .line 194
    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v2

    .line 196
    if-eqz v1, :cond_2

    .line 197
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/android/calendar/timezone/u;->n:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    iget-object v1, p0, Lcom/android/calendar/timezone/u;->n:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/android/calendar/timezone/u;->b:I

    .line 200
    const-string v0, "OK"

    .line 212
    :cond_0
    :goto_0
    return-object v0

    .line 201
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/timezone/u;->m:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->m:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/android/calendar/timezone/u;->b:I

    .line 203
    const-string v0, "OK"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .line 210
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 212
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(DDZLjava/lang/String;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 326
    .line 331
    :try_start_0
    const-string v0, "EN"

    invoke-virtual {v0, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    new-instance v0, Ljava/net/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://maps.google.com/maps/api/geocode/json?latlng="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&sensor=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&language=en"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/timezone/u;->s:Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    .line 352
    iget-object v1, p0, Lcom/android/calendar/timezone/u;->s:Landroid/net/ConnectivityManager;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    .line 354
    const-string v1, "NETWORK_ERROR"

    .line 355
    if-nez v2, :cond_0

    if-eqz v3, :cond_4

    .line 356
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/calendar/timezone/u;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v0

    .line 358
    :goto_1
    const-string v1, "NETWORK_ERROR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 359
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 360
    new-instance v1, Landroid/location/Address;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 361
    const-string v2, "NETWORK_ERROR"

    invoke-virtual {v1, v2}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 362
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    :goto_2
    return-object v0

    .line 334
    :cond_1
    :try_start_1
    const-string v0, "KR"

    invoke-virtual {v0, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 335
    new-instance v0, Ljava/net/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://maps.google.com/maps/api/geocode/json?latlng="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&sensor=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&language=kr"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 341
    :catch_0
    move-exception v0

    .line 342
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 343
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 344
    new-instance v1, Landroid/location/Address;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 345
    const-string v2, "NETWORK_ERROR"

    invoke-virtual {v1, v2}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 346
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 338
    :cond_2
    :try_start_2
    new-instance v0, Ljava/net/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://maps.google.com/maps/api/geocode/json?latlng="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&sensor=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&language=en"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 365
    :cond_3
    invoke-direct {p0, v0, p5}, Lcom/android/calendar/timezone/u;->a(Ljava/lang/String;Z)Ljava/util/ArrayList;

    move-result-object v0

    goto/16 :goto_2

    :cond_4
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public a(Lcom/android/calendar/timezone/p;Lcom/android/calendar/timezone/q;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 508
    if-nez p1, :cond_0

    move v0, v7

    .line 533
    :goto_0
    return v0

    .line 511
    :cond_0
    sput-object p1, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    .line 513
    sget-object v0, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 514
    sget-object v0, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 515
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->r:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 519
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->r:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 523
    :try_start_0
    sget-object v0, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    if-nez v0, :cond_2

    .line 524
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    .line 526
    :cond_2
    sget-object v0, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, p2, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 527
    const/4 v0, 0x1

    goto :goto_0

    .line 528
    :catch_0
    move-exception v0

    .line 529
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    move v0, v7

    .line 533
    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 224
    const-string v0, "network"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    sget-object v0, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/p;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->r:Landroid/location/LocationManager;

    sget-object v1, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 227
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    .line 230
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    return-object v0
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 100
    invoke-direct {p0}, Lcom/android/calendar/timezone/u;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    .line 102
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    sget-object v2, Lcom/android/calendar/timezone/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-direct {p0}, Lcom/android/calendar/timezone/u;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    sget-object v2, Lcom/android/calendar/timezone/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->n:Ljava/util/Map;

    iget-object v2, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->n:Ljava/util/Map;

    iget-object v2, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/android/calendar/timezone/u;->b:I

    .line 109
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/android/calendar/timezone/u;->k:Ljava/util/LinkedList;

    .line 110
    sget-object v0, Lcom/android/calendar/timezone/u;->k:Ljava/util/LinkedList;

    sget v2, Lcom/android/calendar/timezone/u;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    move v0, v1

    .line 118
    :goto_0
    return v0

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    iget-object v2, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->o:Ljava/util/Map;

    iget-object v2, p0, Lcom/android/calendar/timezone/u;->l:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    sput-object v0, Lcom/android/calendar/timezone/u;->k:Ljava/util/LinkedList;

    move v0, v1

    .line 115
    goto :goto_0

    .line 118
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 216
    sget v0, Lcom/android/calendar/timezone/u;->b:I

    return v0
.end method

.method public e()Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/android/calendar/timezone/u;->k:Ljava/util/LinkedList;

    return-object v0
.end method

.method public f()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 494
    sget-object v0, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/android/calendar/timezone/u;->r:Landroid/location/LocationManager;

    sget-object v1, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 496
    sput-object v2, Lcom/android/calendar/timezone/u;->i:Lcom/android/calendar/timezone/p;

    .line 498
    :cond_0
    sget-object v0, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 499
    sget-object v0, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 500
    sput-object v2, Lcom/android/calendar/timezone/u;->j:Ljava/util/Timer;

    .line 502
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
