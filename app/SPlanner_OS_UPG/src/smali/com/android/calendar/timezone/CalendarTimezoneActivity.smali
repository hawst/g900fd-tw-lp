.class public Lcom/android/calendar/timezone/CalendarTimezoneActivity;
.super Landroid/app/Activity;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# static fields
.field public static b:Landroid/widget/SearchView;

.field private static i:Lcom/android/calendar/timezone/u;

.field private static final j:Ljava/util/ArrayList;


# instance fields
.field private A:Lcom/android/calendar/timezone/r;

.field private B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

.field private C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

.field private D:Ljava/util/ArrayList;

.field private E:Landroid/widget/TextView;

.field public final a:Ljava/lang/String;

.field protected final c:Landroid/os/Handler;

.field public d:Landroid/os/Handler;

.field public e:Landroid/os/Handler;

.field private final f:I

.field private final g:I

.field private final h:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I

.field private p:I

.field private q:Landroid/widget/FrameLayout;

.field private r:Landroid/app/Activity;

.field private s:Landroid/widget/ListView;

.field private t:Landroid/widget/ArrayAdapter;

.field private u:Landroid/widget/ImageButton;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/util/ArrayList;

.field private z:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    sput-object v1, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->i:Lcom/android/calendar/timezone/u;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->j:Ljava/util/ArrayList;

    .line 87
    sput-object v1, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 80
    const-class v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a:Ljava/lang/String;

    .line 81
    const/16 v0, 0x64

    iput v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->f:I

    .line 82
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->g:I

    .line 83
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->h:I

    .line 90
    iput-boolean v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l:Z

    .line 92
    iput-boolean v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n:Z

    .line 101
    iput-object v2, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->u:Landroid/widget/ImageButton;

    .line 103
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->x:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    .line 105
    iput-object v2, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    .line 113
    new-instance v0, Lcom/android/calendar/timezone/a;

    invoke-direct {v0, p0}, Lcom/android/calendar/timezone/a;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c:Landroid/os/Handler;

    .line 368
    new-instance v0, Lcom/android/calendar/timezone/g;

    invoke-direct {v0, p0}, Lcom/android/calendar/timezone/g;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->d:Landroid/os/Handler;

    .line 468
    new-instance v0, Lcom/android/calendar/timezone/j;

    invoke-direct {v0, p0}, Lcom/android/calendar/timezone/j;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->e:Landroid/os/Handler;

    .line 729
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;I)I
    .locals 0

    .prologue
    .line 79
    iput p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->o:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/timezone/u;)Lcom/android/calendar/timezone/u;
    .locals 0

    .prologue
    .line 79
    sput-object p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->i:Lcom/android/calendar/timezone/u;

    return-object p0
.end method

.method static synthetic a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->x:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 7

    .prologue
    .line 605
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 685
    :cond_0
    :goto_0
    return-void

    .line 610
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 611
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    .line 612
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 613
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/timezone/r;->a(Ljava/lang/String;)Lcom/android/calendar/timezone/t;

    move-result-object v0

    .line 616
    if-eqz v0, :cond_2

    .line 620
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 622
    invoke-virtual {v0}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 623
    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 624
    invoke-virtual {v0}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 625
    iget-object v3, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    new-instance v4, Lcom/android/calendar/timezone/TimezoneCityListItem;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/calendar/timezone/t;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/android/calendar/timezone/t;->g()I

    move-result v6

    invoke-virtual {v0}, Lcom/android/calendar/timezone/t;->d()I

    move-result v0

    invoke-direct {v4, v2, v5, v6, v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 629
    :cond_3
    const v0, 0x7f12008f

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    .line 631
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 632
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 634
    new-instance v0, Lcom/android/calendar/timezone/x;

    const v1, 0x7f04006b

    iget-object v2, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/timezone/x;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    .line 636
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 641
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/timezone/m;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/m;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k:Z

    return p1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 377
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 378
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 379
    array-length v4, v2

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v2, v1

    .line 380
    invoke-virtual {v5, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 381
    const/4 v0, 0x1

    .line 384
    :cond_0
    return v0

    .line 379
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->w:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->v:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l:Z

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->x:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->o:I

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->o()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l:Z

    return v0
.end method

.method static synthetic i(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q()V

    return-void
.end method

.method static synthetic j(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic l()Lcom/android/calendar/timezone/u;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->i:Lcom/android/calendar/timezone/u;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k:Z

    return v0
.end method

.method static synthetic m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 283
    const v0, 0x7f12008f

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    .line 285
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 286
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/timezone/f;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/f;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 333
    return-void
.end method

.method static synthetic n(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->E:Landroid/widget/TextView;

    return-object v0
.end method

.method private n()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 434
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    .line 436
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f03b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 437
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 438
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 439
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 440
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 441
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/android/calendar/timezone/h;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/h;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 449
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 451
    new-instance v0, Lcom/android/calendar/timezone/i;

    invoke-direct {v0, p0}, Lcom/android/calendar/timezone/i;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    .line 464
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 465
    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 466
    return-void
.end method

.method static synthetic o(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->z:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 481
    :cond_0
    return-void
.end method

.method static synthetic p(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->r:Landroid/app/Activity;

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 485
    const v0, 0x7f12008d

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->u:Landroid/widget/ImageButton;

    .line 487
    invoke-static {}, Lcom/android/calendar/dz;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->u:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->u:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 492
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->u:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/timezone/k;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/k;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->u:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/timezone/l;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/l;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 537
    :cond_1
    return-void
.end method

.method static synthetic q(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 540
    invoke-direct {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n()V

    .line 541
    new-instance v0, Lcom/android/calendar/timezone/n;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/timezone/n;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Lcom/android/calendar/timezone/a;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/calendar/timezone/n;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 542
    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 753
    const v0, 0x7f12008c

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    sput-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    .line 754
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_0

    .line 892
    :goto_0
    return-void

    .line 757
    :cond_0
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    const v1, 0x2000003

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setImeOptions(I)V

    .line 759
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    new-instance v1, Lcom/android/calendar/timezone/b;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/b;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 876
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    new-instance v1, Lcom/android/calendar/timezone/c;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/c;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method

.method static synthetic r(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m:Z

    return v0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 898
    :cond_0
    return-void
.end method

.method static synthetic s(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s()V

    return-void
.end method

.method static synthetic t(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->D:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 336
    invoke-static {}, Lcom/android/calendar/timezone/r;->c()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    .line 338
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 349
    :goto_0
    return-void

    .line 342
    :cond_0
    iget v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->p:I

    .line 343
    invoke-static {v0}, Lcom/android/calendar/timezone/r;->a(I)I

    move-result v1

    .line 344
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V

    .line 345
    new-instance v0, Lcom/android/calendar/timezone/x;

    const v2, 0x7f04006b

    iget-object v3, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v2, v3}, Lcom/android/calendar/timezone/x;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    .line 346
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 347
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 348
    iput v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->o:I

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 352
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->x:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/timezone/r;->b(Ljava/lang/String;)Lcom/android/calendar/timezone/t;

    move-result-object v0

    .line 353
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lcom/android/calendar/timezone/t;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Lcom/android/calendar/timezone/t;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 354
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "timezone"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->setResult(ILandroid/content/Intent;)V

    .line 355
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->finish()V

    .line 356
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 359
    const v0, 0x7f04006b

    .line 360
    iget-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 361
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/calendar/timezone/TimezoneCityListItem;

    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    const v0, 0x7f04006a

    .line 364
    :cond_0
    new-instance v1, Lcom/android/calendar/timezone/x;

    iget-object v2, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->v:Ljava/lang/String;

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/android/calendar/timezone/x;-><init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    .line 365
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 366
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 901
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 902
    sget-object v1, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 904
    invoke-direct {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s()V

    .line 905
    return-void
.end method

.method public e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 908
    new-instance v1, Lcom/sec/android/touchwiz/widget/TwArrayIndexer;

    invoke-static {}, Lcom/android/calendar/timezone/r;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0453

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/sec/android/touchwiz/widget/TwArrayIndexer;-><init>(Ljava/util/List;Ljava/lang/CharSequence;)V

    .line 911
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    if-nez v0, :cond_1

    .line 912
    const v0, 0x7f0400b9

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    .line 913
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 914
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v4}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setHandlePosition(I)V

    .line 917
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    if-nez v0, :cond_2

    .line 954
    :cond_1
    :goto_0
    return-void

    .line 920
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    const v2, 0x7f10000a

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setIndexScrollViewTheme(I)V

    .line 921
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v2, "HK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SG"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 923
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setSimpleIndexHandleChar([Ljava/lang/String;)V

    .line 926
    invoke-static {}, Lcom/android/calendar/timezone/r;->d()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->D:Ljava/util/ArrayList;

    .line 933
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    new-instance v1, Lcom/android/calendar/timezone/d;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/d;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setOnIndexSelectedListener(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$OnIndexSelectedListener;)V

    goto :goto_0

    .line 928
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v4}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 929
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setIndexer(Lcom/sec/android/touchwiz/widget/TwAbstractIndexer;)V

    .line 930
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setSubscrollLimit(I)V

    goto :goto_1
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 957
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 958
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 959
    :cond_0
    const v0, 0x7f12008e

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    .line 960
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->j()V

    .line 961
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 962
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 963
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 966
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 967
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->B:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 968
    :cond_0
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    .line 971
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    if-nez v0, :cond_1

    .line 972
    const v0, 0x7f0400b8

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    .line 973
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 974
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setHandlePosition(I)V

    .line 977
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    if-nez v0, :cond_2

    .line 1005
    :cond_1
    :goto_0
    return-void

    .line 981
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    const v1, 0x7f10000a

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setIndexScrollViewTheme(I)V

    .line 982
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    new-instance v1, Lcom/android/calendar/timezone/e;

    invoke-direct {v1, p0}, Lcom/android/calendar/timezone/e;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setOnIndexSelectedListener(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$OnIndexSelectedListener;)V

    .line 1001
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setSubscrollLimit(I)V

    .line 1002
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setSimpleIndexHandleChar([Ljava/lang/String;)V

    .line 1003
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->invalidate()V

    goto :goto_0
.end method

.method public i()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1007
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 1009
    :cond_0
    const v0, 0x7f12008e

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    .line 1010
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->g()V

    .line 1011
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1012
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 1013
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->C:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1018
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 393
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 395
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->finish()V

    .line 396
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 389
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 390
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 184
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 185
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 188
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 189
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 190
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 192
    :cond_0
    const v0, 0x7f040020

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->setContentView(I)V

    .line 194
    const v0, 0x7f0f043d

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->setTitle(I)V

    .line 195
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 196
    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 197
    iput-object p0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->r:Landroid/app/Activity;

    .line 199
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->r:Landroid/app/Activity;

    const v1, 0x7f040084

    invoke-static {v0, v1}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->E:Landroid/widget/TextView;

    .line 200
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->i:Lcom/android/calendar/timezone/u;

    if-nez v0, :cond_1

    .line 201
    new-instance v0, Lcom/android/calendar/timezone/u;

    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/calendar/timezone/u;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->i:Lcom/android/calendar/timezone/u;

    .line 204
    :cond_1
    sget-object v1, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->j:Ljava/util/ArrayList;

    monitor-enter v1

    .line 205
    :try_start_0
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 206
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    new-instance v0, Lcom/android/calendar/timezone/r;

    invoke-direct {v0, p0}, Lcom/android/calendar/timezone/r;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->A:Lcom/android/calendar/timezone/r;

    .line 209
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 210
    const-string v1, "mVar"

    const/16 v2, 0x94

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->p:I

    .line 212
    invoke-direct {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->r()V

    .line 213
    invoke-direct {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m()V

    .line 214
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->e()V

    .line 215
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->h()V

    .line 216
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->f()V

    .line 217
    invoke-direct {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->p()V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k:Z

    .line 220
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->v:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->v:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 221
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a()V

    .line 224
    :cond_3
    return-void

    .line 206
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 257
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 259
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->A:Lcom/android/calendar/timezone/r;

    if-eqz v0, :cond_2

    .line 260
    sget-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    sget-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 263
    :cond_0
    sget-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    sget-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 266
    :cond_1
    sput-object v1, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    .line 267
    sput-object v1, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    .line 270
    :cond_2
    iput-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->s:Landroid/widget/ListView;

    .line 271
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_3

    .line 272
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 275
    :cond_3
    iput-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->t:Landroid/widget/ArrayAdapter;

    .line 276
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 277
    iget-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 279
    :cond_4
    iput-object v1, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    .line 280
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 400
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move v1, v0

    .line 420
    :goto_0
    return v1

    .line 402
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->finish()V

    goto :goto_0

    .line 405
    :sswitch_1
    iget-boolean v2, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n:Z

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n:Z

    .line 406
    iget-boolean v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n:Z

    if-eqz v0, :cond_1

    .line 407
    invoke-static {}, Lcom/android/calendar/timezone/r;->b()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    .line 408
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c()V

    .line 409
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->i()V

    goto :goto_0

    .line 411
    :cond_1
    invoke-static {}, Lcom/android/calendar/timezone/r;->c()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->y:Ljava/util/ArrayList;

    .line 412
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c()V

    .line 413
    invoke-virtual {p0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->f()V

    goto :goto_0

    .line 400
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 247
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 248
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 228
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 235
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 240
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 241
    iget-boolean v0, p0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k:Z

    if-eqz v0, :cond_0

    .line 243
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 430
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 426
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 252
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 253
    return-void
.end method
