.class final Lcom/android/calendar/timezone/z;
.super Ljava/lang/Object;
.source "TimezoneCityListItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/android/calendar/timezone/TimezoneCityListItem;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 100
    new-instance v1, Lcom/android/calendar/timezone/TimezoneCityListItem;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Lcom/android/calendar/timezone/z;)V

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Lcom/android/calendar/timezone/TimezoneCityListItem;I)I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Lcom/android/calendar/timezone/TimezoneCityListItem;Ljava/lang/String;)Ljava/lang/String;

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/timezone/TimezoneCityListItem;->b(Lcom/android/calendar/timezone/TimezoneCityListItem;Ljava/lang/String;)Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/timezone/TimezoneCityListItem;->c(Lcom/android/calendar/timezone/TimezoneCityListItem;Ljava/lang/String;)Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/calendar/timezone/TimezoneCityListItem;->b(Lcom/android/calendar/timezone/TimezoneCityListItem;I)I

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Lcom/android/calendar/timezone/TimezoneCityListItem;J)J

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v1, v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Lcom/android/calendar/timezone/TimezoneCityListItem;Z)Z

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->c(Lcom/android/calendar/timezone/TimezoneCityListItem;I)I

    .line 109
    return-object v1

    .line 107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)[Lcom/android/calendar/timezone/TimezoneCityListItem;
    .locals 1

    .prologue
    .line 113
    new-array v0, p1, [Lcom/android/calendar/timezone/TimezoneCityListItem;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lcom/android/calendar/timezone/z;->a(Landroid/os/Parcel;)Lcom/android/calendar/timezone/TimezoneCityListItem;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lcom/android/calendar/timezone/z;->a(I)[Lcom/android/calendar/timezone/TimezoneCityListItem;

    move-result-object v0

    return-object v0
.end method
