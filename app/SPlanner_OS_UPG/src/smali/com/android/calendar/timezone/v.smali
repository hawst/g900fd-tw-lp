.class public Lcom/android/calendar/timezone/v;
.super Landroid/widget/ArrayAdapter;
.source "TimezoneAdapter.java"


# instance fields
.field private a:Ljava/util/LinkedHashMap;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:J

.field private e:Ljava/util/Date;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 255
    const v0, 0x7f040097

    const v1, 0x1020014

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 256
    iput-object p1, p0, Lcom/android/calendar/timezone/v;->b:Landroid/content/Context;

    .line 257
    iput-object p2, p0, Lcom/android/calendar/timezone/v;->c:Ljava/lang/String;

    .line 258
    iput-wide p3, p0, Lcom/android/calendar/timezone/v;->d:J

    .line 259
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lcom/android/calendar/timezone/v;->d:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/android/calendar/timezone/v;->e:Ljava/util/Date;

    .line 260
    invoke-virtual {p0}, Lcom/android/calendar/timezone/v;->a()V

    .line 261
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/timezone/v;)J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/android/calendar/timezone/v;->d:J

    return-wide v0
.end method

.method private a(Landroid/content/res/Resources;)V
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 457
    iget-object v1, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    if-nez v1, :cond_4

    .line 458
    const v1, 0x7f090035

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 459
    const v1, 0x7f090034

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 460
    const v1, 0x7f090003

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 461
    const/high16 v1, 0x7f090000

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 462
    const v1, 0x7f090038

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 464
    array-length v7, v2

    .line 465
    array-length v8, v4

    .line 466
    new-instance v1, Ljava/util/LinkedHashMap;

    add-int v9, v7, v8

    invoke-direct {v1, v9}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    .line 468
    array-length v1, v2

    array-length v9, v3

    if-eq v1, v9, :cond_0

    .line 469
    const-string v1, "TimezoneAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ids length ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    array-length v10, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") and labels length("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    array-length v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") should be equal but aren\'t."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v1, v0

    .line 472
    :goto_0
    if-ge v1, v7, :cond_1

    .line 473
    iget-object v9, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    aget-object v10, v2, v1

    new-instance v11, Lcom/android/calendar/timezone/w;

    aget-object v12, v2, v1

    aget-object v13, v3, v1

    invoke-direct {v11, p0, v12, v13}, Lcom/android/calendar/timezone/w;-><init>(Lcom/android/calendar/timezone/v;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10, v11}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 476
    :cond_1
    array-length v1, v4

    array-length v2, v5

    if-ne v1, v2, :cond_2

    array-length v1, v4

    array-length v2, v6

    if-eq v1, v2, :cond_3

    .line 477
    :cond_2
    const-string v1, "TimezoneAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "timezone length ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") and city length("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") and uid length("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") should be equal but aren\'t."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :cond_3
    :goto_1
    if-ge v0, v8, :cond_4

    .line 482
    iget-object v1, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v4, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v6, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/calendar/timezone/w;

    aget-object v7, v4, v0

    aget-object v9, v5, v0

    invoke-direct {v3, p0, v7, v9}, Lcom/android/calendar/timezone/w;-><init>(Lcom/android/calendar/timezone/v;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 485
    :cond_4
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 376
    invoke-static {p1}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 377
    const-string v1, "preferences_recent_timezones"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 379
    if-nez v1, :cond_0

    .line 380
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 386
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lt v1, v3, :cond_1

    .line 387
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 382
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 389
    :cond_1
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    const-string v1, ","

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 391
    const-string v1, "preferences_recent_timezones"

    invoke-static {p1, v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/w;

    .line 272
    if-nez v0, :cond_0

    .line 273
    const/4 v0, -0x1

    .line 275
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/v;->getPosition(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public a()V
    .locals 7

    .prologue
    .line 290
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 293
    iget-object v0, p0, Lcom/android/calendar/timezone/v;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 299
    iget-object v0, p0, Lcom/android/calendar/timezone/v;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 300
    const-string v2, "preferences_recent_timezones"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_0

    .line 302
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 303
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 304
    invoke-virtual {v1, v4}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 308
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/timezone/v;->clear()V

    .line 310
    const-class v2, Lcom/android/calendar/timezone/v;

    monitor-enter v2

    .line 311
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/timezone/v;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/timezone/v;->a(Landroid/content/res/Resources;)V

    .line 312
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    .line 314
    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 315
    iget-object v4, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 316
    if-nez v0, :cond_2

    .line 317
    const-string v0, "GMT"

    .line 321
    :cond_2
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    .line 326
    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 327
    iget-object v4, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    new-instance v5, Lcom/android/calendar/timezone/w;

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/v;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p0, v0, v6}, Lcom/android/calendar/timezone/w;-><init>(Lcom/android/calendar/timezone/v;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    :cond_3
    iget-object v4, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/timezone/v;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 337
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 346
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/timezone/v;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    iput-object p1, p0, Lcom/android/calendar/timezone/v;->c:Ljava/lang/String;

    .line 348
    invoke-virtual {p0}, Lcom/android/calendar/timezone/v;->a()V

    .line 350
    :cond_0
    return-void
.end method

.method public b()[[Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 403
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/CharSequence;

    .line 404
    new-instance v4, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 405
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 406
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 408
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/timezone/w;

    .line 409
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 410
    const-string v8, "id"

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    const-string v8, "name"

    invoke-virtual {v1}, Lcom/android/calendar/timezone/w;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    const-string v8, "offset"

    invoke-static {v1}, Lcom/android/calendar/timezone/w;->b(Lcom/android/calendar/timezone/w;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 415
    goto :goto_0

    .line 418
    :cond_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    .line 419
    :goto_1
    if-ge v2, v4, :cond_1

    .line 420
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 421
    aget-object v6, v0, v3

    const-string v7, "id"

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 422
    const/4 v6, 0x1

    aget-object v6, v0, v6

    const-string v7, "name"

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v2

    .line 419
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 424
    :cond_1
    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 436
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 437
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 439
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 440
    iget-object v2, p0, Lcom/android/calendar/timezone/v;->e:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v2

    const/4 v4, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 441
    const-string v2, "GMT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 442
    :cond_0
    const/4 v0, 0x0

    .line 443
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/w;

    .line 444
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 445
    invoke-static {v0}, Lcom/android/calendar/timezone/w;->c(Lcom/android/calendar/timezone/w;)Ljava/lang/String;

    move-result-object p1

    .line 451
    :cond_1
    :goto_1
    return-object p1

    .line 447
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 448
    goto :goto_0

    :cond_3
    move-object p1, v0

    .line 451
    goto :goto_1
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/android/calendar/timezone/v;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/w;

    .line 490
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/android/calendar/timezone/w;->c(Lcom/android/calendar/timezone/w;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 495
    :cond_0
    :goto_0
    return-object p1

    .line 493
    :cond_1
    invoke-static {v0}, Lcom/android/calendar/timezone/w;->c(Lcom/android/calendar/timezone/w;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
