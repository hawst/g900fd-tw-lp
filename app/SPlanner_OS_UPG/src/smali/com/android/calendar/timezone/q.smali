.class public Lcom/android/calendar/timezone/q;
.super Ljava/util/TimerTask;
.source "CalendarTimezoneActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 732
    iput-object p1, p0, Lcom/android/calendar/timezone/q;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 733
    iput-object p2, p0, Lcom/android/calendar/timezone/q;->b:Ljava/lang/String;

    .line 734
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 738
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/timezone/q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/calendar/timezone/u;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/android/calendar/timezone/q;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    iget-object v0, v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 740
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 741
    const-string v2, "Source"

    const-string v3, "TimoutTask"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 743
    iget-object v1, p0, Lcom/android/calendar/timezone/q;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    iget-object v1, v1, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 744
    iget-object v0, p0, Lcom/android/calendar/timezone/q;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->g(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    .line 746
    :cond_0
    return-void
.end method
