.class Lcom/android/calendar/timezone/b;
.super Ljava/lang/Object;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 759
    iput-object p1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 12

    .prologue
    .line 774
    .line 775
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 777
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 778
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, p1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 791
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v2

    .line 792
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_7

    .line 793
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z

    .line 794
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 795
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 797
    invoke-static {}, Lcom/android/calendar/timezone/r;->a()[Lcom/android/calendar/timezone/t;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_8

    aget-object v6, v4, v0

    .line 798
    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v7, v8, :cond_5

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v7, v8, :cond_5

    .line 797
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 779
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x64

    if-lt v0, v2, :cond_3

    .line 781
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x64

    if-gt v0, v2, :cond_0

    .line 782
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, p1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 784
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x64

    if-le v0, v2, :cond_4

    .line 785
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const/4 v2, 0x0

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 787
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, p1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 802
    :cond_5
    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_6

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_1

    .line 805
    :cond_6
    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 806
    const-string v7, " / "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 807
    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 809
    iget-object v7, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v7}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v7

    new-instance v8, Lcom/android/calendar/timezone/TimezoneCityListItem;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->g()I

    move-result v11

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->d()I

    move-result v6

    invoke-direct {v8, v9, v10, v11, v6}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 812
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    goto/16 :goto_2

    .line 816
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 817
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {}, Lcom/android/calendar/timezone/r;->c()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 821
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_a

    .line 822
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 827
    :goto_3
    const v0, 0x7f04006b

    .line 829
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_b

    .line 830
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_9

    .line 831
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 832
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->o(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 833
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 834
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->bringToFront()V

    .line 845
    :cond_9
    :goto_4
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    new-instance v2, Lcom/android/calendar/timezone/x;

    iget-object v3, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v3}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->p(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v4}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v5}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/android/calendar/timezone/x;-><init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;

    .line 846
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->q(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 853
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x64

    if-gt v0, v1, :cond_c

    .line 854
    const/4 v0, 0x0

    .line 872
    :goto_5
    return v0

    .line 824
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    goto/16 :goto_3

    .line 837
    :cond_b
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_9

    .line 838
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 839
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->m(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->bringToFront()V

    .line 840
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->bringToFront()V

    .line 841
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->o(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 842
    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->n(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 857
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 858
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 863
    :goto_6
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->twSetSelection(I)V

    .line 865
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->r(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 866
    const/4 v0, 0x0

    goto :goto_5

    .line 860
    :cond_d
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_6

    .line 869
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z

    .line 870
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    iget-object v0, v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 872
    const/4 v0, 0x0

    goto/16 :goto_5
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 762
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 763
    const/4 v0, 0x1

    .line 767
    :goto_0
    return v0

    .line 765
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/timezone/b;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 766
    sget-object v2, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    move v0, v1

    .line 767
    goto :goto_0
.end method
