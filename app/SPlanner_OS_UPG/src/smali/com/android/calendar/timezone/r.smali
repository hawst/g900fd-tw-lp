.class public Lcom/android/calendar/timezone/r;
.super Ljava/lang/Object;
.source "CalendarTimezoneCity.java"


# static fields
.field public static a:Ljava/util/HashMap;

.field public static b:Landroid/util/SparseArray;

.field public static c:Ljava/util/HashMap;

.field public static d:Ljava/util/HashMap;

.field private static e:[Lcom/android/calendar/timezone/t;

.field private static f:[Lcom/android/calendar/timezone/t;

.field private static g:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    sput-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    .line 47
    sput-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    .line 48
    sput-object v0, Lcom/android/calendar/timezone/r;->c:Ljava/util/HashMap;

    .line 49
    sput-object v0, Lcom/android/calendar/timezone/r;->d:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, Lcom/android/calendar/timezone/r;->b(Landroid/content/Context;)V

    .line 53
    invoke-static {p1}, Lcom/android/calendar/timezone/r;->c(Landroid/content/Context;)V

    .line 54
    sput-object p1, Lcom/android/calendar/timezone/r;->g:Landroid/content/Context;

    .line 55
    return-void
.end method

.method public static a(I)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 181
    .line 182
    invoke-static {}, Lcom/android/calendar/timezone/r;->a()[Lcom/android/calendar/timezone/t;

    move-result-object v2

    .line 183
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 184
    array-length v4, v2

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v2, v1

    .line 185
    iget v5, v5, Lcom/android/calendar/timezone/t;->e:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 184
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 187
    :cond_0
    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 188
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 190
    if-ne p0, v0, :cond_2

    .line 195
    :cond_1
    return v1

    .line 193
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 194
    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Lcom/android/calendar/timezone/t;
    .locals 1

    .prologue
    .line 243
    sget-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 244
    sget-object v0, Lcom/android/calendar/timezone/r;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/timezone/r;->a(Landroid/content/Context;)V

    .line 246
    :cond_0
    sget-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/t;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 58
    invoke-static {p0}, Lcom/android/calendar/timezone/r;->b(Landroid/content/Context;)V

    .line 59
    invoke-static {p0}, Lcom/android/calendar/timezone/r;->c(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method public static a()[Lcom/android/calendar/timezone/t;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 144
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    .line 145
    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    .line 146
    sget-object v4, Lcom/android/calendar/timezone/r;->e:[Lcom/android/calendar/timezone/t;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    .line 147
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 148
    if-eqz v6, :cond_0

    .line 149
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 155
    :cond_1
    if-eqz v6, :cond_0

    .line 156
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 161
    :cond_2
    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 162
    invoke-virtual {v2}, Ljava/util/TreeSet;->size()I

    move-result v2

    new-array v4, v2, [Lcom/android/calendar/timezone/t;

    .line 165
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 166
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 167
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 170
    :cond_3
    new-instance v1, Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v1

    .line 171
    invoke-static {v2, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 173
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 174
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v3, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/t;

    aput-object v0, v4, v1

    move v1, v2

    .line 175
    goto :goto_3

    .line 176
    :cond_4
    sput-object v4, Lcom/android/calendar/timezone/r;->f:[Lcom/android/calendar/timezone/t;

    .line 177
    return-object v4
.end method

.method public static b(Ljava/lang/String;)Lcom/android/calendar/timezone/t;
    .locals 1

    .prologue
    .line 332
    sget-object v0, Lcom/android/calendar/timezone/r;->d:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 333
    sget-object v0, Lcom/android/calendar/timezone/r;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/timezone/r;->a(Landroid/content/Context;)V

    .line 335
    :cond_0
    sget-object v0, Lcom/android/calendar/timezone/r;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/t;

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 250
    sget-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    .line 251
    sget-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 252
    if-eqz v0, :cond_0

    .line 257
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 255
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 257
    goto :goto_0
.end method

.method public static b()Ljava/util/ArrayList;
    .locals 10

    .prologue
    .line 199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 200
    new-instance v0, Lcom/android/calendar/timezone/s;

    invoke-direct {v0}, Lcom/android/calendar/timezone/s;-><init>()V

    .line 210
    sget-object v2, Lcom/android/calendar/timezone/r;->f:[Lcom/android/calendar/timezone/t;

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 211
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 212
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 213
    sget-object v4, Lcom/android/calendar/timezone/r;->f:[Lcom/android/calendar/timezone/t;

    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    .line 214
    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 216
    :cond_0
    new-instance v7, Lcom/android/calendar/timezone/TimezoneCityListItem;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Lcom/android/calendar/timezone/t;->g()I

    move-result v6

    invoke-direct {v7, v8, v9, v6}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_2
    return-object v1
.end method

.method public static declared-synchronized b(Landroid/content/Context;)V
    .locals 16

    .prologue
    .line 63
    const-class v10, Lcom/android/calendar/timezone/r;

    monitor-enter v10

    :try_start_0
    sget-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 64
    sget-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 65
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    .line 67
    :cond_0
    sget-object v0, Lcom/android/calendar/timezone/r;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 68
    sget-object v0, Lcom/android/calendar/timezone/r;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 69
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/timezone/r;->c:Ljava/util/HashMap;

    .line 71
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/calendar/timezone/r;->c:Ljava/util/HashMap;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    .line 73
    sget-object v0, Lcom/android/calendar/timezone/r;->d:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 74
    sget-object v0, Lcom/android/calendar/timezone/r;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 75
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/timezone/r;->d:Ljava/util/HashMap;

    .line 77
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/calendar/timezone/r;->d:Ljava/util/HashMap;

    .line 78
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v11

    .line 79
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v12

    .line 80
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v13

    .line 81
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v14

    .line 82
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v15

    .line 83
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 84
    invoke-static {}, Lcom/android/calendar/dz;->m()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 85
    const/16 v2, 0x21

    const-string v3, " "

    aput-object v3, v13, v2

    .line 87
    :cond_3
    const/16 v2, 0xab

    const-string v3, " "

    aput-object v3, v13, v2

    .line 88
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 89
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09003e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    move-object v9, v1

    .line 92
    :goto_0
    const-string v0, ""

    .line 95
    array-length v0, v11

    new-array v0, v0, [Lcom/android/calendar/timezone/t;

    sput-object v0, Lcom/android/calendar/timezone/r;->e:[Lcom/android/calendar/timezone/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    const/4 v0, 0x0

    move v7, v0

    :goto_1
    :try_start_1
    sget-object v0, Lcom/android/calendar/timezone/r;->e:[Lcom/android/calendar/timezone/t;

    array-length v0, v0

    if-ge v7, v0, :cond_7

    .line 98
    aget-object v1, v11, v7

    .line 99
    aget-object v0, v12, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 100
    invoke-static {}, Lcom/android/calendar/dz;->n()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x40

    if-ne v0, v2, :cond_8

    .line 101
    const-string v0, "ro.csc.countryiso_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "HK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 102
    const/16 v0, 0xab

    move v6, v0

    .line 107
    :goto_2
    aget-object v2, v13, v6

    .line 108
    aget-object v3, v14, v7

    .line 109
    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    .line 110
    new-instance v0, Lcom/android/calendar/timezone/t;

    aget-object v5, v15, v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/timezone/t;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 111
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 112
    aget-object v3, v9, v7

    aget-object v4, v8, v6

    invoke-virtual {v0, v3, v4}, Lcom/android/calendar/timezone/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_4
    sget-object v3, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 115
    sget-object v3, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v3, Lcom/android/calendar/timezone/r;->c:Ljava/util/HashMap;

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    :goto_3
    sget-object v3, Lcom/android/calendar/timezone/r;->e:[Lcom/android/calendar/timezone/t;

    aput-object v0, v3, v7

    .line 121
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " / "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 122
    sget-object v2, Lcom/android/calendar/timezone/r;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_1

    .line 104
    :cond_5
    const/16 v0, 0x11

    move v6, v0

    goto/16 :goto_2

    .line 118
    :cond_6
    sget-object v3, Lcom/android/calendar/timezone/r;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 124
    :catch_0
    move-exception v0

    .line 125
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 129
    :cond_7
    :goto_4
    monitor-exit v10

    return-void

    .line 126
    :catch_1
    move-exception v0

    .line 127
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0

    :cond_8
    move v6, v0

    goto/16 :goto_2

    :cond_9
    move-object v8, v0

    move-object v9, v1

    goto/16 :goto_0
.end method

.method public static c()Ljava/util/ArrayList;
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 223
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 224
    invoke-static {}, Lcom/android/calendar/timezone/r;->a()[Lcom/android/calendar/timezone/t;

    move-result-object v3

    .line 225
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 226
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 227
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 228
    array-length v7, v3

    move v0, v1

    :goto_0
    if-ge v0, v7, :cond_2

    aget-object v8, v3, v0

    .line 229
    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v5}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v5}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 231
    :cond_0
    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 232
    const-string v9, " / "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 233
    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 234
    new-instance v9, Lcom/android/calendar/timezone/TimezoneCityListItem;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->c()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->g()I

    move-result v12

    invoke-virtual {v8}, Lcom/android/calendar/timezone/t;->d()I

    move-result v8

    invoke-direct {v9, v10, v11, v12, v8}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 228
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239
    :cond_2
    return-object v4
.end method

.method public static declared-synchronized c(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 132
    const-class v1, Lcom/android/calendar/timezone/r;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 133
    sget-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 134
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    .line 136
    :cond_0
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    .line 137
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f090000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 138
    const/4 v0, 0x0

    :goto_0
    sget-object v3, Lcom/android/calendar/timezone/r;->e:[Lcom/android/calendar/timezone/t;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 139
    sget-object v3, Lcom/android/calendar/timezone/r;->b:Landroid/util/SparseArray;

    aget-object v4, v2, v0

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 141
    :cond_1
    monitor-exit v1

    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d()Ljava/util/ArrayList;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 261
    invoke-static {}, Lcom/android/calendar/timezone/r;->a()[Lcom/android/calendar/timezone/t;

    move-result-object v1

    .line 262
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 263
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 264
    array-length v3, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 265
    invoke-virtual {v4}, Lcom/android/calendar/timezone/t;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 268
    :cond_0
    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 269
    invoke-virtual {v4}, Lcom/android/calendar/timezone/t;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 272
    :cond_1
    return-object v2
.end method
