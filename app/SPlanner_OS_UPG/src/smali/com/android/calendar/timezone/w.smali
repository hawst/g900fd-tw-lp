.class public Lcom/android/calendar/timezone/w;
.super Ljava/lang/Object;
.source "TimezoneAdapter.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:Ljava/lang/String;

.field final synthetic b:Lcom/android/calendar/timezone/v;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Z

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/calendar/timezone/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 81
    iput-object p1, p0, Lcom/android/calendar/timezone/w;->b:Lcom/android/calendar/timezone/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p2, p0, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    .line 84
    invoke-static {p2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/timezone/w;->e:Z

    .line 86
    invoke-static {p1}, Lcom/android/calendar/timezone/v;->a(Lcom/android/calendar/timezone/v;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/timezone/w;->d:I

    .line 87
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/timezone/w;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/android/calendar/timezone/w;->d:I

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/timezone/w;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/android/calendar/timezone/w;)I
    .locals 2

    .prologue
    .line 216
    iget v0, p0, Lcom/android/calendar/timezone/w;->d:I

    iget v1, p1, Lcom/android/calendar/timezone/w;->d:I

    if-ne v0, v1, :cond_0

    .line 217
    const/4 v0, 0x0

    .line 219
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/calendar/timezone/w;->d:I

    iget v1, p1, Lcom/android/calendar/timezone/w;->d:I

    if-ge v0, v1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 99
    iget v0, p0, Lcom/android/calendar/timezone/w;->d:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    const-string v2, "GMT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    iget v2, p0, Lcom/android/calendar/timezone/w;->d:I

    if-gez v2, :cond_0

    .line 104
    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 109
    :goto_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 110
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 112
    new-instance v3, Ljava/util/Date;

    int-to-long v4, v0

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 114
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const/4 v0, 0x0

    const-string v2, "("

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string v0, ") "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    const-string v2, "GMT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 106
    :cond_0
    const/16 v2, 0x2b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->b:Lcom/android/calendar/timezone/v;

    iget-object v2, p0, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/calendar/timezone/v;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public b()V
    .locals 6

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 133
    :cond_0
    iget v0, p0, Lcom/android/calendar/timezone/w;->d:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    const-string v2, "GMT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    iget v2, p0, Lcom/android/calendar/timezone/w;->d:I

    if-gez v2, :cond_3

    .line 138
    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 143
    :goto_1
    int-to-long v2, v0

    const-wide/32 v4, 0x36ee80

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 144
    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    const v2, 0xea60

    div-int/2addr v0, v2

    .line 147
    rem-int/lit8 v0, v0, 0x3c

    .line 149
    const/16 v2, 0xa

    if-ge v0, v2, :cond_1

    .line 150
    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 153
    const/4 v0, 0x0

    const-string v2, "("

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string v0, ") "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    const-string v2, "GMT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 158
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    :goto_2
    iget-boolean v0, p0, Lcom/android/calendar/timezone/w;->e:Z

    if-eqz v0, :cond_2

    .line 169
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/w;->f:Ljava/lang/String;

    goto :goto_0

    .line 140
    :cond_3
    const/16 v2, 0x2b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 160
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 61
    check-cast p1, Lcom/android/calendar/timezone/w;

    invoke-virtual {p0, p1}, Lcom/android/calendar/timezone/w;->a(Lcom/android/calendar/timezone/w;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 184
    if-ne p0, p1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v0

    .line 187
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 188
    goto :goto_0

    .line 190
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 191
    goto :goto_0

    .line 193
    :cond_3
    check-cast p1, Lcom/android/calendar/timezone/w;

    .line 194
    iget-object v2, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 195
    iget-object v2, p1, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 196
    goto :goto_0

    .line 198
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 199
    goto :goto_0

    .line 201
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 202
    iget-object v2, p1, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 203
    goto :goto_0

    .line 205
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 206
    goto :goto_0

    .line 208
    :cond_7
    iget v2, p0, Lcom/android/calendar/timezone/w;->d:I

    iget v3, p1, Lcom/android/calendar/timezone/w;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 209
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 174
    .line 176
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 177
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 178
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/android/calendar/timezone/w;->d:I

    add-int/2addr v0, v1

    .line 179
    return v0

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 177
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/timezone/w;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/android/calendar/timezone/w;->b()V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/timezone/w;->f:Ljava/lang/String;

    return-object v0
.end method
