.class public Lcom/android/calendar/timezone/TimezoneCityListItem;
.super Ljava/lang/Object;
.source "TimezoneCityListItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:I

.field private g:I

.field private h:J

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/android/calendar/timezone/z;

    invoke-direct {v0}, Lcom/android/calendar/timezone/z;-><init>()V

    sput-object v0, Lcom/android/calendar/timezone/TimezoneCityListItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/timezone/z;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->a:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->b:Ljava/lang/String;

    .line 51
    iput p3, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->i:I

    .line 52
    iput p4, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->f:I

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/timezone/TimezoneCityListItem;I)I
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->f:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/timezone/TimezoneCityListItem;J)J
    .locals 1

    .prologue
    .line 24
    iput-wide p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->h:J

    return-wide p1
.end method

.method static synthetic a(Lcom/android/calendar/timezone/TimezoneCityListItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/timezone/TimezoneCityListItem;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/timezone/TimezoneCityListItem;I)I
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->g:I

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/timezone/TimezoneCityListItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/android/calendar/timezone/TimezoneCityListItem;I)I
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->i:I

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/timezone/TimezoneCityListItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->c:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->d:Z

    .line 69
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->d:Z

    return v0
.end method

.method d()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->e:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->g:I

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->i:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 88
    iget v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    iget-object v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget-wide v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 94
    invoke-virtual {p0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    iget v0, p0, Lcom/android/calendar/timezone/TimezoneCityListItem;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    return-void

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
