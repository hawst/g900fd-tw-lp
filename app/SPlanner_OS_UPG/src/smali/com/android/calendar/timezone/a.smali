.class Lcom/android/calendar/timezone/a;
.super Landroid/os/Handler;
.source "CalendarTimezoneActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    .line 117
    :try_start_0
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :try_start_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 119
    const-string v2, "Source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkLocationListener"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 120
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 121
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v2

    const-string v3, "Latitude"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    const-string v3, "Longitude"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/android/calendar/timezone/u;->a(DD)Ljava/lang/String;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_1

    .line 124
    const-string v2, "OK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 125
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/timezone/u;->b()Ljava/util/Map;

    move-result-object v2

    .line 127
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 129
    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 132
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 134
    invoke-static {v0}, Lcom/android/calendar/timezone/r;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 179
    :goto_1
    return-void

    .line 138
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/util/List;)V

    .line 141
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z

    .line 175
    :cond_1
    :goto_2
    monitor-exit v1

    goto :goto_1

    .line 144
    :cond_2
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/timezone/u;->d()I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/timezone/r;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/util/List;)V

    .line 150
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z

    goto :goto_2

    .line 153
    :cond_3
    const-string v2, "Source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "DelayTimerTask_external"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/timezone/u;->a()Ljava/lang/String;

    move-result-object v0

    .line 155
    const-string v2, "undefined"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 156
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/timezone/u;->b()Ljava/util/Map;

    move-result-object v2

    .line 158
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 160
    if-eqz v0, :cond_1

    .line 161
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 163
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 164
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 165
    invoke-static {v0}, Lcom/android/calendar/timezone/r;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 168
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 170
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/util/List;)V

    .line 171
    iget-object v0, p0, Lcom/android/calendar/timezone/a;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2
.end method
