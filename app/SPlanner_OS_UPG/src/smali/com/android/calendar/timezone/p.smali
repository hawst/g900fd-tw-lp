.class public Lcom/android/calendar/timezone/p;
.super Ljava/lang/Object;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 1

    .prologue
    .line 690
    iput-object p1, p0, Lcom/android/calendar/timezone/p;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 688
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/timezone/p;->b:Z

    .line 691
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 694
    iget-boolean v0, p0, Lcom/android/calendar/timezone/p;->b:Z

    return v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 699
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/timezone/p;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->g(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    .line 700
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/timezone/u;->f()Z

    .line 702
    iget-object v0, p0, Lcom/android/calendar/timezone/p;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    iget-object v0, v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 704
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 706
    const-string v2, "Source"

    const-string v3, "NetworkLocationListener"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const-string v2, "Latitude"

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 708
    const-string v2, "Longitude"

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 710
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 712
    iget-object v1, p0, Lcom/android/calendar/timezone/p;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    iget-object v1, v1, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 717
    :goto_0
    return-void

    .line 714
    :catch_0
    move-exception v0

    .line 715
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 720
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 723
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 726
    return-void
.end method
