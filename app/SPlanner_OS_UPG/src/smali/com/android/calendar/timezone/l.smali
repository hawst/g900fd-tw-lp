.class Lcom/android/calendar/timezone/l;
.super Ljava/lang/Object;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/android/calendar/timezone/l;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 517
    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 518
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 520
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 521
    invoke-virtual {p1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 523
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 524
    iget-object v0, p0, Lcom/android/calendar/timezone/l;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 525
    aget v0, v1, v7

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 526
    iget-object v4, p0, Lcom/android/calendar/timezone/l;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v4}, Lcom/android/calendar/hj;->e(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 527
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 530
    :cond_0
    iget-object v4, p0, Lcom/android/calendar/timezone/l;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 531
    const/16 v5, 0x35

    aget v1, v1, v6

    sub-int v1, v3, v1

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {v4, v5, v1, v0}, Landroid/widget/Toast;->setGravity(III)V

    .line 532
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 533
    return v7
.end method
