.class Lcom/android/calendar/timezone/k;
.super Ljava/lang/Object;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 492
    iput-object p1, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 494
    iget-object v0, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->h(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z

    .line 496
    invoke-virtual {p1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 497
    invoke-virtual {p1, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 498
    iget-object v0, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->i(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    .line 499
    invoke-virtual {p1, v1}, Landroid/view/View;->setClickable(Z)V

    .line 500
    invoke-virtual {p1, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 501
    iget-object v0, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, v3}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z

    .line 502
    iget-object v0, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->invalidateOptionsMenu()V

    .line 504
    iget-object v0, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 506
    sget-object v0, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->j(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/timezone/k;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->j(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 511
    :cond_0
    return-void
.end method
