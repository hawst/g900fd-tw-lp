.class public Lcom/android/calendar/timezone/x;
.super Landroid/widget/ArrayAdapter;
.source "TimezoneCityListAdapter.java"


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:I

.field private c:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Z

.field private final f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/timezone/x;->f:I

    .line 56
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/x;->a:Landroid/view/LayoutInflater;

    .line 57
    iput p2, p0, Lcom/android/calendar/timezone/x;->b:I

    .line 58
    iput-object p1, p0, Lcom/android/calendar/timezone/x;->c:Landroid/content/Context;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 52
    iput v1, p0, Lcom/android/calendar/timezone/x;->f:I

    .line 63
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/timezone/x;->a:Landroid/view/LayoutInflater;

    .line 64
    iput p2, p0, Lcom/android/calendar/timezone/x;->b:I

    .line 65
    iput-object p1, p0, Lcom/android/calendar/timezone/x;->c:Landroid/content/Context;

    .line 66
    if-nez p4, :cond_0

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    .line 71
    :goto_0
    iput-boolean v1, p0, Lcom/android/calendar/timezone/x;->e:Z

    .line 72
    return-void

    .line 69
    :cond_0
    iput-object p4, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 180
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 181
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 228
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :sswitch_0
    const-string v3, "\\?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 186
    :sswitch_1
    const-string v3, "\\*"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 189
    :sswitch_2
    const-string v3, "\\+"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 192
    :sswitch_3
    const-string v3, "\\&"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 195
    :sswitch_4
    const-string v3, "\\."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 198
    :sswitch_5
    const-string v3, "\\\\"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 201
    :sswitch_6
    const-string v3, "\\:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 204
    :sswitch_7
    const-string v3, "\\^"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 207
    :sswitch_8
    const-string v3, "\\$"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 210
    :sswitch_9
    const-string v3, "\\("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 213
    :sswitch_a
    const-string v3, "\\)"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 216
    :sswitch_b
    const-string v3, "\\["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 219
    :sswitch_c
    const-string v3, "\\]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 222
    :sswitch_d
    const-string v3, "\\{"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 225
    :sswitch_e
    const-string v3, "\\}"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 231
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 181
    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_8
        0x26 -> :sswitch_3
        0x28 -> :sswitch_9
        0x29 -> :sswitch_a
        0x2a -> :sswitch_1
        0x2b -> :sswitch_2
        0x2e -> :sswitch_4
        0x3a -> :sswitch_6
        0x3f -> :sswitch_0
        0x5b -> :sswitch_b
        0x5c -> :sswitch_5
        0x5d -> :sswitch_c
        0x5e -> :sswitch_7
        0x7b -> :sswitch_d
        0x7d -> :sswitch_e
    .end sparse-switch
.end method

.method public static a(C)Z
    .locals 1

    .prologue
    .line 75
    const/16 v0, 0x900

    if-lt p0, v0, :cond_0

    const/16 v0, 0xdff

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;[I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 163
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 165
    invoke-static {v3}, Lcom/android/calendar/timezone/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 166
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 167
    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 168
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    .line 170
    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    aput v2, p2, v1

    .line 171
    aget v1, p2, v1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    aput v1, p2, v0

    .line 174
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const v5, 0x7f120215

    const/4 v7, 0x2

    const/16 v11, 0x21

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 80
    if-nez p2, :cond_0

    .line 81
    iget-object v0, p0, Lcom/android/calendar/timezone/x;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/android/calendar/timezone/x;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 83
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/timezone/x;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    .line 84
    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a()Ljava/lang/String;

    move-result-object v2

    .line 85
    const-string v1, " / "

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 86
    if-eqz v0, :cond_1

    .line 87
    new-instance v4, Lcom/android/calendar/timezone/y;

    invoke-direct {v4}, Lcom/android/calendar/timezone/y;-><init>()V

    .line 88
    iget v1, p0, Lcom/android/calendar/timezone/x;->b:I

    packed-switch v1, :pswitch_data_0

    .line 146
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/android/calendar/timezone/y;->a:Landroid/widget/TextView;

    .line 147
    iget-object v1, v4, Lcom/android/calendar/timezone/y;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 152
    :cond_1
    :goto_0
    return-object p2

    .line 90
    :pswitch_0
    const v1, 0x7f120216

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v4, Lcom/android/calendar/timezone/y;->c:Landroid/widget/LinearLayout;

    .line 91
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/android/calendar/timezone/y;->a:Landroid/widget/TextView;

    .line 92
    new-array v5, v7, [I

    .line 93
    iget-object v1, p0, Lcom/android/calendar/timezone/x;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0b00d3

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 94
    array-length v1, v3

    if-lt v1, v7, :cond_2

    const-string v1, ""

    aget-object v7, v3, v10

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, " "

    aget-object v7, v3, v10

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 95
    :cond_2
    aget-object v1, v3, v9

    .line 97
    :goto_1
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 98
    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->e()I

    move-result v3

    if-ne v3, v10, :cond_3

    .line 99
    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v10}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-interface {v2, v3, v9, v7, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 102
    :cond_3
    iget-boolean v3, p0, Lcom/android/calendar/timezone/x;->e:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    .line 103
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    aget-char v3, v3, v9

    invoke-static {v3}, Lcom/android/calendar/timezone/x;->a(C)Z

    move-result v3

    if-nez v3, :cond_5

    .line 104
    iget-object v3, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    invoke-static {v1, v3, v5}, Lcom/android/calendar/timezone/x;->a(Ljava/lang/String;Ljava/lang/String;[I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 105
    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->e()I

    move-result v3

    if-eq v3, v10, :cond_4

    .line 106
    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    aget v7, v5, v9

    aget v8, v5, v10

    invoke-interface {v2, v3, v7, v8, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 108
    :cond_4
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    aget v7, v5, v9

    aget v5, v5, v10

    invoke-interface {v2, v3, v7, v5, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 113
    :cond_5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v5, "km"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_9

    .line 115
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 118
    iget-object v1, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 119
    iget-object v1, v4, Lcom/android/calendar/timezone/y;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v7

    iget-object v1, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    if-nez v1, :cond_7

    const-string v1, ""

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    :goto_2
    invoke-static {v7, v3, v1}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v1

    .line 122
    if-eqz v1, :cond_8

    .line 123
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 125
    array-length v3, v1

    if-eqz v3, :cond_6

    const/4 v3, -0x1

    if-eq v5, v3, :cond_6

    .line 126
    array-length v1, v1

    add-int/2addr v1, v5

    .line 127
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2, v3, v5, v1, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 131
    :cond_6
    iget-object v1, v4, Lcom/android/calendar/timezone/y;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    :goto_3
    iget-object v1, v4, Lcom/android/calendar/timezone/y;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    .line 139
    const v1, 0x7f120217

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/android/calendar/timezone/y;->b:Landroid/widget/TextView;

    .line 140
    iget-object v1, v4, Lcom/android/calendar/timezone/y;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, v4, Lcom/android/calendar/timezone/y;->b:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextDirection(I)V

    goto/16 :goto_0

    .line 119
    :cond_7
    iget-object v1, p0, Lcom/android/calendar/timezone/x;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    goto :goto_2

    .line 133
    :cond_8
    iget-object v1, v4, Lcom/android/calendar/timezone/y;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 136
    :cond_9
    iget-object v1, v4, Lcom/android/calendar/timezone/y;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_a
    move-object v1, v2

    goto/16 :goto_1

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x7f04006b
        :pswitch_0
    .end packed-switch
.end method
