.class Lcom/android/calendar/timezone/n;
.super Landroid/os/AsyncTask;
.source "CalendarTimezoneActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method private constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Lcom/android/calendar/timezone/a;)V
    .locals 0

    .prologue
    .line 544
    invoke-direct {p0, p1}, Lcom/android/calendar/timezone/n;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 548
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v0

    if-nez v0, :cond_0

    .line 549
    new-instance v0, Lcom/android/calendar/timezone/u;

    iget-object v1, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {v0, v1}, Lcom/android/calendar/timezone/u;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/u;)Lcom/android/calendar/timezone/u;

    .line 551
    :cond_0
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/timezone/u;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const v5, 0x7f0f03fb

    .line 556
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 557
    iget-object v0, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->g(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    .line 559
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 560
    :try_start_0
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 562
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/timezone/u;->e()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 563
    invoke-static {v0}, Lcom/android/calendar/timezone/r;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_0

    .line 565
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 576
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 569
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 570
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 573
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 574
    iget-object v0, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->setVisibility(I)V

    .line 575
    iget-object v0, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/util/List;)V

    .line 576
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 578
    iget-object v0, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Z)Z

    .line 601
    :goto_1
    return-void

    .line 580
    :cond_3
    invoke-static {}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->l()Lcom/android/calendar/timezone/u;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/timezone/p;

    iget-object v2, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {v1, v2}, Lcom/android/calendar/timezone/p;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    new-instance v2, Lcom/android/calendar/timezone/q;

    iget-object v3, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const-string v4, "network"

    invoke-direct {v2, v3, v4}, Lcom/android/calendar/timezone/q;-><init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/timezone/u;->a(Lcom/android/calendar/timezone/p;Lcom/android/calendar/timezone/q;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 583
    iget-object v0, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->g(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    .line 584
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 586
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 587
    const v1, 0x7f0f03fc

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 588
    const v1, 0x7f0f009e

    new-instance v2, Lcom/android/calendar/timezone/o;

    invoke-direct {v2, p0}, Lcom/android/calendar/timezone/o;-><init>(Lcom/android/calendar/timezone/n;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 594
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    .line 596
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->g(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V

    .line 597
    iget-object v0, p0, Lcom/android/calendar/timezone/n;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const/4 v1, 0x0

    invoke-static {v0, v5, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 544
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/calendar/timezone/n;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 544
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/calendar/timezone/n;->a(Ljava/lang/Boolean;)V

    return-void
.end method
