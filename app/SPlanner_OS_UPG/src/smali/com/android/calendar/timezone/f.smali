.class Lcom/android/calendar/timezone/f;
.super Ljava/lang/Object;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 288
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    invoke-virtual {v0, v3}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V

    .line 289
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    .line 290
    if-nez v0, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a()Ljava/lang/String;

    move-result-object v1

    .line 294
    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 298
    const-string v4, " / "

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 300
    array-length v1, v4

    const/4 v5, 0x2

    if-ge v1, v5, :cond_2

    .line 301
    const-string v1, " "

    .line 306
    :goto_1
    iget-object v5, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v4, v3

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " / "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 307
    iget-object v1, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->d(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/timezone/r;->b(Ljava/lang/String;)Lcom/android/calendar/timezone/t;

    .line 309
    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->c()Z

    move-result v4

    .line 311
    if-nez v4, :cond_5

    .line 313
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->e(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V

    .line 314
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    if-nez v4, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V

    .line 315
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, p3}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;I)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 326
    sget-object v1, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 328
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b()V

    goto/16 :goto_0

    .line 303
    :cond_2
    aget-object v1, v4, v2

    goto :goto_1

    :cond_3
    move v1, v3

    .line 314
    goto :goto_2

    .line 316
    :catch_0
    move-exception v0

    .line 317
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    if-nez v4, :cond_4

    move v1, v2

    :goto_4
    invoke-virtual {v0, v1}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V

    .line 318
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, p3}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;I)I

    goto :goto_3

    :cond_4
    move v1, v3

    .line 317
    goto :goto_4

    .line 321
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    invoke-virtual {v0, v4}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V

    .line 322
    iget-object v0, p0, Lcom/android/calendar/timezone/f;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, p3}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;I)I

    goto :goto_3
.end method
