.class Lcom/android/calendar/timezone/m;
.super Ljava/lang/Object;
.source "CalendarTimezoneActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)V
    .locals 0

    .prologue
    .line 641
    iput-object p1, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 643
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    .line 644
    if-nez v0, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    const-string v1, ""

    .line 648
    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a()Ljava/lang/String;

    move-result-object v1

    .line 649
    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 653
    const-string v4, " / "

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 655
    array-length v1, v4

    const/4 v5, 0x2

    if-ge v1, v5, :cond_3

    .line 656
    const-string v1, " "

    .line 661
    :goto_1
    iget-object v5, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v4, v3

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " / "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 662
    iget-object v1, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->d(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/timezone/r;->b(Ljava/lang/String;)Lcom/android/calendar/timezone/t;

    .line 663
    invoke-virtual {v0}, Lcom/android/calendar/timezone/TimezoneCityListItem;->c()Z

    move-result v4

    .line 665
    if-nez v4, :cond_5

    .line 667
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->e(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)I

    move-result v0

    if-eq p3, v0, :cond_2

    .line 668
    iget-object v0, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    if-nez v4, :cond_4

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V

    .line 669
    iget-object v0, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v1}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->e(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    invoke-virtual {v0, v4}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 681
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->b()V

    .line 682
    iget-object v0, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0, p3}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->a(Lcom/android/calendar/timezone/CalendarTimezoneActivity;I)I

    goto/16 :goto_0

    .line 658
    :cond_3
    aget-object v1, v4, v2

    goto :goto_1

    :cond_4
    move v1, v3

    .line 668
    goto :goto_2

    .line 675
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->e(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)I

    move-result v0

    if-eq p3, v0, :cond_2

    .line 676
    iget-object v0, p0, Lcom/android/calendar/timezone/m;->a:Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-static {v0}, Lcom/android/calendar/timezone/CalendarTimezoneActivity;->c(Lcom/android/calendar/timezone/CalendarTimezoneActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/timezone/TimezoneCityListItem;

    invoke-virtual {v0, v4}, Lcom/android/calendar/timezone/TimezoneCityListItem;->a(Z)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 678
    :catch_0
    move-exception v0

    goto :goto_3

    .line 671
    :catch_1
    move-exception v0

    goto :goto_3
.end method
