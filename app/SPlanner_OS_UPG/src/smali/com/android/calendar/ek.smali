.class public Lcom/android/calendar/ek;
.super Ljava/lang/Object;
.source "HoverUtils.java"


# static fields
.field public static a:Z

.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/android/calendar/ek;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/ek;->b:Ljava/lang/String;

    .line 34
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/ek;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 37
    if-ne p0, v0, :cond_0

    .line 38
    sput-boolean v0, Lcom/android/calendar/ek;->a:Z

    .line 43
    :goto_0
    const/4 v0, 0x2

    :try_start_0
    invoke-static {v0, p0}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_1
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/ek;->a:Z

    goto :goto_0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
