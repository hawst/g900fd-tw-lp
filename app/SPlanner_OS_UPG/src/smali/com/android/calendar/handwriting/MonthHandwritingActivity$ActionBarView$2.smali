.class Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$2;
.super Ljava/lang/Object;
.source "MonthHandwritingActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;


# direct methods
.method constructor <init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;)V
    .locals 0

    .prologue
    .line 600
    iput-object p1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$2;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 603
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 633
    :goto_0
    return v2

    .line 607
    :cond_0
    const/4 v0, 0x2

    new-array v3, v0, [I

    .line 608
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 610
    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 611
    invoke-virtual {p1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 613
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 614
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$2;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 615
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->v(Landroid/content/Context;)I

    move-result v6

    .line 616
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v7, 0x7f0c00a2

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 617
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v8, 0x7f0c0201

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 620
    aget v0, v3, v2

    div-int/lit8 v9, v5, 0x2

    if-ge v0, v9, :cond_2

    move v0, v1

    .line 624
    :goto_1
    sget-object v9, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-static {v9, v10, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    .line 625
    if-eqz v0, :cond_1

    .line 626
    const/16 v0, 0x33

    aget v2, v3, v2

    div-int/lit8 v3, v4, 0x2

    add-int/2addr v2, v3

    sub-int/2addr v2, v8

    div-int/lit8 v3, v6, 0x2

    add-int/2addr v3, v7

    invoke-virtual {v9, v0, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 632
    :goto_2
    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    move v2, v1

    .line 633
    goto :goto_0

    .line 629
    :cond_1
    const/16 v0, 0x35

    aget v2, v3, v2

    sub-int v2, v5, v2

    div-int/lit8 v3, v4, 0x2

    sub-int/2addr v2, v3

    sub-int/2addr v2, v8

    div-int/lit8 v3, v6, 0x2

    add-int/2addr v3, v7

    invoke-virtual {v9, v0, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_1
.end method
