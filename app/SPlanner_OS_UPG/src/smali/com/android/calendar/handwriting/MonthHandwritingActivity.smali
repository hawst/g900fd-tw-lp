.class public Lcom/android/calendar/handwriting/MonthHandwritingActivity;
.super Landroid/app/Activity;
.source "MonthHandwritingActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# static fields
.field private static final BUNDLE_KEY_RESTORE_BG:Ljava/lang/String; = "key_restore_bg"

.field private static final DEBUG:Z = false

.field private static final DEFAULT_FILE_EXT_PNG:Ljava/lang/String; = ".png"

.field private static final DEFAULT_SAVE_PATH:Ljava/lang/String; = ".SPlanner_SPenMemo"

.field public static final FILENAME:Ljava/lang/String; = "filename"

.field private static final TAG:Ljava/lang/String; = "Handwriting"

.field public static mContext:Landroid/content/Context;


# instance fields
.field private mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

.field private mCanvasContainer:Landroid/widget/FrameLayout;

.field private mCanvasLayout:Landroid/widget/RelativeLayout;

.field private mDefaultFolderPath:Ljava/lang/String;

.field private mDimLayout:Landroid/widget/FrameLayout;

.field private mFileName:Ljava/lang/String;

.field private mHandWriting:Landroid/widget/LinearLayout;

.field private mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

.field private mHasCocktailBar:Z

.field private mIsShareViaEnable:Z

.field private mIsTabletConfig:Z

.field private mMonthBG:Landroid/graphics/Bitmap;

.field private mOrientation:I

.field private mShareChooserIntent:Landroid/content/Intent;

.field private mShareViaBG:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 70
    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    .line 71
    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    .line 73
    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mDefaultFolderPath:Ljava/lang/String;

    .line 74
    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mFileName:Ljava/lang/String;

    .line 77
    iput-boolean v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsTabletConfig:Z

    .line 79
    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsShareViaEnable:Z

    .line 82
    iput-boolean v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHasCocktailBar:Z

    .line 484
    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsShareViaEnable:Z

    return v0
.end method

.method static synthetic access$202(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsShareViaEnable:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mDefaultFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->disableHandwriting()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->saveCanvas()V

    return-void
.end method

.method private checkHandwritingAlertPopup()V
    .locals 3

    .prologue
    .line 235
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    const-string v1, "preferences_confirm_handwriting_launch"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 239
    if-nez v0, :cond_0

    .line 240
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->displayHandwritingPopup()V

    goto :goto_0
.end method

.method private disableHandwriting()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->finish()V

    .line 302
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    .line 303
    invoke-virtual {p0, v1, v1}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->overridePendingTransition(II)V

    .line 304
    return-void
.end method

.method private initCanvasView()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 137
    iget-boolean v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsTabletConfig:Z

    if-nez v0, :cond_1

    .line 138
    iget v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mOrientation:I

    invoke-static {p0}, Lcom/android/calendar/hj;->b(Landroid/app/Activity;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 139
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->disableHandwriting()V

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 145
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->setCurrentMonthBG()V

    .line 154
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->setShareViaBG()V

    .line 155
    const v0, 0x7f04001c

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->setContentView(I)V

    .line 157
    const v0, 0x7f120083

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mCanvasContainer:Landroid/widget/FrameLayout;

    .line 158
    const v0, 0x7f120084

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mCanvasLayout:Landroid/widget/RelativeLayout;

    .line 160
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0411

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 161
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c01fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 163
    new-instance v0, Lcom/android/calendar/handwriting/HandwritingHelper;

    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mCanvasContainer:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/handwriting/HandwritingHelper;-><init>(Landroid/content/Context;Landroid/widget/FrameLayout;Landroid/widget/RelativeLayout;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    .line 164
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    if-eqz v0, :cond_0

    .line 166
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c040f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 167
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c040e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 168
    iget-boolean v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHasCocktailBar:Z

    if-eqz v0, :cond_3

    .line 169
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0410

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 170
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mCanvasContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 171
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 172
    iget-object v4, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mCanvasContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v0, v1

    .line 174
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    iget-object v4, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mFileName:Ljava/lang/String;

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/calendar/handwriting/HandwritingHelper;->loadNoteFile(Ljava/lang/String;II)V

    .line 175
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    invoke-virtual {v1, v0, v7}, Lcom/android/calendar/handwriting/HandwritingHelper;->resizeCanvasView(II)Z

    .line 176
    new-instance v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-direct {v0, p0, p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;-><init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    .line 177
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    invoke-virtual {v1}, Lcom/android/calendar/handwriting/HandwritingHelper;->getCanvasViewPenName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    invoke-virtual {v2}, Lcom/android/calendar/handwriting/HandwritingHelper;->getCanvasViewPenColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updatePenToolBar(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 147
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->disableHandwriting()V

    goto/16 :goto_0

    :cond_3
    move v0, v6

    goto :goto_1
.end method

.method private saveCanvas()V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mFileName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/handwriting/HandwritingHelper;->saveNoteFile(Ljava/lang/String;Z)Z

    .line 298
    :cond_0
    return-void
.end method

.method private setCurrentMonthBG()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 432
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0411

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 437
    iget-boolean v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHasCocktailBar:Z

    if-eqz v1, :cond_2

    .line 438
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0410

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 440
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c01fe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 442
    sget-object v2, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gt v0, v2, :cond_0

    sget-object v2, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 444
    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 445
    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 447
    :cond_3
    sget-object v2, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    invoke-static {v2, v3, v3, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private setShareViaBG()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 452
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 453
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 455
    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 456
    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 459
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    .line 460
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 461
    sget-object v1, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 462
    sget-object v1, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 464
    :cond_1
    return-void
.end method


# virtual methods
.method public displayHandwritingPopup()V
    .locals 6

    .prologue
    .line 246
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 247
    const v1, 0x7f040061

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 248
    const v1, 0x7f12007b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 249
    const v2, 0x7f120143

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 251
    const v3, 0x7f12007e

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 253
    new-instance v4, Lcom/android/calendar/handwriting/MonthHandwritingActivity$1;

    invoke-direct {v4, p0, v3}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$1;-><init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    new-instance v4, Landroid/app/AlertDialog$Builder;

    sget-object v2, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-direct {v4, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 267
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 268
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 270
    iget-boolean v5, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsTabletConfig:Z

    if-eqz v5, :cond_0

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v5, 0xd5

    if-eq v2, v5, :cond_0

    .line 271
    sget-object v2, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    const v5, 0x7f0f0226

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 276
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    const v1, 0x7f0f022a

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 279
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 280
    const v0, 0x7f0f02e3

    new-instance v1, Lcom/android/calendar/handwriting/MonthHandwritingActivity$2;

    invoke-direct {v1, p0, v3}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$2;-><init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 291
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 292
    return-void

    .line 273
    :cond_0
    sget-object v2, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    const v5, 0x7f0f0225

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 348
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->saveCanvas()V

    .line 349
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->disableHandwriting()V

    .line 350
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 351
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 186
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 188
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mOrientation:I

    .line 189
    iget-boolean v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsTabletConfig:Z

    if-nez v0, :cond_1

    .line 190
    invoke-static {p0}, Lcom/android/calendar/hj;->b(Landroid/app/Activity;)I

    move-result v0

    .line 192
    iget v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mOrientation:I

    if-eq v1, v0, :cond_0

    .line 193
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->saveCanvas()V

    .line 194
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->disableHandwriting()V

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    const v0, 0x7f120085

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandWriting:Landroid/widget/LinearLayout;

    .line 198
    const v0, 0x7f120086

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mDimLayout:Landroid/widget/FrameLayout;

    .line 200
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mCanvasContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 201
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mDimLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 202
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0411

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 203
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c01fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 205
    iget v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mOrientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 206
    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandWriting:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 207
    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 208
    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 214
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mCanvasContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 215
    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mDimLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->resizeCanvasView(II)Z

    goto :goto_0

    .line 210
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandWriting:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 211
    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 212
    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 91
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 92
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 95
    :cond_0
    sput-object p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    .line 96
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mOrientation:I

    .line 97
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsTabletConfig:Z

    .line 98
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHasCocktailBar:Z

    .line 100
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->setFileIO()V

    .line 101
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->initCanvasView()V

    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsTabletConfig:Z

    if-nez v0, :cond_1

    .line 104
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->checkHandwritingAlertPopup()V

    .line 107
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_2

    .line 109
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 110
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 113
    :cond_2
    invoke-static {p0}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 114
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 115
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 117
    :cond_3
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 319
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->closeCanvasView()V

    .line 323
    :cond_0
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 324
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 325
    sput-object v1, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    .line 328
    :cond_1
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 329
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 330
    sput-object v1, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    .line 333
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 334
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 335
    iput-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    .line 338
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 339
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 340
    iput-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    .line 343
    :cond_4
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 344
    return-void
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 418
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v0

    and-int/lit16 v0, v0, -0x7001

    .line 419
    invoke-static {v0}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    packed-switch p1, :pswitch_data_0

    .line 428
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 422
    :pswitch_0
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->saveCanvas()V

    goto :goto_0

    .line 420
    :pswitch_data_0
    .packed-switch 0x2f
        :pswitch_0
    .end packed-switch
.end method

.method public onModeChanged(Z)V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->disableHandwriting()V

    .line 224
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 308
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 309
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 355
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 356
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 357
    const-string v0, "key_restore_bg"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 360
    const-string v0, "key_restore_bg"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    .line 362
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;

    .line 314
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 315
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    const-string v0, "key_restore_bg"

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 371
    const-string v0, "key_restore_bg"

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareViaBG:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 373
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 374
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 232
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method setFileIO()V
    .locals 3

    .prologue
    .line 121
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 122
    new-instance v1, Ljava/io/File;

    const-string v2, ".SPlanner_SPenMemo"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    const-string v0, "cal"

    const-string v1, "Default Save Path Creation Error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mDefaultFolderPath:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mDefaultFolderPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "filename"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mFileName:Ljava/lang/String;

    goto :goto_0
.end method

.method public updateHistoryToolbar()V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updateHistoryState()V

    .line 482
    :cond_0
    return-void
.end method

.method public updateScreen()V
    .locals 2

    .prologue
    .line 467
    invoke-direct {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->setCurrentMonthBG()V

    .line 468
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mMonthBG:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/android/calendar/handwriting/HandwritingHelper;->setCanvasViewBackground(Landroid/graphics/Bitmap;)V

    .line 469
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->updateCanvasView()V

    .line 470
    return-void
.end method

.method public updateToolBar(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mActionBarView:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updatePenToolBar(Ljava/lang/String;I)V

    .line 476
    :cond_0
    return-void
.end method
