.class public Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;
.super Landroid/widget/LinearLayout;
.source "MonthHandwritingActivity.java"


# instance fields
.field longClickListener:Landroid/view/View$OnLongClickListener;

.field mBtnClickListener:Landroid/view/View$OnClickListener;

.field mCancelBtn:Landroid/widget/ImageButton;

.field mEraserBtn:Landroid/widget/ImageView;

.field mPenBtn:Lcom/android/calendar/PenModeBtn;

.field mRedoBtn:Landroid/widget/ImageView;

.field mSaveBtn:Landroid/widget/ImageButton;

.field mShareViaBtn:Landroid/widget/ImageButton;

.field mUndoBtn:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/16 v3, 0x1f4

    const/4 v2, 0x1

    .line 493
    iput-object p1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    .line 494
    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 552
    new-instance v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;-><init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;)V

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mBtnClickListener:Landroid/view/View$OnClickListener;

    .line 600
    new-instance v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$2;

    invoke-direct {v0, p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$2;-><init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;)V

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->longClickListener:Landroid/view/View$OnLongClickListener;

    .line 496
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 498
    const v1, 0x7f040077

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 500
    const v0, 0x7f120256

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/PenModeBtn;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    .line 501
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/calendar/PenModeBtn;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 502
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->longClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Lcom/android/calendar/PenModeBtn;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 504
    const v0, 0x7f120257

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mEraserBtn:Landroid/widget/ImageView;

    .line 505
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mEraserBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 506
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mEraserBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->longClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 508
    const v0, 0x7f120258

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    .line 509
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 510
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->longClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 512
    const v0, 0x7f120259

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    .line 513
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->longClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 516
    const v0, 0x7f12025a

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mShareViaBtn:Landroid/widget/ImageButton;

    .line 517
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mShareViaBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 518
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mShareViaBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->longClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 520
    const v0, 0x7f12025b

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mCancelBtn:Landroid/widget/ImageButton;

    .line 521
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mCancelBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 522
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mCancelBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->longClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 524
    const v0, 0x7f12025c

    invoke-virtual {p0, v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mSaveBtn:Landroid/widget/ImageButton;

    .line 525
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mSaveBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 526
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mSaveBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->longClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 528
    invoke-static {p2}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    invoke-virtual {v0, v2}, Lcom/android/calendar/PenModeBtn;->setHoverPopupType(I)V

    .line 530
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    invoke-virtual {v0}, Lcom/android/calendar/PenModeBtn;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 532
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mEraserBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 533
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mEraserBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 535
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 536
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 538
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 539
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 541
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mShareViaBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 542
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mShareViaBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 544
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mCancelBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 545
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mCancelBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 547
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mSaveBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 548
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mSaveBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 550
    :cond_0
    return-void
.end method


# virtual methods
.method public updateHistoryState()V
    .locals 4

    .prologue
    const/16 v3, 0xff

    const/16 v2, 0x4c

    .line 648
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->isCanvasViewUndoable()Z

    move-result v0

    .line 649
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 650
    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 656
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->isCanvasViewRedoable()Z

    move-result v0

    .line 657
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 658
    if-eqz v0, :cond_1

    .line 659
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 663
    :goto_1
    return-void

    .line 653
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    goto :goto_0

    .line 661
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    goto :goto_1
.end method

.method public updatePenModeState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 638
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->getCanvasViewPenAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 639
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    invoke-virtual {v0, v1}, Lcom/android/calendar/PenModeBtn;->setSelected(Z)V

    .line 640
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mEraserBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 645
    :cond_0
    :goto_0
    return-void

    .line 641
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->getCanvasViewPenAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 642
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    invoke-virtual {v0, v2}, Lcom/android/calendar/PenModeBtn;->setSelected(Z)V

    .line 643
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mEraserBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method public updatePenToolBar(Ljava/lang/String;I)V
    .locals 2

    .prologue
    const v0, 0x7f02016a

    .line 666
    if-eqz p1, :cond_1

    .line 669
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 682
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    invoke-virtual {v1, v0}, Lcom/android/calendar/PenModeBtn;->setImageResource(I)V

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    invoke-virtual {v0, p2}, Lcom/android/calendar/PenModeBtn;->setColor(I)V

    .line 686
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updatePenModeState()V

    .line 687
    invoke-virtual {p0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updateHistoryState()V

    .line 688
    return-void

    .line 671
    :cond_2
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 672
    const v0, 0x7f02016b

    goto :goto_0

    .line 673
    :cond_3
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 674
    const v0, 0x7f020169

    goto :goto_0

    .line 675
    :cond_4
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 676
    const v0, 0x7f020166

    goto :goto_0

    .line 677
    :cond_5
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 678
    const v0, 0x7f020165

    goto :goto_0

    .line 679
    :cond_6
    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 680
    const v0, 0x7f020168

    goto :goto_0
.end method
