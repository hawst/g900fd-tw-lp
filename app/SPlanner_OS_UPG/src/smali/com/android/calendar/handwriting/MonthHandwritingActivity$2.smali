.class Lcom/android/calendar/handwriting/MonthHandwritingActivity$2;
.super Ljava/lang/Object;
.source "MonthHandwritingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

.field final synthetic val$cbNeverShowAgain:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$2;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    iput-object p2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$2;->val$cbNeverShowAgain:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$2;->val$cbNeverShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    const-string v1, "preferences_confirm_handwriting_launch"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 288
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 289
    return-void

    .line 286
    :cond_0
    sget-object v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mContext:Landroid/content/Context;

    const-string v1, "preferences_confirm_handwriting_launch"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method
