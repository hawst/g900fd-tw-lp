.class Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;
.super Ljava/lang/Object;
.source "MonthHandwritingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;


# direct methods
.method constructor <init>(Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 555
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mPenBtn:Lcom/android/calendar/PenModeBtn;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/handwriting/HandwritingHelper;->setCanvasViewToolTypeAction(I)V

    .line 557
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updatePenModeState()V

    .line 597
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mEraserBtn:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 559
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/calendar/handwriting/HandwritingHelper;->setCanvasViewToolTypeAction(I)V

    .line 560
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updatePenModeState()V

    goto :goto_0

    .line 561
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mUndoBtn:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 562
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->isCanvasViewUndoable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/calendar/handwriting/HandwritingHelper;->setCanvasViewUndoRedoAction(I)V

    .line 564
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updateHistoryState()V

    goto :goto_0

    .line 566
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mRedoBtn:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 567
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/HandwritingHelper;->isCanvasViewRedoable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/calendar/handwriting/HandwritingHelper;->setCanvasViewUndoRedoAction(I)V

    .line 569
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    invoke-virtual {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->updateHistoryState()V

    goto :goto_0

    .line 571
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mShareViaBtn:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 572
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$100(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsShareViaEnable:Z
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$200(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v1, v1, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mDefaultFolderPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$300(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Sharevia_Handwriting"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 574
    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v1, v1, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mHandwritingHelper:Lcom/android/calendar/handwriting/HandwritingHelper;
    invoke-static {v1}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$000(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Lcom/android/calendar/handwriting/HandwritingHelper;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/handwriting/HandwritingHelper;->saveNoteFile(Ljava/lang/String;Z)Z

    .line 576
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 577
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 579
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 580
    const-string v2, "image/png"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 581
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 582
    const-string v0, "theme"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 584
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.CHOOSER"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;
    invoke-static {v0, v2}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$102(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Landroid/content/Intent;)Landroid/content/Intent;

    .line 585
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$100(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "android.intent.extra.INTENT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 586
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$100(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.TITLE"

    iget-object v2, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v2, v2, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    const v3, 0x7f0f03d8

    invoke-virtual {v2, v3}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 587
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    iget-object v1, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v1, v1, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # getter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mShareChooserIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$100(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 589
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mCancelBtn:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 590
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # setter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsShareViaEnable:Z
    invoke-static {v0, v1}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$202(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Z)Z

    .line 591
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # invokes: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->disableHandwriting()V
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$400(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)V

    goto/16 :goto_0

    .line 592
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->mSaveBtn:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # setter for: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->mIsShareViaEnable:Z
    invoke-static {v0, v1}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$202(Lcom/android/calendar/handwriting/MonthHandwritingActivity;Z)Z

    .line 594
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # invokes: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->saveCanvas()V
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$500(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)V

    .line 595
    iget-object v0, p0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView$1;->this$1:Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;

    iget-object v0, v0, Lcom/android/calendar/handwriting/MonthHandwritingActivity$ActionBarView;->this$0:Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    # invokes: Lcom/android/calendar/handwriting/MonthHandwritingActivity;->disableHandwriting()V
    invoke-static {v0}, Lcom/android/calendar/handwriting/MonthHandwritingActivity;->access$400(Lcom/android/calendar/handwriting/MonthHandwritingActivity;)V

    goto/16 :goto_0
.end method
