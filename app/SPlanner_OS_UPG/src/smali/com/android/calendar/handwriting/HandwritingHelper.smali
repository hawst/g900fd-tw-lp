.class public Lcom/android/calendar/handwriting/HandwritingHelper;
.super Ljava/lang/Object;
.source "HandwritingHelper.java"


# static fields
.field public static final ACTION_REDO:I = 0x3

.field public static final ACTION_REMOVER:I = 0x1

.field public static final ACTION_STROKE:I = 0x0

.field public static final ACTION_UNDO:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/FrameLayout;Landroid/widget/RelativeLayout;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/FrameLayout;Landroid/widget/RelativeLayout;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method


# virtual methods
.method public closeCanvasView()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public getCanvasViewPenAction()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, -0x1

    return v0
.end method

.method public getCanvasViewPenColor()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, -0x1

    return v0
.end method

.method public getCanvasViewPenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public isCanvasViewRedoable()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public isCanvasViewUndoable()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public loadNoteFile(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public resizeCanvasView(II)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public saveNoteFile(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public saveNoteFile(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method protected saveStrokeSettings()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public setCanvasViewBackground(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public setCanvasViewToolTypeAction(I)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public setCanvasViewUndoRedoAction(I)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public updateCanvasView()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public updatePenToolBar()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method
