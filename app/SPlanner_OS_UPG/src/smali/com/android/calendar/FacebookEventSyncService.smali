.class public Lcom/android/calendar/FacebookEventSyncService;
.super Landroid/app/Service;
.source "FacebookEventSyncService.java"


# static fields
.field private static final i:Ljava/lang/Boolean;


# instance fields
.field a:I

.field private volatile b:Landroid/os/Looper;

.field private volatile c:Lcom/android/calendar/du;

.field private d:Lcom/android/calendar/ds;

.field private e:Lcom/android/calendar/dr;

.field private f:Ljava/util/HashMap;

.field private g:I

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/FacebookEventSyncService;->i:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 68
    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncService;->d:Lcom/android/calendar/ds;

    .line 69
    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncService;->e:Lcom/android/calendar/dr;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncService;->f:Ljava/util/HashMap;

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/FacebookEventSyncService;->g:I

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/FacebookEventSyncService;->a:I

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/FacebookEventSyncService;->h:Z

    .line 437
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/FacebookEventSyncService;I)I
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Lcom/android/calendar/FacebookEventSyncService;->g:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/FacebookEventSyncService;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/android/calendar/FacebookEventSyncService;->h:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/FacebookEventSyncService;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/android/calendar/FacebookEventSyncService;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/FacebookEventSyncService;)Lcom/android/calendar/dr;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/FacebookEventSyncService;->e:Lcom/android/calendar/dr;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/FacebookEventSyncService;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/android/calendar/FacebookEventSyncService;->g:I

    return v0
.end method

.method static synthetic c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/android/calendar/FacebookEventSyncService;->i:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 146
    sget-object v0, Lcom/android/calendar/FacebookEventSyncService;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    const-string v0, "FBEventSyncService"

    const-string v1, "syncEventsPartial"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_0
    sget-object v3, Lcom/android/calendar/hj;->c:Landroid/net/Uri;

    .line 151
    iget-object v0, p0, Lcom/android/calendar/FacebookEventSyncService;->d:Lcom/android/calendar/ds;

    const/4 v1, 0x3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ds;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public b()J
    .locals 12

    .prologue
    const/4 v3, 0x0

    const-wide/16 v8, -0x1

    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 157
    sget-object v0, Lcom/android/calendar/FacebookEventSyncService;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "FBEventSyncService"

    const-string v1, "createFacebookCalendar"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/FacebookEventSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 166
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 167
    array-length v5, v2

    move v1, v3

    :goto_0
    if-ge v1, v5, :cond_9

    aget-object v6, v2, v1

    .line 168
    iget-object v7, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v7, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v10, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 170
    iget-object v2, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 171
    iget-object v1, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object v6, v1

    move-object v7, v2

    .line 176
    :goto_1
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 230
    :cond_1
    :goto_2
    return-wide v8

    .line 167
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    :cond_3
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v11, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "account_name="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v7}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "account_type"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v6}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "name=\'Facebook\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 191
    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_7

    .line 192
    :cond_4
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 195
    const-string v3, "account_name"

    invoke-virtual {v1, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v3, "account_type"

    invoke-virtual {v1, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v3, "name"

    const-string v5, "Facebook"

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v3, "calendar_displayName"

    const-string v5, "Facebook"

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v3, "calendar_access_level"

    const/16 v5, 0xc8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 202
    const-string v3, "visible"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 203
    const-string v3, "sync_events"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    const-string v3, "calendar_color"

    sget v5, Lcom/android/calendar/hj;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 205
    const-string v3, "ownerAccount"

    invoke-virtual {v1, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :try_start_0
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 214
    :goto_3
    if-eqz v4, :cond_8

    .line 215
    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 223
    :goto_4
    if-eqz v2, :cond_5

    .line 224
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 227
    :cond_5
    cmp-long v2, v0, v8

    if-nez v2, :cond_6

    :cond_6
    move-wide v8, v0

    .line 230
    goto/16 :goto_2

    .line 210
    :catch_0
    move-exception v0

    .line 211
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteFullException;->printStackTrace()V

    goto :goto_3

    .line 218
    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 219
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    goto :goto_4

    :cond_8
    move-wide v0, v8

    goto :goto_4

    :cond_9
    move-object v6, v4

    move-object v7, v4

    goto/16 :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 93
    sget-object v0, Lcom/android/calendar/FacebookEventSyncService;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const-string v0, "FBEventSyncService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FacebookEventSyncService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 98
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 99
    new-instance v1, Lcom/android/calendar/ds;

    invoke-virtual {p0}, Lcom/android/calendar/FacebookEventSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/ds;-><init>(Lcom/android/calendar/FacebookEventSyncService;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/android/calendar/FacebookEventSyncService;->d:Lcom/android/calendar/ds;

    .line 100
    new-instance v1, Lcom/android/calendar/dr;

    invoke-virtual {p0}, Lcom/android/calendar/FacebookEventSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/dr;-><init>(Lcom/android/calendar/FacebookEventSyncService;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/android/calendar/FacebookEventSyncService;->e:Lcom/android/calendar/dr;

    .line 102
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncService;->b:Landroid/os/Looper;

    .line 103
    new-instance v0, Lcom/android/calendar/du;

    iget-object v1, p0, Lcom/android/calendar/FacebookEventSyncService;->b:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/du;-><init>(Lcom/android/calendar/FacebookEventSyncService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncService;->c:Lcom/android/calendar/du;

    .line 104
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 128
    sget-object v0, Lcom/android/calendar/FacebookEventSyncService;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const-string v0, "FBEventSyncService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 132
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 108
    sget-object v0, Lcom/android/calendar/FacebookEventSyncService;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "FBEventSyncService"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    if-eqz p1, :cond_1

    .line 112
    iget-object v0, p0, Lcom/android/calendar/FacebookEventSyncService;->c:Lcom/android/calendar/du;

    invoke-virtual {v0}, Lcom/android/calendar/du;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 113
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 116
    iget-object v1, p0, Lcom/android/calendar/FacebookEventSyncService;->c:Lcom/android/calendar/du;

    invoke-virtual {v1, v0}, Lcom/android/calendar/du;->sendMessage(Landroid/os/Message;)Z

    .line 118
    :cond_1
    const/4 v0, 0x3

    return v0
.end method
