.class Lcom/android/calendar/bh;
.super Landroid/content/AsyncQueryHandler;
.source "CalendarUtils.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/bg;


# direct methods
.method public constructor <init>(Lcom/android/calendar/bg;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/android/calendar/bh;->a:Lcom/android/calendar/bg;

    .line 90
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 91
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 95
    invoke-static {}, Lcom/android/calendar/bg;->a()Ljava/util/HashSet;

    move-result-object v5

    monitor-enter v5

    .line 96
    if-eqz p3, :cond_0

    :try_start_0
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    .line 97
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/android/calendar/bg;->a(Z)Z

    .line 98
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/android/calendar/bg;->b(Z)Z

    .line 99
    monitor-exit v5

    .line 154
    :goto_0
    return-void

    .line 104
    :cond_1
    const-string v1, "key"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 105
    const-string v1, "value"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    move v1, v4

    .line 106
    :cond_2
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 107
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 108
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 109
    const-string v9, "timezoneType"

    invoke-static {v3, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 110
    const-string v3, "auto"

    invoke-static {v8, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v2

    .line 112
    :goto_2
    invoke-static {}, Lcom/android/calendar/bg;->b()Z

    move-result v8

    if-eq v3, v8, :cond_2

    .line 114
    invoke-static {v3}, Lcom/android/calendar/bg;->c(Z)Z

    :goto_3
    move v1, v2

    goto :goto_1

    :cond_3
    move v3, v4

    .line 110
    goto :goto_2

    .line 116
    :cond_4
    const-string v9, "timezoneInstancesPrevious"

    invoke-static {v3, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 122
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/bg;->c()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 123
    const-string v9, ","

    invoke-virtual {v3, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 124
    const-string v9, ","

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 125
    const/4 v9, 0x0

    aget-object v3, v3, v9

    .line 128
    :cond_5
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    invoke-static {v3, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 130
    invoke-static {v8}, Lcom/android/calendar/bg;->a(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    .line 153
    :catchall_0
    move-exception v1

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 134
    :cond_6
    :try_start_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 135
    if-eqz v1, :cond_7

    .line 136
    move-object v0, p2

    check-cast v0, Landroid/content/Context;

    move-object v1, v0

    iget-object v2, p0, Lcom/android/calendar/bh;->a:Lcom/android/calendar/bg;

    invoke-static {v2}, Lcom/android/calendar/bg;->a(Lcom/android/calendar/bg;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/bf;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 138
    const-string v2, "preferences_home_tz_enabled"

    invoke-static {}, Lcom/android/calendar/bg;->b()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/android/calendar/bf;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 139
    const-string v2, "preferences_home_tz"

    invoke-static {}, Lcom/android/calendar/bg;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/android/calendar/bf;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_7
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/android/calendar/bg;->a(Z)Z

    .line 143
    invoke-static {}, Lcom/android/calendar/bg;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 144
    if-eqz v1, :cond_8

    .line 145
    instance-of v2, p2, Landroid/app/Activity;

    if-eqz v2, :cond_9

    .line 146
    move-object v0, p2

    check-cast v0, Landroid/app/Activity;

    move-object v2, v0

    invoke-virtual {v2, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_4

    .line 148
    :cond_9
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_4

    .line 152
    :cond_a
    invoke-static {}, Lcom/android/calendar/bg;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 153
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
