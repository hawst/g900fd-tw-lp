.class final Lcom/android/calendar/fy;
.super Landroid/widget/Filter;
.source "RecipientAdapterChina.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/fv;


# direct methods
.method private constructor <init>(Lcom/android/calendar/fv;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/fv;Lcom/android/calendar/fw;)V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0, p1}, Lcom/android/calendar/fy;-><init>(Lcom/android/calendar/fv;)V

    return-void
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 327
    check-cast p1, Lcom/android/ex/a/b;

    .line 328
    invoke-virtual {p1}, Lcom/android/ex/a/b;->b()Ljava/lang/String;

    move-result-object v1

    .line 329
    invoke-virtual {p1}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 330
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Landroid/text/util/Rfc822Token;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, v3}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 16

    .prologue
    .line 223
    new-instance v13, Landroid/widget/Filter$FilterResults;

    invoke-direct {v13}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 224
    const/4 v2, 0x0

    .line 225
    const/4 v14, 0x0

    .line 227
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v13

    .line 297
    :goto_0
    return-object v1

    .line 231
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Ljava/lang/String;)Ljava/lang/String;

    .line 233
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    invoke-static {v3}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;)I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v1, v0, v3, v4}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v15

    .line 235
    if-nez v15, :cond_3

    .line 290
    :goto_1
    if-eqz v15, :cond_1

    .line 291
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 293
    :cond_1
    if-eqz v14, :cond_2

    .line 294
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v1, v13

    .line 297
    goto :goto_0

    .line 243
    :cond_3
    :try_start_1
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 245
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 247
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 249
    :goto_2
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 252
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    new-instance v2, Lcom/android/calendar/gf;

    invoke-direct {v2, v15}, Lcom/android/calendar/gf;-><init>(Landroid/database/Cursor;)V

    const/4 v3, 0x1

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Lcom/android/calendar/gf;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 290
    :catchall_0
    move-exception v1

    move-object v2, v14

    move-object v3, v15

    :goto_3
    if-eqz v3, :cond_4

    .line 291
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 293
    :cond_4
    if-eqz v2, :cond_5

    .line 294
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1

    .line 257
    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    const/4 v2, 0x0

    invoke-static {v1, v2, v4, v5, v6}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v3

    .line 262
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    invoke-static {v1}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;)I

    move-result v1

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v2

    sub-int/2addr v1, v2

    .line 264
    if-lez v1, :cond_8

    .line 270
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    invoke-static {v1}, Lcom/android/calendar/fv;->b(Lcom/android/calendar/fv;)Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/android/calendar/gc;->a:Landroid/net/Uri;

    sget-object v9, Lcom/android/calendar/gc;->b:[Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 274
    if-eqz v2, :cond_7

    .line 275
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    invoke-static {v1, v2}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Landroid/database/Cursor;)Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v7

    move-object v8, v2

    .line 284
    :goto_4
    :try_start_4
    new-instance v2, Lcom/android/calendar/fz;

    invoke-direct/range {v2 .. v7}, Lcom/android/calendar/fz;-><init>(Ljava/util/List;Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;Ljava/util/List;)V

    iput-object v2, v13, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 287
    const/4 v1, 0x1

    iput v1, v13, Landroid/widget/Filter$FilterResults;->count:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-object v14, v8

    goto :goto_1

    .line 277
    :cond_7
    const/4 v7, 0x0

    move-object v8, v2

    goto :goto_4

    .line 281
    :cond_8
    const/4 v7, 0x0

    move-object v8, v14

    goto :goto_4

    .line 290
    :catchall_1
    move-exception v1

    move-object v3, v2

    move-object v2, v14

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v3, v15

    goto :goto_3

    :catchall_3
    move-exception v1

    move-object v2, v8

    move-object v3, v15

    goto :goto_3
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    .prologue
    .line 305
    iget-object v0, p0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    invoke-static {v0, p1}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 307
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/fz;

    .line 309
    iget-object v1, p0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    iget-object v2, v0, Lcom/android/calendar/fz;->b:Ljava/util/LinkedHashMap;

    invoke-static {v1, v2}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Ljava/util/LinkedHashMap;)Ljava/util/LinkedHashMap;

    .line 310
    iget-object v1, p0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    iget-object v2, v0, Lcom/android/calendar/fz;->c:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Ljava/util/List;)Ljava/util/List;

    .line 311
    iget-object v1, p0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    iget-object v2, v0, Lcom/android/calendar/fz;->d:Ljava/util/Set;

    invoke-static {v1, v2}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Ljava/util/Set;)Ljava/util/Set;

    .line 313
    iget-object v1, p0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    iget-object v2, v0, Lcom/android/calendar/fz;->a:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/android/calendar/fv;->b(Lcom/android/calendar/fv;Ljava/util/List;)V

    .line 316
    iget-object v1, v0, Lcom/android/calendar/fz;->e:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, p0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    invoke-static {v1}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;)I

    move-result v1

    iget-object v2, v0, Lcom/android/calendar/fz;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    sub-int/2addr v1, v2

    .line 319
    iget-object v2, p0, Lcom/android/calendar/fy;->a:Lcom/android/calendar/fv;

    iget-object v0, v0, Lcom/android/calendar/fz;->e:Ljava/util/List;

    invoke-static {v2, p1, v0, v1}, Lcom/android/calendar/fv;->a(Lcom/android/calendar/fv;Ljava/lang/CharSequence;Ljava/util/List;I)V

    .line 323
    :cond_0
    return-void
.end method
