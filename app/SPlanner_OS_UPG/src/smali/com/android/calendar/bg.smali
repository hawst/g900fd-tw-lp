.class public Lcom/android/calendar/bg;
.super Ljava/lang/Object;
.source "CalendarUtils.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static d:Ljava/lang/StringBuilder;

.field private static e:Ljava/util/Formatter;

.field private static volatile f:Z

.field private static volatile g:Z

.field private static volatile h:Z

.field private static volatile i:Ljava/lang/String;

.field private static j:Ljava/util/HashSet;

.field private static k:I

.field private static l:Lcom/android/calendar/bh;


# instance fields
.field private final m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "timezoneType"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/calendar/bg;->b:[Ljava/lang/String;

    .line 51
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "timezoneInstances"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/calendar/bg;->c:[Ljava/lang/String;

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "key"

    aput-object v1, v0, v3

    const-string v1, "value"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/bg;->a:[Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    sput-object v0, Lcom/android/calendar/bg;->d:Ljava/lang/StringBuilder;

    .line 58
    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/android/calendar/bg;->d:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/calendar/bg;->e:Ljava/util/Formatter;

    .line 59
    sput-boolean v4, Lcom/android/calendar/bg;->f:Z

    .line 60
    sput-boolean v3, Lcom/android/calendar/bg;->g:Z

    .line 62
    sput-boolean v3, Lcom/android/calendar/bg;->h:Z

    .line 63
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/calendar/bg;->j:Ljava/util/HashSet;

    .line 66
    sput v4, Lcom/android/calendar/bg;->k:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-object p1, p0, Lcom/android/calendar/bg;->m:Ljava/lang/String;

    .line 166
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/bg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/calendar/bg;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 49
    sput-object p0, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a()Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/android/calendar/bg;->j:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 49
    sput-boolean p0, Lcom/android/calendar/bg;->g:Z

    return p0
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 49
    sget-boolean v0, Lcom/android/calendar/bg;->h:Z

    return v0
.end method

.method static synthetic b(Z)Z
    .locals 0

    .prologue
    .line 49
    sput-boolean p0, Lcom/android/calendar/bg;->f:Z

    return p0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Z)Z
    .locals 0

    .prologue
    .line 49
    sput-boolean p0, Lcom/android/calendar/bg;->h:Z

    return p0
.end method


# virtual methods
.method public a(Landroid/content/Context;JJI)Ljava/lang/String;
    .locals 12

    .prologue
    .line 189
    move/from16 v0, p6

    and-int/lit16 v2, v0, 0x2000

    if-eqz v2, :cond_0

    .line 190
    const-string v9, "UTC"

    .line 194
    :goto_0
    sget-object v10, Lcom/android/calendar/bg;->d:Ljava/lang/StringBuilder;

    monitor-enter v10

    .line 195
    :try_start_0
    sget-object v2, Lcom/android/calendar/bg;->d:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 196
    sget-object v3, Lcom/android/calendar/bg;->e:Ljava/util/Formatter;

    move-object v2, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    invoke-static/range {v2 .. v9}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v2

    .line 198
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    return-object v2

    .line 192
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Lcom/android/calendar/bg;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 292
    sget-object v8, Lcom/android/calendar/bg;->j:Ljava/util/HashSet;

    monitor-enter v8

    .line 293
    :try_start_0
    sget-boolean v0, Lcom/android/calendar/bg;->f:Z

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/android/calendar/bg;->m:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/android/calendar/bf;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 295
    const-string v1, "preferences_home_tz_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/calendar/bg;->h:Z

    .line 296
    const-string v1, "preferences_home_tz"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    .line 299
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 300
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/bg;->g:Z

    .line 301
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/bg;->f:Z

    .line 307
    sget-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    if-nez v0, :cond_0

    .line 308
    new-instance v0, Lcom/android/calendar/bh;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/bh;-><init>(Lcom/android/calendar/bg;Landroid/content/ContentResolver;)V

    sput-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    .line 310
    :cond_0
    sget-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    const/4 v1, 0x0

    sget-object v3, Landroid/provider/CalendarContract$CalendarCache;->URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/bg;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/bh;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_1
    sget-boolean v0, Lcom/android/calendar/bg;->g:Z

    if-eqz v0, :cond_2

    .line 315
    sget-object v0, Lcom/android/calendar/bg;->j:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 317
    :cond_2
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    sget-object v0, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    .line 320
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 321
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 322
    aget-object v0, v0, v9

    .line 325
    :cond_3
    sget-boolean v1, Lcom/android/calendar/bg;->h:Z

    if-eqz v1, :cond_4

    :goto_0
    return-object v0

    .line 317
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 325
    :cond_4
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 215
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 275
    :goto_0
    return-void

    .line 222
    :cond_0
    sget-object v7, Lcom/android/calendar/bg;->j:Ljava/util/HashSet;

    monitor-enter v7

    .line 223
    :try_start_0
    const-string v2, "auto"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 224
    sget-boolean v2, Lcom/android/calendar/bg;->h:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 227
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/calendar/bg;->h:Z

    .line 236
    :goto_1
    if-eqz v0, :cond_5

    .line 238
    iget-object v0, p0, Lcom/android/calendar/bg;->m:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/android/calendar/bf;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 239
    const-string v1, "preferences_home_tz_enabled"

    sget-boolean v2, Lcom/android/calendar/bg;->h:Z

    invoke-static {v0, v1, v2}, Lcom/android/calendar/bf;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 240
    const-string v1, "preferences_home_tz"

    sget-object v2, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/calendar/bf;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 244
    sget-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    if-eqz v0, :cond_2

    .line 245
    sget-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    sget v1, Lcom/android/calendar/bg;->k:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/bh;->cancelOperation(I)V

    .line 248
    :cond_2
    new-instance v0, Lcom/android/calendar/bh;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/bh;-><init>(Lcom/android/calendar/bg;Landroid/content/ContentResolver;)V

    sput-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    .line 251
    sget v0, Lcom/android/calendar/bg;->k:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/calendar/bg;->k:I

    if-nez v0, :cond_3

    .line 252
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/bg;->k:I

    .line 256
    :cond_3
    const-string v1, "value"

    sget-boolean v0, Lcom/android/calendar/bg;->h:Z

    if-eqz v0, :cond_9

    const-string v0, "home"

    :goto_2
    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    sget-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    sget v1, Lcom/android/calendar/bg;->k:I

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$CalendarCache;->URI:Landroid/net/Uri;

    const-string v5, "key=?"

    sget-object v6, Lcom/android/calendar/bg;->b:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/bh;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 262
    sget-boolean v0, Lcom/android/calendar/bg;->h:Z

    if-eqz v0, :cond_5

    .line 263
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 264
    sget-object v0, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    .line 265
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 266
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 267
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 269
    :cond_4
    const-string v1, "value"

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    sget-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    sget v1, Lcom/android/calendar/bg;->k:I

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$CalendarCache;->URI:Landroid/net/Uri;

    const-string v5, "key=?"

    sget-object v6, Lcom/android/calendar/bg;->c:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/bh;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 274
    :cond_5
    monitor-exit v7

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 229
    :cond_6
    :try_start_1
    sget-boolean v2, Lcom/android/calendar/bg;->h:Z

    if-eqz v2, :cond_7

    sget-object v2, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_7
    move v0, v1

    .line 232
    :cond_8
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/calendar/bg;->h:Z

    .line 233
    sput-object p2, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    goto/16 :goto_1

    .line 256
    :cond_9
    const-string v0, "auto"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public b(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;
    .locals 9

    .prologue
    .line 329
    sget-object v8, Lcom/android/calendar/bg;->j:Ljava/util/HashSet;

    monitor-enter v8

    .line 330
    :try_start_0
    sget-boolean v0, Lcom/android/calendar/bg;->f:Z

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/android/calendar/bg;->m:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/android/calendar/bf;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 332
    const-string v1, "preferences_home_tz_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/calendar/bg;->h:Z

    .line 333
    const-string v1, "preferences_home_tz"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    .line 336
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 337
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/bg;->g:Z

    .line 338
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/bg;->f:Z

    .line 344
    sget-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    if-nez v0, :cond_0

    .line 345
    new-instance v0, Lcom/android/calendar/bh;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/bh;-><init>(Lcom/android/calendar/bg;Landroid/content/ContentResolver;)V

    sput-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    .line 347
    :cond_0
    sget-object v0, Lcom/android/calendar/bg;->l:Lcom/android/calendar/bh;

    const/4 v1, 0x0

    sget-object v3, Landroid/provider/CalendarContract$CalendarCache;->URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/bg;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/bh;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_1
    sget-boolean v0, Lcom/android/calendar/bg;->g:Z

    if-eqz v0, :cond_2

    .line 352
    sget-object v0, Lcom/android/calendar/bg;->j:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 354
    :cond_2
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    sget-boolean v0, Lcom/android/calendar/bg;->h:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/calendar/bg;->i:Ljava/lang/String;

    :goto_0
    return-object v0

    .line 354
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 355
    :cond_3
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
