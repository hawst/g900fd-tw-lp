.class Lcom/android/calendar/dc;
.super Ljava/lang/Object;
.source "DrawerAdapter.java"

# interfaces
.implements Lcom/android/calendar/dd;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/LayoutInflater;

.field private c:Lcom/android/calendar/h/c;

.field private d:Landroid/widget/FrameLayout;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/TextView;

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    iput-object p1, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    .line 160
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/dc;->b:Landroid/view/LayoutInflater;

    .line 161
    invoke-static {}, Lcom/android/calendar/dz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/dc;->c:Lcom/android/calendar/h/c;

    .line 163
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 164
    const v1, 0x7f0c00e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/dc;->j:I

    .line 165
    const v1, 0x7f0c00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/dc;->k:I

    .line 167
    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/android/calendar/dc;->d:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/android/calendar/dc;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 296
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 283
    if-eqz p1, :cond_0

    .line 284
    const v1, 0x7f0c00e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/calendar/dc;->a(I)V

    .line 285
    const v1, 0x7f0c00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/dc;->b(I)V

    .line 290
    :goto_0
    return-void

    .line 287
    :cond_0
    const v1, 0x7f0c00e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/calendar/dc;->a(I)V

    .line 288
    const v1, 0x7f0c00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/dc;->b(I)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/android/calendar/dc;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/android/calendar/dc;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 301
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 302
    iget-object v1, p0, Lcom/android/calendar/dc;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 304
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 275
    iget-object v0, p0, Lcom/android/calendar/dc;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 276
    iget-object v0, p0, Lcom/android/calendar/dc;->i:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    iget-object v0, p0, Lcom/android/calendar/dc;->i:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 278
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/dc;->a(Z)V

    .line 279
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/android/calendar/de;->a:Lcom/android/calendar/de;

    invoke-virtual {v0}, Lcom/android/calendar/de;->ordinal()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 172
    if-nez p1, :cond_0

    .line 173
    iget-object v0, p0, Lcom/android/calendar/dc;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f040035

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 178
    :cond_0
    const v0, 0x7f1200bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/calendar/dc;->d:Landroid/widget/FrameLayout;

    .line 179
    const v0, 0x7f1200bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/dc;->e:Landroid/widget/TextView;

    .line 180
    const v0, 0x7f1200be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/dc;->f:Landroid/widget/TextView;

    .line 181
    const v0, 0x7f1200bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/dc;->g:Landroid/widget/TextView;

    .line 182
    const v0, 0x7f1200c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/dc;->i:Landroid/widget/TextView;

    .line 183
    const v0, 0x7f1200bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/dc;->h:Landroid/widget/LinearLayout;

    .line 185
    const v0, 0x7f0b0051

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 187
    invoke-virtual {p0}, Lcom/android/calendar/dc;->d()V

    .line 188
    invoke-virtual {p0}, Lcom/android/calendar/dc;->e()V

    .line 189
    return-object p1
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 204
    const-wide/16 v0, 0x3e8

    return-wide v0
.end method

.method public d()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 208
    iget-object v0, p0, Lcom/android/calendar/dc;->e:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 227
    :goto_0
    return-void

    .line 211
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 212
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 214
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 216
    iget-object v0, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    const/16 v1, 0xb

    invoke-static {v2, v3, v0, v1, v5}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    .line 218
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    iget-object v1, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0f0377

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    const/4 v4, 0x7

    invoke-static {v2, v3, v1, v4, v5}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v1

    .line 224
    iget-object v4, p0, Lcom/android/calendar/dc;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/android/calendar/dc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v0, p0, Lcom/android/calendar/dc;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    invoke-static {v1, v2, v3, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public e()V
    .locals 9

    .prologue
    const/16 v8, 0x2f

    const/16 v7, 0x21

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 230
    iget-object v0, p0, Lcom/android/calendar/dc;->i:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 272
    :goto_0
    return-void

    .line 233
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 234
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 236
    iget-object v1, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 237
    iget-object v1, p0, Lcom/android/calendar/dc;->c:Lcom/android/calendar/h/c;

    iget-object v2, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/h/c;->b(Landroid/content/res/Resources;Landroid/text/format/Time;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 238
    if-nez v0, :cond_1

    .line 239
    invoke-direct {p0}, Lcom/android/calendar/dc;->g()V

    goto :goto_0

    .line 242
    :cond_1
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/android/calendar/dc;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 243
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 244
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 245
    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 246
    iget v2, p0, Lcom/android/calendar/dc;->k:I

    iget v3, p0, Lcom/android/calendar/dc;->k:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    invoke-virtual {v1, v4, v4, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 247
    iget-object v0, p0, Lcom/android/calendar/dc;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v1, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 249
    iget-object v0, p0, Lcom/android/calendar/dc;->c:Lcom/android/calendar/h/c;

    invoke-virtual {v0}, Lcom/android/calendar/h/c;->d()Ljava/lang/String;

    move-result-object v0

    .line 250
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 251
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-le v2, v6, :cond_3

    .line 252
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 253
    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    iget v3, p0, Lcom/android/calendar/dc;->j:I

    invoke-direct {v2, v3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {v1, v2, v3, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 258
    :cond_2
    new-instance v0, Landroid/text/style/AbsoluteSizeSpan;

    iget v2, p0, Lcom/android/calendar/dc;->j:I

    invoke-direct {v0, v2}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/dc;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v0, p0, Lcom/android/calendar/dc;->i:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/dc;->c:Lcom/android/calendar/h/c;

    invoke-virtual {v2}, Lcom/android/calendar/h/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 264
    iget-object v0, p0, Lcom/android/calendar/dc;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/android/calendar/dc;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/dc;->c:Lcom/android/calendar/h/c;

    invoke-virtual {v1}, Lcom/android/calendar/h/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    invoke-direct {p0, v6}, Lcom/android/calendar/dc;->a(Z)V

    goto/16 :goto_0

    .line 269
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/dc;->g()V

    .line 270
    iget-object v0, p0, Lcom/android/calendar/dc;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 307
    return-void
.end method
