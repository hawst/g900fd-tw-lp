.class public Lcom/android/calendar/fd;
.super Ljava/lang/Object;
.source "NfcHandler.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# instance fields
.field private a:Z

.field private b:Lcom/android/calendar/detail/EventInfoFragment;

.field private c:Lcom/android/calendar/detail/cu;


# direct methods
.method public constructor <init>(Landroid/app/Fragment;Z)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/fd;->a:Z

    .line 54
    iput-boolean p2, p0, Lcom/android/calendar/fd;->a:Z

    .line 55
    iget-boolean v0, p0, Lcom/android/calendar/fd;->a:Z

    if-eqz v0, :cond_0

    .line 56
    check-cast p1, Lcom/android/calendar/detail/cu;

    iput-object p1, p0, Lcom/android/calendar/fd;->c:Lcom/android/calendar/detail/cu;

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    check-cast p1, Lcom/android/calendar/detail/EventInfoFragment;

    iput-object p1, p0, Lcom/android/calendar/fd;->b:Lcom/android/calendar/detail/EventInfoFragment;

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Landroid/app/Fragment;Z)V
    .locals 3

    .prologue
    .line 46
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 47
    if-nez v0, :cond_0

    .line 51
    :goto_0
    return-void

    .line 50
    :cond_0
    new-instance v1, Lcom/android/calendar/fd;

    invoke-direct {v1, p1, p2}, Lcom/android/calendar/fd;-><init>(Landroid/app/Fragment;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 10

    .prologue
    const/16 v4, 0x400

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 65
    .line 68
    iget-boolean v1, p0, Lcom/android/calendar/fd;->a:Z

    if-eqz v1, :cond_4

    .line 69
    iget-object v1, p0, Lcom/android/calendar/fd;->c:Lcom/android/calendar/detail/cu;

    invoke-virtual {v1}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 70
    iget-object v2, p0, Lcom/android/calendar/fd;->c:Lcom/android/calendar/detail/cu;

    invoke-virtual {v2}, Lcom/android/calendar/detail/cu;->b()Landroid/net/Uri;

    move-result-object v2

    .line 71
    iget-object v3, p0, Lcom/android/calendar/fd;->c:Lcom/android/calendar/detail/cu;

    invoke-virtual {v3}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v2, v7}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object v2

    .line 73
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 74
    new-array v4, v4, [B

    .line 78
    :try_start_0
    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 79
    if-eqz v1, :cond_1

    .line 80
    :goto_0
    :try_start_1
    invoke-virtual {v1, v4}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_1

    .line 81
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 84
    :catch_0
    move-exception v2

    .line 90
    :goto_1
    if-eqz v1, :cond_0

    .line 91
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 139
    :cond_0
    :goto_2
    return-object v0

    .line 90
    :cond_1
    if-eqz v1, :cond_2

    .line 91
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 98
    :cond_2
    :goto_3
    new-instance v0, Landroid/nfc/NdefRecord;

    const-string v1, "text/x-vtodo"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    new-array v2, v6, [B

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v0, v8, v1, v2, v3}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    .line 102
    sget-object v1, Lcom/android/calendar/hj;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/fd;->c:Lcom/android/calendar/detail/cu;

    invoke-virtual {v2}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 139
    :goto_4
    new-instance v1, Landroid/nfc/NdefMessage;

    new-array v2, v7, [Landroid/nfc/NdefRecord;

    aput-object v0, v2, v6

    invoke-direct {v1, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    move-object v0, v1

    goto :goto_2

    .line 93
    :catch_1
    move-exception v0

    .line 94
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 93
    :catch_2
    move-exception v1

    .line 94
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 86
    :catch_3
    move-exception v1

    move-object v1, v0

    .line 90
    :goto_5
    if-eqz v1, :cond_0

    .line 91
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_2

    .line 93
    :catch_4
    move-exception v1

    .line 94
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 89
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    .line 90
    :goto_6
    if-eqz v1, :cond_3

    .line 91
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 95
    :cond_3
    :goto_7
    throw v0

    .line 93
    :catch_5
    move-exception v1

    .line 94
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 104
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/fd;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v1}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 105
    iget-object v2, p0, Lcom/android/calendar/fd;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v2}, Lcom/android/calendar/detail/EventInfoFragment;->d()Landroid/net/Uri;

    move-result-object v2

    .line 106
    iget-object v3, p0, Lcom/android/calendar/fd;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v3}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v2, v6}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object v2

    .line 108
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 109
    new-array v4, v4, [B

    .line 113
    :try_start_6
    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v2

    .line 114
    if-eqz v2, :cond_5

    .line 115
    :goto_8
    :try_start_7
    invoke-virtual {v2, v4}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-lez v1, :cond_5

    .line 116
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_8

    .line 119
    :catch_6
    move-exception v1

    .line 120
    :goto_9
    :try_start_8
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 124
    if-eqz v2, :cond_0

    .line 125
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    goto/16 :goto_2

    .line 127
    :catch_7
    move-exception v1

    .line 128
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 124
    :cond_5
    if-eqz v2, :cond_6

    .line 125
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    .line 132
    :cond_6
    :goto_a
    new-instance v0, Landroid/nfc/NdefRecord;

    const-string v1, "text/x-vCalendar"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    new-array v2, v6, [B

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v0, v8, v1, v2, v3}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    .line 136
    sget-object v1, Lcom/android/calendar/hj;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/fd;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v2}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_4

    .line 127
    :catch_8
    move-exception v0

    .line 128
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_a

    .line 123
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    .line 124
    :goto_b
    if-eqz v2, :cond_7

    .line 125
    :try_start_b
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_9

    .line 129
    :cond_7
    :goto_c
    throw v0

    .line 127
    :catch_9
    move-exception v1

    .line 128
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_c

    .line 123
    :catchall_2
    move-exception v0

    goto :goto_b

    .line 119
    :catch_a
    move-exception v1

    move-object v2, v0

    goto :goto_9

    .line 89
    :catchall_3
    move-exception v0

    goto/16 :goto_6

    .line 86
    :catch_b
    move-exception v2

    goto/16 :goto_5

    .line 84
    :catch_c
    move-exception v1

    move-object v1, v0

    goto/16 :goto_1
.end method
