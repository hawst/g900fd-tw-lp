.class public Lcom/android/calendar/as;
.super Ljava/lang/Object;
.source "CalendarEventModel.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public A:J

.field public B:J

.field public C:I

.field public D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field public F:Ljava/lang/String;

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:I

.field public final K:I

.field public final L:I

.field public final M:I

.field public N:I

.field public O:Ljava/lang/String;

.field public P:I

.field public Q:I

.field public R:I

.field public S:I

.field public T:I

.field public U:Z

.field public V:[Z

.field public W:Z

.field public X:I

.field public Y:I

.field public Z:Ljava/lang/String;

.field public a:Ljava/lang/String;

.field public aA:Lcom/android/calendar/colorpicker/e;

.field public aB:Lcom/android/calendar/colorpicker/e;

.field public aa:J

.field public ab:Ljava/lang/Long;

.field public ac:Ljava/lang/Boolean;

.field public ad:Z

.field public ae:Z

.field public af:Z

.field public ag:Z

.field public ah:I

.field public ai:I

.field public aj:I

.field public transient ak:Landroid/graphics/Bitmap;

.field public al:Z

.field public am:Ljava/lang/String;

.field public an:Ljava/lang/String;

.field public ao:Ljava/lang/String;

.field public ap:Z

.field public aq:Z

.field public ar:J

.field public as:Ljava/lang/String;

.field public at:Z

.field public au:I

.field public av:Ljava/util/ArrayList;

.field public aw:Ljava/util/ArrayList;

.field public ax:Ljava/util/LinkedHashMap;

.field public ay:Z

.field public az:Ljava/lang/String;

.field public b:J

.field public c:J

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Z

.field public w:Z

.field public x:J

.field public y:J

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput-object v2, p0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 219
    iput-wide v4, p0, Lcom/android/calendar/as;->b:J

    .line 220
    iput-wide v4, p0, Lcom/android/calendar/as;->c:J

    .line 221
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/as;->d:Ljava/lang/String;

    .line 222
    iput v3, p0, Lcom/android/calendar/as;->e:I

    .line 226
    iput v3, p0, Lcom/android/calendar/as;->h:I

    .line 227
    iput v6, p0, Lcom/android/calendar/as;->i:I

    .line 229
    iput-object v2, p0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    .line 230
    iput-object v2, p0, Lcom/android/calendar/as;->k:Ljava/lang/String;

    .line 231
    iput-object v2, p0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    .line 232
    iput-object v2, p0, Lcom/android/calendar/as;->m:Ljava/lang/String;

    .line 235
    iput-object v2, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 236
    iput-object v2, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 237
    iput-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 238
    iput-object v2, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    .line 239
    iput-object v2, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 240
    iput-object v2, p0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    .line 241
    iput-object v2, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    .line 242
    iput-object v2, p0, Lcom/android/calendar/as;->u:Ljava/lang/String;

    .line 246
    iput-boolean v1, p0, Lcom/android/calendar/as;->v:Z

    .line 247
    iput-boolean v1, p0, Lcom/android/calendar/as;->w:Z

    .line 251
    iput-wide v4, p0, Lcom/android/calendar/as;->x:J

    .line 252
    iput-wide v4, p0, Lcom/android/calendar/as;->y:J

    .line 253
    iput v3, p0, Lcom/android/calendar/as;->z:I

    .line 257
    iput-wide v4, p0, Lcom/android/calendar/as;->A:J

    .line 258
    iput-wide v4, p0, Lcom/android/calendar/as;->B:J

    .line 259
    iput v3, p0, Lcom/android/calendar/as;->C:I

    .line 260
    iput-object v2, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    .line 261
    iput-object v2, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 262
    iput-object v2, p0, Lcom/android/calendar/as;->F:Ljava/lang/String;

    .line 263
    iput-boolean v3, p0, Lcom/android/calendar/as;->G:Z

    .line 264
    iput-boolean v3, p0, Lcom/android/calendar/as;->H:Z

    .line 265
    iput-boolean v3, p0, Lcom/android/calendar/as;->I:Z

    .line 266
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/as;->J:I

    .line 269
    iput v1, p0, Lcom/android/calendar/as;->K:I

    .line 270
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/as;->L:I

    .line 271
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/as;->M:I

    .line 272
    iput v1, p0, Lcom/android/calendar/as;->N:I

    .line 274
    iput v6, p0, Lcom/android/calendar/as;->P:I

    .line 275
    iput v3, p0, Lcom/android/calendar/as;->Q:I

    .line 276
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/calendar/as;->R:I

    .line 277
    iput v3, p0, Lcom/android/calendar/as;->S:I

    .line 278
    iput v3, p0, Lcom/android/calendar/as;->T:I

    .line 279
    iput-boolean v3, p0, Lcom/android/calendar/as;->U:Z

    .line 280
    const/4 v0, 0x7

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/android/calendar/as;->V:[Z

    .line 284
    iput-boolean v1, p0, Lcom/android/calendar/as;->W:Z

    .line 285
    iput v6, p0, Lcom/android/calendar/as;->X:I

    .line 286
    iput v6, p0, Lcom/android/calendar/as;->Y:I

    .line 287
    iput-object v2, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    .line 288
    iput-wide v4, p0, Lcom/android/calendar/as;->aa:J

    .line 289
    iput-object v2, p0, Lcom/android/calendar/as;->ab:Ljava/lang/Long;

    .line 290
    iput-object v2, p0, Lcom/android/calendar/as;->ac:Ljava/lang/Boolean;

    .line 291
    iput-boolean v3, p0, Lcom/android/calendar/as;->ad:Z

    .line 292
    iput-boolean v3, p0, Lcom/android/calendar/as;->ae:Z

    .line 293
    iput-boolean v3, p0, Lcom/android/calendar/as;->af:Z

    .line 295
    iput-boolean v3, p0, Lcom/android/calendar/as;->ag:Z

    .line 296
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/as;->ah:I

    .line 299
    iput v3, p0, Lcom/android/calendar/as;->ai:I

    .line 300
    iput v3, p0, Lcom/android/calendar/as;->aj:I

    .line 301
    iput-object v2, p0, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    .line 304
    iput-boolean v3, p0, Lcom/android/calendar/as;->al:Z

    .line 305
    iput-object v2, p0, Lcom/android/calendar/as;->am:Ljava/lang/String;

    .line 306
    iput-object v2, p0, Lcom/android/calendar/as;->an:Ljava/lang/String;

    .line 307
    iput-object v2, p0, Lcom/android/calendar/as;->ao:Ljava/lang/String;

    .line 308
    iput-boolean v3, p0, Lcom/android/calendar/as;->ap:Z

    .line 309
    iput-boolean v3, p0, Lcom/android/calendar/as;->aq:Z

    .line 312
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/as;->ar:J

    .line 313
    iput-object v2, p0, Lcom/android/calendar/as;->as:Ljava/lang/String;

    .line 319
    iput v3, p0, Lcom/android/calendar/as;->au:I

    .line 328
    iput-boolean v3, p0, Lcom/android/calendar/as;->ay:Z

    .line 330
    iput-object v2, p0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    .line 335
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    .line 336
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/as;->aw:Ljava/util/ArrayList;

    .line 337
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    .line 338
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 339
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 342
    invoke-direct {p0}, Lcom/android/calendar/as;-><init>()V

    .line 344
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 346
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 349
    invoke-direct {p0, p1}, Lcom/android/calendar/as;-><init>(Landroid/content/Context;)V

    .line 351
    if-nez p2, :cond_1

    .line 429
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    const-string v0, "title"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_2

    .line 357
    iput-object v0, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 359
    :cond_2
    const-string v0, "calendar_id"

    const-wide/16 v4, -0x1

    invoke-virtual {p2, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 360
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    .line 361
    iput-wide v4, p0, Lcom/android/calendar/as;->c:J

    .line 364
    :cond_3
    const-string v0, "eventLocation"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 365
    if-eqz v0, :cond_4

    .line 366
    iput-object v0, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 368
    :cond_4
    const-string v0, "latitude"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->ai:I

    .line 369
    const-string v0, "longitude"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->aj:I

    .line 371
    const-string v0, "description"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 372
    if-eqz v0, :cond_5

    .line 373
    iput-object v0, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    .line 376
    :cond_5
    const-string v0, "availability"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 377
    if-eq v0, v3, :cond_6

    .line 378
    if-eqz v0, :cond_b

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/as;->I:Z

    .line 381
    :cond_6
    const-string v0, "accessLevel"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 382
    if-eq v0, v3, :cond_7

    .line 383
    iput v0, p0, Lcom/android/calendar/as;->au:I

    .line 386
    :cond_7
    const-string v0, "rrule"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 387
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 388
    iput-object v0, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 391
    :cond_8
    const-string v0, "rdate"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 392
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 393
    iput-object v0, p0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    .line 396
    :cond_9
    const-string v0, "android.intent.extra.EMAIL"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 398
    const-string v3, "[ ,;]"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 399
    array-length v4, v3

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_c

    aget-object v5, v3, v0

    .line 400
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    const-string v6, "@"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 401
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 402
    iget-object v6, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 403
    iget-object v6, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    new-instance v7, Lcom/android/calendar/at;

    const-string v8, ""

    invoke-direct {v7, v8, v5}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v5, v7}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_b
    move v0, v2

    .line 378
    goto :goto_1

    .line 409
    :cond_c
    const-string v0, "allDay"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/as;->G:Z

    .line 412
    const-string v0, "filepath"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->am:Ljava/lang/String;

    .line 413
    const-string v0, "page_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->an:Ljava/lang/String;

    .line 415
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 416
    if-eqz v3, :cond_0

    .line 417
    const-string v0, "thumbnail"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 418
    if-eqz v0, :cond_d

    .line 419
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->ao:Ljava/lang/String;

    .line 422
    :cond_d
    const-string v0, "SnbFileLock"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/as;->ap:Z

    .line 423
    iget-object v0, p0, Lcom/android/calendar/as;->an:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/as;->am:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/as;->am:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/as;->an:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    iput-boolean v1, p0, Lcom/android/calendar/as;->al:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Lcom/android/calendar/at;)V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/android/b/b;)V
    .locals 4

    .prologue
    .line 512
    invoke-static {p1, p2}, Lcom/android/calendar/event/av;->a(Ljava/lang/String;Lcom/android/b/b;)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 514
    monitor-enter p0

    .line 515
    :try_start_0
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/util/Rfc822Token;

    .line 516
    new-instance v2, Lcom/android/calendar/at;

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    iget-object v0, v2, Lcom/android/calendar/at;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, v2, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/android/calendar/at;->a:Ljava/lang/String;

    .line 520
    :cond_0
    invoke-virtual {p0, v2}, Lcom/android/calendar/as;->a(Lcom/android/calendar/at;)V

    goto :goto_0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523
    return-void
.end method

.method public a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 432
    iget-wide v2, p0, Lcom/android/calendar/as;->c:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 438
    :cond_0
    :goto_0
    return v0

    .line 435
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 438
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/as;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 704
    iget-boolean v0, p0, Lcom/android/calendar/as;->aq:Z

    if-eqz v0, :cond_1

    .line 804
    :cond_0
    :goto_0
    return v1

    .line 707
    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v2

    .line 708
    goto :goto_0

    .line 710
    :cond_2
    if-eqz p1, :cond_0

    .line 714
    invoke-virtual {p0, p1}, Lcom/android/calendar/as;->b(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 719
    iget-object v0, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 727
    iget-object v0, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 735
    iget-object v0, p1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 743
    iget-object v0, p1, Lcom/android/calendar/as;->D:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    :cond_6
    iget-wide v4, p0, Lcom/android/calendar/as;->B:J

    iget-wide v6, p0, Lcom/android/calendar/as;->A:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 753
    iget-wide v4, p0, Lcom/android/calendar/as;->y:J

    iget-wide v6, p0, Lcom/android/calendar/as;->x:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 757
    iget-object v0, p1, Lcom/android/calendar/as;->az:Ljava/lang/String;

    const-string v3, "com.android.exchange"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 758
    iget v0, p0, Lcom/android/calendar/as;->J:I

    iget v3, p1, Lcom/android/calendar/as;->J:I

    if-ne v0, v3, :cond_0

    .line 765
    :cond_7
    iget-wide v4, p0, Lcom/android/calendar/as;->aa:J

    iget-wide v6, p1, Lcom/android/calendar/as;->aa:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_8

    iget-wide v4, p0, Lcom/android/calendar/as;->aa:J

    iget-wide v6, p1, Lcom/android/calendar/as;->b:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 769
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 771
    iget-object v0, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 772
    iget-object v0, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    :cond_9
    move v0, v2

    .line 774
    :goto_1
    iget-wide v4, p0, Lcom/android/calendar/as;->aa:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_a

    iget-wide v4, p0, Lcom/android/calendar/as;->aa:J

    iget-wide v6, p1, Lcom/android/calendar/as;->b:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_11

    :cond_a
    move v3, v2

    .line 776
    :goto_2
    if-eqz v0, :cond_b

    if-nez v3, :cond_0

    .line 785
    :cond_b
    iget v0, p0, Lcom/android/calendar/as;->ai:I

    iget v3, p1, Lcom/android/calendar/as;->ai:I

    if-ne v0, v3, :cond_0

    .line 788
    iget v0, p0, Lcom/android/calendar/as;->aj:I

    iget v3, p1, Lcom/android/calendar/as;->aj:I

    if-ne v0, v3, :cond_0

    .line 791
    iget-object v0, p0, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    iget-object v3, p1, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    if-ne v0, v3, :cond_0

    .line 796
    iget-boolean v0, p0, Lcom/android/calendar/as;->ay:Z

    iget-boolean v3, p1, Lcom/android/calendar/as;->ay:Z

    if-ne v0, v3, :cond_0

    .line 800
    iget-wide v4, p0, Lcom/android/calendar/as;->ar:J

    iget-wide v6, p1, Lcom/android/calendar/as;->ar:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v1, v2

    .line 804
    goto/16 :goto_0

    .line 722
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto/16 :goto_0

    .line 730
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_0

    .line 738
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    goto/16 :goto_0

    .line 746
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->D:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    goto/16 :goto_0

    :cond_10
    move v0, v1

    .line 772
    goto :goto_1

    :cond_11
    move v3, v1

    .line 774
    goto :goto_2

    .line 780
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    goto/16 :goto_0
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 444
    iput-object v1, p0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 445
    iput-wide v4, p0, Lcom/android/calendar/as;->b:J

    .line 446
    iput-wide v4, p0, Lcom/android/calendar/as;->c:J

    .line 448
    iput-object v1, p0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    .line 449
    iput-object v1, p0, Lcom/android/calendar/as;->k:Ljava/lang/String;

    .line 450
    iput-object v1, p0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    .line 451
    iput-object v1, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 453
    iput-object v1, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 454
    iput-object v1, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 455
    iput-object v1, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    .line 456
    iput-object v1, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 457
    iput-object v1, p0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    .line 458
    iput-object v1, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    .line 459
    iput-object v1, p0, Lcom/android/calendar/as;->u:Ljava/lang/String;

    .line 460
    iput-boolean v0, p0, Lcom/android/calendar/as;->v:Z

    .line 461
    iput-boolean v0, p0, Lcom/android/calendar/as;->w:Z

    .line 463
    iput-wide v4, p0, Lcom/android/calendar/as;->x:J

    .line 464
    iput-wide v4, p0, Lcom/android/calendar/as;->y:J

    .line 465
    iput-wide v4, p0, Lcom/android/calendar/as;->A:J

    .line 466
    iput-wide v4, p0, Lcom/android/calendar/as;->B:J

    .line 467
    iput-object v1, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    .line 468
    iput-object v1, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 469
    iput-object v1, p0, Lcom/android/calendar/as;->F:Ljava/lang/String;

    .line 470
    iput-boolean v2, p0, Lcom/android/calendar/as;->G:Z

    .line 471
    iput-boolean v2, p0, Lcom/android/calendar/as;->H:Z

    .line 473
    iput-boolean v0, p0, Lcom/android/calendar/as;->W:Z

    .line 474
    iput v3, p0, Lcom/android/calendar/as;->X:I

    .line 475
    iput v3, p0, Lcom/android/calendar/as;->Y:I

    .line 476
    iput-wide v4, p0, Lcom/android/calendar/as;->aa:J

    .line 477
    iput-object v1, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    .line 478
    iput-object v1, p0, Lcom/android/calendar/as;->ab:Ljava/lang/Long;

    .line 479
    iput-object v1, p0, Lcom/android/calendar/as;->ac:Ljava/lang/Boolean;

    .line 481
    iput-boolean v2, p0, Lcom/android/calendar/as;->ad:Z

    .line 482
    iput-boolean v2, p0, Lcom/android/calendar/as;->ae:Z

    .line 483
    iput-boolean v2, p0, Lcom/android/calendar/as;->af:Z

    .line 484
    iput v2, p0, Lcom/android/calendar/as;->au:I

    .line 485
    iput-boolean v2, p0, Lcom/android/calendar/as;->ag:Z

    .line 486
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/as;->ah:I

    .line 487
    iput-boolean v2, p0, Lcom/android/calendar/as;->at:Z

    .line 490
    iput v2, p0, Lcom/android/calendar/as;->ai:I

    .line 491
    iput v2, p0, Lcom/android/calendar/as;->aj:I

    .line 492
    iput-object v1, p0, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    .line 495
    iput-object v1, p0, Lcom/android/calendar/as;->O:Ljava/lang/String;

    .line 496
    iput v3, p0, Lcom/android/calendar/as;->P:I

    .line 498
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    .line 499
    iget-object v0, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 502
    iput-boolean v2, p0, Lcom/android/calendar/as;->ay:Z

    .line 504
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/as;->ar:J

    .line 505
    return-void
.end method

.method protected b(Lcom/android/calendar/as;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 817
    iget-boolean v1, p0, Lcom/android/calendar/as;->G:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->G:Z

    if-eq v1, v2, :cond_1

    .line 974
    :cond_0
    :goto_0
    return v0

    .line 820
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    if-nez v1, :cond_e

    .line 821
    iget-object v1, p1, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    if-nez v1, :cond_0

    .line 828
    :cond_2
    iget-wide v2, p0, Lcom/android/calendar/as;->c:J

    iget-wide v4, p1, Lcom/android/calendar/as;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 832
    iget-boolean v1, p0, Lcom/android/calendar/as;->ae:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->ae:Z

    if-ne v1, v2, :cond_0

    .line 835
    iget-boolean v1, p0, Lcom/android/calendar/as;->ad:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->ad:Z

    if-ne v1, v2, :cond_0

    .line 838
    iget-boolean v1, p0, Lcom/android/calendar/as;->af:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->af:Z

    if-ne v1, v2, :cond_0

    .line 841
    iget-boolean v1, p0, Lcom/android/calendar/as;->ag:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->ag:Z

    if-ne v1, v2, :cond_0

    .line 844
    iget v1, p0, Lcom/android/calendar/as;->ah:I

    iget v2, p1, Lcom/android/calendar/as;->ah:I

    if-ne v1, v2, :cond_0

    .line 847
    iget-boolean v1, p0, Lcom/android/calendar/as;->at:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->at:Z

    if-ne v1, v2, :cond_0

    .line 850
    iget-boolean v1, p0, Lcom/android/calendar/as;->H:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->H:Z

    if-ne v1, v2, :cond_0

    .line 853
    iget-boolean v1, p0, Lcom/android/calendar/as;->W:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->W:Z

    if-ne v1, v2, :cond_0

    .line 856
    iget-wide v2, p0, Lcom/android/calendar/as;->b:J

    iget-wide v4, p1, Lcom/android/calendar/as;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 859
    iget-boolean v1, p0, Lcom/android/calendar/as;->v:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->v:Z

    if-ne v1, v2, :cond_0

    .line 863
    iget-object v1, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    if-nez v1, :cond_f

    .line 864
    iget-object v1, p1, Lcom/android/calendar/as;->t:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 871
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/as;->ac:Ljava/lang/Boolean;

    if-nez v1, :cond_10

    .line 872
    iget-object v1, p1, Lcom/android/calendar/as;->ac:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 879
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/as;->ab:Ljava/lang/Long;

    if-nez v1, :cond_11

    .line 880
    iget-object v1, p1, Lcom/android/calendar/as;->ab:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 887
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-nez v1, :cond_12

    .line 888
    iget-object v1, p1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 895
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    if-nez v1, :cond_13

    .line 896
    iget-object v1, p1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 903
    :cond_7
    iget v1, p0, Lcom/android/calendar/as;->X:I

    iget v2, p1, Lcom/android/calendar/as;->X:I

    if-ne v1, v2, :cond_0

    .line 906
    iget v1, p0, Lcom/android/calendar/as;->Y:I

    iget v2, p1, Lcom/android/calendar/as;->Y:I

    if-ne v1, v2, :cond_0

    .line 909
    iget-object v1, p0, Lcom/android/calendar/as;->k:Ljava/lang/String;

    if-nez v1, :cond_14

    .line 910
    iget-object v1, p1, Lcom/android/calendar/as;->k:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 917
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    if-nez v1, :cond_15

    .line 918
    iget-object v1, p1, Lcom/android/calendar/as;->l:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 925
    :cond_9
    iget-object v1, p0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-nez v1, :cond_16

    .line 926
    iget-object v1, p1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 933
    :cond_a
    iget-object v1, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    if-nez v1, :cond_17

    .line 934
    iget-object v1, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 941
    :cond_b
    iget-object v1, p0, Lcom/android/calendar/as;->F:Ljava/lang/String;

    if-nez v1, :cond_18

    .line 942
    iget-object v1, p1, Lcom/android/calendar/as;->F:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 949
    :cond_c
    iget-boolean v1, p0, Lcom/android/calendar/as;->I:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->I:Z

    if-ne v1, v2, :cond_0

    .line 953
    iget-object v1, p0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-nez v1, :cond_19

    .line 954
    iget-object v1, p1, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 961
    :cond_d
    iget v1, p0, Lcom/android/calendar/as;->au:I

    iget v2, p1, Lcom/android/calendar/as;->au:I

    if-ne v1, v2, :cond_0

    .line 966
    iget-boolean v1, p0, Lcom/android/calendar/as;->ay:Z

    iget-boolean v2, p1, Lcom/android/calendar/as;->ay:Z

    if-ne v1, v2, :cond_0

    .line 970
    iget-wide v2, p0, Lcom/android/calendar/as;->ar:J

    iget-wide v4, p1, Lcom/android/calendar/as;->ar:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 974
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 824
    :cond_e
    iget-object v1, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    iget-object v2, p1, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 867
    :cond_f
    iget-object v1, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/as;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 875
    :cond_10
    iget-object v1, p0, Lcom/android/calendar/as;->ac:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/android/calendar/as;->ac:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 883
    :cond_11
    iget-object v1, p0, Lcom/android/calendar/as;->ab:Ljava/lang/Long;

    iget-object v2, p1, Lcom/android/calendar/as;->ab:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 891
    :cond_12
    iget-object v1, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 899
    :cond_13
    iget-object v1, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 913
    :cond_14
    iget-object v1, p0, Lcom/android/calendar/as;->k:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/as;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 921
    :cond_15
    iget-object v1, p0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/as;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 929
    :cond_16
    iget-object v1, p0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 937
    :cond_17
    iget-object v1, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 945
    :cond_18
    iget-object v1, p0, Lcom/android/calendar/as;->F:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/as;->F:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 957
    :cond_19
    iget-object v1, p0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/as;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 6

    .prologue
    .line 530
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 531
    iget-object v0, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 532
    iget-object v3, v0, Lcom/android/calendar/at;->a:Ljava/lang/String;

    .line 533
    iget-object v4, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    .line 534
    iget v0, v0, Lcom/android/calendar/at;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 535
    const-string v5, "name:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    const-string v3, " email:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 537
    const-string v3, " status:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 539
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/android/calendar/as;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 979
    if-nez p1, :cond_1

    .line 1091
    :cond_0
    :goto_0
    return v0

    .line 983
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 984
    goto :goto_0

    .line 987
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 988
    goto :goto_0

    .line 990
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 991
    goto :goto_0

    .line 994
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 995
    goto :goto_0

    .line 997
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 998
    goto :goto_0

    .line 1001
    :cond_6
    iget-boolean v2, p0, Lcom/android/calendar/as;->G:Z

    iget-boolean v3, p1, Lcom/android/calendar/as;->G:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1002
    goto :goto_0

    .line 1005
    :cond_7
    iget-wide v2, p0, Lcom/android/calendar/as;->y:J

    iget-wide v4, p1, Lcom/android/calendar/as;->y:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    move v0, v1

    .line 1006
    goto :goto_0

    .line 1009
    :cond_8
    iget-wide v2, p0, Lcom/android/calendar/as;->B:J

    iget-wide v4, p1, Lcom/android/calendar/as;->B:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    move v0, v1

    .line 1010
    goto :goto_0

    .line 1019
    :cond_9
    iget-wide v2, p0, Lcom/android/calendar/as;->b:J

    iget-wide v4, p1, Lcom/android/calendar/as;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    move v0, v1

    .line 1020
    goto :goto_0

    .line 1023
    :cond_a
    iget v2, p0, Lcom/android/calendar/as;->i:I

    iget v3, p1, Lcom/android/calendar/as;->i:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1024
    goto :goto_0

    .line 1027
    :cond_b
    iget-object v2, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 1028
    iget-object v2, p1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 1029
    goto/16 :goto_0

    .line 1031
    :cond_c
    iget-object v2, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1032
    goto/16 :goto_0

    .line 1035
    :cond_d
    iget-object v2, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 1036
    iget-object v2, p1, Lcom/android/calendar/as;->t:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    .line 1037
    goto/16 :goto_0

    .line 1039
    :cond_e
    iget-object v2, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 1040
    goto/16 :goto_0

    .line 1043
    :cond_f
    iget-boolean v2, p0, Lcom/android/calendar/as;->I:Z

    iget-boolean v3, p1, Lcom/android/calendar/as;->I:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1044
    goto/16 :goto_0

    .line 1047
    :cond_10
    iget v2, p0, Lcom/android/calendar/as;->au:I

    iget v3, p1, Lcom/android/calendar/as;->au:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1048
    goto/16 :goto_0

    .line 1051
    :cond_11
    iget v2, p0, Lcom/android/calendar/as;->J:I

    iget v3, p1, Lcom/android/calendar/as;->J:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1052
    goto/16 :goto_0

    .line 1055
    :cond_12
    iget v2, p0, Lcom/android/calendar/as;->P:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_13

    iget-object v2, p0, Lcom/android/calendar/as;->O:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/android/calendar/as;->O:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->O:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 1056
    goto/16 :goto_0

    .line 1059
    :cond_13
    iget-object v2, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    if-nez v2, :cond_14

    .line 1060
    iget-object v2, p1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    if-eqz v2, :cond_15

    move v0, v1

    .line 1061
    goto/16 :goto_0

    .line 1063
    :cond_14
    iget-object v2, p1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    if-eqz v2, :cond_15

    .line 1064
    iget-object v2, p1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1065
    iget-object v2, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 1066
    goto/16 :goto_0

    .line 1071
    :cond_15
    iget-boolean v2, p0, Lcom/android/calendar/as;->ay:Z

    iget-boolean v3, p1, Lcom/android/calendar/as;->ay:Z

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 1072
    goto/16 :goto_0

    .line 1075
    :cond_16
    iget-wide v2, p0, Lcom/android/calendar/as;->ar:J

    iget-wide v4, p1, Lcom/android/calendar/as;->ar:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_17

    move v0, v1

    .line 1076
    goto/16 :goto_0

    .line 1079
    :cond_17
    iget-object v2, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-nez v2, :cond_18

    .line 1080
    iget-object v2, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-eqz v2, :cond_19

    move v0, v1

    .line 1081
    goto/16 :goto_0

    .line 1083
    :cond_18
    iget-object v2, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    move v0, v1

    .line 1084
    goto/16 :goto_0

    .line 1087
    :cond_19
    iget-object v2, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->size()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 1088
    goto/16 :goto_0
.end method

.method public d()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1131
    iget-object v1, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1147
    :cond_0
    :goto_0
    return v0

    .line 1135
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 1139
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1143
    iget-wide v2, p0, Lcom/android/calendar/as;->ar:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1147
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lcom/android/calendar/as;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1095
    if-nez p1, :cond_1

    .line 1127
    :cond_0
    :goto_0
    return v0

    .line 1099
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 1100
    goto :goto_0

    .line 1103
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1104
    goto :goto_0

    .line 1107
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1108
    goto :goto_0

    .line 1111
    :cond_4
    iget-boolean v2, p0, Lcom/android/calendar/as;->G:Z

    iget-boolean v3, p1, Lcom/android/calendar/as;->G:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1112
    goto :goto_0

    .line 1115
    :cond_5
    iget-wide v2, p0, Lcom/android/calendar/as;->y:J

    iget-wide v4, p1, Lcom/android/calendar/as;->y:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 1116
    goto :goto_0

    .line 1119
    :cond_6
    iget-wide v2, p0, Lcom/android/calendar/as;->B:J

    iget-wide v4, p1, Lcom/android/calendar/as;->B:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 1120
    goto :goto_0

    .line 1123
    :cond_7
    iget-wide v2, p0, Lcom/android/calendar/as;->b:J

    iget-wide v4, p1, Lcom/android/calendar/as;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 1124
    goto :goto_0
.end method

.method public e()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1156
    iget-object v0, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v4, :cond_1

    .line 1174
    :cond_0
    return v4

    .line 1161
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1164
    iget-object v0, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 1165
    iget-object v1, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    move-object v2, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1166
    iget-object v0, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 1167
    invoke-virtual {v2, v0}, Lcom/android/calendar/au;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1169
    iget-object v2, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1165
    :cond_2
    add-int/lit8 v1, v1, -0x1

    move-object v2, v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 593
    if-ne p0, p1, :cond_1

    .line 694
    :cond_0
    :goto_0
    return v0

    .line 596
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 597
    goto :goto_0

    .line 599
    :cond_2
    instance-of v2, p1, Lcom/android/calendar/as;

    if-nez v2, :cond_3

    move v0, v1

    .line 600
    goto :goto_0

    .line 603
    :cond_3
    check-cast p1, Lcom/android/calendar/as;

    .line 604
    invoke-virtual {p0, p1}, Lcom/android/calendar/as;->b(Lcom/android/calendar/as;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 605
    goto :goto_0

    .line 608
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 609
    iget-object v2, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 610
    goto :goto_0

    .line 612
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 613
    goto :goto_0

    .line 616
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 617
    iget-object v2, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 618
    goto :goto_0

    .line 620
    :cond_7
    iget-object v2, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 621
    goto :goto_0

    .line 624
    :cond_8
    iget-object v2, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 625
    iget-object v2, p1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 626
    goto :goto_0

    .line 628
    :cond_9
    iget-object v2, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 629
    goto :goto_0

    .line 632
    :cond_a
    iget-object v2, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 633
    iget-object v2, p1, Lcom/android/calendar/as;->D:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 634
    goto :goto_0

    .line 636
    :cond_b
    iget-object v2, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->D:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 637
    goto :goto_0

    .line 640
    :cond_c
    iget-wide v2, p0, Lcom/android/calendar/as;->B:J

    iget-wide v4, p1, Lcom/android/calendar/as;->B:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    move v0, v1

    .line 641
    goto :goto_0

    .line 643
    :cond_d
    iget-boolean v2, p0, Lcom/android/calendar/as;->w:Z

    iget-boolean v3, p1, Lcom/android/calendar/as;->w:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 644
    goto :goto_0

    .line 646
    :cond_e
    iget-wide v2, p0, Lcom/android/calendar/as;->A:J

    iget-wide v4, p1, Lcom/android/calendar/as;->A:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    move v0, v1

    .line 647
    goto/16 :goto_0

    .line 650
    :cond_f
    iget-wide v2, p0, Lcom/android/calendar/as;->x:J

    iget-wide v4, p1, Lcom/android/calendar/as;->x:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    move v0, v1

    .line 651
    goto/16 :goto_0

    .line 653
    :cond_10
    iget-wide v2, p0, Lcom/android/calendar/as;->y:J

    iget-wide v4, p1, Lcom/android/calendar/as;->y:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_11

    move v0, v1

    .line 654
    goto/16 :goto_0

    .line 657
    :cond_11
    iget-wide v2, p0, Lcom/android/calendar/as;->aa:J

    iget-wide v4, p1, Lcom/android/calendar/as;->aa:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    move v0, v1

    .line 658
    goto/16 :goto_0

    .line 661
    :cond_12
    iget-object v2, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 662
    iget-object v2, p1, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    if-eqz v2, :cond_14

    move v0, v1

    .line 663
    goto/16 :goto_0

    .line 665
    :cond_13
    iget-object v2, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 666
    goto/16 :goto_0

    .line 669
    :cond_14
    iget-object v2, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 670
    iget-object v2, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-eqz v2, :cond_16

    move v0, v1

    .line 671
    goto/16 :goto_0

    .line 673
    :cond_15
    iget-object v2, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 674
    goto/16 :goto_0

    .line 677
    :cond_16
    iget-object v2, p0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 678
    iget-object v2, p1, Lcom/android/calendar/as;->s:Ljava/lang/String;

    if-eqz v2, :cond_18

    move v0, v1

    .line 679
    goto/16 :goto_0

    .line 681
    :cond_17
    iget-object v2, p0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 682
    goto/16 :goto_0

    .line 686
    :cond_18
    iget-boolean v2, p0, Lcom/android/calendar/as;->ay:Z

    iget-boolean v3, p1, Lcom/android/calendar/as;->ay:Z

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 687
    goto/16 :goto_0

    .line 690
    :cond_19
    iget-wide v2, p0, Lcom/android/calendar/as;->ar:J

    iget-wide v4, p1, Lcom/android/calendar/as;->ar:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 691
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    const/4 v3, 0x0

    .line 544
    .line 546
    iget-boolean v0, p0, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 547
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    add-int/2addr v0, v4

    .line 548
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/as;->c:J

    iget-wide v6, p0, Lcom/android/calendar/as;->c:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 549
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v3

    :goto_2
    add-int/2addr v0, v4

    .line 550
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v3

    :goto_3
    add-int/2addr v0, v4

    .line 551
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/as;->B:J

    iget-wide v6, p0, Lcom/android/calendar/as;->B:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 552
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->ae:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    .line 553
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->ad:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    .line 554
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->af:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    .line 555
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->ag:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v4

    .line 556
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->at:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v4

    .line 557
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/android/calendar/as;->ah:I

    add-int/2addr v0, v4

    .line 558
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->H:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v4

    .line 559
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->W:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v4

    .line 560
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/as;->b:J

    iget-wide v6, p0, Lcom/android/calendar/as;->b:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 561
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->w:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v4

    .line 562
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->v:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v4

    .line 563
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v3

    :goto_d
    add-int/2addr v0, v4

    .line 564
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    if-nez v0, :cond_e

    move v0, v3

    :goto_e
    add-int/2addr v0, v4

    .line 565
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->ac:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    move v0, v3

    :goto_f
    add-int/2addr v0, v4

    .line 566
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/as;->A:J

    iget-wide v6, p0, Lcom/android/calendar/as;->A:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 567
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v3

    :goto_10
    add-int/2addr v0, v4

    .line 568
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/as;->aa:J

    iget-wide v6, p0, Lcom/android/calendar/as;->A:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 569
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/as;->x:J

    iget-wide v6, p0, Lcom/android/calendar/as;->x:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 570
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->ab:Ljava/lang/Long;

    if-nez v0, :cond_11

    move v0, v3

    :goto_11
    add-int/2addr v0, v4

    .line 571
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-nez v0, :cond_12

    move v0, v3

    :goto_12
    add-int/2addr v0, v4

    .line 572
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    if-nez v0, :cond_13

    move v0, v3

    :goto_13
    add-int/2addr v0, v4

    .line 573
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-nez v0, :cond_14

    move v0, v3

    :goto_14
    add-int/2addr v0, v4

    .line 574
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/android/calendar/as;->X:I

    add-int/2addr v0, v4

    .line 575
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/android/calendar/as;->Y:I

    add-int/2addr v0, v4

    .line 576
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/as;->y:J

    iget-wide v6, p0, Lcom/android/calendar/as;->y:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 577
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->k:Ljava/lang/String;

    if-nez v0, :cond_15

    move v0, v3

    :goto_15
    add-int/2addr v0, v4

    .line 578
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    if-nez v0, :cond_16

    move v0, v3

    :goto_16
    add-int/2addr v0, v4

    .line 579
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-nez v0, :cond_17

    move v0, v3

    :goto_17
    add-int/2addr v0, v4

    .line 580
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    if-nez v0, :cond_18

    move v0, v3

    :goto_18
    add-int/2addr v0, v4

    .line 581
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->F:Ljava/lang/String;

    if-nez v0, :cond_19

    move v0, v3

    :goto_19
    add-int/2addr v0, v4

    .line 582
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-nez v0, :cond_1a

    move v0, v3

    :goto_1a
    add-int/2addr v0, v4

    .line 583
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/as;->I:Z

    if-eqz v0, :cond_1b

    move v0, v1

    :goto_1b
    add-int/2addr v0, v4

    .line 584
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-nez v4, :cond_1c

    :goto_1c
    add-int/2addr v0, v3

    .line 585
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/android/calendar/as;->au:I

    add-int/2addr v0, v3

    .line 586
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/android/calendar/as;->ay:Z

    if-eqz v3, :cond_1d

    :goto_1d
    add-int/2addr v0, v1

    .line 587
    return v0

    :cond_0
    move v0, v2

    .line 546
    goto/16 :goto_0

    .line 547
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/as;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 549
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 550
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 552
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 553
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 554
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 555
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 556
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 558
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 559
    goto/16 :goto_a

    :cond_b
    move v0, v2

    .line 561
    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 562
    goto/16 :goto_c

    .line 563
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 564
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 565
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/as;->ac:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 567
    :cond_10
    iget-object v0, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 570
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/as;->ab:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_11

    .line 571
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_12

    .line 572
    :cond_13
    iget-object v0, p0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    goto/16 :goto_13

    .line 573
    :cond_14
    iget-object v0, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_14

    .line 577
    :cond_15
    iget-object v0, p0, Lcom/android/calendar/as;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_15

    .line 578
    :cond_16
    iget-object v0, p0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_16

    .line 579
    :cond_17
    iget-object v0, p0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_17

    .line 580
    :cond_18
    iget-object v0, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_18

    .line 581
    :cond_19
    iget-object v0, p0, Lcom/android/calendar/as;->F:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_19

    .line 582
    :cond_1a
    iget-object v0, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1a

    :cond_1b
    move v0, v2

    .line 583
    goto/16 :goto_1b

    .line 584
    :cond_1c
    iget-object v3, p0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto/16 :goto_1c

    :cond_1d
    move v1, v2

    .line 586
    goto/16 :goto_1d
.end method
