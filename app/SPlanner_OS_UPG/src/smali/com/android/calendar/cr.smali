.class Lcom/android/calendar/cr;
.super Landroid/app/DialogFragment;
.source "DeleteEventHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/cj;

.field private b:[Ljava/lang/Long;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/android/calendar/cj;[Ljava/lang/Long;Z)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/android/calendar/cr;->a:Lcom/android/calendar/cj;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 352
    iput-object p2, p0, Lcom/android/calendar/cr;->b:[Ljava/lang/Long;

    .line 353
    iput-boolean p3, p0, Lcom/android/calendar/cr;->c:Z

    .line 354
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/cr;)[Ljava/lang/Long;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/android/calendar/cr;->b:[Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/cr;)Z
    .locals 1

    .prologue
    .line 347
    iget-boolean v0, p0, Lcom/android/calendar/cr;->c:Z

    return v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 358
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/android/calendar/cr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 359
    invoke-virtual {p0}, Lcom/android/calendar/cr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f040033

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 360
    const v0, 0x7f1200b9

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 361
    const v2, 0x7f0f03d0

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(I)V

    .line 362
    new-instance v2, Lcom/android/calendar/cs;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/cs;-><init>(Lcom/android/calendar/cr;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 369
    invoke-virtual {p0}, Lcom/android/calendar/cr;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/cr;->b:[Ljava/lang/Long;

    iget-boolean v2, p0, Lcom/android/calendar/cr;->c:Z

    invoke-static {v0, v1, v2}, Lcom/android/calendar/cj;->a(Landroid/app/Activity;[Ljava/lang/Long;Z)Ljava/lang/String;

    move-result-object v0

    .line 370
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/cr;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 371
    const v2, 0x7f0f0159

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 372
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 373
    const v0, 0x7f0f0149

    new-instance v2, Lcom/android/calendar/ct;

    invoke-direct {v2, p0}, Lcom/android/calendar/ct;-><init>(Lcom/android/calendar/cr;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 382
    const/high16 v0, 0x1040000

    new-instance v2, Lcom/android/calendar/cu;

    invoke-direct {v2, p0}, Lcom/android/calendar/cu;-><init>(Lcom/android/calendar/cr;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 393
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/android/calendar/cr;->dismiss()V

    .line 403
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 404
    return-void
.end method
