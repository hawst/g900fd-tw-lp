.class public Lcom/android/calendar/AllInOneActivity;
.super Lcom/android/calendar/a;
.source "AllInOneActivity.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;
.implements Landroid/app/ActionBar$TabListener;
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/android/calendar/ap;
.implements Lcom/android/calendar/event/jd;
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# static fields
.field public static d:J

.field public static e:Landroid/graphics/Bitmap;

.field public static f:Landroid/graphics/Bitmap;

.field private static i:Z

.field private static j:Z

.field private static k:Z

.field private static l:Z


# instance fields
.field private A:Landroid/view/View;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:I

.field private H:I

.field private I:Landroid/widget/RelativeLayout$LayoutParams;

.field private J:Landroid/widget/LinearLayout$LayoutParams;

.field private K:J

.field private L:J

.field private M:J

.field private N:I

.field private O:Z

.field private P:Z

.field private Q:Landroid/app/ActionBar;

.field private R:Landroid/app/ActionBar$Tab;

.field private S:Landroid/app/ActionBar$Tab;

.field private T:Landroid/app/ActionBar$Tab;

.field private U:Landroid/app/ActionBar$Tab;

.field private V:Landroid/app/ActionBar$Tab;

.field private W:Landroid/view/MenuItem;

.field private X:Landroid/view/MenuItem;

.field private Y:Landroid/view/MenuItem;

.field private Z:Landroid/text/format/Time;

.field private aA:Z

.field private aB:Z

.field private aC:Z

.field private aD:Z

.field private aE:Z

.field private aF:Ljava/lang/String;

.field private aG:Z

.field private final aH:Landroid/database/ContentObserver;

.field private aI:Lcom/android/calendar/ad;

.field private aJ:Z

.field private final aK:Landroid/content/BroadcastReceiver;

.field private aL:Landroid/view/View$OnClickListener;

.field private aM:Landroid/view/View$OnLongClickListener;

.field private aN:Ljava/lang/Runnable;

.field private aO:Ljava/lang/Runnable;

.field private aP:Ljava/lang/Runnable;

.field private aa:Ljava/lang/String;

.field private ab:Lcom/android/calendar/ae;

.field private ac:Landroid/support/v4/widget/DrawerLayout;

.field private ad:Landroid/support/v4/app/ActionBarDrawerToggle;

.field private ae:Landroid/widget/ListView;

.field private af:Lcom/android/calendar/db;

.field private ag:Lcom/android/calendar/gl;

.field private ah:Z

.field private ai:I

.field private aj:Ljava/lang/String;

.field private ak:Ljava/lang/String;

.field private al:I

.field private am:I

.field private an:I

.field private ao:I

.field private ap:Landroid/view/View;

.field private aq:Landroid/view/View;

.field private ar:Landroid/os/Handler;

.field private as:I

.field private at:Landroid/widget/Toast;

.field private au:I

.field private av:Z

.field private aw:I

.field private ax:I

.field private ay:Lcom/android/calendar/h/c;

.field private az:Z

.field public g:Landroid/widget/ImageView;

.field private h:Lcom/android/calendar/al;

.field private m:Z

.field private n:Z

.field private o:Landroid/content/ContentResolver;

.field private p:I

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:Landroid/view/View;

.field private z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 206
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/calendar/AllInOneActivity;->d:J

    .line 275
    sput-object v2, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    .line 276
    sput-object v2, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 142
    invoke-direct {p0}, Lcom/android/calendar/a;-><init>()V

    .line 169
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->m:Z

    .line 170
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->n:Z

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->r:Z

    .line 175
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->s:Z

    .line 176
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->t:Z

    .line 177
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->u:Z

    .line 178
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->v:Z

    .line 190
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->F:Z

    .line 199
    iput-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    .line 200
    iput-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    .line 201
    iput-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->M:J

    .line 202
    iput v2, p0, Lcom/android/calendar/AllInOneActivity;->N:I

    .line 203
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->O:Z

    .line 204
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->P:Z

    .line 220
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aa:Ljava/lang/String;

    .line 228
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->ah:Z

    .line 229
    const/high16 v0, 0x20000

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->ai:I

    .line 250
    const/4 v0, -0x2

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->au:I

    .line 251
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->av:Z

    .line 259
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->az:Z

    .line 261
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aA:Z

    .line 264
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aB:Z

    .line 266
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aC:Z

    .line 269
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aD:Z

    .line 272
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aE:Z

    .line 284
    new-instance v0, Lcom/android/calendar/g;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/g;-><init>(Lcom/android/calendar/AllInOneActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aH:Landroid/database/ContentObserver;

    .line 297
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    .line 320
    new-instance v0, Lcom/android/calendar/r;

    invoke-direct {v0, p0}, Lcom/android/calendar/r;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aK:Landroid/content/BroadcastReceiver;

    .line 1073
    new-instance v0, Lcom/android/calendar/w;

    invoke-direct {v0, p0}, Lcom/android/calendar/w;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aL:Landroid/view/View$OnClickListener;

    .line 1092
    new-instance v0, Lcom/android/calendar/y;

    invoke-direct {v0, p0}, Lcom/android/calendar/y;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aM:Landroid/view/View$OnLongClickListener;

    .line 1669
    new-instance v0, Lcom/android/calendar/i;

    invoke-direct {v0, p0}, Lcom/android/calendar/i;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aN:Ljava/lang/Runnable;

    .line 2207
    new-instance v0, Lcom/android/calendar/o;

    invoke-direct {v0, p0}, Lcom/android/calendar/o;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aO:Ljava/lang/Runnable;

    .line 2479
    new-instance v0, Lcom/android/calendar/p;

    invoke-direct {v0, p0}, Lcom/android/calendar/p;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aP:Ljava/lang/Runnable;

    .line 3247
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    .line 1440
    const v0, 0x102002c

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1441
    if-nez v0, :cond_0

    .line 1448
    :goto_0
    return-void

    .line 1444
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1445
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1446
    const/4 v2, 0x0

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1447
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private B()Landroid/view/View;
    .locals 5

    .prologue
    .line 1451
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1452
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 1453
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "action_bar"

    const-string v3, "id"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1454
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private C()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1602
    invoke-static {p0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1603
    const-string v3, "home_tz_enable_by_roaming"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 1604
    invoke-static {p0}, Lcom/android/calendar/hj;->C(Landroid/content/Context;)I

    move-result v3

    .line 1605
    if-eqz v2, :cond_0

    if-eq v0, v3, :cond_1

    :cond_0
    if-nez v2, :cond_2

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private D()V
    .locals 18

    .prologue
    .line 1610
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/AllInOneActivity;->K:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/AllInOneActivity;->L:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/AllInOneActivity;->M:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1611
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 1612
    const-wide/16 v2, -0x1

    .line 1613
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/calendar/AllInOneActivity;->L:J

    cmp-long v4, v16, v4

    if-lez v4, :cond_2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/calendar/AllInOneActivity;->M:J

    cmp-long v4, v16, v4

    if-gez v4, :cond_2

    .line 1616
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v4, 0x2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/AllInOneActivity;->K:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/AllInOneActivity;->L:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/calendar/AllInOneActivity;->M:J

    const/4 v12, -0x1

    const/4 v13, -0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/AllInOneActivity;->N:I

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/calendar/AllInOneActivity;->O:Z

    invoke-static {v3, v14}, Lcom/android/calendar/aq;->a(IZ)J

    move-result-wide v14

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    .line 1620
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/AllInOneActivity;->K:J

    .line 1621
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/AllInOneActivity;->L:J

    .line 1622
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/AllInOneActivity;->M:J

    .line 1623
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/AllInOneActivity;->O:Z

    .line 1628
    :cond_0
    :goto_1
    return-void

    .line 1624
    :cond_1
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/AllInOneActivity;->K:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/AllInOneActivity;->P:Z

    if-eqz v2, :cond_0

    .line 1625
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v4, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/AllInOneActivity;->K:J

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v11}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    .line 1626
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/AllInOneActivity;->K:J

    goto :goto_1

    :cond_2
    move-wide/from16 v16, v2

    goto :goto_0
.end method

.method private E()V
    .locals 4

    .prologue
    .line 1631
    const-string v0, "writingbuddymanagerservice"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1633
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1634
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.samsung.android.writingbuddyservice"

    const-string v3, "com.samsung.android.writingbuddyservice.WritingBuddyServiceStarter"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1636
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1641
    :cond_0
    :goto_0
    return-void

    .line 1637
    :catch_0
    move-exception v0

    .line 1638
    const-string v1, "DirectPenInputSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting writingbuddy service failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private F()V
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 1644
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->B:Ljava/lang/String;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1645
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1646
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x400

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->c()J

    move-result-wide v9

    move-object v1, p0

    move-object v5, v4

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1648
    return-void
.end method

.method private G()V
    .locals 3

    .prologue
    .line 1651
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 1652
    const-string v0, "MonthHoverEventList"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 1653
    const-string v2, "YearViewZoomIn"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/app/DialogFragment;

    .line 1655
    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/app/DialogFragment;)V

    .line 1656
    invoke-direct {p0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/app/DialogFragment;)V

    .line 1657
    return-void
.end method

.method private H()V
    .locals 1

    .prologue
    .line 1718
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->u:Z

    if-eqz v0, :cond_0

    .line 1719
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0, p0}, Lcom/android/calendar/al;->c(Landroid/content/Context;)V

    .line 1720
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->u:Z

    .line 1722
    :cond_0
    return-void
.end method

.method private I()Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1725
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->B()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1726
    if-eqz v0, :cond_2

    .line 1727
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    .line 1728
    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_1

    .line 1729
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1730
    instance-of v5, v1, Lcom/android/internal/widget/ScrollingTabContainerView;

    if-eqz v5, :cond_0

    move-object v0, v1

    .line 1736
    :goto_1
    return-object v0

    .line 1728
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 1734
    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 1736
    goto :goto_1
.end method

.method private J()V
    .locals 2

    .prologue
    .line 1824
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    .line 1825
    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    .line 1833
    :cond_0
    :goto_0
    return-void

    .line 1828
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1829
    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 1830
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/calendar/month/ci;

    if-eqz v1, :cond_0

    .line 1831
    check-cast v0, Lcom/android/calendar/month/ci;

    invoke-virtual {v0}, Lcom/android/calendar/month/ci;->a()V

    goto :goto_0
.end method

.method private K()Z
    .locals 1

    .prologue
    .line 2065
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_0

    .line 2066
    const/4 v0, 0x1

    .line 2068
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private L()V
    .locals 2

    .prologue
    .line 2097
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    .line 2098
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2099
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2100
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/n;

    invoke-direct {v1, p0}, Lcom/android/calendar/n;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2114
    :cond_0
    :goto_0
    return-void

    .line 2107
    :cond_1
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->G:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->setControlsOffset(I)V

    goto :goto_0

    .line 2110
    :cond_2
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2111
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->setControlsOffset(I)V

    goto :goto_0
.end method

.method private M()Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2237
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v4

    .line 2238
    sget-object v2, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    .line 2239
    const-string v6, "tasks"

    .line 2240
    array-length v7, v4

    move v3, v1

    :goto_0
    if-ge v3, v7, :cond_4

    aget-object v8, v4, v3

    .line 2241
    invoke-static {v8, v5}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    invoke-static {v8, v5}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    .line 2243
    :goto_1
    if-eqz v2, :cond_2

    .line 2252
    :cond_0
    :goto_2
    return v0

    :cond_1
    move v2, v1

    .line 2241
    goto :goto_1

    .line 2246
    :cond_2
    invoke-static {v8, v6}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_3

    invoke-static {v8, v6}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v0

    .line 2248
    :goto_3
    if-nez v2, :cond_0

    .line 2240
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_3
    move v2, v1

    .line 2246
    goto :goto_3

    :cond_4
    move v0, v1

    .line 2252
    goto :goto_2
.end method

.method private N()Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2256
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    .line 2257
    sget-object v2, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    .line 2258
    const-string v5, "tasks"

    .line 2260
    array-length v6, v3

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_2

    aget-object v7, v3, v2

    .line 2261
    invoke-static {v7, v4}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2269
    :cond_0
    :goto_1
    return v0

    .line 2265
    :cond_1
    invoke-static {v7, v5}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2260
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2269
    goto :goto_1
.end method

.method private O()V
    .locals 4

    .prologue
    .line 2403
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->t:Z

    .line 2404
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2405
    const-string v1, "query"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2406
    const-string v1, "key_time"

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2407
    const-class v1, Lcom/android/calendar/CalendarSearchActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 2408
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2409
    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->startActivity(Landroid/content/Intent;)V

    .line 2410
    return-void
.end method

.method private P()V
    .locals 2

    .prologue
    .line 2413
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->R()V

    .line 2415
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 2416
    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 2417
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/calendar/month/k;

    if-eqz v1, :cond_0

    .line 2418
    check-cast v0, Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->p()V

    .line 2419
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->a(I)V

    .line 2423
    :cond_0
    sget-boolean v0, Lcom/android/calendar/dz;->b:Z

    if-eqz v0, :cond_3

    .line 2424
    const v0, 0x7f120069

    .line 2429
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2430
    if-eqz v0, :cond_2

    .line 2432
    sget-object v1, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2433
    sget-object v1, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2436
    :cond_1
    const/4 v1, 0x0

    sput-object v1, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    .line 2438
    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    .line 2439
    invoke-virtual {v0}, Landroid/view/View;->buildDrawingCache()V

    .line 2440
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    .line 2442
    :cond_2
    return-void

    .line 2426
    :cond_3
    const v0, 0x7f120072

    goto :goto_0
.end method

.method private Q()V
    .locals 3

    .prologue
    .line 2445
    :try_start_0
    new-instance v0, Lcom/android/calendar/eg;

    invoke-direct {v0}, Lcom/android/calendar/eg;-><init>()V

    .line 2446
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/eg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/eg;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2450
    :goto_0
    return-void

    .line 2447
    :catch_0
    move-exception v0

    .line 2448
    const-string v0, "AllInOneActivity"

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private R()V
    .locals 5

    .prologue
    .line 2454
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 2455
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "action_bar_container"

    const-string v3, "id"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 2456
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2457
    if-eqz v0, :cond_1

    .line 2459
    sget-object v1, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2460
    sget-object v1, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2463
    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    .line 2465
    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    .line 2466
    invoke-virtual {v0}, Landroid/view/View;->buildDrawingCache()V

    .line 2467
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2468
    if-eqz v0, :cond_1

    .line 2469
    sput-object v0, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    .line 2472
    :cond_1
    return-void
.end method

.method private S()V
    .locals 1

    .prologue
    .line 2474
    new-instance v0, Lcom/android/calendar/gg;

    invoke-direct {v0}, Lcom/android/calendar/gg;-><init>()V

    .line 2475
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gg;->a(Landroid/app/FragmentManager;)V

    .line 2476
    return-void
.end method

.method private T()V
    .locals 2

    .prologue
    .line 2490
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->at:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 2491
    const v0, 0x7f0f041c

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->at:Landroid/widget/Toast;

    .line 2493
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->at:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2494
    return-void
.end method

.method private U()V
    .locals 1

    .prologue
    .line 2560
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/calendar/h/c;->a(Landroid/content/Context;)V

    .line 2562
    return-void
.end method

.method private V()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2748
    const v0, 0x7f12006c

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2749
    if-nez v1, :cond_1

    .line 2773
    :cond_0
    :goto_0
    return-void

    .line 2753
    :cond_1
    sget-boolean v0, Lcom/android/calendar/dz;->b:Z

    if-eqz v0, :cond_0

    .line 2754
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v0, v3, :cond_2

    .line 2755
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2760
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2762
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v0, v3, :cond_3

    .line 2763
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0402

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 2768
    :goto_2
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v3, v0, :cond_0

    .line 2769
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2770
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2757
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 2765
    :cond_3
    invoke-static {p0}, Lcom/android/calendar/hj;->v(Landroid/content/Context;)I

    move-result v0

    goto :goto_2
.end method

.method private W()V
    .locals 2

    .prologue
    .line 3155
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3156
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->bringChildToFront(Landroid/view/View;)V

    .line 3158
    :cond_0
    return-void
.end method

.method private X()V
    .locals 2

    .prologue
    .line 3301
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    packed-switch v0, :pswitch_data_0

    .line 3322
    :goto_0
    :pswitch_0
    return-void

    .line 3303
    :pswitch_1
    sget-object v0, Lcom/android/calendar/hj;->w:Ljava/lang/String;

    const-string v1, "Year"

    invoke-static {v0, p0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 3306
    :pswitch_2
    invoke-static {p0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 3307
    sget-object v0, Lcom/android/calendar/hj;->w:Ljava/lang/String;

    const-string v1, "Month"

    invoke-static {v0, p0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 3309
    :cond_0
    sget-object v0, Lcom/android/calendar/hj;->w:Ljava/lang/String;

    const-string v1, "Month and agenda"

    invoke-static {v0, p0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 3313
    :pswitch_3
    sget-object v0, Lcom/android/calendar/hj;->w:Ljava/lang/String;

    const-string v1, "Week"

    invoke-static {v0, p0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 3316
    :pswitch_4
    sget-object v0, Lcom/android/calendar/hj;->w:Ljava/lang/String;

    const-string v1, "Day"

    invoke-static {v0, p0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 3319
    :pswitch_5
    sget-object v0, Lcom/android/calendar/hj;->w:Ljava/lang/String;

    const-string v1, "Agenda"

    invoke-static {v0, p0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 3301
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;Landroid/content/Intent;)J
    .locals 4

    .prologue
    .line 503
    if-eqz p1, :cond_1

    .line 504
    const-string v0, "key_restore_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 517
    :cond_0
    :goto_0
    return-wide v0

    .line 507
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 508
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 510
    invoke-direct {p0, p2}, Lcom/android/calendar/AllInOneActivity;->b(Landroid/content/Intent;)J

    move-result-wide v0

    .line 511
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 516
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->C:Z

    .line 517
    invoke-static {p2}, Lcom/android/calendar/hj;->a(Landroid/content/Intent;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1458
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 1459
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 1460
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1461
    instance-of v3, v0, Landroid/widget/Spinner;

    if-eqz v3, :cond_1

    .line 1471
    :cond_0
    :goto_1
    return-object v0

    .line 1464
    :cond_1
    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    .line 1465
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1466
    if-eqz v0, :cond_2

    instance-of v3, v0, Landroid/widget/Spinner;

    if-nez v3, :cond_0

    .line 1459
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1471
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/AllInOneActivity;Lcom/android/calendar/h/c;)Lcom/android/calendar/h/c;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/android/calendar/AllInOneActivity;->ay:Lcom/android/calendar/h/c;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/AllInOneActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/android/calendar/AllInOneActivity;->aF:Ljava/lang/String;

    return-object p1
.end method

.method private a(JILandroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 1953
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1955
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    if-eqz v0, :cond_0

    .line 1956
    new-instance v0, Lcom/android/calendar/month/k;

    const/4 v2, 0x1

    invoke-direct {v0, p1, p2, v2}, Lcom/android/calendar/month/k;-><init>(JZ)V

    .line 1957
    const v2, 0x7f120076

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1958
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const v3, 0x7f120076

    check-cast v0, Lcom/android/calendar/ap;

    invoke-virtual {v2, v3, v0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 1961
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    if-ne p3, v0, :cond_2

    .line 1962
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1963
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1964
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1969
    :cond_2
    const/4 v0, 0x5

    if-ne p3, v0, :cond_a

    .line 1970
    invoke-static {p0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "preferred_startView"

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->p:I

    .line 1973
    const-wide/16 v2, -0x1

    .line 1974
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1975
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 1976
    if-eqz v4, :cond_9

    .line 1978
    :try_start_0
    invoke-virtual {v4}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1988
    :cond_3
    :goto_0
    const-string v4, "beginTime"

    const-wide/16 v6, -0x1

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1989
    const-string v6, "endTime"

    const-wide/16 v8, -0x1

    invoke-virtual {v0, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 1990
    new-instance v0, Lcom/android/calendar/aq;

    invoke-direct {v0}, Lcom/android/calendar/aq;-><init>()V

    .line 1991
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_4

    .line 1992
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    iput-object v8, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    .line 1993
    iget-object v8, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v8, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1995
    :cond_4
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_5

    .line 1996
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    iput-object v6, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    .line 1997
    iget-object v6, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v6, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1999
    :cond_5
    iput-wide v2, v0, Lcom/android/calendar/aq;->c:J

    .line 2003
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0, p3}, Lcom/android/calendar/al;->c(I)V

    .line 2004
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/al;->b(J)V

    .line 2012
    :cond_6
    :goto_1
    const v2, 0x7f12006d

    const/4 v6, 0x1

    move-object v0, p0

    move v3, p3

    move-wide v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/app/FragmentTransaction;IIJZ)V

    .line 2013
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2016
    invoke-static {p0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    .line 2017
    if-eqz v0, :cond_b

    .line 2018
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 2022
    :goto_2
    invoke-virtual {v4, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 2023
    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->C:Z

    if-eqz v0, :cond_7

    .line 2024
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->B:Ljava/lang/String;

    iput-object v0, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2025
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 2026
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->C:Z

    .line 2028
    :cond_7
    const/4 v0, 0x5

    if-eq p3, v0, :cond_8

    .line 2029
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    move-object v1, p0

    move v8, p3

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2031
    :cond_8
    return-void

    .line 1984
    :cond_9
    if-eqz p4, :cond_3

    const-string v4, "key_event_id"

    invoke-virtual {p4, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1985
    const-string v2, "key_event_id"

    invoke-virtual {p4, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    goto/16 :goto_0

    .line 2007
    :cond_a
    const/4 v0, 0x2

    if-eq p3, v0, :cond_6

    .line 2008
    iput p3, p0, Lcom/android/calendar/AllInOneActivity;->p:I

    goto :goto_1

    .line 2020
    :cond_b
    new-instance v4, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->B:Ljava/lang/String;

    invoke-direct {v4, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1979
    :catch_0
    move-exception v4

    goto/16 :goto_0
.end method

.method private a(Landroid/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 1660
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/DialogFragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1662
    :try_start_0
    invoke-virtual {p1}, Landroid/app/DialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1667
    :cond_0
    :goto_0
    return-void

    .line 1663
    :catch_0
    move-exception v0

    .line 1664
    const-string v0, "AllInOneActivity"

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Landroid/app/FragmentTransaction;IIJZ)V
    .locals 8

    .prologue
    .line 2565
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->m:Z

    if-eqz v0, :cond_1

    .line 2745
    :cond_0
    :goto_0
    return-void

    .line 2569
    :cond_1
    if-nez p6, :cond_2

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    if-eq v0, p3, :cond_0

    .line 2572
    :cond_2
    invoke-direct {p0, p3}, Lcom/android/calendar/AllInOneActivity;->n(I)I

    move-result v0

    .line 2573
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    if-eqz v1, :cond_3

    .line 2574
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    invoke-virtual {v2, v0}, Lcom/android/calendar/db;->a(I)I

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 2576
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_4

    .line 2577
    const/4 p3, 0x4

    .line 2581
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    .line 2583
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    if-eq p3, v0, :cond_6

    .line 2586
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    if-lez v0, :cond_5

    .line 2587
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->p:I

    .line 2589
    :cond_5
    iput p3, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    .line 2592
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 2593
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 2595
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->V()V

    .line 2599
    const/4 v0, 0x0

    .line 2600
    const/4 v1, 0x0

    .line 2601
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    if-eqz v2, :cond_7

    sget-boolean v2, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v2, :cond_7

    .line 2602
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2605
    :cond_7
    iget-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v2, :cond_8

    .line 2606
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2612
    :goto_1
    packed-switch p3, :pswitch_data_0

    .line 2695
    :pswitch_0
    const-string v0, "AllInOneActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Must be Agenda, Day, Week, or Month ViewType, not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2696
    const/4 v3, 0x4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/app/FragmentTransaction;IIJZ)V

    goto/16 :goto_0

    .line 2608
    :cond_8
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2609
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->W()V

    goto :goto_1

    .line 2614
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-eqz v0, :cond_9

    .line 2615
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    if-eq v0, v2, :cond_9

    .line 2616
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    .line 2619
    :cond_9
    new-instance v2, Lcom/android/calendar/month/ci;

    invoke-direct {v2, p4, p5}, Lcom/android/calendar/month/ci;-><init>(J)V

    .line 2620
    new-instance v0, Lcom/android/calendar/f/o;

    invoke-direct {v0}, Lcom/android/calendar/f/o;-><init>()V

    .line 2621
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/android/calendar/al;->a(I)V

    .line 2622
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    .line 2699
    :goto_2
    invoke-direct {p0, p3}, Lcom/android/calendar/AllInOneActivity;->m(I)V

    .line 2701
    const/4 v3, 0x0

    .line 2702
    if-nez p1, :cond_a

    .line 2703
    const/4 v3, 0x1

    .line 2704
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object p1

    .line 2711
    :cond_a
    if-eqz v0, :cond_b

    .line 2712
    iget-boolean v5, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v5, :cond_1a

    iget v5, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1a

    .line 2713
    const v5, 0x7f1202fe

    invoke-virtual {p1, v5, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2714
    iget-object v5, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const v6, 0x7f1202fe

    check-cast v0, Lcom/android/calendar/ap;

    invoke-virtual {v5, v6, v0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 2721
    :cond_b
    :goto_3
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->v:Z

    if-eqz v0, :cond_1b

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v5, 0x1

    if-ne v0, v5, :cond_1b

    .line 2722
    invoke-virtual {v4, p2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 2723
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    check-cast v0, Lcom/android/calendar/ap;

    invoke-virtual {v1, p2, v0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 2728
    :goto_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->v:Z

    .line 2730
    if-eqz v2, :cond_c

    .line 2731
    const v0, 0x7f120071

    invoke-virtual {p1, v0, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2732
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const v4, 0x7f120071

    move-object v0, v2

    check-cast v0, Lcom/android/calendar/ap;

    invoke-virtual {v1, v4, v0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 2739
    :cond_c
    if-eqz v3, :cond_0

    .line 2743
    invoke-virtual {p1}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    .line 2625
    :pswitch_2
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-eqz v2, :cond_d

    .line 2626
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    if-eq v2, v3, :cond_d

    .line 2627
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    .line 2630
    :cond_d
    new-instance v2, Lcom/android/calendar/agenda/z;

    const/4 v3, 0x0

    invoke-direct {v2, p4, p5, v3}, Lcom/android/calendar/agenda/z;-><init>(JZ)V

    .line 2631
    sget-boolean v3, Lcom/android/calendar/dz;->c:Z

    if-eqz v3, :cond_e

    .line 2632
    new-instance v0, Lcom/android/calendar/f/o;

    invoke-direct {v0}, Lcom/android/calendar/f/o;-><init>()V

    .line 2634
    :cond_e
    iget-boolean v3, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    if-eqz v3, :cond_f

    .line 2635
    new-instance v1, Lcom/android/calendar/selectcalendars/bi;

    invoke-direct {v1}, Lcom/android/calendar/selectcalendars/bi;-><init>()V

    .line 2637
    :cond_f
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/android/calendar/al;->a(I)V

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    .line 2638
    goto/16 :goto_2

    .line 2640
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-eqz v0, :cond_10

    .line 2641
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    if-eq v0, v2, :cond_10

    .line 2642
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    .line 2645
    :cond_10
    new-instance v2, Lcom/android/calendar/e/d;

    const/4 v0, 0x1

    invoke-direct {v2, p4, p5, v0}, Lcom/android/calendar/e/d;-><init>(JI)V

    .line 2646
    new-instance v0, Lcom/android/calendar/f/g;

    invoke-direct {v0}, Lcom/android/calendar/f/g;-><init>()V

    .line 2647
    iget-boolean v3, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    if-eqz v3, :cond_11

    .line 2648
    new-instance v1, Lcom/android/calendar/month/av;

    invoke-direct {v1, p4, p5}, Lcom/android/calendar/month/av;-><init>(J)V

    .line 2650
    :cond_11
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/android/calendar/al;->a(I)V

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    .line 2651
    goto/16 :goto_2

    .line 2653
    :pswitch_4
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-eqz v0, :cond_12

    .line 2654
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    if-eq v0, v2, :cond_12

    .line 2655
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    .line 2659
    :cond_12
    new-instance v2, Lcom/android/calendar/e/d;

    const/4 v0, 0x7

    invoke-direct {v2, p4, p5, v0}, Lcom/android/calendar/e/d;-><init>(JI)V

    .line 2660
    new-instance v0, Lcom/android/calendar/f/n;

    invoke-direct {v0}, Lcom/android/calendar/f/n;-><init>()V

    .line 2661
    iget-boolean v3, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    if-eqz v3, :cond_13

    .line 2662
    new-instance v1, Lcom/android/calendar/month/av;

    invoke-direct {v1, p4, p5}, Lcom/android/calendar/month/av;-><init>(J)V

    .line 2664
    :cond_13
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/android/calendar/al;->a(I)V

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    .line 2665
    goto/16 :goto_2

    .line 2667
    :pswitch_5
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-eqz v0, :cond_14

    .line 2668
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    if-eq v0, v2, :cond_14

    .line 2669
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    .line 2672
    :cond_14
    new-instance v2, Lcom/android/calendar/month/k;

    const/4 v0, 0x0

    invoke-direct {v2, p4, p5, v0}, Lcom/android/calendar/month/k;-><init>(JZ)V

    .line 2673
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_19

    .line 2674
    new-instance v0, Lcom/android/calendar/f/h;

    invoke-direct {v0}, Lcom/android/calendar/f/h;-><init>()V

    .line 2678
    :goto_5
    sget-boolean v3, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v3, :cond_15

    .line 2679
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    if-eqz v3, :cond_15

    .line 2680
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2683
    :cond_15
    iget-boolean v3, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    if-nez v3, :cond_16

    iget-boolean v3, p0, Lcom/android/calendar/AllInOneActivity;->az:Z

    if-nez v3, :cond_16

    sget-boolean v3, Lcom/android/calendar/AllInOneActivity;->l:Z

    if-eqz v3, :cond_17

    iget v3, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_17

    .line 2684
    :cond_16
    new-instance v1, Lcom/android/calendar/month/av;

    invoke-direct {v1, p4, p5}, Lcom/android/calendar/month/av;-><init>(J)V

    .line 2686
    :cond_17
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/android/calendar/al;->a(I)V

    .line 2687
    iget-boolean v3, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v3, :cond_18

    .line 2688
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/android/calendar/al;->a(I)V

    .line 2690
    :cond_18
    invoke-static {p0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v3

    const/4 v5, 0x2

    if-eq v3, v5, :cond_1c

    .line 2691
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto/16 :goto_2

    .line 2676
    :cond_19
    new-instance v0, Lcom/android/calendar/f/i;

    invoke-direct {v0}, Lcom/android/calendar/f/i;-><init>()V

    goto :goto_5

    .line 2716
    :cond_1a
    const v5, 0x7f12006c

    invoke-virtual {p1, v5, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2717
    iget-object v5, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const v6, 0x7f12006c

    check-cast v0, Lcom/android/calendar/ap;

    invoke-virtual {v5, v6, v0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    goto/16 :goto_3

    .line 2725
    :cond_1b
    invoke-virtual {p1, p2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2726
    iget-object v4, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    move-object v0, v1

    check-cast v0, Lcom/android/calendar/ap;

    invoke-virtual {v4, p2, v0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    goto/16 :goto_4

    :cond_1c
    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto/16 :goto_2

    .line 2612
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 537
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 538
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 539
    if-eqz v1, :cond_2

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 540
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "handwriting_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aD:Z

    .line 543
    :cond_0
    const-string v0, "view_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 544
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->b(I)V

    .line 549
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/calendar/hj;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->b(Ljava/lang/String;)V

    .line 550
    return-void

    .line 546
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    if-eqz v1, :cond_1

    const-string v1, "com.sec.android.app.view.calendars"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x2000

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0
.end method

.method private a(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 730
    const v0, 0x7f120076

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    .line 731
    const v0, 0x7f120071

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    .line 732
    const v0, 0x7f120075

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    .line 733
    const v0, 0x7f12006b

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->a:Landroid/view/View;

    .line 734
    const v0, 0x7f120077

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->A:Landroid/view/View;

    .line 735
    const v0, 0x7f120069

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    .line 736
    const v0, 0x7f12006e

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->b:Landroid/widget/ImageView;

    .line 737
    const v0, 0x7f12006f

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    .line 738
    const v0, 0x7f1201d6

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    .line 739
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/android/calendar/g/f;->a(Landroid/view/View;Landroid/content/res/Resources;)V

    .line 740
    const v0, 0x7f1201d7

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ap:Landroid/view/View;

    .line 741
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ap:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/t;

    invoke-direct {v1, p0}, Lcom/android/calendar/t;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 748
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_0

    .line 749
    const v0, 0x7f120079

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    .line 751
    :cond_0
    return-void
.end method

.method private a(Landroid/content/res/Resources;I)V
    .locals 3

    .prologue
    .line 711
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 712
    const v0, 0x7f0c0317

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->G:I

    .line 713
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->I:Landroid/widget/RelativeLayout$LayoutParams;

    if-nez v0, :cond_0

    .line 714
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->G:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->I:Landroid/widget/RelativeLayout$LayoutParams;

    .line 716
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->I:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 720
    :goto_0
    const v0, 0x7f0c006f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->H:I

    .line 721
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_1

    .line 722
    const v0, 0x7f0c036a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->al:I

    .line 724
    :cond_1
    const v0, 0x7f0c0008

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->am:I

    .line 725
    const v0, 0x7f0c0009

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->an:I

    .line 726
    const v0, 0x7f0c0007

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->ao:I

    .line 727
    return-void

    .line 718
    :cond_2
    const v0, 0x7f0c02ac

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->G:I

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 567
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sput-wide v4, Lcom/android/calendar/AllInOneActivity;->d:J

    .line 568
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    .line 569
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/calendar/AllInOneActivity;->k:Z

    .line 570
    invoke-static {p0}, Lcom/android/calendar/dz;->D(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    .line 571
    const v0, 0x7f0a0004

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/AllInOneActivity;->i:Z

    .line 572
    if-eqz p1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->v:Z

    .line 573
    const v0, 0x7f0a0003

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->az:Z

    .line 574
    const v0, 0x7f0a000d

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aB:Z

    .line 575
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aC:Z

    .line 576
    const v0, 0x7f0a0006

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    .line 577
    const v0, 0x7f0a0008

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->E:Z

    .line 580
    invoke-static {p0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->av:Z

    .line 581
    return-void

    :cond_0
    move v0, v2

    .line 569
    goto :goto_0

    :cond_1
    move v1, v2

    .line 572
    goto :goto_1
.end method

.method private a(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2117
    const v1, 0x7f120329

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2118
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    move-result v2

    .line 2119
    if-ne v2, v0, :cond_0

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2120
    return-void

    .line 2119
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1057
    const v0, 0x7f1202fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1058
    if-eqz v0, :cond_0

    .line 1059
    const/16 v1, 0x1335

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1060
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aL:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1061
    const v1, 0x7f0f0211

    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1064
    :cond_0
    const v0, 0x7f1200e0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1065
    if-eqz v0, :cond_1

    .line 1066
    const v1, 0x7f120336

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1067
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aL:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1068
    const v1, 0x7f0f002d

    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1071
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/AllInOneActivity;I)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->l(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/AllInOneActivity;Z)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->g(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;FF)V
    .locals 6

    .prologue
    const v5, 0x7f0b000a

    const/4 v4, 0x0

    .line 1361
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "action_bar_title"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1362
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1363
    if-eqz v0, :cond_0

    .line 1364
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1365
    invoke-virtual {v0, v4, p3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1367
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 1368
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "action_bar_subtitle"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1369
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1370
    if-eqz v0, :cond_1

    .line 1371
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1372
    invoke-virtual {v0, v4, p4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1375
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/AllInOneActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1376
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 1382
    return-void
.end method

.method private a(J)Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 1015
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1021
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "deleted"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1024
    if-eqz v1, :cond_0

    .line 1025
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1026
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_2

    .line 1039
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1040
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1044
    :cond_1
    return v6

    .line 1029
    :cond_2
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1030
    const-string v0, "deleted"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1031
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_4

    move v0, v6

    :goto_1
    move v6, v0

    .line 1034
    goto :goto_0

    .line 1039
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1040
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1039
    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_4
    move v0, v7

    goto :goto_1
.end method

.method private b(Landroid/os/Bundle;Landroid/content/Intent;)I
    .locals 2

    .prologue
    .line 521
    if-eqz p1, :cond_0

    .line 522
    const-string v0, "key_restore_view"

    const/4 v1, -0x2

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 533
    :goto_0
    return v0

    .line 525
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 526
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aE:Z

    if-eqz v1, :cond_1

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 527
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aE:Z

    .line 528
    const-string v0, "preferences_month_views"

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const/4 v0, 0x4

    goto :goto_0

    .line 533
    :cond_1
    invoke-static {p0}, Lcom/android/calendar/hj;->a(Landroid/app/Activity;)I

    move-result v0

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)J
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 913
    .line 914
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 915
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/net/Uri;->isHierarchical()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 916
    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 919
    const-string v0, "sstream"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "content://"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 920
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 923
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_5

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "events"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 925
    :try_start_0
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    .line 926
    iget-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_e

    .line 927
    const-string v0, "beginTime"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    .line 928
    const-string v0, "endTime"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->M:J

    .line 931
    iget-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_2

    .line 932
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "dtstart"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "dtend"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "duration"

    aput-object v1, v2, v0

    .line 938
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 940
    const/4 v8, 0x0

    .line 942
    :try_start_1
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 945
    if-eqz v1, :cond_1

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 946
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    .line 947
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->M:J

    .line 948
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 950
    iget-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->M:J

    cmp-long v2, v2, v10

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    .line 951
    new-instance v2, Lcom/android/calendar/vcal/a/a/e;

    invoke-direct {v2}, Lcom/android/calendar/vcal/a/a/e;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 954
    :try_start_3
    invoke-virtual {v2, v0}, Lcom/android/calendar/vcal/a/a/e;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 958
    :goto_0
    :try_start_4
    iget-wide v4, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    invoke-virtual {v2}, Lcom/android/calendar/vcal/a/a/e;->a()J

    move-result-wide v2

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->M:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 962
    :cond_1
    if-eqz v1, :cond_2

    .line 963
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 968
    :cond_2
    const-string v0, "attendeeStatus"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->N:I

    .line 970
    const-string v0, "allDay"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->O:Z

    .line 971
    iget-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->L:J
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_1

    :goto_1
    move-wide v6, v0

    .line 1011
    :cond_3
    :goto_2
    return-wide v6

    .line 955
    :catch_0
    move-exception v0

    .line 956
    :try_start_6
    invoke-virtual {v0}, Lcom/android/calendar/vcal/a/a/d;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 962
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_4

    .line 963
    :try_start_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_1

    .line 973
    :catch_1
    move-exception v0

    .line 975
    const-string v0, "AllInOneActivity"

    const-string v1, "Wrong event id is given"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 977
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_6

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "syncTasks"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 978
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    .line 979
    iput-boolean v5, p0, Lcom/android/calendar/AllInOneActivity;->P:Z

    goto :goto_2

    .line 980
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_8

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "events"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 981
    const/4 v0, 0x3

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    .line 982
    iget-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_7

    .line 983
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    .line 984
    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->M:J

    .line 986
    :cond_7
    iget-wide v6, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    goto/16 :goto_2

    .line 987
    :cond_8
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "globalSearch"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 988
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_b

    .line 989
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    .line 990
    iput-boolean v5, p0, Lcom/android/calendar/AllInOneActivity;->P:Z

    .line 1000
    :cond_9
    :goto_4
    iget-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/AllInOneActivity;->a(J)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1001
    const v0, 0x7f0f0455

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1002
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->finish()V

    .line 1005
    :cond_a
    iput-boolean v5, p0, Lcom/android/calendar/AllInOneActivity;->F:Z

    goto/16 :goto_2

    .line 991
    :cond_b
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_9

    .line 992
    const/4 v0, 0x3

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    .line 993
    iget-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_c

    .line 994
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    .line 995
    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/AllInOneActivity;->M:J

    .line 997
    :cond_c
    iget-wide v6, p0, Lcom/android/calendar/AllInOneActivity;->L:J

    goto :goto_4

    .line 1006
    :cond_d
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_3

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "handwriting"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1007
    iput-boolean v5, p0, Lcom/android/calendar/AllInOneActivity;->aE:Z

    .line 1008
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto/16 :goto_2

    .line 962
    :catchall_1
    move-exception v0

    move-object v1, v8

    goto/16 :goto_3

    :cond_e
    move-wide v0, v6

    goto/16 :goto_1
.end method

.method private b(Landroid/os/Bundle;)Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 772
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 773
    sget-boolean v1, Lcom/android/calendar/dz;->b:Z

    if-eqz v1, :cond_0

    .line 774
    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 775
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 777
    :cond_0
    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 778
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 779
    const-string v1, "clock.date_format_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 781
    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 783
    invoke-static {}, Lcom/android/calendar/dz;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 784
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ay:Lcom/android/calendar/h/c;

    .line 785
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ay:Lcom/android/calendar/h/c;

    iget-object v1, v1, Lcom/android/calendar/h/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 786
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ay:Lcom/android/calendar/h/c;

    iget-object v1, v1, Lcom/android/calendar/h/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 787
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ay:Lcom/android/calendar/h/c;

    iget-object v1, v1, Lcom/android/calendar/h/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 788
    const-string v1, "com.sec.android.intent.CHANGE_SHARE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 790
    if-nez p1, :cond_1

    .line 791
    invoke-static {p0}, Lcom/android/calendar/hm;->a(Landroid/app/Activity;)V

    .line 794
    :cond_1
    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    return-object v0
.end method

.method private static b(Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 2123
    const v0, 0x7f12032f

    invoke-interface {p0, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2125
    invoke-static {}, Lcom/android/calendar/dz;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    .line 2126
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2130
    :goto_0
    return-void

    .line 2128
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/AllInOneActivity;I)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->j(I)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 496
    sput-object p1, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    .line 497
    sget-object v0, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getTaskId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/hj;->i:Ljava/lang/Integer;

    .line 500
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/AllInOneActivity;Z)Z
    .locals 0

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/h/c;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ay:Lcom/android/calendar/h/c;

    return-object v0
.end method

.method private c(Landroid/view/Menu;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2133
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    move-result v2

    .line 2135
    const v3, 0x7f12032b

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 2137
    if-ne v2, v0, :cond_1

    .line 2138
    sget-boolean v2, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-nez v2, :cond_0

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2142
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2138
    goto :goto_0

    .line 2140
    :cond_1
    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/AllInOneActivity;Z)Z
    .locals 0

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/android/calendar/AllInOneActivity;->ah:Z

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/AllInOneActivity;)I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    return v0
.end method

.method private d(Landroid/view/Menu;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2145
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    .line 2146
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->e()I

    move-result v3

    .line 2148
    const v4, 0x7f12032c

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 2149
    const v5, 0x7f120331

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 2150
    const v6, 0x7f12032e

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 2151
    const v7, 0x7f12032d

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 2153
    if-ne v0, v1, :cond_2

    const/4 v0, 0x2

    if-ne v3, v0, :cond_2

    .line 2154
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2155
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->i()Z

    move-result v0

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2156
    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2157
    const-string v0, "preferences_list_by"

    invoke-static {p0, v0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 2158
    const/4 v3, 0x5

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2165
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 2154
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2158
    goto :goto_1

    .line 2160
    :cond_2
    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2161
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2162
    invoke-interface {v6, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2163
    invoke-interface {v7, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method private e(Landroid/view/Menu;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2168
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2169
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->W:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2170
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->W:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2172
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aO:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2179
    :goto_0
    return-void

    .line 2174
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->W:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 2177
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->W:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/AllInOneActivity;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->q()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/AllInOneActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    return-object v0
.end method

.method private f(I)V
    .locals 3

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 755
    if-eqz v0, :cond_0

    .line 756
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 757
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/2addr v2, p1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 758
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 760
    :cond_0
    return-void
.end method

.method private f(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2182
    const v0, 0x7f120334

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2183
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v1, :cond_0

    .line 2184
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2188
    :goto_0
    return-void

    .line 2186
    :cond_0
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/calendar/AllInOneActivity;)I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    return v0
.end method

.method private g(I)V
    .locals 2

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    .line 1126
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    .line 1128
    :cond_0
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    if-eqz v0, :cond_2

    .line 1129
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->h(I)V

    .line 1130
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 1135
    :goto_0
    return-void

    .line 1132
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->k(I)V

    .line 1133
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->v()V

    goto :goto_0
.end method

.method private g(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2191
    const v0, 0x7f120332

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2192
    if-eqz v0, :cond_0

    .line 2193
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v1, :cond_1

    .line 2194
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2199
    :cond_0
    :goto_0
    return-void

    .line 2196
    :cond_1
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private g(Z)V
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 386
    if-eqz p1, :cond_0

    const-wide/16 v9, 0x0

    .line 387
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/32 v2, 0x10000000

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v5, v4

    move-object v11, v4

    move-object v12, v4

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 388
    return-void

    .line 386
    :cond_0
    const-wide/16 v9, 0x1

    goto :goto_0
.end method

.method private h(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1138
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 1139
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1140
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    if-nez v0, :cond_0

    .line 1141
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    .line 1142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0f0489

    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 1143
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 1145
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 1146
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    if-nez v0, :cond_1

    .line 1147
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    .line 1148
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0f02a8

    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 1149
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 1151
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 1152
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    if-nez v0, :cond_2

    .line 1153
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    .line 1154
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0f0477

    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 1155
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 1157
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 1158
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    if-nez v0, :cond_3

    .line 1159
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    .line 1160
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0f0138

    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 1161
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 1163
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 1164
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    if-nez v0, :cond_4

    .line 1165
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    .line 1166
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    const v1, 0x7f0f0037

    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 1167
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 1169
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 1170
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->i(I)V

    .line 1171
    return-void
.end method

.method private h(Landroid/view/Menu;)V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 2221
    const v0, 0x7f120328

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Y:Landroid/view/MenuItem;

    .line 2223
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_2

    .line 2224
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Y:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 2225
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Y:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2234
    :cond_1
    :goto_0
    return-void

    .line 2231
    :cond_2
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->au:I

    if-eq v0, v1, :cond_1

    .line 2232
    iput v1, p0, Lcom/android/calendar/AllInOneActivity;->au:I

    goto :goto_0
.end method

.method static synthetic h(Lcom/android/calendar/AllInOneActivity;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->y()V

    return-void
.end method

.method private h(Z)V
    .locals 1

    .prologue
    .line 1391
    new-instance v0, Lcom/android/calendar/h;

    invoke-direct {v0, p0}, Lcom/android/calendar/h;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    invoke-static {v0, p0, p1}, Lcom/android/calendar/g/a;->a(Lcom/android/calendar/g/c;Landroid/content/Context;Z)V

    .line 1397
    return-void
.end method

.method private i(I)V
    .locals 2

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    .line 1197
    :goto_0
    return-void

    .line 1177
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1194
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    .line 1179
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    .line 1182
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    .line 1185
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    .line 1188
    :pswitch_4
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    .line 1191
    :pswitch_5
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->selectTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0

    .line 1177
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic i(Lcom/android/calendar/AllInOneActivity;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->z()V

    return-void
.end method

.method private i(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1795
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    .line 1796
    iget-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aB:Z

    if-eqz v2, :cond_3

    .line 1798
    if-nez p1, :cond_5

    .line 1799
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->H:I

    .line 1801
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->setControlsOffset(I)V

    .line 1812
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1813
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-ne v0, v4, :cond_4

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    if-eq v0, v3, :cond_4

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v0, v3, :cond_4

    .line 1816
    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->e(Z)V

    .line 1821
    :cond_1
    :goto_2
    return-void

    .line 1799
    :cond_2
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->G:I

    goto :goto_0

    .line 1803
    :cond_3
    if-ne v0, v4, :cond_1

    .line 1806
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1807
    const v2, 0x7f12006d

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 1808
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/android/calendar/month/k;

    if-eqz v2, :cond_0

    .line 1809
    check-cast v0, Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->o()V

    goto :goto_1

    .line 1818
    :cond_4
    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private j(I)V
    .locals 3

    .prologue
    .line 1200
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1201
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ag:Lcom/android/calendar/gl;

    if-nez v1, :cond_0

    .line 1202
    new-instance v1, Lcom/android/calendar/gl;

    const v2, 0x7f040005

    invoke-direct {v1, p0, v2, v0}, Lcom/android/calendar/gl;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ag:Lcom/android/calendar/gl;

    .line 1205
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1206
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ag:Lcom/android/calendar/gl;

    invoke-virtual {v0, v1, p0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 1207
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->m(I)V

    .line 1208
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->w()V

    .line 1209
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->x()V

    .line 1210
    return-void
.end method

.method static synthetic j(Lcom/android/calendar/AllInOneActivity;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    return v0
.end method

.method static synthetic k(Lcom/android/calendar/AllInOneActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    return-object v0
.end method

.method private k(I)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1237
    iput-object v9, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    .line 1238
    const v0, 0x7f120068

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    .line 1239
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_0

    .line 1240
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v7}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 1245
    :goto_0
    new-instance v0, Lcom/android/calendar/z;

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    const v4, 0x7f020247

    const v5, 0x7f0f0176

    const v6, 0x7f0f0174

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/z;-><init>(Lcom/android/calendar/AllInOneActivity;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    .line 1299
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v7}, Landroid/support/v4/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 1300
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 1301
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1302
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 1303
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1304
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1306
    const v0, 0x7f120070

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    .line 1307
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0175

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1308
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/aa;

    invoke-direct {v1, p0, v9}, Lcom/android/calendar/aa;-><init>(Lcom/android/calendar/AllInOneActivity;Lcom/android/calendar/g;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1309
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 1310
    return-void

    .line 1242
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v8}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0
.end method

.method static synthetic l(Lcom/android/calendar/AllInOneActivity;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method private l(I)V
    .locals 8

    .prologue
    .line 1336
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    .line 1357
    :goto_0
    return-void

    .line 1340
    :cond_0
    const/4 v1, 0x0

    .line 1341
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->an:I

    .line 1342
    iget v3, p0, Lcom/android/calendar/AllInOneActivity;->ao:I

    .line 1343
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    iget-object v4, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->ah:Z

    if-eqz v2, :cond_4

    .line 1344
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->aF:Ljava/lang/String;

    .line 1345
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->am:I

    .line 1356
    :cond_3
    :goto_1
    int-to-float v0, v0

    int-to-float v3, v3

    invoke-direct {p0, v2, v1, v0, v3}, Lcom/android/calendar/AllInOneActivity;->a(Ljava/lang/String;Ljava/lang/String;FF)V

    goto :goto_0

    .line 1346
    :cond_4
    const/4 v2, 0x1

    if-ne p1, v2, :cond_5

    .line 1347
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0037

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1348
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->am:I

    goto :goto_1

    .line 1350
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->aj:Ljava/lang/String;

    .line 1351
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ak:Ljava/lang/String;

    .line 1352
    const/4 v4, 0x4

    if-ne p1, v4, :cond_3

    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1353
    int-to-double v4, v0

    const-wide v6, 0x3fee666666666666L    # 0.95

    mul-double/2addr v4, v6

    double-to-int v0, v4

    goto :goto_1
.end method

.method static synthetic l()Z
    .locals 1

    .prologue
    .line 142
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    return v0
.end method

.method static synthetic m(Lcom/android/calendar/AllInOneActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ap:Landroid/view/View;

    return-object v0
.end method

.method private m(I)V
    .locals 2

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ag:Lcom/android/calendar/gl;

    if-nez v0, :cond_1

    .line 1481
    :cond_0
    :goto_0
    return-void

    .line 1478
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->n(I)I

    move-result v0

    .line 1479
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 1480
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ag:Lcom/android/calendar/gl;

    invoke-virtual {v1, v0}, Lcom/android/calendar/gl;->a(I)V

    goto :goto_0
.end method

.method static synthetic m()Z
    .locals 1

    .prologue
    .line 142
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    return v0
.end method

.method private n(I)I
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 1485
    packed-switch p1, :pswitch_data_0

    .line 1514
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 1487
    :pswitch_1
    const/4 v0, 0x0

    .line 1488
    goto :goto_0

    .line 1491
    :pswitch_2
    invoke-static {p0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v2

    if-eq v2, v1, :cond_0

    move v0, v1

    .line 1496
    goto :goto_0

    .line 1499
    :pswitch_3
    const/4 v0, 0x3

    .line 1500
    goto :goto_0

    .line 1503
    :pswitch_4
    const/4 v0, 0x4

    .line 1504
    goto :goto_0

    .line 1507
    :pswitch_5
    const/4 v0, 0x5

    .line 1508
    goto :goto_0

    .line 1485
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic n(Lcom/android/calendar/AllInOneActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->B()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 553
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v0

    .line 554
    invoke-virtual {v0, p0}, Lcom/android/calendar/h/c;->b(Landroid/content/Context;)V

    .line 555
    return-void
.end method

.method static synthetic o(Lcom/android/calendar/AllInOneActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->I()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 558
    sget-boolean v0, Lcom/android/calendar/dz;->a:Z

    if-eqz v0, :cond_0

    .line 559
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->H(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/dz;->a(Z)V

    .line 561
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/dz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->n()V

    .line 564
    :cond_1
    return-void
.end method

.method private o(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3037
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    .line 3038
    iget-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->aB:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 3044
    :cond_0
    :goto_0
    return v0

    .line 3041
    :cond_1
    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic p(Lcom/android/calendar/AllInOneActivity;)I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->G:I

    return v0
.end method

.method private p()V
    .locals 4

    .prologue
    .line 763
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/u;

    invoke-direct {v1, p0}, Lcom/android/calendar/u;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 769
    return-void
.end method

.method private q()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 798
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 799
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 801
    const-string v3, "ChinaHolidayUpdation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "month="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/text/format/Time;->month:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    iget v3, v2, Landroid/text/format/Time;->month:I

    const/16 v4, 0xb

    if-ne v3, v4, :cond_2

    .line 804
    iget v3, v2, Landroid/text/format/Time;->year:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->year:I

    .line 805
    iput v0, v2, Landroid/text/format/Time;->month:I

    .line 806
    iput v1, v2, Landroid/text/format/Time;->monthDay:I

    .line 807
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/android/calendar/eb;->b(Landroid/content/ContentResolver;Landroid/text/format/Time;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 813
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 814
    const v0, 0x7f0f00ba

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 816
    :cond_1
    return-void

    .line 808
    :cond_2
    iget v3, v2, Landroid/text/format/Time;->month:I

    if-nez v3, :cond_0

    .line 809
    iput v1, v2, Landroid/text/format/Time;->monthDay:I

    .line 810
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/android/calendar/eb;->b(Landroid/content/ContentResolver;Landroid/text/format/Time;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic q(Lcom/android/calendar/AllInOneActivity;)Z
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->N()Z

    move-result v0

    return v0
.end method

.method static synthetic r(Lcom/android/calendar/AllInOneActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aO:Ljava/lang/Runnable;

    return-object v0
.end method

.method private r()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 819
    invoke-static {p0}, Lcom/android/calendar/dz;->D(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820
    invoke-virtual {p0, v1}, Lcom/android/calendar/AllInOneActivity;->setRequestedOrientation(I)V

    .line 823
    :cond_0
    invoke-static {v1}, Lcom/android/calendar/dz;->b(Z)V

    .line 824
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->s()V

    .line 825
    return-void
.end method

.method private s()V
    .locals 3

    .prologue
    .line 828
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 829
    const v1, 0x7f040014

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 831
    new-instance v1, Lcom/android/calendar/c;

    invoke-direct {v1, p0}, Lcom/android/calendar/c;-><init>(Landroid/content/Context;)V

    .line 832
    invoke-virtual {v1, v0}, Lcom/android/calendar/c;->setContentView(Landroid/view/View;)V

    .line 833
    invoke-virtual {v1, p0}, Lcom/android/calendar/c;->setOwnerActivity(Landroid/app/Activity;)V

    .line 834
    new-instance v0, Lcom/android/calendar/v;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/v;-><init>(Lcom/android/calendar/AllInOneActivity;Lcom/android/calendar/c;)V

    invoke-virtual {v1, v0}, Lcom/android/calendar/c;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 845
    invoke-virtual {v1}, Lcom/android/calendar/c;->show()V

    .line 846
    return-void
.end method

.method static synthetic s(Lcom/android/calendar/AllInOneActivity;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->L()V

    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 849
    invoke-static {}, Lcom/android/calendar/AllInOneActivity;->u()V

    .line 850
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 851
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->h(I)V

    .line 852
    return-void
.end method

.method static synthetic t(Lcom/android/calendar/AllInOneActivity;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->P()V

    return-void
.end method

.method static synthetic u(Lcom/android/calendar/AllInOneActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aa:Ljava/lang/String;

    return-object v0
.end method

.method private static u()V
    .locals 0

    .prologue
    .line 855
    return-void
.end method

.method private v()V
    .locals 2

    .prologue
    .line 1048
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 1050
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    const v1, 0x7f0400b7

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 1051
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Q:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    .line 1052
    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/view/View;)V

    .line 1054
    :cond_0
    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 1213
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->B()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1214
    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1215
    if-eqz v0, :cond_0

    .line 1216
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1218
    :cond_0
    return-void
.end method

.method private x()V
    .locals 3

    .prologue
    .line 1221
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_0

    .line 1222
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->B()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1223
    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1224
    if-eqz v1, :cond_0

    .line 1225
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1226
    instance-of v2, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v2, :cond_0

    .line 1227
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1228
    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->al:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1229
    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->al:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1230
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1234
    :cond_0
    return-void
.end method

.method private y()V
    .locals 3

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    invoke-virtual {v0}, Lcom/android/calendar/db;->b()V

    .line 1315
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->closeOptionsMenu()V

    .line 1316
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->l(I)V

    .line 1317
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 1318
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1319
    const v0, 0x7f12006c

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1320
    if-eqz v0, :cond_0

    .line 1321
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1322
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1323
    const/4 v2, 0x0

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1324
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1327
    :cond_0
    return-void
.end method

.method private z()V
    .locals 3

    .prologue
    .line 1400
    new-instance v0, Lcom/android/calendar/db;

    invoke-direct {v0, p0}, Lcom/android/calendar/db;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    .line 1401
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1402
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->n(I)I

    move-result v0

    .line 1403
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    invoke-virtual {v2, v0}, Lcom/android/calendar/db;->a(I)I

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1404
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1330
    iput-object p2, p0, Lcom/android/calendar/AllInOneActivity;->aj:Ljava/lang/String;

    .line 1331
    iput-object p3, p0, Lcom/android/calendar/AllInOneActivity;->ak:Ljava/lang/String;

    .line 1332
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->l(I)V

    .line 1333
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 11

    .prologue
    const/4 v0, -0x1

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/16 v8, 0x8

    const/4 v6, 0x0

    .line 2787
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v7, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2788
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x20

    cmp-long v0, v0, v2

    if-nez v0, :cond_f

    .line 2789
    iget-wide v0, p1, Lcom/android/calendar/aq;->p:J

    const-wide/16 v2, 0x4

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 2790
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->n:Z

    .line 2797
    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;)V

    .line 2798
    const/4 v1, 0x0

    const v2, 0x7f12006d

    iget v3, p1, Lcom/android/calendar/aq;->b:I

    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/app/FragmentTransaction;IIJZ)V

    .line 2800
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2802
    iget v0, p1, Lcom/android/calendar/aq;->b:I

    if-ne v0, v10, :cond_7

    .line 2803
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2804
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2805
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2806
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_1

    .line 2807
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2819
    :cond_1
    :goto_1
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->D:Z

    if-eqz v0, :cond_3

    :cond_2
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    if-eqz v0, :cond_5

    .line 2820
    :cond_3
    iget v0, p1, Lcom/android/calendar/aq;->b:I

    if-ne v0, v10, :cond_c

    .line 2821
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    if-eqz v0, :cond_9

    .line 2822
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v0, v9, :cond_5

    .line 2823
    invoke-static {p0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    .line 2824
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-nez v1, :cond_4

    if-ne v0, v9, :cond_8

    .line 2825
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2826
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2893
    :cond_5
    :goto_2
    return-void

    .line 2791
    :cond_6
    iget v0, p1, Lcom/android/calendar/aq;->b:I

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->h()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/android/calendar/aq;->b:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 2794
    iput-boolean v6, p0, Lcom/android/calendar/AllInOneActivity;->n:Z

    goto :goto_0

    .line 2810
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2811
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2812
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2813
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_1

    .line 2814
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 2828
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2829
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c026c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 2830
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 2833
    :cond_9
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v0, v9, :cond_a

    .line 2834
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2835
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2836
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2837
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_5

    .line 2838
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2841
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2842
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_b

    .line 2843
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2845
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2846
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2849
    :cond_c
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    if-eqz v0, :cond_d

    .line 2850
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v0, v9, :cond_5

    .line 2851
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2852
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 2855
    :cond_d
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_e

    .line 2856
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2858
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2859
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2860
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 2864
    :cond_f
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_10

    .line 2865
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->E:Z

    invoke-static {p0, p1, v0}, Lcom/android/calendar/hj;->a(Landroid/app/Activity;Lcom/android/calendar/aq;Z)V

    .line 2866
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->F:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->E:Z

    if-eqz v0, :cond_5

    .line 2867
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->finish()V

    goto/16 :goto_2

    .line 2869
    :cond_10
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/32 v2, 0x10000

    cmp-long v0, v0, v2

    if-nez v0, :cond_13

    .line 2870
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v0, v9, :cond_5

    iget v0, p1, Lcom/android/calendar/aq;->b:I

    if-ne v0, v10, :cond_5

    .line 2872
    invoke-static {p0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    .line 2873
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-nez v1, :cond_11

    if-ne v0, v9, :cond_12

    .line 2874
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2875
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 2877
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2878
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c026c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 2879
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 2882
    :cond_13
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide v2, 0x200000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 2883
    iget-wide v0, p1, Lcom/android/calendar/aq;->p:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_14

    .line 2884
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_5

    .line 2885
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto/16 :goto_2

    .line 2887
    :cond_14
    iget-wide v0, p1, Lcom/android/calendar/aq;->p:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-nez v0, :cond_5

    .line 2888
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_5

    .line 2889
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v6}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto/16 :goto_2
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3325
    iput-object p1, p0, Lcom/android/calendar/AllInOneActivity;->aa:Ljava/lang/String;

    .line 3326
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 3161
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_1

    .line 3162
    const v0, 0x7f1202fd

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 3163
    if-eqz v0, :cond_0

    .line 3164
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 3172
    :cond_0
    :goto_0
    return-void

    .line 3167
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->X:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 3170
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->X:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 3175
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3176
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aq:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3177
    :cond_0
    return-void

    .line 3176
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 3150
    invoke-super {p0}, Lcom/android/calendar/a;->c()V

    .line 3151
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->W()V

    .line 3152
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 3180
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 3181
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3182
    :cond_0
    return-void

    .line 3181
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public d(I)V
    .locals 3

    .prologue
    .line 1525
    const v0, 0x7f12006c

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1526
    if-nez v0, :cond_0

    .line 1539
    :goto_0
    return-void

    .line 1530
    :cond_0
    sget-boolean v1, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v1, :cond_1

    .line 1531
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1533
    :cond_1
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1534
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1536
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 3185
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Y:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 3186
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->Y:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3187
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 888
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->av:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 909
    :cond_0
    :goto_0
    return v0

    .line 892
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 896
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-nez v1, :cond_0

    .line 900
    invoke-static {p0}, Lcom/android/calendar/hj;->z(Landroid/content/Context;)Z

    move-result v1

    .line 901
    if-nez v1, :cond_0

    .line 905
    invoke-static {p0}, Lcom/android/calendar/hj;->b(Landroid/app/Activity;)I

    move-result v1

    .line 906
    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    if-ne v2, v1, :cond_0

    .line 909
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 3126
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 3127
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 3128
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 3129
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 3130
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3131
    :cond_0
    const/4 v0, 0x0

    .line 3135
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/android/calendar/a;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3057
    if-nez p1, :cond_0

    .line 3098
    :goto_0
    return v0

    .line 3061
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 3063
    sparse-switch v2, :sswitch_data_0

    .line 3091
    :goto_1
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    sget-boolean v0, Lcom/android/calendar/month/k;->r:Z

    if-nez v0, :cond_1

    .line 3092
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0xa0

    if-ne v0, v2, :cond_2

    .line 3098
    :cond_1
    invoke-super {p0, p1}, Lcom/android/calendar/a;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 3065
    :sswitch_0
    sput-boolean v0, Lcom/android/calendar/month/bo;->W:Z

    .line 3066
    sput-boolean v0, Lcom/android/calendar/month/h;->w:Z

    .line 3068
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/k;->s:I

    .line 3069
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/k;->t:I

    goto :goto_1

    .line 3075
    :sswitch_1
    sput-boolean v1, Lcom/android/calendar/month/bo;->W:Z

    .line 3078
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/k;->u:I

    .line 3079
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/k;->v:I

    goto :goto_1

    .line 3083
    :sswitch_2
    sput-boolean v0, Lcom/android/calendar/month/bo;->W:Z

    goto :goto_1

    :cond_2
    move v0, v1

    .line 3095
    goto :goto_0

    .line 3063
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0xd3 -> :sswitch_2
    .end sparse-switch
.end method

.method public e()Landroid/widget/TextView;
    .locals 4

    .prologue
    .line 1386
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "action_bar_title"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1387
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public e(Z)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 3190
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerVisible(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3228
    :cond_0
    :goto_0
    return-void

    .line 3193
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 3194
    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 3197
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->g()I

    move-result v1

    const/4 v4, 0x4

    if-ne v1, v4, :cond_5

    instance-of v1, v0, Lcom/android/calendar/month/k;

    if-eqz v1, :cond_5

    .line 3198
    check-cast v0, Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->n()J

    move-result-wide v0

    .line 3201
    :goto_1
    if-eqz p1, :cond_3

    .line 3202
    new-instance v2, Landroid/text/format/Time;

    invoke-static {p0, v7}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    .line 3203
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 3204
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    invoke-virtual {v2, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 3206
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "req"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->year:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->month:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3207
    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->aa:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3208
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    .line 3209
    iput-object v2, p0, Lcom/android/calendar/AllInOneActivity;->aa:Ljava/lang/String;

    .line 3210
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    if-eqz v2, :cond_2

    .line 3211
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3213
    :cond_2
    new-instance v2, Lcom/android/calendar/ae;

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->aa:Ljava/lang/String;

    invoke-direct {v2, p0, v0, v1, v3}, Lcom/android/calendar/ae;-><init>(Lcom/android/calendar/AllInOneActivity;JLjava/lang/String;)V

    iput-object v2, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    .line 3214
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 3217
    :cond_3
    iget-object v4, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    if-eqz v4, :cond_4

    .line 3218
    iget-object v4, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3220
    :cond_4
    new-instance v4, Landroid/text/format/Time;

    invoke-static {p0, v7}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    .line 3221
    iget-object v4, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 3222
    iget-object v4, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    invoke-virtual {v4, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 3223
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "req"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->year:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/AllInOneActivity;->Z:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->month:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/calendar/AllInOneActivity;->aa:Ljava/lang/String;

    .line 3224
    iget-object v4, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3225
    new-instance v4, Lcom/android/calendar/ae;

    iget-object v5, p0, Lcom/android/calendar/AllInOneActivity;->aa:Ljava/lang/String;

    invoke-direct {v4, p0, v0, v1, v5}, Lcom/android/calendar/ae;-><init>(Lcom/android/calendar/AllInOneActivity;JLjava/lang/String;)V

    iput-object v4, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    .line 3226
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ab:Lcom/android/calendar/ae;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_5
    move-wide v0, v2

    goto/16 :goto_1
.end method

.method public e(I)Z
    .locals 15

    .prologue
    .line 2283
    const-wide/16 v9, 0x1

    .line 2284
    const/4 v11, 0x0

    .line 2288
    const-wide/16 v0, 0x2

    .line 2290
    sparse-switch p1, :sswitch_data_0

    .line 2395
    const/4 v0, 0x0

    .line 2399
    :goto_0
    return v0

    .line 2292
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x1000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2293
    const/4 v0, 0x1

    goto :goto_0

    .line 2296
    :sswitch_1
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->Q()V

    .line 2297
    const/4 v0, 0x1

    goto :goto_0

    .line 2300
    :sswitch_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/32 v2, 0x8000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v12

    move-object v1, p0

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2302
    const/4 v0, 0x1

    goto :goto_0

    .line 2305
    :sswitch_3
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->S()V

    .line 2306
    const/4 v0, 0x1

    goto :goto_0

    .line 2309
    :sswitch_4
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->k()V

    .line 2310
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->T()V

    .line 2311
    const/4 v0, 0x1

    goto :goto_0

    .line 2315
    :sswitch_5
    const/4 v9, 0x0

    .line 2316
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->g()I

    move-result v3

    if-ne v2, v3, :cond_0

    sget-boolean v2, Lcom/android/calendar/f/o;->n:Z

    if-nez v2, :cond_0

    .line 2317
    const/4 v0, 0x1

    goto :goto_0

    .line 2319
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v2

    .line 2320
    if-eqz v2, :cond_2

    .line 2321
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 2325
    :goto_1
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 2326
    if-eqz v2, :cond_1

    .line 2327
    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->B:Ljava/lang/String;

    iput-object v2, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 2328
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 2330
    :cond_1
    const-wide/16 v2, 0x8

    or-long v10, v0, v2

    .line 2331
    new-instance v0, Lcom/android/calendar/hi;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->o:Landroid/content/ContentResolver;

    invoke-direct {v0, v1, p0}, Lcom/android/calendar/hi;-><init>(Landroid/content/ContentResolver;Landroid/app/Activity;)V

    .line 2332
    invoke-virtual {v0}, Lcom/android/calendar/hi;->a()V

    .line 2398
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v5, 0x0

    const-wide/16 v7, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v1, p0

    move-object v6, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2399
    const/4 v0, 0x1

    goto :goto_0

    .line 2323
    :cond_2
    new-instance v4, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->B:Ljava/lang/String;

    invoke-direct {v4, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 2336
    :sswitch_6
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->t:Z

    if-eqz v0, :cond_3

    .line 2337
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2339
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->t:Z

    .line 2340
    invoke-static {p0}, Lcom/android/calendar/hj;->r(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v1

    .line 2342
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x1

    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, -0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    .line 2343
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2346
    :sswitch_7
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x2000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2347
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2350
    :sswitch_8
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x40

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2351
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2354
    :sswitch_9
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->t:Z

    if-eqz v0, :cond_4

    .line 2355
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2357
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->O()V

    .line 2358
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2361
    :sswitch_a
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide v2, 0x400000000L

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v14}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2363
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2366
    :sswitch_b
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/32 v2, 0x20000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v14}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2368
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2371
    :sswitch_c
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/32 v2, 0x40000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2372
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2375
    :sswitch_d
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/32 v2, 0x80000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2376
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2379
    :sswitch_e
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/32 v2, 0x100000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2380
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2383
    :sswitch_f
    sget-boolean v0, Lcom/android/calendar/month/k;->z:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/android/calendar/month/k;->A:Z

    if-nez v0, :cond_6

    .line 2384
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 2385
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    .line 2387
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->b(Z)V

    .line 2388
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->P()V

    .line 2389
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide v2, 0x80000000L

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2391
    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2290
    nop

    :sswitch_data_0
    .sparse-switch
        0x1335 -> :sswitch_5
        0x7f120324 -> :sswitch_2
        0x7f120327 -> :sswitch_5
        0x7f120328 -> :sswitch_f
        0x7f120329 -> :sswitch_0
        0x7f12032a -> :sswitch_1
        0x7f12032b -> :sswitch_a
        0x7f12032c -> :sswitch_b
        0x7f12032d -> :sswitch_e
        0x7f12032e -> :sswitch_d
        0x7f12032f -> :sswitch_3
        0x7f120330 -> :sswitch_9
        0x7f120331 -> :sswitch_c
        0x7f120332 -> :sswitch_7
        0x7f120333 -> :sswitch_4
        0x7f120334 -> :sswitch_8
        0x7f120336 -> :sswitch_6
    .end sparse-switch
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 3293
    iput-boolean p1, p0, Lcom/android/calendar/AllInOneActivity;->aG:Z

    .line 3294
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 1519
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    .line 1521
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()J
    .locals 4

    .prologue
    .line 2777
    const-wide v0, 0x200000022L

    .line 2778
    const-wide/16 v2, 0x400

    or-long/2addr v0, v2

    .line 2779
    sget-boolean v2, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v2, :cond_0

    .line 2780
    const-wide/32 v2, 0x10000

    or-long/2addr v0, v2

    .line 2782
    :cond_0
    return-wide v0
.end method

.method public h()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2904
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x80

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2905
    return-void
.end method

.method public handleSelectSyncedCalendarsClicked(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v4, 0x0

    .line 2898
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x40

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x2

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v12, v4

    move-object v13, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2900
    return-void
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 3032
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    .line 3033
    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->o(I)Z

    move-result v0

    return v0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 3274
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 3275
    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 3277
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/android/calendar/month/k;

    if-eqz v1, :cond_1

    .line 3278
    check-cast v0, Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->h()V

    .line 3279
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 3282
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->e(Z)V

    .line 3290
    :cond_0
    :goto_0
    return-void

    .line 3284
    :cond_1
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/calendar/e/d;

    if-eqz v1, :cond_0

    .line 3285
    check-cast v0, Lcom/android/calendar/e/d;

    invoke-virtual {v0}, Lcom/android/calendar/e/d;->a()Lcom/android/calendar/e/g;

    move-result-object v0

    .line 3286
    if-eqz v0, :cond_0

    .line 3287
    invoke-virtual {v0}, Lcom/android/calendar/e/g;->f()V

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 3297
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aG:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 3049
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/a;->onActivityResult(IILandroid/content/Intent;)V

    .line 3050
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 3053
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2035
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->n:Z

    if-eqz v0, :cond_1

    .line 2036
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    iget v8, p0, Lcom/android/calendar/AllInOneActivity;->p:I

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2040
    :goto_0
    return-void

    .line 2039
    :cond_1
    invoke-super {p0}, Lcom/android/calendar/a;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 877
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 878
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_0

    .line 879
    invoke-static {}, Lcom/android/calendar/AllInOneActivity;->u()V

    .line 881
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_1

    .line 882
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 884
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 885
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x4

    const/4 v7, 0x0

    .line 585
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onCreate(Landroid/os/Bundle;)V

    .line 587
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/os/Bundle;)V

    .line 588
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->o()V

    .line 591
    invoke-static {p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    .line 592
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-static {p0}, Lcom/android/calendar/hj;->d(Landroid/app/Activity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(Z)V

    .line 593
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-static {p0}, Lcom/android/calendar/hj;->e(Landroid/app/Activity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->b(Z)V

    .line 595
    const-string v0, "VerificationLog"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    invoke-static {}, Lcom/android/calendar/dz;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    invoke-static {p0}, Lcom/android/calendar/vcal/x;->b(Landroid/content/Context;)V

    .line 601
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    .line 602
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->h(Z)V

    .line 605
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 606
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/os/Bundle;Landroid/content/Intent;)J

    move-result-wide v4

    .line 607
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/AllInOneActivity;->b(Landroid/os/Bundle;Landroid/content/Intent;)I

    move-result v0

    .line 609
    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->au:I

    .line 611
    if-nez p1, :cond_1

    .line 612
    invoke-direct {p0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/content/Intent;)V

    .line 616
    :cond_1
    invoke-static {}, Lcom/android/calendar/hj;->t()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 617
    invoke-static {p0}, Lcom/android/calendar/hj;->D(Landroid/content/Context;)V

    .line 620
    :cond_2
    invoke-static {p0, v8}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calendar/AllInOneActivity;->B:Ljava/lang/String;

    .line 630
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 631
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    iput v6, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    .line 632
    iget v6, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    invoke-direct {p0, v3, v6}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/content/res/Resources;I)V

    .line 634
    sget-boolean v6, Lcom/android/calendar/AllInOneActivity;->i:Z

    invoke-static {v6}, Lcom/android/calendar/hj;->a(Z)V

    .line 637
    const v6, 0x7f040019

    invoke-virtual {p0, v6}, Lcom/android/calendar/AllInOneActivity;->setContentView(I)V

    .line 640
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 642
    if-eqz v1, :cond_3

    const-string v6, "com.sec.android.intent.calendar.help"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 643
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->r()V

    .line 644
    const/4 v0, 0x3

    .line 648
    :cond_3
    sget-boolean v1, Lcom/android/calendar/dz;->d:Z

    if-eqz v1, :cond_4

    .line 649
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/gz;->a(Landroid/app/FragmentManager;)Lcom/android/calendar/gz;

    .line 652
    :cond_4
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    .line 653
    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v1, :cond_a

    move v1, v2

    .line 659
    :goto_0
    invoke-direct {p0, v1}, Lcom/android/calendar/AllInOneActivity;->g(I)V

    .line 660
    invoke-direct {p0, v3}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/content/res/Resources;)V

    .line 662
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->k:Z

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v6, 0x2

    if-ne v0, v6, :cond_5

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-ne v0, v2, :cond_5

    .line 665
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 666
    const v2, 0x7f0c026c

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 672
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0, v7, p0}, Lcom/android/calendar/al;->b(ILcom/android/calendar/ap;)V

    .line 675
    invoke-static {p0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 676
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 678
    new-instance v0, Lcom/android/calendar/ad;

    invoke-direct {v0, p0, p0}, Lcom/android/calendar/ad;-><init>(Lcom/android/calendar/AllInOneActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aI:Lcom/android/calendar/ad;

    .line 679
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->o:Landroid/content/ContentResolver;

    .line 680
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->o:Landroid/content/ContentResolver;

    const-string v2, "easy_mode_splanner"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/AllInOneActivity;->aI:Lcom/android/calendar/ad;

    invoke-virtual {v0, v2, v7, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 683
    invoke-direct {p0, v1}, Lcom/android/calendar/AllInOneActivity;->l(I)V

    .line 684
    invoke-direct {p0, v4, v5, v1, p1}, Lcom/android/calendar/AllInOneActivity;->a(JILandroid/os/Bundle;)V

    .line 686
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->b(Landroid/os/Bundle;)Landroid/content/IntentFilter;

    move-result-object v0

    .line 687
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aK:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/AllInOneActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 689
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->k:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 690
    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->f(I)V

    .line 693
    :cond_6
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_7

    .line 694
    invoke-static {}, Lcom/android/calendar/AllInOneActivity;->u()V

    .line 697
    :cond_7
    invoke-static {p0}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 698
    invoke-static {p0}, Lcom/android/calendar/hj;->c(Landroid/app/Activity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v0

    .line 699
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 702
    :cond_8
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {p0}, Lcom/android/calendar/hj;->G(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 703
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/eb;->a(Landroid/content/ContentResolver;)V

    .line 704
    if-nez p1, :cond_9

    .line 705
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->p()V

    .line 708
    :cond_9
    return-void

    :cond_a
    move v1, v0

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2044
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 2045
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->ah:Z

    if-eqz v0, :cond_0

    .line 2061
    :goto_0
    return v3

    .line 2048
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2049
    const v0, 0x7f120327

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->X:Landroid/view/MenuItem;

    .line 2050
    const v0, 0x7f120336

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2051
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->K()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2052
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->X:Landroid/view/MenuItem;

    if-eqz v1, :cond_1

    .line 2053
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->X:Landroid/view/MenuItem;

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2055
    :cond_1
    if-eqz v0, :cond_2

    .line 2056
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2060
    :cond_2
    const v0, 0x7f120333

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->W:Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1915
    invoke-static {p0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1916
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1917
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->a()V

    .line 1918
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aI:Lcom/android/calendar/ad;

    if-eqz v0, :cond_0

    .line 1919
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->o:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aI:Lcom/android/calendar/ad;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1921
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/al;->b(Landroid/content/Context;)V

    .line 1923
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->aK:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1925
    invoke-static {p0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1926
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/calendar/dz;->b(Z)V

    .line 1928
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/f/o;->n:Z

    .line 1930
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1931
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1932
    sput-object v2, Lcom/android/calendar/AllInOneActivity;->e:Landroid/graphics/Bitmap;

    .line 1935
    :cond_2
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1936
    sget-object v0, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1937
    sput-object v2, Lcom/android/calendar/AllInOneActivity;->f:Landroid/graphics/Bitmap;

    .line 1940
    :cond_3
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1941
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/eb;->b()V

    .line 1944
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->X()V

    .line 1946
    invoke-super {p0}, Lcom/android/calendar/a;->onDestroy()V

    .line 1947
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3103
    invoke-static {p0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x52

    if-ne p1, v1, :cond_0

    .line 3111
    :goto_0
    return v0

    .line 3106
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayout;->isDrawerVisible(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3108
    invoke-static {p2}, Landroid/support/v4/view/KeyEventCompat;->startTracking(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 3111
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/calendar/a;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 3116
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerVisible(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3118
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->closeDrawers()V

    .line 3119
    const/4 v0, 0x1

    .line 3121
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/calendar/a;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onModeChanged(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1747
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-static {p0}, Lcom/android/calendar/hj;->d(Landroid/app/Activity;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/al;->a(Z)V

    .line 1748
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-static {p0}, Lcom/android/calendar/hj;->e(Landroid/app/Activity;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/al;->b(Z)V

    .line 1749
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 1750
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aB:Z

    if-nez v0, :cond_0

    .line 1751
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->L()V

    .line 1754
    :cond_0
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->i(Z)V

    .line 1755
    if-nez p1, :cond_3

    .line 1756
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->J()V

    .line 1757
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    .line 1758
    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_1

    .line 1759
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1772
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 1754
    goto :goto_0

    .line 1762
    :cond_3
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_4

    .line 1763
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1766
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/iv;

    .line 1768
    if-eqz v0, :cond_1

    .line 1769
    invoke-virtual {v0}, Lcom/android/calendar/event/iv;->a()V

    goto :goto_1
.end method

.method public onNavigationItemSelected(IJ)Z
    .locals 14

    .prologue
    .line 2960
    packed-switch p1, :pswitch_data_0

    .line 3017
    const-string v0, "AllInOneActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ItemSelected event from unknown button: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3020
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2962
    :pswitch_0
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 2963
    sget-object v0, Lcom/android/calendar/hj;->o:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 2964
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x6

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    .line 2970
    :pswitch_1
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    invoke-static {p0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2972
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/cd;->bN:Z

    .line 2975
    :cond_2
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    .line 2976
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x4

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2978
    :cond_3
    const-string v0, "preferences_month_views"

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2984
    :pswitch_2
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    invoke-static {p0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 2986
    :cond_4
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/cd;->bN:Z

    .line 2989
    :cond_5
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_6

    .line 2990
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x4

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 2992
    :cond_6
    const-string v0, "preferences_month_views"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2996
    :pswitch_3
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2997
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/e/ao;->c:Z

    .line 2998
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x2

    const-wide/16 v10, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_0

    .line 3003
    :pswitch_4
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 3004
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/e/ao;->c:Z

    .line 3005
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x3

    const-wide/16 v10, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_0

    .line 3011
    :pswitch_5
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 3012
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto/16 :goto_0

    .line 2960
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const/4 v9, 0x4

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v8, 0x0

    const-wide/16 v6, -0x1

    .line 392
    if-nez p1, :cond_1

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    .line 401
    const-string v0, "com.sec.android.intent.calendar.help"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 402
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->finish()V

    .line 403
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.calendar.help"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 405
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    :cond_2
    :goto_1
    sget-object v0, Lcom/android/calendar/hj;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/AllInOneActivity;->b(Ljava/lang/String;)V

    .line 425
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "KEY_HOME"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_6

    .line 426
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->b(Landroid/content/Intent;)J

    move-result-wide v0

    .line 427
    cmp-long v2, v0, v6

    if-nez v2, :cond_3

    .line 428
    invoke-static {p1}, Lcom/android/calendar/hj;->a(Landroid/content/Intent;)J

    move-result-wide v0

    .line 430
    :cond_3
    cmp-long v2, v0, v6

    if-eqz v2, :cond_6

    iget-wide v2, p0, Lcom/android/calendar/AllInOneActivity;->K:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    if-eqz v2, :cond_6

    .line 431
    new-instance v4, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->B:Ljava/lang/String;

    invoke-direct {v4, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 432
    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 433
    invoke-virtual {v4, v11}, Landroid/text/format/Time;->normalize(Z)J

    .line 434
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 435
    if-eqz v1, :cond_a

    .line 437
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_9

    move v0, v9

    .line 443
    :goto_2
    sget-boolean v2, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v2, :cond_4

    .line 444
    const-string v2, "handwriting_mode"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 445
    iput-boolean v11, p0, Lcom/android/calendar/AllInOneActivity;->aD:Z

    .line 449
    :cond_4
    const-string v2, "view_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 450
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v1, v12}, Lcom/android/calendar/al;->b(I)V

    .line 453
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    new-instance v2, Lcom/android/calendar/s;

    invoke-direct {v2, p0, v0, v4}, Lcom/android/calendar/s;-><init>(Lcom/android/calendar/AllInOneActivity;ILandroid/text/format/Time;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 489
    :cond_6
    :goto_3
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 491
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->L()V

    goto/16 :goto_0

    .line 406
    :catch_0
    move-exception v0

    .line 407
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 409
    :cond_7
    const-string v0, "com.sec.android.app.view.calendars"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 410
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    if-eqz v0, :cond_2

    .line 411
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x2000

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto/16 :goto_1

    .line 414
    :cond_8
    invoke-static {p0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 415
    invoke-static {v8}, Lcom/android/calendar/dz;->b(Z)V

    .line 416
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->finish()V

    .line 417
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/AllInOneActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 418
    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 440
    :cond_9
    invoke-static {v1}, Lcom/android/calendar/hj;->a(Landroid/os/Bundle;)I

    move-result v0

    goto :goto_2

    .line 478
    :cond_a
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aE:Z

    if-eqz v0, :cond_b

    .line 479
    iput-boolean v8, p0, Lcom/android/calendar/AllInOneActivity;->aE:Z

    .line 480
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    move-object v1, p0

    move-object v5, v4

    move v8, v9

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 481
    const-string v0, "preferences_month_views"

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 484
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 2274
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275
    const/4 v0, 0x1

    .line 2277
    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/AllInOneActivity;->e(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2203
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2204
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 2205
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1837
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->closeContextMenu()V

    .line 1838
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1839
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->closeOptionsMenu()V

    .line 1841
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(Ljava/lang/Integer;)V

    .line 1842
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->r:Z

    .line 1843
    iput-boolean v2, p0, Lcom/android/calendar/AllInOneActivity;->u:Z

    .line 1845
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->o:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aH:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1846
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1848
    invoke-static {p0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1849
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1852
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    invoke-static {p0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1853
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;I)V

    .line 1856
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1858
    invoke-super {p0}, Lcom/android/calendar/a;->onPause()V

    .line 1859
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1432
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onPostCreate(Landroid/os/Bundle;)V

    .line 1433
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    .line 1434
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ad:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v4/app/ActionBarDrawerToggle;->syncState()V

    .line 1435
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->A()V

    .line 1437
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 2073
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->ah:Z

    if-eqz v0, :cond_0

    .line 2074
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 2093
    :goto_0
    return v0

    .line 2081
    :cond_0
    const v0, 0x7f120334

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2082
    if-nez v0, :cond_1

    .line 2083
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 2085
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/view/Menu;)V

    .line 2086
    invoke-static {p1}, Lcom/android/calendar/AllInOneActivity;->b(Landroid/view/Menu;)V

    .line 2087
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->c(Landroid/view/Menu;)V

    .line 2088
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->d(Landroid/view/Menu;)V

    .line 2089
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->e(Landroid/view/Menu;)V

    .line 2090
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->h(Landroid/view/Menu;)V

    .line 2091
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->f(Landroid/view/Menu;)V

    .line 2092
    invoke-direct {p0, p1}, Lcom/android/calendar/AllInOneActivity;->g(Landroid/view/Menu;)V

    .line 2093
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1887
    const-string v0, "help"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1888
    invoke-static {v3}, Lcom/android/calendar/dz;->b(Z)V

    .line 1890
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1891
    const-string v0, "key_hide_menu"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1892
    const-string v0, "key_hide_menu"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->ah:Z

    .line 1895
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_4

    .line 1896
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    if-nez v0, :cond_2

    .line 1897
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->z()V

    .line 1900
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1901
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->y()V

    .line 1904
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    if-eqz v0, :cond_5

    .line 1905
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->ae:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 1906
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 1911
    :cond_4
    :goto_0
    return-void

    .line 1908
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ac:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v9, 0x0

    .line 1543
    invoke-super {p0}, Lcom/android/calendar/a;->onResume()V

    .line 1544
    invoke-direct {p0, v3}, Lcom/android/calendar/AllInOneActivity;->h(Z)V

    .line 1549
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0, v9, p0}, Lcom/android/calendar/al;->b(ILcom/android/calendar/ap;)V

    .line 1550
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-static {p0}, Lcom/android/calendar/hj;->d(Landroid/app/Activity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(Z)V

    .line 1551
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-static {p0}, Lcom/android/calendar/hj;->e(Landroid/app/Activity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->b(Z)V

    .line 1552
    const-string v0, "VerificationLog"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1554
    invoke-static {p0, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->B:Ljava/lang/String;

    .line 1557
    invoke-static {}, Lcom/android/calendar/hj;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1558
    invoke-static {p0}, Lcom/android/calendar/hj;->D(Landroid/content/Context;)V

    .line 1561
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1562
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->E()V

    .line 1565
    :cond_1
    iput-boolean v9, p0, Lcom/android/calendar/AllInOneActivity;->m:Z

    .line 1566
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->o:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->aH:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1567
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aJ:Z

    .line 1569
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->s:Z

    if-eqz v0, :cond_2

    .line 1570
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, v4}, Lcom/android/calendar/AllInOneActivity;->a(JILandroid/os/Bundle;)V

    .line 1571
    iput-boolean v9, p0, Lcom/android/calendar/AllInOneActivity;->s:Z

    .line 1574
    :cond_2
    iput-boolean v9, p0, Lcom/android/calendar/AllInOneActivity;->r:Z

    .line 1575
    iput-boolean v9, p0, Lcom/android/calendar/AllInOneActivity;->t:Z

    .line 1577
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->F()V

    .line 1578
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->H()V

    .line 1579
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->D()V

    .line 1581
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/android/calendar/dz;->D(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 1582
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1585
    :cond_4
    invoke-static {p0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1587
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->G()V

    .line 1590
    :cond_5
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aD:Z

    if-eqz v0, :cond_6

    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1591
    iput-boolean v9, p0, Lcom/android/calendar/AllInOneActivity;->aD:Z

    .line 1592
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->aP:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1595
    :cond_6
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->aA:Z

    if-eqz v0, :cond_7

    .line 1596
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x4

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 1597
    iput-boolean v9, p0, Lcom/android/calendar/AllInOneActivity;->aA:Z

    .line 1599
    :cond_7
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1869
    iput-boolean v4, p0, Lcom/android/calendar/AllInOneActivity;->m:Z

    .line 1870
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1872
    const-string v0, "key_restore_time"

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1873
    const-string v0, "key_restore_view"

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1874
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 1875
    const-string v0, "key_event_id"

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->f()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1878
    :cond_0
    const-string v0, "key_hide_menu"

    iget-boolean v1, p0, Lcom/android/calendar/AllInOneActivity;->ah:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1880
    invoke-static {p0}, Lcom/android/calendar/dz;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1881
    const-string v0, "help"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1883
    :cond_1
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 3025
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->t:Z

    if-nez v0, :cond_0

    .line 3026
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->O()V

    .line 3028
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2532
    const-string v0, "preferences_week_start_day"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "preferences_month_views"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "preferences_today_tz"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "preferences_home_tz_enabled"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "preferences_home_tz"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "preferences_show_week_num"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2538
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->r:Z

    if-eqz v0, :cond_5

    .line 2539
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/AllInOneActivity;->s:Z

    .line 2545
    :cond_1
    :goto_0
    const-string v0, "preferences_weather"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "preferences_weather_unit"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2547
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    if-eqz v0, :cond_3

    .line 2548
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    invoke-virtual {v0}, Lcom/android/calendar/db;->b()V

    .line 2550
    :cond_3
    invoke-static {p0}, Lcom/android/calendar/dz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2553
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->U()V

    .line 2554
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/calendar/h/c;->b(Landroid/content/Context;)V

    .line 2557
    :cond_4
    return-void

    .line 2541
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/calendar/AllInOneActivity;->a(JILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 1780
    sget-boolean v0, Lcom/android/calendar/dz;->b:Z

    if-eqz v0, :cond_1

    .line 1792
    :cond_0
    :goto_0
    return-void

    .line 1783
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 1784
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 1785
    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->aw:I

    if-ne v2, v0, :cond_2

    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->ax:I

    if-eq v2, v1, :cond_0

    .line 1786
    :cond_2
    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->aw:I

    if-eqz v2, :cond_3

    .line 1787
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->t()V

    .line 1789
    :cond_3
    iput v0, p0, Lcom/android/calendar/AllInOneActivity;->aw:I

    .line 1790
    iput v1, p0, Lcom/android/calendar/AllInOneActivity;->ax:I

    goto :goto_0
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0

    .prologue
    .line 2952
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 14

    .prologue
    .line 2909
    const/4 v9, -0x2

    .line 2911
    const-string v0, "AllInOneActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TabSelected AllInOne="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " finishing:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2912
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->U:Landroid/app/ActionBar$Tab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    .line 2913
    const/4 v9, 0x2

    .line 2924
    :cond_0
    :goto_0
    const/4 v0, -0x2

    if-eq v9, v0, :cond_1

    .line 2925
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const-wide/16 v10, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2928
    invoke-virtual {p0}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 2929
    invoke-direct {p0}, Lcom/android/calendar/AllInOneActivity;->L()V

    .line 2934
    :cond_1
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->l:Z

    if-eqz v0, :cond_3

    .line 2935
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->ar:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/q;

    invoke-direct {v1, p0}, Lcom/android/calendar/q;-><init>(Lcom/android/calendar/AllInOneActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2948
    :cond_3
    return-void

    .line 2914
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->T:Landroid/app/ActionBar$Tab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    .line 2915
    const/4 v9, 0x3

    goto :goto_0

    .line 2916
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->S:Landroid/app/ActionBar$Tab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_6

    .line 2917
    const/4 v9, 0x4

    goto :goto_0

    .line 2918
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->V:Landroid/app/ActionBar$Tab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    .line 2919
    const/4 v9, 0x1

    goto :goto_0

    .line 2920
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->R:Landroid/app/ActionBar$Tab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->q:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 2921
    const/4 v9, 0x6

    goto :goto_0
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0

    .prologue
    .line 2956
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 3140
    invoke-super {p0, p1}, Lcom/android/calendar/a;->onTrimMemory(I)V

    .line 3141
    const/16 v0, 0x28

    if-lt p1, v0, :cond_0

    .line 3142
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    if-eqz v0, :cond_0

    .line 3143
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->af:Lcom/android/calendar/db;

    invoke-virtual {v0}, Lcom/android/calendar/db;->c()V

    .line 3146
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1863
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->h:Lcom/android/calendar/al;

    const-wide/16 v2, 0x200

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 1864
    invoke-super {p0}, Lcom/android/calendar/a;->onUserLeaveHint()V

    .line 1865
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 0

    .prologue
    .line 1776
    return-void
.end method

.method public setControlsOffset(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2503
    iget v0, p0, Lcom/android/calendar/AllInOneActivity;->as:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2504
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2505
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 2506
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 2507
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->I:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->G:I

    sub-int/2addr v1, p1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2508
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->I:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2509
    sget-boolean v0, Lcom/android/calendar/AllInOneActivity;->j:Z

    if-eqz v0, :cond_0

    .line 2510
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->z:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 2528
    :cond_0
    :goto_0
    return-void

    .line 2514
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2515
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->w:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 2516
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->x:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 2517
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->A:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2518
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->A:Landroid/view/View;

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->H:I

    sub-int/2addr v1, p1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    mul-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 2520
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->J:Landroid/widget/LinearLayout$LayoutParams;

    if-nez v0, :cond_3

    .line 2521
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    iget v2, p0, Lcom/android/calendar/AllInOneActivity;->H:I

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/android/calendar/AllInOneActivity;->J:Landroid/widget/LinearLayout$LayoutParams;

    .line 2524
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->J:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/android/calendar/AllInOneActivity;->H:I

    sub-int/2addr v1, p1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2525
    iget-object v0, p0, Lcom/android/calendar/AllInOneActivity;->y:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/AllInOneActivity;->J:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
