.class Lcom/android/calendar/j;
.super Ljava/lang/Object;
.source "AllInOneActivity.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic a:Lcom/android/internal/widget/ScrollingTabContainerView;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Lcom/android/calendar/i;


# direct methods
.method constructor <init>(Lcom/android/calendar/i;Lcom/android/internal/widget/ScrollingTabContainerView;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1678
    iput-object p1, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iput-object p2, p0, Lcom/android/calendar/j;->a:Lcom/android/internal/widget/ScrollingTabContainerView;

    iput-object p3, p0, Lcom/android/calendar/j;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    .prologue
    .line 1682
    iget-object v0, p0, Lcom/android/calendar/j;->a:Lcom/android/internal/widget/ScrollingTabContainerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1683
    iget-object v1, p0, Lcom/android/calendar/j;->a:Lcom/android/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-static {}, Lcom/android/calendar/AllInOneActivity;->m()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1684
    iget-object v0, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iget-object v0, v0, Lcom/android/calendar/i;->a:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iget-object v1, v1, Lcom/android/calendar/i;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/al;->g()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;I)V

    .line 1685
    iget-object v0, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iget-object v0, v0, Lcom/android/calendar/i;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->f(Lcom/android/calendar/AllInOneActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/k;

    invoke-direct {v1, p0}, Lcom/android/calendar/k;-><init>(Lcom/android/calendar/j;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1703
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/j;->a:Lcom/android/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    .line 1704
    iget-object v0, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iget-object v0, v0, Lcom/android/calendar/i;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->f(Lcom/android/calendar/AllInOneActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/m;

    invoke-direct {v1, p0, p1, p0}, Lcom/android/calendar/m;-><init>(Lcom/android/calendar/j;Landroid/view/View;Landroid/view/View$OnLayoutChangeListener;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1711
    :cond_1
    return-void

    .line 1691
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/j;->a:Lcom/android/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-ne v1, v0, :cond_0

    invoke-static {}, Lcom/android/calendar/AllInOneActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692
    iget-object v0, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iget-object v0, v0, Lcom/android/calendar/i;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->g(Lcom/android/calendar/AllInOneActivity;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1693
    iget-object v0, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iget-object v0, v0, Lcom/android/calendar/i;->a:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iget-object v1, v1, Lcom/android/calendar/i;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/al;->g()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;I)V

    .line 1695
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/j;->c:Lcom/android/calendar/i;

    iget-object v0, v0, Lcom/android/calendar/i;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->f(Lcom/android/calendar/AllInOneActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/l;

    invoke-direct {v1, p0}, Lcom/android/calendar/l;-><init>(Lcom/android/calendar/j;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
