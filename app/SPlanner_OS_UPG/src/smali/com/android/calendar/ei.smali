.class Lcom/android/calendar/ei;
.super Ljava/lang/Object;
.source "GotoDialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field final synthetic d:Lcom/android/calendar/eg;


# direct methods
.method private constructor <init>(Lcom/android/calendar/eg;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/calendar/ei;->d:Lcom/android/calendar/eg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/eg;Lcom/android/calendar/eh;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/calendar/ei;-><init>(Lcom/android/calendar/eg;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 99
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/ei;->d:Lcom/android/calendar/eg;

    invoke-static {v0}, Lcom/android/calendar/eg;->a(Lcom/android/calendar/eg;)Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 102
    iget v0, p0, Lcom/android/calendar/ei;->a:I

    iput v0, v4, Landroid/text/format/Time;->year:I

    .line 103
    iget v0, p0, Lcom/android/calendar/ei;->b:I

    iput v0, v4, Landroid/text/format/Time;->month:I

    .line 104
    iget v0, p0, Lcom/android/calendar/ei;->c:I

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    .line 106
    iget v0, p0, Lcom/android/calendar/ei;->a:I

    const/16 v1, 0x7b2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/ei;->b:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/calendar/ei;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, v4, Landroid/text/format/Time;->allDay:Z

    .line 108
    iget v0, v4, Landroid/text/format/Time;->second:I

    add-int/lit16 v0, v0, 0x12c

    iput v0, v4, Landroid/text/format/Time;->second:I

    .line 110
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 111
    invoke-static {v4}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-nez v0, :cond_3

    .line 112
    iget-object v0, p0, Lcom/android/calendar/ei;->d:Lcom/android/calendar/eg;

    invoke-static {v0}, Lcom/android/calendar/eg;->b(Lcom/android/calendar/eg;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    .line 113
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 114
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/month/k;->b(Z)V

    .line 115
    iget-object v0, p0, Lcom/android/calendar/ei;->d:Lcom/android/calendar/eg;

    invoke-static {v0}, Lcom/android/calendar/eg;->a(Lcom/android/calendar/eg;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/android/calendar/ei;->d:Lcom/android/calendar/eg;

    invoke-static {v0}, Lcom/android/calendar/eg;->a(Lcom/android/calendar/eg;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->f(Z)V

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/ei;->d:Lcom/android/calendar/eg;

    invoke-static {v0}, Lcom/android/calendar/eg;->b(Lcom/android/calendar/eg;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x10

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 127
    :goto_0
    return-void

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/ei;->d:Lcom/android/calendar/eg;

    invoke-static {v0}, Lcom/android/calendar/eg;->b(Lcom/android/calendar/eg;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x20

    const/4 v5, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v1, p0

    move-object v6, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 125
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/ei;->d:Lcom/android/calendar/eg;

    invoke-static {v0}, Lcom/android/calendar/eg;->b(Lcom/android/calendar/eg;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    goto :goto_0
.end method
