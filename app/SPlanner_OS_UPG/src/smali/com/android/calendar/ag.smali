.class public Lcom/android/calendar/ag;
.super Landroid/os/Handler;
.source "AsyncQueryService.java"


# static fields
.field private static a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/android/calendar/ag;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 53
    iput-object p0, p0, Lcom/android/calendar/ag;->c:Landroid/os/Handler;

    .line 112
    iput-object p1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    .line 113
    return-void
.end method

.method public static final a()I
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/android/calendar/ag;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method public static final a(I)I
    .locals 1

    .prologue
    .line 142
    invoke-static {p0}, Lcom/android/calendar/AsyncQueryServiceHelper;->a(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 332
    return-void
.end method

.method protected a(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;J)V
    .locals 3

    .prologue
    .line 202
    new-instance v0, Lcom/android/calendar/ai;

    invoke-direct {v0}, Lcom/android/calendar/ai;-><init>()V

    .line 203
    const/4 v1, 0x2

    iput v1, v0, Lcom/android/calendar/ai;->b:I

    .line 204
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/ai;->c:Landroid/content/ContentResolver;

    .line 205
    iget-object v1, p0, Lcom/android/calendar/ag;->c:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/ai;->f:Landroid/os/Handler;

    .line 207
    iput p1, v0, Lcom/android/calendar/ai;->a:I

    .line 208
    iput-object p2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    .line 209
    iput-object p3, v0, Lcom/android/calendar/ai;->d:Landroid/net/Uri;

    .line 210
    iput-object p4, v0, Lcom/android/calendar/ai;->m:Landroid/content/ContentValues;

    .line 211
    iput-wide p5, v0, Lcom/android/calendar/ai;->o:J

    .line 213
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->a(Landroid/content/Context;Lcom/android/calendar/ai;)V

    .line 214
    return-void
.end method

.method public a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 238
    new-instance v0, Lcom/android/calendar/ai;

    invoke-direct {v0}, Lcom/android/calendar/ai;-><init>()V

    .line 239
    const/4 v1, 0x3

    iput v1, v0, Lcom/android/calendar/ai;->b:I

    .line 240
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/ai;->c:Landroid/content/ContentResolver;

    .line 241
    iget-object v1, p0, Lcom/android/calendar/ag;->c:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/ai;->f:Landroid/os/Handler;

    .line 243
    iput p1, v0, Lcom/android/calendar/ai;->a:I

    .line 244
    iput-object p2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    .line 245
    iput-object p3, v0, Lcom/android/calendar/ai;->d:Landroid/net/Uri;

    .line 246
    iput-object p4, v0, Lcom/android/calendar/ai;->m:Landroid/content/ContentValues;

    .line 247
    iput-object p5, v0, Lcom/android/calendar/ai;->h:Ljava/lang/String;

    .line 248
    iput-object p6, v0, Lcom/android/calendar/ai;->i:[Ljava/lang/String;

    .line 249
    iput-wide p7, v0, Lcom/android/calendar/ai;->o:J

    .line 251
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->a(Landroid/content/Context;Lcom/android/calendar/ai;)V

    .line 252
    return-void
.end method

.method public a(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 275
    new-instance v0, Lcom/android/calendar/ai;

    invoke-direct {v0}, Lcom/android/calendar/ai;-><init>()V

    .line 276
    const/4 v1, 0x4

    iput v1, v0, Lcom/android/calendar/ai;->b:I

    .line 277
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/ai;->c:Landroid/content/ContentResolver;

    .line 278
    iget-object v1, p0, Lcom/android/calendar/ag;->c:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/ai;->f:Landroid/os/Handler;

    .line 280
    iput p1, v0, Lcom/android/calendar/ai;->a:I

    .line 281
    iput-object p2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    .line 282
    iput-object p3, v0, Lcom/android/calendar/ai;->d:Landroid/net/Uri;

    .line 283
    iput-object p4, v0, Lcom/android/calendar/ai;->h:Ljava/lang/String;

    .line 284
    iput-object p5, v0, Lcom/android/calendar/ai;->i:[Ljava/lang/String;

    .line 285
    iput-wide p6, v0, Lcom/android/calendar/ai;->o:J

    .line 287
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->a(Landroid/content/Context;Lcom/android/calendar/ai;)V

    .line 288
    return-void
.end method

.method public a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 170
    new-instance v0, Lcom/android/calendar/ai;

    invoke-direct {v0}, Lcom/android/calendar/ai;-><init>()V

    .line 171
    const/4 v1, 0x1

    iput v1, v0, Lcom/android/calendar/ai;->b:I

    .line 172
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/ai;->c:Landroid/content/ContentResolver;

    .line 174
    iget-object v1, p0, Lcom/android/calendar/ag;->c:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/ai;->f:Landroid/os/Handler;

    .line 175
    iput p1, v0, Lcom/android/calendar/ai;->a:I

    .line 176
    iput-object p2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    .line 177
    iput-object p3, v0, Lcom/android/calendar/ai;->d:Landroid/net/Uri;

    .line 178
    iput-object p4, v0, Lcom/android/calendar/ai;->g:[Ljava/lang/String;

    .line 179
    iput-object p5, v0, Lcom/android/calendar/ai;->h:Ljava/lang/String;

    .line 180
    iput-object p6, v0, Lcom/android/calendar/ai;->i:[Ljava/lang/String;

    .line 181
    iput-object p7, v0, Lcom/android/calendar/ai;->j:Ljava/lang/String;

    .line 183
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->a(Landroid/content/Context;Lcom/android/calendar/ai;)V

    .line 184
    return-void
.end method

.method public a(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V
    .locals 3

    .prologue
    .line 306
    new-instance v0, Lcom/android/calendar/ai;

    invoke-direct {v0}, Lcom/android/calendar/ai;-><init>()V

    .line 307
    const/4 v1, 0x5

    iput v1, v0, Lcom/android/calendar/ai;->b:I

    .line 308
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/ai;->c:Landroid/content/ContentResolver;

    .line 309
    iget-object v1, p0, Lcom/android/calendar/ag;->c:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/ai;->f:Landroid/os/Handler;

    .line 311
    iput p1, v0, Lcom/android/calendar/ai;->a:I

    .line 312
    iput-object p2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    .line 313
    iput-object p3, v0, Lcom/android/calendar/ai;->e:Ljava/lang/String;

    .line 314
    iput-object p4, v0, Lcom/android/calendar/ai;->n:Ljava/util/ArrayList;

    .line 315
    iput-wide p5, v0, Lcom/android/calendar/ai;->o:J

    .line 317
    iget-object v1, p0, Lcom/android/calendar/ag;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->a(Landroid/content/Context;Lcom/android/calendar/ai;)V

    .line 318
    return-void
.end method

.method protected a(ILjava/lang/Object;[Landroid/content/ContentProviderResult;)V
    .locals 0

    .prologue
    .line 394
    return-void
.end method

.method protected b(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 398
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/ai;

    .line 400
    iget v1, p1, Landroid/os/Message;->what:I

    .line 401
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 409
    packed-switch v2, :pswitch_data_0

    .line 431
    const-string v0, "AsyncQuery"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown operation : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    :goto_0
    return-void

    .line 411
    :pswitch_0
    iget-object v2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    iget-object v0, v0, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {p0, v1, v2, v0}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/database/Cursor;)V

    goto :goto_0

    .line 415
    :pswitch_1
    iget-object v2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    iget-object v0, v0, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p0, v1, v2, v0}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;)V

    goto :goto_0

    .line 419
    :pswitch_2
    iget-object v2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    iget-object v0, v0, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;I)V

    goto :goto_0

    .line 423
    :pswitch_3
    iget-object v2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    iget-object v0, v0, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/android/calendar/ag;->b(ILjava/lang/Object;I)V

    goto :goto_0

    .line 427
    :pswitch_4
    iget-object v2, v0, Lcom/android/calendar/ai;->l:Ljava/lang/Object;

    iget-object v0, v0, Lcom/android/calendar/ai;->k:Ljava/lang/Object;

    check-cast v0, [Landroid/content/ContentProviderResult;

    check-cast v0, [Landroid/content/ContentProviderResult;

    invoke-virtual {p0, v1, v2, v0}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;[Landroid/content/ContentProviderResult;)V

    goto :goto_0

    .line 409
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
