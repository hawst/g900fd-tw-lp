.class public Lcom/android/calendar/gr;
.super Ljava/lang/Object;
.source "ShareEventHelper.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static f:J


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Ljava/lang/Runnable;

.field private d:Z

.field private e:Lcom/android/calendar/gv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    const-class v0, Lcom/android/calendar/gr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/gr;->a:Ljava/lang/String;

    .line 62
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/android/calendar/gr;->f:J

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    if-nez p1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    iput-object p1, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    .line 71
    iput-boolean p2, p0, Lcom/android/calendar/gr;->d:Z

    .line 72
    return-void
.end method

.method private a(Landroid/database/Cursor;JJ)J
    .locals 2

    .prologue
    .line 575
    const-string v0, "duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 576
    if-eqz v0, :cond_0

    .line 577
    new-instance v1, Lcom/android/calendar/vcal/a/a/e;

    invoke-direct {v1}, Lcom/android/calendar/vcal/a/a/e;-><init>()V

    .line 579
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/android/calendar/vcal/a/a/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_0 .. :try_end_0} :catch_0

    .line 583
    :goto_0
    invoke-virtual {v1, p2, p3}, Lcom/android/calendar/vcal/a/a/e;->a(J)J

    move-result-wide p2

    .line 587
    :cond_0
    return-wide p2

    .line 580
    :catch_0
    move-exception v0

    .line 581
    invoke-virtual {v0}, Lcom/android/calendar/vcal/a/a/d;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/gr;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/calendar/gr;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(Landroid/net/Uri;Z)Ljava/lang/String;
    .locals 20

    .prologue
    .line 384
    const/16 v2, 0x9

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "title"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "dtstart"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "dtend"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string v3, "allDay"

    aput-object v3, v4, v2

    const/4 v2, 0x4

    const-string v3, "eventLocation"

    aput-object v3, v4, v2

    const/4 v2, 0x5

    const-string v3, "facebook_schedule_id"

    aput-object v3, v4, v2

    const/4 v2, 0x6

    const-string v3, "duration"

    aput-object v3, v4, v2

    const/4 v2, 0x7

    const-string v3, "eventTimezone"

    aput-object v3, v4, v2

    const/16 v2, 0x8

    const-string v3, "description"

    aput-object v3, v4, v2

    .line 396
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 397
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 399
    if-nez v3, :cond_1

    .line 400
    const/4 v2, 0x0

    .line 495
    if-eqz v3, :cond_0

    .line 496
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 499
    :cond_0
    :goto_0
    return-object v2

    .line 402
    :cond_1
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 403
    const-string v2, "title"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 404
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_6

    .line 405
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    :cond_2
    :goto_1
    const-string v2, "dtstart"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 411
    const-string v2, "dtend"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 412
    const-string v2, "allDay"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    move v8, v2

    .line 413
    :goto_2
    const-string v2, "facebook_schedule_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    .line 414
    :goto_3
    const-string v9, "eventLocation"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 415
    const-string v9, "description"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 420
    if-eqz v8, :cond_a

    .line 421
    sget-wide v6, Lcom/android/calendar/gr;->f:J

    .line 422
    const v12, 0x1a016

    move-wide v4, v6

    .line 429
    :cond_3
    :goto_4
    if-nez p2, :cond_b

    .line 431
    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-nez v2, :cond_11

    move-object/from16 v2, p0

    .line 432
    invoke-direct/range {v2 .. v7}, Lcom/android/calendar/gr;->a(Landroid/database/Cursor;JJ)J

    move-result-wide v10

    .line 434
    :goto_5
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    move-wide v8, v4

    invoke-static/range {v7 .. v12}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v2

    .line 435
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    if-eqz v14, :cond_4

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 437
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 495
    :cond_4
    :goto_6
    if-eqz v3, :cond_5

    .line 496
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 499
    :cond_5
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 406
    :cond_6
    if-eqz p2, :cond_2

    .line 407
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    const v5, 0x7f0f02be

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 495
    :catchall_0
    move-exception v2

    if-eqz v3, :cond_7

    .line 496
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2

    .line 412
    :cond_8
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_2

    .line 413
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 424
    :cond_a
    const v12, 0x18b17

    .line 425
    if-eqz v2, :cond_3

    .line 426
    const v12, 0x1ab17

    goto/16 :goto_4

    .line 441
    :cond_b
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    const/4 v10, 0x0

    invoke-static {v2, v10}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v10

    .line 443
    if-eqz v8, :cond_e

    .line 444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-static {v2, v4, v5, v12}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 445
    const-wide/16 v10, 0x0

    cmp-long v8, v4, v10

    if-lez v8, :cond_d

    const-wide/16 v10, 0x0

    cmp-long v8, v6, v10

    if-lez v8, :cond_d

    sub-long v4, v6, v4

    const-wide/32 v10, 0x5265c00

    cmp-long v4, v4, v10

    if-lez v4, :cond_d

    .line 446
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    const-wide/16 v10, 0x3e8

    sub-long/2addr v6, v10

    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    iget-object v5, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v4, v6, v7, v5}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 447
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v5, v6, v7, v12}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v4

    .line 448
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " - "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    :goto_7
    if-eqz v14, :cond_c

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_c

    .line 487
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    :cond_c
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 490
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 450
    :cond_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 453
    :cond_e
    const/16 v8, 0x281

    .line 455
    const-wide/16 v16, 0x0

    cmp-long v2, v6, v16

    if-nez v2, :cond_f

    sget-wide v16, Lcom/android/calendar/gr;->f:J

    const-wide/16 v18, -0x1

    cmp-long v2, v16, v18

    if-eqz v2, :cond_f

    .line 456
    sget-wide v4, Lcom/android/calendar/gr;->f:J

    move-object/from16 v2, p0

    .line 457
    invoke-direct/range {v2 .. v7}, Lcom/android/calendar/gr;->a(Landroid/database/Cursor;JJ)J

    move-result-wide v6

    .line 460
    :cond_f
    const-class v11, Ljava/util/TimeZone;

    monitor-enter v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 461
    :try_start_3
    invoke-static {v10}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    .line 462
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-static {v2, v4, v5, v8}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 463
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-static {v12, v6, v7, v8}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v8

    .line 464
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    .line 465
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 466
    :try_start_4
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, " - "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "\n"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 469
    const v2, 0x18016

    .line 473
    const-class v8, Ljava/util/TimeZone;

    monitor-enter v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 474
    :try_start_5
    invoke-static {v10}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v10

    invoke-static {v10}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    .line 475
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-static {v10, v4, v5, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v10

    .line 476
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    .line 477
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 478
    :try_start_6
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    const-wide/16 v10, 0x0

    cmp-long v8, v4, v10

    if-lez v8, :cond_10

    const-wide/16 v10, 0x0

    cmp-long v8, v6, v10

    if-lez v8, :cond_10

    sub-long v4, v6, v4

    const-wide/32 v10, 0x5265c00

    cmp-long v4, v4, v10

    if-lez v4, :cond_10

    .line 481
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-static {v4, v6, v7, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 482
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    :cond_10
    const-string v2, "\n\n"

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_7

    .line 465
    :catchall_1
    move-exception v2

    :try_start_7
    monitor-exit v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 477
    :catchall_2
    move-exception v2

    :try_start_9
    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_11
    move-wide v10, v6

    goto/16 :goto_5
.end method

.method static synthetic a(Lcom/android/calendar/gr;Landroid/net/Uri;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/gr;->b(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/android/calendar/gr;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/net/Uri;Z)Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    .line 503
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 504
    iget-object v0, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 506
    if-nez v1, :cond_1

    .line 567
    if-eqz v1, :cond_0

    .line 568
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 571
    :cond_0
    :goto_0
    return-object v2

    .line 509
    :cond_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 510
    const-string v0, "subject"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 511
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_7

    .line 513
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    :cond_2
    :goto_1
    const-string v0, "importance"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 520
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 533
    :goto_2
    const-string v0, "utc_due_date"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 534
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 537
    if-eqz p2, :cond_a

    .line 538
    const v0, 0x18016

    .line 541
    iget-object v3, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-static {v3, v4, v5, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 543
    cmp-long v3, v4, v8

    if-nez v3, :cond_9

    .line 544
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    const v4, 0x7f0f02d1

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    :cond_3
    :goto_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    :cond_4
    :goto_4
    const-string v0, "body"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 562
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 563
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 564
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 567
    :cond_5
    if-eqz v1, :cond_6

    .line 568
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 571
    :cond_6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 514
    :cond_7
    if-eqz p2, :cond_2

    .line 515
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    const v4, 0x7f0f02bf

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 567
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_8

    .line 568
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 522
    :pswitch_0
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0347

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 525
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0348

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 528
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0346

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 545
    :cond_9
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 546
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 551
    :cond_a
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_b

    .line 552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    :cond_b
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 555
    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 556
    iget-object v0, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    const/4 v2, 0x2

    invoke-static {v4, v5, v0, v2}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 558
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    cmp-long v2, v4, v8

    if-eqz v2, :cond_4

    .line 559
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_4

    .line 520
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lcom/android/calendar/gr;Landroid/net/Uri;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/gr;->a(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/gr;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/android/calendar/gr;->d:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/calendar/gr;->e:Lcom/android/calendar/gv;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/android/calendar/gr;->e:Lcom/android/calendar/gv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/gv;->cancel(Z)Z

    .line 196
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/calendar/gr;->a(JLjava/lang/Runnable;)V

    .line 82
    return-void
.end method

.method public a(JLjava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0, p3}, Lcom/android/calendar/gr;->a([Ljava/lang/Long;Ljava/lang/Runnable;)V

    .line 93
    return-void
.end method

.method public a([Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/gr;->b([Ljava/lang/Long;Ljava/lang/Runnable;)V

    .line 158
    return-void
.end method

.method public a([Ljava/lang/Long;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 117
    array-length v0, p1

    if-ne v0, v1, :cond_0

    .line 118
    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/gr;->c([Ljava/lang/Long;Ljava/lang/Runnable;)Landroid/app/Dialog;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 120
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 125
    :goto_0
    return-void

    .line 122
    :cond_0
    sget-object v0, Lcom/android/calendar/gu;->a:Lcom/android/calendar/gu;

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/calendar/gr;->a([Ljava/lang/Long;Ljava/lang/Runnable;Lcom/android/calendar/gu;)V

    goto :goto_0
.end method

.method public a([Ljava/lang/Long;Ljava/lang/Runnable;Lcom/android/calendar/gu;)V
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/android/calendar/gr;->a()V

    .line 184
    iput-object p2, p0, Lcom/android/calendar/gr;->c:Ljava/lang/Runnable;

    .line 185
    new-instance v0, Lcom/android/calendar/gv;

    iget-object v1, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-direct {v0, p0, v1, p3}, Lcom/android/calendar/gv;-><init>(Lcom/android/calendar/gr;Landroid/app/Activity;Lcom/android/calendar/gu;)V

    iput-object v0, p0, Lcom/android/calendar/gr;->e:Lcom/android/calendar/gv;

    .line 186
    iget-object v0, p0, Lcom/android/calendar/gr;->e:Lcom/android/calendar/gv;

    invoke-virtual {v0, p1}, Lcom/android/calendar/gv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 187
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/calendar/gr;->b(JLjava/lang/Runnable;)V

    .line 135
    return-void
.end method

.method public b(JLjava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 145
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0, p3}, Lcom/android/calendar/gr;->b([Ljava/lang/Long;Ljava/lang/Runnable;)V

    .line 146
    return-void
.end method

.method public b([Ljava/lang/Long;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 171
    if-nez v0, :cond_0

    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received intent is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 176
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MimeType of the received intent is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_1
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/calendar/gu;->e:Lcom/android/calendar/gu;

    .line 179
    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/calendar/gr;->a([Ljava/lang/Long;Ljava/lang/Runnable;Lcom/android/calendar/gu;)V

    .line 180
    return-void

    .line 178
    :cond_2
    sget-object v0, Lcom/android/calendar/gu;->b:Lcom/android/calendar/gu;

    goto :goto_0
.end method

.method public c([Ljava/lang/Long;Ljava/lang/Runnable;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const v8, 0x7f0f03da

    const/4 v1, 0x0

    .line 591
    iget-object v0, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09000e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 594
    aget-object v0, p1, v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    const/4 v0, 0x1

    .line 595
    :goto_0
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 596
    iget-object v4, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    const v5, 0x7f0f03bc

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 598
    if-eqz v0, :cond_3

    .line 599
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 600
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u200f"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v4, v8}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 610
    :cond_0
    :goto_1
    new-instance v0, Lcom/android/calendar/gs;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/calendar/gs;-><init>(Lcom/android/calendar/gr;[Ljava/lang/Long;Ljava/lang/Runnable;)V

    invoke-virtual {v3, v2, v0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 627
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    .line 594
    goto :goto_0

    .line 602
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    invoke-virtual {v0, v8}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v2, v1

    goto :goto_1

    .line 605
    :cond_3
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u200f"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/calendar/gr;->b:Landroid/app/Activity;

    const v5, 0x7f0f03d9

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    goto :goto_1
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 190
    sput-wide p1, Lcom/android/calendar/gr;->f:J

    .line 191
    return-void
.end method
