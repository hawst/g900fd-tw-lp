.class public Lcom/android/calendar/globalSearch/e;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "StrokeDbHelper.java"


# static fields
.field private static e:Lcom/android/calendar/globalSearch/e;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 28
    const-class v0, Lcom/android/calendar/globalSearch/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/globalSearch/e;->d:Ljava/lang/String;

    .line 32
    const-string v0, "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"

    iput-object v0, p0, Lcom/android/calendar/globalSearch/e;->a:Ljava/lang/String;

    .line 34
    const-string v0, "INTEGER"

    iput-object v0, p0, Lcom/android/calendar/globalSearch/e;->b:Ljava/lang/String;

    .line 36
    const-string v0, "VARCHAR(50)"

    iput-object v0, p0, Lcom/android/calendar/globalSearch/e;->c:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/android/calendar/globalSearch/e;
    .locals 5

    .prologue
    .line 76
    const-class v1, Lcom/android/calendar/globalSearch/e;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/calendar/globalSearch/e;->e:Lcom/android/calendar/globalSearch/e;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/android/calendar/globalSearch/e;

    const-string v2, "StrokeDb.db"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/android/calendar/globalSearch/e;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    sput-object v0, Lcom/android/calendar/globalSearch/e;->e:Lcom/android/calendar/globalSearch/e;

    .line 81
    :cond_0
    sget-object v0, Lcom/android/calendar/globalSearch/e;->e:Lcom/android/calendar/globalSearch/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 56
    :try_start_0
    const-string v0, "CREATE TABLE IF NOT EXISTS word (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,word VARCHAR(50),region VARCHAR(50),photo VARCHAR(50));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 66
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 67
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    iget-object v0, p0, Lcom/android/calendar/globalSearch/e;->d:Ljava/lang/String;

    const-string v1, "Error executing SQL(word)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 71
    const-string v0, "DROP TABLE IF EXISTS word"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    invoke-direct {p0, p1}, Lcom/android/calendar/globalSearch/e;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 73
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/calendar/globalSearch/e;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 45
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/calendar/globalSearch/e;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 50
    return-void
.end method
