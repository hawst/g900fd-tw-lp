.class public Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;
.super Landroid/content/ContentProvider;
.source "CalendarSuggestionsProvider.java"


# static fields
.field private static final a:Landroid/content/UriMatcher;


# instance fields
.field private b:Lcom/android/calendar/globalSearch/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 14
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    .line 25
    sget-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.calendar.CalendarSuggestionsProvider"

    const-string v2, "search_suggest_query"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 26
    sget-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.calendar.CalendarSuggestionsProvider"

    const-string v2, "search_suggest_query/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 27
    sget-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.calendar.CalendarSuggestionsProvider"

    const-string v2, "search_suggest_regex_query/*"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 28
    sget-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.calendar.CalendarSuggestionsProvider"

    const-string v2, "search_suggest_regex_query"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 29
    sget-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.calendar.CalendarSuggestionsProvider"

    const-string v2, "word"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->b:Lcom/android/calendar/globalSearch/e;

    return-void
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;)J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 91
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 92
    if-nez v2, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-wide v0

    .line 97
    :cond_1
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 98
    cmp-long v4, v2, v0

    if-ltz v4, :cond_0

    move-wide v0, v2

    .line 101
    goto :goto_0

    .line 102
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private declared-synchronized a()Lcom/android/calendar/globalSearch/e;
    .locals 1

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->b:Lcom/android/calendar/globalSearch/e;

    if-nez v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/globalSearch/e;->a(Landroid/content/Context;)Lcom/android/calendar/globalSearch/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->b:Lcom/android/calendar/globalSearch/e;

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->b:Lcom/android/calendar/globalSearch/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 124
    sget-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 127
    packed-switch v0, :pswitch_data_0

    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :pswitch_0
    const-string v0, "word"

    .line 136
    monitor-enter p0

    .line 137
    const/4 v1, 0x0

    .line 140
    :try_start_0
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a()Lcom/android/calendar/globalSearch/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/globalSearch/e;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 142
    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {v1, v0, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 149
    :cond_0
    if-eqz v1, :cond_1

    .line 150
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 154
    :cond_1
    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    const/4 v0, 0x0

    return v0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 149
    if-eqz v1, :cond_1

    .line 150
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 149
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_2

    .line 150
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 151
    :cond_2
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 113
    sget-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/stroke"

    return-object v0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    const/4 v0, 0x0

    .line 161
    sget-object v1, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 166
    packed-switch v1, :pswitch_data_0

    .line 172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :pswitch_0
    const-string v1, "word"

    .line 175
    monitor-enter p0

    .line 179
    :try_start_0
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a()Lcom/android/calendar/globalSearch/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/globalSearch/e;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 181
    if-eqz v1, :cond_5

    .line 182
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v2, v1, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-wide v4

    .line 185
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v1, v4, v8

    if-lez v1, :cond_4

    .line 186
    :try_start_2
    invoke-virtual {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 187
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v1

    .line 192
    :goto_1
    if-eqz v2, :cond_0

    .line 193
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 197
    :cond_0
    :goto_2
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 199
    cmp-long v2, v4, v6

    if-nez v2, :cond_2

    :goto_3
    return-object v0

    .line 189
    :catch_0
    move-exception v1

    move-object v2, v0

    move-wide v4, v6

    .line 190
    :goto_4
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 192
    if-eqz v2, :cond_3

    .line 193
    :try_start_5
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    move-object v1, v0

    .line 194
    goto :goto_2

    .line 192
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_5
    if-eqz v2, :cond_1

    .line 193
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 194
    :cond_1
    throw v0

    .line 197
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :cond_2
    move-object v0, v1

    .line 199
    goto :goto_3

    .line 192
    :catchall_2
    move-exception v0

    goto :goto_5

    .line 189
    :catch_1
    move-exception v1

    move-wide v4, v6

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_4

    :cond_3
    move-object v1, v0

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_1

    :cond_5
    move-wide v4, v6

    goto :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x3f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    sget-object v0, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 46
    if-eq v0, v5, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid URI parameter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 87
    :cond_1
    :goto_0
    :pswitch_0
    return-object v3

    .line 53
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-lez v1, :cond_2

    .line 56
    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 60
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 64
    new-instance v1, Lcom/android/calendar/globalSearch/a;

    invoke-virtual {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/android/calendar/globalSearch/a;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 66
    invoke-virtual {v1}, Lcom/android/calendar/globalSearch/a;->a()Landroid/database/MatrixCursor;

    move-result-object v3

    goto :goto_0

    .line 69
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/android/calendar/globalSearch/d;

    invoke-direct {v1}, Lcom/android/calendar/globalSearch/d;-><init>()V

    .line 71
    invoke-virtual {v1, v0}, Lcom/android/calendar/globalSearch/d;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 73
    new-instance v1, Lcom/android/calendar/globalSearch/a;

    invoke-virtual {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v0, "stime"

    invoke-static {p1, v0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v4

    const-string v0, "etime"

    invoke-static {p1, v0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v6

    const-string v0, "searchfilter"

    invoke-static {p1, v0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a()Lcom/android/calendar/globalSearch/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/globalSearch/e;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/android/calendar/globalSearch/a;-><init>(Landroid/content/Context;[Ljava/lang/String;JJLjava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 76
    invoke-virtual {v1}, Lcom/android/calendar/globalSearch/a;->a()Landroid/database/MatrixCursor;

    move-result-object v3

    goto :goto_0

    .line 79
    :pswitch_3
    new-instance v1, Lcom/android/calendar/globalSearch/a;

    invoke-virtual {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v0, "stime"

    invoke-static {p1, v0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v4

    const-string v0, "etime"

    invoke-static {p1, v0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v6

    const-string v0, "searchfilter"

    invoke-static {p1, v0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0}, Lcom/android/calendar/globalSearch/CalendarSuggestionsProvider;->a()Lcom/android/calendar/globalSearch/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/globalSearch/e;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/android/calendar/globalSearch/a;-><init>(Landroid/content/Context;[Ljava/lang/String;JJLjava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 82
    invoke-virtual {v1}, Lcom/android/calendar/globalSearch/a;->a()Landroid/database/MatrixCursor;

    move-result-object v3

    goto/16 :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    return v0
.end method
