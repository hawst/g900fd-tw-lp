.class Lcom/android/calendar/globalSearch/b;
.super Ljava/lang/Object;
.source "GlobalSearchSupport.java"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:J

.field public d:J

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:I

.field public i:I

.field public j:Z

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 766
    iput-wide v4, p0, Lcom/android/calendar/globalSearch/b;->a:J

    .line 767
    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->b:Ljava/lang/String;

    .line 768
    iput-wide v4, p0, Lcom/android/calendar/globalSearch/b;->c:J

    .line 769
    iput-wide v4, p0, Lcom/android/calendar/globalSearch/b;->d:J

    .line 770
    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->e:Ljava/lang/String;

    .line 771
    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->f:Ljava/lang/String;

    .line 772
    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->g:Ljava/lang/String;

    .line 773
    iput v1, p0, Lcom/android/calendar/globalSearch/b;->h:I

    .line 774
    iput v1, p0, Lcom/android/calendar/globalSearch/b;->i:I

    .line 775
    iput-boolean v1, p0, Lcom/android/calendar/globalSearch/b;->j:Z

    .line 776
    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->k:Ljava/lang/String;

    .line 779
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    invoke-static {}, Lcom/android/calendar/globalSearch/a;->b()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 780
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/globalSearch/b;->a:J

    .line 781
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->b:Ljava/lang/String;

    .line 782
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/globalSearch/b;->c:J

    .line 783
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/globalSearch/b;->d:J

    .line 784
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->e:Ljava/lang/String;

    .line 785
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->f:Ljava/lang/String;

    .line 786
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/globalSearch/b;->g:Ljava/lang/String;

    .line 787
    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/globalSearch/b;->h:I

    .line 788
    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/globalSearch/b;->i:I

    .line 789
    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/globalSearch/b;->j:Z

    .line 790
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/globalSearch/b;->k:Ljava/lang/String;

    .line 792
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 789
    goto :goto_0
.end method
