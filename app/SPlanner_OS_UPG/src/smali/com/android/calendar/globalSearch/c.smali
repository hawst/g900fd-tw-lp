.class Lcom/android/calendar/globalSearch/c;
.super Ljava/lang/Object;
.source "GlobalSearchSupport.java"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:J

.field public e:I

.field public f:J

.field public g:Z

.field public h:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796
    iput-wide v4, p0, Lcom/android/calendar/globalSearch/c;->a:J

    .line 797
    iput-object v2, p0, Lcom/android/calendar/globalSearch/c;->b:Ljava/lang/String;

    .line 798
    iput-object v2, p0, Lcom/android/calendar/globalSearch/c;->c:Ljava/lang/String;

    .line 799
    iput-wide v4, p0, Lcom/android/calendar/globalSearch/c;->d:J

    .line 800
    iput v1, p0, Lcom/android/calendar/globalSearch/c;->e:I

    .line 801
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/calendar/globalSearch/c;->f:J

    .line 802
    iput-boolean v1, p0, Lcom/android/calendar/globalSearch/c;->g:Z

    .line 803
    iput v0, p0, Lcom/android/calendar/globalSearch/c;->h:I

    .line 806
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    invoke-static {}, Lcom/android/calendar/globalSearch/a;->c()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 807
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/globalSearch/c;->a:J

    .line 808
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/globalSearch/c;->b:Ljava/lang/String;

    .line 809
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/globalSearch/c;->c:Ljava/lang/String;

    .line 810
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/globalSearch/c;->d:J

    .line 811
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/globalSearch/c;->e:I

    .line 812
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/globalSearch/c;->f:J

    .line 813
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/globalSearch/c;->g:Z

    .line 814
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/globalSearch/c;->h:I

    .line 816
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 813
    goto :goto_0
.end method
