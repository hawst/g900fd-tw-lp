.class public Lcom/android/calendar/globalSearch/a;
.super Ljava/lang/Object;
.source "GlobalSearchSupport.java"


# static fields
.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;

.field private static final j:[Ljava/lang/String;

.field private static final k:[Ljava/lang/String;

.field private static final l:[Ljava/lang/String;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/database/MatrixCursor;

.field private c:[Ljava/lang/String;

.field private d:J

.field private e:J

.field private f:Z

.field private g:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 66
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v4

    const-string v1, "suggest_text_2"

    aput-object v1, v0, v5

    const-string v1, "suggest_intent_data"

    aput-object v1, v0, v6

    const-string v1, "suggest_text_3"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "suggest_text_4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_target_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_extra_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "suggest_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "suggest_mime_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "suggest_ink_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    .line 81
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Events._id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "dtstart"

    aput-object v1, v0, v5

    const-string v1, "dtend"

    aput-object v1, v0, v6

    const-string v1, "description"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "rrule"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/globalSearch/a;->i:[Ljava/lang/String;

    .line 95
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "event_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "begin"

    aput-object v1, v0, v5

    const-string v1, "end"

    aput-object v1, v0, v6

    const-string v1, "description"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "rrule"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/globalSearch/a;->j:[Ljava/lang/String;

    .line 121
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "attendeeName"

    aput-object v1, v0, v3

    const-string v1, "attendeeEmail"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/globalSearch/a;->k:[Ljava/lang/String;

    .line 129
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "subject"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    const-string v1, "complete"

    aput-object v1, v0, v6

    const-string v1, "accountKey"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "reminder_set"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "importance"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/globalSearch/a;->l:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    .line 55
    iput-object v0, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    .line 56
    iput-object v0, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    .line 58
    iput-wide v2, p0, Lcom/android/calendar/globalSearch/a;->d:J

    .line 59
    iput-wide v2, p0, Lcom/android/calendar/globalSearch/a;->e:J

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/globalSearch/a;->f:Z

    .line 178
    iput-object p1, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    .line 179
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    .line 180
    iput-object p2, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    .line 181
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/a;->d()V

    .line 182
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;JJLjava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    .line 55
    iput-object v0, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    .line 56
    iput-object v0, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    .line 58
    iput-wide v2, p0, Lcom/android/calendar/globalSearch/a;->d:J

    .line 59
    iput-wide v2, p0, Lcom/android/calendar/globalSearch/a;->e:J

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/globalSearch/a;->f:Z

    .line 163
    iput-object p1, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    .line 164
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    .line 165
    iput-object p2, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    .line 166
    iput-wide p3, p0, Lcom/android/calendar/globalSearch/a;->d:J

    .line 167
    iput-wide p5, p0, Lcom/android/calendar/globalSearch/a;->e:J

    .line 169
    if-eqz p7, :cond_0

    const-string v0, "handwriting"

    invoke-virtual {v0, p7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/globalSearch/a;->f:Z

    .line 172
    :cond_0
    iput-object p8, p0, Lcom/android/calendar/globalSearch/a;->g:Landroid/database/sqlite/SQLiteDatabase;

    .line 174
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/a;->d()V

    .line 175
    return-void
.end method

.method private a(Ljava/lang/String;)J
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 279
    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 282
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {p0}, Lcom/android/calendar/globalSearch/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 283
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 288
    array-length v3, v1

    if-lez v3, :cond_1

    .line 289
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v1, v3

    .line 290
    const/16 v3, 0x2e

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 293
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v5, :cond_1

    .line 294
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 295
    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 299
    :goto_0
    iput v1, v2, Landroid/text/format/Time;->year:I

    .line 300
    iput v0, v2, Landroid/text/format/Time;->month:I

    .line 303
    invoke-direct {p0, v2}, Lcom/android/calendar/globalSearch/a;->a(Landroid/text/format/Time;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    const-wide/16 v0, -0x1

    .line 307
    :goto_1
    return-wide v0

    :cond_0
    invoke-virtual {v2, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private a(JLjava/lang/String;Z)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 576
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 580
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 581
    iget-object v2, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    const-class v3, Lcom/android/calendar/vcal/VCalUpdateService;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 582
    const-string v2, "uri"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 583
    const-string v2, "isTask"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 584
    iget-object v2, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 586
    if-eqz p4, :cond_0

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Task"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "vts"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 592
    :goto_0
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 589
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invite"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "vcs"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 259
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/hj;->e()[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 260
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 261
    const v2, 0x7f0f0488

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 262
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    const-string v3, "YMD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 265
    invoke-static {}, Lcom/android/calendar/hj;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    :cond_0
    const-string v0, "yyyy"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MMM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 268
    :cond_1
    const-string v0, "yyyy MMM"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 271
    :cond_2
    const-string v0, "MMM yyyy"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 241
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 242
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 243
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 245
    const/4 v0, 0x0

    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_2

    .line 246
    aget-object v4, v2, v0

    invoke-direct {p0, v4}, Lcom/android/calendar/globalSearch/a;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 247
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 248
    const/16 v4, 0x7c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 250
    :cond_0
    aget-object v4, v3, v0

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(JLjava/lang/String;Ljava/lang/String;JJIJILandroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 11

    .prologue
    .line 652
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 655
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 657
    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 658
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/a;->h()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 659
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 661
    move-wide/from16 v0, p7

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 662
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/a;->h()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 663
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 665
    const/4 v2, 0x0

    :goto_0
    sget-object v8, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    array-length v8, v8

    if-ge v2, v8, :cond_d

    .line 666
    const-string v8, "_id"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 667
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 668
    :cond_1
    const-string v8, "suggest_text_1"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 669
    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 670
    :cond_2
    const-string v8, "suggest_text_2"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 671
    invoke-virtual {v3, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 672
    :cond_3
    const-string v8, "suggest_text_3"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 673
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 674
    :cond_4
    const-string v8, "suggest_text_4"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 675
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 676
    :cond_5
    const-string v8, "suggest_intent_data"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 677
    move-object/from16 v0, p13

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 678
    :cond_6
    const-string v8, "suggest_target_type"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 679
    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 680
    :cond_7
    const-string v8, "suggest_extra_flags"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 681
    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 682
    :cond_8
    const-string v8, "suggest_icon_1"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 683
    if-eqz p14, :cond_9

    .line 684
    move-object/from16 v0, p14

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 686
    :cond_9
    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 688
    :cond_a
    const-string v8, "suggest_uri"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 689
    move-object/from16 v0, p15

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 690
    :cond_b
    const-string v8, "suggest_mime_type"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 691
    move-object/from16 v0, p16

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 692
    :cond_c
    const-string v8, "suggest_ink_data"

    sget-object v9, Lcom/android/calendar/globalSearch/a;->h:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 693
    move-object/from16 v0, p17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 696
    :cond_d
    return-object v3
.end method

.method private a(Lcom/android/calendar/globalSearch/b;)Ljava/util/ArrayList;
    .locals 21

    .prologue
    .line 516
    .line 519
    const/4 v12, 0x1

    .line 520
    const-wide/16 v4, 0x0

    .line 522
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/globalSearch/b;->c:J

    .line 523
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/globalSearch/b;->d:J

    .line 525
    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-nez v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/globalSearch/b;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 526
    new-instance v3, Lcom/android/a/b;

    invoke-direct {v3}, Lcom/android/a/b;-><init>()V

    .line 529
    :try_start_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/globalSearch/b;->g:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/android/a/b;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/a/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_0
    invoke-virtual {v3}, Lcom/android/a/b;->a()J

    move-result-wide v2

    add-long v10, v8, v2

    .line 536
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://com.android.calendar/globalSearch/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/globalSearch/b;->a:J

    invoke-static {v2, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    .line 538
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/globalSearch/b;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 539
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    const v3, 0x7f0f02dd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 544
    :goto_1
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/globalSearch/b;->h:I

    .line 546
    if-nez v2, :cond_1

    .line 547
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/globalSearch/b;->i:I

    .line 550
    :cond_1
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/globalSearch/b;->j:Z

    if-eqz v3, :cond_2

    .line 551
    const-wide/16 v14, 0x4

    or-long/2addr v4, v14

    .line 553
    :cond_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/globalSearch/b;->k:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 554
    const-wide/16 v14, 0x8

    or-long v13, v4, v14

    .line 557
    :goto_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/globalSearch/b;->f:Ljava/lang/String;

    if-eqz v3, :cond_5

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/globalSearch/b;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/globalSearch/a;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 558
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/globalSearch/b;->f:Ljava/lang/String;

    .line 559
    const/4 v12, 0x2

    .line 567
    :cond_3
    :goto_3
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/globalSearch/b;->a:J

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/calendar/globalSearch/b;->a:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    invoke-static {v3, v15}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3, v15}, Lcom/android/calendar/globalSearch/a;->a(JLjava/lang/String;Z)Landroid/net/Uri;

    move-result-object v18

    .line 570
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/globalSearch/b;->a:J

    invoke-static {v2}, Lcom/android/calendar/hj;->b(I)I

    move-result v15

    const/16 v17, 0x0

    const-string v19, "text/x-vCalendar"

    const/16 v20, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v20}, Lcom/android/calendar/globalSearch/a;->a(JLjava/lang/String;Ljava/lang/String;JJIJILandroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2

    .line 530
    :catch_0
    move-exception v2

    .line 531
    invoke-virtual {v2}, Lcom/android/a/a;->printStackTrace()V

    goto/16 :goto_0

    .line 541
    :cond_4
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/globalSearch/b;->b:Ljava/lang/String;

    goto :goto_1

    .line 560
    :cond_5
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/globalSearch/b;->e:Ljava/lang/String;

    if-eqz v3, :cond_6

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/globalSearch/b;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/globalSearch/a;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 561
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/globalSearch/b;->e:Ljava/lang/String;

    .line 562
    const/4 v12, 0x2

    goto :goto_3

    .line 563
    :cond_6
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/globalSearch/b;->a:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/calendar/globalSearch/a;->b(J)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 564
    const/4 v12, 0x2

    goto :goto_3

    :cond_7
    move-wide v13, v4

    goto :goto_2
.end method

.method private a(Lcom/android/calendar/globalSearch/c;)Ljava/util/ArrayList;
    .locals 21

    .prologue
    .line 612
    const-string v2, "content://com.android.calendar/globalSearch"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/globalSearch/c;->a:J

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    .line 613
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/globalSearch/c;->a:J

    sget-object v4, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/globalSearch/c;->a:J

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/android/calendar/globalSearch/a;->a(JLjava/lang/String;Z)Landroid/net/Uri;

    move-result-object v18

    .line 617
    const/4 v7, 0x0

    .line 618
    const/4 v12, 0x0

    .line 620
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/globalSearch/c;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 621
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    const v3, 0x7f0f02bf

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 626
    :goto_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/globalSearch/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/globalSearch/c;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/globalSearch/a;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 627
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/globalSearch/c;->c:Ljava/lang/String;

    .line 628
    const/4 v12, 0x4

    .line 631
    :cond_0
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/globalSearch/c;->d:J

    .line 632
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/android/calendar/globalSearch/c;->g:Z

    if-eqz v4, :cond_1

    .line 633
    const-wide/16 v4, 0x4

    or-long/2addr v2, v4

    .line 636
    :cond_1
    const-wide/16 v4, 0x20

    .line 637
    move-object/from16 v0, p1

    iget v8, v0, Lcom/android/calendar/globalSearch/c;->h:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_4

    .line 638
    const-wide/16 v4, 0x40

    .line 642
    :cond_2
    :goto_1
    or-long v13, v2, v4

    .line 644
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/globalSearch/c;->a:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/globalSearch/c;->f:J

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/globalSearch/c;->e:I

    invoke-static {v2, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v15

    const/16 v17, 0x0

    const-string v19, "text/x-vtodo"

    const/16 v20, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v20}, Lcom/android/calendar/globalSearch/a;->a(JLjava/lang/String;Ljava/lang/String;JJIJILandroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2

    .line 623
    :cond_3
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/globalSearch/c;->b:Ljava/lang/String;

    goto :goto_0

    .line 639
    :cond_4
    move-object/from16 v0, p1

    iget v8, v0, Lcom/android/calendar/globalSearch/c;->h:I

    if-nez v8, :cond_2

    .line 640
    const-wide/16 v4, 0x10

    goto :goto_1
.end method

.method private a(Landroid/text/format/Time;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 311
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 313
    iget-wide v4, p0, Lcom/android/calendar/globalSearch/a;->d:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    iget-wide v4, p0, Lcom/android/calendar/globalSearch/a;->e:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 314
    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    .line 315
    iput v1, v2, Landroid/text/format/Time;->hour:I

    .line 316
    iput v1, v2, Landroid/text/format/Time;->minute:I

    .line 317
    iput v1, v2, Landroid/text/format/Time;->second:I

    .line 319
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 321
    iget v3, v2, Landroid/text/format/Time;->month:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->month:I

    .line 322
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 324
    iget-wide v6, p0, Lcom/android/calendar/globalSearch/a;->d:J

    cmp-long v2, v6, v2

    if-gez v2, :cond_1

    iget-wide v2, p0, Lcom/android/calendar/globalSearch/a;->e:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 326
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 324
    goto :goto_0
.end method

.method private b(J)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 700
    .line 701
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 702
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    .line 704
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 709
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/globalSearch/a;->k:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 712
    if-eqz v1, :cond_5

    .line 713
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 714
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 715
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 717
    if-eqz v0, :cond_2

    .line 718
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 719
    const/16 v3, 0x2c

    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 721
    :cond_1
    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 724
    :cond_2
    if-eqz v2, :cond_0

    .line 725
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 726
    const/16 v0, 0x2c

    invoke-virtual {v8, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 728
    :cond_3
    invoke-virtual {v8, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 733
    :catchall_0
    move-exception v0

    move-object v6, v1

    :goto_1
    if-eqz v6, :cond_4

    .line 734
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 735
    :cond_4
    throw v0

    .line 733
    :cond_5
    if-eqz v1, :cond_6

    .line 734
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 739
    :cond_6
    if-eqz v7, :cond_7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/globalSearch/a;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 745
    :goto_2
    return-object v0

    .line 741
    :cond_7
    if-eqz v8, :cond_8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/globalSearch/a;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 742
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 733
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_8
    move-object v0, v6

    goto :goto_2
.end method

.method private b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 596
    .line 598
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    if-nez v1, :cond_1

    .line 608
    :cond_0
    :goto_0
    return v0

    .line 602
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 603
    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    .line 604
    if-eqz v2, :cond_2

    .line 605
    const/4 v0, 0x1

    goto :goto_0

    .line 602
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1
.end method

.method static synthetic b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/android/calendar/globalSearch/a;->i:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/android/calendar/globalSearch/a;->l:[Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/android/calendar/globalSearch/a;->f:Z

    if-nez v0, :cond_0

    .line 186
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/a;->g()V

    .line 187
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/a;->f()V

    .line 189
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/globalSearch/a;->e()V

    .line 190
    return-void
.end method

.method private e()V
    .locals 25

    .prologue
    .line 193
    const-string v3, "word like \'%"

    .line 194
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 197
    const/4 v2, 0x0

    .line 199
    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    add-int/lit8 v7, v2, 0x1

    aget-object v2, v6, v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "%\' escape \'`\' "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    array-length v2, v2

    if-ge v7, v2, :cond_0

    .line 202
    const/16 v2, 0x20

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    add-int/lit8 v2, v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v6, 0x20

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 210
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/globalSearch/a;->g:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "word"

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 212
    const-wide/16 v4, 0x0

    .line 215
    if-eqz v21, :cond_2

    :try_start_0
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 216
    :goto_1
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 217
    const-string v2, "word"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 218
    const-string v3, "region"

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 219
    const-string v6, "photo"

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 221
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/calendar/globalSearch/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 223
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/calendar/globalSearch/a;->a(Ljava/lang/String;)J

    move-result-wide v2

    .line 225
    const-wide/16 v8, -0x1

    cmp-long v7, v2, v8

    if-lez v7, :cond_1

    .line 226
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v17

    .line 227
    const-string v6, "content://com.android.calendar/handwriting"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    move-object/from16 v24, v0

    const-wide/16 v6, 0x1

    add-long v22, v4, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/calendar/globalSearch/a;->a(J)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x3

    const-wide/16 v13, 0x0

    const/4 v15, 0x0

    const-string v19, "image/png"

    move-object/from16 v3, p0

    move-object/from16 v18, v17

    invoke-direct/range {v3 .. v20}, Lcom/android/calendar/globalSearch/a;->a(JLjava/lang/String;Ljava/lang/String;JJIJILandroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    move-wide/from16 v4, v22

    .line 232
    :cond_1
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 236
    :catchall_0
    move-exception v2

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_2
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 238
    return-void
.end method

.method private f()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/16 v5, 0x20

    const/4 v6, 0x0

    .line 330
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 333
    const/4 v0, 0x0

    .line 335
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    aget-object v0, v1, v0

    .line 336
    const-string v1, "( subject LIKE \'%"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "%\' escape \'`\'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " OR "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "body LIKE \'%"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\' escape \'`\' )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ge v2, v0, :cond_0

    .line 341
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    add-int/lit8 v0, v2, 0x1

    aget-object v2, v4, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 348
    :cond_0
    const-string v0, "selected=1 AND deleted=0"

    .line 350
    iget-wide v4, p0, Lcom/android/calendar/globalSearch/a;->d:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_1

    iget-wide v4, p0, Lcom/android/calendar/globalSearch/a;->e:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_1

    .line 351
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    iget-wide v4, p0, Lcom/android/calendar/globalSearch/a;->d:J

    iget-wide v8, p0, Lcom/android/calendar/globalSearch/a;->e:J

    invoke-static {v0, v4, v5, v8, v9}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 355
    const-string v1, " AND "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    :goto_1
    :try_start_0
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 364
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/globalSearch/a;->l:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 367
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/globalSearch/a;->a(Landroid/database/Cursor;Z)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 369
    if-eqz v1, :cond_2

    .line 370
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 374
    :cond_2
    return-void

    .line 357
    :cond_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 369
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_4

    .line 370
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 371
    :cond_4
    throw v0

    .line 369
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private g()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/16 v11, 0x20

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 377
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.android.calendar"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "searchresult"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    .line 387
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 391
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 393
    if-eqz v1, :cond_a

    .line 394
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 395
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v7, v8

    .line 400
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 403
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    add-int/lit8 v2, v6, 0x1

    aget-object v1, v1, v6

    .line 406
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v3

    const-string v4, "JAPAN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 407
    const-string v3, "ifnull(Calendars.name,\'\') !=\'legalHoliday\' AND "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    :cond_0
    const-string v3, "( "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "title"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIKE \'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\' escape \'`\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    const-string v3, "OR "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "eventLocation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIKE \'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\' escape \'`\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    const-string v3, "OR "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "description"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIKE \'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\' escape \'`\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    if-eqz v0, :cond_1

    .line 416
    const-string v3, "OR (( "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "attendeeName"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIKE \'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\' escape \'`\' OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "attendeeEmail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIKE \'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "%\' escape \'`\' )"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    const-string v1, " AND Events.organizer != Attendees.attendeeEmail)"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    :cond_1
    const/16 v1, 0x29

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 423
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v2, v1, :cond_3

    .line 424
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/globalSearch/a;->c:[Ljava/lang/String;

    add-int/lit8 v6, v2, 0x1

    aget-object v2, v3, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 393
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_2

    .line 394
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    .line 395
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 396
    :cond_2
    throw v0

    .line 429
    :cond_3
    const-string v0, " AND lastSynced=0"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    :goto_2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 436
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "allDay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 437
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "allDay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 440
    :try_start_1
    iget-wide v0, p0, Lcom/android/calendar/globalSearch/a;->d:J

    cmp-long v0, v0, v12

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lcom/android/calendar/globalSearch/a;->e:J

    cmp-long v0, v0, v12

    if-eqz v0, :cond_7

    .line 442
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Lcom/android/calendar/globalSearch/a;->d:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/calendar/globalSearch/a;->e:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 443
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/globalSearch/a;->j:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "begin DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 447
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 448
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 450
    iget-wide v2, p0, Lcom/android/calendar/globalSearch/a;->d:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 451
    const-string v2, "UTC"

    iput-object v2, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 453
    iget-wide v2, p0, Lcom/android/calendar/globalSearch/a;->e:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 454
    const-string v2, "UTC"

    iput-object v2, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 458
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    .line 459
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    const-wide/32 v4, 0xea60

    sub-long/2addr v0, v4

    .line 461
    sget-object v4, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 462
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/globalSearch/a;->j:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "begin DESC"

    move-object v3, v10

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 472
    :goto_3
    const/4 v0, 0x0

    invoke-virtual {p0, v7, v0}, Lcom/android/calendar/globalSearch/a;->a(Landroid/database/Cursor;Z)I

    .line 473
    const/4 v0, 0x0

    invoke-virtual {p0, v8, v0}, Lcom/android/calendar/globalSearch/a;->a(Landroid/database/Cursor;Z)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 478
    if-eqz v7, :cond_4

    .line 479
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 483
    :cond_4
    if-eqz v8, :cond_5

    .line 484
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 488
    :cond_5
    return-void

    .line 431
    :cond_6
    const-string v0, "lastSynced=0"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 466
    :cond_7
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/globalSearch/a;->i:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v9

    move-object v3, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v7

    goto :goto_3

    .line 478
    :catchall_1
    move-exception v0

    if-eqz v7, :cond_8

    .line 479
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 483
    :cond_8
    if-eqz v8, :cond_9

    .line 484
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 485
    :cond_9
    throw v0

    :cond_a
    move v0, v6

    move-object v7, v1

    goto/16 :goto_0
.end method

.method private h()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 749
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    const-string v1, "preferences_home_tz_enabled"

    invoke-static {v0, v1, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    .line 750
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->a:Landroid/content/Context;

    const-string v2, "preferences_home_tz"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 753
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 754
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 755
    aget-object v0, v0, v4

    .line 758
    :cond_0
    if-eqz v1, :cond_1

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;Z)I
    .locals 2

    .prologue
    .line 491
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    if-nez v0, :cond_1

    .line 492
    :cond_0
    const/4 v0, -0x1

    .line 512
    :goto_0
    return v0

    .line 496
    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 497
    if-nez p2, :cond_2

    .line 500
    new-instance v0, Lcom/android/calendar/globalSearch/b;

    invoke-direct {v0, p1}, Lcom/android/calendar/globalSearch/b;-><init>(Landroid/database/Cursor;)V

    .line 501
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    invoke-direct {p0, v0}, Lcom/android/calendar/globalSearch/a;->a(Lcom/android/calendar/globalSearch/b;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 507
    :cond_2
    new-instance v0, Lcom/android/calendar/globalSearch/c;

    invoke-direct {v0, p1}, Lcom/android/calendar/globalSearch/c;-><init>(Landroid/database/Cursor;)V

    .line 508
    iget-object v1, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    invoke-direct {p0, v0}, Lcom/android/calendar/globalSearch/a;->a(Lcom/android/calendar/globalSearch/c;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 512
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    invoke-virtual {v0}, Landroid/database/MatrixCursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public a()Landroid/database/MatrixCursor;
    .locals 1

    .prologue
    .line 762
    iget-object v0, p0, Lcom/android/calendar/globalSearch/a;->b:Landroid/database/MatrixCursor;

    return-object v0
.end method
