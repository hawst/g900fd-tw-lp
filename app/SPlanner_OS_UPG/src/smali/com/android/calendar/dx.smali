.class Lcom/android/calendar/dx;
.super Landroid/content/AsyncQueryHandler;
.source "FacebookEventSyncServiceICSUP.java"


# instance fields
.field a:Landroid/content/ContentResolver;

.field final synthetic b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;


# direct methods
.method public constructor <init>(Lcom/android/calendar/FacebookEventSyncServiceICSUP;Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 278
    iput-object p1, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    .line 279
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 276
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/dx;->a:Landroid/content/ContentResolver;

    .line 280
    iput-object p2, p0, Lcom/android/calendar/dx;->a:Landroid/content/ContentResolver;

    .line 281
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 296
    .line 297
    iget-object v0, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-virtual {v0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b()J

    move-result-wide v0

    .line 299
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 300
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 346
    :goto_0
    return-void

    .line 304
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/dx;->a:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/calendar/hj;->e:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 305
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 307
    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 308
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 310
    const-string v3, "calendar_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 311
    const-string v3, "eventTimezone"

    const-string v4, "UTC"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v3, "title"

    const-string v4, "title"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const-string v3, "allDay"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 314
    const-string v3, "dtstart"

    const-string v4, "start_time"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 315
    const-string v3, "dtend"

    const-string v4, "end_time"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 316
    const-string v3, "description"

    const-string v4, "content"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string v3, "eventLocation"

    const-string v4, "place"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const-string v3, "facebook_schedule_id"

    const-string v4, "event_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 320
    const-string v3, "facebook_service_provider"

    const-string v4, "service_provider"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 322
    const-string v3, "facebook_owner"

    const-string v4, "owner"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const-string v3, "post_time"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 325
    const-string v3, "facebook_post_time"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 327
    const-string v3, "photo_url"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 328
    const-string v4, "facebook_photo_url"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget-object v4, p0, Lcom/android/calendar/dx;->a:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 331
    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 333
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 334
    iget-object v6, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-static {v6}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Ljava/util/HashMap;

    move-result-object v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 339
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 341
    iget-object v0, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 342
    new-instance v0, Lcom/android/calendar/dv;

    iget-object v1, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-direct {v0, v1}, Lcom/android/calendar/dv;-><init>(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)V

    .line 343
    invoke-virtual {v0}, Lcom/android/calendar/dv;->start()V

    .line 345
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-virtual {v0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->stopSelf()V

    goto/16 :goto_0
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 350
    .line 351
    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 352
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 353
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    move v10, v0

    .line 356
    :goto_0
    if-eqz p1, :cond_0

    .line 357
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-virtual {v0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b()J

    move-result-wide v0

    long-to-int v0, v0

    .line 361
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 376
    :cond_1
    return-void

    .line 364
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/dx;->a:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/calendar/hj;->e:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 365
    iget-object v1, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-static {v1}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->c(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Lcom/android/calendar/dw;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/calendar/dw;->a(I)V

    move v8, v9

    .line 366
    :goto_1
    div-int/lit8 v0, v10, 0x64

    add-int/lit8 v0, v0, 0x1

    if-ge v8, v0, :cond_1

    .line 367
    const-string v0, "content://com.sec.android.app.provider.sns/event/from/%d/to/%d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    mul-int/lit8 v3, v8, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v9

    const/4 v3, 0x1

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 369
    div-int/lit8 v0, v10, 0x64

    if-ge v8, v0, :cond_3

    .line 370
    iget-object v0, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->c(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Lcom/android/calendar/dw;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v4, Lcom/android/calendar/hj;->L:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/dw;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    :goto_2
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 373
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/dx;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->c(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Lcom/android/calendar/dw;

    move-result-object v0

    const/4 v1, 0x4

    sget-object v4, Lcom/android/calendar/hj;->L:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/dw;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move v10, v9

    goto :goto_0
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 285
    if-nez p3, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 289
    invoke-direct {p0, p3}, Lcom/android/calendar/dx;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 290
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 291
    invoke-direct {p0, p3}, Lcom/android/calendar/dx;->b(Landroid/database/Cursor;)V

    goto :goto_0
.end method
