.class public Lcom/android/calendar/gz;
.super Landroid/app/Fragment;
.source "StickerUtils.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/util/LruCache;

.field private c:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 328
    const-class v0, Lcom/android/calendar/gz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/gz;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/FragmentManager;)Lcom/android/calendar/gz;
    .locals 3

    .prologue
    .line 334
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-nez v0, :cond_1

    .line 335
    const/4 v0, 0x0

    .line 342
    :cond_0
    :goto_0
    return-object v0

    .line 337
    :cond_1
    sget-object v0, Lcom/android/calendar/gz;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/gz;

    .line 338
    if-nez v0, :cond_0

    .line 339
    new-instance v0, Lcom/android/calendar/gz;

    invoke-direct {v0}, Lcom/android/calendar/gz;-><init>()V

    .line 340
    invoke-virtual {p0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/gz;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/android/calendar/gz;->b:Landroid/util/LruCache;

    return-object v0
.end method

.method public a(Landroid/util/LruCache;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/android/calendar/gz;->b:Landroid/util/LruCache;

    .line 351
    return-void
.end method

.method public a(Landroid/util/SparseIntArray;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/android/calendar/gz;->c:Landroid/util/SparseIntArray;

    .line 365
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/android/calendar/gz;->b:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/android/calendar/gz;->b:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 357
    :cond_0
    return-void
.end method

.method public c()Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/android/calendar/gz;->c:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 369
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 370
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/gz;->setRetainInstance(Z)V

    .line 371
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 375
    invoke-super {p0, p1}, Landroid/app/Fragment;->onTrimMemory(I)V

    .line 376
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-nez v0, :cond_1

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    const/16 v0, 0x28

    if-lt p1, v0, :cond_0

    .line 380
    invoke-virtual {p0}, Lcom/android/calendar/gz;->b()V

    goto :goto_0
.end method
