.class public Lcom/android/calendar/CalendarSearchView;
.super Landroid/widget/LinearLayout;
.source "CalendarSearchView.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:I

.field private d:Landroid/widget/SearchView$OnQueryTextListener;

.field private e:Landroid/widget/AutoCompleteTextView;

.field private f:Landroid/view/View;

.field private g:Lcom/android/calendar/al;

.field private h:J

.field private i:Ljava/lang/String;

.field private j:I

.field private k:I

.field private l:Landroid/text/TextWatcher;

.field private final m:Landroid/widget/TextView$OnEditorActionListener;

.field private n:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/CalendarSearchView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    iput v3, p0, Lcom/android/calendar/CalendarSearchView;->b:I

    .line 60
    iput v2, p0, Lcom/android/calendar/CalendarSearchView;->c:I

    .line 341
    new-instance v0, Lcom/android/calendar/bc;

    invoke-direct {v0, p0}, Lcom/android/calendar/bc;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchView;->l:Landroid/text/TextWatcher;

    .line 371
    new-instance v0, Lcom/android/calendar/bd;

    invoke-direct {v0, p0}, Lcom/android/calendar/bd;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchView;->m:Landroid/widget/TextView$OnEditorActionListener;

    .line 411
    new-instance v0, Lcom/android/calendar/be;

    invoke-direct {v0, p0}, Lcom/android/calendar/be;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchView;->n:Ljava/lang/Runnable;

    .line 77
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 79
    const v1, 0x7f040085

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 80
    const v0, 0x7f120284

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    .line 81
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    .line 82
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->l:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 83
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->m:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 84
    new-instance v0, Lcom/android/calendar/en;

    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    .line 86
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    new-array v2, v2, [Landroid/text/InputFilter;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 87
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/aw;

    invoke-direct {v1, p0}, Lcom/android/calendar/aw;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/ax;

    invoke-direct {v1, p0}, Lcom/android/calendar/ax;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 102
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/ay;

    invoke-direct {v1, p0}, Lcom/android/calendar/ay;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 122
    const v0, 0x7f120285

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchView;->f:Landroid/view/View;

    .line 123
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->f:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/az;

    invoke-direct {v1, p0}, Lcom/android/calendar/az;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->f:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/ba;

    invoke-direct {v1, p0}, Lcom/android/calendar/ba;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c02cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 143
    if-eqz v0, :cond_0

    .line 144
    invoke-direct {p0, v0}, Lcom/android/calendar/CalendarSearchView;->setMaxWidth(I)V

    .line 146
    :cond_0
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/CalendarSearchView;->g:Lcom/android/calendar/al;

    .line 147
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/CalendarSearchView;J)J
    .locals 1

    .prologue
    .line 55
    iput-wide p1, p0, Lcom/android/calendar/CalendarSearchView;->h:J

    return-wide p1
.end method

.method static synthetic a(Lcom/android/calendar/CalendarSearchView;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->g:Lcom/android/calendar/al;

    return-object v0
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 150
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 151
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 152
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->g:Lcom/android/calendar/al;

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(I)V

    .line 157
    :goto_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 159
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_0

    .line 162
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    move v0, v1

    .line 167
    :goto_1
    return v0

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->g:Lcom/android/calendar/al;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/android/calendar/al;->a(I)V

    goto :goto_0

    .line 167
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 55
    invoke-static {p0}, Lcom/android/calendar/CalendarSearchView;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 388
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    move v1, v0

    .line 389
    :goto_0
    if-ge v1, v2, :cond_0

    .line 390
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-eq v3, v4, :cond_1

    .line 391
    const/4 v0, 0x1

    .line 394
    :cond_0
    return v0

    .line 389
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/CalendarSearchView;)Z
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/CalendarSearchView;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/SearchView$OnQueryTextListener;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->d:Landroid/widget/SearchView$OnQueryTextListener;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/CalendarSearchView;)J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/android/calendar/CalendarSearchView;->h:J

    return-wide v0
.end method

.method static synthetic f(Lcom/android/calendar/CalendarSearchView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->f:Landroid/view/View;

    return-object v0
.end method

.method private setMaxWidth(I)V
    .locals 0

    .prologue
    .line 171
    iput p1, p0, Lcom/android/calendar/CalendarSearchView;->k:I

    .line 172
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->requestLayout()V

    .line 173
    return-void
.end method


# virtual methods
.method public a(JLjava/lang/String;I)V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/16 v0, 0xb

    const/4 v8, 0x0

    .line 234
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 235
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 236
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    :cond_1
    :goto_0
    return-void

    .line 240
    :cond_2
    iput-wide p1, p0, Lcom/android/calendar/CalendarSearchView;->h:J

    .line 241
    iput-object p3, p0, Lcom/android/calendar/CalendarSearchView;->i:Ljava/lang/String;

    .line 243
    if-ltz p4, :cond_3

    if-le p4, v0, :cond_4

    .line 244
    :cond_3
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 245
    const/16 v1, 0xf

    if-eq p4, v1, :cond_4

    move p4, v0

    .line 253
    :cond_4
    :goto_1
    iput p4, p0, Lcom/android/calendar/CalendarSearchView;->j:I

    .line 254
    invoke-static {p3, v8, v8}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 255
    new-instance v1, Landroid/text/SpannableStringBuilder;

    const-string v2, "  "

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 256
    if-eqz v0, :cond_6

    .line 257
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 258
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "search_sticker_group_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "string"

    const-string v5, "com.android.calendar"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 259
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 260
    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 261
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v3, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 262
    invoke-virtual {v3, v9}, Landroid/graphics/drawable/BitmapDrawable;->setFilterBitmap(Z)V

    .line 263
    const-wide v4, 0x3ff6666666666666L    # 1.4

    .line 264
    const v0, 0x7f0c02cc

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 265
    const v6, 0x7f0c02d0

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 266
    iget-object v6, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v6}, Landroid/widget/AutoCompleteTextView;->getTextSize()F

    move-result v6

    float-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 267
    rsub-int/lit8 v5, v0, 0x0

    neg-int v6, v2

    sub-int v0, v4, v0

    sub-int v2, v4, v2

    invoke-virtual {v3, v5, v6, v0, v2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 268
    new-instance v0, Landroid/text/style/ImageSpan;

    invoke-direct {v0, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/16 v2, 0x21

    invoke-virtual {v1, v0, v8, v9, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 269
    const-string v0, "     "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 274
    :goto_2
    invoke-virtual {p0, v1}, Lcom/android/calendar/CalendarSearchView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getSelectionStart()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    goto/16 :goto_0

    :cond_5
    move p4, v0

    .line 249
    goto/16 :goto_1

    .line 271
    :cond_6
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->clear()V

    goto :goto_2
.end method

.method public getCursorVisibility()Z
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isCursorVisible()Z

    move-result v0

    goto :goto_0
.end method

.method public getInputType()I
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getInputType()I

    move-result v0

    return v0
.end method

.method public getSelectionEnd()I
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    return v0
.end method

.method public getSelectionStart()I
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getSelectionStart()I

    move-result v0

    return v0
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 177
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 178
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 179
    iget v2, p0, Lcom/android/calendar/CalendarSearchView;->k:I

    if-lez v2, :cond_1

    .line 180
    sparse-switch v1, :sswitch_data_0

    .line 195
    :goto_0
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 199
    :goto_1
    return-void

    .line 186
    :sswitch_0
    iget v1, p0, Lcom/android/calendar/CalendarSearchView;->k:I

    if-lez v1, :cond_0

    .line 187
    iget v1, p0, Lcom/android/calendar/CalendarSearchView;->k:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 189
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    .line 190
    goto :goto_0

    .line 197
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_1

    .line 180
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public restoreHierarchyState(Landroid/util/SparseArray;)V
    .locals 6

    .prologue
    const/4 v2, 0x4

    .line 315
    iget v0, p0, Lcom/android/calendar/CalendarSearchView;->c:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 316
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->g:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->d()I

    move-result v1

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    .line 317
    const-string v1, "sticker_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 318
    const-string v1, "sticker_path"

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 319
    const-string v4, "sticker_group"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 320
    invoke-virtual {p0, v2, v3, v1, v0}, Lcom/android/calendar/CalendarSearchView;->a(JLjava/lang/String;I)V

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 323
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->g:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 325
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/bb;

    invoke-direct {v1, p0}, Lcom/android/calendar/bb;-><init>(Lcom/android/calendar/CalendarSearchView;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 333
    :cond_2
    iget v0, p0, Lcom/android/calendar/CalendarSearchView;->b:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 334
    if-eqz v0, :cond_0

    .line 335
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public saveHierarchyState(Landroid/util/SparseArray;)V
    .locals 4

    .prologue
    .line 300
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 302
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 303
    const-string v1, "sticker_id"

    iget-wide v2, p0, Lcom/android/calendar/CalendarSearchView;->h:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 304
    const-string v1, "sticker_path"

    iget-object v2, p0, Lcom/android/calendar/CalendarSearchView;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string v1, "sticker_group"

    iget v2, p0, Lcom/android/calendar/CalendarSearchView;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 307
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 308
    iget v1, p0, Lcom/android/calendar/CalendarSearchView;->b:I

    iget-object v2, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 309
    iget v1, p0, Lcom/android/calendar/CalendarSearchView;->c:I

    invoke-virtual {p1, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 311
    :cond_0
    return-void
.end method

.method public setCursorVisibility(Z)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setCursorVisible(Z)V

    .line 449
    :cond_0
    return-void
.end method

.method public setCursorVisible(Z)V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setCursorVisible(Z)V

    .line 285
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    .line 286
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    iget-object v1, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 288
    return-void
.end method

.method public setImeVisibility(Z)V
    .locals 3

    .prologue
    .line 398
    if-eqz p1, :cond_1

    .line 399
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->n:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchView;->post(Ljava/lang/Runnable;)Z

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->n:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarSearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 402
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 405
    if-eqz v0, :cond_0

    .line 406
    invoke-virtual {p0}, Lcom/android/calendar/CalendarSearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method public setInputType(I)V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setInputType(I)V

    .line 292
    return-void
.end method

.method public setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/android/calendar/CalendarSearchView;->d:Landroid/widget/SearchView$OnQueryTextListener;

    .line 203
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 219
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->d:Landroid/widget/SearchView$OnQueryTextListener;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/android/calendar/CalendarSearchView;->d:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    .line 231
    :cond_0
    return-void
.end method
