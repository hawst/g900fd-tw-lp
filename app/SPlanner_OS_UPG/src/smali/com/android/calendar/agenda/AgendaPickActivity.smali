.class public Lcom/android/calendar/agenda/AgendaPickActivity;
.super Lcom/android/calendar/agenda/a;
.source "AgendaPickActivity.java"

# interfaces
.implements Lcom/android/calendar/ap;
.implements Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private A:Landroid/view/ActionMode;

.field private B:Landroid/view/LayoutInflater;

.field private C:Landroid/text/format/Time;

.field private D:Landroid/content/Intent;

.field private E:I

.field private final F:Ljava/lang/Runnable;

.field private final G:Landroid/database/ContentObserver;

.field private final H:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;

.field public b:J

.field public c:J

.field public d:I

.field public e:I

.field public f:J

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Z

.field private n:Landroid/app/Activity;

.field private o:Lcom/android/calendar/al;

.field private p:Landroid/content/ContentResolver;

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Ljava/util/ArrayList;

.field private t:Lcom/android/calendar/agenda/bb;

.field private u:Lcom/android/calendar/agenda/AgendaListView;

.field private v:Landroid/widget/LinearLayout;

.field private w:Landroid/widget/SearchView;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/CheckBox;

.field private z:Lcom/android/calendar/agenda/ba;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/AgendaPickActivity;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-direct {p0}, Lcom/android/calendar/agenda/a;-><init>()V

    .line 100
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    .line 101
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->i:Z

    .line 102
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->j:Z

    .line 105
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->f:J

    .line 107
    iput v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->l:I

    .line 108
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->m:Z

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->q:Ljava/lang/String;

    .line 113
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->r:Z

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->s:Ljava/util/ArrayList;

    .line 132
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->E:I

    .line 135
    new-instance v0, Lcom/android/calendar/agenda/au;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/au;-><init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->F:Ljava/lang/Runnable;

    .line 146
    new-instance v0, Lcom/android/calendar/agenda/av;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/agenda/av;-><init>(Lcom/android/calendar/agenda/AgendaPickActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->G:Landroid/database/ContentObserver;

    .line 158
    new-instance v0, Lcom/android/calendar/agenda/aw;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/aw;-><init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->H:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;

    .line 598
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->C:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaPickActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->A:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaPickActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->q:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 531
    if-eqz p1, :cond_0

    .line 532
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->n:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 533
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    .line 535
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/agenda/AgendaPickActivity;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->i:Z

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 485
    const v0, 0x7f1200d5

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 486
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->requestFocus()Z

    .line 487
    new-instance v0, Lcom/android/calendar/gr;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/gr;-><init>(Landroid/app/Activity;Z)V

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/bb;->y()[Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/gr;->a([Ljava/lang/Long;)V

    .line 488
    return-void
.end method

.method static synthetic d(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/AgendaListView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 491
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->z:Lcom/android/calendar/agenda/ba;

    if-nez v0, :cond_0

    .line 492
    new-instance v0, Lcom/android/calendar/agenda/ba;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->n:Landroid/app/Activity;

    iget v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/agenda/ba;-><init>(Lcom/android/calendar/agenda/AgendaPickActivity;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->z:Lcom/android/calendar/agenda/ba;

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->n:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->z:Lcom/android/calendar/agenda/ba;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->A:Landroid/view/ActionMode;

    .line 495
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/agenda/AgendaPickActivity;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->m:Z

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/agenda/AgendaPickActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/agenda/AgendaPickActivity;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    return v0
.end method

.method static synthetic h(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->n:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->A:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/widget/SearchView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->w:Landroid/widget/SearchView;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->y:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/agenda/AgendaPickActivity;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c()V

    return-void
.end method


# virtual methods
.method public a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;J)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 566
    if-nez p1, :cond_0

    .line 596
    :goto_0
    return-void

    .line 569
    :cond_0
    iget-boolean v0, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v0, :cond_1

    .line 570
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 571
    const-string v1, "action_memo_id"

    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->f:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 572
    const-string v1, "action_event_title"

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 573
    const-string v1, "task"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 574
    const-string v1, "selected"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 575
    const-string v1, "copyEvent"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 576
    const-string v1, "action_parser_mode"

    iget v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 577
    const-string v1, "launch_from_inside"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 578
    const-class v1, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 579
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->startActivity(Landroid/content/Intent;)V

    .line 595
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->finish()V

    goto :goto_0

    .line 581
    :cond_1
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 582
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.EDIT"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 583
    const-string v0, "action_memo_id"

    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->f:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 584
    const-string v0, "action_event_title"

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 585
    const-string v0, "beginTime"

    iget-wide v2, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 586
    const-string v0, "endTime"

    iget-wide v2, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 587
    const-string v0, "action_view_focus"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 588
    const-string v0, "copyEvent"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 589
    const-string v0, "action_parser_mode"

    iget v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->l:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 590
    const-string v0, "launch_from_inside"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 591
    const-class v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 592
    const-string v0, "editMode"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 593
    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 466
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->b()V

    .line 469
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 457
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 473
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->w:Landroid/widget/SearchView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->w:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->b(Z)V

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 477
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->w:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->w:Landroid/widget/SearchView;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 512
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 527
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 514
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 515
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->finish()V

    goto :goto_0

    .line 521
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 512
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method public g()J
    .locals 2

    .prologue
    .line 461
    const-wide/16 v0, 0x80

    return-wide v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 449
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 451
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    if-eqz v0, :cond_0

    .line 452
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->a()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/bb;->b(Z)V

    .line 454
    :cond_0
    return-void

    .line 452
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v7, -0x1

    const-wide/16 v2, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 183
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onCreate(Landroid/os/Bundle;)V

    .line 184
    const v0, 0x7f040010

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->setContentView(I)V

    .line 186
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 188
    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v5, v5, 0x100

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 189
    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 190
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 192
    iput-object p0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->n:Landroid/app/Activity;

    .line 193
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->n:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->o:Lcom/android/calendar/al;

    .line 194
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->o:Lcom/android/calendar/al;

    const v1, 0x7f040010

    invoke-virtual {v0, v1, p0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 195
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->p:Landroid/content/ContentResolver;

    .line 196
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->n:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->r:Z

    .line 198
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    .line 199
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    :cond_0
    sget-object v0, Lcom/android/calendar/agenda/AgendaPickActivity;->g:Ljava/lang/String;

    const-string v1, "Intent is null. You have to send an intent"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->finish()V

    .line 396
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->n:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->B:Landroid/view/LayoutInflater;

    .line 207
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_3

    array-length v1, v0

    const/4 v5, 0x2

    if-ne v1, v5, :cond_3

    aget-object v1, v0, v4

    const-string v5, "vnd.android.cursor"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 209
    aget-object v1, v0, v4

    const-string v5, ".dir"

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->i:Z

    .line 210
    aget-object v1, v0, v6

    const-string v5, "vnd.samsung.calendar.event"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    aget-object v1, v0, v6

    const-string v5, "event"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_2
    move v0, v6

    :goto_1
    iput v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->E:I

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "choice_limit"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 217
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "choice_limit"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    .line 218
    iget v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    if-lez v0, :cond_f

    move v0, v6

    :goto_2
    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->i:Z

    .line 226
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "multiple_choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 227
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "multiple_choice"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->i:Z

    .line 230
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "hide_recurrent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 231
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "hide_recurrent"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->j:Z

    .line 234
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "action_memo_id"

    invoke-virtual {v0, v1, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->f:J

    .line 235
    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->f:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_6

    .line 236
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "action_memo_id"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->f:J

    .line 239
    :cond_6
    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->f:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_7

    .line 240
    iput-boolean v6, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->m:Z

    .line 241
    iput-boolean v4, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->i:Z

    .line 242
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "action_event_title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->k:Ljava/lang/String;

    .line 243
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "action_event_title"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->l:I

    .line 246
    :cond_7
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->i:Z

    if-nez v0, :cond_8

    .line 247
    iput v6, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    .line 250
    :cond_8
    invoke-static {p0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 251
    :goto_4
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->C:Landroid/text/format/Time;

    .line 252
    if-eqz p1, :cond_14

    const-string v0, "key_restore_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 254
    const-string v0, "key_restore_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 257
    :goto_5
    cmp-long v5, v0, v2

    if-nez v5, :cond_9

    .line 259
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "beginTime"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 261
    :cond_9
    cmp-long v2, v0, v2

    if-nez v2, :cond_a

    .line 262
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 264
    :cond_a
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->C:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 266
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "beginTime"

    invoke-virtual {v0, v1, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->b:J

    .line 267
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "endTime"

    invoke-virtual {v0, v1, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->c:J

    .line 268
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "event_start_day"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->d:I

    .line 269
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->D:Landroid/content/Intent;

    const-string v1, "event_end_day"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->e:I

    .line 271
    const v0, 0x7f12002b

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaListView;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    .line 273
    new-instance v0, Lcom/android/calendar/agenda/bb;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    iget v3, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->E:I

    iget v5, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/agenda/bb;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;IZI)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    .line 275
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v4}, Lcom/android/calendar/agenda/AgendaListView;->setItemsCanFocus(Z)V

    .line 277
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    iget v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setChoiceLimit(I)V

    .line 278
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->H:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setOnChildClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;)V

    .line 280
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setAdapter(Lcom/android/calendar/agenda/AgendaBaseAdapter;)V

    .line 281
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, p0}, Lcom/android/calendar/agenda/AgendaListView;->setOnScrollListener(Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;)V

    .line 282
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->j:Z

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setHideRecurrentEvents(Z)V

    .line 283
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 284
    const v0, 0x7f120044

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->v:Landroid/widget/LinearLayout;

    .line 286
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->r:Z

    if-eqz v0, :cond_b

    .line 287
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    new-instance v1, Lcom/android/calendar/agenda/ax;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/ax;-><init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setTwMultiSelectedListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;)V

    .line 337
    :cond_b
    const v0, 0x7f120045

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->w:Landroid/widget/SearchView;

    .line 339
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->w:Landroid/widget/SearchView;

    new-instance v1, Lcom/android/calendar/agenda/ay;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/ay;-><init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 362
    new-instance v0, Lcom/android/calendar/agenda/az;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/az;-><init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V

    .line 376
    const v1, 0x7f120218

    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->x:Landroid/view/View;

    .line 377
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->x:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 378
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->x:Landroid/view/View;

    const v1, 0x7f120054

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->y:Landroid/widget/CheckBox;

    .line 379
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->y:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/bb;->w()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 381
    if-eqz p1, :cond_13

    const-string v0, "key_selected_events"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 382
    const-string v0, "key_selected_events"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 383
    new-instance v2, Ljava/util/HashSet;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 384
    array-length v3, v1

    :goto_6
    if-ge v4, v3, :cond_12

    aget-object v0, v1, v4

    .line 385
    instance-of v5, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    if-eqz v5, :cond_c

    .line 386
    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 384
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 210
    :cond_d
    aget-object v0, v0, v6

    const-string v1, "vnd.samsung.calendar.task"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x2

    goto/16 :goto_1

    :cond_e
    const/4 v0, 0x3

    goto/16 :goto_1

    :cond_f
    move v0, v4

    .line 218
    goto/16 :goto_2

    .line 220
    :cond_10
    iput v7, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->h:I

    goto/16 :goto_3

    .line 250
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->F:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 388
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/bb;->a(Ljava/util/HashSet;)V

    .line 391
    :cond_13
    const v0, 0x7f1200de

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f02d2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 392
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 394
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaPickActivity;->d()V

    .line 395
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->p:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->G:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/16 :goto_0

    :cond_14
    move-wide v0, v2

    goto/16 :goto_5
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->A:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->A:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->p:Landroid/content/ContentResolver;

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->p:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->G:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->o:Lcom/android/calendar/al;

    const v1, 0x7f040010

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(Ljava/lang/Integer;)V

    .line 408
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onDestroy()V

    .line 409
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 503
    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->w:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    .line 505
    const/4 v0, 0x1

    .line 507
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/calendar/agenda/a;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 430
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onPause()V

    .line 431
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 413
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onResume()V

    .line 415
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setChoiceMode(I)V

    .line 416
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->r:Z

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v7}, Lcom/android/calendar/agenda/AgendaListView;->setEnableDragBlock(Z)V

    .line 420
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v0

    .line 421
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaListView;->setHideDeclinedEvents(Z)V

    .line 422
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->C:Landroid/text/format/Time;

    const v2, 0x24dc87

    const v3, 0x259d23

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->q:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 424
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->F:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 426
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 435
    const-string v0, "key_restore_time"

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->C:Landroid/text/format/Time;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 437
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->t:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->z()Ljava/util/HashSet;

    move-result-object v0

    .line 438
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 439
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 440
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 441
    const-string v0, "key_selected_events"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 444
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 445
    return-void
.end method

.method public onScroll(Lcom/sec/android/touchwiz/widget/TwAbsListView;III)V
    .locals 0

    .prologue
    .line 546
    return-void
.end method

.method public onScrollStateChanged(Lcom/sec/android/touchwiz/widget/TwAbsListView;I)V
    .locals 1

    .prologue
    .line 539
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaPickActivity;->u:Lcom/android/calendar/agenda/AgendaListView;

    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->a(Landroid/view/View;)V

    .line 542
    :cond_0
    return-void
.end method
