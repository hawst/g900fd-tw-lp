.class Lcom/android/calendar/agenda/bl;
.super Ljava/lang/Object;
.source "AgendaSDImportActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/bk;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/bk;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 301
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->b:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->b:Landroid/os/Bundle;

    const-string v1, "key_selected_events"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->b:Landroid/os/Bundle;

    const-string v1, "key_selected_events"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    .line 303
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 304
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->b()Ljava/util/HashSet;

    move-result-object v2

    .line 306
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 308
    array-length v3, v1

    .line 309
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 310
    aget v4, v1, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BUNDLE_KEY_SELECTED_EVENTS "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v1, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/br;->a(Ljava/util/HashSet;)V

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v1, v1, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/br;->a(Ljava/util/ArrayList;)V

    .line 318
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->notifyDataSetChanged()V

    .line 319
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->c(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->c(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 322
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/bl;->a:Lcom/android/calendar/agenda/bk;

    iget-object v0, v0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;I)V

    .line 323
    return-void
.end method
