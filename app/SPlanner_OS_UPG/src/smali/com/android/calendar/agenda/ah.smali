.class Lcom/android/calendar/agenda/ah;
.super Ljava/lang/Object;
.source "AgendaFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/z;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/z;)V
    .locals 0

    .prologue
    .line 1078
    iput-object p1, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1081
    iget-object v0, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1082
    iget-object v0, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getChildCount()I

    move-result v3

    .line 1083
    if-nez v3, :cond_1

    .line 1108
    :cond_0
    return v2

    .line 1084
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0, v2}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;Z)Z

    move v1, v2

    .line 1086
    :goto_0
    if-ge v1, v3, :cond_0

    .line 1087
    iget-object v0, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v4, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v4}, Lcom/android/calendar/agenda/z;->k(Lcom/android/calendar/agenda/z;)F

    move-result v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    .line 1086
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1090
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f120037

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1091
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v4, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/z;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f0d0000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    add-int/2addr v4, v0

    .line 1093
    iget-object v0, p0, Lcom/android/calendar/agenda/ah;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v5, 0x7f120036

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1095
    int-to-float v4, v4

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    .line 1096
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-static {}, Lcom/android/calendar/agenda/z;->c()Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const-wide/16 v6, 0x320

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    new-instance v5, Lcom/android/calendar/agenda/ai;

    invoke-direct {v5, p0, v0}, Lcom/android/calendar/agenda/ai;-><init>(Lcom/android/calendar/agenda/ah;Landroid/widget/LinearLayout;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1
.end method
