.class public Lcom/android/calendar/agenda/al;
.super Landroid/app/DialogFragment;
.source "AgendaFragment.java"


# instance fields
.field private a:Lcom/android/calendar/d/g;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 828
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 824
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/al;->a:Lcom/android/calendar/d/g;

    .line 829
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 831
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 824
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/al;->a:Lcom/android/calendar/d/g;

    .line 832
    iput p1, p0, Lcom/android/calendar/agenda/al;->b:I

    .line 833
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 865
    move v0, v1

    :goto_0
    invoke-static {}, Lcom/android/calendar/agenda/z;->b()[I

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 866
    invoke-static {}, Lcom/android/calendar/agenda/z;->b()[I

    move-result-object v2

    aget v2, v2, v0

    if-ne v2, p0, :cond_0

    .line 870
    :goto_1
    return v0

    .line 865
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 870
    goto :goto_1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 837
    invoke-virtual {p0}, Lcom/android/calendar/agenda/al;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 838
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09000f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 839
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 840
    if-eqz p1, :cond_0

    const-string v2, "listBy"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 841
    const-string v2, "listBy"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/agenda/al;->b:I

    .line 844
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 845
    const v1, 0x7f0f01ef

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 847
    iget v1, p0, Lcom/android/calendar/agenda/al;->b:I

    invoke-static {v1}, Lcom/android/calendar/agenda/al;->a(I)I

    move-result v1

    new-instance v3, Lcom/android/calendar/agenda/am;

    invoke-direct {v3, p0}, Lcom/android/calendar/agenda/am;-><init>(Lcom/android/calendar/agenda/al;)V

    invoke-virtual {v2, v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 858
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 859
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 861
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 875
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 876
    const-string v0, "listBy"

    iget v1, p0, Lcom/android/calendar/agenda/al;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 877
    return-void
.end method
