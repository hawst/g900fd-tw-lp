.class Lcom/android/calendar/agenda/be;
.super Ljava/lang/Object;
.source "AgendaSDExportActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaSDExportActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->d(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->w()Z

    move-result v3

    .line 205
    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->d(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/widget/CheckBox;

    move-result-object v4

    if-nez v3, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 206
    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    if-nez v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->e(Z)V

    .line 207
    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->notifyDataSetChanged()V

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->e(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/android/calendar/agenda/be;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->e(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 212
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 205
    goto :goto_0

    :cond_3
    move v1, v2

    .line 206
    goto :goto_1
.end method
