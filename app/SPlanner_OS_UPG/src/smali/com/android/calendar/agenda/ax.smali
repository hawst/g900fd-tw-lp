.class Lcom/android/calendar/agenda/ax;
.super Ljava/lang/Object;
.source "AgendaPickActivity.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaPickActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 305
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    if-nez v0, :cond_0

    .line 333
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->f(Lcom/android/calendar/agenda/AgendaPickActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 309
    :goto_1
    if-ge v3, v4, :cond_3

    .line 310
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->f(Lcom/android/calendar/agenda/AgendaPickActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 311
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/bb;->g(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 312
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v6

    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/bb;->i(I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v6, v5, v0}, Lcom/android/calendar/agenda/bb;->b(IZ)V

    .line 309
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 312
    goto :goto_2

    .line 316
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->x()I

    move-result v0

    iget-object v3, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaPickActivity;->g(Lcom/android/calendar/agenda/AgendaPickActivity;)I

    move-result v3

    if-le v0, v3, :cond_4

    .line 317
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->h(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0f0038

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v5}, Lcom/android/calendar/agenda/AgendaPickActivity;->g(Lcom/android/calendar/agenda/AgendaPickActivity;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 318
    iget-object v1, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->h(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 321
    :cond_4
    add-int/lit8 v0, v4, -0x1

    move v1, v0

    .line 322
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->x()I

    move-result v0

    iget-object v3, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaPickActivity;->g(Lcom/android/calendar/agenda/AgendaPickActivity;)I

    move-result v3

    if-le v0, v3, :cond_6

    .line 323
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->f(Lcom/android/calendar/agenda/AgendaPickActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 324
    iget-object v3, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/calendar/agenda/bb;->i(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 325
    iget-object v3, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Lcom/android/calendar/agenda/bb;->b(IZ)V

    .line 327
    :cond_5
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    .line 328
    goto :goto_3

    .line 330
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->f(Lcom/android/calendar/agenda/AgendaPickActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 331
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->notifyDataSetChanged()V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->i(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    goto/16 :goto_0
.end method

.method public onTwMultiSelected(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJZZZ)V
    .locals 3

    .prologue
    .line 290
    .line 291
    iget-object v0, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/calendar/agenda/bb;->h(I)I

    move-result v0

    .line 292
    iget-object v1, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->f(Lcom/android/calendar/agenda/AgendaPickActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->f(Lcom/android/calendar/agenda/AgendaPickActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 297
    :goto_0
    return-void

    .line 295
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/ax;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->f(Lcom/android/calendar/agenda/AgendaPickActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
