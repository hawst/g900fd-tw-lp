.class final Lcom/android/calendar/agenda/ba;
.super Lcom/android/calendar/common/a/a;
.source "AgendaPickActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaPickActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/agenda/AgendaPickActivity;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 600
    iput-object p1, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    .line 601
    invoke-direct {p0, p2, p3}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;I)V

    .line 602
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->x()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 677
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 678
    const v1, 0x7f110007

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 679
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->c:Landroid/view/Menu;

    const v1, 0x7f1200d5

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 680
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 681
    iget-object v1, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    const v2, 0x7f0f0398

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaPickActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 682
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->c:Landroid/view/Menu;

    const v1, 0x7f1200d4

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 683
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 684
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 615
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->g(Lcom/android/calendar/agenda/AgendaPickActivity;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 616
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/bb;->e(Z)V

    .line 620
    :goto_0
    return-void

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->g(Lcom/android/calendar/agenda/AgendaPickActivity;)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/android/calendar/agenda/bb;->a(ZI)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 624
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->e(Z)V

    .line 625
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->notifyDataSetChanged()V

    .line 655
    return-void
.end method

.method public e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, -0x1

    .line 629
    iget v1, p0, Lcom/android/calendar/agenda/ba;->b:I

    if-ne v1, v3, :cond_1

    .line 630
    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->h()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->h()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->a()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 640
    :cond_0
    :goto_0
    return v0

    .line 635
    :cond_1
    iget v1, p0, Lcom/android/calendar/agenda/ba;->b:I

    if-eq v1, v3, :cond_2

    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->h()I

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/calendar/agenda/ba;->b:I

    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->a()I

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->h()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->a()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 640
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->w()Z

    move-result v0

    goto :goto_0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->f()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 645
    invoke-virtual {p0}, Lcom/android/calendar/agenda/ba;->a()I

    move-result v0

    .line 646
    iget-object v3, p0, Lcom/android/calendar/agenda/ba;->c:Landroid/view/Menu;

    const v4, 0x7f1200d5

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 647
    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->b(Lcom/android/calendar/agenda/AgendaPickActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 648
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->a()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/ba;->b(Z)V

    .line 649
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->b(Lcom/android/calendar/agenda/AgendaPickActivity;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/ba;->c(Z)V

    .line 650
    return-void

    :cond_0
    move v0, v2

    .line 647
    goto :goto_0

    :cond_1
    move v1, v2

    .line 648
    goto :goto_1
.end method

.method public h()I
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->A()I

    move-result v0

    return v0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 664
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 672
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 666
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->l(Lcom/android/calendar/agenda/AgendaPickActivity;)V

    .line 667
    const/4 v0, 0x1

    goto :goto_0

    .line 664
    :pswitch_data_0
    .packed-switch 0x7f1200d5
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 688
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 689
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->d(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaListView;->setActionMode(Landroid/view/ActionMode;)V

    .line 690
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->a(Landroid/view/ActionMode;)V

    .line 691
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 702
    invoke-super {p0, p1}, Lcom/android/calendar/common/a/a;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 703
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->a(Lcom/android/calendar/agenda/AgendaPickActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 704
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->d(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setActionMode(Landroid/view/ActionMode;)V

    .line 705
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->c(Landroid/view/ActionMode;)V

    .line 707
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->h(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->h(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->h(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->h(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 710
    :cond_0
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/android/calendar/agenda/ba;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->b(Landroid/view/ActionMode;)V

    .line 697
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
