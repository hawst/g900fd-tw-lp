.class public Lcom/android/calendar/agenda/br;
.super Landroid/widget/BaseAdapter;
.source "AgendaSDImportAdapter.java"


# static fields
.field private static final c:Ljava/lang/String;

.field private static final d:Z


# instance fields
.field protected a:Ljava/util/HashSet;

.field protected b:Z

.field private e:Ljava/util/ArrayList;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/view/LayoutInflater;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 37
    const-class v2, Lcom/android/calendar/agenda/br;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/calendar/agenda/br;->c:Ljava/lang/String;

    .line 39
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/android/calendar/agenda/br;->d:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/br;->b:Z

    .line 50
    iput-object p1, p0, Lcom/android/calendar/agenda/br;->f:Landroid/content/Context;

    .line 51
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->f:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/agenda/br;->g:Landroid/view/LayoutInflater;

    .line 52
    iput-object p3, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/br;->h:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Lcom/android/calendar/dh;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    goto :goto_0
.end method

.method public a(IZ)V
    .locals 2

    .prologue
    .line 85
    if-eqz p2, :cond_0

    .line 86
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 91
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/agenda/br;->d()V

    .line 92
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    .line 58
    return-void
.end method

.method public a(Ljava/util/HashSet;)V
    .locals 0

    .prologue
    .line 120
    if-nez p1, :cond_0

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    iput-object p1, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    .line 124
    invoke-virtual {p0}, Lcom/android/calendar/agenda/br;->d()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 95
    if-eqz p1, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/android/calendar/agenda/br;->getCount()I

    move-result v1

    .line 97
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 98
    iget-object v2, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 104
    :cond_1
    iput-boolean p1, p0, Lcom/android/calendar/agenda/br;->b:Z

    .line 105
    return-void
.end method

.method public b()Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    return-object v0
.end method

.method public b(I)Z
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/android/calendar/agenda/br;->b:Z

    return v0
.end method

.method protected d()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-virtual {p0}, Lcom/android/calendar/agenda/br;->getCount()I

    move-result v2

    move v0, v1

    .line 134
    :goto_0
    if-ge v0, v2, :cond_1

    .line 135
    iget-object v3, p0, Lcom/android/calendar/agenda/br;->a:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 136
    iput-boolean v1, p0, Lcom/android/calendar/agenda/br;->b:Z

    .line 142
    :goto_1
    return-void

    .line 134
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 141
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/br;->b:Z

    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 65
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/br;->a(I)Lcom/android/calendar/dh;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 79
    const-wide/16 v0, -0x1

    .line 81
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    iget-wide v0, v0, Lcom/android/calendar/dh;->b:J

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 146
    .line 149
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 152
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 153
    instance-of v2, v0, Lcom/android/calendar/agenda/bs;

    if-eqz v2, :cond_5

    .line 155
    check-cast v0, Lcom/android/calendar/agenda/bs;

    move-object v1, p2

    .line 159
    :goto_0
    if-nez v0, :cond_0

    .line 162
    new-instance v1, Lcom/android/calendar/agenda/bs;

    invoke-direct {v1}, Lcom/android/calendar/agenda/bs;-><init>()V

    .line 164
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->g:Landroid/view/LayoutInflater;

    const v2, 0x7f040013

    invoke-virtual {v0, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 166
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 167
    const v0, 0x7f12002d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/agenda/bs;->a:Landroid/widget/TextView;

    .line 168
    const v0, 0x7f120037

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/android/calendar/agenda/bs;->b:Landroid/widget/CheckBox;

    move-object v0, v1

    move-object v1, v2

    .line 171
    :cond_0
    iget-object v2, v0, Lcom/android/calendar/agenda/bs;->a:Landroid/widget/TextView;

    .line 172
    iget-object v3, v0, Lcom/android/calendar/agenda/bs;->b:Landroid/widget/CheckBox;

    .line 174
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 177
    iget-object v0, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/String;

    .line 178
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/br;->h:Ljava/lang/String;

    .line 181
    :cond_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    sget-boolean v0, Lcom/android/calendar/agenda/br;->d:Z

    if-eqz v0, :cond_3

    .line 184
    sget-object v0, Lcom/android/calendar/agenda/br;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getView position = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/br;->b(I)Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/br;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 188
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 193
    :goto_1
    return-object v1

    .line 190
    :cond_4
    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto/16 :goto_0
.end method
