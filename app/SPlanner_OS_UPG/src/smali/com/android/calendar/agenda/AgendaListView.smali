.class public Lcom/android/calendar/agenda/AgendaListView;
.super Lcom/sec/android/touchwiz/widget/TwExpandableListView;
.source "AgendaListView.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;


# instance fields
.field private a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

.field private b:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;

.field private c:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;

.field private d:Landroid/content/Context;

.field private e:Landroid/view/ActionMode;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I

.field private final l:Ljava/lang/Runnable;

.field private final m:Ljava/lang/Runnable;

.field private final n:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-direct {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaListView;->h:Z

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaListView;->i:Z

    .line 80
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaListView;->j:Z

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/agenda/AgendaListView;->k:I

    .line 84
    new-instance v0, Lcom/android/calendar/agenda/ap;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/ap;-><init>(Lcom/android/calendar/agenda/AgendaListView;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->l:Ljava/lang/Runnable;

    .line 98
    new-instance v0, Lcom/android/calendar/agenda/aq;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/aq;-><init>(Lcom/android/calendar/agenda/AgendaListView;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->m:Ljava/lang/Runnable;

    .line 108
    new-instance v0, Lcom/android/calendar/agenda/ar;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/ar;-><init>(Lcom/android/calendar/agenda/AgendaListView;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->n:Ljava/lang/Runnable;

    .line 121
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/content/Context;)V

    .line 122
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaListView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaListView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->f:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->d:Landroid/content/Context;

    .line 126
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->l:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->f:Ljava/lang/String;

    .line 127
    invoke-virtual {p0, p0}, Lcom/android/calendar/agenda/AgendaListView;->setOnItemClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;)V

    .line 128
    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaListView;->setItemsCanFocus(Z)V

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->setCacheColorHint(I)V

    .line 131
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->d:Landroid/content/Context;

    const v1, 0x7f0a0007

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaListView;->h:Z

    .line 132
    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaListView;->setDividerHeight(I)V

    .line 133
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->setIndicatorGravity(I)V

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/android/calendar/agenda/AgendaListView;->setIndicatorPaddings(II)V

    .line 139
    new-instance v0, Lcom/android/calendar/agenda/as;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/as;-><init>(Lcom/android/calendar/agenda/AgendaListView;)V

    invoke-super {p0, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setOnChildClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;)V

    .line 196
    new-instance v0, Lcom/android/calendar/agenda/at;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/at;-><init>(Lcom/android/calendar/agenda/AgendaListView;)V

    invoke-super {p0, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setOnGroupClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;)V

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaListView;->i:Z

    .line 238
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaListView;->j:Z

    .line 240
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaListView;Z)Z
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaListView;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/agenda/AgendaListView;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->e()V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/agenda/AgendaListView;Z)Z
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaListView;->j:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/agenda/AgendaListView;)Z
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->i()Z

    move-result v0

    return v0
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 706
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleView()Landroid/view/View;

    move-result-object v0

    .line 708
    if-eqz v0, :cond_2

    .line 709
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 710
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 713
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 714
    add-int v2, v0, p1

    iget v0, v1, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_1

    iget v0, v1, Landroid/graphics/Rect;->top:I

    neg-int v0, v0

    :goto_0
    invoke-virtual {p0, v2, v0}, Lcom/android/calendar/agenda/AgendaListView;->setSelectionFromTop(II)V

    .line 733
    :cond_0
    :goto_1
    return-void

    .line 714
    :cond_1
    iget v0, v1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 727
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedItemPosition()I

    move-result v0

    if-ltz v0, :cond_0

    .line 731
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedItemPosition()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->setSelection(I)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/android/calendar/agenda/AgendaListView;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->g()V

    return-void
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 321
    iget v0, p0, Lcom/android/calendar/agenda/AgendaListView;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/agenda/AgendaListView;->k:I

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/agenda/AgendaListView;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->e:Landroid/view/ActionMode;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 327
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->f()V

    .line 330
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 331
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaListView;->f:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 332
    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 333
    const v0, 0x15180

    iget v1, v2, Landroid/text/format/Time;->hour:I

    mul-int/lit16 v1, v1, 0xe10

    sub-int/2addr v0, v1

    iget v1, v2, Landroid/text/format/Time;->minute:I

    mul-int/lit8 v1, v1, 0x3c

    sub-int/2addr v0, v1

    iget v1, v2, Landroid/text/format/Time;->second:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    .line 335
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaListView;->m:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 336
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 341
    if-eqz v0, :cond_0

    .line 342
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->m:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 343
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->m:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v1

    .line 344
    iget v1, v1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 346
    :cond_0
    return-void
.end method

.method private g()V
    .locals 8

    .prologue
    const-wide/32 v6, 0x493e0

    .line 351
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->h()V

    .line 353
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 354
    div-long v2, v0, v6

    mul-long/2addr v2, v6

    .line 355
    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaListView;->n:Ljava/lang/Runnable;

    sub-long/2addr v0, v2

    sub-long v0, v6, v0

    invoke-virtual {p0, v4, v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 356
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/agenda/AgendaListView;)Z
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->d()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/android/calendar/agenda/AgendaListView;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/android/calendar/agenda/AgendaListView;->k:I

    return v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 361
    if-eqz v0, :cond_0

    .line 362
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->n:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 363
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->n:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v1

    .line 364
    iget v1, v1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 366
    :cond_0
    return-void
.end method

.method private i()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 372
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getChildCount()I

    move-result v4

    .line 374
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 375
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaListView;->f:Ljava/lang/String;

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 376
    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 377
    iget-wide v8, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v5

    move v3, v2

    .line 380
    :goto_0
    if-ge v3, v4, :cond_4

    .line 381
    invoke-virtual {p0, v3}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 382
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 383
    instance-of v8, v0, Lcom/android/calendar/agenda/v;

    if-eqz v8, :cond_0

    .line 385
    check-cast v0, Lcom/android/calendar/agenda/v;

    .line 387
    iget v0, v0, Lcom/android/calendar/agenda/v;->b:I

    if-gt v0, v5, :cond_3

    move v0, v1

    .line 403
    :goto_1
    return v0

    .line 391
    :cond_0
    instance-of v8, v0, Lcom/android/calendar/agenda/i;

    if-eqz v8, :cond_3

    .line 394
    check-cast v0, Lcom/android/calendar/agenda/i;

    .line 396
    iget-boolean v8, v0, Lcom/android/calendar/agenda/i;->o:Z

    if-nez v8, :cond_1

    iget-wide v8, v0, Lcom/android/calendar/agenda/i;->m:J

    cmp-long v8, v8, v6

    if-lez v8, :cond_2

    :cond_1
    iget-boolean v8, v0, Lcom/android/calendar/agenda/i;->o:Z

    if-eqz v8, :cond_3

    iget v0, v0, Lcom/android/calendar/agenda/i;->p:I

    if-gt v0, v5, :cond_3

    :cond_2
    move v0, v1

    .line 398
    goto :goto_1

    .line 380
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method static synthetic i(Lcom/android/calendar/agenda/AgendaListView;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaListView;->h:Z

    return v0
.end method

.method static synthetic j(Lcom/android/calendar/agenda/AgendaListView;)Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->c:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/agenda/AgendaListView;)Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    .line 578
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(I)Lcom/android/calendar/agenda/m;

    move-result-object v0

    .line 580
    if-eqz v0, :cond_0

    .line 581
    iget-object v1, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    iget v0, v0, Lcom/android/calendar/agenda/m;->f:I

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/r;->b(I)I

    move-result v0

    .line 583
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->l:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 757
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p()V

    .line 758
    return-void
.end method

.method public declared-synchronized a(II)V
    .locals 2

    .prologue
    .line 258
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->setExpandableListAnimationEnabled(Z)V

    move v0, p1

    .line 259
    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 260
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->expandGroup(I)Z

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->setExpandableListAnimationEnabled(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    monitor-exit p0

    return-void

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 487
    iput-object p6, p0, Lcom/android/calendar/agenda/AgendaListView;->g:Ljava/lang/String;

    .line 488
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    move-object v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 489
    return-void
.end method

.method public a(Landroid/text/format/Time;JLjava/lang/String;Z)V
    .locals 8

    .prologue
    const/4 v2, -0x1

    .line 483
    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move-wide v4, p2

    move-object v6, p4

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 484
    return-void
.end method

.method public a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V
    .locals 19

    .prologue
    .line 426
    if-nez p1, :cond_0

    .line 454
    :goto_0
    return-void

    .line 430
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 432
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v3, :cond_1

    .line 433
    const-wide/16 v4, 0x2

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    const-wide/16 v8, -0x1

    const-wide/16 v10, -0x1

    const/4 v12, -0x2

    const/4 v13, -0x2

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    const/16 v18, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v18}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJZ)V

    goto :goto_0

    .line 436
    :cond_1
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    .line 437
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    .line 438
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 439
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 440
    new-instance v4, Lcom/android/calendar/vcal/a/a/e;

    invoke-direct {v4}, Lcom/android/calendar/vcal/a/a/e;-><init>()V

    .line 442
    :try_start_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/android/calendar/vcal/a/a/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    :goto_1
    invoke-virtual {v4, v10, v11}, Lcom/android/calendar/vcal/a/a/e;->a(J)J

    move-result-wide v10

    .line 451
    :cond_2
    :goto_2
    const-wide/16 v4, 0x2

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    const/4 v12, -0x2

    const/4 v13, -0x2

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_0

    .line 443
    :catch_0
    move-exception v3

    .line 444
    invoke-virtual {v3}, Lcom/android/calendar/vcal/a/a/d;->printStackTrace()V

    goto :goto_1

    :cond_3
    move-wide v10, v4

    goto :goto_2
.end method

.method public a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;Z)V
    .locals 18

    .prologue
    .line 457
    if-nez p1, :cond_0

    .line 480
    :goto_0
    return-void

    .line 460
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 462
    if-nez p2, :cond_2

    .line 463
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v3, :cond_1

    .line 464
    const-wide/16 v4, 0x8

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 467
    :cond_1
    const-wide/16 v4, 0x8

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v14, 0x1

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_0

    .line 471
    :cond_2
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v3, :cond_3

    .line 472
    const-wide/32 v4, 0x200000

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 476
    :cond_3
    const-wide/32 v4, 0x200000

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v14, 0x1

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 248
    if-eqz p1, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->expandAll()V

    .line 253
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaListView;->i:Z

    .line 254
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaListView;->j:Z

    .line 255
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->getGroupCount()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/agenda/AgendaListView;->a(II)V

    goto :goto_0
.end method

.method public a(Landroid/text/format/Time;J)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 588
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 621
    :cond_0
    :goto_0
    return v0

    .line 591
    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 593
    if-eqz v2, :cond_0

    .line 597
    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    .line 598
    invoke-virtual {p1, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 599
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getChildCount()I

    move-result v6

    .line 600
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a()I

    move-result v7

    move v2, v0

    .line 602
    :goto_1
    if-ge v2, v6, :cond_0

    .line 603
    add-int v8, v2, v3

    if-ge v8, v7, :cond_0

    .line 607
    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    add-int v9, v2, v3

    invoke-virtual {v8, v9}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v8

    .line 609
    if-nez v8, :cond_3

    .line 602
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 613
    :cond_3
    iget-wide v10, v8, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    cmp-long v9, v10, p2

    if-nez v9, :cond_2

    iget-wide v8, v8, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    cmp-long v8, v8, v4

    if-nez v8, :cond_2

    .line 614
    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 616
    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getHeight()I

    move-result v10

    if-gt v9, v10, :cond_2

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v8

    if-ltz v8, :cond_2

    move v0, v1

    .line 617
    goto :goto_0
.end method

.method public b(II)I
    .locals 2

    .prologue
    .line 507
    invoke-static {p1, p2}, Lcom/android/calendar/agenda/AgendaListView;->getPackedPositionForChild(II)J

    move-result-wide v0

    .line 508
    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getFlatListPosition(J)I

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 761
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->f()V

    .line 762
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->h()V

    .line 763
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->l:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 764
    return-void
.end method

.method public b(Z)V
    .locals 6

    .prologue
    .line 492
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    const/4 v1, 0x0

    const-wide/16 v2, -0x1

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaListView;->g:Ljava/lang/String;

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/text/format/Time;JLjava/lang/String;Z)V

    .line 493
    return-void
.end method

.method public b(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 625
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 627
    if-nez v1, :cond_1

    .line 638
    :cond_0
    :goto_0
    return v0

    .line 631
    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 632
    sub-int v1, p1, v1

    .line 633
    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 635
    if-eqz v1, :cond_0

    .line 638
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getHeight()I

    move-result v3

    if-gt v2, v3, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d()V

    .line 768
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 693
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaListView;->d(I)V

    .line 694
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedItemPosition()I

    move-result v0

    .line 696
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 697
    add-int/2addr v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setSelectionFromTop(II)V

    .line 699
    :cond_0
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 742
    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->dispatchDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    :goto_0
    return-void

    .line 743
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getAllCollapsed()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaListView;->j:Z

    return v0
.end method

.method public getAllExpanded()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaListView;->i:Z

    return v0
.end method

.method public getChoiceLimit()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/android/calendar/agenda/AgendaListView;->k:I

    return v0
.end method

.method public bridge synthetic getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getExpandableListAdapter()Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getExpandableListAdapter()Lcom/android/calendar/agenda/AgendaBaseAdapter;
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 519
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleView()Landroid/view/View;

    move-result-object v0

    .line 521
    if-eqz v0, :cond_0

    .line 522
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 524
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFirstVisibleTime()J
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 557
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisiblePosition()I

    move-result v0

    .line 562
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v1, v0, v4}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZ)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_0

    .line 565
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaListView;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 566
    iget-wide v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 567
    iget v0, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->f:I

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 572
    invoke-virtual {v1, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 574
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getFirstVisibleView()Landroid/view/View;
    .locals 5

    .prologue
    .line 528
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 529
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getChildCount()I

    move-result v3

    .line 531
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 532
    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 533
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 534
    iget v4, v2, Landroid/graphics/Rect;->top:I

    if-ltz v4, :cond_0

    .line 538
    :goto_1
    return-object v0

    .line 531
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 538
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSelectedInstanceId()J
    .locals 2

    .prologue
    .line 679
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedTime()J
    .locals 2

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedItemPosition()I

    move-result v0

    .line 543
    if-ltz v0, :cond_0

    .line 544
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v0

    .line 545
    if-eqz v0, :cond_0

    .line 546
    iget-wide v0, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    .line 549
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getSelectedViewHolder()Lcom/android/calendar/agenda/i;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->q()Lcom/android/calendar/agenda/i;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 408
    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->e:Landroid/view/ActionMode;

    if-eqz v0, :cond_1

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, p3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v0

    .line 414
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r()J

    move-result-wide v2

    .line 415
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v1, p2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/view/View;)V

    .line 418
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    instance-of v1, v1, Lcom/android/calendar/agenda/bb;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaListView;->h:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 422
    :cond_2
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 772
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->setHeaderDividersEnabled(Z)V

    .line 773
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 775
    if-eqz v0, :cond_0

    .line 776
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 777
    if-eqz v0, :cond_0

    .line 781
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 786
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 787
    if-eqz v0, :cond_0

    .line 788
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 789
    if-eqz v0, :cond_0

    .line 793
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setActionMode(Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 687
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->e:Landroid/view/ActionMode;

    .line 688
    return-void
.end method

.method public setAdapter(Lcom/android/calendar/agenda/AgendaBaseAdapter;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    .line 244
    invoke-super {p0, p1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 245
    return-void
.end method

.method public setChoiceLimit(I)V
    .locals 0

    .prologue
    .line 308
    iput p1, p0, Lcom/android/calendar/agenda/AgendaListView;->k:I

    .line 309
    return-void
.end method

.method public setHideDeclinedEvents(Z)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->c(Z)V

    .line 295
    :cond_0
    return-void
.end method

.method public setHideRecurrentEvents(Z)V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d(Z)V

    .line 301
    :cond_0
    return-void
.end method

.method public setOnChildClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;)V
    .locals 0

    .prologue
    .line 803
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->c:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;

    .line 804
    return-void
.end method

.method public setOnGroupClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;)V
    .locals 0

    .prologue
    .line 798
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;

    .line 799
    return-void
.end method

.method public setSelectedInstanceId(J)V
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->a:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(J)V

    .line 684
    return-void
.end method
