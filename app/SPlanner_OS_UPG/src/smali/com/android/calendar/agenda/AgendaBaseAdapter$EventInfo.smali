.class public Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;
.super Ljava/lang/Object;
.source "AgendaBaseAdapter.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:J

.field public b:J

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 579
    new-instance v0, Lcom/android/calendar/agenda/n;

    invoke-direct {v0}, Lcom/android/calendar/agenda/n;-><init>()V

    sput-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    .line 482
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    .line 483
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    .line 486
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    .line 482
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    .line 483
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    .line 502
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a(Landroid/os/Parcel;)V

    .line 503
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 565
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    .line 566
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    .line 567
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    .line 568
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    .line 569
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    .line 570
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->f:I

    .line 571
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    .line 572
    const/4 v0, 0x3

    new-array v0, v0, [Z

    .line 573
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 574
    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    .line 575
    const/4 v1, 0x1

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    .line 576
    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    .line 577
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 549
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 507
    if-ne p0, p1, :cond_1

    .line 515
    :cond_0
    :goto_0
    return v0

    .line 510
    :cond_1
    instance-of v2, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 511
    goto :goto_0

    .line 513
    :cond_2
    check-cast p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 515
    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    iget-wide v4, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    iget-wide v4, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    iget-wide v4, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    iget-wide v4, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->f:I

    iget v3, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->f:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    iget-boolean v3, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    iget-boolean v3, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    iget-boolean v3, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v8, 0x20

    const/4 v1, 0x0

    .line 529
    .line 533
    iget-wide v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    iget-wide v6, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    add-int/lit16 v0, v0, 0x20f

    .line 534
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    iget-wide v6, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 535
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 536
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    iget-wide v6, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 537
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    iget-wide v6, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 538
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->f:I

    add-int/2addr v0, v3

    .line 539
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 540
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    .line 541
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    .line 542
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    if-eqz v3, :cond_4

    :goto_4
    add-int/2addr v0, v2

    .line 544
    return v0

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 539
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 540
    goto :goto_2

    :cond_3
    move v0, v1

    .line 541
    goto :goto_3

    :cond_4
    move v2, v1

    .line 542
    goto :goto_4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 554
    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 555
    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 556
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 557
    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 558
    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 559
    iget v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 560
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 561
    const/4 v0, 0x3

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 562
    return-void
.end method
