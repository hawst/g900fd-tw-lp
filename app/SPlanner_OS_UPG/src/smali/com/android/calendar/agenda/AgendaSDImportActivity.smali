.class public Lcom/android/calendar/agenda/AgendaSDImportActivity;
.super Lcom/android/calendar/agenda/a;
.source "AgendaSDImportActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field public static final b:[Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Z


# instance fields
.field private e:I

.field private f:Landroid/app/Activity;

.field private g:Landroid/widget/ListView;

.field private h:Lcom/android/calendar/agenda/br;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/CheckBox;

.field private k:Landroid/view/ActionMode;

.field private l:Lcom/android/calendar/agenda/bp;

.field private m:Landroid/widget/LinearLayout;

.field private final n:Ljava/util/ArrayList;

.field private final o:Lcom/android/calendar/agenda/bq;

.field private final p:Landroid/os/Handler;

.field private q:Z

.field private r:I

.field private s:Ljava/util/ArrayList;

.field private t:Ljava/util/ArrayList;

.field private u:Lcom/android/calendar/vcal/u;

.field private v:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    const-class v0, Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->c:Ljava/lang/String;

    .line 76
    const-string v0, "ro.debuggable"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d:Z

    .line 89
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "dtstart"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "rdate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->b:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 76
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lcom/android/calendar/agenda/a;-><init>()V

    .line 87
    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f:Landroid/app/Activity;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    .line 122
    new-instance v0, Lcom/android/calendar/agenda/bq;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/agenda/bq;-><init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;Lcom/android/calendar/agenda/bj;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->o:Lcom/android/calendar/agenda/bq;

    .line 124
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->p:Landroid/os/Handler;

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->q:Z

    .line 689
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->k:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Lcom/android/calendar/agenda/bp;)Lcom/android/calendar/agenda/bp;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->l:Lcom/android/calendar/agenda/bp;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->s:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 344
    iget v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->r:I

    if-ne v0, p1, :cond_0

    .line 373
    :goto_0
    return-void

    .line 348
    :cond_0
    iput p1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->r:I

    .line 349
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 351
    :pswitch_0
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->e()V

    .line 352
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 354
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 355
    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->setProgressBarIndeterminateVisibility(Z)V

    goto :goto_0

    .line 359
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 362
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->setProgressBarIndeterminateVisibility(Z)V

    goto :goto_0

    .line 366
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 369
    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->setProgressBarIndeterminateVisibility(Z)V

    goto :goto_0

    .line 349
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 208
    invoke-static {}, Lcom/android/calendar/gj;->a()Lcom/android/calendar/gj;

    move-result-object v0

    .line 210
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/calendar/agenda/bk;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/calendar/agenda/bk;-><init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;Lcom/android/calendar/gj;Landroid/os/Bundle;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 335
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDImportActivity;I)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->q:Z

    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->u:Lcom/android/calendar/vcal/u;

    invoke-virtual {v0, p1}, Lcom/android/calendar/vcal/u;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->v:Ljava/util/ArrayList;

    .line 340
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->v:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->j:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/agenda/AgendaSDImportActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->t:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic c(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->k:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d:Z

    return v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f:Landroid/app/Activity;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    const v1, 0x7f1200de

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 377
    const v1, 0x7f0f02d2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 378
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/agenda/AgendaSDImportActivity;)I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->e:I

    return v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->l:Lcom/android/calendar/agenda/bp;

    if-nez v0, :cond_0

    .line 397
    new-instance v0, Lcom/android/calendar/agenda/bp;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/agenda/bp;-><init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->l:Lcom/android/calendar/agenda/bp;

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->l:Lcom/android/calendar/agenda/bp;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->k:Landroid/view/ActionMode;

    .line 400
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->t:Ljava/util/ArrayList;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    const v1, 0x7f020132

    const/4 v0, 0x1

    .line 682
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    .line 683
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_1

    .line 684
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f:Landroid/app/Activity;

    invoke-static {v2}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v2

    .line 685
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    if-eqz v2, :cond_2

    const v0, 0x7f020133

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 687
    :cond_0
    return-void

    .line 683
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 685
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method static synthetic h(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->v:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/vcal/u;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->u:Lcom/android/calendar/vcal/u;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->p:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/bq;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->o:Lcom/android/calendar/agenda/bq;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->q:Z

    return v0
.end method


# virtual methods
.method public a(Ljava/io/File;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 628
    .line 632
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 633
    :try_start_1
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 634
    :try_start_2
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 636
    :try_start_3
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 639
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 640
    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_0

    .line 647
    :catch_0
    move-exception v1

    .line 648
    :goto_1
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 653
    if-eqz v4, :cond_0

    .line 654
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 656
    :cond_0
    if-eqz v3, :cond_1

    .line 657
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 659
    :cond_1
    if-eqz v2, :cond_2

    .line 660
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 666
    :cond_2
    :goto_2
    return-object v0

    .line 643
    :cond_3
    :try_start_6
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 644
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 645
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 646
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result-object v0

    .line 653
    if-eqz v4, :cond_4

    .line 654
    :try_start_7
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 656
    :cond_4
    if-eqz v3, :cond_5

    .line 657
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 659
    :cond_5
    if-eqz v2, :cond_2

    .line 660
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 662
    :catch_1
    move-exception v1

    .line 663
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 662
    :catch_2
    move-exception v1

    .line 663
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 649
    :catch_3
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    .line 650
    :goto_3
    :try_start_8
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 653
    if-eqz v4, :cond_6

    .line 654
    :try_start_9
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 656
    :cond_6
    if-eqz v3, :cond_7

    .line 657
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 659
    :cond_7
    if-eqz v2, :cond_2

    .line 660
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto :goto_2

    .line 662
    :catch_4
    move-exception v1

    .line 663
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 652
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    move-object v0, v1

    .line 653
    :goto_4
    if-eqz v4, :cond_8

    .line 654
    :try_start_a
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 656
    :cond_8
    if-eqz v3, :cond_9

    .line 657
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 659
    :cond_9
    if-eqz v2, :cond_a

    .line 660
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 664
    :cond_a
    :goto_5
    throw v0

    .line 662
    :catch_5
    move-exception v1

    .line 663
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 652
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    goto :goto_4

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_4

    :catchall_3
    move-exception v0

    goto :goto_4

    .line 649
    :catch_6
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    goto :goto_3

    :catch_7
    move-exception v1

    move-object v2, v0

    goto :goto_3

    :catch_8
    move-exception v1

    goto :goto_3

    .line 647
    :catch_9
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    goto/16 :goto_1

    :catch_a
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    goto/16 :goto_1

    :catch_b
    move-exception v1

    move-object v2, v0

    goto/16 :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 672
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 609
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 611
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->b()Ljava/util/HashSet;

    move-result-object v1

    .line 612
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 613
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->getCount()I

    move-result v2

    .line 615
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 616
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 617
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 615
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 622
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 623
    const/16 v0, 0x3ed

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->showDialog(I)V

    .line 625
    :cond_2
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 482
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 498
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 484
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 485
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->finish()V

    goto :goto_0

    .line 491
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 482
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 676
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 678
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g()V

    .line 679
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 146
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onCreate(Landroid/os/Bundle;)V

    .line 148
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 151
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 152
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 155
    :cond_0
    const v0, 0x7f040012

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->setContentView(I)V

    .line 157
    iput-object p0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f:Landroid/app/Activity;

    .line 159
    new-instance v0, Lcom/android/calendar/vcal/u;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->u:Lcom/android/calendar/vcal/u;

    .line 161
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 162
    const-string v1, "sd_import_path"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->e:I

    .line 164
    const v0, 0x7f120046

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    .line 166
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 167
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 168
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 169
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 171
    new-instance v0, Lcom/android/calendar/agenda/br;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->t:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/agenda/br;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    .line 173
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 175
    const v0, 0x7f1200dc

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    .line 177
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 178
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 179
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 181
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m:Landroid/widget/LinearLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 183
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g()V

    .line 185
    new-instance v0, Lcom/android/calendar/agenda/bj;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/bj;-><init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;)V

    .line 195
    const v1, 0x7f120218

    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i:Landroid/view/View;

    .line 196
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i:Landroid/view/View;

    const v1, 0x7f120054

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->j:Landroid/widget/CheckBox;

    .line 199
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->j:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/br;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 201
    invoke-direct {p0, v4}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(I)V

    .line 202
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Landroid/os/Bundle;)V

    .line 204
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f()V

    .line 205
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 503
    .line 505
    sparse-switch p1, :sswitch_data_0

    .line 539
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 507
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 509
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 510
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f01d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 511
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 512
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 513
    new-instance v1, Lcom/android/calendar/agenda/bn;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/bn;-><init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 523
    iput-boolean v4, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->q:Z

    goto :goto_0

    .line 527
    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f02d2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f01c1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f009e

    new-instance v2, Lcom/android/calendar/agenda/bo;

    invoke-direct {v2, p0}, Lcom/android/calendar/agenda/bo;-><init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 505
    :sswitch_data_0
    .sparse-switch
        0x3ed -> :sswitch_0
        0x3f2 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->k:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->k:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 392
    :cond_0
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onDestroy()V

    .line 393
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 465
    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-eqz v0, :cond_1

    .line 466
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v0, p3}, Lcom/android/calendar/agenda/br;->b(I)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p3, v0}, Lcom/android/calendar/agenda/br;->a(IZ)V

    .line 467
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->notifyDataSetChanged()V

    .line 468
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->k:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->k:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->j:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/br;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 474
    :cond_1
    return-void

    .line 466
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->o:Lcom/android/calendar/agenda/bq;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 455
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onPause()V

    .line 456
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 545
    invoke-super {p0, p1, p2}, Lcom/android/calendar/agenda/a;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 547
    packed-switch p1, :pswitch_data_0

    .line 555
    :goto_0
    return-void

    .line 549
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->o:Lcom/android/calendar/agenda/bq;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 547
    nop

    :pswitch_data_0
    .packed-switch 0x3ed
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 439
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 441
    const-string v0, "event_ids_to_sd_import"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    .line 442
    if-eqz v1, :cond_0

    array-length v0, v1

    if-lez v0, :cond_0

    .line 443
    array-length v2, v1

    .line 444
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 445
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    aget-wide v4, v1, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    sget-object v3, Lcom/android/calendar/agenda/AgendaSDImportActivity;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onRestoreInstanceState "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-wide v6, v1, v0

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 449
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 382
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onResume()V

    .line 384
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->o:Lcom/android/calendar/agenda/bq;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 385
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 404
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 406
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [J

    .line 408
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 409
    :goto_0
    if-ge v2, v4, :cond_0

    .line 410
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v2

    .line 409
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 412
    :cond_0
    const-string v0, "event_ids_to_sd_import"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    if-eqz v0, :cond_5

    .line 416
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 417
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->b()Ljava/util/HashSet;

    move-result-object v3

    .line 418
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 419
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h:Lcom/android/calendar/agenda/br;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->getCount()I

    move-result v4

    move v0, v1

    .line 421
    :goto_1
    if-ge v0, v4, :cond_3

    .line 422
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 423
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 428
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [I

    .line 429
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 430
    :goto_2
    if-ge v1, v4, :cond_4

    .line 431
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 430
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 433
    :cond_4
    const-string v0, "key_selected_events"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 435
    :cond_5
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 460
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onStop()V

    .line 461
    return-void
.end method
