.class Lcom/android/calendar/agenda/as;
.super Ljava/lang/Object;
.source "AgendaListView.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaListView;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaListView;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Lcom/sec/android/touchwiz/widget/TwExpandableListView;Landroid/view/View;IIJ)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 142
    .line 145
    const-wide/16 v0, -0x1

    cmp-long v0, p5, v0

    if-eqz v0, :cond_2

    .line 146
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->e(Lcom/android/calendar/agenda/AgendaListView;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 147
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, p3, p4}, Lcom/android/calendar/agenda/AgendaListView;->b(II)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaListView;->getHeaderViewsCount()I

    move-result v1

    sub-int/2addr v0, v1

    .line 148
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h(I)I

    move-result v4

    .line 149
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i(I)Z

    move-result v5

    .line 150
    const v0, 0x7f120037

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 151
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaListView;)Landroid/content/Context;

    move-result-object v1

    const-string v6, "accessibility"

    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 153
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 155
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->g(Lcom/android/calendar/agenda/AgendaListView;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v5, :cond_9

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    if-nez v5, :cond_4

    move v0, v2

    :goto_0
    invoke-virtual {v1, v4, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(IZ)V

    .line 157
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 158
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->e(Lcom/android/calendar/agenda/AgendaListView;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    move v0, v2

    .line 162
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x()I

    move-result v1

    iget-object v4, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaListView;->h(Lcom/android/calendar/agenda/AgendaListView;)I

    move-result v4

    if-ne v1, v4, :cond_1

    if-nez v5, :cond_1

    .line 163
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaListView;)Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f0f0038

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v5}, Lcom/android/calendar/agenda/AgendaListView;->h(Lcom/android/calendar/agenda/AgendaListView;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 164
    iget-object v2, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v2}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaListView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_1
    move v3, v0

    .line 189
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->j(Lcom/android/calendar/agenda/AgendaListView;)Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->j(Lcom/android/calendar/agenda/AgendaListView;)Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-interface/range {v1 .. v7}, Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;->onChildClick(Lcom/sec/android/touchwiz/widget/TwExpandableListView;Landroid/view/View;IIJ)Z

    move-result v3

    .line 192
    :cond_3
    return v3

    :cond_4
    move v0, v3

    .line 156
    goto :goto_0

    .line 169
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(II)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r()J

    move-result-wide v4

    .line 171
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/view/View;)V

    .line 173
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    instance-of v1, v1, Lcom/android/calendar/agenda/bb;

    if-nez v1, :cond_8

    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->i(Lcom/android/calendar/agenda/AgendaListView;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 177
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    iget-wide v4, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    invoke-virtual {v1, v4, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(J)V

    .line 178
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaListView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    if-eqz v1, :cond_7

    .line 179
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v1, v0, v3}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;Z)V

    goto :goto_2

    .line 181
    :cond_7
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V

    goto :goto_2

    .line 183
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaListView;)Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Lcom/android/calendar/agenda/AgendaFestivalListActivity;

    if-eqz v1, :cond_2

    .line 184
    iget-object v1, p0, Lcom/android/calendar/agenda/as;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V

    goto/16 :goto_2

    :cond_9
    move v0, v3

    goto/16 :goto_1
.end method
