.class Lcom/android/calendar/agenda/bq;
.super Ljava/lang/Object;
.source "AgendaSDImportActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Z

.field final synthetic b:Lcom/android/calendar/agenda/AgendaSDImportActivity;


# direct methods
.method private constructor <init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;)V
    .locals 0

    .prologue
    .line 557
    iput-object p1, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;Lcom/android/calendar/agenda/bj;)V
    .locals 0

    .prologue
    .line 557
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/bq;-><init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 562
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bq;->a:Z

    if-eqz v0, :cond_1

    .line 564
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->finish()V

    .line 565
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0, v5}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Z)Z

    .line 604
    :cond_0
    :goto_0
    return-void

    .line 571
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->l(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 572
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->l(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 574
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 575
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SDImportRunnable sd_importd id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    :cond_2
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v3

    long-to-int v0, v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 579
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 580
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->e(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f01e7

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 582
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->finish()V

    goto :goto_0

    .line 584
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 586
    iget-object v1, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v1, v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Ljava/lang/String;)Z

    .line 587
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/vcal/u;->b()Z

    .line 589
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->l(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 591
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->l(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 592
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->j(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 594
    :cond_4
    iput-boolean v6, p0, Lcom/android/calendar/agenda/bq;->a:Z

    .line 595
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->j(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 599
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/bq;->b:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->m(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    iput-boolean v6, p0, Lcom/android/calendar/agenda/bq;->a:Z

    goto/16 :goto_0
.end method
