.class Lcom/android/calendar/agenda/h;
.super Ljava/lang/Object;
.source "AgendaAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/b;

.field private b:Landroid/widget/HoverPopupWindow;


# direct methods
.method private constructor <init>(Lcom/android/calendar/agenda/b;)V
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/android/calendar/agenda/h;->a:Lcom/android/calendar/agenda/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/agenda/b;Lcom/android/calendar/agenda/c;)V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/h;-><init>(Lcom/android/calendar/agenda/b;)V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 616
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 671
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 619
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->b(Lcom/android/calendar/agenda/b;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 620
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f04000e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 621
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Landroid/view/View;->setHoverPopupType(I)V

    .line 623
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    .line 624
    iget-object v2, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v1, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0, v3}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 625
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    const/16 v1, 0x3031

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 626
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 627
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 628
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    goto :goto_0

    .line 632
    :pswitch_2
    const v0, 0x7f12002d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 633
    const v1, 0x7f12003c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 635
    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :cond_1
    iget-object v2, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    if-eqz v2, :cond_0

    .line 639
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/widget/TextView;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, Lcom/android/calendar/hj;->a(Landroid/widget/TextView;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 640
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v2}, Landroid/widget/HoverPopupWindow;->getContent()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 641
    if-eqz v2, :cond_4

    .line 642
    const v3, 0x7f120034

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 643
    const v4, 0x7f120035

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 645
    if-eqz v3, :cond_3

    .line 646
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 647
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 648
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 649
    iget-object v3, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 652
    :cond_3
    if-eqz v2, :cond_4

    .line 653
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 654
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 655
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 656
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 660
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 661
    sget-object v1, Lcom/android/calendar/hj;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/agenda/h;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 664
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/h;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->dismiss()V

    goto/16 :goto_0

    .line 616
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
