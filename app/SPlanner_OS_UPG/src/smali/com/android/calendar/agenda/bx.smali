.class Lcom/android/calendar/agenda/bx;
.super Ljava/lang/Object;
.source "DeleteEventFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/bw;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/bw;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    if-nez v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->b(Lcom/android/calendar/agenda/bw;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 146
    :goto_1
    if-ge v3, v4, :cond_3

    .line 147
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->b(Lcom/android/calendar/agenda/bw;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 148
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/bb;->g(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 149
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v6

    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/bb;->i(I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v6, v5, v0}, Lcom/android/calendar/agenda/bb;->b(IZ)V

    .line 146
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 149
    goto :goto_2

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->b(Lcom/android/calendar/agenda/bw;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 153
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->notifyDataSetChanged()V

    .line 154
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->c(Lcom/android/calendar/agenda/bw;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    goto :goto_0
.end method

.method public onTwMultiSelected(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJZZZ)V
    .locals 3

    .prologue
    .line 126
    .line 127
    iget-object v0, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/calendar/agenda/bb;->h(I)I

    move-result v0

    .line 128
    iget-object v1, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v1}, Lcom/android/calendar/agenda/bw;->b(Lcom/android/calendar/agenda/bw;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v1}, Lcom/android/calendar/agenda/bw;->b(Lcom/android/calendar/agenda/bw;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/bx;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v1}, Lcom/android/calendar/agenda/bw;->b(Lcom/android/calendar/agenda/bw;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
