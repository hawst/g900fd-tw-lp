.class final Lcom/android/calendar/agenda/bp;
.super Lcom/android/calendar/common/a/a;
.source "AgendaSDImportActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaSDImportActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 691
    iput-object p1, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    .line 692
    invoke-direct {p0, p2}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;)V

    .line 693
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->a()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 735
    const v0, 0x7f1200d5

    const v1, 0x7f0f0398

    invoke-interface {p2, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 736
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 702
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/br;->a(Z)V

    .line 703
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 707
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/br;->a(Z)V

    .line 708
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->notifyDataSetChanged()V

    .line 726
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 712
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->c()Z

    move-result v0

    return v0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bp;->f()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 717
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bp;->a()I

    move-result v0

    .line 718
    iget-object v3, p0, Lcom/android/calendar/agenda/bp;->c:Landroid/view/Menu;

    const v4, 0x7f1200d5

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 719
    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 720
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/bp;->b(Z)V

    .line 721
    return-void

    :cond_0
    move v0, v2

    .line 719
    goto :goto_0

    :cond_1
    move v1, v2

    .line 720
    goto :goto_1
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 740
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1200d5

    if-ne v0, v1, :cond_0

    .line 741
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->b()V

    .line 742
    const/4 v0, 0x1

    .line 744
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 749
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 750
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 761
    invoke-super {p0, p1}, Lcom/android/calendar/common/a/a;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 762
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 763
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Lcom/android/calendar/agenda/bp;)Lcom/android/calendar/agenda/bp;

    .line 764
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/br;->a(Z)V

    .line 765
    iget-object v0, p0, Lcom/android/calendar/agenda/bp;->a:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/agenda/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/br;->notifyDataSetChanged()V

    .line 766
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 755
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 756
    const/4 v0, 0x1

    return v0
.end method
