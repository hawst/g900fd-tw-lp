.class Lcom/android/calendar/agenda/d;
.super Ljava/lang/Object;
.source "AgendaAdapter.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/android/calendar/agenda/b;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/b;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/android/calendar/agenda/d;->b:Lcom/android/calendar/agenda/b;

    iput-object p2, p0, Lcom/android/calendar/agenda/d;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    const v2, 0x7f120043

    .line 190
    iget-object v0, p0, Lcom/android/calendar/agenda/d;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 191
    iget-object v0, p0, Lcom/android/calendar/agenda/d;->b:Lcom/android/calendar/agenda/b;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;Z)Z

    .line 192
    iget-object v0, p0, Lcom/android/calendar/agenda/d;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 193
    iget-object v1, p0, Lcom/android/calendar/agenda/d;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/agenda/b;->b(I)I

    .line 194
    invoke-static {}, Lcom/android/calendar/agenda/b;->a()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 195
    iget-object v1, p0, Lcom/android/calendar/agenda/d;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 196
    return-void
.end method
