.class Lcom/android/calendar/agenda/e;
.super Ljava/lang/Object;
.source "AgendaAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Landroid/widget/TextView;

.field final synthetic c:Lcom/android/calendar/agenda/b;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/b;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/android/calendar/agenda/e;->c:Lcom/android/calendar/agenda/b;

    iput-object p2, p0, Lcom/android/calendar/agenda/e;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/android/calendar/agenda/e;->b:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const v3, 0x7f0b001b

    .line 314
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 349
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-static {p1}, Lcom/android/calendar/dz;->a(Landroid/widget/CompoundButton;)Z

    move-result v0

    .line 318
    if-eqz p2, :cond_2

    .line 320
    iget-object v1, p0, Lcom/android/calendar/agenda/e;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/agenda/e;->c:Lcom/android/calendar/agenda/b;

    invoke-static {v2}, Lcom/android/calendar/agenda/b;->b(Lcom/android/calendar/agenda/b;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 321
    iget-object v1, p0, Lcom/android/calendar/agenda/e;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/agenda/e;->c:Lcom/android/calendar/agenda/b;

    invoke-static {v2}, Lcom/android/calendar/agenda/b;->b(Lcom/android/calendar/agenda/b;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 322
    if-nez v0, :cond_1

    .line 323
    iget-object v0, p0, Lcom/android/calendar/agenda/e;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/agenda/e;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 332
    :cond_1
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/calendar/agenda/f;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/calendar/agenda/f;-><init>(Lcom/android/calendar/agenda/e;Landroid/widget/CompoundButton;Z)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 326
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/agenda/e;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/agenda/e;->c:Lcom/android/calendar/agenda/b;

    invoke-static {v2}, Lcom/android/calendar/agenda/b;->b(Lcom/android/calendar/agenda/b;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0138

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 327
    iget-object v1, p0, Lcom/android/calendar/agenda/e;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/agenda/e;->c:Lcom/android/calendar/agenda/b;

    invoke-static {v2}, Lcom/android/calendar/agenda/b;->b(Lcom/android/calendar/agenda/b;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0136

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 328
    if-nez v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/android/calendar/agenda/e;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/agenda/e;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    and-int/lit8 v1, v1, -0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_1
.end method
