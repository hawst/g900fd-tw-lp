.class Lcom/android/calendar/agenda/bk;
.super Ljava/lang/Object;
.source "AgendaSDImportActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/gj;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/android/calendar/agenda/AgendaSDImportActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaSDImportActivity;Lcom/android/calendar/gj;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    iput-object p2, p0, Lcom/android/calendar/agenda/bk;->a:Lcom/android/calendar/gj;

    iput-object p3, p0, Lcom/android/calendar/agenda/bk;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 214
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 215
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 222
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->a:Lcom/android/calendar/gj;

    if-eqz v0, :cond_4

    .line 223
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    iget-object v1, p0, Lcom/android/calendar/agenda/bk;->a:Lcom/android/calendar/gj;

    iget-object v2, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v2}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->e(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/app/Activity;

    move-result-object v2

    iget-object v4, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->f(Lcom/android/calendar/agenda/AgendaSDImportActivity;)I

    move-result v4

    invoke-virtual {v1, v2, v4}, Lcom/android/calendar/gj;->a(Landroid/app/Activity;I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 230
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 231
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 232
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 242
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 243
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_6

    if-lez v2, :cond_6

    move v1, v3

    .line 244
    :goto_3
    if-ge v1, v2, :cond_6

    .line 246
    :try_start_0
    new-instance v4, Ljava/io/File;

    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-virtual {v0, v4}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 253
    if-nez v0, :cond_7

    .line 254
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d()Ljava/lang/String;

    move-result-object v0

    const-string v4, "mData is null so continue"

    invoke-static {v0, v4}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    move v1, v2

    .line 244
    :goto_4
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    .line 219
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 225
    :cond_4
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SDImportExportUtils.getInstance() returns null"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 235
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->b(Lcom/android/calendar/agenda/AgendaSDImportActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    goto :goto_2

    .line 247
    :catch_0
    move-exception v0

    .line 248
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 297
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 298
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->j(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/agenda/bl;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/bl;-><init>(Lcom/android/calendar/agenda/bk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 333
    :goto_6
    return-void

    .line 258
    :cond_7
    iget-object v4, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v4, v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->a(Lcom/android/calendar/agenda/AgendaSDImportActivity;Ljava/lang/String;)Z

    .line 259
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->h(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_8

    .line 261
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 266
    add-int/lit8 v2, v2, -0x1

    .line 267
    add-int/lit8 v0, v1, -0x1

    .line 268
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->d()Ljava/lang/String;

    move-result-object v1

    const-string v4, "mContentValues is null so continue"

    invoke-static {v1, v4}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 269
    goto :goto_4

    .line 262
    :catch_1
    move-exception v0

    .line 263
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_5

    .line 272
    :cond_8
    new-instance v0, Lcom/android/calendar/dh;

    invoke-direct {v0}, Lcom/android/calendar/dh;-><init>()V

    .line 273
    int-to-long v4, v1

    iput-wide v4, v0, Lcom/android/calendar/dh;->b:J

    .line 274
    iget-object v4, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/calendar/vcal/u;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    .line 276
    iget-object v4, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/calendar/vcal/u;->c(I)Ljava/lang/Long;

    move-result-object v4

    .line 277
    if-nez v4, :cond_9

    .line 278
    iput-wide v6, v0, Lcom/android/calendar/dh;->m:J

    .line 283
    :goto_7
    iget-object v4, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/calendar/vcal/u;->d(I)Ljava/lang/Long;

    move-result-object v4

    .line 284
    if-nez v4, :cond_a

    .line 285
    iput-wide v6, v0, Lcom/android/calendar/dh;->n:J

    .line 290
    :goto_8
    iget-object v4, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->i(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/calendar/vcal/u;->e(I)Z

    move-result v4

    iput-boolean v4, v0, Lcom/android/calendar/dh;->f:Z

    .line 292
    iget-object v4, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->g(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    move v1, v2

    goto/16 :goto_4

    .line 280
    :cond_9
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/calendar/dh;->m:J

    goto :goto_7

    .line 287
    :cond_a
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/calendar/dh;->n:J

    goto :goto_8

    .line 326
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/agenda/bk;->c:Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDImportActivity;->j(Lcom/android/calendar/agenda/AgendaSDImportActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/agenda/bm;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/bm;-><init>(Lcom/android/calendar/agenda/bk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_6
.end method
