.class Lcom/android/calendar/agenda/cc;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "PinchGestureLayout.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/PinchGestureLayout;


# direct methods
.method private constructor <init>(Lcom/android/calendar/agenda/PinchGestureLayout;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/calendar/agenda/cc;->a:Lcom/android/calendar/agenda/PinchGestureLayout;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/agenda/PinchGestureLayout;Lcom/android/calendar/agenda/cb;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/cc;-><init>(Lcom/android/calendar/agenda/PinchGestureLayout;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 86
    iget-object v0, p0, Lcom/android/calendar/agenda/cc;->a:Lcom/android/calendar/agenda/PinchGestureLayout;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/PinchGestureLayout;->a(Lcom/android/calendar/agenda/PinchGestureLayout;Z)Z

    .line 87
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 76
    iget-object v1, p0, Lcom/android/calendar/agenda/cc;->a:Lcom/android/calendar/agenda/PinchGestureLayout;

    invoke-static {v1, v0}, Lcom/android/calendar/agenda/PinchGestureLayout;->a(Lcom/android/calendar/agenda/PinchGestureLayout;Z)Z

    .line 77
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    .line 79
    iget-object v2, p0, Lcom/android/calendar/agenda/cc;->a:Lcom/android/calendar/agenda/PinchGestureLayout;

    invoke-static {v2}, Lcom/android/calendar/agenda/PinchGestureLayout;->a(Lcom/android/calendar/agenda/PinchGestureLayout;)F

    move-result v2

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/agenda/cc;->a:Lcom/android/calendar/agenda/PinchGestureLayout;

    invoke-static {v2}, Lcom/android/calendar/agenda/PinchGestureLayout;->b(Lcom/android/calendar/agenda/PinchGestureLayout;)F

    move-result v2

    cmpg-float v2, v1, v2

    if-gez v2, :cond_2

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/agenda/cc;->a:Lcom/android/calendar/agenda/PinchGestureLayout;

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v2, v0}, Lcom/android/calendar/agenda/PinchGestureLayout;->b(Lcom/android/calendar/agenda/PinchGestureLayout;Z)V

    .line 82
    :cond_2
    return-void
.end method
