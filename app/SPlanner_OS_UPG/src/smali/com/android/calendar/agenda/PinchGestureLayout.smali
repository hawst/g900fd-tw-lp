.class public Lcom/android/calendar/agenda/PinchGestureLayout;
.super Landroid/widget/FrameLayout;
.source "PinchGestureLayout.java"


# instance fields
.field private a:Z

.field private b:F

.field private c:F

.field private d:Landroid/view/ScaleGestureDetector;

.field private e:Landroid/app/Activity;

.field private f:Lcom/android/calendar/agenda/AgendaListView;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/agenda/PinchGestureLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/calendar/agenda/PinchGestureLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    iput-boolean v1, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->a:Z

    .line 37
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->b:F

    .line 38
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->c:F

    .line 45
    iput-boolean v1, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->g:Z

    .line 49
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/PinchGestureLayout;->a(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/PinchGestureLayout;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->c:F

    return v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/high16 v3, 0x42c80000    # 100.0f

    .line 65
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->e:Landroid/app/Activity;

    .line 66
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/android/calendar/agenda/cc;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/agenda/cc;-><init>(Lcom/android/calendar/agenda/PinchGestureLayout;Lcom/android/calendar/agenda/cb;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->d:Landroid/view/ScaleGestureDetector;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    const v1, 0x7f0d0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    iput v1, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->b:F

    .line 69
    const v1, 0x7f0d0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v3

    iput v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->c:F

    .line 70
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->f:Lcom/android/calendar/agenda/AgendaListView;

    if-nez v0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    if-eqz p1, :cond_2

    .line 108
    iget-object v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->f:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->expandAll()V

    goto :goto_0

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->f:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->collapseAll()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/PinchGestureLayout;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/agenda/PinchGestureLayout;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->b:F

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/agenda/PinchGestureLayout;Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/PinchGestureLayout;->a(Z)V

    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->e:Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 129
    :cond_0
    :goto_0
    return v0

    .line 120
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    .line 121
    iput-boolean v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->g:Z

    .line 124
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->d:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 125
    iget-boolean v1, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->g:Z

    if-eqz v1, :cond_0

    .line 126
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->e:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->d:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 136
    iget-boolean v0, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->g:Z

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setListView(Lcom/android/calendar/agenda/AgendaListView;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/android/calendar/agenda/PinchGestureLayout;->f:Lcom/android/calendar/agenda/AgendaListView;

    .line 62
    return-void
.end method
