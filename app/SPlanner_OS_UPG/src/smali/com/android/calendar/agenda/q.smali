.class public Lcom/android/calendar/agenda/q;
.super Ljava/lang/Object;
.source "AgendaBaseAdapter.java"


# instance fields
.field a:J

.field b:Landroid/text/format/Time;

.field c:I

.field d:I

.field e:Ljava/lang/String;

.field f:I

.field g:I

.field h:J

.field i:J

.field j:Lcom/android/calendar/agenda/w;


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416
    iput p1, p0, Lcom/android/calendar/agenda/q;->f:I

    .line 417
    iput p2, p0, Lcom/android/calendar/agenda/q;->g:I

    .line 418
    new-instance v0, Lcom/android/calendar/agenda/w;

    invoke-direct {v0}, Lcom/android/calendar/agenda/w;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    .line 419
    return-void
.end method

.method public constructor <init>(IILandroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 422
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/agenda/q;-><init>(II)V

    .line 425
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 447
    if-ne p0, p1, :cond_1

    .line 469
    :cond_0
    :goto_0
    return v0

    .line 449
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 450
    goto :goto_0

    .line 451
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 452
    goto :goto_0

    .line 453
    :cond_3
    check-cast p1, Lcom/android/calendar/agenda/q;

    .line 454
    iget v2, p0, Lcom/android/calendar/agenda/q;->d:I

    iget v3, p1, Lcom/android/calendar/agenda/q;->d:I

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lcom/android/calendar/agenda/q;->a:J

    iget-wide v4, p1, Lcom/android/calendar/agenda/q;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget v2, p0, Lcom/android/calendar/agenda/q;->f:I

    iget v3, p1, Lcom/android/calendar/agenda/q;->f:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/android/calendar/agenda/q;->c:I

    iget v3, p1, Lcom/android/calendar/agenda/q;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 457
    goto :goto_0

    .line 460
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    if-eqz v2, :cond_6

    .line 461
    iget-object v2, p0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p1, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    invoke-virtual {v4, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 462
    goto :goto_0

    .line 465
    :cond_6
    iget-object v2, p1, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    if-eqz v2, :cond_0

    move v0, v1

    .line 466
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 429
    .line 431
    iget v0, p0, Lcom/android/calendar/agenda/q;->d:I

    add-int/lit8 v0, v0, 0x1f

    .line 432
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/android/calendar/agenda/q;->a:J

    iget-wide v4, p0, Lcom/android/calendar/agenda/q;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 433
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/android/calendar/agenda/q;->f:I

    add-int/2addr v0, v1

    .line 434
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/android/calendar/agenda/q;->c:I

    add-int/2addr v0, v1

    .line 435
    iget-object v1, p0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 436
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    if-eqz v1, :cond_1

    .line 439
    iget-object v1, p0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 440
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 442
    :cond_1
    return v0
.end method
