.class Lcom/android/calendar/agenda/ay;
.super Ljava/lang/Object;
.source "AgendaPickActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaPickActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/android/calendar/agenda/ay;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 352
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    const/4 p1, 0x0

    .line 354
    iget-object v0, p0, Lcom/android/calendar/agenda/ay;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->e()V

    move-object v6, p1

    .line 356
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/agenda/ay;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0, v6}, Lcom/android/calendar/agenda/AgendaPickActivity;->a(Lcom/android/calendar/agenda/AgendaPickActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 357
    iget-object v0, p0, Lcom/android/calendar/agenda/ay;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->d(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/ay;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->a(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/text/format/Time;

    move-result-object v1

    const v2, 0x24dc87

    const v3, 0x259d23

    const-wide/16 v4, -0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 358
    return v7

    :cond_0
    move-object v6, p1

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 342
    iget-object v0, p0, Lcom/android/calendar/agenda/ay;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 343
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    iget-object v1, p0, Lcom/android/calendar/agenda/ay;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->j(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/widget/SearchView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 347
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
