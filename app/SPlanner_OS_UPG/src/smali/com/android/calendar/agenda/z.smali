.class public Lcom/android/calendar/agenda/z;
.super Landroid/app/Fragment;
.source "AgendaFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;
.implements Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;


# static fields
.field private static F:Landroid/view/animation/Interpolator;

.field private static final I:[I

.field private static final a:Ljava/lang/String;


# instance fields
.field private A:I

.field private B:Ljava/util/ArrayList;

.field private C:Ljava/util/ArrayList;

.field private D:Z

.field private E:Z

.field private G:F

.field private H:Lcom/android/calendar/c/a;

.field private final J:Ljava/lang/Runnable;

.field private final K:Lcom/android/calendar/c/d;

.field private b:Lcom/android/calendar/agenda/AgendaListView;

.field private c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

.field private d:Lcom/android/calendar/agenda/PinchGestureLayout;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/CheckBox;

.field private g:Landroid/view/ActionMode;

.field private h:Lcom/android/calendar/agenda/aj;

.field private i:Lcom/android/calendar/cj;

.field private j:Lcom/android/calendar/cz;

.field private k:Lcom/android/calendar/gr;

.field private l:Landroid/app/Activity;

.field private m:Lcom/android/calendar/al;

.field private n:Lcom/android/calendar/detail/EventInfoFragment;

.field private final o:Landroid/text/format/Time;

.field private p:Ljava/lang/String;

.field private final q:J

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Lcom/android/calendar/aq;

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/android/calendar/agenda/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/z;->a:Ljava/lang/String;

    .line 142
    new-instance v0, Landroid/view/animation/interpolator/SineInOut70;

    invoke-direct {v0}, Landroid/view/animation/interpolator/SineInOut70;-><init>()V

    sput-object v0, Lcom/android/calendar/agenda/z;->F:Landroid/view/animation/Interpolator;

    .line 156
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/calendar/agenda/z;->I:[I

    return-void

    :array_0
    .array-data 4
        0x7
        0x1
        0x2
        0x4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 199
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/agenda/z;-><init>(JZ)V

    .line 200
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 204
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 125
    iput-boolean v1, p0, Lcom/android/calendar/agenda/z;->v:Z

    .line 127
    iput-boolean v1, p0, Lcom/android/calendar/agenda/z;->w:Z

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->x:Lcom/android/calendar/aq;

    .line 131
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/agenda/z;->y:I

    .line 134
    iput v1, p0, Lcom/android/calendar/agenda/z;->z:I

    .line 135
    iput v1, p0, Lcom/android/calendar/agenda/z;->A:I

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->B:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->C:Ljava/util/ArrayList;

    .line 139
    iput-boolean v1, p0, Lcom/android/calendar/agenda/z;->D:Z

    .line 141
    iput-boolean v1, p0, Lcom/android/calendar/agenda/z;->E:Z

    .line 143
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/agenda/z;->G:F

    .line 163
    new-instance v0, Lcom/android/calendar/agenda/aa;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/aa;-><init>(Lcom/android/calendar/agenda/z;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->J:Ljava/lang/Runnable;

    .line 177
    new-instance v0, Lcom/android/calendar/agenda/ab;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/ab;-><init>(Lcom/android/calendar/agenda/z;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->K:Lcom/android/calendar/c/d;

    .line 205
    iput-wide p1, p0, Lcom/android/calendar/agenda/z;->q:J

    .line 206
    iput-boolean p3, p0, Lcom/android/calendar/agenda/z;->v:Z

    .line 208
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    .line 209
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    monitor-enter v1

    .line 210
    :try_start_0
    iget-wide v2, p0, Lcom/android/calendar/agenda/z;->q:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 215
    :goto_0
    monitor-exit v1

    .line 216
    return-void

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/android/calendar/agenda/z;->q:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto :goto_0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 948
    if-ltz p0, :cond_0

    sget-object v0, Lcom/android/calendar/agenda/z;->I:[I

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 949
    :cond_0
    const/4 v0, -0x1

    .line 951
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/android/calendar/agenda/z;->I:[I

    aget v0, v0, p0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/calendar/agenda/z;->g:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;Lcom/android/calendar/c/a;)Lcom/android/calendar/c/a;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/calendar/agenda/z;->H:Lcom/android/calendar/c/a;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/calendar/agenda/z;->p:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/app/Fragment;)V
    .locals 2

    .prologue
    .line 618
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 619
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 620
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 621
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 744
    if-eqz p1, :cond_0

    .line 745
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 746
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    .line 748
    :cond_0
    return-void
.end method

.method private a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V
    .locals 18

    .prologue
    .line 677
    if-nez p1, :cond_0

    .line 687
    :goto_0
    return-void

    .line 680
    :cond_0
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v2, :cond_1

    .line 681
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    const-wide/16 v4, 0x8

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 684
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    const-wide/16 v4, 0x8

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v14, 0x1

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_0
.end method

.method private a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;Ljava/lang/Runnable;)V
    .locals 10

    .prologue
    .line 703
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/z;->i:Lcom/android/calendar/cj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/z;->j:Lcom/android/calendar/cz;

    if-nez v0, :cond_1

    .line 710
    :cond_0
    :goto_0
    return-void

    .line 706
    :cond_1
    iget-boolean v0, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v0, :cond_2

    .line 707
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->j:Lcom/android/calendar/cz;

    iget-wide v2, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    invoke-virtual {v0, v2, v3, p2}, Lcom/android/calendar/cz;->a(JLjava/lang/Runnable;)V

    goto :goto_0

    .line 709
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->i:Lcom/android/calendar/cj;

    iget-wide v2, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    iget-wide v4, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    iget-wide v6, p1, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    const/4 v8, -0x1

    move-object v9, p2

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/cj;->a(JJJILjava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;I)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/z;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;[Ljava/lang/Long;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/agenda/z;->a([Ljava/lang/Long;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;[Ljava/lang/Long;ZLjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/agenda/z;->a([Ljava/lang/Long;ZLjava/lang/Runnable;)V

    return-void
.end method

.method private a(Lcom/android/calendar/aq;Z)V
    .locals 14

    .prologue
    .line 526
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    if-nez v0, :cond_0

    .line 529
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    monitor-enter v1

    .line 530
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    iget-object v2, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 531
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 532
    monitor-exit v1

    .line 557
    :goto_0
    return-void

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 537
    :cond_0
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->after(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 539
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    monitor-enter v1

    .line 540
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    iget-object v2, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 541
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 542
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 545
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    iget-wide v2, p1, Lcom/android/calendar/aq;->c:J

    iget-object v4, p0, Lcom/android/calendar/agenda/z;->r:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;JLjava/lang/String;Z)V

    .line 547
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 548
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 549
    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 550
    const-wide/16 v2, 0x400

    iget-object v4, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x24

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 555
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedViewHolder()Lcom/android/calendar/agenda/i;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_4

    iget-boolean v0, v0, Lcom/android/calendar/agenda/i;->o:Z

    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/android/calendar/agenda/z;->b(Lcom/android/calendar/aq;Z)V

    goto :goto_0

    .line 542
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 556
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1005
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1006
    const-string v1, "com.sec.android.intent.CHANGE_SHARE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1007
    const-string v1, "key"

    const-string v2, "preferences_list_by"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1008
    const-string v1, "value"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1009
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1010
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/text/format/Time;)V
    .locals 6

    .prologue
    .line 560
    iput-object p1, p0, Lcom/android/calendar/agenda/z;->r:Ljava/lang/String;

    .line 561
    if-eqz p2, :cond_0

    .line 562
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    monitor-enter v1

    .line 563
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    invoke-virtual {v0, p2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 564
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    if-nez v0, :cond_1

    .line 571
    :goto_0
    return-void

    .line 564
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 570
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    const-wide/16 v2, -0x1

    iget-object v4, p0, Lcom/android/calendar/agenda/z;->r:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;JLjava/lang/String;Z)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 1013
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->E:Z

    if-nez v0, :cond_0

    .line 1014
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->j()V

    .line 1016
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->E:Z

    if-eqz v0, :cond_1

    .line 1017
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->k()V

    .line 1019
    :cond_1
    return-void
.end method

.method private a([Ljava/lang/Long;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 720
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/z;->k:Lcom/android/calendar/gr;

    if-nez v0, :cond_1

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 723
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    .line 724
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "enterprise_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 725
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/RestrictionPolicy;->isShareListAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 728
    array-length v0, p1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 729
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h(Z)Ljava/util/HashSet;

    move-result-object v1

    .line 731
    const/4 v0, 0x0

    .line 732
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 733
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 735
    :cond_2
    if-eqz v0, :cond_3

    .line 736
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->k:Lcom/android/calendar/gr;

    iget-wide v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    invoke-virtual {v1, v2, v3}, Lcom/android/calendar/gr;->c(J)V

    .line 740
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->k:Lcom/android/calendar/gr;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/gr;->a([Ljava/lang/Long;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private a([Ljava/lang/Long;ZLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 713
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/z;->i:Lcom/android/calendar/cj;

    if-nez v0, :cond_1

    .line 717
    :cond_0
    :goto_0
    return-void

    .line 716
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->i:Lcom/android/calendar/cj;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;ZLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/z;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/android/calendar/agenda/z;->E:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/agenda/z;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->p:Ljava/lang/String;

    return-object v0
.end method

.method private b(I)V
    .locals 8

    .prologue
    const/4 v3, -0x1

    .line 955
    iput p1, p0, Lcom/android/calendar/agenda/z;->z:I

    .line 956
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    invoke-static {p1}, Lcom/android/calendar/agenda/z;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->b(I)V

    .line 958
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i()I

    move-result v2

    .line 959
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->j()I

    move-result v0

    .line 960
    const v1, 0x24dc87

    if-ne v2, v1, :cond_0

    const v1, 0x259d23

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 964
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e()V

    .line 965
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/android/calendar/agenda/z;->r:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 966
    return-void

    :cond_0
    move v3, v0

    goto :goto_0
.end method

.method private b(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V
    .locals 18

    .prologue
    .line 690
    if-nez p1, :cond_0

    .line 700
    :goto_0
    return-void

    .line 693
    :cond_0
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v2, :cond_1

    .line 694
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    const-wide/32 v4, 0x200000

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 697
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    const-wide/32 v4, 0x200000

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v14, 0x1

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/agenda/z;I)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/z;->c(I)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/agenda/z;Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/z;->b(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/agenda/z;Z)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/z;->a(Z)V

    return-void
.end method

.method private b(Lcom/android/calendar/aq;Z)V
    .locals 13

    .prologue
    const v12, 0x7f120030

    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 641
    iget-wide v0, p1, Lcom/android/calendar/aq;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 642
    sget-object v0, Lcom/android/calendar/agenda/z;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showEventInfo, event ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/android/calendar/aq;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->t:Z

    if-eqz v0, :cond_0

    .line 648
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 649
    if-nez v0, :cond_2

    .line 652
    iput-object p1, p0, Lcom/android/calendar/agenda/z;->x:Lcom/android/calendar/aq;

    .line 653
    iput-boolean p2, p0, Lcom/android/calendar/agenda/z;->w:Z

    goto :goto_0

    .line 656
    :cond_2
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v11

    .line 658
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 659
    :cond_3
    iget-wide v0, p1, Lcom/android/calendar/aq;->p:J

    long-to-int v8, v0

    .line 662
    :goto_1
    if-eqz p2, :cond_4

    .line 663
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 664
    iget-object v0, p1, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 667
    :cond_4
    new-instance v0, Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    iget-wide v2, p1, Lcom/android/calendar/aq;->c:J

    iget-object v4, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v4, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-object v6, p1, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v6, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-direct/range {v0 .. v10}, Lcom/android/calendar/detail/EventInfoFragment;-><init>(Landroid/content/Context;JJJIZI)V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->n:Lcom/android/calendar/detail/EventInfoFragment;

    .line 670
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->n:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v11, v12, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 671
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->n:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0, v12, v1}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 672
    invoke-virtual {v11}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    :cond_5
    move v8, v9

    goto :goto_1
.end method

.method static synthetic b()[I
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/android/calendar/agenda/z;->I:[I

    return-object v0
.end method

.method static synthetic c()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/android/calendar/agenda/z;->F:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 969
    iget v0, p0, Lcom/android/calendar/agenda/z;->A:I

    if-eq p1, v0, :cond_1

    .line 970
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/z;->d(I)V

    .line 972
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz v0, :cond_0

    .line 973
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->b(Z)V

    .line 975
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/z;->a(Ljava/lang/String;)V

    .line 977
    :cond_1
    return-void
.end method

.method static synthetic d(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/c/a;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->H:Lcom/android/calendar/c/a;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 624
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 632
    :goto_0
    return-void

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->h:Lcom/android/calendar/agenda/aj;

    if-nez v0, :cond_1

    .line 629
    new-instance v0, Lcom/android/calendar/agenda/aj;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x12c

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/agenda/aj;-><init>(Lcom/android/calendar/agenda/z;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->h:Lcom/android/calendar/agenda/aj;

    .line 631
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->h:Lcom/android/calendar/agenda/aj;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->g:Landroid/view/ActionMode;

    goto :goto_0
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 980
    iput p1, p0, Lcom/android/calendar/agenda/z;->A:I

    .line 981
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    if-eqz v0, :cond_0

    .line 982
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    iget v1, p0, Lcom/android/calendar/agenda/z;->A:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e(I)V

    .line 985
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 986
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 987
    const-string v1, "preferences_list_by"

    iget v2, p0, Lcom/android/calendar/agenda/z;->A:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 988
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 989
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->D()V

    .line 636
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/agenda/z;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->g:Landroid/view/ActionMode;

    return-object v0
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 881
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 882
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/android/calendar/AllInOneActivity;

    if-nez v0, :cond_1

    .line 892
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, v1

    .line 885
    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 886
    invoke-static {v1}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 887
    :goto_1
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 889
    invoke-virtual {v1, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v6, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 890
    iget-object v2, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v4, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisiblePosition()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/android/calendar/agenda/AgendaListView;->a(I)I

    move-result v2

    .line 891
    if-eq v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Z)V

    goto :goto_0

    .line 886
    :cond_2
    new-instance v2, Landroid/text/format/Time;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_1

    :cond_3
    move v1, v3

    .line 891
    goto :goto_2
.end method

.method static synthetic g(Lcom/android/calendar/agenda/z;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->d()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->B:Ljava/util/ArrayList;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x7

    .line 992
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    const-string v1, "preferences_filter"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/z;->z:I

    .line 993
    iget v0, p0, Lcom/android/calendar/agenda/z;->z:I

    if-ne v0, v2, :cond_0

    .line 994
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/agenda/z;->z:I

    .line 996
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->C:Ljava/util/ArrayList;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 999
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    const-string v1, "preferences_list_by"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/z;->A:I

    .line 1001
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    iget v1, p0, Lcom/android/calendar/agenda/z;->A:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e(I)V

    .line 1002
    return-void
.end method

.method static synthetic j(Lcom/android/calendar/agenda/z;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->f:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1022
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/android/calendar/agenda/af;

    invoke-direct {v2, p0}, Lcom/android/calendar/agenda/af;-><init>(Lcom/android/calendar/agenda/z;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1065
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getChildCount()I

    move-result v2

    move v0, v1

    .line 1066
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1067
    iget-object v3, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v3, v0}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/android/calendar/agenda/z;->G:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 1066
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1070
    :cond_0
    iget-object v3, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v3, v0}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f120037

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1071
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1072
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 1074
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->invalidate()V

    .line 1075
    return-void
.end method

.method static synthetic k(Lcom/android/calendar/agenda/z;)F
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/android/calendar/agenda/z;->G:F

    return v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/agenda/ah;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/ah;-><init>(Lcom/android/calendar/agenda/z;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1111
    return-void
.end method

.method static synthetic l(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/c/d;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->K:Lcom/android/calendar/c/d;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/agenda/z;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic n(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    return-object v0
.end method

.method static synthetic o(Lcom/android/calendar/agenda/z;)Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->e:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 575
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->b(Z)V

    .line 578
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 6

    .prologue
    .line 590
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x20

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 591
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/aq;Z)V

    .line 615
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x100

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 593
    iget-object v0, p1, Lcom/android/calendar/aq;->k:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/agenda/z;->a(Ljava/lang/String;Landroid/text/format/Time;)V

    goto :goto_0

    .line 594
    :cond_2
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide v2, 0x200000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 595
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->g:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    .line 596
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->a()V

    goto :goto_0

    .line 598
    :cond_4
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x1000

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 599
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->d()V

    goto :goto_0

    .line 600
    :cond_5
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide v2, 0x400000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    .line 601
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->h()V

    .line 602
    new-instance v0, Lcom/android/calendar/agenda/al;

    iget v1, p0, Lcom/android/calendar/agenda/z;->z:I

    invoke-direct {v0, v1}, Lcom/android/calendar/agenda/al;-><init>(I)V

    .line 603
    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/z;->a(Landroid/app/Fragment;)V

    goto :goto_0

    .line 604
    :cond_6
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/32 v2, 0x20000

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    .line 605
    iget-wide v0, p1, Lcom/android/calendar/aq;->p:J

    long-to-int v0, v0

    .line 606
    int-to-long v2, v0

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_7

    .line 607
    new-instance v0, Lcom/android/calendar/agenda/an;

    iget v1, p0, Lcom/android/calendar/agenda/z;->A:I

    invoke-direct {v0, v1}, Lcom/android/calendar/agenda/an;-><init>(I)V

    .line 608
    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/z;->a(Landroid/app/Fragment;)V

    goto :goto_0

    .line 610
    :cond_7
    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/z;->c(I)V

    goto :goto_0

    .line 612
    :cond_8
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/32 v2, 0x80000

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 613
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->e()V

    goto :goto_0
.end method

.method public g()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 582
    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->v:Z

    if-eqz v0, :cond_1

    move-wide v0, v2

    :goto_0
    const-wide/16 v4, 0x1000

    or-long/2addr v0, v4

    const-wide v4, 0x200000000L

    or-long/2addr v0, v4

    const-wide/16 v4, 0x80

    or-long/2addr v0, v4

    const-wide v4, 0x400000000L

    or-long/2addr v0, v4

    const-wide/32 v4, 0x20000

    or-long/2addr v0, v4

    const-wide/32 v4, 0x80000

    or-long/2addr v0, v4

    iget-boolean v4, p0, Lcom/android/calendar/agenda/z;->v:Z

    if-eqz v4, :cond_0

    const-wide/16 v2, 0x100

    :cond_0
    or-long/2addr v0, v2

    .line 585
    return-wide v0

    .line 582
    :cond_1
    const-wide/16 v0, 0x20

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 220
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 222
    iput-object p1, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    .line 223
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    new-instance v0, Lcom/android/calendar/cj;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/cj;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->i:Lcom/android/calendar/cj;

    .line 227
    new-instance v0, Lcom/android/calendar/cz;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/cz;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->j:Lcom/android/calendar/cz;

    .line 228
    new-instance v0, Lcom/android/calendar/gr;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/gr;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->k:Lcom/android/calendar/gr;

    .line 230
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/z;->setHasOptionsMenu(Z)V

    .line 232
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->J:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->p:Ljava/lang/String;

    .line 233
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    monitor-enter v1

    .line 234
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/z;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 235
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->x:Lcom/android/calendar/aq;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->x:Lcom/android/calendar/aq;

    iget-boolean v1, p0, Lcom/android/calendar/agenda/z;->w:Z

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/agenda/z;->b(Lcom/android/calendar/aq;Z)V

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->x:Lcom/android/calendar/aq;

    goto :goto_0

    .line 235
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 245
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 246
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    .line 247
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/z;->s:Z

    .line 248
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/z;->D:Z

    .line 249
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    const v1, 0x7f0a0007

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/z;->t:Z

    .line 250
    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->v:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->s:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/agenda/z;->u:Z

    .line 251
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/z;->G:F

    .line 252
    if-eqz p1, :cond_0

    .line 253
    const-string v0, "key_restore_time"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 254
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 255
    iget-object v2, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    monitor-enter v2

    .line 256
    :try_start_0
    iget-object v3, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 257
    monitor-exit v2

    .line 260
    :cond_0
    return-void

    .line 250
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    .line 264
    const v0, 0x7f04000c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 266
    const v0, 0x7f12002b

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaListView;

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    .line 267
    new-instance v0, Lcom/android/calendar/agenda/bt;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v3, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    const v4, 0x7f0a0007

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v3

    iget-boolean v4, p0, Lcom/android/calendar/agenda/z;->v:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendar/agenda/bt;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;ZZ)V

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    .line 268
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setAdapter(Lcom/android/calendar/agenda/AgendaBaseAdapter;)V

    .line 269
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setClickable(Z)V

    .line 270
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, p0}, Lcom/android/calendar/agenda/AgendaListView;->setOnScrollListener(Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;)V

    .line 271
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    new-instance v1, Lcom/android/calendar/agenda/ac;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/ac;-><init>(Lcom/android/calendar/agenda/z;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setOnItemLongClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;)V

    .line 287
    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->D:Z

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    new-instance v1, Lcom/android/calendar/agenda/ad;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/ad;-><init>(Lcom/android/calendar/agenda/z;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setTwMultiSelectedListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;)V

    .line 353
    :cond_0
    const v0, 0x7f12002f

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/PinchGestureLayout;

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->d:Lcom/android/calendar/agenda/PinchGestureLayout;

    .line 354
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->d:Lcom/android/calendar/agenda/PinchGestureLayout;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/PinchGestureLayout;->setListView(Lcom/android/calendar/agenda/AgendaListView;)V

    .line 356
    new-instance v0, Lcom/android/calendar/agenda/ae;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/ae;-><init>(Lcom/android/calendar/agenda/z;)V

    .line 370
    const v1, 0x7f120218

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/z;->e:Landroid/view/View;

    .line 371
    iget-object v1, p0, Lcom/android/calendar/agenda/z;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 372
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->e:Landroid/view/View;

    const v1, 0x7f120054

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/agenda/z;->f:Landroid/widget/CheckBox;

    .line 373
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->f:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->w()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 375
    const-wide/16 v4, -0x1

    .line 376
    const/4 v1, -0x1

    .line 377
    const/4 v0, -0x1

    .line 379
    if-eqz p3, :cond_9

    .line 380
    const-string v2, "key_restore_instance_id"

    const-wide/16 v6, -0x1

    invoke-virtual {p3, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 381
    const-wide/16 v6, -0x1

    cmp-long v6, v2, v6

    if-eqz v6, :cond_1

    .line 382
    iget-object v6, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v6, v2, v3}, Lcom/android/calendar/agenda/AgendaListView;->setSelectedInstanceId(J)V

    .line 385
    :cond_1
    const-string v2, "key_start_day"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "key_end_day"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 386
    const-string v0, "key_start_day"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 387
    const-string v0, "key_end_day"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    move v2, v1

    move v1, v0

    .line 390
    :goto_0
    const-string v0, "key_event_ids_selected"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 391
    const-string v0, "key_event_ids_selected"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    .line 392
    new-instance v7, Ljava/util/HashSet;

    array-length v0, v6

    invoke-direct {v7, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 393
    array-length v9, v6

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v9, :cond_3

    aget-object v0, v6, v3

    .line 394
    instance-of v10, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    if-eqz v10, :cond_2

    .line 395
    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 397
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, v7}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Ljava/util/HashSet;)V

    .line 400
    :cond_4
    const-string v0, "key_selection_mode"

    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 401
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->d()V

    :cond_5
    move v3, v1

    .line 405
    :goto_2
    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->t:Z

    if-nez v0, :cond_6

    .line 406
    const v0, 0x7f120030

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 409
    :cond_6
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_7

    .line 410
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    .line 411
    iget-object v6, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    monitor-enter v6

    .line 412
    :try_start_0
    iget-object v7, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    invoke-virtual {v7, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 413
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/agenda/AgendaListView;->setSelectedInstanceId(J)V

    .line 417
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setHideDeclinedEvents(Z)V

    .line 418
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    new-instance v1, Landroid/text/format/Time;

    iget-object v6, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    invoke-direct {v1, v6}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iget-object v6, p0, Lcom/android/calendar/agenda/z;->r:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 420
    return-object v8

    .line 413
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_8
    move v2, v1

    move v1, v0

    goto/16 :goto_0

    :cond_9
    move v3, v0

    move v2, v1

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->c()V

    .line 458
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->g:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->g:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->k:Lcom/android/calendar/gr;

    if-eqz v0, :cond_1

    .line 462
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->k:Lcom/android/calendar/gr;

    invoke-virtual {v0}, Lcom/android/calendar/gr;->a()V

    .line 464
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 465
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->b()V

    .line 452
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 453
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 799
    const v0, 0x7f12032e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 800
    if-nez v3, :cond_0

    .line 817
    :goto_0
    return-void

    .line 803
    :cond_0
    iget v0, p0, Lcom/android/calendar/agenda/z;->z:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    .line 805
    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 808
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->u()I

    move-result v0

    .line 809
    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 814
    :goto_2
    const v0, 0x7f120329

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_3
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 815
    const v0, 0x7f120324

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a()I

    move-result v3

    if-lez v3, :cond_4

    :goto_4
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 816
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->f()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 809
    goto :goto_1

    .line 811
    :cond_2
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    :cond_3
    move v0, v2

    .line 814
    goto :goto_3

    :cond_4
    move v1, v2

    .line 815
    goto :goto_4
.end method

.method public onResume()V
    .locals 14

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 425
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 426
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->l:Landroid/app/Activity;

    const v1, 0x7f100011

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTheme(I)V

    .line 428
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->h()V

    .line 429
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->i()V

    .line 431
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v9}, Lcom/android/calendar/agenda/AgendaListView;->setChoiceMode(I)V

    .line 432
    iget-boolean v0, p0, Lcom/android/calendar/agenda/z;->D:Z

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/AgendaListView;->setEnableDragBlock(Z)V

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setHideDeclinedEvents(Z)V

    .line 438
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/AgendaListView;->b(Z)V

    .line 440
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->a()V

    .line 442
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 443
    invoke-virtual {p0}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x400

    const-wide/16 v7, -0x1

    const-wide/16 v10, 0x24

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v12, v4

    move-object v13, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 446
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 469
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    if-nez v0, :cond_0

    .line 506
    :goto_0
    return-void

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->m:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    .line 473
    cmp-long v0, v2, v6

    if-lez v0, :cond_1

    .line 474
    iget-object v4, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    monitor-enter v4

    .line 475
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 476
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 477
    const-string v0, "key_restore_time"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 480
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getSelectedInstanceId()J

    move-result-wide v2

    .line 481
    cmp-long v0, v2, v6

    if-ltz v0, :cond_2

    .line 482
    const-string v0, "key_restore_instance_id"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 485
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    if-eqz v0, :cond_3

    .line 486
    const-string v0, "key_start_day"

    iget-object v2, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 487
    const-string v0, "key_end_day"

    iget-object v2, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->j()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 489
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->z()Ljava/util/HashSet;

    move-result-object v0

    .line 490
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 491
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v2

    new-array v2, v2, [Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 492
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 493
    const-string v0, "key_event_ids_selected"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 497
    :cond_3
    const-string v2, "key_selection_mode"

    iget-object v0, p0, Lcom/android/calendar/agenda/z;->g:Landroid/view/ActionMode;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 502
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setChoiceMode(I)V

    .line 503
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setMultiChoiceModeListener(Lcom/sec/android/touchwiz/widget/TwAbsListView$MultiChoiceModeListener;)V

    .line 505
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    .line 476
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move v0, v1

    .line 497
    goto :goto_1
.end method

.method public onScroll(Lcom/sec/android/touchwiz/widget/TwAbsListView;III)V
    .locals 1

    .prologue
    .line 764
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0, p2, p3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d(II)V

    .line 765
    invoke-direct {p0}, Lcom/android/calendar/agenda/z;->f()V

    .line 795
    return-void
.end method

.method public onScrollStateChanged(Lcom/sec/android/touchwiz/widget/TwAbsListView;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 753
    if-nez p2, :cond_0

    .line 754
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->G:Z

    .line 756
    :cond_0
    if-ne p2, v2, :cond_1

    .line 757
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->c:Lcom/android/calendar/agenda/AgendaBaseAdapter;

    iput-boolean v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->G:Z

    .line 758
    iget-object v0, p0, Lcom/android/calendar/agenda/z;->b:Lcom/android/calendar/agenda/AgendaListView;

    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/z;->a(Landroid/view/View;)V

    .line 760
    :cond_1
    return-void
.end method
