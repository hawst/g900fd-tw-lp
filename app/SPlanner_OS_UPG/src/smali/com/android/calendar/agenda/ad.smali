.class Lcom/android/calendar/agenda/ad;
.super Ljava/lang/Object;
.source "AgendaFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/z;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/z;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 307
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 349
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->h(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 312
    :goto_1
    if-ge v3, v4, :cond_4

    .line 313
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->h(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 314
    iget-object v5, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v5}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g(I)I

    move-result v5

    if-ne v5, v1, :cond_1

    .line 315
    iget-object v5, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v5}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v0

    .line 316
    if-nez v0, :cond_2

    .line 312
    :cond_1
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 318
    :cond_2
    iget-object v5, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v5}, Lcom/android/calendar/agenda/z;->i(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 319
    add-int/lit8 v5, v4, -0x1

    if-eq v3, v5, :cond_1

    .line 320
    iget-object v5, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v5}, Lcom/android/calendar/agenda/z;->i(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 323
    :cond_3
    iget-object v5, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v5}, Lcom/android/calendar/agenda/z;->i(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move v3, v2

    .line 328
    :goto_3
    if-ge v3, v4, :cond_8

    .line 329
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->h(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 330
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g(I)I

    move-result v0

    if-ne v0, v1, :cond_5

    .line 331
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v6

    .line 332
    if-nez v6, :cond_6

    .line 328
    :cond_5
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 334
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->i(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 335
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i(I)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    invoke-virtual {v7, v5, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(IZ)V

    .line 336
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->i(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    move v0, v2

    .line 335
    goto :goto_5

    .line 340
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->h(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 341
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->i(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 343
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->f(Lcom/android/calendar/agenda/z;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 344
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 345
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->f(Lcom/android/calendar/agenda/z;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    goto/16 :goto_0

    .line 347
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->g(Lcom/android/calendar/agenda/z;)V

    goto/16 :goto_0
.end method

.method public onTwMultiSelected(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJZZZ)V
    .locals 3

    .prologue
    .line 291
    iget-object v0, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p3, v0

    .line 292
    iget-object v1, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v1}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h(I)I

    move-result v0

    .line 293
    iget-object v1, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v1}, Lcom/android/calendar/agenda/z;->h(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v1}, Lcom/android/calendar/agenda/z;->h(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 298
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/ad;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v1}, Lcom/android/calendar/agenda/z;->h(Lcom/android/calendar/agenda/z;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
