.class public Lcom/android/calendar/agenda/AgendaFestivalListActivity;
.super Lcom/android/calendar/agenda/a;
.source "AgendaFestivalListActivity.java"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:Z


# instance fields
.field private d:J

.field private e:Landroid/app/Activity;

.field private f:Lcom/android/calendar/agenda/bb;

.field private g:Lcom/android/calendar/agenda/AgendaListView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/text/format/Time;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    const-class v2, Lcom/android/calendar/agenda/AgendaFestivalListActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->b:Ljava/lang/String;

    .line 54
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->c:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/calendar/agenda/a;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->e:Landroid/app/Activity;

    .line 72
    new-instance v0, Lcom/android/calendar/agenda/y;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/y;-><init>(Lcom/android/calendar/agenda/AgendaFestivalListActivity;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->k:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaFestivalListActivity;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->j:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaFestivalListActivity;Landroid/text/format/Time;)Landroid/text/format/Time;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->j:Landroid/text/format/Time;

    return-object p1
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 134
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->i:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->i:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 140
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 141
    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->d:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 142
    iget v2, v0, Landroid/text/format/Time;->year:I

    invoke-static {p0, v2}, Lcom/android/calendar/gm;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 143
    iget v0, v0, Landroid/text/format/Time;->month:I

    const/4 v2, 0x3

    invoke-static {v0, v2, v1}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v4

    .line 147
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/hj;->e()[C

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 148
    const v2, 0x7f0f012b

    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 150
    const-string v2, "YMD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    new-instance v2, Landroid/text/SpannableStringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 152
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    .line 158
    :goto_1
    const/16 v3, 0x21

    .line 160
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0367

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 161
    new-instance v6, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v6, v5}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v2, v6, v1, v5, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 163
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0b010d

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 164
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {v2, v5, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 165
    sget-boolean v1, Landroid/graphics/Typeface;->isFlipFontUsed:Z

    if-nez v1, :cond_1

    .line 166
    new-instance v1, Landroid/text/style/TypefaceSpan;

    const-string v5, "sans-serif"

    invoke-direct {v1, v5}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v4, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 154
    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v2, v0

    move v0, v1

    .line 155
    goto :goto_1
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 220
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/android/calendar/agenda/AgendaListView;->setVisibility(I)V

    .line 221
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->h:Landroid/view/View;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 222
    const v0, 0x7f1200dc

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 223
    return-void

    :cond_0
    move v0, v2

    .line 220
    goto :goto_0

    :cond_1
    move v0, v2

    .line 221
    goto :goto_1

    :cond_2
    move v2, v1

    .line 222
    goto :goto_2
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 239
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 255
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 241
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 242
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->finish()V

    goto :goto_0

    .line 248
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 239
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const v2, 0x7f020132

    const/4 v1, 0x1

    .line 227
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 229
    const v0, 0x7f1200dc

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 230
    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v1, :cond_1

    .line 232
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->e:Landroid/app/Activity;

    invoke-static {v3}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v3

    .line 233
    if-eqz v1, :cond_3

    if-eqz v3, :cond_2

    const v1, 0x7f020133

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 235
    :cond_0
    return-void

    .line 231
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 233
    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 83
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-static {}, Lcom/android/calendar/dz;->x()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->finish()V

    .line 89
    :cond_0
    const v0, 0x7f04000a

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->setContentView(I)V

    .line 91
    iput-object p0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->e:Landroid/app/Activity;

    .line 94
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->k:Ljava/lang/Runnable;

    invoke-static {p0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->j:Landroid/text/format/Time;

    .line 96
    cmp-long v0, v2, v2

    if-nez v0, :cond_4

    .line 98
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "beginTime"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 99
    sget-boolean v5, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->c:Z

    if-eqz v5, :cond_1

    .line 100
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 101
    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 102
    sget-object v6, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->b:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Restore value from intent: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_1
    :goto_0
    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 107
    sget-boolean v0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->c:Z

    if-eqz v0, :cond_2

    .line 108
    sget-object v0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->b:Ljava/lang/String;

    const-string v1, "Restored from current time"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 113
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->j:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 115
    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->d:J

    .line 116
    const v0, 0x7f12002b

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaListView;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    .line 117
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v4}, Lcom/android/calendar/agenda/AgendaListView;->setItemsCanFocus(Z)V

    .line 118
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setChoiceMode(I)V

    .line 119
    new-instance v0, Lcom/android/calendar/agenda/bb;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v3, 0x1

    const/4 v5, -0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/agenda/bb;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;IZI)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->f:Lcom/android/calendar/agenda/bb;

    .line 120
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->f:Lcom/android/calendar/agenda/bb;

    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/agenda/bb;->b(J)V

    .line 122
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->f:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setAdapter(Lcom/android/calendar/agenda/AgendaBaseAdapter;)V

    .line 123
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 125
    const v0, 0x7f12002c

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->h:Landroid/view/View;

    .line 126
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->h:Landroid/view/View;

    const v1, 0x7f12002d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->i:Landroid/widget/TextView;

    .line 127
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->b()V

    .line 129
    const v0, 0x7f1200de

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f02d2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 130
    invoke-virtual {p0, v4}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->a(Z)V

    .line 131
    return-void

    :cond_4
    move-wide v0, v2

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 260
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 261
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 263
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 215
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onDestroy()V

    .line 216
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 268
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 273
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 270
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->finish()V

    .line 271
    const/4 v0, 0x1

    goto :goto_0

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 205
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onPause()V

    .line 206
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 200
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 201
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    .line 174
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onResume()V

    .line 176
    sget-boolean v0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->c:Z

    if-eqz v0, :cond_0

    .line 177
    sget-object v0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnResume to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->j:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    const v2, 0x24dc87

    const v3, 0x259d23

    const-wide/16 v4, -0x1

    const-string v6, ""

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 182
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->k:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 183
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 187
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->g:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleTime()J

    move-result-wide v0

    .line 188
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 189
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->j:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 190
    sget-boolean v0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->c:Z

    if-eqz v0, :cond_0

    .line 191
    sget-object v0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;->j:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 196
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 210
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onStop()V

    .line 211
    return-void
.end method
