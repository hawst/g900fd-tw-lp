.class public Lcom/android/calendar/agenda/r;
.super Landroid/widget/BaseAdapter;
.source "AgendaByDayAdapter.java"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/android/calendar/agenda/b;

.field private final e:Ljava/util/ArrayList;

.field private f:Landroid/text/format/Time;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private final k:Ljava/util/ArrayList;

.field private final l:Ljava/util/ArrayList;

.field private m:Z

.field private final n:Z

.field private o:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/android/calendar/agenda/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/r;->b:Ljava/lang/String;

    .line 62
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, " -   "

    :goto_0
    sput-object v0, Lcom/android/calendar/agenda/r;->a:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, " - "

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 106
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/agenda/r;->k:Ljava/util/ArrayList;

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/agenda/r;->l:Ljava/util/ArrayList;

    .line 81
    iput-boolean v0, p0, Lcom/android/calendar/agenda/r;->m:Z

    .line 83
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/android/calendar/agenda/r;->n:Z

    .line 92
    new-instance v0, Lcom/android/calendar/agenda/s;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/s;-><init>(Lcom/android/calendar/agenda/r;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/r;->o:Ljava/lang/Runnable;

    .line 107
    iput-object p1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    .line 108
    iput p2, p0, Lcom/android/calendar/agenda/r;->j:I

    .line 109
    new-instance v0, Lcom/android/calendar/agenda/b;

    const v1, 0x7f04000f

    invoke-direct {v0, p1, v1, p2}, Lcom/android/calendar/agenda/b;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    .line 110
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->o:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/calendar/agenda/r;->h:Ljava/lang/String;

    .line 112
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    .line 113
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/r;->m:Z

    .line 114
    return-void

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/r;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const v4, 0x24dc87

    const v3, 0x7f0f017d

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1120
    const-string v0, ""

    .line 1122
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const-string v2, "preferences_list_by"

    invoke-static {v1, v2, v8}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 1126
    if-nez v1, :cond_1

    .line 1127
    packed-switch p1, :pswitch_data_0

    move-object p2, v0

    .line 1273
    :cond_0
    :goto_0
    return-object p2

    .line 1129
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1132
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1135
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1138
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1141
    :pswitch_4
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1147
    :cond_1
    if-ne v1, v6, :cond_2

    .line 1148
    packed-switch p1, :pswitch_data_1

    move-object p2, v0

    .line 1159
    goto :goto_0

    .line 1150
    :pswitch_5
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0346

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1153
    :pswitch_6
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0348

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1156
    :pswitch_7
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0347

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1161
    :cond_2
    if-ne v1, v7, :cond_7

    .line 1162
    if-ne p1, v6, :cond_3

    .line 1163
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 1164
    :cond_3
    if-ge p1, v4, :cond_4

    .line 1167
    const-string p2, ""

    goto/16 :goto_0

    .line 1173
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/agenda/r;->o:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 1174
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1175
    iput-object v0, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    .line 1176
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    .line 1182
    :cond_5
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/r;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1183
    invoke-virtual {v0, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v0

    .line 1185
    iget-boolean v2, p0, Lcom/android/calendar/agenda/r;->n:Z

    if-eqz v2, :cond_6

    .line 1186
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v0

    .line 1187
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    invoke-virtual {v2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_6

    .line 1188
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7b2

    if-eq v2, v3, :cond_6

    .line 1189
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 1190
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    iput v6, v2, Landroid/text/format/Time;->hour:I

    .line 1191
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    iput-boolean v8, v2, Landroid/text/format/Time;->allDay:Z

    .line 1192
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    invoke-virtual {v2, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 1197
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-static {v0, v1, v2, v7}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 1199
    :cond_7
    const/4 v2, 0x3

    if-ne v1, v2, :cond_c

    .line 1201
    if-ne p1, v6, :cond_8

    .line 1202
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 1203
    :cond_8
    if-ge p1, v4, :cond_9

    .line 1206
    const-string p2, ""

    goto/16 :goto_0

    .line 1212
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/agenda/r;->o:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 1213
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1214
    iput-object v0, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    .line 1215
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    .line 1221
    :cond_a
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    .line 1222
    invoke-virtual {v1, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v2

    invoke-static {v0}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 1223
    add-int/lit8 v4, p1, 0x6

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v4

    invoke-static {v0}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v0

    sub-long v0, v4, v0

    .line 1224
    iget-boolean v4, p0, Lcom/android/calendar/agenda/r;->n:Z

    if-eqz v4, :cond_b

    .line 1225
    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v2

    .line 1226
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v0

    .line 1229
    :cond_b
    iget-object v4, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-static {v2, v3, v4, v7}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 1230
    iget-object v3, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-static {v0, v1, v3, v7}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 1231
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 1234
    :cond_c
    const/4 v2, 0x4

    if-ne v1, v2, :cond_11

    .line 1235
    if-ne p1, v6, :cond_d

    .line 1236
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 1237
    :cond_d
    if-ge p1, v4, :cond_e

    .line 1240
    const-string p2, ""

    goto/16 :goto_0

    .line 1246
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/agenda/r;->o:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    .line 1247
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1248
    iput-object v2, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    .line 1249
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    .line 1254
    :cond_f
    iget-object v3, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    .line 1255
    invoke-virtual {v3, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v0

    invoke-static {v2}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v0, v4

    .line 1256
    iget-boolean v4, p0, Lcom/android/calendar/agenda/r;->n:Z

    if-eqz v4, :cond_10

    .line 1257
    invoke-static {v3, p1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    invoke-static {v2}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 1258
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v0

    .line 1261
    :cond_10
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-static {v0, v1, v2, v6}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 1263
    :cond_11
    const/4 v2, 0x5

    if-ne v1, v2, :cond_13

    .line 1264
    const-string v0, "My task"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1265
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 1266
    :cond_12
    const-string v0, "Default"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1267
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_13
    move-object p2, v0

    goto/16 :goto_0

    .line 1127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 1148
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lcom/android/calendar/agenda/r;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/android/calendar/agenda/r;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/agenda/r;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/android/calendar/agenda/r;->h:Ljava/lang/String;

    return-object p1
.end method

.method private b(Lcom/android/calendar/agenda/m;)V
    .locals 20

    .prologue
    .line 415
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v13

    .line 416
    if-eqz p1, :cond_0

    :try_start_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    if-nez v2, :cond_1

    .line 417
    :cond_0
    monitor-exit v13

    .line 573
    :goto_0
    return-void

    .line 420
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/agenda/r;->j:I

    and-int/lit8 v2, v2, 0x2

    if-lez v2, :cond_5

    const/4 v2, 0x1

    move v12, v2

    .line 421
    :goto_1
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    .line 422
    new-instance v15, Ljava/util/LinkedList;

    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    .line 423
    const/4 v3, -0x1

    .line 424
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 425
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/r;->o:Ljava/lang/Runnable;

    invoke-static {v2, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    .line 426
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    iget-object v5, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 427
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    .line 428
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 430
    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 431
    :try_start_2
    new-instance v16, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 432
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 434
    invoke-virtual {v14}, Lcom/android/calendar/agenda/w;->b()V

    .line 435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 436
    const/4 v2, 0x0

    move v11, v2

    move v2, v3

    :goto_2
    invoke-virtual {v14}, Lcom/android/calendar/agenda/w;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 437
    invoke-virtual {v14}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x3

    invoke-virtual {v14, v3}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    move v10, v3

    .line 441
    :goto_3
    if-eqz v12, :cond_7

    if-eqz v10, :cond_7

    .line 442
    const/16 v3, 0xe

    invoke-virtual {v14, v3}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v9

    .line 460
    :goto_4
    invoke-virtual {v14}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v3

    if-eqz v3, :cond_b

    if-nez v9, :cond_b

    const/4 v3, 0x1

    .line 461
    :goto_5
    if-eqz v3, :cond_3

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/android/calendar/agenda/m;->j:Z

    if-nez v4, :cond_4

    .line 465
    :cond_3
    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/agenda/m;->d:I

    invoke-static {v9, v4}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 467
    :cond_4
    if-eqz v3, :cond_c

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/agenda/m;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-nez v3, :cond_c

    .line 436
    :goto_6
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    goto :goto_2

    .line 420
    :cond_5
    const/4 v2, 0x0

    move v12, v2

    goto/16 :goto_1

    .line 430
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 572
    :catchall_1
    move-exception v2

    monitor-exit v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    .line 437
    :cond_6
    const/4 v3, 0x0

    move v10, v3

    goto :goto_3

    .line 444
    :cond_7
    :try_start_5
    invoke-virtual {v14}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 445
    const/4 v3, 0x3

    invoke-virtual {v14, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    .line 449
    :goto_7
    invoke-virtual {v14}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v3

    if-eqz v3, :cond_9

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_9

    .line 450
    const/4 v9, 0x0

    goto :goto_4

    .line 447
    :cond_8
    sget-object v3, Lcom/android/calendar/agenda/w;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v14, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    goto :goto_7

    .line 452
    :cond_9
    if-eqz v10, :cond_a

    .line 453
    const-wide/16 v6, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v9

    goto :goto_4

    .line 455
    :cond_a
    move-object/from16 v0, v16

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v9

    goto :goto_4

    .line 460
    :cond_b
    const/4 v3, 0x0

    goto :goto_5

    .line 472
    :cond_c
    if-eq v9, v2, :cond_1e

    .line 474
    const/4 v3, -0x1

    if-eq v2, v3, :cond_d

    if-eqz v2, :cond_d

    if-nez v9, :cond_11

    .line 475
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/calendar/agenda/u;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v9, v5}, Lcom/android/calendar/agenda/u;-><init>(III)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    :goto_8
    move v8, v9

    .line 512
    :goto_9
    const/4 v2, 0x0

    .line 513
    const/4 v7, 0x0

    .line 516
    if-eqz v12, :cond_10

    .line 517
    if-eqz v10, :cond_15

    .line 518
    const/16 v3, 0xf

    invoke-virtual {v14, v3}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v7

    .line 531
    :cond_f
    :goto_a
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/m;->e:I

    invoke-static {v7, v3}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 532
    if-le v7, v9, :cond_10

    .line 533
    new-instance v2, Lcom/android/calendar/agenda/t;

    invoke-direct {v2, v11, v9, v7}, Lcom/android/calendar/agenda/t;-><init>(III)V

    invoke-virtual {v15, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 534
    const/4 v2, 0x1

    .line 539
    :cond_10
    if-eqz v2, :cond_17

    .line 540
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    move v4, v9

    move v5, v11

    move v6, v9

    invoke-direct/range {v2 .. v7}, Lcom/android/calendar/agenda/u;-><init>(IIIII)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v8

    goto/16 :goto_6

    .line 480
    :cond_11
    const/4 v3, 0x0

    .line 481
    add-int/lit8 v4, v2, 0x1

    :goto_b
    if-gt v4, v9, :cond_14

    .line 482
    const/4 v3, 0x0

    .line 483
    invoke-virtual {v15}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 484
    :goto_c
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 485
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/android/calendar/agenda/t;

    move-object v7, v0

    .line 488
    iget v2, v7, Lcom/android/calendar/agenda/t;->c:I

    if-ge v2, v4, :cond_12

    .line 489
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->remove()V

    goto :goto_c

    .line 495
    :cond_12
    if-nez v3, :cond_1d

    .line 496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/calendar/agenda/u;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v3, v5, v4, v6}, Lcom/android/calendar/agenda/u;-><init>(III)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    const/4 v2, 0x1

    move v8, v2

    .line 499
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    iget v5, v7, Lcom/android/calendar/agenda/t;->a:I

    iget v6, v7, Lcom/android/calendar/agenda/t;->b:I

    iget v7, v7, Lcom/android/calendar/agenda/t;->c:I

    invoke-direct/range {v2 .. v7}, Lcom/android/calendar/agenda/u;-><init>(IIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v8

    .line 500
    goto :goto_c

    .line 481
    :cond_13
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 505
    :cond_14
    if-nez v3, :cond_e

    .line 506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/calendar/agenda/u;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v9, v5}, Lcom/android/calendar/agenda/u;-><init>(III)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 521
    :cond_15
    invoke-virtual {v14}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v3

    if-eqz v3, :cond_16

    .line 522
    const/4 v3, 0x3

    invoke-virtual {v14, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    .line 526
    :goto_e
    const-wide/16 v18, 0x0

    cmp-long v3, v4, v18

    if-eqz v3, :cond_f

    .line 527
    const-wide/16 v6, 0x3e8

    sub-long/2addr v4, v6

    move-object/from16 v0, v16

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v7

    goto/16 :goto_a

    .line 524
    :cond_16
    sget-object v3, Lcom/android/calendar/agenda/w;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v14, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    goto :goto_e

    .line 542
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/calendar/agenda/u;

    const/4 v4, 0x1

    invoke-direct {v3, v4, v9, v11}, Lcom/android/calendar/agenda/u;-><init>(III)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v8

    goto/16 :goto_6

    .line 548
    :cond_18
    if-eqz v12, :cond_1b

    if-lez v2, :cond_1b

    .line 549
    add-int/lit8 v4, v2, 0x1

    :goto_f
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/m;->e:I

    if-gt v4, v2, :cond_1b

    .line 550
    const/4 v3, 0x0

    .line 551
    invoke-virtual {v15}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 552
    :goto_10
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 553
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/android/calendar/agenda/t;

    move-object v7, v0

    .line 556
    iget v2, v7, Lcom/android/calendar/agenda/t;->c:I

    if-ge v2, v4, :cond_19

    .line 557
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto :goto_10

    .line 563
    :cond_19
    if-nez v3, :cond_1c

    .line 564
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v3, Lcom/android/calendar/agenda/u;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v3, v5, v4, v6}, Lcom/android/calendar/agenda/u;-><init>(III)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 565
    const/4 v2, 0x1

    move v8, v2

    .line 567
    :goto_11
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    iget v5, v7, Lcom/android/calendar/agenda/t;->a:I

    iget v6, v7, Lcom/android/calendar/agenda/t;->b:I

    iget v7, v7, Lcom/android/calendar/agenda/t;->c:I

    invoke-direct/range {v2 .. v7}, Lcom/android/calendar/agenda/u;-><init>(IIIII)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v8

    .line 568
    goto :goto_10

    .line 549
    :cond_1a
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 571
    :cond_1b
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/agenda/r;->d()V

    .line 572
    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0

    :cond_1c
    move v8, v3

    goto :goto_11

    :cond_1d
    move v8, v3

    goto/16 :goto_d

    :cond_1e
    move v8, v2

    goto/16 :goto_9
.end method

.method static synthetic c(Lcom/android/calendar/agenda/r;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    return-object v0
.end method

.method private c(Lcom/android/calendar/agenda/m;)V
    .locals 1

    .prologue
    .line 576
    invoke-virtual {p0}, Lcom/android/calendar/agenda/r;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 604
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/agenda/r;->d()V

    .line 605
    return-void

    .line 578
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/r;->d(Lcom/android/calendar/agenda/m;)V

    goto :goto_0

    .line 582
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/r;->e(Lcom/android/calendar/agenda/m;)V

    goto :goto_0

    .line 586
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/r;->f(Lcom/android/calendar/agenda/m;)V

    goto :goto_0

    .line 590
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/r;->i(Lcom/android/calendar/agenda/m;)V

    goto :goto_0

    .line 594
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/r;->g(Lcom/android/calendar/agenda/m;)V

    goto :goto_0

    .line 598
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/r;->h(Lcom/android/calendar/agenda/m;)V

    goto :goto_0

    .line 576
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method private d()V
    .locals 0

    .prologue
    .line 1446
    invoke-direct {p0}, Lcom/android/calendar/agenda/r;->e()V

    .line 1447
    invoke-direct {p0}, Lcom/android/calendar/agenda/r;->g()V

    .line 1448
    invoke-direct {p0}, Lcom/android/calendar/agenda/r;->f()V

    .line 1449
    return-void
.end method

.method private d(Lcom/android/calendar/agenda/m;)V
    .locals 40

    .prologue
    .line 611
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v33, v0

    monitor-enter v33

    .line 612
    :try_start_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    move-object/from16 v34, v0

    .line 613
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 615
    const/16 v30, 0x0

    .line 616
    const/16 v29, 0x0

    .line 617
    const/16 v28, 0x0

    .line 618
    const/16 v27, 0x0

    .line 619
    const/16 v26, 0x0

    .line 620
    const/4 v2, -0x1

    .line 626
    new-instance v35, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->h:Ljava/lang/String;

    move-object/from16 v0, v35

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 627
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 628
    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 629
    move-object/from16 v0, v35

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v12

    .line 632
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    if-eqz v3, :cond_0

    .line 633
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v3

    .line 634
    if-eqz v3, :cond_0

    .line 635
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v4

    .line 636
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v2

    .line 637
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 638
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    .line 639
    move-object/from16 v0, v35

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 640
    move-object/from16 v0, v35

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 641
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    :cond_0
    move/from16 v32, v2

    .line 645
    const/4 v2, -0x1

    move-object/from16 v0, v34

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 646
    const/4 v2, 0x0

    move/from16 v31, v2

    :goto_0
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 648
    const/4 v2, 0x0

    move-object/from16 v0, v34

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    .line 651
    const/16 v2, 0xc

    move-object/from16 v0, v34

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v38, v0

    .line 654
    const/4 v2, 0x3

    move-object/from16 v0, v34

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 655
    move-object/from16 v0, v35

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 656
    move-object/from16 v0, v35

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v24

    .line 658
    const/4 v2, 0x3

    move-object/from16 v0, v34

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 659
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/agenda/m;->j:Z

    if-eqz v2, :cond_d

    .line 660
    if-nez v30, :cond_1

    move/from16 v0, v32

    move/from16 v1, v24

    if-eq v0, v1, :cond_1

    .line 661
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 663
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v36

    move-wide/from16 v8, v38

    move/from16 v13, v31

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 664
    add-int/lit8 v2, v30, 0x1

    move/from16 v3, v27

    move/from16 v4, v28

    move/from16 v5, v29

    move v6, v2

    move/from16 v2, v26

    .line 646
    :goto_1
    add-int/lit8 v7, v31, 0x1

    move/from16 v31, v7

    move/from16 v26, v2

    move/from16 v27, v3

    move/from16 v28, v4

    move/from16 v29, v5

    move/from16 v30, v6

    goto/16 :goto_0

    .line 669
    :cond_2
    move/from16 v0, v24

    if-ge v0, v12, :cond_4

    .line 670
    if-nez v26, :cond_3

    move/from16 v0, v32

    move/from16 v1, v24

    if-eq v0, v1, :cond_3

    .line 671
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x0

    const/16 v16, 0x5

    const-string v17, ""

    const-wide/16 v18, -0x1

    const-wide/16 v20, -0x1

    const/16 v22, 0x0

    const/16 v23, -0x1

    const/16 v25, 0x0

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 673
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x1

    const/16 v16, -0x1

    const-string v17, ""

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-wide/from16 v18, v36

    move-wide/from16 v20, v38

    move/from16 v25, v31

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674
    add-int/lit8 v2, v26, 0x1

    move/from16 v3, v27

    move/from16 v4, v28

    move/from16 v5, v29

    move/from16 v6, v30

    .line 675
    goto :goto_1

    .line 676
    :cond_4
    move/from16 v0, v24

    if-ne v0, v12, :cond_6

    .line 677
    if-nez v29, :cond_5

    move/from16 v0, v32

    move/from16 v1, v24

    if-eq v0, v1, :cond_5

    .line 678
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x0

    const/16 v16, 0x2

    const-string v17, ""

    const-wide/16 v18, -0x1

    const-wide/16 v20, -0x1

    const/16 v22, 0x0

    const/16 v23, -0x1

    const/16 v25, 0x0

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 680
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x1

    const/16 v16, -0x1

    const-string v17, ""

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-wide/from16 v18, v36

    move-wide/from16 v20, v38

    move/from16 v25, v31

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 681
    add-int/lit8 v2, v29, 0x1

    move/from16 v3, v27

    move/from16 v4, v28

    move v5, v2

    move/from16 v6, v30

    move/from16 v2, v26

    .line 682
    goto/16 :goto_1

    .line 683
    :cond_6
    add-int/lit8 v2, v12, 0x7

    move/from16 v0, v24

    if-gt v0, v2, :cond_8

    .line 684
    if-nez v28, :cond_7

    move/from16 v0, v32

    move/from16 v1, v24

    if-eq v0, v1, :cond_7

    .line 685
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x0

    const/16 v16, 0x3

    const-string v17, ""

    const-wide/16 v18, -0x1

    const-wide/16 v20, -0x1

    const/16 v22, 0x0

    const/16 v23, -0x1

    const/16 v25, 0x0

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 687
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x1

    const/16 v16, -0x1

    const-string v17, ""

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-wide/from16 v18, v36

    move-wide/from16 v20, v38

    move/from16 v25, v31

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 688
    add-int/lit8 v2, v28, 0x1

    move/from16 v3, v27

    move v4, v2

    move/from16 v5, v29

    move/from16 v6, v30

    move/from16 v2, v26

    .line 689
    goto/16 :goto_1

    .line 690
    :cond_8
    add-int/lit8 v2, v12, 0x7

    move/from16 v0, v24

    if-lt v0, v2, :cond_d

    .line 691
    if-nez v27, :cond_9

    move/from16 v0, v32

    move/from16 v1, v24

    if-eq v0, v1, :cond_9

    .line 692
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x0

    const/16 v16, 0x4

    const-string v17, ""

    const-wide/16 v18, -0x1

    const-wide/16 v20, -0x1

    const/16 v22, 0x0

    const/16 v23, -0x1

    const/16 v25, 0x0

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 694
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x1

    const/16 v16, -0x1

    const-string v17, ""

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-wide/from16 v18, v36

    move-wide/from16 v20, v38

    move/from16 v25, v31

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 695
    add-int/lit8 v2, v27, 0x1

    move v3, v2

    move/from16 v4, v28

    move/from16 v5, v29

    move/from16 v6, v30

    move/from16 v2, v26

    .line 696
    goto/16 :goto_1

    .line 701
    :cond_a
    if-lez v30, :cond_b

    .line 702
    const/4 v2, 0x0

    :goto_2
    add-int/lit8 v3, v30, 0x1

    if-ge v2, v3, :cond_b

    .line 703
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 704
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 702
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 708
    :cond_b
    if-lez v26, :cond_c

    .line 709
    const/4 v2, 0x0

    :goto_3
    add-int/lit8 v3, v26, 0x1

    if-ge v2, v3, :cond_c

    .line 710
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 711
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 709
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 714
    :cond_c
    monitor-exit v33

    .line 715
    return-void

    .line 714
    :catchall_0
    move-exception v2

    monitor-exit v33
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_d
    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    move/from16 v5, v29

    move/from16 v6, v30

    goto/16 :goto_1
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1452
    const/4 v1, 0x0

    .line 1453
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1454
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 1455
    iget v0, v0, Lcom/android/calendar/agenda/u;->a:I

    if-nez v0, :cond_1

    .line 1456
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 1458
    goto :goto_0

    .line 1459
    :cond_0
    iput v1, p0, Lcom/android/calendar/agenda/r;->i:I

    .line 1460
    monitor-exit v2

    .line 1461
    return-void

    .line 1460
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private e(Lcom/android/calendar/agenda/m;)V
    .locals 26

    .prologue
    .line 721
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 722
    :try_start_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    move-object/from16 v20, v0

    .line 723
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 725
    const/16 v16, 0x0

    const/4 v15, 0x0

    const/4 v14, 0x0

    .line 726
    const/4 v2, -0x1

    .line 732
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    if-eqz v3, :cond_0

    .line 733
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v3

    .line 734
    if-eqz v3, :cond_0

    .line 735
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v4

    .line 736
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v2

    .line 737
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 738
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v2

    .line 739
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    :cond_0
    move/from16 v18, v2

    .line 743
    const/4 v2, -0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 744
    const/4 v2, 0x0

    move/from16 v17, v2

    :goto_0
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 746
    const/4 v2, 0x3

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/agenda/m;->j:Z

    if-nez v2, :cond_1

    move v2, v14

    move v3, v15

    move/from16 v4, v16

    .line 744
    :goto_1
    add-int/lit8 v5, v17, 0x1

    move/from16 v17, v5

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    goto :goto_0

    .line 750
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 753
    const/16 v2, 0xc

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v24, v0

    .line 756
    const/4 v2, 0x4

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 757
    if-nez v2, :cond_3

    .line 758
    if-nez v16, :cond_2

    move/from16 v0, v18

    if-eq v0, v2, :cond_2

    .line 759
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 761
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v6, v22

    move-wide/from16 v8, v24

    move/from16 v13, v17

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 762
    add-int/lit8 v2, v16, 0x1

    move v3, v15

    move v4, v2

    move v2, v14

    .line 763
    goto :goto_1

    .line 764
    :cond_3
    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 765
    if-nez v15, :cond_4

    move/from16 v0, v18

    if-eq v0, v2, :cond_4

    .line 766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 768
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v6, v22

    move-wide/from16 v8, v24

    move/from16 v13, v17

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 769
    add-int/lit8 v2, v15, 0x1

    move v3, v2

    move/from16 v4, v16

    move v2, v14

    .line 770
    goto/16 :goto_1

    .line 771
    :cond_5
    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    .line 772
    if-nez v14, :cond_6

    move/from16 v0, v18

    if-eq v0, v2, :cond_6

    .line 773
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v6, v22

    move-wide/from16 v8, v24

    move/from16 v13, v17

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 776
    add-int/lit8 v2, v14, 0x1

    move v3, v15

    move/from16 v4, v16

    .line 777
    goto/16 :goto_1

    .line 780
    :cond_7
    monitor-exit v19

    .line 781
    return-void

    .line 780
    :catchall_0
    move-exception v2

    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_8
    move v2, v14

    move v3, v15

    move/from16 v4, v16

    goto/16 :goto_1
.end method

.method private f()V
    .locals 11

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 1476
    iget-object v6, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v6

    .line 1477
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1481
    iget-object v8, p0, Lcom/android/calendar/agenda/r;->l:Ljava/util/ArrayList;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1482
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v4, v2

    move v1, v5

    move v3, v2

    .line 1483
    :goto_0
    if-ge v4, v7, :cond_1

    .line 1484
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 1485
    iget v0, v0, Lcom/android/calendar/agenda/u;->a:I

    const/4 v9, 0x1

    if-ne v0, v9, :cond_0

    .line 1486
    add-int/lit8 v0, v3, 0x1

    move v10, v1

    move v1, v0

    move v0, v10

    .line 1483
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1487
    :cond_0
    add-int/lit8 v0, v1, 0x1

    if-eq v1, v5, :cond_2

    .line 1488
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->l:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 1489
    goto :goto_1

    .line 1492
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->l:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1493
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1494
    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1495
    return-void

    .line 1493
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 1494
    :catchall_1
    move-exception v0

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method private f(Lcom/android/calendar/agenda/m;)V
    .locals 38

    .prologue
    .line 787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    monitor-enter v31

    .line 788
    :try_start_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    move-object/from16 v32, v0

    .line 789
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 791
    const/16 v30, 0x0

    .line 792
    const/4 v14, -0x1

    .line 798
    new-instance v33, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->h:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 799
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 800
    move-object/from16 v0, v33

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 801
    move-object/from16 v0, v33

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v12

    .line 804
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    if-eqz v2, :cond_0

    .line 805
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v2

    .line 806
    if-eqz v2, :cond_0

    .line 807
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v3

    .line 808
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v4, v4, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v4, v2}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v2

    .line 809
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v4, v4, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v4, v2}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 810
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v2

    int-to-long v4, v2

    .line 811
    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 812
    move-object/from16 v0, v33

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v14

    .line 813
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 817
    :cond_0
    const/4 v2, -0x1

    move-object/from16 v0, v32

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 818
    const/16 v29, 0x0

    :goto_0
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 821
    const/4 v2, 0x0

    move-object/from16 v0, v32

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v34

    .line 824
    const/16 v2, 0xc

    move-object/from16 v0, v32

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v36, v0

    .line 827
    const/4 v2, 0x3

    move-object/from16 v0, v32

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 828
    move-object/from16 v0, v33

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 829
    move-object/from16 v0, v33

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 831
    const/4 v3, 0x3

    move-object/from16 v0, v32

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 832
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/agenda/m;->j:Z

    if-eqz v3, :cond_6

    .line 833
    if-nez v30, :cond_1

    if-eq v14, v2, :cond_1

    .line 834
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 837
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v34

    move-wide/from16 v8, v36

    move/from16 v13, v29

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 838
    add-int/lit8 v2, v30, 0x1

    move v3, v2

    move v2, v14

    .line 818
    :goto_1
    add-int/lit8 v29, v29, 0x1

    move v14, v2

    move/from16 v30, v3

    goto :goto_0

    .line 844
    :cond_2
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/m;->d:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 846
    move/from16 v0, v16

    if-eq v0, v14, :cond_5

    .line 847
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x0

    const-string v17, ""

    const-wide/16 v18, -0x1

    const-wide/16 v20, -0x1

    const/16 v22, 0x0

    const/16 v23, -0x1

    const/16 v25, 0x0

    move/from16 v24, v16

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v16

    .line 852
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v18, Lcom/android/calendar/agenda/u;

    const/16 v19, 0x1

    const/16 v20, -0x1

    const-string v21, ""

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-wide/from16 v22, v34

    move-wide/from16 v24, v36

    move/from16 v28, v16

    invoke-direct/range {v18 .. v29}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v3, v30

    goto :goto_1

    .line 856
    :cond_3
    if-lez v30, :cond_4

    .line 857
    const/4 v2, 0x0

    :goto_3
    add-int/lit8 v3, v30, 0x1

    if-ge v2, v3, :cond_4

    .line 858
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 859
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 857
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 862
    :cond_4
    monitor-exit v31

    .line 863
    return-void

    .line 862
    :catchall_0
    move-exception v2

    monitor-exit v31
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_5
    move v2, v14

    goto :goto_2

    :cond_6
    move v2, v14

    move/from16 v3, v30

    goto/16 :goto_1
.end method

.method private g()V
    .locals 6

    .prologue
    .line 1513
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1514
    :try_start_0
    iget-object v3, p0, Lcom/android/calendar/agenda/r;->k:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1515
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1516
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1517
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 1518
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 1519
    iget v0, v0, Lcom/android/calendar/agenda/u;->a:I

    if-nez v0, :cond_0

    .line 1520
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->k:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1517
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1523
    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1524
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1525
    return-void

    .line 1523
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 1524
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method private g(Lcom/android/calendar/agenda/m;)V
    .locals 38

    .prologue
    .line 869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    monitor-enter v29

    .line 870
    :try_start_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    move-object/from16 v30, v0

    .line 871
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 873
    const/16 v26, 0x0

    .line 874
    const/4 v14, -0x1

    .line 875
    const/4 v2, -0x1

    .line 881
    new-instance v31, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->h:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 882
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 883
    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 884
    move-object/from16 v0, v31

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v12

    .line 886
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v32

    .line 889
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    if-eqz v3, :cond_0

    .line 890
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v3

    .line 891
    if-eqz v3, :cond_0

    .line 892
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v4

    .line 893
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v2

    .line 894
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 895
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    .line 896
    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 897
    move-object/from16 v0, v31

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 898
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    :cond_0
    move/from16 v28, v2

    .line 902
    const/4 v2, -0x1

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 903
    const/4 v2, 0x0

    move/from16 v27, v2

    :goto_0
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 905
    const/4 v2, 0x0

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v34

    .line 908
    const/16 v2, 0xc

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v36, v0

    .line 911
    const/4 v2, 0x3

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 912
    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 913
    move-object/from16 v0, v31

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 915
    const/4 v3, 0x3

    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 916
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/agenda/m;->j:Z

    if-eqz v3, :cond_8

    .line 917
    if-nez v26, :cond_1

    move/from16 v0, v28

    if-eq v0, v2, :cond_1

    .line 918
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 921
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v34

    move-wide/from16 v8, v36

    move/from16 v13, v27

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 922
    add-int/lit8 v2, v26, 0x1

    move v3, v2

    move v2, v14

    .line 903
    :goto_1
    add-int/lit8 v4, v27, 0x1

    move/from16 v27, v4

    move v14, v2

    move/from16 v26, v3

    goto :goto_0

    .line 928
    :cond_2
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/m;->d:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v24

    .line 930
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 932
    move/from16 v0, v24

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 934
    iget v2, v3, Landroid/text/format/Time;->weekDay:I

    .line 935
    sub-int v2, v2, v32

    .line 936
    if-eqz v2, :cond_4

    .line 937
    if-gez v2, :cond_3

    .line 938
    add-int/lit8 v2, v2, 0x7

    .line 940
    :cond_3
    iget v4, v3, Landroid/text/format/Time;->monthDay:I

    sub-int v2, v4, v2

    iput v2, v3, Landroid/text/format/Time;->monthDay:I

    .line 943
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 944
    iget-wide v2, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v16

    .line 946
    add-int/lit8 v2, v16, 0x6

    move/from16 v0, v24

    if-gt v0, v2, :cond_8

    .line 947
    move/from16 v0, v16

    if-eq v0, v14, :cond_7

    .line 948
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x0

    const-string v17, ""

    const-wide/16 v18, -0x1

    const-wide/16 v20, -0x1

    const/16 v22, 0x0

    const/16 v23, -0x1

    const/16 v25, 0x0

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v16

    .line 953
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x1

    const/16 v16, -0x1

    const-string v17, ""

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-wide/from16 v18, v34

    move-wide/from16 v20, v36

    move/from16 v25, v27

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v3, v26

    goto :goto_1

    .line 958
    :cond_5
    if-lez v26, :cond_6

    .line 959
    const/4 v2, 0x0

    :goto_3
    add-int/lit8 v3, v26, 0x1

    if-ge v2, v3, :cond_6

    .line 960
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 961
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 959
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 964
    :cond_6
    monitor-exit v29

    .line 965
    return-void

    .line 964
    :catchall_0
    move-exception v2

    monitor-exit v29
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_7
    move v2, v14

    goto :goto_2

    :cond_8
    move v2, v14

    move/from16 v3, v26

    goto/16 :goto_1
.end method

.method private h(Lcom/android/calendar/agenda/m;)V
    .locals 36

    .prologue
    .line 971
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    monitor-enter v29

    .line 972
    :try_start_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    move-object/from16 v30, v0

    .line 973
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 975
    const/16 v26, 0x0

    .line 976
    const/4 v14, -0x1

    .line 977
    const/4 v2, -0x1

    .line 983
    new-instance v31, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->h:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 984
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 985
    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 986
    move-object/from16 v0, v31

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v12

    .line 989
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    if-eqz v3, :cond_0

    .line 990
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v3

    .line 991
    if-eqz v3, :cond_0

    .line 992
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v4

    .line 993
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v2

    .line 994
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 995
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    .line 996
    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 997
    move-object/from16 v0, v31

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 998
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    iget-object v3, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    :cond_0
    move/from16 v28, v2

    .line 1002
    const/4 v2, -0x1

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1003
    const/4 v2, 0x0

    move/from16 v27, v2

    :goto_0
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1006
    const/4 v2, 0x0

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v32

    .line 1009
    const/16 v2, 0xc

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v34, v0

    .line 1012
    const/4 v2, 0x3

    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1013
    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1014
    move-object/from16 v0, v31

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 1016
    const/4 v3, 0x3

    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1017
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/agenda/m;->j:Z

    if-eqz v3, :cond_6

    .line 1018
    if-nez v26, :cond_1

    move/from16 v0, v28

    if-eq v0, v2, :cond_1

    .line 1019
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1022
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v32

    move-wide/from16 v8, v34

    move/from16 v13, v27

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1023
    add-int/lit8 v2, v26, 0x1

    move v3, v2

    move v2, v14

    .line 1003
    :goto_1
    add-int/lit8 v4, v27, 0x1

    move/from16 v27, v4

    move v14, v2

    move/from16 v26, v3

    goto :goto_0

    .line 1029
    :cond_2
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/m;->d:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v24

    .line 1031
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 1033
    move/from16 v0, v24

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1034
    const/4 v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 1035
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 1036
    iget-wide v6, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v16

    .line 1038
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v2

    .line 1040
    add-int v2, v2, v16

    move/from16 v0, v24

    if-gt v0, v2, :cond_6

    .line 1041
    move/from16 v0, v16

    if-eq v0, v14, :cond_5

    .line 1042
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x0

    const-string v17, ""

    const-wide/16 v18, -0x1

    const-wide/16 v20, -0x1

    const/16 v22, 0x0

    const/16 v23, -0x1

    const/16 v25, 0x0

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v2, v16

    .line 1047
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/android/calendar/agenda/u;

    const/4 v15, 0x1

    const/16 v16, -0x1

    const-string v17, ""

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-wide/from16 v18, v32

    move-wide/from16 v20, v34

    move/from16 v25, v27

    invoke-direct/range {v14 .. v25}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v3, v26

    goto :goto_1

    .line 1052
    :cond_3
    if-lez v26, :cond_4

    .line 1053
    const/4 v2, 0x0

    :goto_3
    add-int/lit8 v3, v26, 0x1

    if-ge v2, v3, :cond_4

    .line 1054
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1055
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1053
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1058
    :cond_4
    monitor-exit v29

    .line 1059
    return-void

    .line 1058
    :catchall_0
    move-exception v2

    monitor-exit v29
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_5
    move v2, v14

    goto :goto_2

    :cond_6
    move v2, v14

    move/from16 v3, v26

    goto/16 :goto_1
.end method

.method private i(Lcom/android/calendar/agenda/m;)V
    .locals 22

    .prologue
    .line 1065
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    monitor-enter v17

    .line 1066
    :try_start_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    move-object/from16 v18, v0

    .line 1067
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1069
    const-wide/16 v4, -0x1

    .line 1072
    const-string v2, ""

    .line 1077
    const/4 v2, -0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1078
    const/4 v2, 0x0

    move/from16 v16, v2

    move-wide v2, v4

    :goto_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1080
    const/4 v4, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/android/calendar/agenda/m;->j:Z

    if-nez v4, :cond_0

    .line 1078
    :goto_1
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    goto :goto_0

    .line 1084
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 1092
    const/16 v4, 0xa

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1093
    const/16 v4, 0xe

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1099
    cmp-long v4, v14, v2

    if-eqz v4, :cond_2

    .line 1100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105
    :goto_2
    const/16 v2, 0xc

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v8, v2

    .line 1108
    const/16 v2, 0x10

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1111
    const/16 v2, 0xd

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 1114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    new-instance v2, Lcom/android/calendar/agenda/u;

    const/4 v3, 0x1

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v12, 0x0

    move-wide/from16 v6, v20

    move/from16 v13, v16

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-wide v2, v14

    goto :goto_1

    .line 1116
    :cond_1
    monitor-exit v17

    .line 1117
    return-void

    .line 1116
    :catchall_0
    move-exception v2

    monitor-exit v17
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    move-wide v14, v2

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1464
    iget v0, p0, Lcom/android/calendar/agenda/r;->i:I

    return v0
.end method

.method public a(Landroid/text/format/Time;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1347
    iget-object v4, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1348
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 1349
    iget-wide v6, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v5

    .line 1350
    const/16 v3, 0x3e8

    .line 1352
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    .line 1353
    :goto_0
    if-ge v1, v6, :cond_1

    .line 1354
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 1355
    iget v7, v0, Lcom/android/calendar/agenda/u;->a:I

    if-nez v7, :cond_2

    .line 1356
    iget v0, v0, Lcom/android/calendar/agenda/u;->b:I

    sub-int v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1357
    if-nez v0, :cond_0

    .line 1358
    monitor-exit v4

    .line 1369
    :goto_1
    return v1

    .line 1360
    :cond_0
    if-ge v0, v3, :cond_2

    move v2, v0

    move v0, v1

    .line 1353
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 1369
    :cond_1
    monitor-exit v4

    move v1, v2

    goto :goto_1

    .line 1370
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v0, v2

    move v2, v3

    goto :goto_2
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 121
    iput p1, p0, Lcom/android/calendar/agenda/r;->j:I

    .line 122
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/b;->a(I)V

    .line 123
    return-void
.end method

.method public a(Lcom/android/calendar/agenda/m;)V
    .locals 2

    .prologue
    .line 401
    iget v0, p0, Lcom/android/calendar/agenda/r;->j:I

    and-int/lit8 v0, v0, 0x8

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 403
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/calendar/agenda/r;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 404
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/r;->c(Lcom/android/calendar/agenda/m;)V

    .line 408
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    iget-object v1, p1, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/b;->changeCursor(Landroid/database/Cursor;)V

    .line 409
    return-void

    .line 401
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 406
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/r;->b(Lcom/android/calendar/agenda/m;)V

    goto :goto_1
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1427
    const/4 v0, 0x0

    return v0
.end method

.method public b()I
    .locals 3

    .prologue
    .line 1468
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const-string v1, "preferences_filter"

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public b(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1380
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1381
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1382
    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_2

    .line 1383
    :cond_0
    monitor-exit v2

    move v0, v1

    .line 1393
    :goto_0
    return v0

    .line 1386
    :cond_1
    add-int/lit8 p1, p1, -0x1

    :cond_2
    if-ltz p1, :cond_3

    .line 1387
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 1388
    iget v3, v0, Lcom/android/calendar/agenda/u;->a:I

    if-nez v3, :cond_1

    .line 1389
    iget v0, v0, Lcom/android/calendar/agenda/u;->b:I

    monitor-exit v2

    goto :goto_0

    .line 1392
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 1393
    goto :goto_0
.end method

.method public c()I
    .locals 3

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const-string v1, "preferences_list_by"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c(I)I
    .locals 4

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1405
    :cond_0
    const/4 v0, 0x0

    .line 1422
    :goto_0
    return v0

    .line 1408
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1409
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 1410
    iget v2, v0, Lcom/android/calendar/agenda/u;->a:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1411
    iget v0, v0, Lcom/android/calendar/agenda/u;->e:I

    monitor-exit v1

    goto :goto_0

    .line 1421
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1413
    :cond_2
    add-int/lit8 v0, p1, 0x1

    .line 1414
    :try_start_1
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 1415
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v0

    .line 1416
    if-ltz v0, :cond_3

    .line 1417
    neg-int v0, v0

    monitor-exit v1

    goto :goto_0

    .line 1421
    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1422
    const/high16 v0, -0x80000000

    goto :goto_0
.end method

.method public d(I)I
    .locals 2

    .prologue
    .line 1500
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->l:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1501
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1502
    monitor-exit v1

    .line 1504
    if-nez v0, :cond_0

    .line 1505
    const/4 v0, 0x0

    .line 1507
    :goto_0
    return v0

    .line 1502
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1507
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public e(I)I
    .locals 2

    .prologue
    .line 1530
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->k:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1531
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1532
    monitor-exit v1

    .line 1534
    if-nez v0, :cond_0

    .line 1535
    const/4 v0, 0x0

    .line 1537
    :goto_0
    return v0

    .line 1532
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1537
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/b;->getCount()I

    move-result v0

    .line 164
    :goto_0
    return v0

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/b;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 179
    :goto_0
    return-object v0

    .line 174
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 176
    iget v2, v0, Lcom/android/calendar/agenda/u;->a:I

    if-nez v2, :cond_1

    .line 177
    monitor-exit v1

    goto :goto_0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 179
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    iget v0, v0, Lcom/android/calendar/agenda/u;->e:I

    invoke-virtual {v2, v0}, Lcom/android/calendar/agenda/b;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/b;->getItemId(I)J

    move-result-wide v0

    .line 195
    :goto_0
    return-wide v0

    .line 190
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v2

    .line 191
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 192
    iget v1, v0, Lcom/android/calendar/agenda/u;->a:I

    if-nez v1, :cond_1

    .line 193
    neg-int v0, p1

    int-to-long v0, v0

    monitor-exit v2

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 195
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    iget v0, v0, Lcom/android/calendar/agenda/u;->e:I

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/b;->getItemId(I)J

    move-result-wide v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 207
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 211
    :goto_0
    return v0

    .line 210
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 211
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, p1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    iget v0, v0, Lcom/android/calendar/agenda/u;->a:I

    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 221
    iget-object v7, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v7

    .line 222
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lt p1, v0, :cond_0

    .line 225
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/agenda/b;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v7

    .line 392
    :goto_0
    return-object v0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    sget-object v0, Lcom/android/calendar/agenda/r;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Day adapter doesn\'t have an item at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mRowInfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 230
    monitor-exit v7

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 234
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 235
    iget v1, v0, Lcom/android/calendar/agenda/u;->a:I

    if-nez v1, :cond_f

    .line 238
    if-eqz p2, :cond_16

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_16

    .line 241
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 242
    instance-of v3, v1, Lcom/android/calendar/agenda/v;

    if-eqz v3, :cond_16

    .line 244
    check-cast v1, Lcom/android/calendar/agenda/v;

    .line 245
    iget v3, v0, Lcom/android/calendar/agenda/u;->b:I

    iput v3, v1, Lcom/android/calendar/agenda/v;->b:I

    move-object v3, p2

    .line 249
    :goto_1
    if-nez v1, :cond_15

    .line 251
    new-instance v6, Lcom/android/calendar/agenda/v;

    invoke-direct {v6}, Lcom/android/calendar/agenda/v;-><init>()V

    .line 252
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const v3, 0x7f040008

    const/4 v8, 0x0

    invoke-static {v1, v3, p3, v8}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 253
    const v1, 0x7f12002a

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v6, Lcom/android/calendar/agenda/v;->a:Landroid/widget/TextView;

    .line 254
    iget v1, v0, Lcom/android/calendar/agenda/u;->b:I

    iput v1, v6, Lcom/android/calendar/agenda/v;->b:I

    .line 255
    invoke-virtual {v3, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 258
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    monitor-enter v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 259
    :try_start_4
    iget-object v8, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/calendar/agenda/r;->o:Ljava/lang/Runnable;

    invoke-static {v8, v9}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v8

    .line 260
    iget-object v9, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    iget-object v9, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 261
    iput-object v8, p0, Lcom/android/calendar/agenda/r;->g:Ljava/lang/String;

    .line 262
    iget-object v9, p0, Lcom/android/calendar/agenda/r;->f:Landroid/text/format/Time;

    invoke-virtual {v9, v8}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 264
    :cond_1
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 266
    :try_start_5
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const v8, 0x7f0a000a

    invoke-static {v1, v8}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v8

    .line 267
    iget v1, p0, Lcom/android/calendar/agenda/r;->j:I

    and-int/lit8 v1, v1, 0x8

    if-lez v1, :cond_4

    move v1, v4

    .line 269
    :goto_3
    if-eqz v8, :cond_2

    .line 270
    const v4, 0x7f020250

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 273
    :cond_2
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/agenda/r;->h:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 274
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 275
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v4, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    .line 278
    iget-object v5, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 279
    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/android/calendar/agenda/r;->b()I

    move-result v1

    const/4 v8, 0x2

    if-ne v1, v8, :cond_5

    .line 280
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    iget v1, v0, Lcom/android/calendar/agenda/u;->f:I

    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    iget-object v0, v0, Lcom/android/calendar/agenda/u;->g:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/agenda/r;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 340
    :cond_3
    :goto_4
    iget-object v0, v6, Lcom/android/calendar/agenda/v;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    const-string v0, ""

    .line 343
    invoke-static {}, Lcom/android/calendar/hj;->r()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 349
    :goto_5
    iget-object v1, v6, Lcom/android/calendar/agenda/v;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 351
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v0, v3

    goto/16 :goto_0

    .line 264
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0

    :cond_4
    move v1, v5

    .line 267
    goto :goto_3

    .line 281
    :cond_5
    iget v1, v0, Lcom/android/calendar/agenda/u;->b:I

    if-nez v1, :cond_6

    .line 282
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 284
    :cond_6
    iget v1, v0, Lcom/android/calendar/agenda/u;->b:I

    if-ne v1, v4, :cond_a

    .line 285
    const-string v1, ""

    .line 286
    sget-object v1, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 287
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const v2, 0x7f0f0211

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 288
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/android/calendar/agenda/u;->b:I

    iget-object v8, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v9, 0x0

    invoke-static {v5, v8, v9}, Lcom/android/calendar/hj;->a(ILandroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_6
    move-object v10, v1

    move-object v1, v2

    move-object v2, v10

    .line 313
    :goto_7
    iget-boolean v5, p0, Lcom/android/calendar/agenda/r;->n:Z

    if-eqz v5, :cond_3

    .line 314
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 315
    iget v2, v0, Lcom/android/calendar/agenda/u;->b:I

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 317
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    .line 318
    invoke-static {v8, v9, v1}, Lcom/android/calendar/hj;->a(JLandroid/text/format/Time;)J

    move-result-wide v8

    .line 320
    iget v0, v0, Lcom/android/calendar/agenda/u;->b:I

    if-ne v0, v4, :cond_d

    .line 321
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const v1, 0x7f0f0211

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    invoke-static {}, Lcom/android/calendar/hj;->h()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v8, v9, v2, v4}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_8
    move-object v2, v0

    .line 329
    goto/16 :goto_4

    .line 290
    :cond_7
    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 291
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const v2, 0x7f0f0212

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 292
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/android/calendar/agenda/u;->b:I

    iget-object v8, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v9, 0x4

    invoke-static {v5, v8, v9}, Lcom/android/calendar/hj;->a(ILandroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 293
    goto/16 :goto_6

    .line 294
    :cond_8
    invoke-static {}, Lcom/android/calendar/hj;->o()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 295
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const v2, 0x7f0f0211

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 296
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u060c "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/android/calendar/agenda/u;->b:I

    iget-object v5, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-static {v2, v5, v8}, Lcom/android/calendar/hj;->a(ILandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 297
    goto/16 :goto_6

    .line 299
    :cond_9
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const v2, 0x7f0f0211

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 300
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/android/calendar/agenda/u;->b:I

    iget-object v5, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-static {v2, v5, v8}, Lcom/android/calendar/hj;->a(ILandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 301
    goto/16 :goto_6

    .line 304
    :cond_a
    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 305
    iget v1, v0, Lcom/android/calendar/agenda/u;->b:I

    iget-object v2, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v5, 0x4

    invoke-static {v1, v2, v5}, Lcom/android/calendar/hj;->a(ILandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 306
    goto/16 :goto_7

    .line 308
    :cond_b
    iget v1, v0, Lcom/android/calendar/agenda/u;->b:I

    iget-object v2, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/android/calendar/hj;->a(ILandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 309
    goto/16 :goto_7

    .line 326
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v8, v9, v1, v2}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 327
    goto/16 :goto_8

    .line 330
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v8, v9, v0, v1}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 331
    goto/16 :goto_4

    .line 346
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    const v2, 0x7f0f01e3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 353
    :cond_f
    iget v1, v0, Lcom/android/calendar/agenda/u;->a:I

    if-ne v1, v4, :cond_14

    .line 355
    if-eqz p2, :cond_10

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_10

    move-object p2, v2

    .line 358
    :cond_10
    iget-object v1, p0, Lcom/android/calendar/agenda/r;->d:Lcom/android/calendar/agenda/b;

    iget v2, v0, Lcom/android/calendar/agenda/u;->e:I

    invoke-virtual {v1, v2, p2, p3}, Lcom/android/calendar/agenda/b;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 359
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/agenda/i;

    .line 360
    iget-object v3, v1, Lcom/android/calendar/agenda/i;->a:Landroid/widget/TextView;

    .line 365
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    const-string v3, ""

    .line 372
    const/16 v3, 0xb01

    .line 373
    iget-object v4, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0045

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 375
    iget v5, v0, Lcom/android/calendar/agenda/u;->c:I

    if-eqz v5, :cond_11

    iget v5, v0, Lcom/android/calendar/agenda/u;->d:I

    if-eqz v5, :cond_11

    .line 376
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 377
    iget v5, v0, Lcom/android/calendar/agenda/u;->b:I

    iget v6, v0, Lcom/android/calendar/agenda/u;->c:I

    if-ne v5, v6, :cond_12

    .line 378
    iget-object v4, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    iget-wide v8, v1, Lcom/android/calendar/agenda/i;->m:J

    invoke-static {v4, v8, v9, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    .line 379
    iget-object v4, v1, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 391
    :cond_11
    :goto_9
    iget v0, v0, Lcom/android/calendar/agenda/u;->b:I

    iput v0, v1, Lcom/android/calendar/agenda/i;->p:I

    .line 392
    monitor-exit v7

    move-object v0, v2

    goto/16 :goto_0

    .line 380
    :cond_12
    iget v5, v0, Lcom/android/calendar/agenda/u;->b:I

    iget v6, v0, Lcom/android/calendar/agenda/u;->d:I

    if-ne v5, v6, :cond_13

    .line 381
    iget-object v5, v1, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 382
    iget-object v4, p0, Lcom/android/calendar/agenda/r;->c:Landroid/content/Context;

    iget-wide v8, v1, Lcom/android/calendar/agenda/i;->n:J

    invoke-static {v4, v8, v9, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    .line 383
    iget-object v4, v1, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 386
    :cond_13
    iget-object v3, v1, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    iget-object v3, v1, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 395
    :cond_14
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown event type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/android/calendar/agenda/u;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_15
    move-object v6, v1

    goto/16 :goto_2

    :cond_16
    move-object v3, v2

    move-object v1, v2

    goto/16 :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1432
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v1

    .line 1442
    :goto_0
    return v0

    .line 1436
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1437
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 1438
    iget-object v0, p0, Lcom/android/calendar/agenda/r;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/u;

    .line 1439
    iget v0, v0, Lcom/android/calendar/agenda/u;->a:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    monitor-exit v2

    goto :goto_0

    .line 1441
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1439
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1441
    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 1442
    goto :goto_0
.end method
