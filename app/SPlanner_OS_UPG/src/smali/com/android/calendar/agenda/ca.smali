.class Lcom/android/calendar/agenda/ca;
.super Lcom/android/calendar/ag;
.source "DeleteEventFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/bw;


# direct methods
.method public constructor <init>(Lcom/android/calendar/agenda/bw;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/android/calendar/agenda/ca;->a:Lcom/android/calendar/agenda/bw;

    .line 321
    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 322
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 326
    iget-object v0, p0, Lcom/android/calendar/agenda/ca;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->d(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaDeleteActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 327
    if-eqz p3, :cond_0

    .line 328
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 334
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/agenda/ca;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->x()I

    move-result v0

    .line 335
    if-ne v0, v4, :cond_0

    .line 336
    iget-object v0, p0, Lcom/android/calendar/agenda/ca;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->y()[Ljava/lang/Long;

    move-result-object v0

    .line 337
    iget-object v1, p0, Lcom/android/calendar/agenda/ca;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v1}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/agenda/bb;->w()Z

    move-result v1

    .line 338
    new-instance v2, Lcom/android/calendar/cj;

    iget-object v3, p0, Lcom/android/calendar/agenda/ca;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v3}, Lcom/android/calendar/agenda/bw;->d(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaDeleteActivity;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcom/android/calendar/cj;-><init>(Landroid/app/Activity;Z)V

    invoke-virtual {v2, v0, v1}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;Z)V

    goto :goto_0

    .line 332
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
