.class public Lcom/android/calendar/agenda/bt;
.super Lcom/android/calendar/agenda/AgendaBaseAdapter;
.source "AgendaWindowAdapter.java"


# instance fields
.field private final W:Landroid/widget/LinearLayout;

.field private final X:Landroid/widget/LinearLayout;

.field private final Y:Landroid/widget/TextView;

.field private final Z:Landroid/widget/TextView;

.field private aa:Z

.field private final ab:Landroid/widget/TextView;

.field private final ac:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;ZZ)V
    .locals 5

    .prologue
    const v4, 0x7f120032

    const v3, 0x7f0c01de

    const v1, 0x7f04000d

    const/4 v2, 0x0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;Z)V

    .line 74
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    .line 75
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    .line 76
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/agenda/bt;->Y:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/agenda/bt;->Z:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->e:Landroid/app/Activity;

    const v1, 0x7f12006b

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/calendar/agenda/bt;->ac:Landroid/view/ViewGroup;

    .line 80
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    const v1, 0x7f040084

    invoke-static {v0, v1}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/agenda/bt;->ab:Landroid/widget/TextView;

    .line 82
    iput-boolean p4, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    .line 83
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Y:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 85
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 88
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bt;->q:Z

    if-nez v0, :cond_1

    .line 89
    new-instance v0, Lcom/android/calendar/agenda/bu;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/bu;-><init>(Lcom/android/calendar/agenda/bt;)V

    .line 101
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->addHeaderView(Landroid/view/View;)V

    .line 105
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->addFooterView(Landroid/view/View;)V

    .line 108
    :cond_1
    return-void
.end method

.method private E()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 227
    iget-boolean v1, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-eqz v1, :cond_1

    .line 234
    :cond_0
    :goto_0
    return v0

    .line 230
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->f:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->e()I

    move-result v1

    .line 231
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 232
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()V
    .locals 5

    .prologue
    .line 342
    new-instance v0, Lcom/android/calendar/agenda/bv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/agenda/bv;-><init>(Lcom/android/calendar/agenda/bt;Lcom/android/calendar/agenda/bu;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    iget v4, p0, Lcom/android/calendar/agenda/bt;->N:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/android/calendar/agenda/bt;->O:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/agenda/bv;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 343
    return-void
.end method

.method private G()V
    .locals 1

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/android/calendar/agenda/bt;->E()Z

    move-result v0

    .line 551
    if-nez v0, :cond_0

    .line 552
    invoke-direct {p0, v0, v0}, Lcom/android/calendar/agenda/bt;->d(ZZ)V

    .line 554
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/bt;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private a(Landroid/widget/TextView;II)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const v10, 0x7f0f0095

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 596
    invoke-direct {p0, p3}, Lcom/android/calendar/agenda/bt;->j(I)Ljava/lang/String;

    move-result-object v6

    .line 597
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    new-array v1, v9, [Ljava/lang/Object;

    aput-object v6, v1, v2

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 598
    new-instance v8, Landroid/text/SpannableString;

    invoke-direct {v8, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 599
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const-string v1, "sans-serif"

    move v3, v2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    invoke-virtual {v7, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v7, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    const/16 v4, 0x21

    invoke-virtual {v8, v0, v1, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 601
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 602
    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    invoke-static {}, Lcom/android/calendar/hj;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    invoke-direct {p0, p3}, Lcom/android/calendar/agenda/bt;->k(I)Ljava/lang/String;

    move-result-object v0

    .line 606
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v2

    invoke-virtual {v1, p2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 607
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 609
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/bt;Z)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/bt;->i(Z)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/bt;ZZ)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/agenda/bt;->d(ZZ)V

    return-void
.end method

.method private a(IIZ)Z
    .locals 3

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->f:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->e()I

    move-result v1

    .line 266
    iget-boolean v2, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-nez v2, :cond_0

    and-int/lit8 v2, v1, 0x5

    if-eqz v2, :cond_1

    .line 267
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/agenda/bt;->b(IIZ)Z

    move-result v2

    or-int/2addr v0, v2

    .line 269
    :cond_1
    iget-boolean v2, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-nez v2, :cond_2

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 270
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/agenda/bt;->c(IIZ)Z

    move-result v1

    or-int/2addr v0, v1

    .line 272
    :cond_3
    return v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/bt;II)Z
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/agenda/bt;->e(II)Z

    move-result v0

    return v0
.end method

.method private b(IIZ)Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 287
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-static {v0, p1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v2

    .line 288
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-static {v0, p2}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;I)J

    move-result-wide v4

    .line 290
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_BY_DAY_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    .line 291
    new-array v9, v8, [Ljava/lang/String;

    const-string v0, "COUNT(*) AS count"

    aput-object v0, v9, v1

    move-object v0, p0

    .line 292
    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/agenda/bt;->a(ZJJ)Ljava/lang/String;

    move-result-object v5

    .line 293
    if-eqz p3, :cond_0

    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND calendar_access_level>=500 AND contact_id IS NULL"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object v3, v7

    move-object v4, v9

    move-object v7, v6

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 298
    if-nez v0, :cond_2

    .line 305
    :cond_1
    :goto_0
    return v1

    .line 302
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 303
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 304
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 305
    if-lez v2, :cond_1

    move v1, v8

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/agenda/bt;)Z
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/android/calendar/agenda/bt;->E()Z

    move-result v0

    return v0
.end method

.method private c(IIZ)Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 320
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-static {v0, p1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v2

    .line 321
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-static {v0, p2}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;I)J

    move-result-wide v4

    .line 323
    sget-object v7, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    .line 324
    new-array v9, v1, [Ljava/lang/String;

    const-string v0, "COUNT(*) AS count"

    aput-object v0, v9, v8

    move-object v0, p0

    .line 325
    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/agenda/bt;->a(ZJJ)Ljava/lang/String;

    move-result-object v5

    .line 326
    if-nez p3, :cond_0

    .line 327
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND utc_due_date IS NOT NULL"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object v3, v7

    move-object v4, v9

    move-object v7, v6

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 331
    if-nez v0, :cond_1

    .line 338
    :goto_0
    return v8

    .line 335
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 336
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 337
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 338
    if-lez v2, :cond_2

    :goto_1
    move v8, v1

    goto :goto_0

    :cond_2
    move v1, v8

    goto :goto_1
.end method

.method private d(ZZ)V
    .locals 6

    .prologue
    const v5, 0x7f120031

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 557
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bt;->q:Z

    if-eqz v0, :cond_1

    .line 593
    :cond_0
    :goto_0
    return-void

    .line 560
    :cond_1
    if-nez p1, :cond_2

    .line 561
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bt;->b()V

    .line 563
    :cond_2
    if-nez p2, :cond_3

    .line 564
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bt;->c()V

    .line 567
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->M:Landroid/view/ActionMode;

    if-eqz v0, :cond_5

    .line 568
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 569
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 570
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 571
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Z:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 579
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/agenda/bt;->Y:Landroid/widget/TextView;

    if-eqz p1, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 580
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 581
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    const v3, 0x7f120033

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz p1, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 582
    iget v0, p0, Lcom/android/calendar/agenda/bt;->U:I

    if-nez v0, :cond_4

    .line 583
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/bt;->U:I

    .line 584
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Z:Landroid/widget/TextView;

    if-eqz p2, :cond_8

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 585
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 586
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    const v1, 0x7f120033

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 588
    iget v0, p0, Lcom/android/calendar/agenda/bt;->N:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/android/calendar/agenda/bt;->O:I

    if-eq v0, v4, :cond_0

    .line 591
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Y:Landroid/widget/TextView;

    const v1, 0x7f0f0442

    iget v2, p0, Lcom/android/calendar/agenda/bt;->N:I

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/agenda/bt;->a(Landroid/widget/TextView;II)V

    .line 592
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Z:Landroid/widget/TextView;

    const v1, 0x7f0f0441

    iget v2, p0, Lcom/android/calendar/agenda/bt;->O:I

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/agenda/bt;->a(Landroid/widget/TextView;II)V

    goto/16 :goto_0

    .line 573
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 574
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 575
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 576
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->Z:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_6
    move v0, v2

    .line 579
    goto :goto_2

    :cond_7
    move v0, v2

    .line 581
    goto :goto_3

    :cond_8
    move v1, v2

    .line 584
    goto :goto_4
.end method

.method private e(II)Z
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/calendar/agenda/bt;->a(IIZ)Z

    move-result v0

    return v0
.end method

.method private i(Z)V
    .locals 7

    .prologue
    const v4, 0x7f020132

    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 508
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->e()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v1, v2

    .line 510
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-eqz v0, :cond_3

    .line 511
    if-eqz p1, :cond_2

    .line 512
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->ac:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->ab:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 513
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->ac:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->ab:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 514
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/AgendaListView;->setVisibility(I)V

    .line 515
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->ab:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->ab:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 547
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v3

    .line 508
    goto :goto_0

    .line 518
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->ac:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->ab:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 519
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->ab:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/AgendaListView;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->bringToFront()V

    goto :goto_1

    .line 526
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->e:Landroid/app/Activity;

    const v6, 0x7f12006d

    invoke-virtual {v0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 527
    if-eqz v0, :cond_0

    .line 530
    const v6, 0x7f1200dc

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 531
    if-eqz v6, :cond_0

    .line 534
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->f:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-ne v0, v2, :cond_0

    if-eqz v6, :cond_0

    .line 535
    if-eqz p1, :cond_4

    .line 536
    const v0, 0x7f1200de

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 537
    if-eqz v1, :cond_5

    const v1, 0x7f0f02dc

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 540
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_6

    .line 541
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v0

    .line 542
    if-eqz v2, :cond_8

    if-eqz v0, :cond_7

    const v0, 0x7f020133

    :goto_4
    invoke-virtual {v6, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 544
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz p1, :cond_9

    move v0, v5

    :goto_5
    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaListView;->setVisibility(I)V

    .line 545
    if-eqz p1, :cond_a

    :goto_6
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 537
    :cond_5
    const v1, 0x7f0f02d2

    goto :goto_2

    :cond_6
    move v2, v3

    .line 540
    goto :goto_3

    :cond_7
    move v0, v4

    .line 542
    goto :goto_4

    :cond_8
    move v0, v4

    goto :goto_4

    :cond_9
    move v0, v3

    .line 544
    goto :goto_5

    :cond_a
    move v3, v5

    .line 545
    goto :goto_6
.end method

.method private j(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 612
    new-instance v2, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->x:Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 613
    invoke-static {v2, p1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 614
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 615
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 616
    :cond_0
    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(JLandroid/text/format/Time;)J

    move-result-wide v0

    .line 617
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->x:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 620
    :cond_1
    new-instance v3, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([C)V

    .line 621
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f013b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 622
    if-eqz v3, :cond_2

    .line 623
    const-string v4, "MDY"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 624
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 630
    :cond_2
    :goto_0
    invoke-static {v2, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 625
    :cond_3
    const-string v4, "YMD"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 626
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f048f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private k(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 634
    new-instance v2, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->x:Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 635
    invoke-static {v2, p1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 636
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 637
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 638
    :cond_0
    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(JLandroid/text/format/Time;)J

    move-result-wide v0

    .line 641
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f048f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 643
    invoke-static {v2, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a(ZIILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZIILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 190
    :goto_0
    return-object v0

    .line 186
    :cond_1
    if-eqz p1, :cond_2

    .line 187
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.android.calendar"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "searchresultTask"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 190
    :cond_2
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.android.calendar"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "searchinstances"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(ZJJ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-eqz v0, :cond_1

    .line 137
    invoke-super/range {p0 .. p5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZJJ)Ljava/lang/String;

    move-result-object v0

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 140
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZJJ)Ljava/lang/String;

    move-result-object v0

    .line 142
    if-nez p1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->f:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->e()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "contact_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IS NOT NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected declared-synchronized a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 206
    move-object v0, p2

    check-cast v0, Lcom/android/calendar/agenda/q;

    move-object v1, v0

    .line 207
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    iget v1, v1, Lcom/android/calendar/agenda/q;->g:I

    if-nez v1, :cond_0

    .line 208
    invoke-direct {p0}, Lcom/android/calendar/agenda/bt;->F()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    :cond_0
    monitor-exit p0

    return-void

    .line 204
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V
    .locals 12

    .prologue
    .line 347
    iget-boolean v2, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-nez v2, :cond_1

    .line 348
    invoke-super/range {p0 .. p7}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    if-eqz p6, :cond_0

    .line 355
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/android/calendar/agenda/bt;->H:Ljava/lang/String;

    .line 357
    new-instance v6, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->y:Ljava/lang/String;

    invoke-direct {v6, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 358
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    invoke-virtual {v6, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 359
    if-nez p1, :cond_a

    .line 364
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, v6, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 366
    if-nez p7, :cond_3

    invoke-virtual {p0, v2, v2}, Lcom/android/calendar/agenda/bt;->b(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 368
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    move-wide/from16 v0, p4

    invoke-virtual {v2, v6, v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 369
    invoke-virtual {p0, v6}, Lcom/android/calendar/agenda/bt;->a(Landroid/text/format/Time;)I

    move-result v2

    .line 370
    if-ltz v2, :cond_2

    .line 371
    iget-object v3, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v4, p0, Lcom/android/calendar/agenda/bt;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/AgendaListView;->getHeaderViewsCount()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/AgendaListView;->setSelection(I)V

    .line 375
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 376
    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 377
    const-wide/16 v4, 0x400

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    move-object v3, p0

    move-object v7, v6

    invoke-virtual/range {v2 .. v10}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_0

    .line 384
    :cond_3
    const/4 v2, 0x0

    .line 385
    iget-object v3, p0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    if-eqz v3, :cond_4

    .line 386
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bt;->j()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bt;->i()I

    move-result v3

    sub-int/2addr v2, v3

    .line 389
    :cond_4
    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bt;->k()I

    move-result v3

    if-lt v2, v3, :cond_6

    :cond_5
    iget-object v2, p0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_9

    .line 390
    :cond_6
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/agenda/bt;->x:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 391
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 392
    iget v3, v2, Landroid/text/format/Time;->month:I

    .line 393
    const/4 v4, 0x6

    if-ge v3, v4, :cond_7

    .line 394
    add-int/lit8 v4, v3, 0x6

    iput v4, v2, Landroid/text/format/Time;->month:I

    .line 395
    iget v4, v2, Landroid/text/format/Time;->year:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v2, Landroid/text/format/Time;->year:I

    .line 399
    :goto_2
    const/4 v4, 0x1

    iput v4, v2, Landroid/text/format/Time;->monthDay:I

    .line 400
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v8, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v8, v9}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v4

    .line 402
    new-instance v2, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/agenda/bt;->x:Ljava/lang/String;

    invoke-direct {v2, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 403
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 404
    const/4 v5, 0x5

    if-le v3, v5, :cond_8

    .line 405
    add-int/lit8 v3, v3, -0x6

    iput v3, v2, Landroid/text/format/Time;->month:I

    .line 406
    iget v3, v2, Landroid/text/format/Time;->year:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->year:I

    .line 410
    :goto_3
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v3

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 411
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v5

    .line 413
    invoke-super {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f()I

    move-result v8

    .line 422
    :goto_4
    const/4 v7, 0x2

    move-object v3, p0

    move-object/from16 v9, p6

    invoke-virtual/range {v3 .. v9}, Lcom/android/calendar/agenda/bt;->a(IILandroid/text/format/Time;IILjava/lang/String;)Lcom/android/calendar/agenda/q;

    move-result-object v2

    .line 423
    if-eqz v2, :cond_0

    .line 424
    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/bt;->a(Lcom/android/calendar/agenda/q;)Z

    goto/16 :goto_0

    .line 397
    :cond_7
    add-int/lit8 v4, v3, -0x6

    iput v4, v2, Landroid/text/format/Time;->month:I

    goto :goto_2

    .line 408
    :cond_8
    add-int/lit8 v3, v3, 0x6

    iput v3, v2, Landroid/text/format/Time;->month:I

    goto :goto_3

    .line 416
    :cond_9
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bt;->i()I

    move-result v4

    .line 417
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bt;->j()I

    move-result v5

    .line 419
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bt;->f()I

    move-result v8

    goto :goto_4

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 214
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/view/ActionMode;)V

    .line 215
    invoke-direct {p0}, Lcom/android/calendar/agenda/bt;->G()V

    .line 216
    invoke-direct {p0}, Lcom/android/calendar/agenda/bt;->F()V

    .line 217
    return-void
.end method

.method protected a(Z)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    if-eqz p1, :cond_0

    sget-object v0, Lcom/android/calendar/agenda/bt;->d:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/calendar/agenda/bt;->c:[Ljava/lang/String;

    goto :goto_0
.end method

.method protected b(Lcom/android/calendar/agenda/q;)V
    .locals 17

    .prologue
    .line 431
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-nez v2, :cond_1

    .line 432
    invoke-super/range {p0 .. p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(Lcom/android/calendar/agenda/q;)V

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->H:Ljava/lang/String;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->H:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->H:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v2, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    .line 440
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    if-eqz v2, :cond_0

    .line 441
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    monitor-enter v10

    .line 442
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 443
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/m;

    iget v3, v2, Lcom/android/calendar/agenda/m;->d:I

    .line 444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/m;

    iget v2, v2, Lcom/android/calendar/agenda/m;->e:I

    .line 446
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lcom/android/calendar/agenda/bt;->c(II)I

    move-result v4

    .line 447
    new-instance v5, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/agenda/bt;->x:Ljava/lang/String;

    invoke-direct {v5, v6}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 448
    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/calendar/agenda/q;->f:I

    packed-switch v6, :pswitch_data_0

    .line 467
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->e:Landroid/app/Activity;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 468
    invoke-virtual {v2}, Lcom/android/calendar/al;->d()I

    move-result v3

    .line 469
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    if-eqz v2, :cond_8

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    .line 470
    :goto_2
    const-string v7, "*"

    .line 472
    if-eqz v2, :cond_4

    const/4 v2, 0x4

    if-ne v3, v2, :cond_4

    .line 473
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    .line 474
    const-string v2, "*"

    move-object/from16 v0, p1

    iput-object v2, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    .line 477
    :cond_4
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    const v3, 0x259d23

    if-le v2, v3, :cond_5

    .line 478
    const v2, 0x259d23

    move-object/from16 v0, p1

    iput v2, v0, Lcom/android/calendar/agenda/q;->d:I

    .line 479
    :cond_5
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/q;->c:I

    const v3, 0x24dc87

    if-ge v2, v3, :cond_6

    .line 480
    const v2, 0x24dc87

    move-object/from16 v0, p1

    iput v2, v0, Lcom/android/calendar/agenda/q;->c:I

    .line 482
    :cond_6
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 483
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/q;->c:I

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v8

    .line 484
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/q;->d:I

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;I)J

    move-result-wide v12

    .line 486
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v2, v2, 0x1

    if-lez v2, :cond_9

    .line 487
    const/4 v3, 0x0

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/agenda/q;->c:I

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/calendar/agenda/q;->d:I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/calendar/agenda/bt;->a(ZIILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 488
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->i:Lcom/android/calendar/agenda/p;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/p;->cancelOperation(I)V

    .line 489
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/calendar/agenda/bt;->i:Lcom/android/calendar/agenda/p;

    const/4 v15, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/bt;->a(Z)[Ljava/lang/String;

    move-result-object v16

    const/4 v3, 0x0

    move-object/from16 v2, p0

    move-wide v4, v8

    move-wide v6, v12

    invoke-virtual/range {v2 .. v7}, Lcom/android/calendar/agenda/bt;->a(ZJJ)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const-string v9, "startDay ASC, startMinute ASC, title ASC"

    move-object v2, v14

    move v3, v15

    move-object/from16 v4, p1

    move-object v5, v11

    move-object/from16 v6, v16

    invoke-virtual/range {v2 .. v9}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_7
    :goto_3
    monitor-exit v10

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 450
    :pswitch_0
    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v7, -0x1

    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2, v6, v7}, Lcom/android/calendar/agenda/bt;->a(IZII)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lcom/android/calendar/agenda/q;->d:I

    .line 451
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    const/4 v3, 0x1

    const/4 v6, 0x0

    neg-int v4, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v6, v4}, Lcom/android/calendar/agenda/bt;->a(IZII)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lcom/android/calendar/agenda/q;->c:I

    .line 452
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/q;->c:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v6, v2

    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/m;

    iget-wide v2, v2, Lcom/android/calendar/agenda/m;->h:J

    move-object/from16 v0, p1

    iput-wide v2, v0, Lcom/android/calendar/agenda/q;->i:J

    .line 454
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/agenda/q;->i:J

    sub-long/2addr v2, v6

    move-object/from16 v0, p1

    iput-wide v2, v0, Lcom/android/calendar/agenda/q;->h:J

    .line 455
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->f:Lcom/android/calendar/al;

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/q;->c:I

    invoke-static {v5, v3}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/android/calendar/al;->a(J)V

    goto/16 :goto_1

    .line 458
    :pswitch_1
    const/4 v3, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v6, v7}, Lcom/android/calendar/agenda/bt;->a(IZII)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lcom/android/calendar/agenda/q;->c:I

    .line 459
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/q;->c:I

    const/4 v3, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v6, v4}, Lcom/android/calendar/agenda/bt;->a(IZII)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lcom/android/calendar/agenda/q;->d:I

    .line 460
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/q;->c:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v6, v2

    .line 461
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/m;

    iget-wide v2, v2, Lcom/android/calendar/agenda/m;->i:J

    move-object/from16 v0, p1

    iput-wide v2, v0, Lcom/android/calendar/agenda/q;->h:J

    .line 462
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/agenda/q;->h:J

    add-long/2addr v2, v6

    move-object/from16 v0, p1

    iput-wide v2, v0, Lcom/android/calendar/agenda/q;->i:J

    .line 463
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->f:Lcom/android/calendar/al;

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/agenda/q;->d:I

    invoke-static {v5, v3}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/android/calendar/al;->a(J)V

    goto/16 :goto_1

    .line 469
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 491
    :cond_9
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v2, v2, 0x2

    if-lez v2, :cond_7

    .line 492
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/agenda/q;->c:I

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/calendar/agenda/q;->d:I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/calendar/agenda/bt;->a(ZIILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/bt;->i:Lcom/android/calendar/agenda/p;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/p;->cancelOperation(I)V

    .line 494
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/calendar/agenda/bt;->i:Lcom/android/calendar/agenda/p;

    const/4 v15, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/bt;->a(Z)[Ljava/lang/String;

    move-result-object v16

    const/4 v3, 0x1

    move-object/from16 v2, p0

    move-wide v4, v8

    move-wide v6, v12

    invoke-virtual/range {v2 .. v7}, Lcom/android/calendar/agenda/bt;->a(ZJJ)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const-string v9, "utc_due_date ASC"

    move-object v2, v14

    move v3, v15

    move-object/from16 v4, p1

    move-object v5, v11

    move-object/from16 v6, v16

    invoke-virtual/range {v2 .. v9}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    .line 448
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected b(Z)V
    .locals 1

    .prologue
    .line 503
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bt;->q:Z

    if-eqz v0, :cond_0

    .line 504
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/bt;->i(Z)V

    .line 505
    :cond_0
    return-void
.end method

.method public c(Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/android/calendar/agenda/bt;->G()V

    .line 222
    invoke-direct {p0}, Lcom/android/calendar/agenda/bt;->F()V

    .line 223
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->c(Landroid/view/ActionMode;)V

    .line 224
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->X:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    invoke-super {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d()V

    .line 115
    return-void
.end method

.method protected f()I
    .locals 2

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 157
    iget-boolean v1, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-nez v1, :cond_0

    .line 158
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->f:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->e()I

    move-result v1

    .line 163
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 179
    :goto_1
    :pswitch_0
    return v0

    .line 160
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/bt;->f:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->d()I

    move-result v1

    goto :goto_0

    .line 166
    :pswitch_1
    const/4 v0, 0x1

    .line 167
    goto :goto_1

    .line 169
    :pswitch_2
    const/4 v0, 0x2

    .line 170
    goto :goto_1

    .line 173
    :pswitch_3
    const/4 v0, 0x3

    .line 174
    goto :goto_1

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-eqz v0, :cond_0

    .line 197
    invoke-super {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g()I

    move-result v0

    or-int/lit8 v0, v0, 0x1

    .line 199
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g()I

    move-result v0

    or-int/lit8 v0, v0, 0x1

    or-int/lit8 v0, v0, 0x2

    or-int/lit8 v0, v0, 0x8

    goto :goto_0
.end method

.method public k()I
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->e()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 120
    :goto_0
    if-eqz v0, :cond_1

    const/16 v0, 0x5a

    :goto_1
    return v0

    .line 119
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 120
    :cond_1
    const/16 v0, 0x1e

    goto :goto_1
.end method

.method public l()I
    .locals 3

    .prologue
    const/16 v1, 0xb4

    .line 125
    iget-object v0, p0, Lcom/android/calendar/agenda/bt;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->e()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 126
    :goto_0
    iget-boolean v2, p0, Lcom/android/calendar/agenda/bt;->aa:Z

    if-eqz v2, :cond_1

    move v0, v1

    :goto_1
    return v0

    .line 125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 126
    :cond_1
    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    const/16 v0, 0x3c

    goto :goto_1
.end method
