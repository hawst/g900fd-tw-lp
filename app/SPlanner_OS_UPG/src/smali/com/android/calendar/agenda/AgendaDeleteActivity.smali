.class public Lcom/android/calendar/agenda/AgendaDeleteActivity;
.super Lcom/android/calendar/agenda/a;
.source "AgendaDeleteActivity.java"


# instance fields
.field protected b:Ljava/lang/String;

.field protected c:I

.field protected d:Landroid/text/format/Time;

.field protected e:Ljava/lang/Runnable;

.field private f:Lcom/android/calendar/agenda/bw;

.field private g:I

.field private h:J

.field private i:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/android/calendar/agenda/a;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    .line 59
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->h:J

    .line 60
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->i:Ljava/lang/Boolean;

    .line 62
    new-instance v0, Lcom/android/calendar/agenda/x;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/x;-><init>(Lcom/android/calendar/agenda/AgendaDeleteActivity;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->e:Ljava/lang/Runnable;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 147
    invoke-static {p0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_0
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->d:Landroid/text/format/Time;

    .line 149
    const-string v0, "beginTime"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->h:J

    .line 150
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->d:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->h:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 153
    const-string v0, "queryDelete"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->b:Ljava/lang/String;

    .line 154
    const-string v0, "search_type_for_delete"

    const-wide/16 v2, 0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->c:I

    .line 156
    invoke-static {p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->c:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(I)V

    .line 158
    const-string v0, "delete_type"

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->g:I

    .line 161
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const-string v0, "0"

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    :cond_0
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->g:I

    .line 165
    :cond_1
    return-void

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->e:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 88
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    if-nez v2, :cond_0

    .line 89
    if-eqz p1, :cond_2

    const-string v2, "key_delete_fragment"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 90
    const-string v2, "key_delete_fragment"

    invoke-virtual {v0, p1, v2}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 91
    instance-of v2, v0, Lcom/android/calendar/agenda/bw;

    if-eqz v2, :cond_1

    .line 92
    check-cast v0, Lcom/android/calendar/agenda/bw;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    .line 100
    :cond_0
    :goto_0
    const v0, 0x7f12006d

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 101
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 102
    return-void

    .line 94
    :cond_1
    new-instance v0, Lcom/android/calendar/agenda/bw;

    invoke-direct {v0}, Lcom/android/calendar/agenda/bw;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    goto :goto_0

    .line 97
    :cond_2
    new-instance v0, Lcom/android/calendar/agenda/bw;

    invoke-direct {v0}, Lcom/android/calendar/agenda/bw;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bw;->a(Z)V

    .line 237
    :cond_0
    return-void
.end method

.method protected b()J
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->h:J

    return-wide v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->g:I

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 188
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 213
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 190
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 191
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 194
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 195
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 196
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->finish()V

    goto :goto_0

    .line 198
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 205
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 188
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const v0, 0x7f040034

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->setContentView(I)V

    .line 76
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->a(Landroid/content/Intent;)V

    .line 77
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->a(Landroid/os/Bundle;)V

    .line 78
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 182
    invoke-static {p0}, Lcom/android/calendar/al;->b(Landroid/content/Context;)V

    .line 183
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onDestroy()V

    .line 184
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 170
    const-string v1, "key_delete_fragment"

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->f:Lcom/android/calendar/agenda/bw;

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 171
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 172
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 176
    invoke-static {p0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    .line 177
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onStop()V

    .line 178
    return-void
.end method
