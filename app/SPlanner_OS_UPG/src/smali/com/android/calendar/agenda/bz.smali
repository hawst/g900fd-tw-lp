.class Lcom/android/calendar/agenda/bz;
.super Lcom/android/calendar/common/a/a;
.source "DeleteEventFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/bw;


# direct methods
.method public constructor <init>(Lcom/android/calendar/agenda/bw;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    .line 373
    invoke-direct {p0, p2}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;)V

    .line 374
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->f(Z)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 419
    const v0, 0x7f120324

    const v1, 0x7f0f0398

    invoke-interface {p2, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 420
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->e(Z)V

    .line 384
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->e(Z)V

    .line 389
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->notifyDataSetChanged()V

    .line 410
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->w()Z

    move-result v0

    return v0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bw;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 4

    .prologue
    const v3, 0x7f120324

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 398
    iget-object v2, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v2}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v2

    invoke-virtual {v2, v0, v0}, Lcom/android/calendar/agenda/bb;->a(ZZ)I

    move-result v2

    .line 399
    if-lez v2, :cond_0

    .line 400
    iget-object v2, p0, Lcom/android/calendar/agenda/bz;->c:Landroid/view/Menu;

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 404
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v2}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/agenda/bb;->a()I

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/bz;->b(Z)V

    .line 405
    return-void

    .line 402
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/agenda/bz;->c:Landroid/view/Menu;

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 404
    goto :goto_1
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 438
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f120324

    if-ne v0, v1, :cond_0

    .line 439
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    iget-object v1, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v1}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/agenda/bb;->y()[Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v2}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/agenda/bb;->w()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;[Ljava/lang/Long;Z)V

    .line 440
    const/4 v0, 0x1

    .line 442
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 424
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 425
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->e(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaListView;->setActionMode(Landroid/view/ActionMode;)V

    .line 426
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->a(Landroid/view/ActionMode;)V

    .line 427
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 447
    invoke-super {p0, p1}, Lcom/android/calendar/common/a/a;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 448
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 449
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;Lcom/android/calendar/agenda/bz;)Lcom/android/calendar/agenda/bz;

    .line 450
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->e(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setActionMode(Landroid/view/ActionMode;)V

    .line 451
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->c(Landroid/view/ActionMode;)V

    .line 453
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->d(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaDeleteActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->d(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaDeleteActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->d(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaDeleteActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->d(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaDeleteActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->onBackPressed()V

    .line 456
    :cond_0
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/android/calendar/agenda/bz;->a:Lcom/android/calendar/agenda/bw;

    invoke-static {v0}, Lcom/android/calendar/agenda/bw;->a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->b(Landroid/view/ActionMode;)V

    .line 433
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
