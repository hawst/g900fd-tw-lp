.class Lcom/android/calendar/agenda/aw;
.super Ljava/lang/Object;
.source "AgendaPickActivity.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaPickActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Lcom/sec/android/touchwiz/widget/TwExpandableListView;Landroid/view/View;IIJ)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 161
    iget-object v2, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v2}, Lcom/android/calendar/agenda/AgendaPickActivity;->b(Lcom/android/calendar/agenda/AgendaPickActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v2}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v2

    if-nez v2, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v0

    .line 164
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v2}, Lcom/android/calendar/agenda/AgendaPickActivity;->d(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Lcom/android/calendar/agenda/AgendaListView;->b(II)I

    move-result v2

    iget-object v3, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaPickActivity;->d(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/calendar/agenda/AgendaListView;->getHeaderViewsCount()I

    move-result v3

    sub-int/2addr v2, v3

    .line 165
    iget-object v3, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/bb;->h(I)I

    move-result v2

    .line 166
    iget-object v3, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/android/calendar/agenda/bb;->a(IZ)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v3

    .line 167
    iget-object v4, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaPickActivity;->c(Lcom/android/calendar/agenda/AgendaPickActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/calendar/agenda/bb;->c(I)J

    move-result-wide v4

    .line 168
    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_0

    .line 171
    iget-object v0, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaPickActivity;->e(Lcom/android/calendar/agenda/AgendaPickActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    iget-object v0, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-virtual {v0, v3, v4, v5}, Lcom/android/calendar/agenda/AgendaPickActivity;->a(Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;J)V

    :goto_1
    move v0, v1

    .line 177
    goto :goto_0

    .line 174
    :cond_2
    new-instance v0, Lcom/android/calendar/gr;

    iget-object v2, p0, Lcom/android/calendar/agenda/aw;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-direct {v0, v2, v1}, Lcom/android/calendar/gr;-><init>(Landroid/app/Activity;Z)V

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/gr;->b(J)V

    goto :goto_1
.end method
