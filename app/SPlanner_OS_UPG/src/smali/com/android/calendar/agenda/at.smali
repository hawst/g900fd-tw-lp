.class Lcom/android/calendar/agenda/at;
.super Ljava/lang/Object;
.source "AgendaListView.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaListView;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaListView;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Lcom/sec/android/touchwiz/widget/TwExpandableListView;Landroid/view/View;IJ)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 199
    .line 200
    iget-object v0, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->f(Lcom/android/calendar/agenda/AgendaListView;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->getGroupCount()I

    move-result v5

    move v4, v1

    move v0, v3

    move v2, v3

    .line 204
    :goto_0
    if-ge v4, v5, :cond_2

    .line 205
    if-ne v4, p3, :cond_0

    .line 204
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 209
    :cond_0
    iget-object v6, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v6, v4}, Lcom/android/calendar/agenda/AgendaListView;->isGroupExpanded(I)Z

    move-result v6

    if-nez v6, :cond_1

    move v2, v1

    .line 210
    goto :goto_1

    :cond_1
    move v0, v1

    .line 212
    goto :goto_1

    .line 216
    :cond_2
    if-eqz v2, :cond_3

    .line 217
    iget-object v2, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v2, p3}, Lcom/android/calendar/agenda/AgendaListView;->isGroupExpanded(I)Z

    move-result v2

    if-nez v2, :cond_7

    move v2, v3

    .line 220
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 221
    iget-object v0, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, p3}, Lcom/android/calendar/agenda/AgendaListView;->isGroupExpanded(I)Z

    move-result v0

    .line 224
    :cond_4
    if-eq v2, v0, :cond_5

    .line 225
    iget-object v3, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v3, v2}, Lcom/android/calendar/agenda/AgendaListView;->a(Lcom/android/calendar/agenda/AgendaListView;Z)Z

    .line 226
    iget-object v2, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v2, v0}, Lcom/android/calendar/agenda/AgendaListView;->b(Lcom/android/calendar/agenda/AgendaListView;Z)Z

    .line 229
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->k(Lcom/android/calendar/agenda/AgendaListView;)Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 230
    iget-object v0, p0, Lcom/android/calendar/agenda/at;->a:Lcom/android/calendar/agenda/AgendaListView;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaListView;->k(Lcom/android/calendar/agenda/AgendaListView;)Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;->onGroupClick(Lcom/sec/android/touchwiz/widget/TwExpandableListView;Landroid/view/View;IJ)Z

    move-result v1

    .line 233
    :cond_6
    return v1

    :cond_7
    move v2, v1

    .line 217
    goto :goto_2
.end method
