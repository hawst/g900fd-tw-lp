.class Lcom/android/calendar/agenda/ab;
.super Ljava/lang/Object;
.source "AgendaFragment.java"

# interfaces
.implements Lcom/android/calendar/c/d;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/z;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/z;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/android/calendar/agenda/ab;->a:Lcom/android/calendar/agenda/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 181
    iget-object v0, p0, Lcom/android/calendar/agenda/ab;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g(Z)[Ljava/lang/Long;

    move-result-object v5

    .line 183
    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v0, v5, v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 184
    const-wide/16 v8, 0x0

    cmp-long v2, v0, v8

    if-gez v2, :cond_1

    const/4 v2, 0x1

    .line 186
    :goto_1
    if-eqz v2, :cond_0

    .line 187
    const-wide/16 v8, -0x1

    mul-long/2addr v0, v8

    .line 189
    :cond_0
    if-eqz v2, :cond_2

    sget-object v7, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v7, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 192
    :goto_2
    new-instance v1, Lcom/android/calendar/c/c;

    invoke-direct {v1, v0, v2}, Lcom/android/calendar/c/c;-><init>(Landroid/net/Uri;Z)V

    .line 193
    iget-object v0, p0, Lcom/android/calendar/agenda/ab;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->d(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/c/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/c/a;->a(Lcom/android/calendar/c/c;)V

    .line 183
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_1
    move v2, v3

    .line 184
    goto :goto_1

    .line 189
    :cond_2
    sget-object v7, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 195
    :cond_3
    return-void
.end method
