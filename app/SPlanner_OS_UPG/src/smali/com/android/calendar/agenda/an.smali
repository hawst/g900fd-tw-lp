.class public Lcom/android/calendar/agenda/an;
.super Landroid/app/DialogFragment;
.source "AgendaFragment.java"


# instance fields
.field private a:Lcom/android/calendar/d/g;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 899
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 895
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/an;->a:Lcom/android/calendar/d/g;

    .line 900
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 902
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 895
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/an;->a:Lcom/android/calendar/d/g;

    .line 903
    iput p1, p0, Lcom/android/calendar/agenda/an;->b:I

    .line 904
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 908
    invoke-virtual {p0}, Lcom/android/calendar/agenda/an;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 909
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090015

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 911
    iget-object v2, p0, Lcom/android/calendar/agenda/an;->a:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->i()Z

    move-result v2

    if-nez v2, :cond_0

    .line 913
    const/4 v2, 0x0

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 916
    :cond_0
    if-eqz p1, :cond_1

    const-string v2, "listBy"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 917
    const-string v2, "listBy"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/agenda/an;->b:I

    .line 920
    :cond_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 921
    const v1, 0x7f0f0286

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 923
    iget v1, p0, Lcom/android/calendar/agenda/an;->b:I

    new-instance v3, Lcom/android/calendar/agenda/ao;

    invoke-direct {v3, p0}, Lcom/android/calendar/agenda/ao;-><init>(Lcom/android/calendar/agenda/an;)V

    invoke-virtual {v2, v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 934
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 935
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 937
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 942
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 943
    const-string v0, "listBy"

    iget v1, p0, Lcom/android/calendar/agenda/an;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 944
    return-void
.end method
