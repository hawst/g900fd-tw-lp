.class Lcom/android/calendar/agenda/s;
.super Ljava/lang/Object;
.source "AgendaByDayAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/r;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/r;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    invoke-static {v0}, Lcom/android/calendar/agenda/r;->a(Lcom/android/calendar/agenda/r;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 103
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    iget-object v1, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    invoke-static {v1}, Lcom/android/calendar/agenda/r;->a(Lcom/android/calendar/agenda/r;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/r;->a(Lcom/android/calendar/agenda/r;Ljava/lang/String;)Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    iget-object v0, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    invoke-static {v0}, Lcom/android/calendar/agenda/r;->a(Lcom/android/calendar/agenda/r;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Lcom/android/calendar/agenda/r;->b(Lcom/android/calendar/agenda/r;Ljava/lang/String;)Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    invoke-static {v0}, Lcom/android/calendar/agenda/r;->c(Lcom/android/calendar/agenda/r;)Landroid/text/format/Time;

    move-result-object v1

    monitor-enter v1

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    invoke-static {v0}, Lcom/android/calendar/agenda/r;->c(Lcom/android/calendar/agenda/r;)Landroid/text/format/Time;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    invoke-static {v2}, Lcom/android/calendar/agenda/r;->b(Lcom/android/calendar/agenda/r;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 102
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/s;->a:Lcom/android/calendar/agenda/r;

    invoke-static {v0}, Lcom/android/calendar/agenda/r;->b(Lcom/android/calendar/agenda/r;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
