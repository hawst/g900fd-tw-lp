.class final Lcom/android/calendar/agenda/aj;
.super Lcom/android/calendar/common/a/a;
.source "AgendaFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/z;


# direct methods
.method public constructor <init>(Lcom/android/calendar/agenda/z;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 1115
    iput-object p1, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    .line 1116
    invoke-direct {p0, p2, p3}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;I)V

    .line 1117
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 1121
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f(Z)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 1166
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1167
    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1168
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 1126
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/calendar/agenda/aj;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZI)V

    .line 1127
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1131
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e(Z)V

    .line 1132
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 1137
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->w()Z

    move-result v0

    return v0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1146
    invoke-virtual {p0}, Lcom/android/calendar/agenda/aj;->f()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1151
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->c:Landroid/view/Menu;

    if-nez v0, :cond_0

    .line 1162
    :goto_0
    return-void

    .line 1154
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f(Z)I

    move-result v3

    .line 1155
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f(Z)I

    move-result v0

    .line 1156
    iget-object v4, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v4}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v4

    invoke-virtual {v4, v1, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZZ)I

    move-result v4

    .line 1157
    iget-object v5, p0, Lcom/android/calendar/agenda/aj;->c:Landroid/view/Menu;

    const v6, 0x7f120323

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-ne v3, v1, :cond_1

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1158
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->c:Landroid/view/Menu;

    const v5, 0x7f120324

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-lez v4, :cond_2

    if-ne v3, v4, :cond_2

    move v0, v1

    :goto_2
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1159
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->c:Landroid/view/Menu;

    const v4, 0x7f120326

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-lez v3, :cond_3

    const/16 v0, 0x12c

    if-gt v3, v0, :cond_3

    move v0, v1

    :goto_3
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1160
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->c:Landroid/view/Menu;

    const v4, 0x7f1200d4

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1161
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->c:Landroid/view/Menu;

    const v4, 0x7f120325

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/dz;->E()Z

    move-result v4

    if-eqz v4, :cond_4

    if-ne v3, v1, :cond_4

    :goto_4
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1157
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1158
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1159
    goto :goto_3

    :cond_4
    move v1, v2

    .line 1161
    goto :goto_4
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1207
    iget-object v2, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v2}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h(Z)Ljava/util/HashSet;

    move-result-object v3

    .line 1208
    iget-object v2, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v2}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h(Z)Ljava/util/HashSet;

    move-result-object v4

    .line 1210
    new-instance v5, Lcom/android/calendar/agenda/ak;

    invoke-direct {v5, p0}, Lcom/android/calendar/agenda/ak;-><init>(Lcom/android/calendar/agenda/aj;)V

    .line 1218
    const/4 v2, 0x0

    .line 1219
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 1276
    :goto_0
    return v0

    .line 1222
    :sswitch_0
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1223
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-object v2, v0

    .line 1225
    :cond_0
    if-eqz v2, :cond_1

    .line 1226
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0, v2}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V

    .line 1227
    if-eqz p1, :cond_1

    .line 1228
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    :cond_1
    :goto_1
    move v0, v1

    .line 1276
    goto :goto_0

    .line 1235
    :sswitch_1
    if-eqz p1, :cond_1

    .line 1236
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto :goto_1

    .line 1241
    :sswitch_2
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1243
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1244
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 1246
    :goto_2
    if-eqz v0, :cond_1

    .line 1247
    iget-object v2, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v2, v0, v5}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;Ljava/lang/Runnable;)V

    goto :goto_1

    .line 1251
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    iget-object v2, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v2}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v2

    invoke-virtual {v2, v1, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(ZZ)[Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v3}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->w()Z

    move-result v3

    invoke-static {v0, v2, v3, v5}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;[Ljava/lang/Long;ZLjava/lang/Runnable;)V

    goto :goto_1

    .line 1258
    :sswitch_3
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    iget-object v2, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v2}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->y()[Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2, v5}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;[Ljava/lang/Long;Ljava/lang/Runnable;)V

    goto :goto_1

    .line 1262
    :sswitch_4
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1263
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 1265
    :goto_3
    if-eqz v0, :cond_1

    .line 1266
    iget-object v2, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v2, v0}, Lcom/android/calendar/agenda/z;->b(Lcom/android/calendar/agenda/z;Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;)V

    .line 1267
    if-eqz p1, :cond_1

    .line 1268
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_3

    :cond_4
    move-object v0, v2

    goto :goto_2

    .line 1219
    :sswitch_data_0
    .sparse-switch
        0x7f1200d4 -> :sswitch_1
        0x7f120323 -> :sswitch_0
        0x7f120324 -> :sswitch_2
        0x7f120325 -> :sswitch_4
        0x7f120326 -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 14

    .prologue
    .line 1172
    invoke-super/range {p0 .. p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 1173
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0, p1}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 1174
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/z;->b(Lcom/android/calendar/agenda/z;Z)V

    .line 1175
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaListView;->setActionMode(Landroid/view/ActionMode;)V

    .line 1176
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/view/ActionMode;)V

    .line 1177
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    new-instance v1, Lcom/android/calendar/c/a;

    iget-object v2, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/z;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v3}, Lcom/android/calendar/agenda/z;->l(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/c/d;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/calendar/c/a;-><init>(Landroid/app/Activity;Lcom/android/calendar/c/d;)V

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;Lcom/android/calendar/c/a;)Lcom/android/calendar/c/a;

    .line 1178
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->d(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/c/a;->b()V

    .line 1180
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->m(Lcom/android/calendar/agenda/z;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 1181
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->m(Lcom/android/calendar/agenda/z;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->b(Z)V

    .line 1184
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->n(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/al;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    const-wide v2, 0x200000000L

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, -0x1

    const/4 v9, 0x1

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1187
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 1281
    invoke-super {p0, p1}, Lcom/android/calendar/common/a/a;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 1282
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0, v4}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 1283
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/calendar/agenda/AgendaListView;->setActionMode(Landroid/view/ActionMode;)V

    .line 1284
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->c(Landroid/view/ActionMode;)V

    .line 1286
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->d(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/c/a;->c()V

    .line 1287
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0, v4}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;Lcom/android/calendar/c/a;)Lcom/android/calendar/c/a;

    .line 1289
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->m(Lcom/android/calendar/agenda/z;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 1290
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->m(Lcom/android/calendar/agenda/z;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v9}, Lcom/android/calendar/AllInOneActivity;->b(Z)V

    .line 1292
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/z;->b(Lcom/android/calendar/agenda/z;Z)V

    .line 1293
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->o(Lcom/android/calendar/agenda/z;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1294
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->n(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/al;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    const-wide v2, 0x200000000L

    const-wide/16 v7, -0x1

    const-wide/16 v10, 0x1

    move-object v5, v4

    move-object v6, v4

    move-object v12, v4

    move-object v13, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1295
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(Landroid/view/ActionMode;)V

    .line 1196
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a()I

    move-result v0

    .line 1197
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->f(Lcom/android/calendar/agenda/z;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1198
    iget-object v0, p0, Lcom/android/calendar/agenda/aj;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->f(Lcom/android/calendar/agenda/z;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1199
    const/4 v0, 0x1

    .line 1202
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method
