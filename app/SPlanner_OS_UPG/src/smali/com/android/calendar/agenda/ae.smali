.class Lcom/android/calendar/agenda/ae;
.super Ljava/lang/Object;
.source "AgendaFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/z;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/z;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 359
    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->j(Lcom/android/calendar/agenda/z;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->w()Z

    move-result v3

    .line 361
    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->j(Lcom/android/calendar/agenda/z;)Landroid/widget/CheckBox;

    move-result-object v4

    if-nez v3, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 362
    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    if-nez v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e(Z)V

    .line 363
    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->f(Lcom/android/calendar/agenda/z;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/android/calendar/agenda/ae;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->f(Lcom/android/calendar/agenda/z;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 368
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 361
    goto :goto_0

    :cond_3
    move v1, v2

    .line 362
    goto :goto_1
.end method
