.class Lcom/android/calendar/agenda/x;
.super Ljava/lang/Object;
.source "AgendaDeleteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaDeleteActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaDeleteActivity;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/android/calendar/agenda/x;->a:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/calendar/agenda/x;->a:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    .line 65
    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/agenda/x;->a:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    iget-object v1, v1, Lcom/android/calendar/agenda/AgendaDeleteActivity;->d:Landroid/text/format/Time;

    monitor-enter v1

    .line 67
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/agenda/x;->a:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    iget-object v2, v2, Lcom/android/calendar/agenda/AgendaDeleteActivity;->d:Landroid/text/format/Time;

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 68
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    return-void

    .line 65
    :cond_0
    invoke-static {v0, p0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
