.class Lcom/android/calendar/agenda/bf;
.super Ljava/lang/Object;
.source "AgendaSDExportActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaSDExportActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/android/calendar/agenda/bf;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 391
    iget-object v0, p0, Lcom/android/calendar/agenda/bf;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0, v7}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->a(Lcom/android/calendar/agenda/AgendaSDExportActivity;Z)Z

    .line 392
    iget-object v0, p0, Lcom/android/calendar/agenda/bf;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0, v8}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b(Lcom/android/calendar/agenda/AgendaSDExportActivity;Z)Z

    .line 393
    iget-object v0, p0, Lcom/android/calendar/agenda/bf;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    const/16 v1, 0x3ed

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->removeDialog(I)V

    .line 394
    iget-object v0, p0, Lcom/android/calendar/agenda/bf;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x24dc87

    const v3, 0x259d23

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/android/calendar/agenda/bf;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v6}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->f(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 395
    iget-object v0, p0, Lcom/android/calendar/agenda/bf;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/bb;

    .line 397
    invoke-virtual {v0, v8}, Lcom/android/calendar/agenda/bb;->e(Z)V

    .line 398
    return-void
.end method
