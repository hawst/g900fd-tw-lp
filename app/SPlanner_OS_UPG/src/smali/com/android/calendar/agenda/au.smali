.class Lcom/android/calendar/agenda/au;
.super Ljava/lang/Object;
.source "AgendaPickActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaPickActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/AgendaPickActivity;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/calendar/agenda/au;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/calendar/agenda/au;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    .line 139
    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 140
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/agenda/au;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v1}, Lcom/android/calendar/agenda/AgendaPickActivity;->a(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/text/format/Time;

    move-result-object v1

    monitor-enter v1

    .line 141
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/agenda/au;->a:Lcom/android/calendar/agenda/AgendaPickActivity;

    invoke-static {v2}, Lcom/android/calendar/agenda/AgendaPickActivity;->a(Lcom/android/calendar/agenda/AgendaPickActivity;)Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 142
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    return-void

    .line 139
    :cond_0
    invoke-static {v0, p0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 142
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
