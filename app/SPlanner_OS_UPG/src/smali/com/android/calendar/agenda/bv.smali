.class Lcom/android/calendar/agenda/bv;
.super Landroid/os/AsyncTask;
.source "AgendaWindowAdapter.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/bt;


# direct methods
.method private constructor <init>(Lcom/android/calendar/agenda/bt;)V
    .locals 0

    .prologue
    .line 646
    iput-object p1, p0, Lcom/android/calendar/agenda/bv;->a:Lcom/android/calendar/agenda/bt;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/agenda/bt;Lcom/android/calendar/agenda/bu;)V
    .locals 0

    .prologue
    .line 646
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/bv;-><init>(Lcom/android/calendar/agenda/bt;)V

    return-void
.end method


# virtual methods
.method protected a([Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 663
    aget-object v0, p1, v2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 664
    aget-object v0, p1, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 665
    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 667
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/bv;->a:Lcom/android/calendar/agenda/bt;

    invoke-static {v0}, Lcom/android/calendar/agenda/bt;->b(Lcom/android/calendar/agenda/bt;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v3, v1

    .line 668
    :goto_0
    if-eqz v5, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/bv;->a:Lcom/android/calendar/agenda/bt;

    invoke-static {v0}, Lcom/android/calendar/agenda/bt;->b(Lcom/android/calendar/agenda/bt;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 670
    :goto_1
    iget-object v5, p0, Lcom/android/calendar/agenda/bv;->a:Lcom/android/calendar/agenda/bt;

    invoke-static {v5, v3, v0}, Lcom/android/calendar/agenda/bt;->a(Lcom/android/calendar/agenda/bt;ZZ)V

    .line 671
    iget-object v5, p0, Lcom/android/calendar/agenda/bv;->a:Lcom/android/calendar/agenda/bt;

    if-eqz v4, :cond_2

    if-nez v0, :cond_2

    if-nez v3, :cond_2

    :goto_2
    invoke-static {v5, v1}, Lcom/android/calendar/agenda/bt;->a(Lcom/android/calendar/agenda/bt;Z)V

    .line 672
    return-void

    :cond_0
    move v3, v2

    .line 667
    goto :goto_0

    :cond_1
    move v0, v2

    .line 668
    goto :goto_1

    :cond_2
    move v1, v2

    .line 671
    goto :goto_2
.end method

.method protected varargs a([Ljava/lang/Integer;)[Ljava/lang/Boolean;
    .locals 8

    .prologue
    const v7, 0x259d23

    const v6, 0x24dc87

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 649
    aget-object v0, p1, v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 650
    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 652
    iget-object v3, p0, Lcom/android/calendar/agenda/bv;->a:Lcom/android/calendar/agenda/bt;

    iget v3, v3, Lcom/android/calendar/agenda/bt;->l:I

    if-nez v3, :cond_0

    move v4, v1

    .line 653
    :goto_0
    if-le v0, v6, :cond_1

    iget-object v3, p0, Lcom/android/calendar/agenda/bv;->a:Lcom/android/calendar/agenda/bt;

    add-int/lit8 v0, v0, -0x1

    invoke-static {v3, v6, v0}, Lcom/android/calendar/agenda/bt;->a(Lcom/android/calendar/agenda/bt;II)Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v1

    .line 654
    :goto_1
    if-ge v5, v7, :cond_2

    iget-object v0, p0, Lcom/android/calendar/agenda/bv;->a:Lcom/android/calendar/agenda/bt;

    add-int/lit8 v5, v5, 0x1

    invoke-static {v0, v5, v7}, Lcom/android/calendar/agenda/bt;->a(Lcom/android/calendar/agenda/bt;II)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 656
    :goto_2
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v5, v2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v1

    .line 658
    return-object v5

    :cond_0
    move v4, v2

    .line 652
    goto :goto_0

    :cond_1
    move v3, v2

    .line 653
    goto :goto_1

    :cond_2
    move v0, v2

    .line 654
    goto :goto_2
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 646
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/bv;->a([Ljava/lang/Integer;)[Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 646
    check-cast p1, [Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/bv;->a([Ljava/lang/Boolean;)V

    return-void
.end method
