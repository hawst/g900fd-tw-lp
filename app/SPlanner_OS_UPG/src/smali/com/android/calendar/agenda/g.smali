.class Lcom/android/calendar/agenda/g;
.super Ljava/lang/Object;
.source "AgendaAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/b;

.field private b:Landroid/database/Cursor;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/android/calendar/agenda/b;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 724
    iput-object p1, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 722
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/g;->c:Z

    .line 725
    iput-object p2, p0, Lcom/android/calendar/agenda/g;->b:Landroid/database/Cursor;

    .line 726
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 13

    .prologue
    const/16 v12, 0x3d

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 791
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->b:Landroid/database/Cursor;

    check-cast v0, Lcom/android/calendar/agenda/w;

    .line 792
    const-string v7, ""

    .line 793
    invoke-virtual {v0}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 794
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    .line 795
    const-string v1, "_id"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 797
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 799
    const-wide/16 v8, 0x0

    .line 801
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "accountKey"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 805
    if-eqz v1, :cond_d

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 806
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    move-result-wide v2

    .line 809
    :goto_0
    if-eqz v1, :cond_0

    .line 810
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 813
    :cond_0
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 814
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    invoke-virtual {v10, v11, v0}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 816
    :cond_1
    const-string v0, "_id"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 817
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 820
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "displayName"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 824
    if-eqz v6, :cond_c

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 825
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v7

    move-object v0, v7

    .line 828
    :goto_1
    if-eqz v6, :cond_2

    .line 829
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 869
    :cond_2
    :goto_2
    return-object v0

    .line 809
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_3

    .line 810
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 828
    :catchall_1
    move-exception v0

    if-eqz v6, :cond_4

    .line 829
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 832
    :cond_5
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    .line 833
    const-string v0, "_id"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/agenda/g;->b:Landroid/database/Cursor;

    sget-object v2, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 834
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 836
    const-wide/16 v8, 0x0

    .line 838
    :try_start_3
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "calendar_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    .line 842
    if-eqz v1, :cond_b

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 843
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result-wide v2

    .line 846
    :goto_4
    if-eqz v1, :cond_6

    .line 847
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 850
    :cond_6
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 851
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    invoke-virtual {v10, v11, v0}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 853
    :cond_7
    const-string v0, "_id"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 854
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 857
    :try_start_5
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "calendar_displayName"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 861
    if-eqz v6, :cond_a

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 862
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v0

    .line 865
    :goto_5
    if-eqz v6, :cond_2

    .line 866
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 846
    :catchall_2
    move-exception v0

    :goto_6
    if-eqz v6, :cond_8

    .line 847
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 865
    :catchall_3
    move-exception v0

    if-eqz v6, :cond_9

    .line 866
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    .line 846
    :catchall_4
    move-exception v0

    move-object v6, v1

    goto :goto_6

    .line 809
    :catchall_5
    move-exception v0

    goto/16 :goto_3

    :cond_a
    move-object v0, v7

    goto :goto_5

    :cond_b
    move-wide v2, v8

    goto :goto_4

    :cond_c
    move-object v0, v7

    goto/16 :goto_1

    :cond_d
    move-wide v2, v8

    goto/16 :goto_0
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x32

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 730
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->c(Landroid/content/Context;)Z

    move-result v0

    .line 731
    iget-object v1, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v1}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/dz;->d(Landroid/content/Context;)Z

    move-result v1

    .line 733
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 787
    :cond_0
    :goto_0
    return v6

    .line 736
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/g;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 739
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 742
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 743
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 744
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0034

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 750
    :goto_1
    const-string v3, "translationX"

    new-array v4, v5, [F

    aput v1, v4, v6

    aput v0, v4, v7

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 751
    const-string v4, "translationX"

    new-array v5, v5, [F

    aput v0, v5, v6

    aput v1, v5, v7

    invoke-static {p1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 752
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->b:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 753
    const v0, 0x7f120043

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 755
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 763
    :pswitch_1
    invoke-static {}, Lcom/android/calendar/agenda/b;->c()I

    .line 764
    invoke-static {}, Lcom/android/calendar/agenda/b;->b()I

    move-result v2

    const/4 v4, 0x7

    if-ne v2, v4, :cond_0

    .line 765
    iget-boolean v2, p0, Lcom/android/calendar/agenda/g;->c:Z

    if-nez v2, :cond_0

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 766
    invoke-static {v6}, Lcom/android/calendar/agenda/b;->c(I)I

    .line 767
    invoke-virtual {v3, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 768
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->start()V

    .line 769
    iput-boolean v7, p0, Lcom/android/calendar/agenda/g;->c:Z

    .line 770
    invoke-direct {p0}, Lcom/android/calendar/agenda/g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 746
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/g;->a:Lcom/android/calendar/agenda/b;

    invoke-static {v0}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 747
    const/4 v0, 0x0

    goto :goto_1

    .line 758
    :pswitch_2
    invoke-static {}, Lcom/android/calendar/agenda/b;->b()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 759
    invoke-static {v6}, Lcom/android/calendar/agenda/b;->c(I)I

    goto/16 :goto_0

    .line 776
    :pswitch_3
    invoke-static {v6}, Lcom/android/calendar/agenda/b;->c(I)I

    .line 777
    iget-boolean v2, p0, Lcom/android/calendar/agenda/g;->c:Z

    if-eqz v2, :cond_0

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    .line 779
    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 780
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 781
    iput-boolean v6, p0, Lcom/android/calendar/agenda/g;->c:Z

    .line 783
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 755
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
