.class public Lcom/android/calendar/agenda/w;
.super Landroid/database/AbstractWindowedCursor;
.source "AgendaCursor.java"


# static fields
.field public static final a:Ljava/lang/Integer;

.field public static final b:Ljava/lang/Integer;

.field public static final c:Ljava/lang/Integer;

.field public static final d:Ljava/lang/Integer;


# instance fields
.field private e:Landroid/database/Cursor;

.field private f:Landroid/database/Cursor;

.field private g:Landroid/database/Cursor;

.field private final h:Ljava/util/Vector;

.field private final i:Landroid/util/SparseArray;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:J

.field private o:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/16 v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    .line 22
    const/16 v0, 0x3e9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/w;->b:Ljava/lang/Integer;

    .line 23
    const/16 v0, 0x3ea

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/w;->c:Ljava/lang/Integer;

    .line 24
    const/16 v0, 0x3eb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/w;->d:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 48
    invoke-direct {p0}, Landroid/database/AbstractWindowedCursor;-><init>()V

    .line 37
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/w;->h:Ljava/util/Vector;

    .line 40
    iput v4, p0, Lcom/android/calendar/agenda/w;->j:I

    .line 41
    iput v4, p0, Lcom/android/calendar/agenda/w;->k:I

    .line 42
    iput v4, p0, Lcom/android/calendar/agenda/w;->l:I

    .line 43
    iput v4, p0, Lcom/android/calendar/agenda/w;->m:I

    .line 45
    iput-wide v2, p0, Lcom/android/calendar/agenda/w;->n:J

    .line 46
    iput-wide v2, p0, Lcom/android/calendar/agenda/w;->o:J

    .line 49
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    .line 50
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    sget-object v1, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-array v2, v6, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    const/16 v1, 0x3f0

    new-array v2, v6, [Ljava/lang/Integer;

    const/16 v3, 0xd

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 58
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    sget-object v1, Lcom/android/calendar/agenda/w;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-array v2, v6, [Ljava/lang/Integer;

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    sget-object v1, Lcom/android/calendar/agenda/w;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-array v2, v6, [Ljava/lang/Integer;

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    sget-object v1, Lcom/android/calendar/agenda/w;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-array v2, v6, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    const/16 v1, 0x3ef

    new-array v2, v6, [Ljava/lang/Integer;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 74
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 124
    :goto_1
    return v0

    .line 116
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 118
    goto :goto_1

    :cond_2
    move v0, v1

    .line 121
    goto :goto_1

    .line 123
    :catch_0
    move-exception v0

    move v0, v1

    .line 124
    goto :goto_1
.end method

.method private a(Z)Z
    .locals 2

    .prologue
    .line 291
    const/4 v0, 0x0

    .line 292
    iget-object v1, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 293
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 295
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 296
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 298
    :cond_1
    :goto_1
    return v0

    .line 293
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    goto :goto_0

    .line 296
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    goto :goto_1
.end method

.method private b(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 173
    iget v0, p0, Lcom/android/calendar/agenda/w;->mPos:I

    if-eq v2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/agenda/w;->mPos:I

    if-ne v0, v1, :cond_1

    .line 174
    :cond_0
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    iget v1, p0, Lcom/android/calendar/agenda/w;->mPos:I

    invoke-virtual {p0}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(II)V

    throw v0

    .line 176
    :cond_1
    if-ne p1, v2, :cond_2

    .line 177
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CursorIndexOutOfBoundsException is Caught: Index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "requested"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_2
    return-void
.end method

.method private c(I)V
    .locals 6

    .prologue
    .line 315
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/agenda/w;->m:I

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/android/calendar/agenda/w;->j:I

    if-lt p1, v0, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 320
    const-string v0, "1"

    .line 321
    invoke-virtual {p0}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 322
    iget-object v1, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 323
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 324
    :cond_2
    const-string v0, "0"

    .line 327
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/agenda/w;->h:Ljava/util/Vector;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget v0, p0, Lcom/android/calendar/agenda/w;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/agenda/w;->m:I

    goto :goto_0

    .line 327
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private d()V
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget v0, p0, Lcom/android/calendar/agenda/w;->j:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/calendar/agenda/w;->k:I

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_3

    .line 141
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    goto :goto_0

    .line 144
    :cond_3
    iget v0, p0, Lcom/android/calendar/agenda/w;->l:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_4

    .line 145
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    goto :goto_0

    .line 148
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    .line 149
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iget-object v1, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    const-string v3, "utc_due_date"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/w;->n:J

    .line 152
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_6

    .line 153
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/agenda/w;->o:J

    .line 155
    :cond_6
    iget-wide v0, p0, Lcom/android/calendar/agenda/w;->o:J

    iget-wide v2, p0, Lcom/android/calendar/agenda/w;->n:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    .line 156
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    .line 161
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/agenda/w;->mPos:I

    goto :goto_0

    .line 158
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    goto :goto_1
.end method

.method private d(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 333
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 334
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 335
    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 336
    if-ne v3, v1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    :goto_0
    iput-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    .line 337
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    if-ltz v2, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v2, v0, :cond_2

    .line 343
    :cond_0
    :goto_1
    return-void

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    goto :goto_0

    .line 340
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1
.end method

.method private e()Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 302
    iget v0, p0, Lcom/android/calendar/agenda/w;->j:I

    if-nez v0, :cond_0

    .line 303
    const/4 v0, 0x0

    .line 311
    :goto_0
    return-object v0

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    goto :goto_0

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-nez v0, :cond_2

    .line 309
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    goto :goto_0

    .line 311
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 77
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/agenda/w;->mPos:I

    .line 81
    iput-object p1, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    .line 82
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 83
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/w;->l:I

    .line 84
    iget v0, p0, Lcom/android/calendar/agenda/w;->l:I

    iget v1, p0, Lcom/android/calendar/agenda/w;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/agenda/w;->j:I

    .line 85
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 86
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->h:Ljava/util/Vector;

    iget v1, p0, Lcom/android/calendar/agenda/w;->j:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 87
    iget v0, p0, Lcom/android/calendar/agenda/w;->j:I

    iput v0, p0, Lcom/android/calendar/agenda/w;->m:I

    .line 88
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 91
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/agenda/w;->d()V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/agenda/w;->mPos:I

    .line 134
    return-void
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 95
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/agenda/w;->mPos:I

    .line 99
    iput-object p1, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    .line 100
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 101
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/w;->k:I

    .line 102
    iget v0, p0, Lcom/android/calendar/agenda/w;->l:I

    iget v1, p0, Lcom/android/calendar/agenda/w;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/agenda/w;->j:I

    .line 103
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 104
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->h:Ljava/util/Vector;

    iget v1, p0, Lcom/android/calendar/agenda/w;->j:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 105
    iget v0, p0, Lcom/android/calendar/agenda/w;->j:I

    iput v0, p0, Lcom/android/calendar/agenda/w;->m:I

    .line 106
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 109
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/agenda/w;->d()V

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/android/calendar/agenda/w;->k:I

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 281
    invoke-super {p0}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 282
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 288
    :cond_1
    return-void
.end method

.method public fillWindow(ILandroid/database/CursorWindow;)V
    .locals 0

    .prologue
    .line 385
    return-void
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 355
    iget v0, p0, Lcom/android/calendar/agenda/w;->j:I

    return v0
.end method

.method public getInt(I)I
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    :cond_0
    :goto_0
    return p1

    .line 186
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/w;->b(I)V

    .line 187
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/w;->a(I)I

    move-result v0

    .line 188
    iget-object v1, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    :goto_1
    invoke-interface {v1, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result p1

    goto :goto_0

    :cond_2
    move p1, v0

    goto :goto_1
.end method

.method public getLong(I)J
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    :cond_0
    int-to-long v0, p1

    .line 198
    :goto_0
    return-wide v0

    .line 196
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/w;->b(I)V

    .line 197
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/w;->a(I)I

    move-result v0

    .line 198
    iget-object v1, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    :goto_1
    invoke-interface {v1, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    :cond_2
    move p1, v0

    goto :goto_1
.end method

.method public getString(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/w;->b(I)V

    .line 361
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/w;->a(I)I

    move-result v0

    .line 362
    iget-object v1, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    :goto_0
    invoke-interface {v1, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move p1, v0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 367
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/w;->b(I)V

    .line 368
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 8

    .prologue
    .line 203
    iget v0, p0, Lcom/android/calendar/agenda/w;->m:I

    if-nez v0, :cond_0

    .line 204
    invoke-direct {p0, p2}, Lcom/android/calendar/agenda/w;->d(I)V

    .line 205
    const/4 v0, 0x1

    .line 276
    :goto_0
    return v0

    .line 207
    :cond_0
    sub-int v3, p2, p1

    .line 208
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 211
    if-nez p2, :cond_1

    .line 212
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/agenda/w;->a(Z)Z

    .line 213
    invoke-direct {p0}, Lcom/android/calendar/agenda/w;->d()V

    .line 214
    invoke-direct {p0, p2}, Lcom/android/calendar/agenda/w;->c(I)V

    .line 215
    const/4 v0, 0x1

    goto :goto_0

    .line 218
    :cond_1
    iget v0, p0, Lcom/android/calendar/agenda/w;->k:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/calendar/agenda/w;->l:I

    if-nez v0, :cond_4

    .line 219
    :cond_2
    iget v0, p0, Lcom/android/calendar/agenda/w;->k:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    :goto_1
    iput-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    .line 220
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    add-int/2addr v1, v3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 221
    invoke-direct {p0, p2}, Lcom/android/calendar/agenda/w;->c(I)V

    .line 222
    const/4 v0, 0x1

    goto :goto_0

    .line 219
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    goto :goto_1

    .line 226
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-nez v0, :cond_5

    .line 227
    const/4 v0, 0x0

    goto :goto_0

    .line 230
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    move v0, v2

    :goto_2
    if-ge v1, v2, :cond_18

    .line 231
    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-nez v4, :cond_7

    .line 230
    :cond_6
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 234
    :cond_7
    if-lez v3, :cond_8

    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_9

    :cond_8
    if-gez v3, :cond_c

    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 235
    :cond_9
    invoke-direct {p0}, Lcom/android/calendar/agenda/w;->e()Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    .line 236
    iget-object v1, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    if-eqz v1, :cond_b

    .line 237
    iget-object v1, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    if-lez v3, :cond_a

    add-int/lit8 v0, v0, -0x1

    :goto_4
    add-int/2addr v0, v2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 238
    invoke-direct {p0, p2}, Lcom/android/calendar/agenda/w;->c(I)V

    .line 239
    const/4 v0, 0x1

    goto :goto_0

    .line 237
    :cond_a
    neg-int v0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 241
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 244
    :cond_c
    if-lez v3, :cond_e

    .line 245
    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    .line 249
    :goto_5
    add-int/lit8 v0, v0, -0x1

    .line 250
    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_d

    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 251
    :cond_d
    invoke-virtual {p0}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 252
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_f

    const-wide v0, 0x7fffffffffffffffL

    :goto_6
    iput-wide v0, p0, Lcom/android/calendar/agenda/w;->n:J

    .line 256
    :goto_7
    invoke-direct {p0}, Lcom/android/calendar/agenda/w;->e()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    .line 257
    invoke-direct {p0, p2}, Lcom/android/calendar/agenda/w;->c(I)V

    .line 258
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 247
    :cond_e
    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToPrevious()Z

    goto :goto_5

    .line 252
    :cond_f
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_6

    .line 254
    :cond_10
    iget-object v0, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_11

    const-wide v0, 0x7fffffffffffffffL

    :goto_8
    iput-wide v0, p0, Lcom/android/calendar/agenda/w;->o:J

    goto :goto_7

    :cond_11
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_8

    .line 260
    :cond_12
    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-ne v4, v5, :cond_17

    .line 261
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    iget-object v5, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    const-string v7, "utc_due_date"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/agenda/w;->n:J

    .line 266
    :goto_9
    if-lez v3, :cond_14

    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-ne v4, v5, :cond_13

    iget-wide v4, p0, Lcom/android/calendar/agenda/w;->n:J

    iget-wide v6, p0, Lcom/android/calendar/agenda/w;->o:J

    cmp-long v4, v4, v6

    if-gtz v4, :cond_16

    :cond_13
    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-ne v4, v5, :cond_14

    iget-wide v4, p0, Lcom/android/calendar/agenda/w;->n:J

    iget-wide v6, p0, Lcom/android/calendar/agenda/w;->o:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_16

    :cond_14
    if-gez v3, :cond_6

    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/android/calendar/agenda/w;->e:Landroid/database/Cursor;

    if-ne v4, v5, :cond_15

    iget-wide v4, p0, Lcom/android/calendar/agenda/w;->n:J

    iget-wide v6, p0, Lcom/android/calendar/agenda/w;->o:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_16

    :cond_15
    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/android/calendar/agenda/w;->f:Landroid/database/Cursor;

    if-ne v4, v5, :cond_6

    iget-wide v4, p0, Lcom/android/calendar/agenda/w;->n:J

    iget-wide v6, p0, Lcom/android/calendar/agenda/w;->o:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_6

    .line 272
    :cond_16
    invoke-direct {p0}, Lcom/android/calendar/agenda/w;->e()Landroid/database/Cursor;

    move-result-object v4

    iput-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    goto/16 :goto_3

    .line 264
    :cond_17
    iget-object v4, p0, Lcom/android/calendar/agenda/w;->g:Landroid/database/Cursor;

    const/4 v5, 0x7

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/agenda/w;->o:J

    goto :goto_9

    .line 275
    :cond_18
    invoke-direct {p0, p2}, Lcom/android/calendar/agenda/w;->c(I)V

    .line 276
    const/4 v0, 0x1

    goto/16 :goto_0
.end method
