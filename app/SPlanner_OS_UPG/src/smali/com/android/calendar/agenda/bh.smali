.class final Lcom/android/calendar/agenda/bh;
.super Lcom/android/calendar/common/a/a;
.source "AgendaSDExportActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaSDExportActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    .line 491
    invoke-direct {p0, p2}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;)V

    .line 492
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->f(Z)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 534
    const v0, 0x7f1200d5

    const v1, 0x7f0f0398

    invoke-interface {p2, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 535
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->e(Z)V

    .line 502
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->e(Z)V

    .line 507
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->notifyDataSetChanged()V

    .line 525
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->w()Z

    move-result v0

    return v0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 529
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bh;->f()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 516
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bh;->a()I

    move-result v0

    .line 517
    iget-object v3, p0, Lcom/android/calendar/agenda/bh;->c:Landroid/view/Menu;

    const v4, 0x7f1200d5

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 518
    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 519
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->a()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/bh;->b(Z)V

    .line 520
    return-void

    :cond_0
    move v0, v2

    .line 518
    goto :goto_0

    :cond_1
    move v1, v2

    .line 519
    goto :goto_1
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 539
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1200d5

    if-ne v0, v1, :cond_0

    .line 540
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b()V

    .line 541
    const/4 v0, 0x1

    .line 543
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 548
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 549
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaListView;->setActionMode(Landroid/view/ActionMode;)V

    .line 550
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->a(Landroid/view/ActionMode;)V

    .line 551
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 562
    invoke-super {p0, p1}, Lcom/android/calendar/common/a/a;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 563
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->a(Lcom/android/calendar/agenda/AgendaSDExportActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 564
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->a(Lcom/android/calendar/agenda/AgendaSDExportActivity;Lcom/android/calendar/agenda/bh;)Lcom/android/calendar/agenda/bh;

    .line 565
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setActionMode(Landroid/view/ActionMode;)V

    .line 566
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->c(Landroid/view/ActionMode;)V

    .line 567
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/android/calendar/agenda/bh;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/bb;->b(Landroid/view/ActionMode;)V

    .line 557
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
