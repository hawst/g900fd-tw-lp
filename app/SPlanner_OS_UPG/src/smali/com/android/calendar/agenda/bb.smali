.class public Lcom/android/calendar/agenda/bb;
.super Lcom/android/calendar/agenda/AgendaBaseAdapter;
.source "AgendaPickAdapter.java"


# static fields
.field private static final ac:Ljava/lang/String;


# instance fields
.field protected W:I

.field protected X:I

.field protected Y:I

.field protected Z:J

.field protected final aa:I

.field protected ab:Lcom/android/calendar/agenda/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/bb;->ac:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;IZI)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p4}, Lcom/android/calendar/agenda/AgendaBaseAdapter;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;Z)V

    .line 69
    check-cast p1, Lcom/android/calendar/agenda/a;

    iput-object p1, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    .line 70
    iput p3, p0, Lcom/android/calendar/agenda/bb;->W:I

    .line 71
    iput p5, p0, Lcom/android/calendar/agenda/bb;->aa:I

    .line 72
    return-void
.end method


# virtual methods
.method protected E()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/android/calendar/agenda/bb;->b:[Ljava/lang/String;

    return-object v0
.end method

.method protected F()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/android/calendar/agenda/bb;->c:[Ljava/lang/String;

    return-object v0
.end method

.method protected G()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    return-object v0
.end method

.method protected a(JJ)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x4

    .line 99
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 100
    const-string v1, "visible=1 AND deleted=0 AND (eventStatus!=2 OR eventStatus is null) AND calendar_access_level>= 500"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    iget-boolean v1, p0, Lcom/android/calendar/agenda/bb;->D:Z

    if-eqz v1, :cond_0

    .line 105
    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 107
    :cond_0
    iget-boolean v1, p0, Lcom/android/calendar/agenda/bb;->F:Z

    if-eqz v1, :cond_1

    .line 108
    const-string v1, " AND (rrule IS NULL OR rrule=\'\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 112
    :cond_2
    cmp-long v1, p1, v4

    if-eqz v1, :cond_3

    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND dtstart>="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 115
    :cond_3
    cmp-long v1, p3, v4

    if-eqz v1, :cond_4

    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND dtstart<="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 120
    :cond_4
    const-string v1, " AND contact_id IS NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 122
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    invoke-static {v1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v1

    .line 123
    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v1}, Lcom/android/calendar/al;->d()I

    move-result v2

    if-eq v2, v3, :cond_6

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND (title LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    :cond_5
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->g:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 126
    :cond_6
    invoke-virtual {v1}, Lcom/android/calendar/al;->d()I

    move-result v1

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND (sticker_type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method protected a(JJLjava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 161
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 162
    const-string v1, "visible=1 AND deleted=0 AND (eventStatus!=2 OR eventStatus is null)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    iget-boolean v1, p0, Lcom/android/calendar/agenda/bb;->D:Z

    if-eqz v1, :cond_0

    .line 168
    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 170
    :cond_0
    iget-boolean v1, p0, Lcom/android/calendar/agenda/bb;->F:Z

    if-eqz v1, :cond_1

    .line 171
    const-string v1, " AND (rrule IS NULL OR rrule=\'\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 174
    :cond_1
    cmp-long v1, p1, v4

    if-eqz v1, :cond_2

    .line 175
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND dtstart>="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 177
    :cond_2
    cmp-long v1, p3, v4

    if-eqz v1, :cond_3

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND dtstart<="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 181
    :cond_3
    if-eqz p5, :cond_4

    .line 182
    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "title LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 184
    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "description LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 186
    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eventLocation LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 188
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 193
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    if-nez v1, :cond_5

    .line 194
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    .line 197
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    invoke-virtual {v1}, Lcom/android/calendar/d/g;->e()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    instance-of v1, v1, Lcom/android/calendar/d/f;

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    instance-of v1, v1, Lcom/android/calendar/d/a;

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    instance-of v1, v1, Lcom/android/calendar/d/d;

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    instance-of v1, v1, Lcom/android/calendar/d/h;

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    instance-of v1, v1, Lcom/android/calendar/d/e;

    if-eqz v1, :cond_8

    .line 202
    :cond_6
    const-string v1, " AND calendar_id!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 204
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    invoke-virtual {v1}, Lcom/android/calendar/d/g;->f()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    invoke-virtual {v1}, Lcom/android/calendar/d/g;->g()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 206
    :cond_7
    const-string v1, " AND calendar_id!=3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 209
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->j:Lcom/android/calendar/d/g;

    invoke-virtual {v1}, Lcom/android/calendar/d/g;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 210
    const-string v1, " AND setLunar!=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 214
    :cond_9
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->g:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->l(Landroid/content/Context;)Z

    move-result v0

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    const-string v2, "selected=1 AND groupSelected=1 AND deleted = 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    if-eqz v0, :cond_0

    .line 224
    const-string v0, " AND complete = 0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_0
    if-eqz p1, :cond_1

    .line 228
    const-string v0, " AND ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "subject LIKE \'%"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "%\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const-string v0, " OR "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "body LIKE \'%"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "%\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 235
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 335
    iput-object p6, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    .line 340
    const/4 v0, -0x1

    if-ne p2, v0, :cond_b

    .line 341
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->i()I

    move-result v2

    .line 343
    :goto_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 344
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->j()I

    move-result p3

    .line 346
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->y:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 347
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/al;->b()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 348
    if-nez p1, :cond_a

    .line 349
    new-instance v3, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->y:Ljava/lang/String;

    invoke-direct {v3, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 350
    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 353
    :goto_1
    if-eqz v3, :cond_1

    move-object v0, v3

    .line 355
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 357
    if-nez p7, :cond_3

    invoke-virtual {p0, v1, v1}, Lcom/android/calendar/agenda/bb;->b(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 359
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v1, v0, p4, p5}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 360
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/bb;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 361
    if-ltz v0, :cond_2

    .line 362
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->k:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/AgendaListView;->getHeaderViewsCount()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaListView;->setSelection(I)V

    .line 387
    :cond_2
    :goto_2
    return-void

    .line 368
    :cond_3
    const/4 v0, -0x1

    if-eq v2, v0, :cond_4

    const/4 v0, -0x1

    if-ne p3, v0, :cond_9

    .line 370
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->k()I

    move-result v0

    add-int v2, v1, v0

    move v0, v1

    .line 373
    :goto_3
    if-ge v1, v0, :cond_5

    .line 375
    sub-int v0, v2, v1

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->k()I

    move-result v4

    if-le v0, v4, :cond_8

    .line 376
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->k()I

    move-result v0

    add-int v2, v1, v0

    move v0, v1

    .line 378
    :cond_5
    :goto_4
    if-le v1, v2, :cond_7

    .line 380
    sub-int v2, v1, v0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->k()I

    move-result v4

    if-le v2, v4, :cond_6

    .line 382
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->k()I

    move-result v0

    add-int v2, v1, v0

    .line 386
    :goto_5
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->f()I

    move-result v6

    move-object v0, p0

    move-object v4, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/agenda/bb;->a(IILandroid/text/format/Time;Ljava/lang/String;II)Z

    goto :goto_2

    :cond_6
    move v2, v1

    move v1, v0

    goto :goto_5

    :cond_7
    move v1, v0

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_4

    :cond_9
    move v0, v2

    move v2, p3

    goto :goto_3

    :cond_a
    move-object v3, p1

    goto :goto_1

    :cond_b
    move v2, p2

    goto/16 :goto_0
.end method

.method protected b(JJ)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 135
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 136
    const-string v1, "selected=1 AND deleted=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 137
    const-string v1, " AND _sync_account NOT LIKE \'task_personal%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->l(Landroid/content/Context;)Z

    move-result v1

    .line 140
    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 141
    :cond_0
    cmp-long v2, p1, v4

    if-eqz v2, :cond_1

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND due_date>="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    :cond_1
    cmp-long v2, p3, v4

    if-eqz v2, :cond_2

    .line 145
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND due_date<="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 149
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 150
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND (subject LIKE \'%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%\')"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 153
    :cond_3
    if-eqz v1, :cond_4

    .line 154
    const-string v1, " AND complete = 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 157
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 610
    iput-wide p1, p0, Lcom/android/calendar/agenda/bb;->Z:J

    .line 611
    return-void
.end method

.method protected declared-synchronized b(Lcom/android/calendar/agenda/q;)V
    .locals 11

    .prologue
    .line 391
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->o:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 392
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v1, v0, Lcom/android/calendar/agenda/m;->d:I

    .line 393
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->e:I

    .line 394
    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/agenda/bb;->c(II)I

    move-result v2

    .line 395
    iget v3, p1, Lcom/android/calendar/agenda/q;->f:I

    packed-switch v3, :pswitch_data_0

    .line 410
    :goto_0
    iget v2, p0, Lcom/android/calendar/agenda/bb;->l:I

    const/16 v3, 0x14

    if-ge v2, v3, :cond_1

    iget v2, p1, Lcom/android/calendar/agenda/q;->f:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    .line 416
    const/4 v2, 0x2

    iput v2, p1, Lcom/android/calendar/agenda/q;->f:I

    .line 417
    iget v2, p1, Lcom/android/calendar/agenda/q;->c:I

    if-le v2, v1, :cond_0

    .line 418
    iput v1, p1, Lcom/android/calendar/agenda/q;->c:I

    .line 420
    :cond_0
    iget v1, p1, Lcom/android/calendar/agenda/q;->d:I

    if-ge v1, v0, :cond_1

    .line 421
    iput v0, p1, Lcom/android/calendar/agenda/q;->d:I

    .line 433
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/p;->cancelOperation(I)V

    .line 438
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    instance-of v0, v0, Lcom/android/calendar/agenda/AgendaDeleteActivity;

    if-eqz v0, :cond_d

    .line 440
    iget v0, p0, Lcom/android/calendar/agenda/bb;->X:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 441
    iget v0, p1, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_3

    .line 442
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->E()[Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/android/calendar/agenda/bb;->a(JJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "dtstart ASC, title ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 591
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 397
    :pswitch_0
    add-int/lit8 v3, v1, -0x1

    :try_start_1
    iput v3, p1, Lcom/android/calendar/agenda/q;->d:I

    .line 398
    iget v3, p1, Lcom/android/calendar/agenda/q;->d:I

    sub-int v2, v3, v2

    iput v2, p1, Lcom/android/calendar/agenda/q;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 401
    :pswitch_1
    add-int/lit8 v3, v0, 0x1

    :try_start_2
    iput v3, p1, Lcom/android/calendar/agenda/q;->c:I

    .line 402
    iget v3, p1, Lcom/android/calendar/agenda/q;->c:I

    add-int/2addr v2, v3

    iput v2, p1, Lcom/android/calendar/agenda/q;->d:I

    goto :goto_0

    .line 444
    :cond_3
    iget v0, p1, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_2

    .line 445
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->G()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/calendar/agenda/bb;->d:[Ljava/lang/String;

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/android/calendar/agenda/bb;->b(JJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "utc_due_date ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 450
    :cond_4
    new-instance v5, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->x:Ljava/lang/String;

    invoke-direct {v5, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 451
    iget-wide v0, p0, Lcom/android/calendar/agenda/bb;->Z:J

    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 452
    new-instance v6, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->x:Ljava/lang/String;

    invoke-direct {v6, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 453
    iget-wide v0, p0, Lcom/android/calendar/agenda/bb;->Z:J

    invoke-virtual {v6, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 455
    iget v0, p0, Lcom/android/calendar/agenda/bb;->X:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 456
    iget v0, v5, Landroid/text/format/Time;->weekDay:I

    .line 457
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 458
    if-eqz v0, :cond_6

    .line 459
    if-gez v0, :cond_5

    .line 460
    add-int/lit8 v0, v0, 0x7

    .line 462
    :cond_5
    iget v1, v5, Landroid/text/format/Time;->monthDay:I

    sub-int v0, v1, v0

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 464
    :cond_6
    iget v0, v5, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x6

    iput v0, v6, Landroid/text/format/Time;->monthDay:I

    .line 480
    :cond_7
    :goto_2
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->hour:I

    .line 481
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->minute:I

    .line 482
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->second:I

    .line 483
    const/16 v0, 0x17

    iput v0, v6, Landroid/text/format/Time;->hour:I

    .line 484
    const/16 v0, 0x3b

    iput v0, v6, Landroid/text/format/Time;->minute:I

    .line 485
    const/16 v0, 0x3b

    iput v0, v6, Landroid/text/format/Time;->second:I

    .line 487
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iget-wide v2, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 488
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iget-wide v8, v6, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v8, v9}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 489
    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 490
    iget-object v3, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    if-eqz v3, :cond_18

    iget-object v3, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/android/calendar/al;->d()I

    move-result v2

    const/4 v4, 0x4

    if-ne v2, v4, :cond_b

    const-string v2, "0"

    :goto_3
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 491
    const v1, 0x24dc87

    .line 492
    const v0, 0x259d23

    move v2, v0

    move v3, v1

    .line 494
    :goto_4
    iget v0, p1, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_c

    .line 495
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {p0, v3, v2}, Lcom/android/calendar/agenda/bb;->e(II)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->F()[Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/android/calendar/agenda/bb;->a(JJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "startDay ASC, startMinute ASC, title ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 465
    :cond_8
    iget v0, p0, Lcom/android/calendar/agenda/bb;->X:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_9

    .line 466
    const/4 v0, 0x1

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 467
    iget v0, v6, Landroid/text/format/Time;->month:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Landroid/text/format/Time;->month:I

    .line 468
    const/4 v0, 0x1

    iput v0, v6, Landroid/text/format/Time;->monthDay:I

    .line 469
    iget v0, v6, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v6, Landroid/text/format/Time;->monthDay:I

    goto/16 :goto_2

    .line 470
    :cond_9
    iget v0, p0, Lcom/android/calendar/agenda/bb;->X:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_a

    .line 471
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->month:I

    .line 472
    const/4 v0, 0x1

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 473
    iget v0, v6, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Landroid/text/format/Time;->year:I

    .line 474
    const/4 v0, 0x0

    iput v0, v6, Landroid/text/format/Time;->month:I

    .line 475
    const/4 v0, 0x1

    iput v0, v6, Landroid/text/format/Time;->monthDay:I

    .line 476
    iget v0, v6, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v6, Landroid/text/format/Time;->monthDay:I

    goto/16 :goto_2

    .line 477
    :cond_a
    iget v0, p0, Lcom/android/calendar/agenda/bb;->X:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    .line 478
    iget v0, v6, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x2

    iput v0, v6, Landroid/text/format/Time;->monthDay:I

    goto/16 :goto_2

    .line 490
    :cond_b
    const-string v2, ""

    goto :goto_3

    .line 498
    :cond_c
    iget v0, p1, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_2

    .line 499
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->G()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/calendar/agenda/bb;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    invoke-virtual {p0, v8, v9, v6, v7}, Lcom/android/calendar/agenda/bb;->b(JJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "utc_due_date ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 504
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    instance-of v0, v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;

    if-eqz v0, :cond_15

    .line 506
    iget v0, p0, Lcom/android/calendar/agenda/bb;->Y:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_e

    .line 507
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->E()[Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/android/calendar/agenda/bb;->a(JJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "dtstart ASC, title ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 512
    :cond_e
    new-instance v1, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->x:Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 513
    iget-wide v2, p0, Lcom/android/calendar/agenda/bb;->Z:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 514
    new-instance v2, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->x:Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 515
    iget-wide v4, p0, Lcom/android/calendar/agenda/bb;->Z:J

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 517
    iget v0, p0, Lcom/android/calendar/agenda/bb;->Y:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_12

    .line 518
    iget v0, v1, Landroid/text/format/Time;->weekDay:I

    .line 519
    iget-object v3, p0, Lcom/android/calendar/agenda/bb;->g:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v3

    sub-int/2addr v0, v3

    .line 520
    if-eqz v0, :cond_10

    .line 521
    if-gez v0, :cond_f

    .line 522
    add-int/lit8 v0, v0, 0x7

    .line 524
    :cond_f
    iget v3, v1, Landroid/text/format/Time;->monthDay:I

    sub-int v0, v3, v0

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 526
    :cond_10
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x6

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    .line 543
    :cond_11
    :goto_5
    const/4 v0, 0x0

    iput v0, v1, Landroid/text/format/Time;->hour:I

    .line 544
    const/4 v0, 0x0

    iput v0, v1, Landroid/text/format/Time;->minute:I

    .line 545
    const/4 v0, 0x0

    iput v0, v1, Landroid/text/format/Time;->second:I

    .line 546
    const/16 v0, 0x17

    iput v0, v2, Landroid/text/format/Time;->hour:I

    .line 547
    const/16 v0, 0x3b

    iput v0, v2, Landroid/text/format/Time;->minute:I

    .line 548
    const/16 v0, 0x3b

    iput v0, v2, Landroid/text/format/Time;->second:I

    .line 550
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    iget-wide v0, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v0, v1}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 551
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iget-wide v4, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 553
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {p0, v3, v2}, Lcom/android/calendar/agenda/bb;->e(II)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->F()[Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/android/calendar/agenda/bb;->a(JJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "startDay ASC, startMinute ASC, title ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 527
    :cond_12
    iget v0, p0, Lcom/android/calendar/agenda/bb;->Y:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_13

    .line 528
    const/4 v0, 0x1

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 529
    iget v0, v2, Landroid/text/format/Time;->month:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Landroid/text/format/Time;->month:I

    .line 530
    const/4 v0, 0x1

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    .line 531
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    goto :goto_5

    .line 532
    :cond_13
    iget v0, p0, Lcom/android/calendar/agenda/bb;->Y:I

    const/4 v3, 0x4

    if-ne v0, v3, :cond_14

    .line 533
    const/4 v0, 0x0

    iput v0, v1, Landroid/text/format/Time;->month:I

    .line 534
    const/4 v0, 0x1

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 535
    iget v0, v2, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Landroid/text/format/Time;->year:I

    .line 536
    const/4 v0, 0x0

    iput v0, v2, Landroid/text/format/Time;->month:I

    .line 537
    const/4 v0, 0x1

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    .line 538
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    goto :goto_5

    .line 539
    :cond_14
    iget v0, p0, Lcom/android/calendar/agenda/bb;->Y:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_11

    .line 540
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x2

    iput v0, v2, Landroid/text/format/Time;->monthDay:I

    goto/16 :goto_5

    .line 557
    :cond_15
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->e:Landroid/app/Activity;

    instance-of v0, v0, Lcom/android/calendar/agenda/AgendaFestivalListActivity;

    if-eqz v0, :cond_16

    .line 558
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->x:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 559
    iget-wide v2, p0, Lcom/android/calendar/agenda/bb;->Z:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 560
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->x:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 561
    iget-wide v2, p0, Lcom/android/calendar/agenda/bb;->Z:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 563
    const/4 v2, 0x1

    iput v2, v0, Landroid/text/format/Time;->monthDay:I

    .line 564
    iget v2, v1, Landroid/text/format/Time;->month:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/text/format/Time;->month:I

    .line 565
    const/4 v2, 0x1

    iput v2, v1, Landroid/text/format/Time;->monthDay:I

    .line 566
    iget v2, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Landroid/text/format/Time;->monthDay:I

    .line 567
    const/4 v2, 0x0

    iput v2, v0, Landroid/text/format/Time;->hour:I

    .line 568
    const/4 v2, 0x0

    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 569
    const/4 v2, 0x0

    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 570
    const/16 v2, 0x17

    iput v2, v1, Landroid/text/format/Time;->hour:I

    .line 571
    const/16 v2, 0x3b

    iput v2, v1, Landroid/text/format/Time;->minute:I

    .line 572
    const/16 v2, 0x3b

    iput v2, v1, Landroid/text/format/Time;->second:I

    .line 574
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 575
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    iget-wide v0, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v0, v1}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 577
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/android/calendar/agenda/bb;->e(II)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->F()[Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/android/calendar/agenda/bb;->c(JJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "startDay ASC, startMinute ASC, title ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 580
    :cond_16
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    instance-of v0, v0, Lcom/android/calendar/agenda/AgendaPickActivity;

    if-eqz v0, :cond_2

    .line 581
    iget v0, p1, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_17

    .line 582
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v8, 0x0

    sget-object v9, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bb;->E()[Ljava/lang/String;

    move-result-object v10

    const-wide/16 v2, -0x1

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/android/calendar/agenda/bb;->a(JJLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "dtstart ASC, title ASC"

    move v1, v8

    move-object v2, p1

    move-object v3, v9

    move-object v4, v10

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 585
    :cond_17
    iget v0, p1, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_2

    .line 586
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    sget-object v3, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/agenda/bb;->d:[Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "utc_due_date ASC"

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :cond_18
    move v2, v0

    move v3, v1

    goto/16 :goto_4

    .line 395
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected b(Z)V
    .locals 10

    .prologue
    const v7, 0x7f020132

    const/16 v6, 0x8

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 300
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    if-nez v1, :cond_0

    .line 331
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->H:Ljava/lang/String;

    if-eqz v1, :cond_3

    move v3, v0

    .line 304
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_4

    move v5, v0

    .line 306
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    const v1, 0x7f1200dc

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 307
    if-eqz v0, :cond_1

    .line 308
    const v1, 0x7f1200dd

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 309
    const v1, 0x7f1200de

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 311
    if-eqz p1, :cond_5

    if-nez v3, :cond_5

    move v2, v4

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 313
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 314
    iget v8, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v9, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v2, v8, v4, v9, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 315
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 317
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 318
    const v2, 0x7f0f02d2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 320
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    invoke-static {v1}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v1

    .line 321
    if-eqz v5, :cond_7

    if-eqz v1, :cond_6

    const v1, 0x7f020133

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 324
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    const v1, 0x7f120282

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 325
    if-eqz v0, :cond_2

    .line 326
    if-eqz p1, :cond_8

    if-eqz v3, :cond_8

    move v1, v4

    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 329
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->k:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz p1, :cond_9

    :goto_6
    invoke-virtual {v0, v6}, Lcom/android/calendar/agenda/AgendaListView;->setVisibility(I)V

    .line 330
    iget-object v0, p0, Lcom/android/calendar/agenda/bb;->ab:Lcom/android/calendar/agenda/a;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/a;->a(Z)V

    goto/16 :goto_0

    :cond_3
    move v3, v4

    .line 303
    goto/16 :goto_1

    :cond_4
    move v5, v4

    .line 304
    goto :goto_2

    :cond_5
    move v2, v6

    .line 311
    goto :goto_3

    :cond_6
    move v1, v7

    .line 321
    goto :goto_4

    :cond_7
    move v1, v7

    goto :goto_4

    :cond_8
    move v1, v6

    .line 326
    goto :goto_5

    :cond_9
    move v6, v4

    .line 329
    goto :goto_6
.end method

.method protected c(JJ)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 239
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 241
    const-string v1, "visible=1 AND deleted=0 AND (eventStatus!=2 OR eventStatus is null)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 244
    cmp-long v1, p1, v4

    if-eqz v1, :cond_0

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND dtstart>="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 247
    :cond_0
    cmp-long v1, p3, v4

    if-eqz v1, :cond_1

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND dtstart<="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 251
    :cond_1
    const-string v1, " AND (calendar_id=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 252
    const-string v1, " OR (calendar_id=1 AND contact_id IS NOT NULL AND contactEventType=3))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 255
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected e(II)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 276
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_BY_DAY_URI:Landroid/net/Uri;

    .line 277
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 278
    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 279
    int-to-long v2, p2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 281
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected f()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/android/calendar/agenda/bb;->W:I

    return v0
.end method

.method protected g()I
    .locals 2

    .prologue
    .line 290
    invoke-super {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g()I

    move-result v0

    .line 292
    iget-object v1, p0, Lcom/android/calendar/agenda/bb;->e:Landroid/app/Activity;

    instance-of v1, v1, Lcom/android/calendar/agenda/AgendaFestivalListActivity;

    if-eqz v1, :cond_0

    .line 293
    and-int/lit8 v0, v0, -0x5

    .line 295
    :cond_0
    return v0
.end method

.method public j(I)V
    .locals 0

    .prologue
    .line 602
    iput p1, p0, Lcom/android/calendar/agenda/bb;->X:I

    .line 603
    return-void
.end method

.method public k(I)V
    .locals 0

    .prologue
    .line 606
    iput p1, p0, Lcom/android/calendar/agenda/bb;->Y:I

    .line 607
    return-void
.end method
