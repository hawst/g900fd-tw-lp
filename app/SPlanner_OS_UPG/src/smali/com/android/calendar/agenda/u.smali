.class Lcom/android/calendar/agenda/u;
.super Ljava/lang/Object;
.source "AgendaByDayAdapter.java"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public g:Ljava/lang/String;

.field public h:I

.field public i:I

.field public j:J

.field public k:J


# direct methods
.method public constructor <init>(III)V
    .locals 14

    .prologue
    .line 1296
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move v1, p1

    move/from16 v10, p2

    move/from16 v11, p3

    invoke-direct/range {v0 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIIIII)V

    .line 1297
    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 14

    .prologue
    .line 1300
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move v1, p1

    move/from16 v10, p2

    move/from16 v11, p3

    move/from16 v12, p4

    move/from16 v13, p5

    invoke-direct/range {v0 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIIIII)V

    .line 1301
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;JJIIII)V
    .locals 14

    .prologue
    .line 1305
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-direct/range {v0 .. v13}, Lcom/android/calendar/agenda/u;-><init>(IILjava/lang/String;JJIIIIII)V

    .line 1306
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;JJIIIIII)V
    .locals 0

    .prologue
    .line 1310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1311
    iput p1, p0, Lcom/android/calendar/agenda/u;->a:I

    .line 1312
    iput p10, p0, Lcom/android/calendar/agenda/u;->b:I

    .line 1313
    iput p11, p0, Lcom/android/calendar/agenda/u;->e:I

    .line 1315
    iput p2, p0, Lcom/android/calendar/agenda/u;->f:I

    .line 1316
    iput-object p3, p0, Lcom/android/calendar/agenda/u;->g:Ljava/lang/String;

    .line 1317
    iput-wide p4, p0, Lcom/android/calendar/agenda/u;->k:J

    .line 1318
    iput-wide p6, p0, Lcom/android/calendar/agenda/u;->j:J

    .line 1319
    iput p8, p0, Lcom/android/calendar/agenda/u;->h:I

    .line 1320
    iput p9, p0, Lcom/android/calendar/agenda/u;->i:I

    .line 1321
    iput p12, p0, Lcom/android/calendar/agenda/u;->c:I

    .line 1322
    iput p13, p0, Lcom/android/calendar/agenda/u;->d:I

    .line 1323
    return-void
.end method
