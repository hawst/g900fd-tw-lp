.class public Lcom/android/calendar/agenda/bw;
.super Landroid/app/Fragment;
.source "DeleteEventFragment.java"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

.field private c:Landroid/view/View;

.field private d:Lcom/android/calendar/agenda/AgendaListView;

.field private e:Lcom/android/calendar/agenda/bb;

.field private f:Ljava/util/ArrayList;

.field private g:Lcom/android/calendar/agenda/bz;

.field private h:Landroid/view/ActionMode;

.field private i:Landroid/database/Cursor;

.field private j:Lcom/android/calendar/agenda/ca;

.field private k:Landroid/content/ContentResolver;

.field private l:Z

.field private m:Z

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "attendeeName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "attendeeEmail"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/agenda/bw;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 71
    iput-object v1, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    .line 72
    iput-object v1, p0, Lcom/android/calendar/agenda/bw;->c:Landroid/view/View;

    .line 73
    iput-object v1, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    .line 74
    iput-object v1, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->f:Ljava/util/ArrayList;

    .line 82
    iput-object v1, p0, Lcom/android/calendar/agenda/bw;->i:Landroid/database/Cursor;

    .line 83
    iput-object v1, p0, Lcom/android/calendar/agenda/bw;->j:Lcom/android/calendar/agenda/ca;

    .line 84
    iput-object v1, p0, Lcom/android/calendar/agenda/bw;->k:Landroid/content/ContentResolver;

    .line 86
    iput-boolean v2, p0, Lcom/android/calendar/agenda/bw;->m:Z

    .line 87
    iput-boolean v2, p0, Lcom/android/calendar/agenda/bw;->n:Z

    .line 90
    return-void
.end method

.method private static a([Ljava/lang/Long;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 252
    .line 253
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p0, v1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 254
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 255
    add-int/lit8 v0, v0, 0x1

    .line 253
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 257
    :cond_1
    return v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/bw;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/android/calendar/agenda/bw;->h:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/bb;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/bw;Lcom/android/calendar/agenda/bz;)Lcom/android/calendar/agenda/bz;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/android/calendar/agenda/bw;->g:Lcom/android/calendar/agenda/bz;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/bw;[Ljava/lang/Long;Z)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/agenda/bw;->a([Ljava/lang/Long;Z)V

    return-void
.end method

.method private a([Ljava/lang/Long;Z)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 280
    if-nez p1, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    array-length v0, p1

    if-ne v0, v7, :cond_3

    invoke-static {p1}, Lcom/android/calendar/agenda/bw;->a([Ljava/lang/Long;)I

    move-result v0

    if-nez v0, :cond_3

    .line 287
    aget-object v0, p1, v6

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 288
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 289
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->k:Landroid/content/ContentResolver;

    sget-object v2, Lcom/android/calendar/agenda/bb;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->i:Landroid/database/Cursor;

    .line 291
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->i:Landroid/database/Cursor;

    if-eqz v0, :cond_4

    .line 292
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->i:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v1

    .line 294
    if-eqz v1, :cond_4

    .line 295
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 297
    const/16 v0, 0xc

    :try_start_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.android.exchange"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 300
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 304
    :goto_2
    if-eqz v0, :cond_2

    .line 305
    sget-object v4, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    .line 306
    new-array v7, v7, [Ljava/lang/String;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    .line 307
    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->j:Lcom/android/calendar/agenda/ca;

    const/4 v2, 0x2

    sget-object v5, Lcom/android/calendar/agenda/bw;->a:[Ljava/lang/String;

    const-string v6, "event_id=?"

    move-object v8, v3

    invoke-virtual/range {v1 .. v8}, Lcom/android/calendar/agenda/ca;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :catch_0
    move-exception v0

    move v0, v6

    goto :goto_1

    .line 310
    :cond_2
    new-instance v0, Lcom/android/calendar/cj;

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-direct {v0, v1, v7}, Lcom/android/calendar/cj;-><init>(Landroid/app/Activity;Z)V

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;Z)V

    goto :goto_0

    .line 313
    :cond_3
    array-length v0, p1

    if-lez v0, :cond_0

    .line 314
    new-instance v0, Lcom/android/calendar/cj;

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-direct {v0, v1, v7}, Lcom/android/calendar/cj;-><init>(Landroid/app/Activity;Z)V

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;Z)V

    goto :goto_0

    :cond_4
    move v0, v6

    goto :goto_2
.end method

.method static synthetic b(Lcom/android/calendar/agenda/bw;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->g:Lcom/android/calendar/agenda/bz;

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Lcom/android/calendar/agenda/bz;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/bw;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/agenda/bz;-><init>(Lcom/android/calendar/agenda/bw;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->g:Lcom/android/calendar/agenda/bz;

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->g:Lcom/android/calendar/agenda/bz;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->h:Landroid/view/ActionMode;

    .line 277
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/agenda/bw;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->h:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaDeleteActivity;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/agenda/bw;)Lcom/android/calendar/agenda/AgendaListView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 361
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 368
    :goto_0
    return-void

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 365
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 366
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 367
    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 7

    .prologue
    const v6, 0x7f1200dc

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 230
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bw;->l:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 231
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 232
    const v1, 0x7f1200de

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 234
    const v4, 0x7f0f02d2

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 236
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 237
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v5, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v1, v4, v2, v5, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 238
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_1

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/AgendaListView;->setVisibility(I)V

    .line 245
    return-void

    :cond_1
    move v0, v3

    .line 243
    goto :goto_0

    :cond_2
    move v3, v2

    .line 244
    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 184
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 186
    if-eqz p1, :cond_2

    const-string v0, "event_ids_selected"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187
    const-string v0, "event_ids_selected"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 188
    new-instance v3, Ljava/util/HashSet;

    array-length v0, v2

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 189
    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v2, v1

    .line 190
    instance-of v5, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    if-eqz v5, :cond_0

    .line 191
    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/bb;->a(Ljava/util/HashSet;)V

    .line 195
    :cond_2
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 349
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 351
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    if-eqz v0, :cond_0

    .line 352
    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->a()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/bb;->b(Z)V

    .line 355
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bw;->l:Z

    if-eqz v0, :cond_1

    .line 356
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bw;->a()V

    .line 358
    :cond_1
    return-void

    .line 352
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bw;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaDeleteActivity;

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    .line 101
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/bw;->l:Z

    .line 102
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->k:Landroid/content/ContentResolver;

    .line 103
    new-instance v0, Lcom/android/calendar/agenda/ca;

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/agenda/ca;-><init>(Lcom/android/calendar/agenda/bw;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->j:Lcom/android/calendar/agenda/ca;

    .line 104
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/bw;->m:Z

    .line 105
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/bw;->n:Z

    .line 106
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 110
    const v0, 0x7f040009

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 111
    const v0, 0x7f12002b

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaListView;

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    .line 112
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v4}, Lcom/android/calendar/agenda/AgendaListView;->setItemsCanFocus(Z)V

    .line 114
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 116
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bw;->m:Z

    if-eqz v0, :cond_2

    const/4 v3, 0x1

    .line 117
    :goto_0
    new-instance v0, Lcom/android/calendar/agenda/bb;

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    iget-object v2, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v5, -0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/agenda/bb;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;IZI)V

    iput-object v0, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    .line 118
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->j(I)V

    .line 119
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/agenda/bb;->b(J)V

    .line 120
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setAdapter(Lcom/android/calendar/agenda/AgendaBaseAdapter;)V

    .line 122
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bw;->n:Z

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    new-instance v1, Lcom/android/calendar/agenda/bx;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/bx;-><init>(Lcom/android/calendar/agenda/bw;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setTwMultiSelectedListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnTwMultiSelectedListener;)V

    .line 159
    :cond_0
    new-instance v0, Lcom/android/calendar/agenda/by;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/by;-><init>(Lcom/android/calendar/agenda/bw;)V

    .line 173
    invoke-direct {p0}, Lcom/android/calendar/agenda/bw;->b()V

    .line 175
    iput-object v6, p0, Lcom/android/calendar/agenda/bw;->c:Landroid/view/View;

    .line 176
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bw;->l:Z

    if-eqz v0, :cond_1

    .line 177
    invoke-virtual {p0}, Lcom/android/calendar/agenda/bw;->a()V

    .line 179
    :cond_1
    return-object v6

    .line 116
    :cond_2
    const/4 v3, 0x3

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->h:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->h:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 269
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 270
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 199
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 201
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bw;->m:Z

    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-static {v1}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->finish()V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setChoiceMode(I)V

    .line 206
    iget-boolean v0, p0, Lcom/android/calendar/agenda/bw;->n:Z

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v7}, Lcom/android/calendar/agenda/AgendaListView;->setEnableDragBlock(Z)V

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-static {v0}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v0

    .line 210
    iget-object v1, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaListView;->setHideDeclinedEvents(Z)V

    .line 211
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->d:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    const v2, 0x24dc87

    const v3, 0x259d23

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    iget-object v6, v6, Lcom/android/calendar/agenda/AgendaDeleteActivity;->b:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 213
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    iget-object v0, v0, Lcom/android/calendar/agenda/AgendaDeleteActivity;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 214
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->b:Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaDeleteActivity;->invalidateOptionsMenu()V

    .line 215
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 221
    iget-object v0, p0, Lcom/android/calendar/agenda/bw;->e:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->z()Ljava/util/HashSet;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 223
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 224
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 225
    const-string v0, "event_ids_selected"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 227
    :cond_0
    return-void
.end method
