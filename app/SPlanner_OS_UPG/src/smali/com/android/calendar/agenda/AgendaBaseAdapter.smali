.class public abstract Lcom/android/calendar/agenda/AgendaBaseAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "AgendaBaseAdapter.java"


# static fields
.field protected static final a:Ljava/lang/String;

.field protected static final b:[Ljava/lang/String;

.field protected static final c:[Ljava/lang/String;

.field protected static final d:[Ljava/lang/String;


# instance fields
.field protected final A:Landroid/os/Handler;

.field protected B:Ljava/lang/Runnable;

.field protected C:Z

.field protected D:Z

.field protected E:Z

.field protected F:Z

.field protected G:Z

.field protected H:Ljava/lang/String;

.field protected I:J

.field protected final J:I

.field protected final K:I

.field protected L:Lcom/android/calendar/agenda/i;

.field protected M:Landroid/view/ActionMode;

.field protected N:I

.field protected O:I

.field protected P:I

.field protected Q:Ljava/util/HashSet;

.field protected R:Ljava/util/HashSet;

.field protected S:Z

.field protected T:I

.field protected U:I

.field protected V:Lcom/android/calendar/agenda/o;

.field private W:I

.field private X:I

.field protected e:Landroid/app/Activity;

.field protected f:Lcom/android/calendar/al;

.field protected g:Landroid/content/Context;

.field protected h:Landroid/content/res/Resources;

.field protected i:Lcom/android/calendar/agenda/p;

.field protected j:Lcom/android/calendar/d/g;

.field protected k:Lcom/android/calendar/agenda/AgendaListView;

.field protected l:I

.field protected m:I

.field protected n:Lcom/android/calendar/agenda/m;

.field protected final o:Ljava/util/LinkedList;

.field protected final p:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field protected final q:Z

.field protected r:I

.field protected s:I

.field protected t:I

.field protected u:I

.field protected v:Ljava/util/Formatter;

.field protected w:Ljava/lang/StringBuilder;

.field protected x:Ljava/lang/String;

.field protected y:Ljava/lang/String;

.field protected z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 98
    const-class v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a:Ljava/lang/String;

    .line 167
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "eventLocation"

    aput-object v1, v0, v5

    const-string v1, "allDay"

    aput-object v1, v0, v6

    const-string v1, "hasAlarm"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "dtstart"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "original_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "dtstart"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "canOrganizerRespond"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "guestsCanModify"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "hasAttendeeData"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "eventColor_index"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "calendar_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "rdate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b:[Ljava/lang/String;

    .line 198
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "event_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "eventLocation"

    aput-object v1, v0, v5

    const-string v1, "allDay"

    aput-object v1, v0, v6

    const-string v1, "hasAlarm"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "end"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "startDay"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "endDay"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "canOrganizerRespond"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "guestsCanModify"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "hasAttendeeData"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "eventColor_index"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "calendar_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "rdate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->c:[Ljava/lang/String;

    .line 229
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "subject"

    aput-object v1, v0, v4

    const-string v1, "due_date"

    aput-object v1, v0, v5

    const-string v1, "utc_due_date"

    aput-object v1, v0, v6

    const-string v1, "importance"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "reminder_set"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "previousId"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "parentId"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "task_order"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "groupName"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "_sync_account"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;Z)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 627
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 294
    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->m:I

    .line 299
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    .line 300
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 332
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->A:Landroid/os/Handler;

    .line 334
    new-instance v0, Lcom/android/calendar/agenda/j;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/j;-><init>(Lcom/android/calendar/agenda/AgendaBaseAdapter;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->B:Ljava/lang/Runnable;

    .line 347
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->D:Z

    .line 348
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->E:Z

    .line 349
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->F:Z

    .line 350
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->G:Z

    .line 355
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    .line 360
    iput-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->L:Lcom/android/calendar/agenda/i;

    .line 364
    iput v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->N:I

    .line 365
    iput v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->O:I

    .line 367
    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->P:I

    .line 369
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    .line 370
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->R:Ljava/util/HashSet;

    .line 371
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->S:Z

    .line 372
    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->T:I

    .line 389
    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->W:I

    .line 390
    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->X:I

    .line 391
    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->U:I

    .line 393
    iput-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->V:Lcom/android/calendar/agenda/o;

    .line 628
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    .line 629
    iput-object p2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    .line 630
    iput-boolean p3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->z:Z

    move-object v0, p1

    .line 632
    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    .line 633
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    .line 635
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h:Landroid/content/res/Resources;

    .line 636
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h:Landroid/content/res/Resources;

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->J:I

    .line 637
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h:Landroid/content/res/Resources;

    const v1, 0x7f0b0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->K:I

    .line 639
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->q:Z

    .line 640
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->B:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    .line 641
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->y:Ljava/lang/String;

    .line 643
    new-instance v0, Lcom/android/calendar/agenda/p;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/agenda/p;-><init>(Lcom/android/calendar/agenda/AgendaBaseAdapter;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i:Lcom/android/calendar/agenda/p;

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->w:Ljava/lang/StringBuilder;

    .line 645
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->w:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->v:Ljava/util/Formatter;

    .line 646
    iput-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    .line 647
    return-void

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    goto :goto_0
.end method

.method private E()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2675
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2678
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    if-eqz v2, :cond_1

    .line 2679
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2680
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getHeight()I

    move-result v2

    .line 2681
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2683
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->getGroupCount()I

    move-result v4

    sub-int/2addr v3, v4

    .line 2684
    iget v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->X:I

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->W:I

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->getGroupCount()I

    move-result v5

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->U:I

    add-int/2addr v3, v4

    .line 2686
    sub-int/2addr v0, v2

    if-lt v3, v0, :cond_0

    .line 2687
    const/4 v1, 0x1

    .line 2688
    :cond_0
    return v1

    :cond_1
    move v0, v1

    move v2, v1

    goto :goto_0
.end method

.method protected static a(Landroid/content/Context;J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2664
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 2665
    if-nez v0, :cond_1

    .line 2672
    :cond_0
    :goto_0
    return-void

    .line 2668
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2669
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2670
    const-string v0, "GATE"

    const-string v1, "<GATE-M>TASK_DELETED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected static a(Landroid/view/View;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2094
    const v0, 0x7f12002d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2095
    const v1, 0x7f12003c

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2097
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 2102
    :cond_0
    :goto_0
    return-void

    .line 2100
    :cond_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 2101
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected static a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/16 v1, 0x12c

    const/16 v8, 0x21

    const/4 v2, 0x0

    .line 2105
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2106
    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2171
    :cond_0
    :goto_0
    return-void

    .line 2110
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2113
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2114
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 2115
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 2117
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 2118
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 2120
    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2121
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    .line 2122
    if-ltz v1, :cond_0

    .line 2125
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    aget-char v4, v4, v2

    invoke-static {v4}, Lcom/android/calendar/hj;->a(C)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2127
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 2128
    array-length v0, v3

    .line 2129
    new-array v4, v0, [C

    .line 2132
    array-length v5, v3

    move v1, v2

    move v0, v2

    :goto_1
    if-ge v1, v5, :cond_4

    aget-char v6, v3, v1

    .line 2134
    const/16 v7, 0x200c

    if-eq v6, v7, :cond_3

    .line 2135
    aput-char v6, v4, v0

    .line 2136
    add-int/lit8 v0, v0, 0x1

    .line 2132
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2139
    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-static {v4, v2, v0}, Ljava/util/Arrays;->copyOfRange([CII)[C

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    .line 2141
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-static {v0, v2, v3}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v0

    .line 2142
    const-string v2, "#007ad2"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    .line 2143
    if-eqz v0, :cond_6

    .line 2144
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2146
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2148
    array-length v4, v0

    if-eqz v4, :cond_5

    const/4 v4, -0x1

    if-eq v1, v4, :cond_5

    .line 2149
    array-length v0, v0

    add-int/2addr v0, v1

    .line 2150
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v4, v1, v0, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2153
    :cond_5
    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2155
    :cond_6
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2160
    :cond_7
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-static {v2, v4, v5}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v2

    .line 2161
    if-eqz v2, :cond_8

    .line 2162
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    .line 2163
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2164
    array-length v0, v2

    add-int/2addr v0, v1

    .line 2166
    :cond_8
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2167
    const-string v3, "#007ad2"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    .line 2168
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2, v4, v1, v0, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2169
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method protected static a(Lcom/android/calendar/agenda/w;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 980
    .line 982
    if-eqz p0, :cond_0

    .line 983
    invoke-virtual {p0}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 984
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 985
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "task_personal"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 995
    :cond_0
    :goto_0
    return v1

    .line 991
    :cond_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v0

    const/16 v3, 0x1f4

    if-lt v0, v3, :cond_3

    move v0, v1

    .line 992
    :goto_1
    const/16 v3, 0x16

    invoke-virtual {p0, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    move v3, v1

    .line 993
    :goto_2
    if-eqz v0, :cond_2

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move v0, v2

    .line 991
    goto :goto_1

    :cond_4
    move v3, v2

    .line 992
    goto :goto_2
.end method

.method protected static a(Lcom/android/calendar/agenda/w;Ljava/lang/Boolean;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 998
    .line 1000
    if-eqz p0, :cond_0

    .line 1001
    invoke-virtual {p0}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1002
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1003
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "task_personal"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 1022
    :cond_0
    :goto_0
    return v1

    .line 1009
    :cond_1
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1011
    if-eqz v0, :cond_3

    .line 1016
    :goto_1
    const/16 v3, 0x10

    invoke-virtual {p0, v3}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1017
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    .line 1018
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 1019
    :goto_2
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 1014
    :cond_3
    const-string v0, ""

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1018
    goto :goto_2
.end method


# virtual methods
.method public A()I
    .locals 1

    .prologue
    .line 2576
    iget v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->T:I

    return v0
.end method

.method protected B()V
    .locals 2

    .prologue
    .line 2596
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->v()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->R:Ljava/util/HashSet;

    .line 2597
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2598
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->R:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    .line 2600
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->C()V

    .line 2601
    return-void
.end method

.method protected C()V
    .locals 3

    .prologue
    .line 2607
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->R:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2608
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    .line 2609
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->t()I

    move-result v1

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->S:Z

    .line 2610
    return-void

    .line 2609
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2638
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v2

    .line 2639
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 2640
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    .line 2641
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->c()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2644
    const/4 v0, -0x1

    invoke-virtual {v4, v0}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 2645
    :cond_1
    :goto_0
    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2646
    const-string v0, "complete"

    invoke-virtual {v4, v0}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 2647
    :goto_1
    if-eqz v0, :cond_1

    .line 2648
    const-string v0, "_id"

    invoke-virtual {v4, v0}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v6

    .line 2649
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-static {v0, v6, v7}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/content/Context;J)V

    goto :goto_0

    .line 2653
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    .line 2646
    goto :goto_1

    .line 2653
    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2654
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    const v2, 0x7f0f015a

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2655
    return-void
.end method

.method public a()I
    .locals 2

    .prologue
    .line 651
    .line 652
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v1

    .line 653
    :try_start_0
    iget v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    .line 654
    monitor-exit v1

    .line 655
    return v0

    .line 654
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected a(IZII)I
    .locals 3

    .prologue
    const v2, 0x259d23

    const v0, 0x24dc87

    .line 1351
    .line 1352
    add-int v1, p1, p4

    .line 1353
    packed-switch p3, :pswitch_data_0

    :cond_0
    move p1, v1

    .line 1371
    :cond_1
    :goto_0
    return p1

    .line 1355
    :pswitch_0
    if-ge v1, v0, :cond_0

    .line 1356
    if-eqz p2, :cond_1

    move p1, v0

    .line 1357
    goto :goto_0

    .line 1363
    :pswitch_1
    if-le v1, v2, :cond_0

    .line 1364
    if-nez p2, :cond_1

    move p1, v2

    .line 1367
    goto :goto_0

    .line 1353
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/text/format/Time;)I
    .locals 2

    .prologue
    .line 688
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(Landroid/text/format/Time;)Lcom/android/calendar/agenda/m;

    move-result-object v0

    .line 689
    if-eqz v0, :cond_0

    .line 690
    iget v1, v0, Lcom/android/calendar/agenda/m;->f:I

    iget-object v0, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/r;->a(Landroid/text/format/Time;)I

    move-result v0

    add-int/2addr v0, v1

    .line 692
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected a(Lcom/android/calendar/agenda/q;Lcom/android/calendar/agenda/w;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1516
    iget-object v7, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v7

    .line 1517
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1518
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1575
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1522
    :cond_1
    :try_start_1
    iget v0, p1, Lcom/android/calendar/agenda/q;->f:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d(I)Lcom/android/calendar/agenda/m;

    move-result-object v0

    .line 1528
    if-nez v0, :cond_3

    .line 1529
    new-instance v0, Lcom/android/calendar/agenda/m;

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g()I

    move-result v5

    invoke-direct {v0, v4, v5}, Lcom/android/calendar/agenda/m;-><init>(Landroid/content/Context;I)V

    move v4, v3

    move-object v5, v0

    .line 1537
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget v6, p1, Lcom/android/calendar/agenda/q;->d:I

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->d:I

    if-gt v6, v0, :cond_4

    :cond_2
    move v0, v2

    :goto_1
    iput-boolean v0, v5, Lcom/android/calendar/agenda/m;->j:Z

    .line 1538
    iget v0, p1, Lcom/android/calendar/agenda/q;->c:I

    iput v0, v5, Lcom/android/calendar/agenda/m;->d:I

    .line 1539
    iget v0, p1, Lcom/android/calendar/agenda/q;->d:I

    iput v0, v5, Lcom/android/calendar/agenda/m;->e:I

    .line 1540
    iput-object p2, v5, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    .line 1541
    iget-object v0, v5, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v0

    iput v0, v5, Lcom/android/calendar/agenda/m;->g:I

    .line 1542
    iget-wide v8, p1, Lcom/android/calendar/agenda/q;->h:J

    iput-wide v8, v5, Lcom/android/calendar/agenda/m;->h:J

    .line 1543
    iget-wide v8, p1, Lcom/android/calendar/agenda/q;->i:J

    iput-wide v8, v5, Lcom/android/calendar/agenda/m;->i:J

    .line 1546
    iget-boolean v0, v5, Lcom/android/calendar/agenda/m;->j:Z

    if-eqz v0, :cond_5

    .line 1547
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 1548
    iget-object v0, v5, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v0, v5}, Lcom/android/calendar/agenda/r;->a(Lcom/android/calendar/agenda/m;)V

    .line 1549
    iget-object v0, v5, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v0

    add-int/2addr v0, v4

    move v6, v0

    .line 1555
    :goto_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    .line 1558
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v4, v1

    move v1, v3

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 1559
    add-int/lit8 v5, v1, 0x1

    if-nez v1, :cond_6

    move v1, v2

    :goto_4
    iput-boolean v1, v0, Lcom/android/calendar/agenda/m;->j:Z

    .line 1560
    iput-object v4, v0, Lcom/android/calendar/agenda/m;->c:Lcom/android/calendar/agenda/m;

    .line 1561
    iget-object v1, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/r;->a(Lcom/android/calendar/agenda/m;)V

    .line 1562
    iget v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    iput v1, v0, Lcom/android/calendar/agenda/m;->f:I

    .line 1563
    iget-object v1, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/r;->getCount()I

    move-result v1

    iput v1, v0, Lcom/android/calendar/agenda/m;->g:I

    .line 1564
    iget v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    iget v4, v0, Lcom/android/calendar/agenda/m;->g:I

    add-int/2addr v1, v4

    iput v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    move-object v4, v0

    move v1, v5

    .line 1566
    goto :goto_3

    .line 1533
    :cond_3
    iget v4, v0, Lcom/android/calendar/agenda/m;->g:I

    neg-int v4, v4

    move-object v5, v0

    goto/16 :goto_0

    :cond_4
    move v0, v3

    .line 1537
    goto :goto_1

    .line 1551
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    move v6, v4

    goto :goto_2

    :cond_6
    move v1, v3

    .line 1559
    goto :goto_4

    .line 1567
    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->n:Lcom/android/calendar/agenda/m;

    .line 1569
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1570
    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1572
    :cond_9
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 1574
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v6
.end method

.method public a(ZZ)I
    .locals 3

    .prologue
    .line 2484
    if-eqz p1, :cond_3

    .line 2485
    const/4 v0, 0x0

    .line 2486
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 2487
    if-eqz p2, :cond_1

    iget-boolean v0, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    if-eqz v0, :cond_2

    .line 2488
    :cond_0
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 2489
    goto :goto_0

    .line 2487
    :cond_1
    iget-boolean v0, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    if-nez v0, :cond_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 2492
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    :cond_4
    return v1
.end method

.method protected a(ZIILjava/lang/String;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 1270
    if-eqz p1, :cond_0

    .line 1271
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    .line 1280
    :goto_0
    return-object v0

    .line 1273
    :cond_0
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_BY_DAY_URI:Landroid/net/Uri;

    .line 1274
    :goto_1
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1275
    int-to-long v2, p2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 1276
    int-to-long v2, p3

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 1277
    if-eqz p4, :cond_2

    .line 1278
    invoke-virtual {v0, p4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1280
    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1273
    :cond_3
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_SEARCH_BY_DAY_URI:Landroid/net/Uri;

    goto :goto_1
.end method

.method public a(II)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 804
    .line 806
    iget-object v7, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v7

    .line 807
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v4

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 808
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/r;->a()I

    move-result v3

    .line 809
    add-int v6, v2, v3

    .line 810
    if-le v6, p1, :cond_3

    .line 811
    iget-object v2, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    sub-int v3, v6, v3

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/r;->e(I)I

    move-result v2

    add-int/2addr v2, p2

    add-int/lit8 v2, v2, 0x1

    .line 813
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v3

    .line 814
    const/high16 v2, -0x80000000

    if-ne v3, v2, :cond_0

    .line 815
    monitor-exit v7

    move-object v0, v1

    .line 853
    :goto_1
    return-object v0

    .line 818
    :cond_0
    new-instance v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    invoke-direct {v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;-><init>()V

    .line 821
    if-gez v3, :cond_5

    .line 822
    neg-int v5, v3

    .line 823
    const/4 v3, 0x1

    .line 826
    :goto_2
    iget-object v9, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    if-eqz v9, :cond_3

    iget-object v9, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v9}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v9

    if-ge v5, v9, :cond_3

    .line 827
    iget-object v1, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v1

    .line 828
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v4, v5}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 829
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    sget-object v5, Lcom/android/calendar/agenda/w;->d:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    .line 830
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    sget-object v5, Lcom/android/calendar/agenda/w;->b:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    .line 831
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v4

    iput-boolean v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    .line 832
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/w;)Z

    move-result v4

    iput-boolean v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    .line 833
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    iget-boolean v5, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/w;Ljava/lang/Boolean;)Z

    move-result v4

    iput-boolean v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    .line 835
    if-eqz v3, :cond_1

    .line 836
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 837
    iget-wide v8, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    invoke-virtual {v4, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 838
    invoke-static {v4}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 839
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    .line 841
    :cond_1
    if-nez v3, :cond_2

    .line 842
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    .line 843
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->c:Ljava/lang/String;

    .line 844
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    sget-object v4, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    .line 845
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/16 v4, 0x3f0

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    .line 847
    :cond_2
    iget-object v0, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 848
    monitor-exit v7

    move-object v0, v2

    goto/16 :goto_1

    :cond_3
    move v2, v6

    .line 851
    goto/16 :goto_0

    .line 852
    :cond_4
    monitor-exit v7

    move-object v0, v1

    .line 853
    goto/16 :goto_1

    .line 852
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    move v5, v3

    move v3, v4

    goto/16 :goto_2
.end method

.method public a(IZ)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 750
    if-gez p1, :cond_0

    move-object v0, v2

    .line 793
    :goto_0
    return-object v0

    .line 755
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(I)Lcom/android/calendar/agenda/m;

    move-result-object v3

    .line 756
    if-nez v3, :cond_1

    move-object v0, v2

    .line 757
    goto :goto_0

    .line 760
    :cond_1
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    iget v1, v3, Lcom/android/calendar/agenda/m;->f:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v1

    .line 761
    const/high16 v0, -0x80000000

    if-ne v1, v0, :cond_2

    move-object v0, v2

    .line 762
    goto :goto_0

    .line 765
    :cond_2
    const/4 v0, 0x0

    .line 766
    if-gez v1, :cond_3

    .line 767
    neg-int v1, v1

    .line 768
    const/4 v0, 0x1

    .line 771
    :cond_3
    iget-object v4, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    if-eqz v4, :cond_6

    iget-object v4, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v4

    if-ge v1, v4, :cond_6

    .line 772
    iget-object v2, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v4

    .line 773
    iget-object v2, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2, v1}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 774
    iget-object v2, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {p0, v2, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/database/Cursor;Z)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v2

    .line 775
    iget-object v5, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    sget-object v6, Lcom/android/calendar/agenda/w;->b:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    .line 777
    if-nez p2, :cond_4

    if-nez v0, :cond_4

    .line 778
    iget-object v5, v3, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v5, v1}, Lcom/android/calendar/agenda/r;->b(I)I

    move-result v1

    iput v1, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->f:I

    .line 781
    :cond_4
    if-nez v0, :cond_5

    .line 782
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    sget-object v1, Lcom/android/calendar/agenda/w;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    .line 783
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    sget-object v1, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    .line 784
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    const/16 v1, 0x3f0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    .line 785
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    sget-object v1, Lcom/android/calendar/agenda/w;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->g:Ljava/lang/String;

    .line 786
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v0

    iput-boolean v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    .line 787
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/w;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    .line 788
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    iget-boolean v1, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/w;Ljava/lang/Boolean;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    .line 790
    :cond_5
    iget-object v0, v3, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v0, v4}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    move-object v0, v2

    .line 791
    goto/16 :goto_0

    :cond_6
    move-object v0, v2

    .line 793
    goto/16 :goto_0
.end method

.method protected a(Landroid/database/Cursor;Z)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 1026
    new-instance v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    invoke-direct {v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;-><init>()V

    .line 1028
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    .line 1029
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    .line 1030
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->f:I

    .line 1032
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1033
    :goto_0
    if-eqz v0, :cond_3

    .line 1034
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1035
    iget-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    invoke-static {v4, v5, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1036
    invoke-virtual {v3, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    .line 1046
    :cond_0
    :goto_1
    if-nez p2, :cond_1

    .line 1047
    if-eqz v0, :cond_4

    .line 1048
    new-instance v0, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1049
    iget-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    invoke-static {v4, v5, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1050
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    .line 1054
    :goto_2
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    .line 1055
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->e:J

    .line 1057
    :cond_1
    return-object v2

    :cond_2
    move v0, v1

    .line 1032
    goto :goto_0

    .line 1037
    :cond_3
    if-eqz p2, :cond_0

    .line 1038
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1039
    iget-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1040
    iput v1, v3, Landroid/text/format/Time;->hour:I

    .line 1041
    iput v1, v3, Landroid/text/format/Time;->minute:I

    .line 1042
    iput v1, v3, Landroid/text/format/Time;->second:I

    .line 1043
    invoke-virtual {v3, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    goto :goto_1

    .line 1052
    :cond_4
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    goto :goto_2
.end method

.method protected a(I)Lcom/android/calendar/agenda/m;
    .locals 5

    .prologue
    .line 697
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v1

    .line 698
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->n:Lcom/android/calendar/agenda/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->n:Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->f:I

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->n:Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->f:I

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->n:Lcom/android/calendar/agenda/m;

    iget v2, v2, Lcom/android/calendar/agenda/m;->g:I

    add-int/2addr v0, v2

    if-ge p1, v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->n:Lcom/android/calendar/agenda/m;

    monitor-exit v1

    .line 709
    :goto_0
    return-object v0

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 703
    iget v3, v0, Lcom/android/calendar/agenda/m;->f:I

    if-gt v3, p1, :cond_1

    iget v3, v0, Lcom/android/calendar/agenda/m;->f:I

    iget v4, v0, Lcom/android/calendar/agenda/m;->g:I

    add-int/2addr v3, v4

    if-ge p1, v3, :cond_1

    .line 704
    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->n:Lcom/android/calendar/agenda/m;

    .line 705
    monitor-exit v1

    goto :goto_0

    .line 708
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 709
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected declared-synchronized a(IILandroid/text/format/Time;IILjava/lang/String;)Lcom/android/calendar/agenda/q;
    .locals 8

    .prologue
    .line 1375
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/android/calendar/agenda/q;

    invoke-direct {v0, p4, p5}, Lcom/android/calendar/agenda/q;-><init>(II)V

    .line 1377
    iput p1, v0, Lcom/android/calendar/agenda/q;->c:I

    .line 1378
    iput p2, v0, Lcom/android/calendar/agenda/q;->d:I

    .line 1379
    iput-object p6, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    .line 1380
    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/agenda/q;->h:J

    .line 1381
    iget-wide v2, v0, Lcom/android/calendar/agenda/q;->h:J

    sub-int v1, p2, p1

    int-to-long v4, v1

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/android/calendar/agenda/q;->i:J

    .line 1383
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1384
    iget-wide v2, v0, Lcom/android/calendar/agenda/q;->h:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1385
    iput-object v1, v0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1387
    monitor-exit p0

    return-object v0

    .line 1375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(ZJJ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1231
    if-eqz p1, :cond_1

    .line 1232
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->l(Landroid/content/Context;)Z

    move-result v0

    .line 1233
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1234
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selected=1 AND groupSelected=1  AND deleted=0 AND (utc_due_date IS NULL OR (utc_due_date >="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND utc_due_date < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "))"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1237
    if-eqz v0, :cond_0

    .line 1238
    const-string v0, " AND complete=0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1240
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1257
    :goto_0
    return-object v0

    .line 1242
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1243
    const-string v1, "visible=1 AND lastSynced=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1245
    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->D:Z

    if-eqz v1, :cond_2

    .line 1246
    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1249
    :cond_2
    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->F:Z

    if-eqz v1, :cond_3

    .line 1250
    const-string v1, " AND (rrule IS NULL OR rrule=\'\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1253
    :cond_3
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "JAPAN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1254
    const-string v1, " AND organizer!=\'docomo\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1257
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected declared-synchronized a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 17

    .prologue
    .line 1579
    monitor-enter p0

    if-eqz p3, :cond_0

    if-nez p2, :cond_2

    .line 1580
    :cond_0
    const/4 v2, 0x1

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1800
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 1584
    :cond_2
    :try_start_1
    check-cast p2, Lcom/android/calendar/agenda/q;

    .line 1591
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->C:Z

    if-eqz v2, :cond_4

    .line 1592
    if-eqz p3, :cond_3

    .line 1593
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 1595
    :cond_3
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    if-eqz v2, :cond_1

    .line 1596
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1579
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1601
    :cond_4
    :try_start_2
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v2, v2, 0x1

    if-lez v2, :cond_6

    .line 1602
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/android/calendar/agenda/w;->a(Landroid/database/Cursor;)V

    .line 1603
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v2, v2, -0x2

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/calendar/agenda/q;->g:I

    .line 1609
    :cond_5
    :goto_1
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->g:I

    if-lez v2, :cond_7

    .line 1610
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->m()Z

    goto :goto_0

    .line 1604
    :cond_6
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v2, v2, 0x2

    if-lez v2, :cond_5

    .line 1605
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/android/calendar/agenda/w;->b(Landroid/database/Cursor;)V

    .line 1606
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v2, v2, -0x3

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/calendar/agenda/q;->g:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1616
    :cond_7
    :try_start_3
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v16

    .line 1617
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 1618
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/agenda/AgendaBaseAdapter;->T:I

    .line 1620
    :cond_8
    if-gtz v16, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1621
    :cond_9
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/q;Lcom/android/calendar/agenda/w;)I

    move-result v2

    .line 1622
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    if-nez v3, :cond_13

    .line 1624
    if-eqz v2, :cond_a

    .line 1625
    if-gez v2, :cond_12

    .line 1626
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 1627
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/AgendaListView;->c(I)V

    .line 1662
    :cond_a
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_d

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 1663
    const/4 v2, 0x0

    .line 1664
    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1665
    :cond_b
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1666
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    const/16 v3, 0xd

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_b

    .line 1667
    const/4 v2, 0x1

    .line 1672
    :cond_c
    if-nez v2, :cond_d

    .line 1673
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    .line 1676
    :cond_d
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_e

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1677
    const/16 v2, 0xd

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    .line 1682
    new-instance v2, Lcom/android/calendar/agenda/i;

    invoke-direct {v2}, Lcom/android/calendar/agenda/i;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->L:Lcom/android/calendar/agenda/i;

    .line 1683
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->L:Lcom/android/calendar/agenda/i;

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_16

    const/4 v2, 0x1

    :goto_3
    iput-boolean v2, v3, Lcom/android/calendar/agenda/i;->o:Z

    .line 1684
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/database/Cursor;Z)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v3

    .line 1685
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->z:Z

    if-eqz v2, :cond_e

    .line 1686
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    const-wide/16 v4, 0x2

    iget-wide v6, v3, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    iget-wide v8, v3, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->a:J

    iget-wide v10, v3, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->b:J

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v15}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    .line 1696
    :cond_e
    :goto_4
    if-eqz v16, :cond_1a

    .line 1698
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    .line 1699
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->m:I

    .line 1700
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/dz;->E(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1701
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->M:Landroid/view/ActionMode;

    if-nez v2, :cond_f

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->E()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1702
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->f:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_19

    .line 1703
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->u:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->u:I

    .line 1709
    :cond_f
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/m;

    iget v2, v2, Lcom/android/calendar/agenda/m;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->N:I

    .line 1710
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/m;

    iget v2, v2, Lcom/android/calendar/agenda/m;->e:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->O:I

    .line 1763
    :cond_10
    :goto_6
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->f:I

    packed-switch v2, :pswitch_data_0

    .line 1781
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->m()Z

    move-result v2

    if-nez v2, :cond_1f

    const/4 v2, 0x1

    .line 1783
    :goto_8
    if-eqz v2, :cond_1

    .line 1784
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/AgendaListView;->setVisibility(I)V

    .line 1785
    if-nez v16, :cond_20

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(Z)V

    .line 1786
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->B()V

    .line 1787
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 1788
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->M:Landroid/view/ActionMode;

    if-eqz v2, :cond_11

    .line 1789
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->M:Landroid/view/ActionMode;

    invoke-virtual {v2}, Landroid/view/ActionMode;->invalidate()V

    .line 1791
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->V:Lcom/android/calendar/agenda/o;

    if-eqz v2, :cond_1

    .line 1792
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->V:Lcom/android/calendar/agenda/o;

    invoke-interface {v2}, Lcom/android/calendar/agenda/o;->a()V

    .line 1793
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->V:Lcom/android/calendar/agenda/o;
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 1796
    :catch_0
    move-exception v2

    .line 1797
    :try_start_4
    sget-object v3, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ". Discarding query result."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1629
    :cond_12
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->A:Landroid/os/Handler;

    new-instance v4, Lcom/android/calendar/agenda/k;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/android/calendar/agenda/k;-><init>(Lcom/android/calendar/agenda/AgendaBaseAdapter;I)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1635
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    goto/16 :goto_2

    .line 1639
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    .line 1640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/AgendaListView;->setSelection(I)V

    goto/16 :goto_2

    .line 1642
    :cond_14
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/text/format/Time;)I

    move-result v2

    .line 1643
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/AgendaListView;->b(I)Z

    move-result v3

    if-nez v3, :cond_a

    .line 1644
    if-ltz v2, :cond_15

    .line 1645
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->A:Landroid/os/Handler;

    new-instance v4, Lcom/android/calendar/agenda/l;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/android/calendar/agenda/l;-><init>(Lcom/android/calendar/agenda/AgendaBaseAdapter;I)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1653
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    .line 1654
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    const-wide/16 v4, 0x400

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v10}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto/16 :goto_2

    .line 1683
    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 1690
    :cond_17
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_18

    .line 1691
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/q;Lcom/android/calendar/agenda/w;)I

    goto/16 :goto_4

    .line 1693
    :cond_18
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 1694
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/agenda/q;->j:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->close()V

    goto/16 :goto_4

    .line 1704
    :cond_19
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->f:I

    if-nez v2, :cond_f

    .line 1705
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->s:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->s:I

    goto/16 :goto_5

    .line 1713
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    .line 1716
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 1717
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/m;

    .line 1718
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/calendar/agenda/m;

    .line 1720
    iget v4, v2, Lcom/android/calendar/agenda/m;->d:I

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/agenda/q;->d:I

    if-gt v4, v5, :cond_1b

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/agenda/q;->c:I

    iget v5, v2, Lcom/android/calendar/agenda/m;->d:I

    if-ge v4, v5, :cond_1b

    .line 1721
    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/agenda/q;->c:I

    iput v4, v2, Lcom/android/calendar/agenda/m;->d:I

    .line 1724
    :cond_1b
    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/agenda/q;->c:I

    iget v5, v3, Lcom/android/calendar/agenda/m;->e:I

    add-int/lit8 v5, v5, 0x1

    if-gt v4, v5, :cond_1c

    iget v4, v3, Lcom/android/calendar/agenda/m;->e:I

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/agenda/q;->d:I

    if-ge v4, v5, :cond_1c

    .line 1725
    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/agenda/q;->d:I

    iput v4, v3, Lcom/android/calendar/agenda/m;->e:I

    .line 1728
    :cond_1c
    iget v2, v2, Lcom/android/calendar/agenda/m;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->N:I

    .line 1729
    iget v2, v3, Lcom/android/calendar/agenda/m;->e:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->O:I

    .line 1737
    :goto_a
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->f:I

    packed-switch v2, :pswitch_data_1

    .line 1757
    :goto_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->m:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->m:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_10

    .line 1759
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    goto/16 :goto_6

    .line 1731
    :cond_1d
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->c:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->N:I

    .line 1732
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->O:I

    goto :goto_a

    .line 1739
    :pswitch_0
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->c:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->N:I

    .line 1740
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->c:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l()I

    move-result v5

    neg-int v5, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZII)I

    move-result v2

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/calendar/agenda/q;->c:I

    goto :goto_b

    .line 1744
    :pswitch_1
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->O:I

    .line 1745
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l()I

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZII)I

    move-result v2

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/calendar/agenda/q;->d:I

    goto :goto_b

    .line 1749
    :pswitch_2
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->c:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->N:I

    .line 1750
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->O:I

    .line 1751
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->c:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    neg-int v5, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZII)I

    move-result v2

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/calendar/agenda/q;->c:I

    .line 1752
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/agenda/q;->d:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZII)I

    move-result v2

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/calendar/agenda/q;->d:I

    goto/16 :goto_b

    .line 1765
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v3
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1766
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lez v2, :cond_1e

    .line 1767
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->getGroupCount()I

    move-result v4

    .line 1768
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/m;

    iget-object v2, v2, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/r;->a()I

    move-result v2

    .line 1769
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    sub-int/2addr v4, v2

    invoke-virtual {v5, v4, v2}, Lcom/android/calendar/agenda/AgendaListView;->a(II)V

    .line 1771
    :cond_1e
    monitor-exit v3

    goto/16 :goto_7

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v2

    .line 1775
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/calendar/agenda/AgendaListView;->a(Z)V
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_7

    .line 1781
    :cond_1f
    const/4 v2, 0x0

    goto/16 :goto_8

    .line 1785
    :cond_20
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 1763
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 1737
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1855
    iput-wide p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    .line 1856
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->L:Lcom/android/calendar/agenda/i;

    .line 1857
    return-void
.end method

.method public a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V
    .locals 12

    .prologue
    .line 1065
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    .line 1070
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/android/calendar/hj;->c(Landroid/text/format/Time;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1071
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    move/from16 v0, p7

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/android/calendar/al;->a(J)V

    .line 1072
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->c()V

    .line 1073
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b()V

    .line 1075
    :cond_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_f

    .line 1076
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i()I

    move-result v4

    .line 1078
    :goto_0
    const/4 v2, -0x1

    if-ne p3, v2, :cond_1

    .line 1079
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->j()I

    move-result p3

    .line 1081
    :cond_1
    new-instance v6, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->y:Ljava/lang/String;

    invoke-direct {v6, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1082
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    invoke-virtual {v6, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1084
    if-eqz p1, :cond_2

    move-object v6, p1

    .line 1086
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v8, v6, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v8, v9}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 1088
    if-nez p7, :cond_5

    invoke-virtual {p0, v3, v3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1090
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    move-wide/from16 v0, p4

    invoke-virtual {v2, v6, v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;J)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1091
    invoke-virtual {p0, v6}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/text/format/Time;)I

    move-result v2

    .line 1092
    if-ltz v2, :cond_3

    .line 1093
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/AgendaListView;->getHeaderViewsCount()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/AgendaListView;->setSelection(I)V

    .line 1096
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 1097
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    const-wide/16 v4, 0x400

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    move-object v3, p0

    move-object v7, v6

    invoke-virtual/range {v2 .. v10}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 1147
    :cond_4
    :goto_1
    return-void

    .line 1103
    :cond_5
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->n()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/android/calendar/dz;->q()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1106
    :cond_6
    const/4 v2, -0x1

    if-eq v4, v2, :cond_7

    const/4 v2, -0x1

    if-ne p3, v2, :cond_e

    .line 1108
    :cond_7
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k()I

    move-result v2

    add-int/2addr v2, v3

    move v4, v3

    .line 1111
    :goto_2
    if-ge v3, v4, :cond_8

    .line 1113
    sub-int v4, v2, v3

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k()I

    move-result v5

    if-le v4, v5, :cond_d

    .line 1114
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k()I

    move-result v2

    add-int/2addr v2, v3

    move v4, v3

    .line 1116
    :cond_8
    :goto_3
    if-le v3, v2, :cond_c

    .line 1118
    sub-int v2, v3, v4

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k()I

    move-result v5

    if-le v2, v5, :cond_b

    .line 1120
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k()I

    move-result v2

    add-int/2addr v2, v3

    .line 1124
    :goto_4
    iget-boolean v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->q:Z

    if-eqz v4, :cond_9

    .line 1125
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1126
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1127
    const/4 v3, 0x0

    iput v3, v2, Landroid/text/format/Time;->month:I

    .line 1128
    const/4 v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 1129
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 1131
    new-instance v2, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    invoke-direct {v2, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1132
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1133
    const/16 v4, 0xb

    iput v4, v2, Landroid/text/format/Time;->month:I

    .line 1134
    const/16 v4, 0x1f

    iput v4, v2, Landroid/text/format/Time;->monthDay:I

    .line 1135
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v6, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    .line 1141
    :goto_5
    const/4 v7, 0x2

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f()I

    move-result v8

    move-object v2, p0

    move-object v5, p1

    move-object/from16 v6, p6

    invoke-virtual/range {v2 .. v8}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IILandroid/text/format/Time;Ljava/lang/String;II)Z

    .line 1143
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 1144
    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 1145
    const-wide/16 v4, 0x400

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    move-object v3, p0

    invoke-virtual/range {v2 .. v10}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto/16 :goto_1

    .line 1136
    :cond_9
    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    invoke-virtual {v4}, Lcom/android/calendar/al;->e()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_a

    .line 1137
    const v3, 0x24dc87

    .line 1138
    const v4, 0x259d23

    goto :goto_5

    :cond_a
    move v4, v2

    goto :goto_5

    :cond_b
    move v2, v3

    move v3, v4

    goto :goto_4

    :cond_c
    move v3, v4

    goto :goto_4

    :cond_d
    move v4, v3

    goto/16 :goto_3

    :cond_e
    move v2, p3

    goto/16 :goto_2

    :cond_f
    move v4, p2

    goto/16 :goto_0
.end method

.method public a(Landroid/text/format/Time;JLjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 1061
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->j()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p2

    move-object v6, p4

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 1062
    return-void
.end method

.method public a(Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 1807
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->M:Landroid/view/ActionMode;

    .line 1809
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h()V

    .line 1810
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 1811
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1837
    if-eqz p1, :cond_0

    .line 1838
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 1839
    instance-of v1, v0, Lcom/android/calendar/agenda/i;

    if-eqz v1, :cond_0

    .line 1840
    check-cast v0, Lcom/android/calendar/agenda/i;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->L:Lcom/android/calendar/agenda/i;

    .line 1841
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->L:Lcom/android/calendar/agenda/i;

    iget-wide v0, v0, Lcom/android/calendar/agenda/i;->g:J

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    .line 1844
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2080
    if-nez p1, :cond_1

    .line 2091
    :cond_0
    :goto_0
    return-void

    .line 2083
    :cond_1
    const v0, 0x7f120037

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2084
    if-eqz v0, :cond_0

    .line 2087
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g()I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-lez v1, :cond_2

    const/4 v1, 0x1

    .line 2088
    :goto_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setSelected(Z)V

    .line 2089
    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2090
    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 2087
    goto :goto_1

    .line 2090
    :cond_3
    const/16 v2, 0x8

    goto :goto_2
.end method

.method public a(Ljava/util/HashSet;)V
    .locals 0

    .prologue
    .line 2585
    if-nez p1, :cond_0

    .line 2590
    :goto_0
    return-void

    .line 2588
    :cond_0
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    .line 2589
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->C()V

    goto :goto_0
.end method

.method public a(ZI)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2415
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a()I

    move-result v2

    .line 2417
    if-eqz p1, :cond_2

    move v0, v1

    .line 2418
    :goto_0
    if-ge v0, v2, :cond_0

    .line 2419
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v3

    .line 2420
    if-eqz v3, :cond_1

    iget-wide v4, v3, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 2421
    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2422
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    if-lt v3, p2, :cond_1

    .line 2423
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    const v2, 0x7f0f0038

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2424
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2432
    :cond_0
    :goto_1
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->S:Z

    .line 2433
    return-void

    .line 2418
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2430
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto :goto_1
.end method

.method protected declared-synchronized a(IILandroid/text/format/Time;Ljava/lang/String;II)Z
    .locals 1

    .prologue
    .line 1391
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/android/calendar/agenda/q;

    invoke-direct {v0, p5, p6}, Lcom/android/calendar/agenda/q;-><init>(II)V

    .line 1393
    iput-object p3, v0, Lcom/android/calendar/agenda/q;->b:Landroid/text/format/Time;

    .line 1394
    iput p1, v0, Lcom/android/calendar/agenda/q;->c:I

    .line 1395
    iput p2, v0, Lcom/android/calendar/agenda/q;->d:I

    .line 1396
    iput-object p4, v0, Lcom/android/calendar/agenda/q;->e:Ljava/lang/String;

    .line 1398
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/q;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 1391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/android/calendar/agenda/q;)Z
    .locals 2

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    .line 1403
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 1404
    if-eqz v0, :cond_0

    .line 1405
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(Lcom/android/calendar/agenda/q;)V

    .line 1408
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected a(Z)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1227
    if-eqz p1, :cond_0

    sget-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b:[Ljava/lang/String;

    goto :goto_0
.end method

.method public b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;
    .locals 1

    .prologue
    .line 736
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZ)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v0

    return-object v0
.end method

.method protected b(Landroid/text/format/Time;)Lcom/android/calendar/agenda/m;
    .locals 5

    .prologue
    .line 716
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 717
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 718
    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v0, v1}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    .line 719
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v2

    .line 720
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 721
    iget v4, v0, Lcom/android/calendar/agenda/m;->d:I

    if-gt v4, v1, :cond_0

    iget v4, v0, Lcom/android/calendar/agenda/m;->e:I

    if-gt v1, v4, :cond_0

    .line 722
    monitor-exit v2

    .line 726
    :goto_0
    return-object v0

    .line 725
    :cond_1
    monitor-exit v2

    .line 726
    const/4 v0, 0x0

    goto :goto_0

    .line 725
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1150
    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r:I

    .line 1151
    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->s:I

    .line 1152
    return-void
.end method

.method public b(IZ)V
    .locals 2

    .prologue
    .line 2367
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v0

    .line 2368
    if-nez v0, :cond_0

    .line 2378
    :goto_0
    return-void

    .line 2371
    :cond_0
    if-eqz p2, :cond_1

    .line 2372
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2377
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->C()V

    goto :goto_0

    .line 2374
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public b(Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 1814
    return-void
.end method

.method protected declared-synchronized b(Lcom/android/calendar/agenda/q;)V
    .locals 12

    .prologue
    const v7, 0x259d23

    const v6, 0x24dc87

    .line 1435
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1436
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v1, v0, Lcom/android/calendar/agenda/m;->d:I

    .line 1437
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->e:I

    .line 1439
    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->c(II)I

    move-result v2

    .line 1440
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->x:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1441
    iget v4, p1, Lcom/android/calendar/agenda/q;->f:I

    packed-switch v4, :pswitch_data_0

    .line 1466
    :cond_0
    :goto_0
    iget v0, p1, Lcom/android/calendar/agenda/q;->d:I

    if-le v0, v7, :cond_1

    .line 1467
    const v0, 0x259d23

    iput v0, p1, Lcom/android/calendar/agenda/q;->d:I

    .line 1468
    :cond_1
    iget v0, p1, Lcom/android/calendar/agenda/q;->c:I

    if-ge v0, v6, :cond_2

    .line 1469
    const v0, 0x24dc87

    iput v0, p1, Lcom/android/calendar/agenda/q;->c:I

    .line 1471
    :cond_2
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 1472
    iget v1, p1, Lcom/android/calendar/agenda/q;->c:I

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v2

    .line 1473
    iget v1, p1, Lcom/android/calendar/agenda/q;->d:I

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;I)J

    move-result-wide v4

    .line 1475
    iget v0, p1, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_4

    .line 1476
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/p;->cancelOperation(I)V

    .line 1477
    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i:Lcom/android/calendar/agenda/p;

    const/4 v9, 0x0

    const/4 v0, 0x0

    iget v1, p1, Lcom/android/calendar/agenda/q;->c:I

    iget v6, p1, Lcom/android/calendar/agenda/q;->d:I

    iget-object v7, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZIILjava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Z)[Ljava/lang/String;

    move-result-object v11

    const/4 v1, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZJJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "startDay ASC, startMinute ASC, title ASC"

    move-object v0, v8

    move v1, v9

    move-object v2, p1

    move-object v3, v10

    move-object v4, v11

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1484
    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    .line 1443
    :pswitch_0
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v5, -0x1

    :try_start_1
    invoke-virtual {p0, v1, v0, v4, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZII)I

    move-result v0

    iput v0, p1, Lcom/android/calendar/agenda/q;->d:I

    .line 1444
    iget v0, p1, Lcom/android/calendar/agenda/q;->d:I

    const/4 v1, 0x1

    const/4 v4, 0x0

    neg-int v2, v2

    invoke-virtual {p0, v0, v1, v4, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZII)I

    move-result v0

    iput v0, p1, Lcom/android/calendar/agenda/q;->c:I

    .line 1445
    iget v0, p1, Lcom/android/calendar/agenda/q;->d:I

    iget v1, p1, Lcom/android/calendar/agenda/q;->c:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v4, v0

    .line 1446
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget-wide v0, v0, Lcom/android/calendar/agenda/m;->h:J

    iput-wide v0, p1, Lcom/android/calendar/agenda/q;->i:J

    .line 1447
    iget-wide v0, p1, Lcom/android/calendar/agenda/q;->i:J

    sub-long/2addr v0, v4

    iput-wide v0, p1, Lcom/android/calendar/agenda/q;->h:J

    .line 1448
    iget-wide v0, p1, Lcom/android/calendar/agenda/q;->h:J

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1449
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    iget v1, p1, Lcom/android/calendar/agenda/q;->c:I

    invoke-static {v3, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/al;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1452
    :pswitch_1
    const/4 v1, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    :try_start_2
    invoke-virtual {p0, v0, v1, v4, v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZII)I

    move-result v0

    iput v0, p1, Lcom/android/calendar/agenda/q;->c:I

    .line 1453
    iget v0, p1, Lcom/android/calendar/agenda/q;->c:I

    const/4 v1, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v0, v1, v4, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(IZII)I

    move-result v0

    iput v0, p1, Lcom/android/calendar/agenda/q;->d:I

    .line 1454
    iget v0, p1, Lcom/android/calendar/agenda/q;->d:I

    iget v1, p1, Lcom/android/calendar/agenda/q;->c:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v4, v0

    .line 1455
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget-wide v0, v0, Lcom/android/calendar/agenda/m;->i:J

    iput-wide v0, p1, Lcom/android/calendar/agenda/q;->h:J

    .line 1456
    iget-wide v0, p1, Lcom/android/calendar/agenda/q;->h:J

    add-long/2addr v0, v4

    iput-wide v0, p1, Lcom/android/calendar/agenda/q;->i:J

    .line 1457
    iget-wide v0, p1, Lcom/android/calendar/agenda/q;->h:J

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1458
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f:Lcom/android/calendar/al;

    iget v1, p1, Lcom/android/calendar/agenda/q;->d:I

    invoke-static {v3, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/al;->a(J)V

    goto/16 :goto_0

    .line 1479
    :cond_4
    iget v0, p1, Lcom/android/calendar/agenda/q;->g:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_3

    .line 1480
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/p;->cancelOperation(I)V

    .line 1481
    iget-object v8, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i:Lcom/android/calendar/agenda/p;

    const/4 v9, 0x0

    const/4 v0, 0x1

    iget v1, p1, Lcom/android/calendar/agenda/q;->c:I

    iget v6, p1, Lcom/android/calendar/agenda/q;->d:I

    iget-object v7, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZIILjava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Z)[Ljava/lang/String;

    move-result-object v11

    const/4 v1, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZJJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->s()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, v8

    move v1, v9

    move-object v2, p1

    move-object v3, v10

    move-object v4, v11

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/p;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 1441
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected b(Z)V
    .locals 0

    .prologue
    .line 1487
    return-void
.end method

.method public b(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1301
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1302
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1303
    monitor-exit v2

    move v0, v1

    .line 1305
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->d:I

    if-gt v0, p1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->e:I

    if-gt p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    monitor-exit v2

    goto :goto_0

    .line 1306
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    .line 1305
    goto :goto_1
.end method

.method public b(ZZ)[Ljava/lang/Long;
    .locals 4

    .prologue
    .line 2525
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2526
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 2527
    if-eqz p2, :cond_2

    iget-boolean v1, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    if-eqz v1, :cond_3

    .line 2528
    :cond_1
    :goto_1
    iget-boolean v1, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->h:Z

    if-eqz v1, :cond_4

    iget-wide v0, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    neg-long v0, v0

    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2527
    :cond_2
    iget-boolean v1, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    if-nez v1, :cond_1

    :cond_3
    if-nez p1, :cond_0

    goto :goto_1

    .line 2528
    :cond_4
    iget-wide v0, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    goto :goto_2

    .line 2531
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Long;

    .line 2532
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 2533
    return-object v0
.end method

.method protected c(II)I
    .locals 3

    .prologue
    .line 1336
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l()I

    move-result v0

    .line 1337
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1338
    :try_start_0
    iget v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    if-eqz v2, :cond_0

    .line 1339
    sub-int v0, p2, p1

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x32

    iget v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    div-int/2addr v0, v2

    .line 1341
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l()I

    move-result v2

    if-le v0, v2, :cond_2

    .line 1342
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l()I

    move-result v0

    .line 1346
    :cond_1
    :goto_0
    monitor-exit v1

    .line 1347
    return v0

    .line 1343
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1344
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k()I

    move-result v0

    goto :goto_0

    .line 1346
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(I)J
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    .line 869
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(I)Lcom/android/calendar/agenda/m;

    move-result-object v2

    .line 870
    if-nez v2, :cond_1

    .line 890
    :cond_0
    :goto_0
    return-wide v0

    .line 873
    :cond_1
    iget-object v3, v2, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    iget v4, v2, Lcom/android/calendar/agenda/m;->f:I

    sub-int v4, p1, v4

    invoke-virtual {v3, v4}, Lcom/android/calendar/agenda/r;->c(I)I

    move-result v3

    .line 874
    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_0

    .line 877
    if-ltz v3, :cond_0

    .line 880
    iget-object v4, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 881
    iget-object v0, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v4

    .line 882
    iget-object v0, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    .line 883
    iget-object v0, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    sget-object v1, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v0

    .line 884
    iget-object v3, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 885
    const-wide/16 v6, -0x1

    mul-long/2addr v0, v6

    .line 886
    :cond_2
    iget-object v2, v2, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2, v4}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    goto :goto_0
.end method

.method public c(ZZ)Ljava/util/HashSet;
    .locals 4

    .prologue
    .line 2562
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2563
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 2564
    if-eqz p2, :cond_2

    iget-boolean v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->i:Z

    if-eqz v3, :cond_3

    .line 2565
    :cond_1
    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2564
    :cond_2
    iget-boolean v3, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->j:Z

    if-nez v3, :cond_1

    :cond_3
    if-nez p1, :cond_0

    goto :goto_1

    .line 2567
    :cond_4
    return-object v1
.end method

.method protected c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1155
    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->t:I

    .line 1156
    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->u:I

    .line 1157
    return-void
.end method

.method public c(Landroid/view/ActionMode;)V
    .locals 1

    .prologue
    .line 1817
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->M:Landroid/view/ActionMode;

    .line 1819
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h()V

    .line 1820
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e(Z)V

    .line 1821
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 1822
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 1825
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->D:Z

    .line 1826
    return-void
.end method

.method protected d(I)Lcom/android/calendar/agenda/m;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1174
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v3

    .line 1176
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1179
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    const/4 v4, 0x5

    if-lt v2, v4, :cond_3

    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->E()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1180
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    .line 1181
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 1182
    iget v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    iget v4, v0, Lcom/android/calendar/agenda/m;->g:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    .line 1189
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 1190
    iget-object v1, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    if-eqz v1, :cond_1

    .line 1191
    iget-object v1, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/w;->close()V

    .line 1193
    :cond_1
    monitor-exit v3

    .line 1222
    :goto_1
    return-object v0

    .line 1183
    :cond_2
    if-nez p1, :cond_0

    .line 1184
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 1185
    iget v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    iget v4, v0, Lcom/android/calendar/agenda/m;->g:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    .line 1187
    const/4 v2, 0x0

    iput v2, v0, Lcom/android/calendar/agenda/m;->g:I

    goto :goto_0

    .line 1223
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1197
    :cond_3
    :try_start_1
    iget v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    if-ne p1, v2, :cond_8

    .line 1198
    :cond_4
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    move-object v2, v0

    .line 1202
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 1203
    if-eqz v0, :cond_a

    .line 1205
    iget-object v2, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    if-eqz v2, :cond_5

    .line 1206
    iget-object v2, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/w;->close()V

    .line 1208
    :cond_5
    iget v2, v0, Lcom/android/calendar/agenda/m;->g:I

    add-int/2addr v1, v2

    move v2, v1

    move-object v1, v0

    .line 1211
    :goto_3
    if-nez v0, :cond_9

    .line 1213
    if-eqz v1, :cond_7

    .line 1214
    iget-object v0, v1, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    if-eqz v0, :cond_6

    .line 1215
    iget-object v0, v1, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/w;->close()V

    .line 1217
    :cond_6
    const/4 v0, 0x0

    iput-object v0, v1, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    .line 1218
    iput v2, v1, Lcom/android/calendar/agenda/m;->g:I

    :cond_7
    move-object v0, v1

    .line 1222
    :cond_8
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_9
    move v5, v2

    move-object v2, v1

    move v1, v5

    goto :goto_2

    :cond_a
    move v5, v1

    move-object v1, v2

    move v2, v5

    goto :goto_3
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->C:Z

    .line 1161
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d(I)Lcom/android/calendar/agenda/m;

    .line 1162
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetInvalidated()V

    .line 1163
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i:Lcom/android/calendar/agenda/p;

    if-eqz v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i:Lcom/android/calendar/agenda/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/p;->cancelOperation(I)V

    .line 1166
    :cond_0
    return-void
.end method

.method public d(II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2692
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->M:Landroid/view/ActionMode;

    if-nez v0, :cond_1

    .line 2694
    add-int v0, p1, p2

    iget v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->l:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->t:I

    iget v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->u:I

    if-ge v0, v1, :cond_0

    .line 2695
    iget v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->t:I

    .line 2696
    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->s:I

    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r:I

    .line 2697
    new-instance v0, Lcom/android/calendar/agenda/q;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f()I

    move-result v1

    invoke-direct {v0, v3, v1}, Lcom/android/calendar/agenda/q;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/q;)Z

    .line 2700
    :cond_0
    if-gt p1, v3, :cond_1

    iget v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r:I

    iget v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->s:I

    if-ge v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->G:Z

    if-eqz v0, :cond_1

    .line 2701
    iget v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->r:I

    .line 2702
    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->G:Z

    .line 2703
    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->u:I

    iput v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->t:I

    .line 2704
    new-instance v0, Lcom/android/calendar/agenda/q;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f()I

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/android/calendar/agenda/q;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Lcom/android/calendar/agenda/q;)Z

    .line 2707
    :cond_1
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 1833
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->F:Z

    .line 1834
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->d(I)Lcom/android/calendar/agenda/m;

    .line 1170
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->notifyDataSetChanged()V

    .line 1171
    return-void
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 1860
    iput p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->P:I

    .line 1861
    return-void
.end method

.method public e(Z)V
    .locals 8

    .prologue
    .line 2386
    if-eqz p1, :cond_1

    .line 2387
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a()I

    move-result v1

    .line 2389
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 2390
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v2

    .line 2391
    if-eqz v2, :cond_0

    iget-wide v4, v2, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;->d:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 2392
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2389
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2395
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 2398
    :cond_2
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->S:Z

    .line 2399
    return-void
.end method

.method protected f()I
    .locals 2

    .prologue
    .line 1262
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DCM"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1263
    :cond_1
    const/4 v0, 0x1

    .line 1265
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public f(Z)I
    .locals 1

    .prologue
    .line 2474
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(ZZ)I

    move-result v0

    return v0
.end method

.method protected f(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1871
    .line 1872
    packed-switch p1, :pswitch_data_0

    .line 1892
    const-string v0, "utc_due_date ASC, importance DESC,group_order ASC, task_order ASC"

    .line 1895
    :goto_0
    return-object v0

    .line 1874
    :pswitch_0
    const-string v0, "utc_due_date ASC, importance DESC,group_order ASC, task_order ASC"

    goto :goto_0

    .line 1877
    :pswitch_1
    const-string v0, "importance DESC, utc_due_date ASC,group_order ASC, task_order ASC"

    goto :goto_0

    .line 1880
    :pswitch_2
    const-string v0, "utc_due_date ASC, importance DESC, group_order ASC,task_order ASC"

    goto :goto_0

    .line 1883
    :pswitch_3
    const-string v0, "utc_due_date ASC, importance DESC, group_order ASC,task_order ASC"

    goto :goto_0

    .line 1886
    :pswitch_4
    const-string v0, "utc_due_date ASC, importance DESC, group_order ASC, task_order ASC"

    goto :goto_0

    .line 1889
    :pswitch_5
    const-string v0, "group_order ASC, task_order ASC"

    goto :goto_0

    .line 1872
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected g()I
    .locals 2

    .prologue
    .line 1284
    const/4 v0, 0x0

    .line 1286
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->M:Landroid/view/ActionMode;

    if-eqz v1, :cond_0

    .line 1287
    const/4 v0, 0x4

    .line 1289
    :cond_0
    return v0
.end method

.method public g(I)I
    .locals 2

    .prologue
    .line 2174
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(I)Lcom/android/calendar/agenda/m;

    move-result-object v0

    .line 2175
    if-nez v0, :cond_0

    .line 2176
    const/4 v0, -0x1

    .line 2178
    :goto_0
    return v0

    :cond_0
    iget-object v1, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    iget v0, v0, Lcom/android/calendar/agenda/m;->f:I

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/r;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public g(Z)[Ljava/lang/Long;
    .locals 1

    .prologue
    .line 2513
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(ZZ)[Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1929
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 4

    .prologue
    .line 1934
    const/4 v0, 0x0

    .line 1936
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 1937
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v3}, Lcom/android/calendar/agenda/r;->a()I

    move-result v3

    .line 1938
    add-int/2addr v1, v3

    .line 1939
    if-le v1, p1, :cond_0

    .line 1940
    iget-object v0, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    sub-int/2addr v1, v3

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/r;->d(I)I

    move-result v0

    int-to-long v0, v0

    .line 1943
    :goto_0
    return-wide v0

    :cond_1
    int-to-long v0, p2

    goto :goto_0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1949
    const/4 v0, 0x0

    .line 1957
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->getChildrenCount(I)I

    move-result v1

    .line 1958
    if-lt p2, v1, :cond_0

    .line 1959
    sget-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: Agenda group "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has no child at position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (children count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1961
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2001
    :goto_0
    return-object v0

    .line 1982
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1983
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 1984
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/r;->a()I

    move-result v4

    .line 1985
    add-int/2addr v1, v4

    .line 1986
    if-le v1, p1, :cond_1

    .line 1987
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    sub-int/2addr v1, v4

    sub-int v1, p1, v1

    invoke-virtual {v3, v1}, Lcom/android/calendar/agenda/r;->e(I)I

    move-result v1

    add-int/2addr v1, p2

    add-int/lit8 v1, v1, 0x1

    .line 1988
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v3, p1, p2}, Lcom/android/calendar/agenda/AgendaListView;->b(II)I

    move-result v3

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/AgendaListView;->getHeaderViewsCount()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1989
    invoke-virtual {p0, v3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h(I)I

    move-result v3

    .line 1990
    iget-object v0, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v0, v1, p4, p5}, Lcom/android/calendar/agenda/r;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1991
    invoke-virtual {p0, v3}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i(I)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/view/View;Z)V

    .line 1992
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->H:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 1993
    iget v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->X:I

    if-nez v1, :cond_2

    .line 1994
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->X:I

    .line 1995
    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 1998
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2000
    sget-object v0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a:Ljava/lang/String;

    const-string v1, "Error: getChildView() returns NULL"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2001
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChildrenCount(I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2006
    .line 2009
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v3

    .line 2010
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 2011
    iget-object v5, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v5}, Lcom/android/calendar/agenda/r;->a()I

    move-result v5

    .line 2012
    add-int/2addr v2, v5

    .line 2013
    if-le v2, p1, :cond_0

    .line 2014
    iget-object v0, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    sub-int v1, v2, v5

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/r;->d(I)I

    move-result v0

    .line 2019
    :goto_0
    monitor-exit v3

    .line 2020
    return v0

    .line 2019
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2025
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 4

    .prologue
    .line 2030
    const/4 v0, 0x0

    .line 2031
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v2

    .line 2032
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 2033
    iget-object v0, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/r;->a()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 2034
    goto :goto_0

    .line 2035
    :cond_0
    monitor-exit v2

    .line 2036
    return v1

    .line 2035
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 2041
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2046
    .line 2049
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v2

    .line 2050
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 2051
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/r;->a()I

    move-result v4

    .line 2052
    add-int/2addr v1, v4

    .line 2053
    if-le v1, p1, :cond_0

    .line 2054
    iget-object v3, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    iget-object v0, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    sub-int/2addr v1, v4

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/r;->e(I)I

    move-result v0

    invoke-virtual {v3, v0, p3, p4}, Lcom/android/calendar/agenda/r;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2057
    const v1, 0x7f120029

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2058
    if-eqz v1, :cond_1

    .line 2059
    if-nez p1, :cond_3

    .line 2060
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2065
    :cond_1
    :goto_0
    iget v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->W:I

    if-nez v1, :cond_2

    .line 2066
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->W:I

    .line 2067
    :cond_2
    monitor-exit v2

    .line 2071
    :goto_1
    return-object v0

    .line 2062
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2070
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public h(I)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2191
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->getGroupCount()I

    move-result v4

    move v2, v0

    move v1, v0

    .line 2195
    :goto_0
    if-ge v2, v4, :cond_3

    .line 2196
    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->getChildrenCount(I)I

    move-result v3

    .line 2198
    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->k:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v5, v2}, Lcom/android/calendar/agenda/AgendaListView;->isGroupExpanded(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2199
    if-gt v1, p1, :cond_0

    add-int v5, v1, v3

    if-gt p1, v5, :cond_0

    .line 2200
    add-int/2addr v0, p1

    .line 2213
    :goto_1
    return v0

    .line 2202
    :cond_0
    add-int/2addr v1, v3

    .line 2210
    :goto_2
    add-int/lit8 v3, v1, 0x1

    .line 2195
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_0

    .line 2204
    :cond_1
    if-ne v1, p1, :cond_2

    .line 2205
    add-int/2addr v0, p1

    goto :goto_1

    .line 2207
    :cond_2
    add-int/2addr v0, v3

    goto :goto_2

    .line 2213
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public h(Z)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 2552
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->c(ZZ)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 4

    .prologue
    .line 1293
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1294
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 1295
    iget-object v0, v0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/r;->a(I)V

    goto :goto_0

    .line 1297
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1298
    return-void
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 671
    const/4 v0, 0x1

    return v0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 1310
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1311
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1312
    const/4 v0, -0x1

    monitor-exit v1

    .line 1314
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->d:I

    monitor-exit v1

    goto :goto_0

    .line 1315
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public i(I)Z
    .locals 2

    .prologue
    .line 2451
    invoke-virtual {p0, p1}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v0

    .line 2452
    if-nez v0, :cond_0

    .line 2453
    const/4 v0, 0x0

    .line 2455
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->Q:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 2076
    const/4 v0, 0x1

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 1319
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1320
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1321
    const/4 v0, -0x1

    monitor-exit v1

    .line 1323
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    iget v0, v0, Lcom/android/calendar/agenda/m;->e:I

    monitor-exit v1

    goto :goto_0

    .line 1324
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 1328
    const/16 v0, 0x1e

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 1332
    const/16 v0, 0x3c

    return v0
.end method

.method protected m()Z
    .locals 2

    .prologue
    .line 1413
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/q;

    .line 1414
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(Lcom/android/calendar/agenda/q;)V

    .line 1415
    const/4 v0, 0x1

    .line 1418
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 3

    .prologue
    .line 1422
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->p:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/q;

    .line 1423
    iget v0, v0, Lcom/android/calendar/agenda/q;->f:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 1424
    const/4 v0, 0x1

    .line 1427
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 1431
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->C:Z

    return v0
.end method

.method public p()V
    .locals 1

    .prologue
    .line 1803
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->B:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1804
    return-void
.end method

.method public q()Lcom/android/calendar/agenda/i;
    .locals 1

    .prologue
    .line 1847
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->L:Lcom/android/calendar/agenda/i;

    return-object v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 1851
    iget-wide v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->I:J

    return-wide v0
.end method

.method public s()I
    .locals 3

    .prologue
    .line 1864
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->e:Landroid/app/Activity;

    const-string v1, "preferences_list_by"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->P:I

    .line 1867
    iget v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->P:I

    return v0
.end method

.method public t()I
    .locals 5

    .prologue
    .line 2222
    const/4 v0, 0x0

    .line 2223
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v2

    .line 2224
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 2225
    iget-object v0, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    .line 2226
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v4

    if-eqz v4, :cond_0

    .line 2229
    invoke-virtual {v0}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 2230
    goto :goto_0

    .line 2231
    :cond_1
    monitor-exit v2

    .line 2232
    return v1

    .line 2231
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public u()I
    .locals 6

    .prologue
    .line 2279
    const/4 v1, 0x0

    .line 2280
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    monitor-enter v2

    .line 2281
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->o:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/m;

    .line 2282
    iget-object v4, v0, Lcom/android/calendar/agenda/m;->a:Lcom/android/calendar/agenda/w;

    .line 2283
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->c()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2286
    const/4 v0, -0x1

    invoke-virtual {v4, v0}, Lcom/android/calendar/agenda/w;->moveToPosition(I)Z

    move v0, v1

    .line 2287
    :cond_1
    :goto_1
    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2288
    invoke-virtual {v4}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2289
    const-string v1, "complete"

    invoke-virtual {v4, v1}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v4, v1}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v1

    .line 2290
    const/4 v5, 0x1

    if-ne v1, v5, :cond_1

    .line 2291
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v0

    .line 2294
    goto :goto_0

    .line 2295
    :cond_3
    monitor-exit v2

    .line 2296
    return v1

    .line 2295
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public v()Ljava/util/HashSet;
    .locals 4

    .prologue
    .line 2309
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->a()I

    move-result v1

    .line 2310
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 2312
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 2313
    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(I)Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    move-result-object v3

    .line 2314
    if-eqz v3, :cond_0

    .line 2315
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2312
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2318
    :cond_1
    return-object v2
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 2441
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaBaseAdapter;->S:Z

    return v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 2464
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->f(Z)I

    move-result v0

    return v0
.end method

.method public y()[Ljava/lang/Long;
    .locals 1

    .prologue
    .line 2502
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g(Z)[Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public z()Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 2542
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h(Z)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method
