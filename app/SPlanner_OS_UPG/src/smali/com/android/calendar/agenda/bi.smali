.class Lcom/android/calendar/agenda/bi;
.super Ljava/lang/Object;
.source "AgendaSDExportActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/AgendaSDExportActivity;


# direct methods
.method private constructor <init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V
    .locals 0

    .prologue
    .line 438
    iput-object p1, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;Lcom/android/calendar/agenda/bc;)V
    .locals 0

    .prologue
    .line 438
    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/bi;-><init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 442
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SDExportRunnable event size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v3}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/calendar/agenda/bb;->x()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->x()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 446
    iget-object v0, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->y()[Ljava/lang/Long;

    move-result-object v2

    .line 448
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 449
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 450
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->d()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SDExportRunnable sd_exportd id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    :cond_1
    sget-object v6, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 455
    invoke-static {}, Lcom/android/calendar/gj;->a()Lcom/android/calendar/gj;

    move-result-object v5

    .line 456
    if-eqz v5, :cond_4

    .line 457
    iget-object v6, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v6}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->a(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v7}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i(Lcom/android/calendar/agenda/AgendaSDExportActivity;)I

    move-result v7

    invoke-virtual {v5, v6, v4, v7}, Lcom/android/calendar/gj;->a(Landroid/app/Activity;Landroid/net/Uri;I)Z

    move-result v4

    .line 458
    if-nez v4, :cond_2

    .line 459
    iget-object v4, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->j(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 467
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v4}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->k(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 468
    iget-object v0, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b(Lcom/android/calendar/agenda/AgendaSDExportActivity;Z)Z

    .line 469
    iget-object v0, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->a(Lcom/android/calendar/agenda/AgendaSDExportActivity;Z)Z

    .line 470
    iget-object v0, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->j(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 478
    :cond_3
    :goto_2
    return-void

    .line 462
    :cond_4
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 463
    invoke-static {}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->d()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SDImportExportUtils.getInstance() returns null"

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 448
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 475
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->j(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 476
    iget-object v0, p0, Lcom/android/calendar/agenda/bi;->a:Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b(Lcom/android/calendar/agenda/AgendaSDExportActivity;Z)Z

    goto :goto_2
.end method
