.class public Lcom/android/calendar/agenda/AgendaSDExportActivity;
.super Lcom/android/calendar/agenda/a;
.source "AgendaSDExportActivity.java"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:Z


# instance fields
.field private d:I

.field private e:I

.field private f:J

.field private g:Landroid/app/Activity;

.field private h:Lcom/android/calendar/agenda/bb;

.field private i:Lcom/android/calendar/agenda/AgendaListView;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/CheckBox;

.field private l:Landroid/view/ActionMode;

.field private m:Lcom/android/calendar/agenda/bh;

.field private n:Landroid/text/format/Time;

.field private final o:Lcom/android/calendar/agenda/bi;

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Z

.field private final s:Landroid/os/Handler;

.field private final t:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 68
    const-class v2, Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b:Ljava/lang/String;

    .line 70
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Lcom/android/calendar/agenda/a;-><init>()V

    .line 96
    iput-object v2, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g:Landroid/app/Activity;

    .line 108
    new-instance v0, Lcom/android/calendar/agenda/bi;

    invoke-direct {v0, p0, v2}, Lcom/android/calendar/agenda/bi;-><init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;Lcom/android/calendar/agenda/bc;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->o:Lcom/android/calendar/agenda/bi;

    .line 110
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->p:Z

    .line 116
    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->r:Z

    .line 122
    new-instance v0, Lcom/android/calendar/agenda/bc;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/bc;-><init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->s:Landroid/os/Handler;

    .line 142
    new-instance v0, Lcom/android/calendar/agenda/bd;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/bd;-><init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->t:Ljava/lang/Runnable;

    .line 488
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDExportActivity;Landroid/text/format/Time;)Landroid/text/format/Time;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->n:Landroid/text/format/Time;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDExportActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->l:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDExportActivity;Lcom/android/calendar/agenda/bh;)Lcom/android/calendar/agenda/bh;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->m:Lcom/android/calendar/agenda/bh;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/agenda/AgendaSDExportActivity;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->n:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/agenda/AgendaSDExportActivity;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->p:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/bb;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    return-object v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 66
    sget-boolean v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c:Z

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->k:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->l:Landroid/view/ActionMode;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->m:Lcom/android/calendar/agenda/bh;

    if-nez v0, :cond_0

    .line 318
    new-instance v0, Lcom/android/calendar/agenda/bh;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/agenda/bh;-><init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->m:Lcom/android/calendar/agenda/bh;

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->m:Lcom/android/calendar/agenda/bh;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->l:Landroid/view/ActionMode;

    .line 321
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Lcom/android/calendar/agenda/AgendaListView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->p:Z

    return v0
.end method

.method static synthetic i(Lcom/android/calendar/agenda/AgendaSDExportActivity;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->e:I

    return v0
.end method

.method static synthetic j(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->s:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/agenda/AgendaSDExportActivity;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->r:Z

    return v0
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 325
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/android/calendar/agenda/AgendaListView;->setVisibility(I)V

    .line 327
    const v0, 0x7f1200dc

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 328
    return-void

    :cond_0
    move v0, v2

    .line 325
    goto :goto_0

    :cond_1
    move v2, v1

    .line 327
    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->x()I

    move-result v0

    if-lez v0, :cond_0

    .line 484
    const/16 v0, 0x3ed

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->showDialog(I)V

    .line 486
    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 354
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 370
    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 356
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 357
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->finish()V

    goto :goto_0

    .line 363
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 354
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const v2, 0x7f020132

    const/4 v1, 0x1

    .line 332
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 334
    const v0, 0x7f1200dc

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 335
    if-eqz v0, :cond_0

    .line 336
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v1, :cond_1

    .line 337
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g:Landroid/app/Activity;

    invoke-static {v3}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v3

    .line 338
    if-eqz v1, :cond_3

    if-eqz v3, :cond_2

    const v1, 0x7f020133

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 340
    :cond_0
    return-void

    .line 336
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 338
    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 153
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onCreate(Landroid/os/Bundle;)V

    .line 155
    const v0, 0x7f040011

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->setContentView(I)V

    .line 157
    iput-object p0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->g:Landroid/app/Activity;

    .line 160
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->t:Ljava/lang/Runnable;

    invoke-static {p0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->n:Landroid/text/format/Time;

    .line 161
    if-eqz p1, :cond_6

    .line 163
    const-string v0, "key_restore_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 166
    :goto_0
    cmp-long v2, v0, v6

    if-nez v2, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "beginTime"

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 169
    sget-boolean v2, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c:Z

    if-eqz v2, :cond_0

    .line 170
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 171
    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 172
    sget-object v5, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Restore value from intent: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_0
    cmp-long v2, v0, v6

    if-nez v2, :cond_2

    .line 177
    sget-boolean v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c:Z

    if-eqz v0, :cond_1

    .line 178
    sget-object v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b:Ljava/lang/String;

    const-string v1, "Restored from current time"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 183
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->n:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 185
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 186
    const-string v1, "beginTime"

    const-wide/16 v6, -0x1

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->f:J

    .line 187
    const-string v1, "sd_export_type"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->d:I

    .line 188
    const-string v1, "sd_export_path"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->e:I

    .line 190
    const v0, 0x7f12002b

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/agenda/AgendaListView;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    .line 191
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0, v4}, Lcom/android/calendar/agenda/AgendaListView;->setItemsCanFocus(Z)V

    .line 192
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setChoiceMode(I)V

    .line 193
    new-instance v0, Lcom/android/calendar/agenda/bb;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v5, -0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/agenda/bb;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;IZI)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    .line 194
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    iget v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->d:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/bb;->k(I)V

    .line 195
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    iget-wide v2, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/agenda/bb;->b(J)V

    .line 197
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setAdapter(Lcom/android/calendar/agenda/AgendaBaseAdapter;)V

    .line 198
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 200
    new-instance v0, Lcom/android/calendar/agenda/be;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/be;-><init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V

    .line 214
    const v1, 0x7f120218

    invoke-virtual {p0, v1}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->j:Landroid/view/View;

    .line 215
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->j:Landroid/view/View;

    const v1, 0x7f120054

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->k:Landroid/widget/CheckBox;

    .line 218
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->k:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/bb;->w()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 220
    if-eqz p1, :cond_5

    const-string v0, "event_ids_selected"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 221
    const-string v0, "event_ids_selected"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 222
    new-instance v3, Ljava/util/HashSet;

    array-length v0, v2

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 223
    array-length v5, v2

    move v1, v4

    :goto_1
    if-ge v1, v5, :cond_4

    aget-object v0, v2, v1

    .line 224
    instance-of v6, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    if-eqz v6, :cond_3

    .line 225
    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 227
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/bb;->a(Ljava/util/HashSet;)V

    .line 230
    :cond_5
    const v0, 0x7f1200de

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f02d2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 231
    invoke-virtual {p0, v4}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->a(Z)V

    .line 233
    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->e()V

    .line 234
    return-void

    :cond_6
    move-wide v0, v6

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 375
    .line 377
    sparse-switch p1, :sswitch_data_0

    .line 415
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 379
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->x()I

    move-result v1

    .line 381
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 382
    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaSDExportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f01d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 383
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 384
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 385
    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIconAttribute(I)V

    .line 386
    new-instance v1, Lcom/android/calendar/agenda/bf;

    invoke-direct {v1, p0}, Lcom/android/calendar/agenda/bf;-><init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 400
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->p:Z

    goto :goto_0

    .line 404
    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f02d2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f01c1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f009e

    new-instance v2, Lcom/android/calendar/agenda/bg;

    invoke-direct {v2, p0}, Lcom/android/calendar/agenda/bg;-><init>(Lcom/android/calendar/agenda/AgendaSDExportActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 377
    nop

    :sswitch_data_0
    .sparse-switch
        0x3ed -> :sswitch_0
        0x3f2 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->l:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->l:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 312
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->r:Z

    .line 313
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onDestroy()V

    .line 314
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 297
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onPause()V

    .line 298
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 421
    invoke-super {p0, p1, p2}, Lcom/android/calendar/agenda/a;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 423
    packed-switch p1, :pswitch_data_0

    .line 432
    :goto_0
    return-void

    .line 425
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->r:Z

    .line 426
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->o:Lcom/android/calendar/agenda/bi;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 423
    nop

    :pswitch_data_0
    .packed-switch 0x3ed
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 277
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 279
    if-nez p1, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    const-string v1, "sd_export_start"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->p:Z

    .line 284
    const-string v1, "event_ids_selected"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    const-string v1, "event_ids_selected"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 286
    new-instance v3, Ljava/util/HashSet;

    array-length v1, v2

    invoke-direct {v3, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 287
    array-length v4, v2

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v0, v2, v1

    .line 288
    instance-of v5, v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    if-eqz v5, :cond_2

    .line 289
    check-cast v0, Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 287
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 291
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0, v3}, Lcom/android/calendar/agenda/bb;->a(Ljava/util/HashSet;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 8

    .prologue
    .line 238
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onResume()V

    .line 240
    sget-boolean v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c:Z

    if-eqz v0, :cond_0

    .line 241
    sget-object v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnResume to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->n:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v0

    .line 246
    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/agenda/AgendaListView;->setHideDeclinedEvents(Z)V

    .line 247
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    const/4 v1, 0x0

    const v2, 0x24dc87

    const v3, 0x259d23

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->q:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/agenda/AgendaListView;->a(Landroid/text/format/Time;IIJLjava/lang/String;Z)V

    .line 249
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->t:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 250
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->i:Lcom/android/calendar/agenda/AgendaListView;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleTime()J

    move-result-wide v0

    .line 255
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 256
    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->n:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 257
    const-string v2, "key_restore_time"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 258
    sget-boolean v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->c:Z

    if-eqz v0, :cond_0

    .line 259
    sget-object v0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->n:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_0
    const-string v0, "sd_export_start"

    iget-boolean v1, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 265
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaSDExportActivity;->h:Lcom/android/calendar/agenda/bb;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/bb;->z()Ljava/util/HashSet;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 267
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/calendar/agenda/AgendaBaseAdapter$EventInfo;

    .line 268
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 269
    const-string v0, "event_ids_selected"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 272
    :cond_1
    invoke-super {p0, p1}, Lcom/android/calendar/agenda/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 273
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 302
    invoke-static {p0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    .line 304
    invoke-super {p0}, Lcom/android/calendar/agenda/a;->onStop()V

    .line 305
    return-void
.end method
