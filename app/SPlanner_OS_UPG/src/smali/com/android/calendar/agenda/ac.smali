.class Lcom/android/calendar/agenda/ac;
.super Ljava/lang/Object;
.source "AgendaFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/z;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/z;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/android/calendar/agenda/ac;->a:Lcom/android/calendar/agenda/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 274
    iget-object v2, p0, Lcom/android/calendar/agenda/ac;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v2}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/agenda/AgendaListView;->getHeaderViewsCount()I

    move-result v2

    sub-int v2, p3, v2

    .line 275
    iget-object v3, p0, Lcom/android/calendar/agenda/ac;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v3}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->h(I)I

    move-result v2

    .line 277
    iget-object v3, p0, Lcom/android/calendar/agenda/ac;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v3}, Lcom/android/calendar/agenda/z;->f(Lcom/android/calendar/agenda/z;)Landroid/view/ActionMode;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/calendar/agenda/ac;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v3}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->g(I)I

    move-result v3

    if-eq v3, v1, :cond_1

    :cond_0
    move v1, v0

    .line 283
    :goto_0
    return v1

    .line 281
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/agenda/ac;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v3}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/agenda/ac;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v4}, Lcom/android/calendar/agenda/z;->c(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaBaseAdapter;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->i(I)Z

    move-result v4

    if-nez v4, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {v3, v2, v0}, Lcom/android/calendar/agenda/AgendaBaseAdapter;->b(IZ)V

    .line 282
    iget-object v0, p0, Lcom/android/calendar/agenda/ac;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->g(Lcom/android/calendar/agenda/z;)V

    goto :goto_0
.end method
