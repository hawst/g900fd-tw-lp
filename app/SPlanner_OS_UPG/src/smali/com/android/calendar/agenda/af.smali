.class Lcom/android/calendar/agenda/af;
.super Ljava/lang/Object;
.source "AgendaFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/agenda/z;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/z;)V
    .locals 0

    .prologue
    .line 1022
    iput-object p1, p0, Lcom/android/calendar/agenda/af;->a:Lcom/android/calendar/agenda/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1026
    iget-object v0, p0, Lcom/android/calendar/agenda/af;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1027
    iget-object v0, p0, Lcom/android/calendar/agenda/af;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaListView;->getChildCount()I

    move-result v3

    .line 1028
    if-nez v3, :cond_1

    .line 1062
    :cond_0
    return v2

    .line 1030
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/af;->a:Lcom/android/calendar/agenda/z;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/agenda/z;->a(Lcom/android/calendar/agenda/z;Z)Z

    move v1, v2

    .line 1032
    :goto_0
    if-ge v1, v3, :cond_0

    .line 1033
    iget-object v0, p0, Lcom/android/calendar/agenda/af;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v4, p0, Lcom/android/calendar/agenda/af;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v4}, Lcom/android/calendar/agenda/z;->k(Lcom/android/calendar/agenda/z;)F

    move-result v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    .line 1032
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1036
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/agenda/af;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f120037

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1037
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 1039
    invoke-virtual {v0, v6}, Landroid/view/View;->setTranslationX(F)V

    .line 1040
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/agenda/z;->c()Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1048
    iget-object v0, p0, Lcom/android/calendar/agenda/af;->a:Lcom/android/calendar/agenda/z;

    invoke-static {v0}, Lcom/android/calendar/agenda/z;->e(Lcom/android/calendar/agenda/z;)Lcom/android/calendar/agenda/AgendaListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/agenda/AgendaListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v5, 0x7f120036

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1049
    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    .line 1050
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-static {}, Lcom/android/calendar/agenda/z;->c()Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    new-instance v5, Lcom/android/calendar/agenda/ag;

    invoke-direct {v5, p0, v0}, Lcom/android/calendar/agenda/ag;-><init>(Lcom/android/calendar/agenda/af;Landroid/widget/LinearLayout;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1
.end method
