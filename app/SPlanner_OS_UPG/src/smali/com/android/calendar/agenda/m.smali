.class public Lcom/android/calendar/agenda/m;
.super Ljava/lang/Object;
.source "AgendaBaseAdapter.java"


# instance fields
.field a:Lcom/android/calendar/agenda/w;

.field b:Lcom/android/calendar/agenda/r;

.field c:Lcom/android/calendar/agenda/m;

.field d:I

.field e:I

.field f:I

.field g:I

.field h:J

.field i:J

.field j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 606
    new-instance v0, Lcom/android/calendar/agenda/r;

    invoke-direct {v0, p1, p2}, Lcom/android/calendar/agenda/r;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/calendar/agenda/m;->b:Lcom/android/calendar/agenda/r;

    .line 607
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 613
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 614
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 615
    iget v2, p0, Lcom/android/calendar/agenda/m;->d:I

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 616
    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 617
    const-string v2, "Start:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    iget v2, p0, Lcom/android/calendar/agenda/m;->e:I

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 619
    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 620
    const-string v2, " End:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const-string v0, " Offset:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/calendar/agenda/m;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 622
    const-string v0, " Size:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/calendar/agenda/m;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 623
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
