.class public Lcom/android/calendar/agenda/b;
.super Landroid/widget/ResourceCursorAdapter;
.source "AgendaAdapter.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static h:I

.field private static i:I


# instance fields
.field private b:Landroid/content/res/Resources;

.field private c:Lcom/android/calendar/d/g;

.field private d:I

.field private e:Ljava/lang/StringBuilder;

.field private f:Landroid/util/LruCache;

.field private g:Ljava/lang/String;

.field private j:Z

.field private k:Landroid/content/Context;

.field private l:Z

.field private m:I

.field private n:I

.field private o:Z

.field private p:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    const-class v0, Lcom/android/calendar/agenda/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/agenda/b;->a:Ljava/lang/String;

    .line 86
    sput v1, Lcom/android/calendar/agenda/b;->h:I

    .line 88
    sput v1, Lcom/android/calendar/agenda/b;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v2}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;Z)V

    .line 91
    iput-boolean v2, p0, Lcom/android/calendar/agenda/b;->l:Z

    .line 92
    iput v2, p0, Lcom/android/calendar/agenda/b;->m:I

    .line 93
    iput v2, p0, Lcom/android/calendar/agenda/b;->n:I

    .line 94
    iput-boolean v2, p0, Lcom/android/calendar/agenda/b;->o:Z

    .line 96
    new-instance v0, Lcom/android/calendar/agenda/c;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/c;-><init>(Lcom/android/calendar/agenda/b;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/b;->p:Ljava/lang/Runnable;

    .line 137
    iput-object p1, p0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/b;->b:Landroid/content/res/Resources;

    .line 139
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/b;->c:Lcom/android/calendar/d/g;

    .line 140
    iput p3, p0, Lcom/android/calendar/agenda/b;->d:I

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/agenda/b;->e:Ljava/lang/StringBuilder;

    .line 142
    iget-object v0, p0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gx;->a(Landroid/app/FragmentManager;)Landroid/util/LruCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/b;->f:Landroid/util/LruCache;

    .line 143
    iget-object v0, p0, Lcom/android/calendar/agenda/b;->p:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/b;->g:Ljava/lang/String;

    .line 144
    iput-boolean v2, p0, Lcom/android/calendar/agenda/b;->j:Z

    .line 145
    iget-object v0, p0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/b;->l:Z

    .line 146
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 74
    sget v0, Lcom/android/calendar/agenda/b;->i:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/agenda/b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/android/calendar/agenda/b;->g:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 607
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 608
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/agenda/b;Z)Z
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/android/calendar/agenda/b;->j:Z

    return p1
.end method

.method static synthetic b()I
    .locals 1

    .prologue
    .line 74
    sget v0, Lcom/android/calendar/agenda/b;->h:I

    return v0
.end method

.method static synthetic b(I)I
    .locals 0

    .prologue
    .line 74
    sput p0, Lcom/android/calendar/agenda/b;->i:I

    return p0
.end method

.method static synthetic b(Lcom/android/calendar/agenda/b;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/agenda/b;->b:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic c()I
    .locals 2

    .prologue
    .line 74
    sget v0, Lcom/android/calendar/agenda/b;->h:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/calendar/agenda/b;->h:I

    return v0
.end method

.method static synthetic c(I)I
    .locals 0

    .prologue
    .line 74
    sput p0, Lcom/android/calendar/agenda/b;->h:I

    return p0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 153
    iput p1, p0, Lcom/android/calendar/agenda/b;->d:I

    .line 154
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 17

    .prologue
    .line 159
    const/4 v2, 0x0

    .line 163
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 164
    instance-of v3, v3, Lcom/android/calendar/agenda/i;

    if-eqz v3, :cond_0

    .line 165
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/agenda/i;

    .line 168
    :cond_0
    if-nez v2, :cond_2f

    .line 169
    new-instance v3, Lcom/android/calendar/agenda/i;

    invoke-direct {v3}, Lcom/android/calendar/agenda/i;-><init>()V

    .line 170
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 171
    const v2, 0x7f12002d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->a:Landroid/widget/TextView;

    .line 172
    const v2, 0x7f12003c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->b:Landroid/widget/TextView;

    .line 173
    const v2, 0x7f12003a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    .line 174
    const v2, 0x7f120039

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->d:Landroid/widget/ImageView;

    .line 175
    const v2, 0x7f12003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->e:Landroid/widget/TextView;

    .line 176
    const v2, 0x7f120041

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->f:Landroid/view/View;

    .line 177
    const v2, 0x7f120038

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    .line 178
    const v2, 0x7f120043

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->h:Landroid/widget/TextView;

    .line 179
    const v2, 0x7f120040

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    .line 182
    const v2, 0x7f12003f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    .line 183
    const v2, 0x7f120042

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->i:Landroid/widget/ImageView;

    .line 184
    const v2, 0x7f120037

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v3, Lcom/android/calendar/agenda/i;->q:Landroid/widget/CheckBox;

    move-object v10, v3

    .line 187
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/android/calendar/agenda/d;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/android/calendar/agenda/d;-><init>(Lcom/android/calendar/agenda/b;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 199
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/b;->j:Z

    if-eqz v2, :cond_1

    .line 200
    const v2, 0x7f120043

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 201
    sget v3, Lcom/android/calendar/agenda/b;->i:I

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 202
    const v3, 0x7f120043

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/hj;->t(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 206
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->a:Landroid/widget/TextView;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c01de

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 209
    :cond_2
    iget-object v5, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    .line 210
    iget-object v11, v10, Lcom/android/calendar/agenda/i;->a:Landroid/widget/TextView;

    .line 211
    iget-object v12, v10, Lcom/android/calendar/agenda/i;->e:Landroid/widget/TextView;

    .line 212
    iget-object v13, v10, Lcom/android/calendar/agenda/i;->b:Landroid/widget/TextView;

    move-object/from16 v9, p3

    .line 214
    check-cast v9, Lcom/android/calendar/agenda/w;

    .line 215
    sget-object v2, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v9, v2}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 216
    const/16 v2, 0x3ef

    invoke-virtual {v9, v2}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    .line 217
    :goto_1
    const/4 v3, 0x0

    .line 219
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 221
    invoke-virtual {v9}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 222
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->d:Landroid/widget/ImageView;

    if-eqz v4, :cond_3

    .line 223
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->d:Landroid/widget/ImageView;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 225
    :cond_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/agenda/b;->d:I

    and-int/lit8 v4, v4, 0x4

    if-gtz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    instance-of v4, v4, Lcom/android/calendar/agenda/AgendaPickActivity;

    if-eqz v4, :cond_12

    .line 226
    :cond_4
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 227
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 228
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    const v7, 0x7f0f0428

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(I)V

    .line 235
    :goto_2
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v7, 0x2

    new-array v7, v7, [J

    const/4 v8, 0x0

    invoke-virtual {v9}, Lcom/android/calendar/agenda/w;->getPosition()I

    move-result v14

    int-to-long v14, v14

    aput-wide v14, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    aput-wide v14, v7, v8

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 239
    const-string v4, "complete"

    invoke-virtual {v9, v4}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v9, v4}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v4

    .line 240
    const/4 v6, 0x1

    if-ne v4, v6, :cond_2e

    .line 241
    const/4 v3, 0x1

    .line 242
    if-eqz v2, :cond_2e

    .line 243
    const/4 v2, 0x0

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 245
    :goto_3
    const/16 v4, 0xf

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 246
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_13

    const-string v6, "task_personal"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 248
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 252
    :goto_4
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 253
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->b:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    move/from16 v16, v2

    move v2, v3

    move/from16 v3, v16

    .line 286
    :cond_5
    :goto_5
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    invoke-static {v4}, Lcom/android/calendar/dz;->a(Landroid/widget/CompoundButton;)Z

    move-result v4

    .line 288
    new-instance v6, Landroid/content/Intent;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Checked"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-virtual {v7, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 289
    const-string v7, "Checked"

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 290
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v7, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 293
    iget-object v6, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 294
    iget-object v6, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setAlpha(F)V

    .line 295
    if-eqz v3, :cond_18

    .line 296
    iget-object v6, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 297
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/agenda/b;->b:Landroid/content/res/Resources;

    const v7, 0x7f0b001b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 299
    if-nez v4, :cond_6

    .line 300
    invoke-virtual {v11}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v4

    or-int/lit8 v4, v4, 0x10

    invoke-virtual {v11, v4}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 308
    :cond_6
    :goto_6
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 310
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v3

    if-nez v3, :cond_7

    .line 311
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    new-instance v4, Lcom/android/calendar/agenda/e;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v11, v5}, Lcom/android/calendar/agenda/e;-><init>(Lcom/android/calendar/agenda/b;Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 353
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/b;->d:I

    and-int/lit8 v3, v3, 0x2

    if-lez v3, :cond_19

    .line 354
    const/16 v3, 0x3f0

    invoke-virtual {v9, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v10, Lcom/android/calendar/agenda/i;->g:J

    .line 358
    :goto_7
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->q:Landroid/widget/CheckBox;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setAlpha(F)V

    .line 359
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/b;->d:I

    and-int/lit8 v3, v3, 0x4

    if-lez v3, :cond_1a

    .line 360
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->q:Landroid/widget/CheckBox;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 365
    :goto_8
    const/4 v3, 0x0

    .line 366
    invoke-virtual {v9}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v4

    if-nez v4, :cond_8

    .line 367
    const/16 v3, 0x19

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 370
    :cond_8
    const/4 v4, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 371
    if-eqz v3, :cond_1b

    .line 372
    const/16 v3, 0x18

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 377
    :goto_9
    invoke-virtual {v9}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v4

    if-eqz v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    const-string v4, "accountKey"

    invoke-virtual {v9, v4}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v9, v4}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v3

    .line 380
    :goto_a
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->h:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 382
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 383
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0035

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    .line 384
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->h:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 387
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 388
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->h:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 389
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->h:Landroid/widget/TextView;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 390
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->h:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 391
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->h:Landroid/widget/TextView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 392
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->h:Landroid/widget/TextView;

    new-instance v4, Lcom/android/calendar/agenda/g;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v4, v0, v1}, Lcom/android/calendar/agenda/g;-><init>(Lcom/android/calendar/agenda/b;Landroid/database/Cursor;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 396
    :cond_a
    sget-object v3, Lcom/android/calendar/agenda/w;->d:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v9, v3}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 397
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_c

    .line 398
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->b:Landroid/content/res/Resources;

    invoke-virtual {v9}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v3

    if-eqz v3, :cond_1d

    const v3, 0x7f0f02bf

    :goto_b
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 400
    :cond_c
    invoke-virtual {v11, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    invoke-virtual {v9}, Lcom/android/calendar/agenda/w;->a()Z

    move-result v3

    if-eqz v3, :cond_23

    .line 403
    const-string v2, "accountKey"

    invoke-virtual {v9, v2}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 404
    invoke-virtual {v9, v2}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v2

    .line 405
    const-string v3, "groupName"

    invoke-virtual {v9, v3}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v9, v3}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 407
    if-eqz v3, :cond_1f

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->c:Lcom/android/calendar/d/g;

    invoke-virtual {v4}, Lcom/android/calendar/d/g;->i()Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 408
    if-nez v2, :cond_1e

    const-string v2, "Default"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 409
    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 434
    :goto_c
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 437
    const-string v2, "importance"

    invoke-virtual {v9, v2}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 439
    packed-switch v2, :pswitch_data_0

    .line 466
    :cond_d
    :goto_d
    const/16 v2, 0x3ef

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 467
    packed-switch v2, :pswitch_data_1

    .line 483
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 488
    :cond_e
    :goto_e
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->i:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 598
    :cond_f
    :goto_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 599
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/calendar/agenda/b;->a(Landroid/view/View;)V

    .line 600
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/agenda/b;->a(Landroid/view/View;)V

    .line 601
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/calendar/agenda/b;->a(Landroid/view/View;)V

    .line 602
    new-instance v2, Lcom/android/calendar/agenda/h;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/calendar/agenda/h;-><init>(Lcom/android/calendar/agenda/b;Lcom/android/calendar/agenda/c;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 604
    :cond_10
    return-void

    .line 216
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 231
    :cond_12
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 232
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 233
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 250
    :cond_13
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setClickable(Z)V

    goto/16 :goto_4

    .line 255
    :cond_14
    const-string v4, "selfAttendeeStatus"

    invoke-virtual {v9, v4}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v9, v4}, Lcom/android/calendar/agenda/w;->getInt(I)I

    move-result v4

    const/4 v7, 0x2

    if-ne v4, v7, :cond_15

    .line 257
    const v4, 0x3ecccccd    # 0.4f

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 260
    :cond_15
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 261
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 262
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->d:Landroid/widget/ImageView;

    if-eqz v4, :cond_5

    .line 265
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->d:Landroid/widget/ImageView;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 266
    invoke-static {}, Lcom/android/calendar/dz;->x()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 267
    const/4 v4, 0x0

    .line 268
    const/16 v7, 0x1a

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_17

    .line 269
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    const/4 v7, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/android/calendar/eb;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 277
    :cond_16
    :goto_10
    if-eqz v4, :cond_5

    .line 278
    iget-object v6, v10, Lcom/android/calendar/agenda/i;->d:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 279
    iget-object v6, v10, Lcom/android/calendar/agenda/i;->d:Landroid/widget/ImageView;

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 280
    iget-object v4, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 272
    :cond_17
    const/16 v7, 0x16

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_16

    invoke-static {}, Lcom/android/calendar/dz;->p()Z

    move-result v7

    if-nez v7, :cond_16

    .line 273
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v4, v7, v14, v15}, Lcom/android/calendar/eb;->a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    goto :goto_10

    .line 302
    :cond_18
    iget-object v6, v10, Lcom/android/calendar/agenda/i;->j:Landroid/widget/CheckBox;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 303
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/agenda/b;->b:Landroid/content/res/Resources;

    const v7, 0x7f0b0138

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 305
    if-nez v4, :cond_6

    .line 306
    invoke-virtual {v11}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v4

    and-int/lit8 v4, v4, -0x11

    invoke-virtual {v11, v4}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto/16 :goto_6

    .line 356
    :cond_19
    sget-object v3, Lcom/android/calendar/agenda/w;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v9, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v10, Lcom/android/calendar/agenda/i;->g:J

    goto/16 :goto_7

    .line 362
    :cond_1a
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->q:Landroid/widget/CheckBox;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_8

    .line 373
    :cond_1b
    if-nez v4, :cond_2d

    .line 374
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b0073

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    goto/16 :goto_9

    .line 377
    :cond_1c
    invoke-static {v3}, Lcom/android/calendar/hj;->b(I)I

    move-result v3

    goto/16 :goto_a

    .line 398
    :cond_1d
    const v3, 0x7f0f02dd

    goto/16 :goto_b

    .line 411
    :cond_1e
    const v2, 0x7f0f009c

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_c

    .line 413
    :cond_1f
    const-string v2, "utc_due_date"

    invoke-virtual {v9, v2}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 414
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_c

    .line 416
    :cond_20
    const-string v2, "utc_due_date"

    invoke-virtual {v9, v2}, Lcom/android/calendar/agenda/w;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 417
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->p:Ljava/lang/Runnable;

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2, v3}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 420
    sub-long v6, v2, v4

    const-wide v14, -0x1f3a565e880L

    cmp-long v6, v6, v14

    if-gez v6, :cond_22

    .line 421
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 422
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x76e

    invoke-virtual/range {v2 .. v8}, Landroid/text/format/Time;->set(IIIIII)V

    .line 423
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 430
    :cond_21
    :goto_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    const/4 v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 431
    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_c

    .line 424
    :cond_22
    sub-long v4, v2, v4

    const-wide v6, 0x1ec4d45f520L

    cmp-long v4, v4, v6

    if-lez v4, :cond_21

    .line 425
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 426
    const/4 v3, 0x0

    const/16 v4, 0x3b

    const/16 v5, 0x17

    const/16 v6, 0x1f

    const/16 v7, 0xb

    const/16 v8, 0x7f4

    invoke-virtual/range {v2 .. v8}, Landroid/text/format/Time;->set(IIIIII)V

    .line 427
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    goto :goto_11

    .line 441
    :pswitch_0
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 442
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    const v3, 0x7f02013c

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 443
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/b;->l:Z

    if-eqz v2, :cond_d

    .line 444
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0347

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 445
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 446
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/16 v3, 0x1f4

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    goto/16 :goto_d

    .line 450
    :pswitch_1
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_d

    .line 453
    :pswitch_2
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 454
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    const v3, 0x7f02013b

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 455
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/b;->l:Z

    if-eqz v2, :cond_d

    .line 456
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0346

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 457
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 458
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/16 v3, 0x1f4

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    goto/16 :goto_d

    .line 469
    :pswitch_3
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_e

    .line 474
    :pswitch_4
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 475
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const v3, 0x7f0200b6

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 476
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/b;->l:Z

    if-eqz v2, :cond_e

    .line 477
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f003d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setHoverPopupType(I)V

    .line 479
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/16 v3, 0x1f4

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    goto/16 :goto_e

    .line 490
    :cond_23
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->l:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 491
    const/16 v3, 0x8

    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 494
    if-eqz v2, :cond_27

    .line 495
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const v3, 0x7f0200b6

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 496
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 497
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/b;->l:Z

    if-eqz v2, :cond_24

    .line 498
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f003d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 499
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setHoverPopupType(I)V

    .line 500
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/16 v3, 0x1f4

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 507
    :cond_24
    :goto_12
    const/4 v2, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v10, Lcom/android/calendar/agenda/i;->m:J

    .line 508
    const/16 v2, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v10, Lcom/android/calendar/agenda/i;->n:J

    .line 510
    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_28

    const/4 v2, 0x1

    .line 511
    :goto_13
    iput-boolean v2, v10, Lcom/android/calendar/agenda/i;->o:Z

    .line 513
    new-instance v3, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->g:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 514
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 515
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 516
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 518
    iget-wide v6, v3, Landroid/text/format/Time;->gmtoff:J

    iget-wide v4, v4, Landroid/text/format/Time;->gmtoff:J

    sub-long v4, v6, v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    .line 519
    sget-object v3, Lcom/android/calendar/agenda/w;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v9, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v6

    add-long/2addr v6, v4

    .line 520
    sget-object v3, Lcom/android/calendar/agenda/w;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v9, v3}, Lcom/android/calendar/agenda/w;->getLong(I)J

    move-result-wide v14

    add-long/2addr v4, v14

    .line 523
    const-string v3, ""

    const-string v3, " - "

    .line 524
    const-wide/16 v14, 0x0

    cmp-long v3, v4, v14

    if-nez v3, :cond_25

    .line 525
    const/16 v3, 0xa

    invoke-virtual {v9, v3}, Lcom/android/calendar/agenda/w;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 526
    if-eqz v3, :cond_29

    .line 527
    new-instance v4, Lcom/android/calendar/vcal/a/a/e;

    invoke-direct {v4}, Lcom/android/calendar/vcal/a/a/e;-><init>()V

    .line 529
    :try_start_0
    invoke-virtual {v4, v3}, Lcom/android/calendar/vcal/a/a/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_14
    invoke-virtual {v4, v6, v7}, Lcom/android/calendar/vcal/a/a/e;->a(J)J

    move-result-wide v4

    .line 539
    :cond_25
    :goto_15
    const-string v3, ""

    .line 540
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->e:Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 541
    if-eqz v2, :cond_2a

    .line 542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0045

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 543
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    :goto_16
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 559
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 562
    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 563
    if-eqz v2, :cond_2b

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2b

    .line 564
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 565
    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 572
    :goto_17
    const/4 v2, 0x0

    .line 573
    const/16 v3, 0x17

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_26

    .line 574
    const/16 v3, 0x17

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 575
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_26

    .line 576
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/b;->f:Landroid/util/LruCache;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 577
    if-nez v2, :cond_26

    .line 578
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-static {v2, v4, v5}, Lcom/android/calendar/gx;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 579
    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v3, v6}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 580
    if-eqz v2, :cond_26

    .line 581
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->f:Landroid/util/LruCache;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    :cond_26
    if-eqz v2, :cond_2c

    .line 586
    iget-object v3, v10, Lcom/android/calendar/agenda/i;->i:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 587
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->i:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 588
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/b;->l:Z

    if-eqz v2, :cond_f

    .line 589
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->i:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f03a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 590
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->i:Landroid/widget/ImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 591
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->i:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/16 v3, 0x1f4

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    goto/16 :goto_f

    .line 503
    :cond_27
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->k:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_12

    .line 510
    :cond_28
    const/4 v2, 0x0

    goto/16 :goto_13

    .line 530
    :catch_0
    move-exception v3

    .line 531
    invoke-virtual {v3}, Lcom/android/calendar/vcal/a/a/d;->printStackTrace()V

    goto/16 :goto_14

    :cond_29
    move-wide v4, v6

    .line 535
    goto/16 :goto_15

    .line 546
    :cond_2a
    const/16 v2, 0xb01

    .line 547
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-static {v3, v6, v7, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    .line 548
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    invoke-static {v6, v4, v5, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 549
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/calendar/agenda/r;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/agenda/b;->k:Landroid/content/Context;

    const v5, 0x7f0f0036

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object v2, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_16

    .line 567
    :cond_2b
    const/16 v2, 0x8

    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 568
    const-string v2, ""

    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_17

    .line 594
    :cond_2c
    iget-object v2, v10, Lcom/android/calendar/agenda/i;->i:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_f

    :cond_2d
    move v3, v4

    goto/16 :goto_9

    :cond_2e
    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    goto/16 :goto_3

    :cond_2f
    move-object v10, v2

    goto/16 :goto_0

    .line 439
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 467
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
