.class Lcom/android/calendar/agenda/f;
.super Ljava/lang/Object;
.source "AgendaAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/widget/CompoundButton;

.field final synthetic b:Z

.field final synthetic c:Lcom/android/calendar/agenda/e;


# direct methods
.method constructor <init>(Lcom/android/calendar/agenda/e;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/android/calendar/agenda/f;->c:Lcom/android/calendar/agenda/e;

    iput-object p2, p0, Lcom/android/calendar/agenda/f;->a:Landroid/widget/CompoundButton;

    iput-boolean p3, p0, Lcom/android/calendar/agenda/f;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 335
    iget-object v0, p0, Lcom/android/calendar/agenda/f;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    check-cast v0, [J

    .line 336
    if-eqz v0, :cond_0

    array-length v3, v0

    if-ge v3, v4, :cond_1

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/agenda/f;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v3, v1}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 341
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 342
    const-string v4, "complete"

    iget-boolean v5, p0, Lcom/android/calendar/agenda/f;->b:Z

    if-eqz v5, :cond_2

    move v1, v2

    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 343
    const-string v1, "date_completed"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 344
    sget-object v1, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    aget-wide v4, v0, v2

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 345
    iget-object v1, p0, Lcom/android/calendar/agenda/f;->c:Lcom/android/calendar/agenda/e;

    iget-object v1, v1, Lcom/android/calendar/agenda/e;->c:Lcom/android/calendar/agenda/b;

    invoke-static {v1}, Lcom/android/calendar/agenda/b;->a(Lcom/android/calendar/agenda/b;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method
