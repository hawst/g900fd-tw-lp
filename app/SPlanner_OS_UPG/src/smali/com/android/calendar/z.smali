.class Lcom/android/calendar/z;
.super Landroid/support/v4/app/ActionBarDrawerToggle;
.source "AllInOneActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/AllInOneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/AllInOneActivity;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
    .locals 6

    .prologue
    .line 1246
    iput-object p1, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1251
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0, v4}, Lcom/android/calendar/AllInOneActivity;->c(Lcom/android/calendar/AllInOneActivity;Z)Z

    .line 1253
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->g(Lcom/android/calendar/AllInOneActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1254
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    const v1, 0x7f12006c

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1255
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1}, Lcom/android/calendar/AllInOneActivity;->d(Lcom/android/calendar/AllInOneActivity;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 1256
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1257
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1258
    iget-object v2, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0361

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1259
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1260
    sget-boolean v1, Lcom/android/calendar/dz;->c:Z

    if-eqz v1, :cond_0

    .line 1261
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1265
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1}, Lcom/android/calendar/AllInOneActivity;->d(Lcom/android/calendar/AllInOneActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;I)V

    .line 1266
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 1267
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerClosed(Landroid/view/View;)V

    .line 1268
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1273
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerOpened(Landroid/view/View;)V

    .line 1274
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/AllInOneActivity;->c(Lcom/android/calendar/AllInOneActivity;Z)Z

    .line 1275
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->h(Lcom/android/calendar/AllInOneActivity;)V

    .line 1276
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1280
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerSlide(Landroid/view/View;F)V

    .line 1281
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1282
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->i(Lcom/android/calendar/AllInOneActivity;)V

    .line 1284
    :cond_0
    cmpl-float v0, p2, v1

    if-nez v0, :cond_3

    .line 1285
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->j(Lcom/android/calendar/AllInOneActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->g(Lcom/android/calendar/AllInOneActivity;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1288
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->e(Z)V

    .line 1297
    :cond_1
    :goto_0
    return-void

    .line 1290
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    goto :goto_0

    .line 1292
    :cond_3
    cmpl-float v0, p2, v1

    if-eqz v0, :cond_1

    .line 1293
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    iget-object v0, v0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1294
    iget-object v0, p0, Lcom/android/calendar/z;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    goto :goto_0
.end method
