.class Lcom/android/calendar/r;
.super Landroid/content/BroadcastReceiver;
.source "AllInOneActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/AllInOneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/AllInOneActivity;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 324
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 325
    const-string v0, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/32 v2, 0x10000

    const/4 v4, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;I)V

    .line 328
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/db;->a()V

    .line 363
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;Lcom/android/calendar/h/c;)Lcom/android/calendar/h/c;

    .line 364
    invoke-static {p1}, Lcom/android/calendar/dz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.sec.android.intent.CHANGE_SHARE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->c(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/h/c;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/h/c;->a:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->c(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/h/c;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/h/c;->b:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->c(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/h/c;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/h/c;->c:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 370
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->c(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/h/c;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v1}, Lcom/android/calendar/h/c;->b(Landroid/content/Context;)V

    .line 371
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 372
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/db;->b()V

    .line 377
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->c(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/h/c;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/h/c;->c:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "aw_daemon_service_key_s_planner"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 379
    const-string v0, "aw_daemon_service_key_s_planner"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 380
    iget-object v1, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v1}, Lcom/android/calendar/AllInOneActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "preferences_weather"

    if-lez v0, :cond_4

    move v5, v6

    :cond_4
    invoke-static {v1, v2, v5}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 382
    :cond_5
    :goto_1
    return-void

    .line 331
    :cond_6
    const-string v0, "android.intent.action.TIME_SET"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "android.intent.action.TIME_TICK"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 332
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/db;->a()V

    goto/16 :goto_0

    .line 335
    :cond_8
    const-string v0, "clock.date_format_changed"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 336
    invoke-static {}, Lcom/android/calendar/AllInOneActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 337
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 338
    const-string v0, "EventInfoFragment"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_9

    .line 340
    check-cast v0, Lcom/android/calendar/detail/EventInfoFragment;

    .line 341
    invoke-virtual {v0, v6}, Lcom/android/calendar/detail/EventInfoFragment;->a(Z)V

    goto :goto_1

    .line 344
    :cond_9
    const-string v0, "TaskInfoFragment"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_a

    .line 346
    check-cast v0, Lcom/android/calendar/detail/cu;

    .line 347
    invoke-virtual {v0, v6}, Lcom/android/calendar/detail/cu;->a(Z)V

    goto :goto_1

    .line 351
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/db;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/db;->a()V

    goto/16 :goto_0

    .line 354
    :cond_b
    const-string v0, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355
    iget-object v0, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;)Lcom/android/calendar/al;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 356
    const-string v0, "easymode"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 357
    iget-object v1, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1, v0}, Lcom/android/calendar/AllInOneActivity;->b(Lcom/android/calendar/AllInOneActivity;Z)Z

    .line 358
    iget-object v1, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v1}, Lcom/android/calendar/AllInOneActivity;->recreate()V

    .line 359
    iget-object v1, p0, Lcom/android/calendar/r;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1, v0}, Lcom/android/calendar/AllInOneActivity;->a(Lcom/android/calendar/AllInOneActivity;Z)V

    goto/16 :goto_0
.end method
