.class public Lcom/android/calendar/RotateTextView;
.super Landroid/widget/TextView;
.source "RotateTextView.java"


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0, v3}, Lcom/android/calendar/RotateTextView;->setHoverPopupType(I)V

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getGravity()I

    move-result v0

    .line 38
    invoke-static {v0}, Landroid/view/Gravity;->isVertical(I)Z

    move-result v1

    if-eqz v1, :cond_1

    and-int/lit8 v1, v0, 0x70

    const/16 v2, 0x50

    if-ne v1, v2, :cond_1

    .line 40
    and-int/lit8 v0, v0, 0x7

    or-int/lit8 v0, v0, 0x30

    invoke-virtual {p0, v0}, Lcom/android/calendar/RotateTextView;->setGravity(I)V

    .line 41
    iput-boolean v3, p0, Lcom/android/calendar/RotateTextView;->a:Z

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/RotateTextView;->a:Z

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 57
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 58
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getDrawableState()[I

    move-result-object v1

    iput-object v1, v0, Landroid/text/TextPaint;->drawableState:[I

    .line 59
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 63
    iget-boolean v2, p0, Lcom/android/calendar/RotateTextView;->a:Z

    if-eqz v2, :cond_1

    .line 64
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 65
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 71
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 72
    if-eqz v2, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v4

    invoke-virtual {v0}, Landroid/text/TextPaint;->ascent()F

    move-result v5

    sub-float/2addr v4, v5

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v4

    sub-float/2addr v3, v4

    .line 75
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 76
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 77
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getTextSize()F

    move-result v0

    div-float/2addr v0, v6

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    sub-float v0, v3, v0

    .line 81
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getCompoundPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 82
    invoke-virtual {v2, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 84
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 85
    return-void

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 68
    const/high16 v2, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    goto :goto_0

    .line 79
    :cond_2
    const/high16 v0, 0x40800000    # 4.0f

    add-float/2addr v0, v3

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p2, p1}, Landroid/widget/TextView;->onMeasure(II)V

    .line 51
    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/RotateTextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/RotateTextView;->setMeasuredDimension(II)V

    .line 52
    return-void
.end method
