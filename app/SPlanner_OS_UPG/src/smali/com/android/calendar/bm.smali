.class public Lcom/android/calendar/bm;
.super Ljava/lang/Object;
.source "CopyEventToSolarHelper.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field private static f:I


# instance fields
.field private b:Landroid/app/Activity;

.field private c:J

.field private d:Lcom/android/calendar/bs;

.field private e:Landroid/database/MatrixCursor;

.field private g:J

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58
    sput v2, Lcom/android/calendar/bm;->f:I

    .line 62
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "account_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/bm;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/bm;->g:J

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/bm;->h:Ljava/lang/String;

    .line 86
    if-nez p1, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    iput-object p1, p0, Lcom/android/calendar/bm;->b:Landroid/app/Activity;

    .line 90
    new-instance v0, Lcom/android/calendar/bs;

    iget-object v1, p0, Lcom/android/calendar/bm;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/bs;-><init>(Lcom/android/calendar/bm;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/bm;->d:Lcom/android/calendar/bs;

    .line 91
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 52
    sget v0, Lcom/android/calendar/bm;->f:I

    return v0
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 52
    sput p0, Lcom/android/calendar/bm;->f:I

    return p0
.end method

.method static synthetic a(Lcom/android/calendar/bm;J)J
    .locals 1

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/android/calendar/bm;->g:J

    return-wide p1
.end method

.method static synthetic a(Lcom/android/calendar/bm;Landroid/database/MatrixCursor;)Landroid/database/MatrixCursor;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/android/calendar/bm;->e:Landroid/database/MatrixCursor;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/bm;Lcom/android/calendar/bs;)Lcom/android/calendar/bs;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/android/calendar/bm;->d:Lcom/android/calendar/bs;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/bm;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/android/calendar/bm;->h:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/bm;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/android/calendar/bm;->b()V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/bm;I)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/calendar/bm;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/bm;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/bm;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 104
    iget-object v0, p0, Lcom/android/calendar/bm;->d:Lcom/android/calendar/bs;

    const/4 v1, 0x2

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/bm;->a:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND deleted!=1 AND account_type!=\'LOCAL\'"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/bs;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 220
    iget-object v0, p0, Lcom/android/calendar/bm;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 221
    iget-object v1, p0, Lcom/android/calendar/bm;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 222
    if-eqz v1, :cond_0

    .line 223
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 225
    :cond_0
    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 228
    packed-switch p1, :pswitch_data_0

    .line 240
    :goto_0
    return-void

    .line 230
    :pswitch_0
    new-instance v1, Lcom/android/calendar/bp;

    invoke-direct {v1, p0, v3}, Lcom/android/calendar/bp;-><init>(Lcom/android/calendar/bm;Lcom/android/calendar/bn;)V

    .line 231
    const-string v2, "dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 234
    :pswitch_1
    new-instance v1, Lcom/android/calendar/bu;

    iget-object v2, p0, Lcom/android/calendar/bm;->e:Landroid/database/MatrixCursor;

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/bu;-><init>(Lcom/android/calendar/bm;Landroid/database/MatrixCursor;)V

    .line 235
    const-string v2, "dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(Lcom/android/calendar/bm;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/android/calendar/bm;->c:J

    return-wide v0
.end method

.method static synthetic d(Lcom/android/calendar/bm;)Lcom/android/calendar/bs;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/bm;->d:Lcom/android/calendar/bs;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/bm;)Landroid/database/MatrixCursor;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/bm;->e:Landroid/database/MatrixCursor;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/bm;)J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/android/calendar/bm;->g:J

    return-wide v0
.end method

.method static synthetic g(Lcom/android/calendar/bm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/bm;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 3

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/android/calendar/bm;->c:J

    .line 95
    iget-object v0, p0, Lcom/android/calendar/bm;->b:Landroid/app/Activity;

    const-string v1, "preferences_copy_rec_lun_event_to_sol"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 96
    if-eqz v0, :cond_0

    .line 97
    invoke-direct {p0}, Lcom/android/calendar/bm;->b()V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/bm;->b(I)V

    goto :goto_0
.end method
