.class Lcom/android/calendar/bw;
.super Ljava/lang/Object;
.source "CopyEventToSolarHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/bu;


# direct methods
.method constructor <init>(Lcom/android/calendar/bu;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/android/calendar/bw;->a:Lcom/android/calendar/bu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 194
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/calendar/bw;->a:Lcom/android/calendar/bu;

    iget-object v1, v1, Lcom/android/calendar/bu;->a:Lcom/android/calendar/bm;

    invoke-static {v1}, Lcom/android/calendar/bm;->c(Lcom/android/calendar/bm;)J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 195
    iget-object v0, p0, Lcom/android/calendar/bw;->a:Lcom/android/calendar/bu;

    iget-object v0, v0, Lcom/android/calendar/bu;->a:Lcom/android/calendar/bm;

    invoke-static {v0}, Lcom/android/calendar/bm;->d(Lcom/android/calendar/bm;)Lcom/android/calendar/bs;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {}, Lcom/android/calendar/event/av;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/bs;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/android/calendar/bw;->a:Lcom/android/calendar/bu;

    invoke-virtual {v0}, Lcom/android/calendar/bu;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/android/calendar/bw;->a:Lcom/android/calendar/bu;

    invoke-virtual {v0}, Lcom/android/calendar/bu;->dismiss()V

    .line 199
    :cond_0
    return-void
.end method
