.class Lcom/android/calendar/ba;
.super Ljava/lang/Object;
.source "CalendarSearchView.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/CalendarSearchView;


# direct methods
.method constructor <init>(Lcom/android/calendar/CalendarSearchView;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/android/calendar/ba;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 134
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x42

    if-ne v1, v2, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 135
    iget-object v1, p0, Lcom/android/calendar/ba;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/CalendarSearchView;->playSoundEffect(I)V

    .line 136
    iget-object v0, p0, Lcom/android/calendar/ba;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0, v3}, Lcom/android/calendar/CalendarSearchView;->setImeVisibility(Z)V

    .line 137
    iget-object v0, p0, Lcom/android/calendar/ba;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->c(Lcom/android/calendar/CalendarSearchView;)Z

    move-result v0

    .line 139
    :cond_0
    return v0
.end method
