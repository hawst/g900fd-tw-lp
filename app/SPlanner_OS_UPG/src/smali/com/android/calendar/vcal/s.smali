.class Lcom/android/calendar/vcal/s;
.super Landroid/widget/BaseAdapter;
.source "VCalListActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VCalListActivity;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/calendar/vcal/VCalListActivity;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 374
    iput-object p1, p0, Lcom/android/calendar/vcal/s;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 375
    iput-object p2, p0, Lcom/android/calendar/vcal/s;->b:Landroid/content/Context;

    .line 376
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/s;->c:Ljava/lang/String;

    .line 377
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/android/calendar/vcal/s;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalListActivity;->b(Lcom/android/calendar/vcal/VCalListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/s;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalListActivity;->b(Lcom/android/calendar/vcal/VCalListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 384
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 388
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 393
    .line 396
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 397
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 398
    instance-of v1, v0, Lcom/android/calendar/vcal/t;

    if-eqz v1, :cond_8

    .line 400
    check-cast v0, Lcom/android/calendar/vcal/t;

    move-object v1, p2

    .line 404
    :goto_0
    if-nez v0, :cond_7

    .line 405
    new-instance v1, Lcom/android/calendar/vcal/t;

    invoke-direct {v1, v2}, Lcom/android/calendar/vcal/t;-><init>(Lcom/android/calendar/vcal/l;)V

    .line 406
    iget-object v0, p0, Lcom/android/calendar/vcal/s;->b:Landroid/content/Context;

    const v2, 0x7f0400bc

    invoke-static {v0, v2, p3, v7}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 407
    const v0, 0x7f12002d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/vcal/t;->a:Landroid/widget/TextView;

    .line 408
    const v0, 0x7f120064

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/vcal/t;->b:Landroid/widget/TextView;

    .line 409
    const v0, 0x7f12030c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/vcal/t;->c:Landroid/view/View;

    .line 410
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    move-object v8, v2

    .line 412
    :goto_1
    iget-object v1, v0, Lcom/android/calendar/vcal/t;->a:Landroid/widget/TextView;

    .line 413
    iget-object v9, v0, Lcom/android/calendar/vcal/t;->b:Landroid/widget/TextView;

    .line 414
    iget-object v0, v0, Lcom/android/calendar/vcal/t;->c:Landroid/view/View;

    .line 416
    iget-object v2, p0, Lcom/android/calendar/vcal/s;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/vcal/x;->c(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 418
    iget-object v0, p0, Lcom/android/calendar/vcal/s;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalListActivity;->c(Lcom/android/calendar/vcal/VCalListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/vcal/u;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 419
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/s;->c:Ljava/lang/String;

    .line 422
    :cond_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    iget-object v0, p0, Lcom/android/calendar/vcal/s;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalListActivity;->c(Lcom/android/calendar/vcal/VCalListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/vcal/u;->c(I)Ljava/lang/Long;

    move-result-object v2

    .line 425
    iget-object v0, p0, Lcom/android/calendar/vcal/s;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalListActivity;->c(Lcom/android/calendar/vcal/VCalListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/vcal/u;->d(I)Ljava/lang/Long;

    move-result-object v0

    .line 426
    if-nez v2, :cond_2

    .line 427
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 428
    :cond_2
    if-nez v0, :cond_3

    .line 429
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v4, 0x36ee80

    add-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 431
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/vcal/s;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalListActivity;->c(Lcom/android/calendar/vcal/VCalListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/calendar/vcal/u;->e(I)Z

    move-result v1

    .line 432
    if-eqz v1, :cond_6

    .line 433
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 434
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 435
    iget-object v5, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 437
    const-string v10, "UTC"

    iput-object v10, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 438
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v3, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 439
    iput-object v5, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 440
    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 441
    invoke-virtual {v3, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 443
    const-string v3, "UTC"

    iput-object v3, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 444
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 445
    iput-object v5, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 446
    invoke-virtual {v4, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 448
    invoke-virtual {v4, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 449
    iget v3, v4, Landroid/text/format/Time;->hour:I

    if-nez v3, :cond_5

    iget v3, v4, Landroid/text/format/Time;->minute:I

    if-nez v3, :cond_5

    .line 450
    invoke-virtual {v2, v0}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 451
    iget v0, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    .line 452
    :cond_4
    invoke-virtual {v4, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 454
    :cond_5
    invoke-virtual {v4, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v4, v0

    .line 457
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/vcal/s;->b:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static/range {v0 .. v7}, Lcom/android/calendar/vcal/x;->a(Landroid/content/Context;ZJJZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 459
    return-object v8

    :cond_6
    move-object v4, v0

    goto :goto_2

    :cond_7
    move-object v8, v1

    goto/16 :goto_1

    :cond_8
    move-object v0, v2

    move-object v1, v2

    goto/16 :goto_0
.end method
