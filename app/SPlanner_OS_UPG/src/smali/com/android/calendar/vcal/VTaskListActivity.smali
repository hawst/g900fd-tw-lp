.class public Lcom/android/calendar/vcal/VTaskListActivity;
.super Landroid/app/Activity;
.source "VTaskListActivity.java"


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Lcom/android/calendar/vcal/u;

.field private e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->a:Landroid/widget/ListView;

    .line 41
    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->b:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->d:Lcom/android/calendar/vcal/u;

    .line 45
    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->e:Ljava/util/ArrayList;

    .line 215
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VTaskListActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 103
    const v0, 0x7f0400c2

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskListActivity;->setContentView(I)V

    .line 105
    const v0, 0x7f1200df

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->a:Landroid/widget/ListView;

    .line 106
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->a:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    .line 107
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->a:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 108
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->a:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b008c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 109
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/vcal/aj;

    invoke-direct {v1, p0, p0}, Lcom/android/calendar/vcal/aj;-><init>(Lcom/android/calendar/vcal/VTaskListActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 110
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/vcal/ag;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/ag;-><init>(Lcom/android/calendar/vcal/VTaskListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 122
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->b()V

    .line 123
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->d:Lcom/android/calendar/vcal/u;

    invoke-virtual {v0, p1}, Lcom/android/calendar/vcal/u;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->e:Ljava/util/ArrayList;

    .line 191
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    const v2, 0x7f0f0242

    .line 126
    const v0, 0x7f12030f

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 127
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    const v0, 0x7f120310

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 129
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 130
    new-instance v1, Lcom/android/calendar/vcal/ah;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/ah;-><init>(Lcom/android/calendar/vcal/VTaskListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    const v0, 0x7f120311

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 139
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0163

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 140
    new-instance v1, Lcom/android/calendar/vcal/ai;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/ai;-><init>(Lcom/android/calendar/vcal/VTaskListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/vcal/VTaskListActivity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->d()V

    return-void
.end method

.method static synthetic c(Lcom/android/calendar/vcal/VTaskListActivity;)Lcom/android/calendar/vcal/u;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->d:Lcom/android/calendar/vcal/u;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    const v3, 0x7f0f02f2

    const/4 v2, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 150
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    .line 151
    :cond_0
    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 152
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->finish()V

    .line 187
    :cond_1
    :goto_0
    return-void

    .line 155
    :cond_2
    const-string v0, "ics"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->b:Ljava/lang/String;

    .line 156
    const-string v0, "silent"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->c:Z

    .line 158
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 159
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 160
    if-nez v0, :cond_3

    .line 161
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_3

    .line 163
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 166
    :cond_3
    invoke-static {p0, v0}, Lcom/android/calendar/vcal/x;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->b:Ljava/lang/String;

    .line 169
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 170
    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 171
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->finish()V

    goto :goto_0

    .line 175
    :cond_5
    new-instance v0, Lcom/android/calendar/vcal/u;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->d:Lcom/android/calendar/vcal/u;

    .line 176
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/vcal/VTaskListActivity;->a(Ljava/lang/String;)Z

    move-result v0

    .line 177
    if-nez v0, :cond_6

    .line 178
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->finish()V

    goto :goto_0

    .line 182
    :cond_6
    iget-boolean v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->c:Z

    if-eqz v0, :cond_1

    .line 183
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->d()V

    .line 184
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->finish()V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskListActivity;->d:Lcom/android/calendar/vcal/u;

    invoke-virtual {v0}, Lcom/android/calendar/vcal/u;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0410

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 202
    :goto_0
    iget-boolean v1, p0, Lcom/android/calendar/vcal/VTaskListActivity;->c:Z

    if-nez v1, :cond_0

    .line 203
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 205
    :cond_0
    return-void

    .line 199
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f01e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 89
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 91
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->finish()V

    .line 95
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 100
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->c()V

    .line 52
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->a()V

    .line 53
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 67
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 75
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 57
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 59
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskListActivity;->finish()V

    .line 60
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 85
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 80
    return-void
.end method
