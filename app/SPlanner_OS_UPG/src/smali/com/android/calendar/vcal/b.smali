.class Lcom/android/calendar/vcal/b;
.super Ljava/lang/Object;
.source "VCalInfoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VCalInfoActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 160
    iget-object v0, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 161
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 162
    iget-object v1, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->c(Lcom/android/calendar/vcal/VCalInfoActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalInfoActivity;->b(Lcom/android/calendar/vcal/VCalInfoActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/a/w;

    iget-object v2, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalInfoActivity;->d(Lcom/android/calendar/vcal/VCalInfoActivity;)I

    move-result v2

    int-to-long v2, v2

    const/4 v4, 0x1

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/vcal/x;->a(Landroid/content/ContentResolver;Lcom/android/a/w;JILandroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0410

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 171
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->setResult(I)V

    .line 172
    iget-object v0, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->finish()V

    .line 174
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/b;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f01e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
