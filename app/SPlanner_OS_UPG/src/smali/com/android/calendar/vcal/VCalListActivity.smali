.class public Lcom/android/calendar/vcal/VCalListActivity;
.super Landroid/app/Activity;
.source "VCalListActivity.java"


# instance fields
.field private a:Lcom/android/a/w;

.field private b:Ljava/util/ArrayList;

.field private c:Landroid/widget/ListView;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Lcom/android/calendar/vcal/u;

.field private g:Z

.field private h:Ljava/util/ArrayList;

.field private i:I

.field private j:Landroid/net/Uri;

.field private k:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 69
    iput-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->a:Lcom/android/a/w;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->b:Ljava/util/ArrayList;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->i:I

    .line 78
    iput-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->j:Landroid/net/Uri;

    .line 463
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VCalListActivity;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->i:I

    return v0
.end method

.method private a()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 347
    iget v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->i:I

    if-nez v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->f:Lcom/android/calendar/vcal/u;

    invoke-virtual {v0}, Lcom/android/calendar/vcal/u;->b()Z

    move-result v4

    .line 359
    :cond_0
    :goto_0
    return v4

    .line 350
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 352
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 354
    iget-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v7

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/a/w;

    .line 355
    const-wide/16 v2, 0x1

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/vcal/x;->a(Landroid/content/ContentResolver;Lcom/android/a/w;JILandroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 356
    add-int/lit8 v1, v6, 0x1

    :goto_2
    move v6, v1

    .line 358
    goto :goto_1

    .line 359
    :cond_2
    if-gtz v6, :cond_0

    move v4, v7

    goto :goto_0

    :cond_3
    move v1, v6

    goto :goto_2
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VCalListActivity;Z)Z
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/android/calendar/vcal/VCalListActivity;->g:Z

    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->f:Lcom/android/calendar/vcal/u;

    invoke-virtual {v0, p1}, Lcom/android/calendar/vcal/u;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->h:Ljava/util/ArrayList;

    .line 269
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/vcal/VCalListActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 9

    .prologue
    const v8, 0x7f0f02f2

    const/4 v1, 0x0

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->a:Lcom/android/a/w;

    .line 275
    :try_start_0
    invoke-static {p1}, Lcom/android/a/u;->a(Ljava/lang/String;)Lcom/android/a/w;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->a:Lcom/android/a/w;
    :try_end_0
    .catch Lcom/android/a/x; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->a:Lcom/android/a/w;

    invoke-virtual {v0}, Lcom/android/a/w;->d()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 282
    invoke-static {p0, v8, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 342
    :goto_0
    return v0

    .line 276
    :catch_0
    move-exception v0

    .line 277
    invoke-static {p0, v8, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 279
    goto :goto_0

    .line 286
    :cond_0
    new-instance v2, Lcom/android/a/c;

    invoke-direct {v2}, Lcom/android/a/c;-><init>()V

    .line 288
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->a:Lcom/android/a/w;

    invoke-virtual {v0}, Lcom/android/a/w;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/a/w;

    .line 289
    const-string v4, "VEVENT"

    invoke-virtual {v0}, Lcom/android/a/w;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 290
    new-instance v4, Landroid/text/format/Time;

    const-string v5, "UTC"

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 293
    const-string v5, "DTSTART"

    invoke-virtual {v0, v5}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v5

    .line 294
    if-eqz v5, :cond_1

    .line 295
    invoke-virtual {v5}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v6

    .line 296
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 297
    const-string v7, "TZID"

    invoke-virtual {v5, v7}, Lcom/android/a/aa;->c(Ljava/lang/String;)Lcom/android/a/y;

    move-result-object v5

    .line 298
    if-eqz v5, :cond_2

    iget-object v7, v5, Lcom/android/a/y;->b:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 299
    iget-object v5, v5, Lcom/android/a/y;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 302
    :cond_2
    :try_start_1
    invoke-virtual {v4, v6}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 307
    :cond_3
    const-string v5, "DTEND"

    invoke-virtual {v0, v5}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v5

    .line 308
    if-eqz v5, :cond_1

    .line 309
    invoke-virtual {v5}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v5

    .line 310
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 312
    :try_start_2
    invoke-virtual {v4, v5}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 323
    :cond_4
    const-string v4, "RRULE"

    invoke-virtual {v0, v4}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v4

    .line 324
    if-eqz v4, :cond_5

    .line 325
    invoke-virtual {v4}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v4

    .line 327
    :try_start_3
    invoke-virtual {v2, v4}, Lcom/android/a/c;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/android/a/e; {:try_start_3 .. :try_end_3} :catch_1

    .line 334
    :cond_5
    iget-object v4, p0, Lcom/android/calendar/vcal/VCalListActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 328
    :catch_1
    move-exception v0

    .line 329
    invoke-static {p0, v8, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 330
    goto/16 :goto_0

    .line 338
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_7

    .line 339
    invoke-static {p0, v8, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 340
    goto/16 :goto_0

    .line 342
    :cond_7
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 303
    :catch_2
    move-exception v0

    goto/16 :goto_1

    .line 313
    :catch_3
    move-exception v0

    goto/16 :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/vcal/VCalListActivity;)Lcom/android/calendar/vcal/u;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->f:Lcom/android/calendar/vcal/u;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/vcal/VCalListActivity;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->j:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/vcal/VCalListActivity;)Z
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/android/calendar/vcal/VCalListActivity;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/vcal/VCalListActivity;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->g:Z

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/vcal/VCalListActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->k:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/vcal/VCalListActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->b:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 260
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 261
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->finish()V

    .line 265
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v6, 0x7f0f023f

    const v3, 0x7f0f02f2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 83
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 86
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->k:Landroid/os/Handler;

    .line 87
    const-string v1, "ics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->d:Ljava/lang/String;

    .line 88
    const-string v1, "silent"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->e:Z

    .line 89
    iget-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->d:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 90
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->j:Landroid/net/Uri;

    .line 91
    iget-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->j:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 92
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->j:Landroid/net/Uri;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->j:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/android/calendar/vcal/x;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->d:Ljava/lang/String;

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 98
    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 99
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->finish()V

    .line 241
    :goto_0
    return-void

    .line 103
    :cond_2
    new-instance v0, Lcom/android/calendar/vcal/u;

    invoke-direct {v0, p0}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->f:Lcom/android/calendar/vcal/u;

    .line 104
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/vcal/VCalListActivity;->a(Ljava/lang/String;)Z

    move-result v0

    .line 105
    iget-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->j:Landroid/net/Uri;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/vcal/VCalListActivity;->j:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".ics"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 106
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/vcal/VCalListActivity;->b(Ljava/lang/String;)Z

    move-result v0

    .line 107
    if-eqz v0, :cond_5

    .line 108
    iput v5, p0, Lcom/android/calendar/vcal/VCalListActivity;->i:I

    .line 118
    :goto_1
    if-nez v0, :cond_7

    .line 119
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 120
    :cond_3
    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 122
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->finish()V

    goto :goto_0

    .line 110
    :cond_5
    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 111
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->finish()V

    goto :goto_0

    .line 115
    :cond_6
    iput v4, p0, Lcom/android/calendar/vcal/VCalListActivity;->i:I

    goto :goto_1

    .line 126
    :cond_7
    if-nez p1, :cond_8

    sget-boolean v0, Lcom/android/calendar/vcal/x;->a:Z

    if-eqz v0, :cond_8

    .line 127
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 128
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.android.exchange"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 129
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.snsaccountfacebook.account_type"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 130
    array-length v0, v0

    array-length v1, v1

    add-int/2addr v0, v1

    array-length v1, v2

    add-int/2addr v0, v1

    .line 132
    if-nez v0, :cond_8

    .line 139
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 140
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 141
    const-string v1, "authorities"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "com.android.calendar"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalListActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_2
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->finish()V

    .line 151
    invoke-virtual {p0, v5}, Lcom/android/calendar/vcal/VCalListActivity;->requestWindowFeature(I)Z

    goto/16 :goto_0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 156
    :cond_8
    iget-boolean v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->e:Z

    if-eqz v0, :cond_9

    .line 157
    invoke-direct {p0}, Lcom/android/calendar/vcal/VCalListActivity;->a()Z

    .line 158
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->finish()V

    goto/16 :goto_0

    .line 162
    :cond_9
    const v0, 0x7f0400bb

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalListActivity;->setContentView(I)V

    .line 164
    const v0, 0x7f12030a

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->c:Landroid/widget/ListView;

    .line 165
    iget v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->i:I

    if-nez v0, :cond_b

    .line 166
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/vcal/s;

    invoke-direct {v1, p0, p0}, Lcom/android/calendar/vcal/s;-><init>(Lcom/android/calendar/vcal/VCalListActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 167
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/vcal/l;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/l;-><init>(Lcom/android/calendar/vcal/VCalListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 195
    :goto_3
    const v0, 0x7f12030f

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 196
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    invoke-static {}, Lcom/android/calendar/hj;->j()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 198
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 199
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 200
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 202
    :cond_a
    const v0, 0x7f120310

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 203
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 204
    new-instance v1, Lcom/android/calendar/vcal/n;

    invoke-direct {v1, p0, v0}, Lcom/android/calendar/vcal/n;-><init>(Lcom/android/calendar/vcal/VCalListActivity;Landroid/widget/ImageButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    const v0, 0x7f120311

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 235
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0163

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 236
    new-instance v1, Lcom/android/calendar/vcal/q;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/q;-><init>(Lcom/android/calendar/vcal/VCalListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 181
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/vcal/r;

    invoke-direct {v1, p0, p0}, Lcom/android/calendar/vcal/r;-><init>(Lcom/android/calendar/vcal/VCalListActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 182
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalListActivity;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/vcal/m;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/m;-><init>(Lcom/android/calendar/vcal/VCalListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 255
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 256
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 245
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 250
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 247
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalListActivity;->finish()V

    .line 248
    const/4 v0, 0x1

    goto :goto_0

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
