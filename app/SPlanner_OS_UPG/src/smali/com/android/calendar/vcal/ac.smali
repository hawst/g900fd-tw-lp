.class Lcom/android/calendar/vcal/ac;
.super Ljava/lang/Object;
.source "VTaskInfoActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VTaskInfoActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/vcal/VTaskInfoActivity;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 342
    invoke-static {p2}, Lcom/android/calendar/vcal/VTaskInfoActivity;->a(I)I

    .line 344
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->c(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 345
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->c(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/vcal/VTaskInfoActivity;->a()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 347
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->c(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->a(Lcom/android/calendar/vcal/VTaskInfoActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 348
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->c(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->a(Lcom/android/calendar/vcal/VTaskInfoActivity;I)I

    .line 353
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->d(Lcom/android/calendar/vcal/VTaskInfoActivity;)I

    move-result v0

    if-nez v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-virtual {v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0288

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->a(Lcom/android/calendar/vcal/VTaskInfoActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->e(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 359
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->e(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->f(Lcom/android/calendar/vcal/VTaskInfoActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->g(Lcom/android/calendar/vcal/VTaskInfoActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->d(Lcom/android/calendar/vcal/VTaskInfoActivity;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 365
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->h(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    iget-object v2, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VTaskInfoActivity;->d(Lcom/android/calendar/vcal/VTaskInfoActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 368
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/ac;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->d(Lcom/android/calendar/vcal/VTaskInfoActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->b(Lcom/android/calendar/vcal/VTaskInfoActivity;I)I

    .line 370
    :cond_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 371
    return-void
.end method
