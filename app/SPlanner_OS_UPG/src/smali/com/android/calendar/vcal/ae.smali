.class Lcom/android/calendar/vcal/ae;
.super Landroid/widget/ResourceCursorAdapter;
.source "VTaskInfoActivity.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    const v0, 0x7f04001d

    .line 389
    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    .line 390
    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/ae;->setDropDownViewResource(I)V

    .line 391
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 395
    const v0, 0x7f120087

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 396
    const-string v1, "_sync_account_key"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 397
    const-string v2, "_sync_account"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 398
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 399
    if-eqz v0, :cond_0

    .line 400
    invoke-static {p2, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 403
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 405
    const v0, 0x7f120088

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 406
    if-eqz v0, :cond_2

    .line 407
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 409
    if-nez v3, :cond_1

    .line 410
    const v1, 0x7f0f0288

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 412
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    :cond_2
    const v0, 0x7f120089

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 416
    if-eqz v0, :cond_3

    .line 417
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 420
    :cond_3
    const v0, 0x7f12008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 421
    if-eqz v0, :cond_4

    .line 422
    invoke-static {}, Lcom/android/calendar/vcal/VTaskInfoActivity;->a()I

    move-result v1

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    if-ne v1, v2, :cond_5

    .line 423
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-nez v1, :cond_4

    .line 424
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 430
    :cond_4
    :goto_0
    return-void

    .line 426
    :cond_5
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 427
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method
