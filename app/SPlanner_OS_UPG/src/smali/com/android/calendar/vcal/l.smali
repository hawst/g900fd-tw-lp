.class Lcom/android/calendar/vcal/l;
.super Ljava/lang/Object;
.source "VCalListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VCalListActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/vcal/VCalListActivity;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/android/calendar/vcal/l;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 169
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 170
    const-string v0, "CALENDAR_TYPE"

    iget-object v2, p0, Lcom/android/calendar/vcal/l;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalListActivity;->a(Lcom/android/calendar/vcal/VCalListActivity;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 171
    const-string v2, "VCAL_DATA"

    iget-object v0, p0, Lcom/android/calendar/vcal/l;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalListActivity;->b(Lcom/android/calendar/vcal/VCalListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 172
    const-string v0, "VCAL_DATA_REMINDERS"

    iget-object v2, p0, Lcom/android/calendar/vcal/l;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalListActivity;->c(Lcom/android/calendar/vcal/VCalListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/android/calendar/vcal/u;->a(I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 173
    const-string v0, "VCAL_POSITION"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    iget-object v0, p0, Lcom/android/calendar/vcal/l;->a:Lcom/android/calendar/vcal/VCalListActivity;

    const-class v2, Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    const/high16 v0, 0x20020000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 177
    iget-object v0, p0, Lcom/android/calendar/vcal/l;->a:Lcom/android/calendar/vcal/VCalListActivity;

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/vcal/VCalListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 178
    return-void
.end method
