.class Lcom/android/calendar/vcal/j;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "VCalInfoScrollView.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VCalInfoScrollView;


# direct methods
.method constructor <init>(Lcom/android/calendar/vcal/VCalInfoScrollView;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/android/calendar/vcal/j;->a:Lcom/android/calendar/vcal/VCalInfoScrollView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 45
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 46
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 48
    invoke-static {}, Lcom/android/calendar/vcal/VCalInfoScrollView;->a()I

    move-result v4

    if-lt v2, v4, :cond_0

    if-ge v2, v3, :cond_2

    :cond_0
    move v1, v0

    .line 57
    :cond_1
    :goto_0
    return v1

    .line 51
    :cond_2
    const/4 v2, 0x0

    cmpg-float v2, p3, v2

    if-gez v2, :cond_3

    move v0, v1

    .line 53
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/vcal/j;->a:Lcom/android/calendar/vcal/VCalInfoScrollView;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalInfoScrollView;->a(Lcom/android/calendar/vcal/VCalInfoScrollView;)Lcom/android/calendar/vcal/k;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 54
    iget-object v2, p0, Lcom/android/calendar/vcal/j;->a:Lcom/android/calendar/vcal/VCalInfoScrollView;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalInfoScrollView;->a(Lcom/android/calendar/vcal/VCalInfoScrollView;)Lcom/android/calendar/vcal/k;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/android/calendar/vcal/k;->a(Z)V

    goto :goto_0
.end method
