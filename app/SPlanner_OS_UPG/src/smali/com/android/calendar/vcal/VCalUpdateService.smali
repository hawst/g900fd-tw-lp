.class public Lcom/android/calendar/vcal/VCalUpdateService;
.super Landroid/app/IntentService;
.source "VCalUpdateService.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/net/Uri;

.field private c:Z

.field private d:Lcom/android/calendar/vcal/u;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    const-string v0, "VCalUpdateService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 14
    iput-object v1, p0, Lcom/android/calendar/vcal/VCalUpdateService;->a:Ljava/lang/String;

    .line 15
    iput-object v1, p0, Lcom/android/calendar/vcal/VCalUpdateService;->b:Landroid/net/Uri;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->c:Z

    .line 26
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const v5, 0x7f0f02f2

    const/4 v4, 0x0

    .line 30
    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    .line 34
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "isTask"

    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/net/Uri;

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    const-string v0, "type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    const-string v1, "VCalUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Data type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    const-string v1, "text/x-vCalendar"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 42
    iput-boolean v4, p0, Lcom/android/calendar/vcal/VCalUpdateService;->c:Z

    .line 48
    :goto_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->b:Landroid/net/Uri;

    .line 50
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->b:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/android/calendar/vcal/x;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->a:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 53
    invoke-static {p0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 43
    :cond_2
    const-string v1, "text/x-vtodo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->c:Z

    goto :goto_1

    .line 57
    :cond_3
    new-instance v0, Lcom/android/calendar/vcal/u;

    iget-boolean v1, p0, Lcom/android/calendar/vcal/VCalUpdateService;->c:Z

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->d:Lcom/android/calendar/vcal/u;

    .line 59
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->d:Lcom/android/calendar/vcal/u;

    iget-object v1, p0, Lcom/android/calendar/vcal/VCalUpdateService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/calendar/vcal/u;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_4

    .line 60
    invoke-static {p0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 64
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalUpdateService;->d:Lcom/android/calendar/vcal/u;

    invoke-virtual {v0}, Lcom/android/calendar/vcal/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-static {p0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
