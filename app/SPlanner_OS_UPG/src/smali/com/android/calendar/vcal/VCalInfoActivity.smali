.class public Lcom/android/calendar/vcal/VCalInfoActivity;
.super Landroid/app/Activity;
.source "VCalInfoActivity.java"


# static fields
.field private static l:I


# instance fields
.field private a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private d:I

.field private e:Ljava/util/ArrayList;

.field private f:Lcom/android/calendar/vcal/u;

.field private g:Landroid/content/ContentValues;

.field private h:I

.field private i:Landroid/app/AlertDialog;

.field private j:Landroid/database/Cursor;

.field private k:Lcom/android/calendar/vcal/h;

.field private m:I

.field private n:I

.field private o:Ljava/lang/String;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/vcal/VCalInfoActivity;->l:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 68
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 70
    const-string v0, "VCalInfoActivity"

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->a:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->b:I

    .line 72
    iput v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->c:I

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->e:Ljava/util/ArrayList;

    .line 79
    iput-object v2, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->i:Landroid/app/AlertDialog;

    .line 83
    iput-object v2, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->k:Lcom/android/calendar/vcal/h;

    .line 85
    iput v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->m:I

    .line 86
    iput v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->n:I

    .line 87
    iput-object v2, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->o:Ljava/lang/String;

    .line 767
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 68
    sget v0, Lcom/android/calendar/vcal/VCalInfoActivity;->l:I

    return v0
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 68
    sput p0, Lcom/android/calendar/vcal/VCalInfoActivity;->l:I

    return p0
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VCalInfoActivity;I)I
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->m:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VCalInfoActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->i:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VCalInfoActivity;)Lcom/android/calendar/vcal/u;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->f:Lcom/android/calendar/vcal/u;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VCalInfoActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->o:Ljava/lang/String;

    return-object p1
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 599
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 600
    if-eqz v0, :cond_0

    .line 601
    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 603
    :cond_0
    return-void
.end method

.method private a(ILjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 592
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 593
    if-nez v0, :cond_0

    .line 596
    :goto_0
    return-void

    .line 595
    :cond_0
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 211
    .line 213
    :try_start_0
    invoke-static {p1}, Lcom/android/a/u;->a(Ljava/lang/String;)Lcom/android/a/w;
    :try_end_0
    .catch Lcom/android/a/x; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lcom/android/a/w;->d()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    .line 223
    const-string v0, "No events in iCalendar."

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 225
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->finish()V

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 214
    :catch_0
    move-exception v0

    .line 216
    const-string v0, "Could not parse iCalendar."

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 218
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->finish()V

    goto :goto_0

    .line 228
    :cond_1
    invoke-virtual {v0}, Lcom/android/a/w;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/a/w;

    .line 229
    const-string v2, "VEVENT"

    invoke-virtual {v0}, Lcom/android/a/w;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 230
    new-instance v2, Landroid/text/format/Time;

    const-string v3, "UTC"

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 233
    const-string v3, "DTSTART"

    invoke-virtual {v0, v3}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v3

    .line 235
    if-eqz v3, :cond_2

    .line 236
    invoke-virtual {v3}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v4

    .line 237
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 238
    const-string v5, "TZID"

    invoke-virtual {v3, v5}, Lcom/android/a/aa;->c(Ljava/lang/String;)Lcom/android/a/y;

    move-result-object v3

    .line 239
    if-eqz v3, :cond_3

    iget-object v5, v3, Lcom/android/a/y;->b:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 240
    iget-object v3, v3, Lcom/android/a/y;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 243
    :cond_3
    :try_start_1
    invoke-virtual {v2, v4}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 249
    :cond_4
    const-string v3, "DTEND"

    invoke-virtual {v0, v3}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v3

    .line 250
    if-eqz v3, :cond_2

    .line 251
    invoke-virtual {v3}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v3

    .line 252
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 256
    :try_start_2
    invoke-virtual {v2, v3}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 272
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 275
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 277
    const-string v0, "No events in iCalendar."

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 279
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->finish()V

    goto/16 :goto_0

    .line 244
    :catch_1
    move-exception v0

    goto :goto_1

    .line 257
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method static synthetic b(Lcom/android/calendar/vcal/VCalInfoActivity;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->d:I

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/vcal/VCalInfoActivity;I)I
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->n:I

    return p1
.end method

.method private b()V
    .locals 23

    .prologue
    .line 286
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->h:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_a

    .line 287
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->c()Landroid/content/ContentValues;

    move-result-object v4

    move-object v8, v4

    .line 290
    :goto_0
    const-string v4, "title"

    invoke-virtual {v8, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 291
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1d

    .line 292
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 293
    const v5, 0x7f0f02dd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v9, v4

    .line 295
    :goto_1
    const-string v4, "dtstart"

    invoke-virtual {v8, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 296
    const-string v5, "dtend"

    invoke-virtual {v8, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 297
    const-string v6, "allDay"

    invoke-virtual {v8, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eqz v6, :cond_b

    const/4 v6, 0x1

    move v10, v6

    .line 298
    :goto_2
    const-string v6, "eventLocation"

    invoke-virtual {v8, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 299
    const-string v7, "description"

    invoke-virtual {v8, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 300
    const-string v11, "rrule"

    invoke-virtual {v8, v11}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 303
    if-eqz v9, :cond_1

    .line 305
    const v11, 0x7f12002d

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v9}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(ILjava/lang/CharSequence;)V

    .line 309
    :cond_1
    if-nez v4, :cond_1c

    .line 311
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 312
    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 315
    :goto_3
    if-nez v4, :cond_2

    move-object v4, v5

    .line 321
    :cond_2
    const v9, 0x7f120303

    const/16 v11, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v11}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 329
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v9

    const-string v11, "JAPAN"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 330
    if-eqz v10, :cond_c

    .line 331
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    .line 332
    new-instance v11, Landroid/text/format/Time;

    invoke-direct {v11}, Landroid/text/format/Time;-><init>()V

    .line 333
    iget-object v12, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 335
    const-string v13, "UTC"

    iput-object v13, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 336
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v9, v14, v15}, Landroid/text/format/Time;->set(J)V

    .line 337
    iput-object v12, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 338
    const/4 v13, 0x1

    invoke-virtual {v9, v13}, Landroid/text/format/Time;->normalize(Z)J

    .line 339
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v14

    .line 341
    const-string v9, "UTC"

    iput-object v9, v11, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 342
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v11, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 343
    iput-object v12, v11, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 344
    const/4 v4, 0x1

    invoke-virtual {v11, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 346
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 347
    iget v9, v11, Landroid/text/format/Time;->hour:I

    if-nez v9, :cond_4

    iget v9, v11, Landroid/text/format/Time;->minute:I

    if-nez v9, :cond_4

    .line 348
    invoke-virtual {v5, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 349
    iget v9, v11, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v11, Landroid/text/format/Time;->monthDay:I

    .line 350
    :cond_3
    const/4 v9, 0x1

    invoke-virtual {v11, v9}, Landroid/text/format/Time;->normalize(Z)J

    .line 352
    :cond_4
    const/4 v9, 0x0

    invoke-virtual {v11, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    move-object/from16 v16, v4

    .line 380
    :goto_4
    if-eqz v10, :cond_11

    .line 381
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 382
    :cond_5
    invoke-static {v14, v15}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v14

    .line 383
    invoke-static {v12, v13}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v12

    .line 386
    :cond_6
    cmp-long v4, v14, v12

    if-nez v4, :cond_10

    .line 387
    move-object/from16 v0, p0

    invoke-static {v14, v15, v0}, Lcom/android/calendar/vcal/x;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 388
    const/4 v4, 0x0

    .line 425
    :goto_5
    const v11, 0x7f120305

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v9}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(ILjava/lang/CharSequence;)V

    .line 426
    const v9, 0x7f120305

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v11}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 427
    if-nez v4, :cond_15

    .line 428
    const v4, 0x7f120306

    const/16 v9, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v9}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 435
    :goto_6
    const v4, 0x7f120088

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->p:Landroid/widget/TextView;

    .line 437
    const v4, 0x7f120087

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->q:Landroid/widget/ImageView;

    .line 438
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->q:Landroid/widget/ImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/android/calendar/vcal/x;->c(Landroid/content/Context;)I

    move-result v9

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 440
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->d()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 441
    const v4, 0x7f120304

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 442
    const/16 v9, 0x8

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 444
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v9, 0x7f0f02ba

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->o:Ljava/lang/String;

    .line 445
    const/4 v4, 0x0

    sput v4, Lcom/android/calendar/vcal/VCalInfoActivity;->l:I

    .line 446
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->n:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->m:I

    .line 447
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->p:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->o:Ljava/lang/String;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 452
    :goto_7
    const v4, 0x7f120302

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    new-instance v9, Lcom/android/calendar/vcal/d;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/android/calendar/vcal/d;-><init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 459
    const/4 v9, 0x0

    .line 460
    const v4, 0x7f12003c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 461
    if-eqz v6, :cond_17

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_17

    .line 462
    if-eqz v4, :cond_7

    .line 463
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    :cond_7
    const v4, 0x7f12003c

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 466
    const/4 v4, 0x1

    .line 470
    :goto_8
    const v6, 0x7f120116

    const/16 v9, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v9}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 471
    if-eqz v4, :cond_18

    .line 472
    const v4, 0x7f12005d

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 475
    :goto_9
    const v4, 0x7f120307

    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 476
    const v4, 0x7f120308

    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 477
    const v4, 0x7f120307

    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 478
    const v4, 0x7f120158

    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 481
    if-eqz v8, :cond_9

    .line 482
    new-instance v4, Lcom/android/a/c;

    invoke-direct {v4}, Lcom/android/a/c;-><init>()V

    .line 484
    :try_start_0
    invoke-virtual {v4, v8}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 485
    new-instance v6, Landroid/text/format/Time;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 486
    if-eqz v10, :cond_8

    .line 487
    const-string v9, "UTC"

    iput-object v9, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 489
    :cond_8
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 490
    invoke-virtual {v4, v6}, Lcom/android/a/c;->a(Landroid/text/format/Time;)V

    .line 491
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/4 v9, 0x1

    invoke-static {v5, v6, v4, v9}, Lcom/android/calendar/dp;->a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/android/a/c;Z)Ljava/lang/String;

    move-result-object v4

    .line 493
    const v5, 0x7f120182

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(ILjava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 499
    :cond_9
    :goto_a
    const v5, 0x7f120180

    if-eqz v8, :cond_19

    const/4 v4, 0x0

    :goto_b
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 501
    if-eqz v7, :cond_1a

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1a

    .line 502
    const v4, 0x7f12010d

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v7}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(ILjava/lang/CharSequence;)V

    .line 503
    const v4, 0x7f120191

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    .line 507
    :goto_c
    return-void

    .line 289
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/vcal/VCalInfoActivity;->g:Landroid/content/ContentValues;

    move-object v8, v4

    goto/16 :goto_0

    .line 297
    :cond_b
    const/4 v6, 0x0

    move v10, v6

    goto/16 :goto_2

    .line 354
    :cond_c
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 355
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-object/from16 v16, v4

    goto/16 :goto_4

    .line 358
    :cond_d
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    .line 359
    new-instance v11, Landroid/text/format/Time;

    invoke-direct {v11}, Landroid/text/format/Time;-><init>()V

    .line 360
    iget-object v12, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 362
    const-string v13, "UTC"

    iput-object v13, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 363
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v9, v14, v15}, Landroid/text/format/Time;->set(J)V

    .line 364
    iput-object v12, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 365
    const/4 v13, 0x1

    invoke-virtual {v9, v13}, Landroid/text/format/Time;->normalize(Z)J

    .line 366
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v14

    .line 368
    const-string v13, "UTC"

    iput-object v13, v11, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 369
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v11, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 370
    iput-object v12, v11, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 371
    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/text/format/Time;->normalize(Z)J

    .line 372
    iget v12, v11, Landroid/text/format/Time;->hour:I

    if-nez v12, :cond_f

    iget v12, v11, Landroid/text/format/Time;->minute:I

    if-nez v12, :cond_f

    if-eqz v10, :cond_f

    .line 373
    iget v9, v9, Landroid/text/format/Time;->monthDay:I

    iget v12, v11, Landroid/text/format/Time;->monthDay:I

    if-eq v9, v12, :cond_e

    .line 374
    iget v9, v11, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v11, Landroid/text/format/Time;->monthDay:I

    .line 375
    :cond_e
    const/4 v9, 0x1

    invoke-virtual {v11, v9}, Landroid/text/format/Time;->normalize(Z)J

    .line 377
    :cond_f
    const/4 v9, 0x0

    invoke-virtual {v11, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    move-object/from16 v16, v4

    goto/16 :goto_4

    .line 390
    :cond_10
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v11, 0x7f0f018e

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " : "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v14, v15, v0}, Lcom/android/calendar/vcal/x;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 392
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v14, 0x7f0f019d

    invoke-virtual {v11, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, " : "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v12, v13, v0}, Lcom/android/calendar/vcal/x;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 396
    :cond_11
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/vcal/x;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v17

    .line 397
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/vcal/x;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    .line 398
    const/16 v4, 0x201

    .line 399
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 400
    const/16 v4, 0x281

    .line 402
    :cond_12
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v11

    .line 403
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v9

    .line 405
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v19

    const-string v20, "JAPAN"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1b

    .line 406
    move-object/from16 v0, p0

    invoke-static {v0, v14, v15, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v9

    .line 407
    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v4

    .line 410
    :goto_d
    const-string v11, ""

    .line 411
    invoke-static {}, Lcom/android/calendar/hj;->o()Z

    move-result v12

    if-eqz v12, :cond_13

    .line 412
    const-string v11, "\u202a"

    .line 415
    :cond_13
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_14

    .line 416
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v12, " "

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 417
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 419
    :cond_14
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0f018e

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 421
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0f019d

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 430
    :cond_15
    const v9, 0x7f120306

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v4}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(ILjava/lang/CharSequence;)V

    .line 431
    const v4, 0x7f120306

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v9}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    goto/16 :goto_6

    .line 449
    :cond_16
    const v4, 0x7f120302

    const/16 v9, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v9}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    goto/16 :goto_7

    .line 468
    :cond_17
    const v4, 0x7f12003c

    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    move v4, v9

    goto/16 :goto_8

    .line 474
    :cond_18
    const v4, 0x7f12005d

    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    goto/16 :goto_9

    .line 494
    :catch_0
    move-exception v4

    .line 495
    const/4 v8, 0x0

    .line 496
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_a

    .line 499
    :cond_19
    const/16 v4, 0x8

    goto/16 :goto_b

    .line 505
    :cond_1a
    const v4, 0x7f120191

    const/16 v5, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(II)V

    goto/16 :goto_c

    :cond_1b
    move-object v4, v9

    move-object v9, v11

    goto/16 :goto_d

    :cond_1c
    move-object/from16 v22, v5

    move-object v5, v4

    move-object/from16 v4, v22

    goto/16 :goto_3

    :cond_1d
    move-object v9, v4

    goto/16 :goto_1
.end method

.method private c()Landroid/content/ContentValues;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 510
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 511
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->e:Ljava/util/ArrayList;

    iget v4, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->d:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/a/w;

    .line 512
    const-string v4, "calendar_id"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 515
    const-string v4, "SUMMARY"

    invoke-virtual {v0, v4}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v4

    .line 516
    if-eqz v4, :cond_0

    .line 518
    const-string v5, "title"

    invoke-static {v4}, Lcom/android/calendar/vcal/x;->a(Lcom/android/a/aa;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "\\\\n"

    const-string v7, "\\\n"

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "\\\\,"

    const-string v7, ","

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "\\\\;"

    const-string v7, ";"

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    :cond_0
    new-instance v4, Landroid/text/format/Time;

    const-string v5, "UTC"

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 525
    new-instance v5, Landroid/text/format/Time;

    const-string v6, "UTC"

    invoke-direct {v5, v6}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 528
    const-string v6, "DTSTART"

    invoke-virtual {v0, v6}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v6

    .line 530
    if-eqz v6, :cond_2

    .line 531
    invoke-virtual {v6}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v7

    .line 532
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 533
    const-string v8, "TZID"

    invoke-virtual {v6, v8}, Lcom/android/a/aa;->c(Ljava/lang/String;)Lcom/android/a/y;

    move-result-object v6

    .line 534
    if-eqz v6, :cond_1

    iget-object v8, v6, Lcom/android/a/y;->b:Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 535
    iget-object v6, v6, Lcom/android/a/y;->b:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 538
    :cond_1
    :try_start_0
    invoke-virtual {v4, v7}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 539
    const-string v6, "dtstart"

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 540
    const-string v6, "allDay"

    iget-boolean v4, v4, Landroid/text/format/Time;->allDay:Z

    if-eqz v4, :cond_6

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 550
    :goto_1
    const-string v1, "DTEND"

    invoke-virtual {v0, v1}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v1

    .line 551
    if-eqz v1, :cond_2

    .line 552
    invoke-virtual {v1}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v1

    .line 553
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 557
    :try_start_1
    invoke-virtual {v5, v1}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 558
    const-string v1, "dtend"

    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 568
    :cond_2
    :goto_2
    const-string v1, "LOCATION"

    invoke-virtual {v0, v1}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v1

    .line 569
    if-eqz v1, :cond_3

    .line 572
    const-string v2, "eventLocation"

    invoke-static {v1}, Lcom/android/calendar/vcal/x;->a(Lcom/android/a/aa;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "\\\\n"

    const-string v5, "\\\n"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "\\\\,"

    const-string v5, ","

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "\\\\;"

    const-string v5, ";"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    :cond_3
    const-string v1, "DESCRIPTION"

    invoke-virtual {v0, v1}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v1

    .line 577
    if-eqz v1, :cond_4

    .line 580
    const-string v2, "description"

    invoke-static {v1}, Lcom/android/calendar/vcal/x;->a(Lcom/android/a/aa;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "\\\\n"

    const-string v5, "\\\n"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "\\\\,"

    const-string v5, ","

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "\\\\;"

    const-string v5, ";"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_4
    const-string v1, "RRULE"

    invoke-virtual {v0, v1}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v0

    .line 585
    if-eqz v0, :cond_5

    .line 586
    const-string v1, "rrule"

    invoke-virtual {v0}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_5
    return-object v3

    :cond_6
    move v1, v2

    .line 540
    goto/16 :goto_0

    .line 547
    :cond_7
    const-string v1, "allDay"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 559
    :catch_0
    move-exception v1

    goto :goto_2

    .line 541
    :catch_1
    move-exception v1

    goto/16 :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/vcal/VCalInfoActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/vcal/VCalInfoActivity;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->m:I

    return v0
.end method

.method private d()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 607
    const/4 v6, 0x1

    .line 609
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 610
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/vcal/x;->b:[Ljava/lang/String;

    const-string v3, "calendar_access_level>=500 AND account_name!=\'docomo\' AND deleted!=1"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v1, v0

    .line 618
    :goto_0
    :try_start_0
    invoke-static {v1}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 620
    iput-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->j:Landroid/database/Cursor;

    .line 622
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->j:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->j:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->a:Ljava/lang/String;

    const-string v2, " no calendar account found "

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 624
    const/4 v0, 0x0

    .line 630
    :goto_1
    if-eqz v1, :cond_1

    .line 631
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 635
    :cond_1
    return v0

    .line 613
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/vcal/x;->b:[Ljava/lang/String;

    const-string v3, "calendar_access_level>=500"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 626
    :cond_3
    :try_start_1
    new-instance v0, Lcom/android/calendar/vcal/h;

    iget-object v2, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->j:Landroid/database/Cursor;

    invoke-direct {v0, p0, v2}, Lcom/android/calendar/vcal/h;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->k:Lcom/android/calendar/vcal/h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v6

    goto :goto_1

    .line 630
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 631
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 640
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->i:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 641
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 642
    const v1, 0x7f0f01a8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 643
    iget-object v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->k:Lcom/android/calendar/vcal/h;

    sget v2, Lcom/android/calendar/vcal/VCalInfoActivity;->l:I

    new-instance v3, Lcom/android/calendar/vcal/e;

    invoke-direct {v3, p0}, Lcom/android/calendar/vcal/e;-><init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 679
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/vcal/f;

    invoke-direct {v2, p0}, Lcom/android/calendar/vcal/f;-><init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 688
    new-instance v1, Lcom/android/calendar/vcal/g;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/g;-><init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 696
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 697
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->i:Landroid/app/AlertDialog;

    .line 700
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/vcal/VCalInfoActivity;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->e()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->j:Landroid/database/Cursor;

    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 763
    new-instance v0, Lcom/android/calendar/vcal/i;

    invoke-direct {v0, p0}, Lcom/android/calendar/vcal/i;-><init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V

    .line 764
    invoke-virtual {v0}, Lcom/android/calendar/vcal/i;->start()V

    .line 765
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/vcal/VCalInfoActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->p:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/vcal/VCalInfoActivity;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->n:I

    return v0
.end method

.method static synthetic j(Lcom/android/calendar/vcal/VCalInfoActivity;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->h:I

    return v0
.end method

.method static synthetic k(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->g:Landroid/content/ContentValues;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->q:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 191
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->p:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v4, 0x7f0f02f2

    const v3, 0x7f0f023f

    const/4 v2, 0x0

    .line 101
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    const v0, 0x7f0400ba

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->setContentView(I)V

    .line 104
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 105
    const-string v0, "CALENDAR_TYPE"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->h:I

    .line 106
    const-string v0, "VCAL_POSITION"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->d:I

    .line 107
    iget v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->h:I

    if-nez v0, :cond_1

    .line 108
    const-string v0, "VCAL_DATA"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->g:Landroid/content/ContentValues;

    .line 109
    const-string v0, "VCAL_DATA_REMINDERS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->g:Landroid/content/ContentValues;

    if-nez v1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 113
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->finish()V

    .line 185
    :goto_0
    return-void

    .line 116
    :cond_0
    new-instance v1, Lcom/android/calendar/vcal/u;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->f:Lcom/android/calendar/vcal/u;

    .line 117
    iget-object v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->f:Lcom/android/calendar/vcal/u;

    iget-object v2, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->g:Landroid/content/ContentValues;

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/vcal/u;->a(Landroid/content/ContentValues;Ljava/util/ArrayList;)V

    .line 135
    :goto_1
    invoke-direct {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->b()V

    .line 136
    invoke-direct {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->f()V

    .line 138
    const v0, 0x7f12030f

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 139
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    const v0, 0x7f120310

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 141
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 143
    iget v1, p0, Lcom/android/calendar/vcal/VCalInfoActivity;->h:I

    if-nez v1, :cond_4

    .line 144
    new-instance v1, Lcom/android/calendar/vcal/a;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/a;-><init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    :goto_2
    const v0, 0x7f120311

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 179
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0163

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 180
    new-instance v1, Lcom/android/calendar/vcal/c;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/c;-><init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 122
    if-nez v0, :cond_2

    .line 123
    const-string v0, "VCAL_DATA"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 125
    :cond_2
    invoke-static {p0, v0}, Lcom/android/calendar/vcal/x;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_3

    .line 127
    invoke-direct {p0, v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 129
    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 131
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->finish()V

    goto/16 :goto_0

    .line 158
    :cond_4
    new-instance v1, Lcom/android/calendar/vcal/b;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/b;-><init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 206
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 207
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 196
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 201
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 198
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VCalInfoActivity;->finish()V

    .line 199
    const/4 v0, 0x1

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
