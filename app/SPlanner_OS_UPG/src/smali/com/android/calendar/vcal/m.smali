.class Lcom/android/calendar/vcal/m;
.super Ljava/lang/Object;
.source "VCalListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VCalListActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/vcal/VCalListActivity;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/android/calendar/vcal/m;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 184
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 185
    const-string v1, "CALENDAR_TYPE"

    iget-object v2, p0, Lcom/android/calendar/vcal/m;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalListActivity;->a(Lcom/android/calendar/vcal/VCalListActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 186
    const-string v1, "VCAL_DATA"

    iget-object v2, p0, Lcom/android/calendar/vcal/m;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalListActivity;->d(Lcom/android/calendar/vcal/VCalListActivity;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 187
    const-string v1, "VCAL_POSITION"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 188
    iget-object v1, p0, Lcom/android/calendar/vcal/m;->a:Lcom/android/calendar/vcal/VCalListActivity;

    const-class v2, Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 190
    iget-object v1, p0, Lcom/android/calendar/vcal/m;->a:Lcom/android/calendar/vcal/VCalListActivity;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/vcal/VCalListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 191
    return-void
.end method
