.class Lcom/android/calendar/vcal/i;
.super Ljava/lang/Thread;
.source "VCalInfoActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VCalInfoActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V
    .locals 1

    .prologue
    .line 768
    iput-object p1, p0, Lcom/android/calendar/vcal/i;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    .line 769
    const-class v0, Lcom/android/calendar/vcal/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 770
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 778
    const-string v0, ""

    .line 779
    iget-object v1, p0, Lcom/android/calendar/vcal/i;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 782
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/vcal/i;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v2}, Lcom/android/calendar/vcal/VCalInfoActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 783
    if-eqz v2, :cond_0

    .line 784
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 788
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 789
    iget-object v0, p0, Lcom/android/calendar/vcal/i;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    const v1, 0x7f0f005e

    invoke-virtual {v0, v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 791
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/vcal/i;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v1, v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 792
    return-void

    .line 786
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 0

    .prologue
    .line 774
    invoke-direct {p0}, Lcom/android/calendar/vcal/i;->a()V

    .line 775
    return-void
.end method
