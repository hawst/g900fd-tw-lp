.class Lcom/android/calendar/vcal/ag;
.super Ljava/lang/Object;
.source "VTaskListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VTaskListActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/vcal/VTaskListActivity;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/android/calendar/vcal/ag;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 113
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 114
    const-string v2, "VCAL_DATA"

    iget-object v0, p0, Lcom/android/calendar/vcal/ag;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskListActivity;->a(Lcom/android/calendar/vcal/VTaskListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 116
    iget-object v0, p0, Lcom/android/calendar/vcal/ag;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    const-class v2, Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    const/high16 v0, 0x20020000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 119
    iget-object v0, p0, Lcom/android/calendar/vcal/ag;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/vcal/VTaskListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 120
    return-void
.end method
