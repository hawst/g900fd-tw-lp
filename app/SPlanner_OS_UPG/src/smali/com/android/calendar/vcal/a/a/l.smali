.class public Lcom/android/calendar/vcal/a/a/l;
.super Lcom/android/calendar/vcal/a/a/k;
.source "VCalParser_V20.java"


# static fields
.field private static final d:Ljava/util/HashSet;

.field private static final e:Ljava/util/HashSet;

.field private static final f:Ljava/util/HashSet;


# instance fields
.field private g:Z

.field private h:[Ljava/lang/String;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 30
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "VEVENT"

    aput-object v2, v1, v4

    const-string v2, "VTODO"

    aput-object v2, v1, v5

    const-string v2, "VALARM"

    aput-object v2, v1, v6

    const-string v2, "VTIMEZONE"

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/l;->d:Ljava/util/HashSet;

    .line 33
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "DESCRIPTION"

    aput-object v2, v1, v4

    const-string v2, "DTEND"

    aput-object v2, v1, v5

    const-string v2, "DTSTART"

    aput-object v2, v1, v6

    const-string v2, "DUE"

    aput-object v2, v1, v3

    const-string v2, "COMPLETED"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "RRULE"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "STATUS"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "SUMMARY"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "LOCATION"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/l;->e:Ljava/util/HashSet;

    .line 37
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "ENCODING"

    aput-object v2, v1, v4

    const-string v2, "CHARSET"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/l;->f:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/calendar/vcal/a/a/k;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/vcal/a/a/l;->g:Z

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "END:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 129
    sget-object v0, Lcom/android/calendar/vcal/a/a/l;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 130
    const-string v0, "VEVENT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "VTODO"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 131
    :cond_0
    const-string v0, "BEGIN:"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\r\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 133
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v0, v0, v4

    const-string v4, ":"

    invoke-virtual {v0, v4, v12}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 134
    aget-object v0, v4, v1

    const-string v5, ";"

    invoke-virtual {v0, v5, v12}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 135
    aget-object v5, v0, v1

    .line 136
    array-length v6, v0

    if-le v6, v2, :cond_2

    aget-object v0, v0, v2

    .line 137
    :goto_1
    aget-object v4, v4, v2

    .line 140
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_4

    .line 141
    const-string v6, ";"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 142
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 143
    array-length v8, v6

    move v0, v1

    :goto_2
    if-ge v0, v8, :cond_3

    aget-object v9, v6, v0

    .line 144
    sget-object v10, Lcom/android/calendar/vcal/a/a/l;->f:Ljava/util/HashSet;

    const-string v11, "="

    invoke-virtual {v9, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v9, v1, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 145
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ";"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 136
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 148
    :cond_3
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    :cond_4
    iget-object v6, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v7, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v6, v6, v7

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v7, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v6, v6, v7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_7

    .line 152
    :cond_5
    const-string v0, "\r\n"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_6
    :goto_3
    iget v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    goto/16 :goto_0

    .line 153
    :cond_7
    sget-object v6, Lcom/android/calendar/vcal/a/a/l;->e:Ljava/util/HashSet;

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 154
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 155
    :cond_8
    const-string v0, "BEGIN"

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 157
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "VALARM"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 159
    const-string v0, "AALARM:default"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\r\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    :goto_4
    const-string v0, "END:VALARM"

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v5, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 161
    iget v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    goto :goto_4

    :cond_9
    move v0, v1

    .line 192
    :goto_5
    return v0

    .line 167
    :cond_a
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    :goto_6
    move v0, v2

    .line 192
    goto :goto_5

    .line 168
    :cond_c
    const-string v0, "VALARM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    .line 170
    goto :goto_5

    .line 171
    :cond_d
    const-string v0, "VTIMEZONE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 173
    :cond_e
    iget-boolean v0, p0, Lcom/android/calendar/vcal/a/a/l;->g:Z

    if-nez v0, :cond_f

    .line 174
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v0, v0, v4

    const-string v4, ":"

    invoke-virtual {v0, v4, v12}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 175
    aget-object v4, v0, v1

    const-string v5, ";"

    invoke-virtual {v4, v5, v12}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    .line 177
    const-string v5, "TZOFFSETFROM"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 178
    aget-object v0, v0, v2

    .line 179
    const-string v4, "TZ"

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\r\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    iput-boolean v2, p0, Lcom/android/calendar/vcal/a/a/l;->g:Z

    .line 183
    :cond_f
    iget v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    .line 184
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    goto :goto_6

    :cond_10
    move v0, v1

    .line 186
    goto :goto_5

    .line 188
    :cond_11
    :goto_7
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v1, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v0, v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 189
    iget v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    goto :goto_7
.end method

.method private a([Ljava/lang/String;Ljava/lang/StringBuilder;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 90
    :goto_0
    :try_start_0
    const-string v2, "VERSION:2.0"

    iget v3, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 91
    iget v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    goto :goto_0

    .line 108
    :catch_0
    move-exception v1

    .line 112
    :cond_0
    :goto_1
    return v0

    .line 92
    :cond_1
    const-string v2, "VERSION:1.0"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    iget v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    .line 95
    :goto_2
    iget v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_3

    .line 96
    iget v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v2, p1, v2

    const-string v3, ":"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 97
    const/4 v3, 0x0

    aget-object v3, v2, v3

    .line 98
    const/4 v4, 0x1

    aget-object v2, v2, v4

    .line 100
    const-string v4, "BEGIN"

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 101
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 103
    iget v3, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    .line 104
    invoke-direct {p0, v2, p2}, Lcom/android/calendar/vcal/a/a/l;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    :cond_2
    iget v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :cond_3
    move v0, v1

    .line 112
    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 200
    const-string v0, "\r\n"

    const-string v1, "\n"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n\t"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 202
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 203
    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;Ljava/lang/String;Lcom/android/calendar/vcal/a/b;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v1

    new-array v1, v1, [B

    .line 51
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 52
    if-gtz v2, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v0

    .line 55
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    .line 56
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 58
    invoke-static {v2}, Lcom/android/calendar/vcal/a/a/l;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    .line 59
    iput v0, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    .line 61
    const-string v2, "BEGIN:VCALENDAR"

    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    const-string v2, "BEGIN:VCALENDAR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    iget v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    .line 66
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lcom/android/calendar/vcal/a/a/l;->a([Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-gt v2, v3, :cond_0

    .line 70
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    if-ne v2, v3, :cond_0

    const-string v2, "END:VCALENDAR"

    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/l;->h:[Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/vcal/a/a/l;->i:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    const-string v0, "END:VCALENDAR"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\r\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-super {p0, v0, p2, p3}, Lcom/android/calendar/vcal/a/a/k;->a(Ljava/io/InputStream;Ljava/lang/String;Lcom/android/calendar/vcal/a/b;)Z

    move-result v0

    goto :goto_0
.end method
