.class Lcom/android/calendar/vcal/r;
.super Landroid/widget/BaseAdapter;
.source "VCalListActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VCalListActivity;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/calendar/vcal/VCalListActivity;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 468
    iput-object p1, p0, Lcom/android/calendar/vcal/r;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 469
    iput-object p2, p0, Lcom/android/calendar/vcal/r;->b:Landroid/content/Context;

    .line 470
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/r;->c:Ljava/lang/String;

    .line 471
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/android/calendar/vcal/r;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalListActivity;->h(Lcom/android/calendar/vcal/VCalListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 478
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 482
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 487
    .line 490
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 493
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 494
    instance-of v1, v0, Lcom/android/calendar/vcal/t;

    if-eqz v1, :cond_8

    .line 496
    check-cast v0, Lcom/android/calendar/vcal/t;

    move-object v1, p2

    .line 500
    :goto_0
    if-nez v0, :cond_7

    .line 503
    new-instance v1, Lcom/android/calendar/vcal/t;

    invoke-direct {v1, v3}, Lcom/android/calendar/vcal/t;-><init>(Lcom/android/calendar/vcal/l;)V

    .line 504
    iget-object v0, p0, Lcom/android/calendar/vcal/r;->b:Landroid/content/Context;

    const v2, 0x7f0400bc

    invoke-static {v0, v2, p3, v7}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 505
    const v0, 0x7f12002d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/vcal/t;->a:Landroid/widget/TextView;

    .line 506
    const v0, 0x7f120064

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/vcal/t;->b:Landroid/widget/TextView;

    .line 507
    const v0, 0x7f12030c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/vcal/t;->c:Landroid/view/View;

    .line 508
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    move-object v8, v2

    .line 511
    :goto_1
    iget-object v2, v0, Lcom/android/calendar/vcal/t;->a:Landroid/widget/TextView;

    .line 512
    iget-object v9, v0, Lcom/android/calendar/vcal/t;->b:Landroid/widget/TextView;

    .line 513
    iget-object v1, v0, Lcom/android/calendar/vcal/t;->c:Landroid/view/View;

    .line 515
    iget-object v0, p0, Lcom/android/calendar/vcal/r;->a:Lcom/android/calendar/vcal/VCalListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalListActivity;->h(Lcom/android/calendar/vcal/VCalListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/a/w;

    .line 517
    const v4, -0xfb4a01

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 519
    const-string v1, "SUMMARY"

    invoke-virtual {v0, v1}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v1

    .line 521
    if-eqz v1, :cond_6

    .line 522
    invoke-static {v1}, Lcom/android/calendar/vcal/x;->a(Lcom/android/a/aa;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\\\n"

    const-string v4, "\\\n"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\\\,"

    const-string v4, ","

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\\\;"

    const-string v4, ";"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 525
    :goto_2
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 526
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/vcal/r;->c:Ljava/lang/String;

    .line 528
    :cond_1
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 530
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 531
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 532
    iget-object v1, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 533
    const-string v3, "UTC"

    iput-object v3, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 534
    const-string v3, "UTC"

    iput-object v3, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 537
    const-string v3, "DTSTART"

    invoke-virtual {v0, v3}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v3

    .line 538
    if-eqz v3, :cond_4

    .line 539
    invoke-virtual {v3}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v5

    .line 540
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 541
    const-string v10, "TZID"

    invoke-virtual {v3, v10}, Lcom/android/a/aa;->c(Ljava/lang/String;)Lcom/android/a/y;

    move-result-object v3

    .line 542
    if-eqz v3, :cond_2

    iget-object v10, v3, Lcom/android/a/y;->b:Ljava/lang/String;

    if-eqz v10, :cond_2

    .line 543
    iget-object v3, v3, Lcom/android/a/y;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 546
    :cond_2
    :try_start_0
    invoke-virtual {v2, v5}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 547
    iget-boolean v3, v2, Landroid/text/format/Time;->allDay:Z

    if-eqz v3, :cond_3

    .line 548
    iput-object v1, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 549
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 555
    :cond_3
    :goto_3
    const-string v3, "DTEND"

    invoke-virtual {v0, v3}, Lcom/android/a/w;->b(Ljava/lang/String;)Lcom/android/a/aa;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_4

    .line 557
    invoke-virtual {v0}, Lcom/android/a/aa;->b()Ljava/lang/String;

    move-result-object v0

    .line 558
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 560
    :try_start_1
    invoke-virtual {v4, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 561
    iget-boolean v0, v4, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_4

    .line 562
    iput-object v1, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 563
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 571
    :cond_4
    :goto_4
    iget-boolean v1, v2, Landroid/text/format/Time;->allDay:Z

    .line 572
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 573
    if-eqz v1, :cond_5

    .line 574
    iget v0, v4, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_5

    iget v0, v4, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_5

    .line 575
    iget v0, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    .line 576
    invoke-virtual {v4, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 580
    :cond_5
    invoke-virtual {v4, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 581
    iget-object v0, p0, Lcom/android/calendar/vcal/r;->b:Landroid/content/Context;

    invoke-static/range {v0 .. v7}, Lcom/android/calendar/vcal/x;->a(Landroid/content/Context;ZJJZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 582
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 583
    return-object v8

    .line 565
    :catch_0
    move-exception v0

    goto :goto_4

    .line 551
    :catch_1
    move-exception v3

    goto :goto_3

    :cond_6
    move-object v1, v3

    goto/16 :goto_2

    :cond_7
    move-object v8, v1

    goto/16 :goto_1

    :cond_8
    move-object v0, v3

    move-object v1, v3

    goto/16 :goto_0
.end method
