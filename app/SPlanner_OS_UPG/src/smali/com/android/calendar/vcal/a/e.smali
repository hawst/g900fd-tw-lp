.class public abstract Lcom/android/calendar/vcal/a/e;
.super Ljava/lang/Object;
.source "VParser.java"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Lcom/android/calendar/vcal/a/b;

.field protected c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->b:Lcom/android/calendar/vcal/a/b;

    .line 39
    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    return-void
.end method

.method protected static a(C)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 459
    const/16 v1, 0x30

    if-lt p0, v1, :cond_1

    const/16 v1, 0x39

    if-gt p0, v1, :cond_1

    .line 465
    :cond_0
    :goto_0
    return v0

    .line 461
    :cond_1
    const/16 v1, 0x61

    if-lt p0, v1, :cond_2

    const/16 v1, 0x7a

    if-le p0, v1, :cond_0

    .line 463
    :cond_2
    const/16 v1, 0x41

    if-lt p0, v1, :cond_3

    const/16 v1, 0x5a

    if-le p0, v1, :cond_0

    .line 465
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static b(C)Z
    .locals 1

    .prologue
    .line 472
    const/16 v0, 0x20

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7e

    if-gt p0, v0, :cond_0

    .line 473
    const/4 v0, 0x1

    .line 474
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static c(C)Z
    .locals 1

    .prologue
    .line 481
    const/16 v0, 0x61

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7a

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_2

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_2

    .line 482
    :cond_1
    const/4 v0, 0x1

    .line 484
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 49
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    .line 51
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 52
    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    .line 53
    add-int/lit8 v1, p1, 0x1

    .line 54
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 55
    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 56
    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected a(ILjava/lang/String;Z)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 184
    .line 185
    if-nez p2, :cond_1

    move v0, v1

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 189
    :cond_1
    if-eqz p3, :cond_2

    .line 190
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .line 192
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    add-int v3, p1, v0

    invoke-virtual {v2, p1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 196
    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    move v0, v1

    .line 199
    goto :goto_0

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 204
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 206
    goto :goto_0
.end method

.method protected a(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0xd

    const/4 v4, -0x1

    .line 100
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 101
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->read()I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 106
    if-ne v0, v5, :cond_5

    .line 107
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->read()I

    move-result v0

    .line 108
    const/16 v3, 0xa

    if-ne v0, v3, :cond_4

    .line 109
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->read()I

    move-result v0

    .line 110
    const/16 v3, 0x20

    if-eq v0, v3, :cond_1

    const/16 v3, 0x9

    if-ne v0, v3, :cond_2

    .line 111
    :cond_1
    int-to-char v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 130
    :goto_1
    return-void

    .line 114
    :cond_2
    const-string v3, "\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    if-ne v0, v4, :cond_5

    .line 126
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    goto :goto_1

    .line 119
    :cond_4
    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 122
    :cond_5
    if-eqz v0, :cond_0

    .line 124
    int-to-char v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public a(Ljava/io/InputStream;Ljava/lang/String;Lcom/android/calendar/vcal/a/b;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/vcal/a/e;->a(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 74
    iput-object p3, p0, Lcom/android/calendar/vcal/a/e;->b:Lcom/android/calendar/vcal/a/b;

    .line 77
    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v0}, Lcom/android/calendar/vcal/a/b;->a()V

    :cond_0
    move v0, v1

    move v2, v1

    .line 81
    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/calendar/vcal/a/e;->b(I)I

    move-result v3

    .line 82
    const/4 v4, -0x1

    if-ne v4, v3, :cond_3

    .line 89
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v2}, Lcom/android/calendar/vcal/a/b;->b()V

    .line 92
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1

    .line 85
    :cond_3
    add-int/2addr v2, v3

    .line 86
    add-int/2addr v0, v3

    goto :goto_0
.end method

.method protected abstract b(I)I
.end method

.method protected c(I)I
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/16 v4, 0x9

    const/4 v0, -0x1

    .line 142
    const/4 v1, 0x0

    .line 145
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 146
    if-eq v2, v5, :cond_0

    if-ne v2, v4, :cond_2

    .line 147
    :cond_0
    const/4 v1, 0x1

    .line 148
    add-int/lit8 v2, p1, 0x1

    .line 156
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 157
    if-eq v3, v5, :cond_1

    if-ne v3, v4, :cond_3

    .line 158
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 159
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 149
    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->a(I)I

    move-result v3

    if-eq v3, v0, :cond_5

    .line 150
    add-int v2, p1, v3

    .line 151
    add-int/2addr v1, v3

    goto :goto_0

    .line 160
    :cond_3
    invoke-virtual {p0, v2}, Lcom/android/calendar/vcal/a/e;->a(I)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eq v3, v0, :cond_4

    .line 161
    add-int/2addr v2, v3

    .line 162
    add-int/2addr v1, v3

    goto :goto_0

    .line 167
    :catch_0
    move-exception v2

    .line 170
    :cond_4
    if-lez v1, :cond_5

    move v0, v1

    .line 172
    :cond_5
    return v0
.end method

.method protected d(I)I
    .locals 3

    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 217
    const/4 v0, -0x1

    .line 224
    :cond_0
    return v0

    .line 218
    :cond_1
    const/4 v0, 0x0

    .line 220
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x20

    if-eq v1, v2, :cond_2

    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    .line 221
    :cond_2
    add-int/lit8 p1, p1, 0x1

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected e(I)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 231
    const/4 v1, 0x0

    .line 232
    const-string v2, "X-"

    const/4 v3, 0x1

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 233
    if-ne v0, v2, :cond_1

    .line 243
    :cond_0
    :goto_0
    return v0

    .line 235
    :cond_1
    add-int v3, p1, v2

    .line 236
    add-int/2addr v1, v2

    .line 238
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/e;->l(I)I

    move-result v2

    .line 239
    if-eq v0, v2, :cond_0

    .line 242
    add-int v0, v1, v2

    .line 243
    goto :goto_0
.end method

.method protected f(I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 250
    .line 252
    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    const-string v2, "7BIT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    const-string v2, "8BIT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "X-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 255
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->g(I)I

    move-result v0

    .line 256
    if-eq v0, v1, :cond_2

    .line 277
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 259
    goto :goto_0

    .line 262
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    const-string v2, "QUOTED-PRINTABLE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 263
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->h(I)I

    move-result v0

    .line 264
    if-ne v0, v1, :cond_1

    move v0, v1

    .line 267
    goto :goto_0

    .line 270
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    const-string v2, "BASE64"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 271
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->k(I)I

    move-result v0

    .line 272
    if-ne v0, v1, :cond_1

    move v0, v1

    .line 275
    goto :goto_0

    :cond_5
    move v0, v1

    .line 277
    goto :goto_0
.end method

.method protected g(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 284
    .line 286
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 288
    if-ne v1, v0, :cond_0

    .line 291
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected h(I)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 299
    .line 301
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->d(I)I

    move-result v0

    .line 302
    add-int v1, p1, v0

    .line 303
    add-int/2addr v0, v4

    .line 306
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/e;->i(I)I

    move-result v2

    .line 307
    if-ne v3, v2, :cond_1

    .line 317
    const-string v2, "="

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v1

    .line 318
    if-eq v1, v3, :cond_0

    .line 320
    add-int/2addr v0, v1

    .line 323
    :cond_0
    return v0

    .line 309
    :cond_1
    add-int/2addr v1, v2

    .line 310
    add-int/2addr v0, v2

    .line 312
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/e;->d(I)I

    move-result v2

    .line 313
    add-int/2addr v1, v2

    .line 314
    add-int/2addr v0, v2

    goto :goto_0
.end method

.method protected i(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 330
    .line 333
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 334
    const/16 v2, 0xd

    if-ne v1, v2, :cond_1

    .line 335
    add-int/lit8 p1, p1, 0x1

    .line 336
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->charAt(I)C
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 337
    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    .line 352
    :cond_0
    :goto_0
    return v0

    .line 341
    :cond_1
    const/16 v2, 0x3d

    if-eq v1, v2, :cond_2

    const/16 v2, 0x20

    if-eq v1, v2, :cond_2

    const/16 v2, 0x9

    if-eq v1, v2, :cond_2

    .line 342
    const/4 v0, 0x1

    goto :goto_0

    .line 348
    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->j(I)I

    move-result v1

    .line 349
    if-eq v1, v0, :cond_0

    move v0, v1

    .line 350
    goto :goto_0

    .line 344
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected j(I)I
    .locals 8

    .prologue
    const/16 v7, 0x41

    const/16 v6, 0x39

    const/16 v5, 0x30

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 359
    .line 361
    const-string v1, "="

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v1

    .line 362
    if-ne v0, v1, :cond_1

    .line 393
    :cond_0
    :goto_0
    return v0

    .line 364
    :cond_1
    add-int v3, p1, v1

    .line 365
    add-int/2addr v2, v1

    .line 368
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 369
    const/16 v4, 0x20

    if-eq v1, v4, :cond_2

    const/16 v4, 0x9

    if-ne v1, v4, :cond_3

    .line 370
    :cond_2
    add-int/lit8 v0, v2, 0x1

    goto :goto_0

    .line 372
    :cond_3
    const/16 v4, 0xd

    if-ne v1, v4, :cond_4

    .line 373
    add-int/lit8 v3, v3, 0x1

    .line 374
    add-int/lit8 v2, v2, 0x1

    .line 375
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 376
    const/16 v4, 0xa

    if-ne v1, v4, :cond_4

    .line 377
    add-int/lit8 v0, v2, 0x1

    .line 378
    goto :goto_0

    .line 381
    :cond_4
    if-lt v1, v5, :cond_5

    if-le v1, v6, :cond_6

    :cond_5
    if-lt v1, v7, :cond_0

    const/16 v4, 0x46

    if-gt v1, v4, :cond_0

    .line 382
    :cond_6
    add-int/lit8 v1, v3, 0x1

    .line 383
    add-int/lit8 v2, v2, 0x1

    .line 384
    iget-object v3, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 385
    if-lt v1, v5, :cond_7

    if-le v1, v6, :cond_8

    :cond_7
    if-lt v1, v7, :cond_0

    const/16 v3, 0x46

    if-gt v1, v3, :cond_0

    .line 386
    :cond_8
    add-int/lit8 v0, v2, 0x1

    .line 387
    goto :goto_0

    .line 390
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected k(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 400
    .line 404
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 406
    const/16 v3, 0xd

    if-ne v2, v3, :cond_0

    .line 407
    const-string v2, "\r\n\r\n"

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 408
    if-eq v2, v0, :cond_0

    .line 409
    add-int v0, v1, v2

    .line 420
    add-int/lit8 v0, v0, -0x2

    .line 421
    :goto_1
    return v0

    .line 414
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 415
    add-int/lit8 p1, p1, 0x1

    .line 416
    goto :goto_0

    .line 417
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method protected l(I)I
    .locals 5

    .prologue
    const/16 v4, 0x3b

    .line 428
    const/4 v0, 0x0

    move v1, p1

    .line 431
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 432
    invoke-static {v2}, Lcom/android/calendar/vcal/a/e;->b(C)Z

    move-result v3

    if-nez v3, :cond_2

    .line 450
    :cond_0
    :goto_1
    if-nez v0, :cond_1

    .line 451
    const/4 v0, -0x1

    .line 452
    :cond_1
    return v0

    .line 434
    :cond_2
    const/16 v3, 0x20

    if-eq v2, v3, :cond_0

    const/16 v3, 0x3d

    if-eq v2, v3, :cond_0

    const/16 v3, 0x3a

    if-eq v2, v3, :cond_0

    const/16 v3, 0x2e

    if-eq v2, v3, :cond_0

    const/16 v3, 0x2c

    if-eq v2, v3, :cond_0

    if-eq v2, v4, :cond_0

    .line 437
    const/16 v3, 0x5c

    if-ne v2, v3, :cond_3

    .line 438
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 439
    if-ne v2, v4, :cond_3

    .line 440
    add-int/lit8 v1, v1, 0x1

    .line 441
    add-int/lit8 v0, v0, 0x1

    .line 444
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 445
    add-int/lit8 v0, v0, 0x1

    .line 446
    goto :goto_0

    .line 447
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method protected m(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 491
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 494
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 495
    invoke-static {v1}, Lcom/android/calendar/vcal/a/e;->a(C)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x2d

    if-ne v1, v2, :cond_1

    .line 496
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    add-int/lit8 p1, p1, 0x1

    .line 501
    goto :goto_0

    .line 502
    :catch_0
    move-exception v1

    .line 505
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected n(I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    .line 512
    const/4 v1, 0x0

    .line 514
    const-string v2, "INLINE"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 515
    if-eq v2, v0, :cond_1

    .line 516
    add-int v0, v1, v2

    .line 562
    :cond_0
    :goto_0
    return v0

    .line 520
    :cond_1
    const-string v2, "URL"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 521
    if-eq v2, v0, :cond_2

    .line 522
    add-int v0, v1, v2

    .line 523
    goto :goto_0

    .line 526
    :cond_2
    const-string v2, "CONTENT-ID"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 527
    if-eq v2, v0, :cond_3

    .line 528
    add-int v0, v1, v2

    .line 529
    goto :goto_0

    .line 532
    :cond_3
    const-string v2, "CID"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 533
    if-eq v2, v0, :cond_4

    .line 534
    add-int v0, v1, v2

    .line 535
    goto :goto_0

    .line 538
    :cond_4
    const-string v2, "INLINE"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 539
    if-eq v2, v0, :cond_5

    .line 540
    add-int v0, v1, v2

    .line 541
    goto :goto_0

    .line 544
    :cond_5
    const-string v2, "BINARY"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 545
    if-eq v2, v0, :cond_6

    .line 546
    add-int v0, v1, v2

    .line 547
    goto :goto_0

    .line 550
    :cond_6
    const-string v2, "PRESET"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 551
    if-eq v2, v0, :cond_7

    .line 552
    add-int v0, v1, v2

    .line 553
    goto :goto_0

    .line 556
    :cond_7
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->e(I)I

    move-result v2

    .line 557
    if-eq v2, v0, :cond_0

    .line 558
    add-int v0, v1, v2

    .line 559
    goto :goto_0
.end method

.method protected o(I)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v0, -0x1

    .line 569
    .line 571
    const-string v1, "7BIT"

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v1

    .line 572
    if-eq v1, v0, :cond_1

    .line 573
    const-string v0, "7BIT"

    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    .line 574
    add-int v0, v3, v1

    .line 606
    :cond_0
    :goto_0
    return v0

    .line 578
    :cond_1
    const-string v1, "8BIT"

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v1

    .line 579
    if-eq v1, v0, :cond_2

    .line 580
    const-string v0, "8BIT"

    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    .line 581
    add-int v0, v3, v1

    .line 582
    goto :goto_0

    .line 585
    :cond_2
    const-string v1, "QUOTED-PRINTABLE"

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v1

    .line 586
    if-eq v1, v0, :cond_3

    .line 587
    const-string v0, "QUOTED-PRINTABLE"

    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    .line 588
    add-int v0, v3, v1

    .line 589
    goto :goto_0

    .line 592
    :cond_3
    const-string v1, "BASE64"

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v1

    .line 593
    if-eq v1, v0, :cond_4

    .line 594
    const-string v0, "BASE64"

    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    .line 595
    add-int v0, v3, v1

    .line 596
    goto :goto_0

    .line 599
    :cond_4
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->e(I)I

    move-result v1

    .line 600
    if-eq v1, v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/a/e;->c:Ljava/lang/String;

    .line 602
    add-int v0, v3, v1

    .line 603
    goto :goto_0
.end method

.method protected p(I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    .line 613
    const/4 v1, 0x0

    .line 615
    const-string v2, "us-ascii"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 616
    if-eq v2, v0, :cond_1

    .line 617
    add-int v0, v1, v2

    .line 699
    :cond_0
    :goto_0
    return v0

    .line 621
    :cond_1
    const-string v2, "iso-8859-1"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 622
    if-eq v2, v0, :cond_2

    .line 623
    add-int v0, v1, v2

    .line 624
    goto :goto_0

    .line 627
    :cond_2
    const-string v2, "iso-8859-2"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 628
    if-eq v2, v0, :cond_3

    .line 629
    add-int v0, v1, v2

    .line 630
    goto :goto_0

    .line 633
    :cond_3
    const-string v2, "iso-8859-3"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 634
    if-eq v2, v0, :cond_4

    .line 635
    add-int v0, v1, v2

    .line 636
    goto :goto_0

    .line 639
    :cond_4
    const-string v2, "iso-8859-4"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 640
    if-eq v2, v0, :cond_5

    .line 641
    add-int v0, v1, v2

    .line 642
    goto :goto_0

    .line 645
    :cond_5
    const-string v2, "iso-8859-5"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 646
    if-eq v2, v0, :cond_6

    .line 647
    add-int v0, v1, v2

    .line 648
    goto :goto_0

    .line 651
    :cond_6
    const-string v2, "iso-8859-6"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 652
    if-eq v2, v0, :cond_7

    .line 653
    add-int v0, v1, v2

    .line 654
    goto :goto_0

    .line 657
    :cond_7
    const-string v2, "iso-8859-7"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 658
    if-eq v2, v0, :cond_8

    .line 659
    add-int v0, v1, v2

    .line 660
    goto :goto_0

    .line 663
    :cond_8
    const-string v2, "iso-8859-8"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 664
    if-eq v2, v0, :cond_9

    .line 665
    add-int v0, v1, v2

    .line 666
    goto :goto_0

    .line 669
    :cond_9
    const-string v2, "iso-8859-9"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 670
    if-eq v2, v0, :cond_a

    .line 671
    add-int v0, v1, v2

    .line 672
    goto :goto_0

    .line 675
    :cond_a
    const-string v2, "euc-kr"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 676
    if-eq v2, v0, :cond_b

    .line 677
    add-int v0, v1, v2

    .line 678
    goto :goto_0

    .line 681
    :cond_b
    const-string v2, "shift_jis"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 682
    if-eq v2, v0, :cond_c

    .line 683
    add-int v0, v1, v2

    .line 684
    goto :goto_0

    .line 687
    :cond_c
    const-string v2, "utf-8"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 688
    if-eq v2, v0, :cond_d

    .line 689
    add-int v0, v1, v2

    .line 690
    goto/16 :goto_0

    .line 693
    :cond_d
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->e(I)I

    move-result v2

    .line 694
    if-eq v2, v0, :cond_0

    .line 695
    add-int v0, v1, v2

    .line 696
    goto/16 :goto_0
.end method

.method protected q(I)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 706
    .line 708
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/e;->r(I)I

    move-result v0

    .line 709
    if-ne v2, v0, :cond_1

    move v0, v2

    .line 730
    :cond_0
    return v0

    .line 712
    :cond_1
    add-int v1, p1, v0

    .line 713
    add-int/2addr v0, v4

    .line 716
    :goto_0
    const-string v3, "-"

    invoke-virtual {p0, v1, v3, v4}, Lcom/android/calendar/vcal/a/e;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 717
    if-eq v2, v3, :cond_0

    .line 720
    add-int/2addr v1, v3

    .line 721
    add-int/2addr v0, v3

    .line 723
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/e;->r(I)I

    move-result v3

    .line 724
    if-eq v2, v3, :cond_0

    .line 727
    add-int/2addr v1, v3

    .line 728
    add-int/2addr v0, v3

    goto :goto_0
.end method

.method protected r(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 737
    move v1, v0

    .line 740
    :goto_0
    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    .line 741
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/vcal/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 742
    invoke-static {v2}, Lcom/android/calendar/vcal/a/e;->c(C)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_2

    .line 751
    :cond_0
    :goto_1
    if-nez v1, :cond_1

    .line 752
    const/4 v0, -0x1

    .line 754
    :cond_1
    return v0

    .line 745
    :cond_2
    add-int/lit8 v2, v0, 0x1

    .line 746
    add-int/lit8 p1, p1, 0x1

    .line 740
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 748
    :catch_0
    move-exception v2

    goto :goto_1
.end method
