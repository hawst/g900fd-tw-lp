.class Lcom/android/calendar/vcal/aj;
.super Landroid/widget/BaseAdapter;
.source "VTaskListActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VTaskListActivity;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/calendar/vcal/VTaskListActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/android/calendar/vcal/aj;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 219
    iput-object p2, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    .line 220
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 262
    const v0, 0x7f0400c0

    const/4 v1, 0x0

    invoke-static {p1, v0, p2, v1}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 263
    new-instance v2, Lcom/android/calendar/vcal/ak;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lcom/android/calendar/vcal/ak;-><init>(Lcom/android/calendar/vcal/ag;)V

    .line 264
    const v0, 0x7f120319

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lcom/android/calendar/vcal/ak;->a:Landroid/view/View;

    .line 265
    const v0, 0x7f120165

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/android/calendar/vcal/ak;->b:Landroid/widget/TextView;

    .line 266
    const v0, 0x7f120155

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/android/calendar/vcal/ak;->c:Landroid/widget/TextView;

    .line 267
    const v0, 0x7f12031b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/android/calendar/vcal/ak;->d:Landroid/widget/ImageView;

    .line 268
    const v0, 0x7f12031c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/android/calendar/vcal/ak;->e:Landroid/widget/ImageView;

    .line 269
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 270
    return-object v1
.end method

.method private a(Landroid/view/View;Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 274
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/ak;

    .line 277
    iget-object v1, v0, Lcom/android/calendar/vcal/ak;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 280
    invoke-direct {p0, v0, p3}, Lcom/android/calendar/vcal/aj;->a(Lcom/android/calendar/vcal/ak;I)V

    .line 283
    invoke-direct {p0, v0, p3}, Lcom/android/calendar/vcal/aj;->b(Lcom/android/calendar/vcal/ak;I)V

    .line 286
    invoke-direct {p0, v0, p3}, Lcom/android/calendar/vcal/aj;->c(Lcom/android/calendar/vcal/ak;I)V

    .line 289
    invoke-direct {p0, v0, p3}, Lcom/android/calendar/vcal/aj;->d(Lcom/android/calendar/vcal/ak;I)V

    .line 290
    return-void
.end method

.method private a(Lcom/android/calendar/vcal/ak;I)V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskListActivity;->c(Lcom/android/calendar/vcal/VTaskListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/calendar/vcal/u;->f(I)Ljava/lang/String;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 297
    :cond_1
    iget-object v1, p1, Lcom/android/calendar/vcal/ak;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    return-void
.end method

.method private b(Lcom/android/calendar/vcal/ak;I)V
    .locals 4

    .prologue
    .line 301
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskListActivity;->c(Lcom/android/calendar/vcal/VTaskListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/calendar/vcal/u;->i(I)Ljava/lang/Long;

    move-result-object v0

    .line 302
    if-nez v0, :cond_0

    .line 303
    iget-object v0, p1, Lcom/android/calendar/vcal/ak;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f02d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    :goto_0
    return-void

    .line 305
    :cond_0
    iget-object v1, p1, Lcom/android/calendar/vcal/ak;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    invoke-static {v2, v3, v0}, Lcom/android/calendar/vcal/x;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private c(Lcom/android/calendar/vcal/ak;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 310
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskListActivity;->c(Lcom/android/calendar/vcal/VTaskListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/calendar/vcal/u;->g(I)I

    move-result v0

    .line 312
    packed-switch v0, :pswitch_data_0

    .line 327
    :goto_0
    return-void

    .line 314
    :pswitch_0
    iget-object v0, p1, Lcom/android/calendar/vcal/ak;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 315
    iget-object v0, p1, Lcom/android/calendar/vcal/ak;->e:Landroid/widget/ImageView;

    const v1, 0x7f02017a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 318
    :pswitch_1
    iget-object v0, p1, Lcom/android/calendar/vcal/ak;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 321
    :pswitch_2
    iget-object v0, p1, Lcom/android/calendar/vcal/ak;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 322
    iget-object v0, p1, Lcom/android/calendar/vcal/ak;->e:Landroid/widget/ImageView;

    const v1, 0x7f020179

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private d(Lcom/android/calendar/vcal/ak;I)V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskListActivity;->c(Lcom/android/calendar/vcal/VTaskListActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/calendar/vcal/u;->k(I)Z

    move-result v0

    .line 331
    iget-object v1, p1, Lcom/android/calendar/vcal/ak;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 332
    iget-object v0, p1, Lcom/android/calendar/vcal/ak;->d:Landroid/widget/ImageView;

    const v1, 0x7f0200b6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 333
    return-void

    .line 331
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskListActivity;->a(Lcom/android/calendar/vcal/VTaskListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->a:Lcom/android/calendar/vcal/VTaskListActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VTaskListActivity;->a(Lcom/android/calendar/vcal/VTaskListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 234
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const v3, 0x7f120319

    .line 240
    if-nez p2, :cond_0

    .line 241
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    invoke-direct {p0, v0, p3}, Lcom/android/calendar/vcal/aj;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0374

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 247
    iget-object v1, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0373

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 249
    if-nez p1, :cond_1

    .line 250
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    float-to-int v1, v1

    float-to-int v0, v0

    invoke-direct {v3, v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 257
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/vcal/aj;->b:Landroid/content/Context;

    invoke-direct {p0, p2, v0, p1}, Lcom/android/calendar/vcal/aj;->a(Landroid/view/View;Landroid/content/Context;I)V

    .line 258
    return-object p2

    .line 253
    :cond_1
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    float-to-int v1, v1

    const/4 v3, -0x1

    invoke-direct {v2, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
