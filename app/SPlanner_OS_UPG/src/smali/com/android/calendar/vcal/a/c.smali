.class public Lcom/android/calendar/vcal/a/c;
.super Ljava/lang/Object;
.source "VDataBuilder.java"

# interfaces
.implements Lcom/android/calendar/vcal/a/b;


# static fields
.field private static b:Ljava/lang/String;


# instance fields
.field public a:Ljava/util/List;

.field private c:I

.field private d:Lcom/android/calendar/vcal/a/d;

.field private e:Lcom/android/calendar/vcal/a/a;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "VDATABuilder"

    sput-object v0, Lcom/android/calendar/vcal/a/c;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/c;->a:Ljava/util/List;

    .line 44
    iput v1, p0, Lcom/android/calendar/vcal/a/c;->c:I

    .line 57
    const-string v0, "ISO-8859-1"

    iput-object v0, p0, Lcom/android/calendar/vcal/a/c;->g:Ljava/lang/String;

    .line 58
    iput-boolean v1, p0, Lcom/android/calendar/vcal/a/c;->h:Z

    .line 59
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    .line 127
    invoke-virtual {v0, p1}, Ljava/nio/charset/Charset;->encode(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v1, v1, [B

    .line 131
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 133
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-object v0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/Collection;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x3b

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 252
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 253
    if-lez v0, :cond_1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_1

    .line 254
    const/4 v2, 0x0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 256
    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/android/calendar/vcal/a/d;

    invoke-direct {v0}, Lcom/android/calendar/vcal/a/d;-><init>()V

    .line 74
    const/4 v1, 0x1

    iput v1, v0, Lcom/android/calendar/vcal/a/d;->c:I

    .line 75
    iput-object p1, v0, Lcom/android/calendar/vcal/a/d;->a:Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/android/calendar/vcal/a/c;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/vcal/a/c;->c:I

    .line 78
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->a:Ljava/util/List;

    iget v1, p0, Lcom/android/calendar/vcal/a/c;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/d;

    iput-object v0, p0, Lcom/android/calendar/vcal/a/c;->d:Lcom/android/calendar/vcal/a/d;

    .line 79
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 140
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->e:Landroid/content/ContentValues;

    .line 142
    const-string v1, "CHARSET"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 146
    const-string v1, "ENCODING"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 147
    const-string v1, "ENCODING"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 148
    if-eqz v1, :cond_1

    const-string v0, "BASE64"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "B"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    :cond_0
    iget-object v3, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, " "

    const-string v6, ""

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "\t"

    const-string v6, ""

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "\r\n"

    const-string v6, ""

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/mms/pdu/Base64;->decodeBase64([B)[B

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/vcal/a/a;->d:[B

    .line 157
    :cond_1
    if-eqz v1, :cond_5

    const-string v0, "QUOTED-PRINTABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 159
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 160
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 163
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 164
    const-string v3, "= "

    const-string v4, " "

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "=\t"

    const-string v4, "\t"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 167
    iget-boolean v3, p0, Lcom/android/calendar/vcal/a/c;->h:Z

    if-eqz v3, :cond_3

    .line 168
    const-string v3, "\r\n"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 173
    :goto_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 174
    array-length v10, v4

    move v3, v2

    :goto_2
    if-ge v3, v10, :cond_4

    aget-object v0, v4, v3

    .line 175
    const-string v11, "="

    invoke-virtual {v0, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 176
    const/4 v11, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v0, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 178
    :cond_2
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 170
    :cond_3
    const-string v3, "\r\n"

    const-string v4, "\n"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "\r"

    const-string v4, "\n"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 180
    :cond_4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/mms/pdu/QuotedPrintable;->decodeQuotedPrintable([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 182
    if-eqz v0, :cond_10

    .line 183
    if-eqz v5, :cond_6

    .line 185
    :try_start_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 200
    :goto_3
    :try_start_2
    invoke-virtual {v7, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 201
    array-length v0, v0

    add-int/2addr v0, v1

    :goto_4
    move v1, v0

    .line 203
    goto/16 :goto_0

    .line 186
    :catch_0
    move-exception v3

    .line 187
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 220
    :catch_1
    move-exception v0

    .line 221
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_5
    move v0, v2

    .line 226
    :goto_5
    if-nez v0, :cond_f

    .line 228
    if-eqz v5, :cond_e

    .line 229
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 230
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 231
    invoke-direct {p0, v0, v5}, Lcom/android/calendar/vcal/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 232
    if-eqz v3, :cond_c

    .line 233
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 190
    :cond_6
    :try_start_3
    invoke-static {}, Lcom/android/calendar/dz;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "KDDI"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-static {}, Lcom/android/calendar/dz;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DCM"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v3

    if-eqz v3, :cond_8

    .line 192
    :cond_7
    :try_start_4
    new-instance v3, Ljava/lang/String;

    const-string v4, "Shift-JIS"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    .line 193
    :catch_2
    move-exception v3

    .line 194
    :try_start_5
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 197
    :cond_8
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 204
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    iput-object v6, v0, Lcom/android/calendar/vcal/a/a;->c:Ljava/util/List;

    .line 205
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    invoke-static {v6}, Lcom/android/calendar/vcal/a/c;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    .line 207
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    new-array v1, v1, [B

    iput-object v1, v0, Lcom/android/calendar/vcal/a/a;->d:[B

    .line 210
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->d:[B

    .line 212
    invoke-virtual {v7}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 213
    array-length v7, v0

    move v3, v1

    move v1, v2

    .line 214
    :goto_8
    if-ge v1, v7, :cond_a

    .line 215
    aget-byte v8, v0, v1

    aput-byte v8, v4, v3
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 214
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_a
    move v1, v3

    .line 217
    goto :goto_7

    .line 219
    :cond_b
    const/4 v0, 0x1

    goto/16 :goto_5

    .line 235
    :cond_c
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :cond_d
    move-object p1, v1

    .line 241
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    iput-object p1, v0, Lcom/android/calendar/vcal/a/a;->c:Ljava/util/List;

    .line 242
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    invoke-static {p1}, Lcom/android/calendar/vcal/a/c;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    .line 244
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->d:Lcom/android/calendar/vcal/a/d;

    iget-object v0, v0, Lcom/android/calendar/vcal/a/d;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    return-void

    :cond_10
    move v0, v1

    goto/16 :goto_4
.end method

.method public b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/android/calendar/vcal/a/a;

    invoke-direct {v0}, Lcom/android/calendar/vcal/a/a;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    .line 102
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    iput-object p1, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->a:Ljava/util/List;

    iget v1, p0, Lcom/android/calendar/vcal/a/c;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/d;

    .line 83
    const/4 v1, 0x0

    iput v1, v0, Lcom/android/calendar/vcal/a/d;->c:I

    .line 84
    :cond_0
    iget v0, p0, Lcom/android/calendar/vcal/a/c;->c:I

    if-lez v0, :cond_1

    .line 85
    iget v0, p0, Lcom/android/calendar/vcal/a/c;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/vcal/a/c;->c:I

    .line 86
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->a:Ljava/util/List;

    iget v1, p0, Lcom/android/calendar/vcal/a/c;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/d;

    iget v0, v0, Lcom/android/calendar/vcal/a/d;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->a:Ljava/util/List;

    iget v1, p0, Lcom/android/calendar/vcal/a/c;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/d;

    iput-object v0, p0, Lcom/android/calendar/vcal/a/c;->d:Lcom/android/calendar/vcal/a/d;

    .line 90
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/android/calendar/vcal/a/c;->f:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->f:Ljava/lang/String;

    const-string v1, "TYPE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 122
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/vcal/a/c;->f:Ljava/lang/String;

    .line 123
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/vcal/a/c;->e:Lcom/android/calendar/vcal/a/a;

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->e:Landroid/content/ContentValues;

    iget-object v1, p0, Lcom/android/calendar/vcal/a/c;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method
