.class public Lcom/android/calendar/vcal/a/a/j;
.super Ljava/lang/Object;
.source "VCalParser.java"


# instance fields
.field private a:Lcom/android/calendar/vcal/a/e;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/j;->a:Lcom/android/calendar/vcal/a/e;

    .line 31
    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/j;->b:Ljava/lang/String;

    .line 34
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/j;->b(Ljava/lang/String;)V

    .line 62
    return-object p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/j;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 72
    const-string v0, "\nVERSION:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 74
    const-string v1, "vcalendar1.0"

    iput-object v1, p0, Lcom/android/calendar/vcal/a/a/j;->b:Ljava/lang/String;

    .line 76
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 77
    const/16 v1, 0xa

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 79
    const-string v1, "2.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 80
    const-string v0, "vcalendar2.0"

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/j;->b:Ljava/lang/String;

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/j;->b:Ljava/lang/String;

    const-string v1, "vcalendar1.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    new-instance v0, Lcom/android/calendar/vcal/a/a/k;

    invoke-direct {v0}, Lcom/android/calendar/vcal/a/a/k;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/j;->a:Lcom/android/calendar/vcal/a/e;

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/j;->b:Ljava/lang/String;

    const-string v1, "vcalendar2.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    new-instance v0, Lcom/android/calendar/vcal/a/a/l;

    invoke-direct {v0}, Lcom/android/calendar/vcal/a/a/l;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/j;->a:Lcom/android/calendar/vcal/a/e;

    .line 89
    :cond_2
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/android/calendar/vcal/a/c;Z)Z
    .locals 4

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/j;->a:Lcom/android/calendar/vcal/a/e;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string v3, "UTF-8"

    invoke-virtual {v1, v2, v3, p2}, Lcom/android/calendar/vcal/a/e;->a(Ljava/io/InputStream;Ljava/lang/String;Lcom/android/calendar/vcal/a/b;)Z

    move-result v1

    .line 44
    if-nez v1, :cond_1

    .line 45
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/j;->b:Ljava/lang/String;

    const-string v2, "vcalendar1.0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const-string v1, "vcalendar2.0"

    iput-object v1, p0, Lcom/android/calendar/vcal/a/a/j;->b:Ljava/lang/String;

    .line 47
    invoke-virtual {p0, v0, p2, p3}, Lcom/android/calendar/vcal/a/a/j;->a(Ljava/lang/String;Lcom/android/calendar/vcal/a/c;Z)Z

    move-result v0

    .line 54
    :goto_0
    return v0

    .line 49
    :cond_0
    new-instance v0, Lcom/android/calendar/vcal/a/a/i;

    const-string v1, "parse failed.(even use 2.0 parser)"

    invoke-direct {v0, v1}, Lcom/android/calendar/vcal/a/a/i;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    new-instance v1, Lcom/android/calendar/vcal/a/a/i;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/android/calendar/vcal/a/a/i;-><init>(Ljava/lang/String;)V

    throw v1

    .line 54
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
