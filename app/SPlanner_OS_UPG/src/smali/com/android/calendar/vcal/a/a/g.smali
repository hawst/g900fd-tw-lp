.class public Lcom/android/calendar/vcal/a/a/g;
.super Ljava/lang/Object;
.source "RecurrenceProcessor.java"


# instance fields
.field private a:Lcom/android/a/c;

.field private b:I

.field private c:Landroid/text/format/Time;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/g;->c:Landroid/text/format/Time;

    .line 304
    return-void
.end method

.method private static a(Landroid/text/format/Time;Lcom/android/a/c;)I
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 379
    .line 386
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v6

    .line 389
    iget v7, p1, Lcom/android/a/c;->o:I

    .line 390
    if-lez v7, :cond_7

    .line 392
    iget v0, p0, Landroid/text/format/Time;->monthDay:I

    .line 393
    :goto_0
    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 394
    add-int/lit8 v0, v0, -0x7

    goto :goto_0

    .line 396
    :cond_0
    iget v1, p0, Landroid/text/format/Time;->weekDay:I

    .line 397
    if-lt v1, v0, :cond_2

    .line 398
    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x1

    .line 407
    :goto_1
    iget-object v8, p1, Lcom/android/a/c;->m:[I

    .line 408
    iget-object v9, p1, Lcom/android/a/c;->n:[I

    move v5, v2

    move v1, v2

    .line 409
    :goto_2
    if-ge v5, v7, :cond_8

    .line 410
    aget v10, v9, v5

    .line 411
    aget v3, v8, v5

    invoke-static {v3}, Lcom/android/a/c;->c(I)I

    move-result v3

    sub-int/2addr v3, v0

    add-int/lit8 v3, v3, 0x1

    .line 412
    if-gtz v3, :cond_1

    .line 413
    add-int/lit8 v3, v3, 0x7

    .line 415
    :cond_1
    if-nez v10, :cond_3

    .line 417
    :goto_3
    if-gt v3, v6, :cond_4

    .line 420
    shl-int v10, v4, v3

    or-int/2addr v1, v10

    .line 417
    add-int/lit8 v3, v3, 0x7

    goto :goto_3

    .line 400
    :cond_2
    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x8

    goto :goto_1

    .line 423
    :cond_3
    if-lez v10, :cond_5

    .line 426
    add-int/lit8 v10, v10, -0x1

    mul-int/lit8 v10, v10, 0x7

    add-int/2addr v3, v10

    .line 427
    if-gt v3, v6, :cond_4

    .line 431
    shl-int v3, v4, v3

    or-int/2addr v1, v3

    .line 409
    :cond_4
    :goto_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 437
    :cond_5
    :goto_5
    if-gt v3, v6, :cond_6

    add-int/lit8 v3, v3, 0x7

    goto :goto_5

    .line 443
    :cond_6
    mul-int/lit8 v10, v10, 0x7

    add-int/2addr v3, v10

    .line 444
    if-lt v3, v4, :cond_4

    .line 447
    shl-int v3, v4, v3

    or-int/2addr v1, v3

    goto :goto_4

    :cond_7
    move v1, v2

    .line 457
    :cond_8
    iget v0, p1, Lcom/android/a/c;->b:I

    const/4 v3, 0x5

    if-le v0, v3, :cond_f

    .line 458
    iget v5, p1, Lcom/android/a/c;->q:I

    .line 459
    if-eqz v5, :cond_f

    .line 460
    iget-object v7, p1, Lcom/android/a/c;->p:[I

    .line 461
    iget v0, p1, Lcom/android/a/c;->o:I

    if-nez v0, :cond_b

    move v0, v1

    .line 462
    :goto_6
    if-ge v2, v5, :cond_10

    .line 463
    aget v1, v7, v2

    .line 464
    if-ltz v1, :cond_a

    .line 465
    shl-int v1, v4, v1

    or-int/2addr v0, v1

    .line 462
    :cond_9
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 467
    :cond_a
    add-int/2addr v1, v6

    add-int/lit8 v1, v1, 0x1

    .line 468
    if-lt v1, v4, :cond_9

    if-gt v1, v6, :cond_9

    .line 469
    shl-int v1, v4, v1

    or-int/2addr v0, v1

    goto :goto_7

    :cond_b
    move v3, v4

    move v0, v1

    .line 476
    :goto_8
    if-gt v3, v6, :cond_10

    .line 478
    shl-int v1, v4, v3

    and-int/2addr v1, v0

    if-eqz v1, :cond_c

    move v1, v2

    .line 479
    :goto_9
    if-ge v1, v5, :cond_e

    .line 480
    aget v8, v7, v1

    if-ne v8, v3, :cond_d

    .line 476
    :cond_c
    :goto_a
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_8

    .line 479
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 484
    :cond_e
    shl-int v1, v4, v3

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    goto :goto_a

    :cond_f
    move v0, v1

    .line 491
    :cond_10
    return v0
.end method


# virtual methods
.method a(Lcom/android/a/c;)V
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/vcal/a/a/g;->d:I

    .line 309
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/vcal/a/a/g;->e:I

    .line 310
    iput-object p1, p0, Lcom/android/calendar/vcal/a/a/g;->a:Lcom/android/a/c;

    .line 311
    return-void
.end method

.method a(Landroid/text/format/Time;I)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 315
    iget v2, p1, Landroid/text/format/Time;->year:I

    .line 316
    iget v1, p1, Landroid/text/format/Time;->month:I

    .line 318
    const/4 v0, 0x0

    .line 326
    if-lt p2, v3, :cond_0

    const/16 v4, 0x1c

    if-le p2, v4, :cond_1

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/g;->c:Landroid/text/format/Time;

    .line 329
    invoke-virtual {v0, p2, v1, v2}, Landroid/text/format/Time;->set(III)V

    .line 330
    invoke-static {v0}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;)V

    .line 331
    iget v2, v0, Landroid/text/format/Time;->year:I

    .line 332
    iget v1, v0, Landroid/text/format/Time;->month:I

    .line 333
    iget p2, v0, Landroid/text/format/Time;->monthDay:I

    .line 346
    :cond_1
    iget v4, p0, Lcom/android/calendar/vcal/a/a/g;->d:I

    if-ne v2, v4, :cond_2

    iget v4, p0, Lcom/android/calendar/vcal/a/a/g;->e:I

    if-eq v1, v4, :cond_4

    .line 347
    :cond_2
    if-nez v0, :cond_3

    .line 348
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/g;->c:Landroid/text/format/Time;

    .line 349
    invoke-virtual {v0, p2, v1, v2}, Landroid/text/format/Time;->set(III)V

    .line 350
    invoke-static {v0}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;)V

    .line 358
    :cond_3
    iput v2, p0, Lcom/android/calendar/vcal/a/a/g;->d:I

    .line 359
    iput v1, p0, Lcom/android/calendar/vcal/a/a/g;->e:I

    .line 360
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/g;->a:Lcom/android/a/c;

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/a/a/g;->a(Landroid/text/format/Time;Lcom/android/a/c;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/vcal/a/a/g;->b:I

    .line 365
    :cond_4
    iget v0, p0, Lcom/android/calendar/vcal/a/a/g;->b:I

    shl-int v1, v3, p2

    and-int/2addr v0, v1

    if-eqz v0, :cond_5

    move v0, v3

    :goto_0
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
