.class public Lcom/android/calendar/vcal/a/a/f;
.super Ljava/lang/Object;
.source "RecurrenceProcessor.java"


# static fields
.field private static final f:[I

.field private static final g:[I


# instance fields
.field private a:Landroid/text/format/Time;

.field private b:Landroid/text/format/Time;

.field private c:Ljava/lang/StringBuilder;

.field private d:Landroid/text/format/Time;

.field private e:Lcom/android/calendar/vcal/a/a/g;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 1136
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/calendar/vcal/a/a/f;->f:[I

    .line 1138
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/calendar/vcal/a/a/f;->g:[I

    return-void

    .line 1136
    nop

    :array_0
    .array-data 4
        0x1f
        0x1c
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
    .end array-data

    .line 1138
    :array_1
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb4
        0xd4
        0xf3
        0x111
        0x130
        0x14e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    .line 33
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/f;->b:Landroid/text/format/Time;

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/f;->c:Ljava/lang/StringBuilder;

    .line 35
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/f;->d:Landroid/text/format/Time;

    .line 36
    new-instance v0, Lcom/android/calendar/vcal/a/a/g;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/calendar/vcal/a/a/g;-><init>(Z)V

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/f;->e:Lcom/android/calendar/vcal/a/a/g;

    .line 41
    return-void
.end method

.method static a(II)I
    .locals 2

    .prologue
    const/16 v0, 0x1c

    .line 1149
    sget-object v1, Lcom/android/calendar/vcal/a/a/f;->f:[I

    aget v1, v1, p1

    .line 1150
    if-eq v1, v0, :cond_1

    move v0, v1

    .line 1153
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/android/calendar/vcal/a/a/f;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x1d

    goto :goto_0
.end method

.method static a(III)I
    .locals 2

    .prologue
    .line 1166
    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    .line 1167
    add-int/lit8 p1, p1, 0xc

    .line 1168
    add-int/lit8 p0, p0, -0x1

    .line 1170
    :cond_0
    mul-int/lit8 v0, p1, 0xd

    add-int/lit8 v0, v0, -0xe

    div-int/lit8 v0, v0, 0x5

    add-int/2addr v0, p2

    add-int/2addr v0, p0

    div-int/lit8 v1, p0, 0x4

    add-int/2addr v0, v1

    div-int/lit8 v1, p0, 0x64

    sub-int/2addr v0, v1

    div-int/lit16 v1, p0, 0x190

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x7

    return v0
.end method

.method private static a(Lcom/android/a/c;Landroid/text/format/Time;)I
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 183
    iget v5, p0, Lcom/android/a/c;->b:I

    .line 185
    const/4 v6, 0x6

    if-lt v6, v5, :cond_0

    .line 187
    iget v6, p0, Lcom/android/a/c;->w:I

    if-lez v6, :cond_0

    .line 188
    iget-object v6, p0, Lcom/android/a/c;->v:[I

    iget v7, p0, Lcom/android/a/c;->w:I

    iget v8, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v8, v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/calendar/vcal/a/a/f;->a([III)Z

    move-result v6

    .line 190
    if-nez v6, :cond_0

    .line 271
    :goto_0
    return v0

    .line 195
    :cond_0
    const/4 v6, 0x5

    if-lt v6, v5, :cond_1

    .line 198
    iget v6, p0, Lcom/android/a/c;->u:I

    if-lez v6, :cond_1

    .line 199
    iget-object v6, p0, Lcom/android/a/c;->t:[I

    iget v7, p0, Lcom/android/a/c;->u:I

    invoke-virtual {p1}, Landroid/text/format/Time;->getWeekNumber()I

    move-result v8

    const/16 v9, 0x9

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/vcal/a/a/f;->a([IIII)Z

    move-result v6

    .line 202
    if-nez v6, :cond_1

    move v0, v1

    .line 203
    goto :goto_0

    .line 207
    :cond_1
    if-lt v3, v5, :cond_4

    .line 209
    iget v6, p0, Lcom/android/a/c;->s:I

    if-lez v6, :cond_2

    .line 210
    iget-object v6, p0, Lcom/android/a/c;->r:[I

    iget v7, p0, Lcom/android/a/c;->s:I

    iget v8, p1, Landroid/text/format/Time;->yearDay:I

    const/16 v9, 0x8

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/vcal/a/a/f;->a([IIII)Z

    move-result v6

    .line 212
    if-nez v6, :cond_2

    move v0, v2

    .line 213
    goto :goto_0

    .line 217
    :cond_2
    iget v6, p0, Lcom/android/a/c;->q:I

    if-lez v6, :cond_3

    .line 218
    iget-object v6, p0, Lcom/android/a/c;->p:[I

    iget v7, p0, Lcom/android/a/c;->q:I

    iget v8, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p1, v3}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/vcal/a/a/f;->a([IIII)Z

    move-result v6

    .line 221
    if-nez v6, :cond_3

    move v0, v3

    .line 222
    goto :goto_0

    .line 228
    :cond_3
    iget v3, p0, Lcom/android/a/c;->o:I

    if-lez v3, :cond_4

    .line 229
    iget-object v6, p0, Lcom/android/a/c;->m:[I

    .line 230
    iget v7, p0, Lcom/android/a/c;->o:I

    .line 231
    iget v3, p1, Landroid/text/format/Time;->weekDay:I

    invoke-static {v3}, Lcom/android/a/c;->b(I)I

    move-result v8

    move v3, v4

    .line 232
    :goto_1
    if-ge v3, v7, :cond_6

    .line 233
    aget v9, v6, v3

    if-ne v9, v8, :cond_5

    .line 240
    :cond_4
    if-lt v2, v5, :cond_7

    .line 242
    iget-object v3, p0, Lcom/android/a/c;->k:[I

    iget v6, p0, Lcom/android/a/c;->l:I

    iget v7, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {p1, v2}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v2

    invoke-static {v3, v6, v7, v2}, Lcom/android/calendar/vcal/a/a/f;->a([IIII)Z

    move-result v2

    .line 245
    if-nez v2, :cond_7

    .line 246
    const/4 v0, 0x6

    goto :goto_0

    .line 232
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 237
    :cond_6
    const/4 v0, 0x5

    goto :goto_0

    .line 249
    :cond_7
    if-lt v1, v5, :cond_8

    .line 251
    iget-object v2, p0, Lcom/android/a/c;->i:[I

    iget v3, p0, Lcom/android/a/c;->j:I

    iget v6, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {p1, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    invoke-static {v2, v3, v6, v1}, Lcom/android/calendar/vcal/a/a/f;->a([IIII)Z

    move-result v1

    .line 254
    if-nez v1, :cond_8

    .line 255
    const/4 v0, 0x7

    goto/16 :goto_0

    .line 258
    :cond_8
    if-lt v0, v5, :cond_9

    .line 260
    iget-object v1, p0, Lcom/android/a/c;->g:[I

    iget v2, p0, Lcom/android/a/c;->h:I

    iget v3, p1, Landroid/text/format/Time;->second:I

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    invoke-static {v1, v2, v3, v0}, Lcom/android/calendar/vcal/a/a/f;->a([IIII)Z

    move-result v0

    .line 263
    if-nez v0, :cond_9

    .line 264
    const/16 v0, 0x8

    goto/16 :goto_0

    :cond_9
    move v0, v4

    .line 271
    goto/16 :goto_0
.end method

.method static a(Landroid/text/format/Time;)V
    .locals 9

    .prologue
    const/16 v8, 0xc

    .line 1040
    iget v1, p0, Landroid/text/format/Time;->second:I

    .line 1041
    iget v4, p0, Landroid/text/format/Time;->minute:I

    .line 1042
    iget v5, p0, Landroid/text/format/Time;->hour:I

    .line 1043
    iget v6, p0, Landroid/text/format/Time;->monthDay:I

    .line 1044
    iget v2, p0, Landroid/text/format/Time;->month:I

    .line 1045
    iget v3, p0, Landroid/text/format/Time;->year:I

    .line 1047
    if-gez v1, :cond_0

    add-int/lit8 v0, v1, -0x3b

    :goto_0
    div-int/lit8 v0, v0, 0x3c

    .line 1048
    mul-int/lit8 v7, v0, 0x3c

    sub-int v7, v1, v7

    .line 1049
    add-int v1, v4, v0

    .line 1050
    if-gez v1, :cond_1

    add-int/lit8 v0, v1, -0x3b

    :goto_1
    div-int/lit8 v0, v0, 0x3c

    .line 1051
    mul-int/lit8 v4, v0, 0x3c

    sub-int v4, v1, v4

    .line 1052
    add-int v1, v5, v0

    .line 1053
    if-gez v1, :cond_2

    add-int/lit8 v0, v1, -0x17

    :goto_2
    div-int/lit8 v0, v0, 0x18

    .line 1054
    mul-int/lit8 v5, v0, 0x18

    sub-int v5, v1, v5

    .line 1055
    add-int/2addr v0, v6

    move v1, v3

    move v3, v0

    .line 1060
    :goto_3
    if-gtz v3, :cond_4

    .line 1068
    const/4 v0, 0x1

    if-le v2, v0, :cond_3

    invoke-static {v1}, Lcom/android/calendar/vcal/a/a/f;->b(I)I

    move-result v0

    .line 1069
    :goto_4
    add-int/2addr v3, v0

    .line 1070
    add-int/lit8 v1, v1, -0x1

    .line 1071
    goto :goto_3

    :cond_0
    move v0, v1

    .line 1047
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1050
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1053
    goto :goto_2

    .line 1068
    :cond_3
    add-int/lit8 v0, v1, -0x1

    invoke-static {v0}, Lcom/android/calendar/vcal/a/a/f;->b(I)I

    move-result v0

    goto :goto_4

    .line 1073
    :cond_4
    if-gez v2, :cond_7

    .line 1074
    add-int/lit8 v0, v2, 0x1

    div-int/lit8 v0, v0, 0xc

    add-int/lit8 v6, v0, -0x1

    .line 1075
    add-int v0, v1, v6

    .line 1076
    mul-int/lit8 v1, v6, 0xc

    sub-int v1, v2, v1

    move v2, v1

    move v1, v3

    .line 1086
    :goto_5
    if-nez v2, :cond_5

    .line 1087
    invoke-static {v0}, Lcom/android/calendar/vcal/a/a/f;->b(I)I

    move-result v3

    .line 1088
    if-le v1, v3, :cond_5

    .line 1089
    add-int/lit8 v0, v0, 0x1

    .line 1090
    sub-int/2addr v1, v3

    .line 1093
    :cond_5
    invoke-static {v0, v2}, Lcom/android/calendar/vcal/a/a/f;->a(II)I

    move-result v3

    .line 1094
    if-le v1, v3, :cond_8

    .line 1095
    sub-int v3, v1, v3

    .line 1096
    add-int/lit8 v1, v2, 0x1

    .line 1097
    if-lt v1, v8, :cond_6

    .line 1098
    add-int/lit8 v1, v1, -0xc

    .line 1099
    add-int/lit8 v0, v0, 0x1

    :cond_6
    move v2, v1

    move v1, v3

    .line 1102
    goto :goto_5

    .line 1077
    :cond_7
    if-lt v2, v8, :cond_9

    .line 1078
    div-int/lit8 v6, v2, 0xc

    .line 1079
    add-int v0, v1, v6

    .line 1080
    mul-int/lit8 v1, v6, 0xc

    sub-int v1, v2, v1

    move v2, v1

    move v1, v3

    goto :goto_5

    .line 1106
    :cond_8
    iput v7, p0, Landroid/text/format/Time;->second:I

    .line 1107
    iput v4, p0, Landroid/text/format/Time;->minute:I

    .line 1108
    iput v5, p0, Landroid/text/format/Time;->hour:I

    .line 1109
    iput v1, p0, Landroid/text/format/Time;->monthDay:I

    .line 1110
    iput v2, p0, Landroid/text/format/Time;->month:I

    .line 1111
    iput v0, p0, Landroid/text/format/Time;->year:I

    .line 1112
    invoke-static {v0, v2, v1}, Lcom/android/calendar/vcal/a/a/f;->a(III)I

    move-result v3

    iput v3, p0, Landroid/text/format/Time;->weekDay:I

    .line 1113
    invoke-static {v0, v2, v1}, Lcom/android/calendar/vcal/a/a/f;->b(III)I

    move-result v0

    iput v0, p0, Landroid/text/format/Time;->yearDay:I

    .line 1114
    return-void

    :cond_9
    move v0, v1

    move v1, v3

    goto :goto_5
.end method

.method private static final a(Landroid/text/format/Time;J)V
    .locals 3

    .prologue
    .line 1210
    const/16 v0, 0x1a

    shr-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Landroid/text/format/Time;->year:I

    .line 1211
    const/16 v0, 0x16

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, Landroid/text/format/Time;->month:I

    .line 1212
    const/16 v0, 0x11

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x1f

    iput v0, p0, Landroid/text/format/Time;->monthDay:I

    .line 1213
    const/16 v0, 0xc

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x1f

    iput v0, p0, Landroid/text/format/Time;->hour:I

    .line 1214
    const/4 v0, 0x6

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x3f

    iput v0, p0, Landroid/text/format/Time;->minute:I

    .line 1215
    const-wide/16 v0, 0x3f

    and-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Landroid/text/format/Time;->second:I

    .line 1216
    return-void
.end method

.method static a(I)Z
    .locals 1

    .prologue
    .line 1123
    rem-int/lit8 v0, p0, 0x4

    if-nez v0, :cond_1

    rem-int/lit8 v0, p0, 0x64

    if-nez v0, :cond_0

    rem-int/lit16 v0, p0, 0x190

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([III)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 138
    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 139
    aget v2, p0, v1

    if-ne v2, p2, :cond_1

    .line 140
    const/4 v0, 0x1

    .line 143
    :cond_0
    return v0

    .line 138
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static a([IIII)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 156
    move v3, v2

    move v0, p3

    :goto_0
    if-ge v3, p1, :cond_2

    .line 157
    aget v4, p0, v3

    .line 158
    if-lez v4, :cond_0

    .line 159
    if-ne v4, p2, :cond_1

    move v0, v1

    .line 169
    :goto_1
    return v0

    .line 163
    :cond_0
    add-int/2addr v0, v4

    .line 164
    if-ne v0, p2, :cond_1

    move v0, v1

    .line 165
    goto :goto_1

    .line 156
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 169
    goto :goto_1
.end method

.method static b(I)I
    .locals 1

    .prologue
    .line 1133
    invoke-static {p0}, Lcom/android/calendar/vcal/a/a/f;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16e

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x16d

    goto :goto_0
.end method

.method static b(III)I
    .locals 2

    .prologue
    .line 1182
    sget-object v0, Lcom/android/calendar/vcal/a/a/f;->g:[I

    aget v0, v0, p1

    add-int/2addr v0, p2

    add-int/lit8 v0, v0, -0x1

    .line 1183
    const/4 v1, 0x2

    if-lt p1, v1, :cond_0

    invoke-static {p0}, Lcom/android/calendar/vcal/a/a/f;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1184
    add-int/lit8 v0, v0, 0x1

    .line 1186
    :cond_0
    return v0
.end method

.method private static final b(Landroid/text/format/Time;)J
    .locals 4

    .prologue
    .line 1204
    iget v0, p0, Landroid/text/format/Time;->year:I

    int-to-long v0, v0

    const/16 v2, 0x1a

    shl-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->month:I

    shl-int/lit8 v2, v2, 0x16

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->monthDay:I

    shl-int/lit8 v2, v2, 0x11

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->hour:I

    shl-int/lit8 v2, v2, 0xc

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->minute:I

    shl-int/lit8 v2, v2, 0x6

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->second:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private static c(III)Z
    .locals 1

    .prologue
    .line 296
    if-le p0, p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/text/format/Time;Lcom/android/a/ad;)J
    .locals 12

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    .line 66
    .line 71
    iget-object v0, p2, Lcom/android/a/ad;->a:[Lcom/android/a/c;

    if-eqz v0, :cond_4

    .line 72
    iget-object v9, p2, Lcom/android/a/ad;->a:[Lcom/android/a/c;

    array-length v10, v9

    move v8, v5

    move v4, v5

    move-wide v2, v6

    :goto_0
    if-ge v8, v10, :cond_1

    aget-object v0, v9, v8

    .line 73
    iget v1, v0, Lcom/android/a/c;->d:I

    if-eqz v1, :cond_0

    .line 74
    const/4 v0, 0x1

    .line 72
    :goto_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v4, v0

    goto :goto_0

    .line 75
    :cond_0
    iget-object v1, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 77
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    iget-object v0, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 78
    iget-object v0, p0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 79
    cmp-long v11, v0, v2

    if-lez v11, :cond_c

    :goto_2
    move-wide v2, v0

    move v0, v4

    .line 82
    goto :goto_1

    .line 88
    :cond_1
    cmp-long v0, v2, v6

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/android/a/ad;->b:[J

    if-eqz v0, :cond_2

    .line 89
    iget-object v9, p2, Lcom/android/a/ad;->b:[J

    array-length v10, v9

    move v8, v5

    :goto_3
    if-ge v8, v10, :cond_2

    aget-wide v0, v9, v8

    .line 90
    cmp-long v11, v0, v2

    if-lez v11, :cond_b

    .line 89
    :goto_4
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move-wide v2, v0

    goto :goto_3

    .line 98
    :cond_2
    cmp-long v0, v2, v6

    if-eqz v0, :cond_7

    if-nez v4, :cond_7

    move-wide v6, v2

    .line 128
    :cond_3
    :goto_5
    return-wide v6

    .line 101
    :cond_4
    iget-object v0, p2, Lcom/android/a/ad;->b:[J

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/android/a/ad;->c:[Lcom/android/a/c;

    if-nez v0, :cond_6

    iget-object v0, p2, Lcom/android/a/ad;->d:[J

    if-nez v0, :cond_6

    .line 104
    iget-object v4, p2, Lcom/android/a/ad;->b:[J

    array-length v8, v4

    move-wide v2, v6

    :goto_6
    if-ge v5, v8, :cond_5

    aget-wide v0, v4, v5

    .line 105
    cmp-long v6, v0, v2

    if-lez v6, :cond_a

    .line 104
    :goto_7
    add-int/lit8 v5, v5, 0x1

    move-wide v2, v0

    goto :goto_6

    :cond_5
    move-wide v6, v2

    .line 109
    goto :goto_5

    :cond_6
    move v4, v5

    .line 114
    :cond_7
    if-nez v4, :cond_8

    iget-object v0, p2, Lcom/android/a/ad;->b:[J

    if-eqz v0, :cond_3

    .line 117
    :cond_8
    invoke-virtual {p1, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;Lcom/android/a/ad;JJ)[J

    move-result-object v0

    .line 123
    array-length v1, v0

    if-nez v1, :cond_9

    .line 124
    const-wide/16 v6, 0x0

    goto :goto_5

    .line 126
    :cond_9
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-wide v6, v0, v1

    goto :goto_5

    :cond_a
    move-wide v0, v2

    goto :goto_7

    :cond_b
    move-wide v0, v2

    goto :goto_4

    :cond_c
    move-wide v0, v2

    goto :goto_2
.end method

.method public a(Landroid/text/format/Time;Lcom/android/a/c;JJZLjava/util/TreeSet;)V
    .locals 43

    .prologue
    .line 629
    invoke-static/range {p1 .. p1}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;)V

    .line 630
    invoke-static/range {p1 .. p1}, Lcom/android/calendar/vcal/a/a/f;->b(Landroid/text/format/Time;)J

    move-result-wide v28

    .line 631
    const/4 v2, 0x0

    .line 639
    if-eqz p7, :cond_25

    cmp-long v3, v28, p3

    if-ltz v3, :cond_25

    cmp-long v3, v28, p5

    if-gez v3, :cond_25

    .line 641
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 642
    const/4 v2, 0x1

    move v3, v2

    .line 645
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    move-object/from16 v27, v0

    .line 646
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/vcal/a/a/f;->b:Landroid/text/format/Time;

    .line 647
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/vcal/a/a/f;->c:Ljava/lang/StringBuilder;

    .line 648
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/vcal/a/a/f;->d:Landroid/text/format/Time;

    .line 649
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a/f;->e:Lcom/android/calendar/vcal/a/a/g;

    move-object/from16 v30, v0

    .line 653
    :try_start_0
    move-object/from16 v0, v30

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/calendar/vcal/a/a/g;->a(Lcom/android/a/c;)V

    .line 654
    const-wide v4, 0x7fffffffffffffffL

    cmp-long v4, p5, v4

    if-nez v4, :cond_0

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    if-nez v4, :cond_0

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/a/c;->d:I

    if-nez v4, :cond_0

    .line 655
    new-instance v2, Lcom/android/calendar/vcal/a/a/d;

    const-string v3, "No range end provided for a recurrence that has no UNTIL or COUNT."

    invoke-direct {v2, v3}, Lcom/android/calendar/vcal/a/a/d;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1016
    :catch_0
    move-exception v2

    .line 1017
    const-string v3, "RecurrenceProcessor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DateException with r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rangeStart="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rangeEnd="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1019
    throw v2

    .line 661
    :cond_0
    :try_start_1
    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/a/c;->e:I

    .line 662
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->b:I

    move/from16 v31, v0

    .line 663
    packed-switch v31, :pswitch_data_0

    .line 691
    new-instance v2, Lcom/android/calendar/vcal/a/a/d;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bad freq="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v31

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/calendar/vcal/a/a/d;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1021
    :catch_1
    move-exception v2

    .line 1022
    const-string v3, "RecurrenceProcessor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RuntimeException with r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rangeStart="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rangeEnd="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1024
    throw v2

    .line 666
    :pswitch_0
    const/4 v5, 0x1

    move/from16 v26, v5

    .line 693
    :goto_1
    if-gtz v4, :cond_23

    .line 694
    const/4 v4, 0x1

    move/from16 v25, v4

    .line 697
    :goto_2
    :try_start_2
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->w:I

    move/from16 v32, v0

    .line 698
    const/4 v4, 0x6

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-static {v0, v4, v1}, Lcom/android/calendar/vcal/a/a/f;->c(III)Z

    move-result v33

    .line 699
    const/4 v4, 0x5

    move/from16 v0, v31

    if-lt v0, v4, :cond_5

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/a/c;->o:I

    if-gtz v4, :cond_1

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/a/c;->q:I

    if-lez v4, :cond_5

    :cond_1
    const/4 v4, 0x1

    move/from16 v24, v4

    .line 701
    :goto_3
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->l:I

    move/from16 v34, v0

    .line 702
    const/4 v4, 0x3

    move/from16 v0, v31

    move/from16 v1, v34

    invoke-static {v0, v4, v1}, Lcom/android/calendar/vcal/a/a/f;->c(III)Z

    move-result v35

    .line 703
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->j:I

    move/from16 v36, v0

    .line 704
    const/4 v4, 0x2

    move/from16 v0, v31

    move/from16 v1, v36

    invoke-static {v0, v4, v1}, Lcom/android/calendar/vcal/a/a/f;->c(III)Z

    move-result v37

    .line 705
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->h:I

    move/from16 v38, v0

    .line 706
    const/4 v4, 0x1

    move/from16 v0, v31

    move/from16 v1, v38

    invoke-static {v0, v4, v1}, Lcom/android/calendar/vcal/a/a/f;->c(III)Z

    move-result v39

    .line 709
    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 710
    const/4 v4, 0x5

    move/from16 v0, v26

    if-ne v0, v4, :cond_2

    .line 711
    if-eqz v24, :cond_2

    .line 717
    const/4 v4, 0x1

    move-object/from16 v0, v27

    iput v4, v0, Landroid/text/format/Time;->monthDay:I

    .line 722
    :cond_2
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 724
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 728
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v8, 0xf

    if-ne v5, v8, :cond_3

    .line 729
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x5a

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 732
    :cond_3
    invoke-virtual {v6, v4}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 734
    iget v4, v6, Landroid/text/format/Time;->year:I

    const/16 v5, 0x7f4

    if-le v4, v5, :cond_4

    .line 735
    const/16 v4, 0x7f4

    iput v4, v6, Landroid/text/format/Time;->year:I

    .line 741
    :cond_4
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v6, v4}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 742
    invoke-static {v6}, Lcom/android/calendar/vcal/a/a/f;->b(Landroid/text/format/Time;)J

    move-result-wide v4

    move-wide/from16 v22, v4

    .line 780
    :goto_4
    const/16 v4, 0xf

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->ensureCapacity(I)V

    .line 781
    const/16 v4, 0xf

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 790
    const/4 v4, 0x0

    move v5, v4

    move v4, v3

    .line 793
    :goto_5
    const/4 v3, 0x0

    .line 795
    add-int/lit8 v21, v5, 0x1

    const/16 v6, 0x7d0

    if-le v5, v6, :cond_7

    .line 796
    new-instance v2, Lcom/android/calendar/vcal/a/a/d;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Recurrence processing stuck: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/android/a/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/calendar/vcal/a/a/d;-><init>(Ljava/lang/String;)V

    throw v2

    .line 669
    :pswitch_1
    const/4 v5, 0x2

    move/from16 v26, v5

    .line 670
    goto/16 :goto_1

    .line 672
    :pswitch_2
    const/4 v5, 0x3

    move/from16 v26, v5

    .line 673
    goto/16 :goto_1

    .line 675
    :pswitch_3
    const/4 v5, 0x4

    move/from16 v26, v5

    .line 676
    goto/16 :goto_1

    .line 678
    :pswitch_4
    const/4 v5, 0x4

    .line 679
    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/a/c;->e:I

    mul-int/lit8 v4, v4, 0x7

    .line 680
    if-gtz v4, :cond_24

    .line 681
    const/4 v4, 0x7

    move/from16 v26, v5

    goto/16 :goto_1

    .line 685
    :pswitch_5
    const/4 v5, 0x5

    move/from16 v26, v5

    .line 686
    goto/16 :goto_1

    .line 688
    :pswitch_6
    const/4 v5, 0x6

    move/from16 v26, v5

    .line 689
    goto/16 :goto_1

    .line 699
    :cond_5
    const/4 v4, 0x0

    move/from16 v24, v4

    goto/16 :goto_3

    .line 746
    :cond_6
    new-instance v4, Landroid/text/format/Time;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 747
    packed-switch v31, :pswitch_data_1

    .line 777
    :goto_6
    :pswitch_7
    invoke-static {v4}, Lcom/android/calendar/vcal/a/a/f;->b(Landroid/text/format/Time;)J

    move-result-wide v4

    move-wide/from16 v22, v4

    goto :goto_4

    .line 753
    :pswitch_8
    iget v5, v4, Landroid/text/format/Time;->year:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Landroid/text/format/Time;->year:I

    goto :goto_6

    .line 756
    :pswitch_9
    iget v5, v4, Landroid/text/format/Time;->year:I

    add-int/lit8 v5, v5, 0x5

    iput v5, v4, Landroid/text/format/Time;->year:I

    goto :goto_6

    .line 759
    :pswitch_a
    iget v5, v4, Landroid/text/format/Time;->year:I

    add-int/lit8 v5, v5, 0xa

    iput v5, v4, Landroid/text/format/Time;->year:I

    goto :goto_6

    .line 762
    :pswitch_b
    iget v5, v4, Landroid/text/format/Time;->year:I

    add-int/lit8 v5, v5, 0x32

    iput v5, v4, Landroid/text/format/Time;->year:I

    goto :goto_6

    .line 799
    :cond_7
    invoke-static/range {v27 .. v27}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;)V

    .line 801
    move-object/from16 v0, v27

    iget v8, v0, Landroid/text/format/Time;->year:I

    .line 802
    move-object/from16 v0, v27

    iget v5, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v19, v5, 0x1

    .line 803
    move-object/from16 v0, v27

    iget v10, v0, Landroid/text/format/Time;->monthDay:I

    .line 804
    move-object/from16 v0, v27

    iget v11, v0, Landroid/text/format/Time;->hour:I

    .line 805
    move-object/from16 v0, v27

    iget v12, v0, Landroid/text/format/Time;->minute:I

    .line 806
    move-object/from16 v0, v27

    iget v14, v0, Landroid/text/format/Time;->second:I

    .line 809
    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    :cond_8
    move/from16 v20, v3

    .line 814
    if-eqz v33, :cond_b

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/c;->v:[I

    aget v3, v3, v20

    .line 817
    :goto_7
    add-int/lit8 v7, v3, -0x1

    .line 820
    const/4 v9, 0x1

    .line 821
    const/4 v3, 0x0

    .line 826
    if-eqz v24, :cond_22

    .line 830
    const/4 v3, 0x5

    move/from16 v0, v31

    if-ne v0, v3, :cond_c

    .line 831
    move-object/from16 v0, v27

    iget v3, v0, Landroid/text/format/Time;->weekDay:I

    .line 832
    move-object/from16 v0, v27

    iget v5, v0, Landroid/text/format/Time;->monthDay:I

    sub-int v9, v5, v3

    .line 833
    add-int/lit8 v3, v9, 0x6

    move/from16 v18, v3

    .line 845
    :goto_8
    if-eqz v24, :cond_11

    .line 846
    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v9}, Lcom/android/calendar/vcal/a/a/g;->a(Landroid/text/format/Time;I)Z

    move-result v3

    if-nez v3, :cond_d

    .line 847
    add-int/lit8 v3, v9, 0x1

    .line 961
    :goto_9
    if-eqz v24, :cond_9

    move/from16 v0, v18

    if-le v3, v0, :cond_1e

    .line 962
    :cond_9
    add-int/lit8 v3, v20, 0x1

    .line 963
    if-eqz v33, :cond_a

    move/from16 v0, v32

    if-lt v3, v0, :cond_8

    .line 969
    :cond_a
    move-object/from16 v0, v27

    iget v5, v0, Landroid/text/format/Time;->monthDay:I

    .line 970
    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 971
    const/4 v3, 0x1

    .line 973
    :goto_a
    mul-int v6, v25, v3

    .line 974
    packed-switch v26, :pswitch_data_2

    .line 1000
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bad field="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_b
    move/from16 v3, v19

    .line 814
    goto :goto_7

    .line 835
    :cond_c
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v3

    move/from16 v18, v3

    goto :goto_8

    :cond_d
    move v6, v9

    .line 858
    :goto_b
    const/4 v3, 0x0

    move/from16 v17, v3

    move v13, v4

    .line 860
    :goto_c
    if-eqz v35, :cond_12

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/c;->k:[I

    aget v5, v3, v17

    .line 866
    :goto_d
    const/4 v3, 0x0

    move/from16 v16, v3

    .line 868
    :goto_e
    if-eqz v37, :cond_13

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/c;->i:[I

    aget v4, v3, v16

    .line 874
    :goto_f
    const/4 v3, 0x0

    move v15, v3

    .line 876
    :goto_10
    if-eqz v39, :cond_14

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/c;->g:[I

    aget v3, v3, v15

    .line 884
    :goto_11
    invoke-virtual/range {v2 .. v8}, Landroid/text/format/Time;->set(IIIIII)V

    .line 885
    invoke-static {v2}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;)V

    .line 887
    invoke-static {v2}, Lcom/android/calendar/vcal/a/a/f;->b(Landroid/text/format/Time;)J

    move-result-wide v40

    .line 891
    cmp-long v3, v40, v28

    if-ltz v3, :cond_17

    .line 893
    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/calendar/vcal/a/a/f;->a(Lcom/android/a/c;Landroid/text/format/Time;)I

    move-result v3

    .line 894
    if-nez v3, :cond_17

    .line 910
    cmp-long v3, v28, v40

    if-nez v3, :cond_e

    if-eqz p7, :cond_e

    cmp-long v3, v28, p3

    if-ltz v3, :cond_e

    cmp-long v3, v28, p5

    if-ltz v3, :cond_f

    .line 914
    :cond_e
    add-int/lit8 v13, v13, 0x1

    .line 918
    :cond_f
    cmp-long v3, v40, v22

    if-lez v3, :cond_15

    .line 1026
    :cond_10
    return-void

    :cond_11
    move v6, v10

    .line 853
    goto :goto_b

    :cond_12
    move v5, v11

    .line 860
    goto :goto_d

    :cond_13
    move v4, v12

    .line 868
    goto :goto_f

    :cond_14
    move v3, v14

    .line 876
    goto :goto_11

    .line 928
    :cond_15
    cmp-long v3, v40, p5

    if-gez v3, :cond_10

    .line 937
    cmp-long v3, v40, p3

    if-ltz v3, :cond_16

    .line 941
    if-eqz p7, :cond_1b

    .line 942
    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 948
    :cond_16
    :goto_12
    move-object/from16 v0, p2

    iget v3, v0, Lcom/android/a/c;->d:I

    if-lez v3, :cond_17

    move-object/from16 v0, p2

    iget v3, v0, Lcom/android/a/c;->d:I

    if-eq v3, v13, :cond_10

    .line 954
    :cond_17
    add-int/lit8 v3, v15, 0x1

    .line 955
    if-eqz v39, :cond_18

    move/from16 v0, v38

    if-lt v3, v0, :cond_21

    .line 956
    :cond_18
    add-int/lit8 v3, v16, 0x1

    .line 957
    if-eqz v37, :cond_19

    move/from16 v0, v36

    if-lt v3, v0, :cond_20

    .line 958
    :cond_19
    add-int/lit8 v3, v17, 0x1

    .line 959
    if-eqz v35, :cond_1a

    move/from16 v0, v34

    if-lt v3, v0, :cond_1f

    .line 960
    :cond_1a
    add-int/lit8 v3, v9, 0x1

    move v4, v13

    goto/16 :goto_9

    .line 944
    :cond_1b
    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    goto :goto_12

    .line 976
    :pswitch_c
    move-object/from16 v0, v27

    iget v7, v0, Landroid/text/format/Time;->second:I

    add-int/2addr v6, v7

    move-object/from16 v0, v27

    iput v6, v0, Landroid/text/format/Time;->second:I

    .line 1003
    :goto_13
    invoke-static/range {v27 .. v27}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;)V

    .line 1004
    const/4 v6, 0x6

    move/from16 v0, v26

    if-eq v0, v6, :cond_1d

    const/4 v6, 0x5

    move/from16 v0, v26

    if-eq v0, v6, :cond_1d

    :cond_1c
    move/from16 v5, v21

    .line 1013
    goto/16 :goto_5

    .line 979
    :pswitch_d
    move-object/from16 v0, v27

    iget v7, v0, Landroid/text/format/Time;->minute:I

    add-int/2addr v6, v7

    move-object/from16 v0, v27

    iput v6, v0, Landroid/text/format/Time;->minute:I

    goto :goto_13

    .line 982
    :pswitch_e
    move-object/from16 v0, v27

    iget v7, v0, Landroid/text/format/Time;->hour:I

    add-int/2addr v6, v7

    move-object/from16 v0, v27

    iput v6, v0, Landroid/text/format/Time;->hour:I

    goto :goto_13

    .line 985
    :pswitch_f
    move-object/from16 v0, v27

    iget v7, v0, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v6, v7

    move-object/from16 v0, v27

    iput v6, v0, Landroid/text/format/Time;->monthDay:I

    goto :goto_13

    .line 988
    :pswitch_10
    move-object/from16 v0, v27

    iget v7, v0, Landroid/text/format/Time;->month:I

    add-int/2addr v6, v7

    move-object/from16 v0, v27

    iput v6, v0, Landroid/text/format/Time;->month:I

    goto :goto_13

    .line 991
    :pswitch_11
    move-object/from16 v0, v27

    iget v7, v0, Landroid/text/format/Time;->year:I

    add-int/2addr v6, v7

    move-object/from16 v0, v27

    iput v6, v0, Landroid/text/format/Time;->year:I

    goto :goto_13

    .line 994
    :pswitch_12
    move-object/from16 v0, v27

    iget v7, v0, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v6, v7

    move-object/from16 v0, v27

    iput v6, v0, Landroid/text/format/Time;->monthDay:I

    goto :goto_13

    .line 997
    :pswitch_13
    move-object/from16 v0, v27

    iget v7, v0, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v6, v7

    move-object/from16 v0, v27

    iput v6, v0, Landroid/text/format/Time;->monthDay:I

    goto :goto_13

    .line 1007
    :cond_1d
    move-object/from16 v0, v27

    iget v6, v0, Landroid/text/format/Time;->monthDay:I

    if-eq v6, v5, :cond_1c

    .line 1010
    add-int/lit8 v3, v3, 0x1

    .line 1011
    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V
    :try_end_2
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_a

    :cond_1e
    move v9, v3

    goto/16 :goto_8

    :cond_1f
    move/from16 v17, v3

    goto/16 :goto_c

    :cond_20
    move/from16 v16, v3

    goto/16 :goto_e

    :cond_21
    move v15, v3

    goto/16 :goto_10

    :cond_22
    move/from16 v18, v3

    goto/16 :goto_8

    :cond_23
    move/from16 v25, v4

    goto/16 :goto_2

    :cond_24
    move/from16 v26, v5

    goto/16 :goto_1

    :cond_25
    move v3, v2

    goto/16 :goto_0

    .line 663
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 747
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 974
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public a(Landroid/text/format/Time;Lcom/android/a/ad;JJ)[J
    .locals 17

    .prologue
    .line 522
    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    invoke-virtual {v2, v12}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 524
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/vcal/a/a/f;->d:Landroid/text/format/Time;

    invoke-virtual {v2, v12}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 530
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/vcal/a/a/f;->b(Landroid/text/format/Time;)J

    move-result-wide v6

    .line 534
    const-wide/16 v2, -0x1

    cmp-long v2, p5, v2

    if-eqz v2, :cond_0

    .line 535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/vcal/a/a/f;->b(Landroid/text/format/Time;)J

    move-result-wide v8

    .line 541
    :goto_0
    new-instance v11, Ljava/util/TreeSet;

    invoke-direct {v11}, Ljava/util/TreeSet;-><init>()V

    .line 543
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/ad;->a:[Lcom/android/a/c;

    if-eqz v2, :cond_1

    .line 544
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/android/a/ad;->a:[Lcom/android/a/c;

    array-length v14, v13

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v14, :cond_1

    aget-object v5, v13, v2

    .line 545
    const/4 v10, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;Lcom/android/a/c;JJZLjava/util/TreeSet;)V

    .line 544
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 538
    :cond_0
    const-wide v8, 0x7fffffffffffffffL

    goto :goto_0

    .line 549
    :cond_1
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/ad;->b:[J

    if-eqz v2, :cond_2

    .line 550
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/ad;->b:[J

    array-length v4, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_2

    aget-wide v14, v3, v2

    .line 553
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    invoke-virtual {v5, v14, v15}, Landroid/text/format/Time;->set(J)V

    .line 554
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    invoke-static {v5}, Lcom/android/calendar/vcal/a/a/f;->b(Landroid/text/format/Time;)J

    move-result-wide v14

    .line 555
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 550
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 558
    :cond_2
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/ad;->c:[Lcom/android/a/c;

    if-eqz v2, :cond_3

    .line 559
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/android/a/ad;->c:[Lcom/android/a/c;

    array-length v14, v13

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v14, :cond_3

    aget-object v5, v13, v2

    .line 560
    const/4 v10, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;Lcom/android/a/c;JJZLjava/util/TreeSet;)V

    .line 559
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 564
    :cond_3
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/ad;->d:[J

    if-eqz v2, :cond_4

    .line 565
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/ad;->d:[J

    array-length v4, v3

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v4, :cond_4

    aget-wide v6, v3, v2

    .line 568
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    invoke-virtual {v5, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 569
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/vcal/a/a/f;->a:Landroid/text/format/Time;

    invoke-static {v5}, Lcom/android/calendar/vcal/a/a/f;->b(Landroid/text/format/Time;)J

    move-result-wide v6

    .line 570
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 565
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 573
    :cond_4
    invoke-virtual {v11}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 576
    const/4 v2, 0x0

    new-array v2, v2, [J

    .line 598
    :goto_5
    return-object v2

    .line 583
    :cond_5
    invoke-virtual {v11}, Ljava/util/TreeSet;->size()I

    move-result v2

    .line 584
    new-array v4, v2, [J

    .line 585
    const/4 v2, 0x0

    .line 592
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 593
    invoke-virtual {v5, v12}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 594
    invoke-virtual {v11}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 595
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v5, v8, v9}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;J)V

    .line 596
    add-int/lit8 v2, v3, 0x1

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    aput-wide v8, v4, v3

    move v3, v2

    .line 597
    goto :goto_6

    :cond_6
    move-object v2, v4

    .line 598
    goto :goto_5
.end method
