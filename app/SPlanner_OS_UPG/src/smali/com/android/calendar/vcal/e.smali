.class Lcom/android/calendar/vcal/e;
.super Ljava/lang/Object;
.source "VCalInfoActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VCalInfoActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/vcal/VCalInfoActivity;)V
    .locals 0

    .prologue
    .line 644
    iput-object p1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 646
    invoke-static {p2}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(I)I

    .line 648
    iget-object v0, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->f(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 649
    iget-object v0, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->f(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/vcal/VCalInfoActivity;->a()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 650
    iget-object v0, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->f(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(Lcom/android/calendar/vcal/VCalInfoActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 652
    iget-object v0, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->f(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(Lcom/android/calendar/vcal/VCalInfoActivity;I)I

    .line 653
    iget-object v0, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->f(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 655
    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->g(Lcom/android/calendar/vcal/VCalInfoActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "My calendar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 656
    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    iget-object v2, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v2}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(Lcom/android/calendar/vcal/VCalInfoActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 659
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->h(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalInfoActivity;->g(Lcom/android/calendar/vcal/VCalInfoActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 661
    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->i(Lcom/android/calendar/vcal/VCalInfoActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalInfoActivity;->d(Lcom/android/calendar/vcal/VCalInfoActivity;)I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 662
    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->j(Lcom/android/calendar/vcal/VCalInfoActivity;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->k(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/content/ContentValues;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 663
    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->k(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "calendar_id"

    iget-object v3, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v3}, Lcom/android/calendar/vcal/VCalInfoActivity;->d(Lcom/android/calendar/vcal/VCalInfoActivity;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 664
    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(Lcom/android/calendar/vcal/VCalInfoActivity;)Lcom/android/calendar/vcal/u;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v2}, Lcom/android/calendar/vcal/VCalInfoActivity;->k(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/vcal/u;->a(Landroid/content/ContentValues;)V

    .line 666
    :cond_1
    if-nez v0, :cond_2

    .line 667
    iget-object v0, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-virtual {v0}, Lcom/android/calendar/vcal/VCalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 669
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->l(Lcom/android/calendar/vcal/VCalInfoActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-static {v0}, Lcom/android/calendar/hj;->b(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 672
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    iget-object v1, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    invoke-static {v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->d(Lcom/android/calendar/vcal/VCalInfoActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->b(Lcom/android/calendar/vcal/VCalInfoActivity;I)I

    .line 674
    :cond_4
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 675
    iget-object v0, p0, Lcom/android/calendar/vcal/e;->a:Lcom/android/calendar/vcal/VCalInfoActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/vcal/VCalInfoActivity;->a(Lcom/android/calendar/vcal/VCalInfoActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 676
    return-void
.end method
