.class public Lcom/android/calendar/vcal/VCalInfoScrollView;
.super Landroid/widget/ScrollView;
.source "VCalInfoScrollView.java"


# static fields
.field private static b:I


# instance fields
.field private a:Landroid/view/GestureDetector;

.field private c:Lcom/android/calendar/vcal/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x32

    sput v0, Lcom/android/calendar/vcal/VCalInfoScrollView;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalInfoScrollView;->c:Lcom/android/calendar/vcal/k;

    .line 40
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/android/calendar/vcal/j;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/j;-><init>(Lcom/android/calendar/vcal/VCalInfoScrollView;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/vcal/VCalInfoScrollView;->a:Landroid/view/GestureDetector;

    .line 62
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/android/calendar/vcal/VCalInfoScrollView;->b:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VCalInfoScrollView;)Lcom/android/calendar/vcal/k;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoScrollView;->c:Lcom/android/calendar/vcal/k;

    return-object v0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoScrollView;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 68
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/vcal/VCalInfoScrollView;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 73
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnFlingListener(Lcom/android/calendar/vcal/k;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/android/calendar/vcal/VCalInfoScrollView;->c:Lcom/android/calendar/vcal/k;

    .line 78
    return-void
.end method
