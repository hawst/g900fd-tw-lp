.class public Lcom/android/calendar/vcal/a/a/k;
.super Lcom/android/calendar/vcal/a/e;
.source "VCalParser_V10.java"


# static fields
.field private static final d:Ljava/util/HashSet;

.field private static final e:Ljava/util/HashSet;

.field private static final f:Ljava/util/HashSet;

.field private static final g:Ljava/util/HashSet;

.field private static final h:Ljava/util/HashSet;

.field private static final i:Ljava/util/HashSet;

.field private static final j:Ljava/util/HashSet;

.field private static final k:Ljava/util/HashSet;

.field private static final l:Ljava/util/HashMap;


# instance fields
.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x1a

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "ATTACH"

    aput-object v2, v1, v4

    const-string v2, "ATTENDEE"

    aput-object v2, v1, v5

    const-string v2, "DCREATED"

    aput-object v2, v1, v6

    const-string v2, "COMPLETED"

    aput-object v2, v1, v7

    const-string v2, "DESCRIPTION"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "DUE"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "DTEND"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "EXRULE"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "LAST-MODIFIED"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "LOCATION"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "RNUM"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "PRIORITY"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "RELATED-TO"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "RRULE"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "SEQUENCE"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "DTSTART"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "SUMMARY"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "TRANSP"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "URL"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "UID"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "CLASS"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "STATUS"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "TZ"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "ORGANIZER"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "TRIGGER"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "GEO"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->d:Ljava/util/HashSet;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "AALARM"

    aput-object v2, v1, v4

    const-string v2, "CATEGORIES"

    aput-object v2, v1, v5

    const-string v2, "DALARM"

    aput-object v2, v1, v6

    const-string v2, "EXDATE"

    aput-object v2, v1, v7

    const-string v2, "MALARM"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "PALARM"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "RDATE"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "RESOURCES"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->e:Ljava/util/HashSet;

    .line 58
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "APPOINTMENT"

    aput-object v2, v1, v4

    const-string v2, "BUSINESS"

    aput-object v2, v1, v5

    const-string v2, "EDUCATION"

    aput-object v2, v1, v6

    const-string v2, "HOLIDAY"

    aput-object v2, v1, v7

    const-string v2, "MEETING"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "MISCELLANEOUS"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "PERSONAL"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "PHONE CALL"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "SICK DAY"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "SPECIAL OCCASION"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "TRAVEL"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "VACATION"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->f:Ljava/util/HashSet;

    .line 63
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "PUBLIC"

    aput-object v2, v1, v4

    const-string v2, "PRIVATE"

    aput-object v2, v1, v5

    const-string v2, "CONFIDENTIAL"

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->g:Ljava/util/HashSet;

    .line 66
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "CATERING"

    aput-object v2, v1, v4

    const-string v2, "CHAIRS"

    aput-object v2, v1, v5

    const-string v2, "EASEL"

    aput-object v2, v1, v6

    const-string v2, "PROJECTOR"

    aput-object v2, v1, v7

    const-string v2, "VCR"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "VEHICLE"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->h:Ljava/util/HashSet;

    .line 70
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "ACCEPTED"

    aput-object v2, v1, v4

    const-string v2, "NEEDS ACTION"

    aput-object v2, v1, v5

    const-string v2, "SENT"

    aput-object v2, v1, v6

    const-string v2, "TENTATIVE"

    aput-object v2, v1, v7

    const-string v2, "CONFIRMED"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "DECLINED"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "COMPLETED"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "DELEGATED"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->i:Ljava/util/HashSet;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "ACCEPTED"

    aput-object v2, v1, v4

    const-string v2, "NEEDS ACTION"

    aput-object v2, v1, v5

    const-string v2, "SENT"

    aput-object v2, v1, v6

    const-string v2, "TENTATIVE"

    aput-object v2, v1, v7

    const-string v2, "CONFIRMED"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "DECLINED"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "COMPLETED"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "DELEGATED"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->j:Ljava/util/HashSet;

    .line 81
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "DESCRIPTION"

    aput-object v2, v1, v4

    const-string v2, "SUMMARY"

    aput-object v2, v1, v5

    const-string v2, "AALARM"

    aput-object v2, v1, v6

    const-string v2, "DALARM"

    aput-object v2, v1, v7

    const-string v2, "MALARM"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "PALARM"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->k:Ljava/util/HashSet;

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/calendar/vcal/a/a/k;->l:Ljava/util/HashMap;

    .line 90
    sget-object v0, Lcom/android/calendar/vcal/a/a/k;->l:Ljava/util/HashMap;

    const-string v1, "CATEGORIES"

    sget-object v2, Lcom/android/calendar/vcal/a/a/k;->f:Ljava/util/HashSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/android/calendar/vcal/a/a/k;->l:Ljava/util/HashMap;

    const-string v1, "CLASS"

    sget-object v2, Lcom/android/calendar/vcal/a/a/k;->g:Ljava/util/HashSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/android/calendar/vcal/a/a/k;->l:Ljava/util/HashMap;

    const-string v1, "RESOURCES"

    sget-object v2, Lcom/android/calendar/vcal/a/a/k;->h:Ljava/util/HashSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    sget-object v0, Lcom/android/calendar/vcal/a/a/k;->l:Ljava/util/HashMap;

    const-string v1, "STATUS"

    sget-object v2, Lcom/android/calendar/vcal/a/a/k;->j:Ljava/util/HashSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    sget-object v0, Lcom/android/calendar/vcal/a/a/k;->l:Ljava/util/HashMap;

    const-string v1, "STATUS"

    sget-object v2, Lcom/android/calendar/vcal/a/a/k;->i:Ljava/util/HashSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/android/calendar/vcal/a/e;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/vcal/a/a/k;->m:I

    .line 101
    return-void
.end method

.method private A(I)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 697
    .line 699
    const-string v0, "BEGIN"

    invoke-virtual {p0, p1, v0, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 700
    if-ne v2, v0, :cond_1

    move v0, v2

    .line 819
    :cond_0
    :goto_0
    return v0

    .line 703
    :cond_1
    add-int v1, p1, v0

    .line 704
    add-int/2addr v0, v5

    .line 707
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 708
    add-int/2addr v1, v3

    .line 709
    add-int/2addr v0, v3

    .line 712
    const-string v3, ":"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 713
    if-ne v2, v3, :cond_2

    move v0, v2

    .line 714
    goto :goto_0

    .line 716
    :cond_2
    add-int/2addr v1, v3

    .line 717
    add-int/2addr v0, v3

    .line 720
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 721
    add-int/2addr v1, v3

    .line 722
    add-int/2addr v0, v3

    .line 725
    const-string v3, "VTODO"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 726
    if-ne v2, v3, :cond_3

    move v0, v2

    .line 727
    goto :goto_0

    .line 729
    :cond_3
    add-int/2addr v1, v3

    .line 730
    add-int/2addr v0, v3

    .line 731
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_4

    .line 732
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    const-string v4, "VTODO"

    invoke-interface {v3, v4}, Lcom/android/calendar/vcal/a/b;->a(Ljava/lang/String;)V

    .line 736
    :cond_4
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    .line 737
    if-ne v2, v3, :cond_5

    move v0, v2

    .line 738
    goto :goto_0

    .line 740
    :cond_5
    add-int/2addr v1, v3

    .line 741
    add-int/2addr v0, v3

    .line 742
    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_6

    .line 743
    add-int/2addr v1, v3

    .line 744
    add-int/2addr v0, v3

    goto :goto_1

    .line 747
    :cond_6
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->B(I)I

    move-result v3

    .line 748
    if-ne v2, v3, :cond_7

    move v0, v2

    .line 749
    goto :goto_0

    .line 751
    :cond_7
    add-int/2addr v1, v3

    .line 752
    add-int/2addr v0, v3

    .line 755
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 756
    add-int/2addr v1, v3

    .line 757
    add-int/2addr v0, v3

    .line 760
    :goto_2
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_8

    .line 761
    add-int/2addr v1, v3

    .line 762
    add-int/2addr v0, v3

    goto :goto_2

    .line 766
    :cond_8
    const-string v3, "END"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 767
    if-ne v2, v3, :cond_9

    move v0, v2

    .line 768
    goto :goto_0

    .line 770
    :cond_9
    add-int/2addr v1, v3

    .line 771
    add-int/2addr v0, v3

    .line 774
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 775
    add-int/2addr v1, v3

    .line 776
    add-int/2addr v0, v3

    .line 779
    const-string v3, ":"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 780
    if-ne v2, v3, :cond_a

    move v0, v2

    .line 781
    goto :goto_0

    .line 783
    :cond_a
    add-int/2addr v1, v3

    .line 784
    add-int/2addr v0, v3

    .line 787
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 788
    add-int/2addr v1, v3

    .line 789
    add-int/2addr v0, v3

    .line 792
    const-string v3, "VTODO"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 793
    if-ne v2, v3, :cond_b

    move v0, v2

    .line 794
    goto/16 :goto_0

    .line 796
    :cond_b
    add-int/2addr v1, v3

    .line 797
    add-int/2addr v0, v3

    .line 798
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_c

    .line 799
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3}, Lcom/android/calendar/vcal/a/b;->c()V

    .line 803
    :cond_c
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 804
    add-int/2addr v1, v3

    .line 805
    add-int/2addr v0, v3

    .line 808
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    .line 809
    if-ne v2, v3, :cond_d

    move v0, v2

    .line 810
    goto/16 :goto_0

    .line 812
    :cond_d
    add-int/2addr v1, v3

    .line 813
    add-int/2addr v0, v3

    .line 814
    :goto_3
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 815
    add-int/2addr v1, v3

    .line 816
    add-int/2addr v0, v3

    goto :goto_3
.end method

.method private B(I)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 826
    const/4 v0, 0x0

    .line 827
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v1, :cond_0

    .line 828
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v1}, Lcom/android/calendar/vcal/a/b;->d()V

    .line 831
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->C(I)I

    move-result v3

    .line 832
    if-ne v2, v3, :cond_2

    move v0, v2

    .line 860
    :cond_1
    return v0

    .line 835
    :cond_2
    add-int v1, p1, v3

    .line 836
    add-int/2addr v0, v3

    .line 837
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_3

    .line 838
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3}, Lcom/android/calendar/vcal/a/b;->e()V

    .line 842
    :cond_3
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_4

    .line 843
    add-int/2addr v1, v3

    .line 844
    add-int/2addr v0, v3

    goto :goto_0

    .line 846
    :cond_4
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_5

    .line 847
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3}, Lcom/android/calendar/vcal/a/b;->d()V

    .line 850
    :cond_5
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->C(I)I

    move-result v3

    .line 851
    if-eq v2, v3, :cond_1

    .line 854
    add-int/2addr v1, v3

    .line 855
    add-int/2addr v0, v3

    .line 856
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_3

    .line 857
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3}, Lcom/android/calendar/vcal/a/b;->e()V

    goto :goto_0
.end method

.method private C(I)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 867
    .line 869
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/vcal/a/a/k;->c:Ljava/lang/String;

    .line 870
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->G(I)I

    move-result v0

    .line 871
    if-eq v1, v0, :cond_1

    .line 879
    :cond_0
    :goto_0
    return v0

    .line 875
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->H(I)I

    move-result v0

    .line 876
    if-ne v1, v0, :cond_0

    move v0, v1

    .line 879
    goto :goto_0
.end method

.method private D(I)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 886
    const/4 v1, 0x0

    .line 888
    const-string v2, ";"

    const/4 v3, 0x1

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 889
    if-ne v0, v2, :cond_1

    .line 905
    :cond_0
    :goto_0
    return v0

    .line 892
    :cond_1
    add-int v3, p1, v2

    .line 893
    add-int/2addr v1, v2

    .line 895
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 896
    add-int/2addr v3, v2

    .line 897
    add-int/2addr v1, v2

    .line 899
    invoke-direct {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->E(I)I

    move-result v2

    .line 900
    if-eq v0, v2, :cond_0

    .line 903
    add-int v0, v1, v2

    .line 905
    goto :goto_0
.end method

.method private E(I)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, -0x1

    .line 912
    .line 914
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->F(I)I

    move-result v0

    .line 915
    if-ne v3, v0, :cond_1

    move v2, v3

    .line 949
    :cond_0
    return v2

    .line 918
    :cond_1
    add-int v1, p1, v0

    .line 919
    add-int/2addr v0, v5

    move v2, v0

    .line 924
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v4

    .line 925
    add-int/2addr v1, v4

    .line 926
    add-int/2addr v0, v4

    .line 928
    const-string v4, ";"

    invoke-virtual {p0, v1, v4, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v4

    .line 929
    if-eq v3, v4, :cond_0

    .line 932
    add-int/2addr v1, v4

    .line 933
    add-int/2addr v0, v4

    .line 935
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v4

    .line 936
    add-int/2addr v1, v4

    .line 937
    add-int/2addr v0, v4

    .line 939
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->F(I)I

    move-result v4

    .line 940
    if-eq v3, v4, :cond_0

    .line 943
    add-int/2addr v1, v4

    .line 944
    add-int/2addr v0, v4

    move v2, v0

    .line 947
    goto :goto_0
.end method

.method private F(I)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 956
    .line 958
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->I(I)I

    move-result v0

    .line 959
    if-eq v1, v0, :cond_1

    .line 1009
    :cond_0
    :goto_0
    return v0

    .line 963
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->J(I)I

    move-result v0

    .line 964
    if-ne v1, v0, :cond_0

    .line 968
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->K(I)I

    move-result v0

    .line 969
    if-ne v1, v0, :cond_0

    .line 973
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->L(I)I

    move-result v0

    .line 974
    if-ne v1, v0, :cond_0

    .line 978
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->M(I)I

    move-result v0

    .line 979
    if-ne v1, v0, :cond_0

    .line 983
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->N(I)I

    move-result v0

    .line 984
    if-ne v1, v0, :cond_0

    .line 988
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->O(I)I

    move-result v0

    .line 989
    if-ne v1, v0, :cond_0

    .line 993
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->P(I)I

    move-result v0

    .line 994
    if-ne v1, v0, :cond_0

    .line 999
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->Q(I)I

    move-result v0

    .line 1000
    if-ne v1, v0, :cond_2

    move v0, v1

    .line 1001
    goto :goto_0

    .line 1003
    :cond_2
    add-int v1, p1, v0

    .line 1004
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v2, :cond_0

    .line 1005
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1006
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v3, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private G(I)I
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 1019
    .line 1021
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v0

    .line 1022
    add-int v1, p1, v0

    .line 1023
    add-int/2addr v0, v5

    .line 1025
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->m(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 1026
    sget-object v4, Lcom/android/calendar/vcal/a/a/k;->d:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1027
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->e(I)I

    move-result v4

    if-ne v2, v4, :cond_0

    move v0, v2

    .line 1072
    :goto_0
    return v0

    .line 1030
    :cond_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 1031
    add-int/2addr v1, v4

    .line 1032
    add-int/2addr v0, v4

    .line 1033
    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v4, :cond_1

    .line 1034
    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v4, v3}, Lcom/android/calendar/vcal/a/b;->b(Ljava/lang/String;)V

    .line 1037
    :cond_1
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->D(I)I

    move-result v4

    .line 1038
    if-eq v2, v4, :cond_2

    .line 1039
    add-int/2addr v1, v4

    .line 1040
    add-int/2addr v0, v4

    .line 1043
    :cond_2
    const-string v4, ":"

    invoke-virtual {p0, v1, v4, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v4

    .line 1044
    if-ne v2, v4, :cond_3

    move v0, v2

    .line 1045
    goto :goto_0

    .line 1047
    :cond_3
    add-int/2addr v1, v4

    .line 1048
    add-int/2addr v0, v4

    .line 1051
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->f(I)I

    move-result v4

    .line 1052
    if-ne v2, v4, :cond_4

    move v0, v2

    .line 1053
    goto :goto_0

    .line 1055
    :cond_4
    add-int v5, v1, v4

    .line 1056
    add-int/2addr v0, v4

    .line 1057
    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v4, :cond_5

    .line 1058
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1059
    iget-object v6, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v6, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/android/calendar/vcal/a/a/k;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1061
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v1, v4}, Lcom/android/calendar/vcal/a/b;->a(Ljava/util/List;)V

    .line 1063
    invoke-static {v3, v4}, Lcom/android/calendar/vcal/a/a/k;->a(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v1

    if-ne v2, v1, :cond_5

    move v0, v2

    .line 1064
    goto :goto_0

    .line 1067
    :cond_5
    invoke-virtual {p0, v5}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v1

    .line 1068
    if-ne v2, v1, :cond_6

    move v0, v2

    .line 1069
    goto :goto_0

    .line 1071
    :cond_6
    add-int/2addr v0, v1

    .line 1072
    goto :goto_0
.end method

.method private H(I)I
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 1081
    .line 1083
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v0

    .line 1084
    add-int v1, p1, v0

    .line 1085
    add-int/2addr v0, v5

    .line 1087
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->m(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 1088
    sget-object v4, Lcom/android/calendar/vcal/a/a/k;->e:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 1150
    :goto_0
    return v0

    .line 1091
    :cond_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 1092
    add-int/2addr v1, v4

    .line 1093
    add-int/2addr v0, v4

    .line 1094
    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v4, :cond_1

    .line 1095
    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v4, v3}, Lcom/android/calendar/vcal/a/b;->b(Ljava/lang/String;)V

    .line 1098
    :cond_1
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->D(I)I

    move-result v4

    .line 1099
    if-eq v2, v4, :cond_2

    .line 1100
    add-int/2addr v1, v4

    .line 1101
    add-int/2addr v0, v4

    .line 1104
    :cond_2
    const-string v4, ":"

    invoke-virtual {p0, v1, v4, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v4

    .line 1105
    if-ne v2, v4, :cond_3

    move v0, v2

    .line 1106
    goto :goto_0

    .line 1108
    :cond_3
    add-int/2addr v1, v4

    .line 1109
    add-int/2addr v0, v4

    .line 1112
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->f(I)I

    move-result v4

    .line 1113
    if-ne v2, v4, :cond_4

    move v0, v2

    .line 1114
    goto :goto_0

    .line 1116
    :cond_4
    add-int v5, v1, v4

    .line 1117
    add-int/2addr v0, v4

    .line 1119
    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v4, :cond_7

    .line 1121
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1122
    const-string v6, "([^;\\\\]*(\\\\[\\\\;:,])*[^;\\\\]*)(;?)"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 1124
    iget-object v7, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v7, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 1125
    :cond_5
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1126
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/android/calendar/vcal/a/a/k;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1127
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1128
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->end()I

    move-result v7

    .line 1129
    add-int/2addr v7, v1

    if-ne v5, v7, :cond_5

    .line 1130
    const/4 v1, 0x3

    invoke-virtual {v6, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 1131
    const-string v6, ";"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1132
    const-string v1, ""

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1137
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v1, v4}, Lcom/android/calendar/vcal/a/b;->a(Ljava/util/List;)V

    .line 1142
    invoke-static {v3, v4}, Lcom/android/calendar/vcal/a/a/k;->a(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 1145
    :cond_7
    invoke-virtual {p0, v5}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v1

    .line 1146
    if-ne v2, v1, :cond_8

    move v0, v2

    .line 1147
    goto/16 :goto_0

    .line 1149
    :cond_8
    add-int/2addr v0, v1

    .line 1150
    goto/16 :goto_0
.end method

.method private I(I)I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v0, -0x1

    .line 1157
    .line 1159
    const-string v1, "TYPE"

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v1

    .line 1160
    if-ne v0, v1, :cond_0

    .line 1161
    const-string v1, "FMTYPE"

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v1

    .line 1163
    :cond_0
    if-ne v0, v1, :cond_2

    .line 1197
    :cond_1
    :goto_0
    return v0

    .line 1166
    :cond_2
    add-int v2, p1, v1

    .line 1167
    add-int/2addr v1, v5

    .line 1168
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_3

    .line 1169
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v4, p1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1172
    :cond_3
    invoke-virtual {p0, v2}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 1173
    add-int/2addr v2, v3

    .line 1174
    add-int/2addr v1, v3

    .line 1176
    const-string v3, "="

    invoke-virtual {p0, v2, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 1177
    if-eq v0, v3, :cond_1

    .line 1180
    add-int/2addr v2, v3

    .line 1181
    add-int/2addr v1, v3

    .line 1183
    invoke-virtual {p0, v2}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 1184
    add-int/2addr v2, v3

    .line 1185
    add-int/2addr v1, v3

    .line 1188
    invoke-direct {p0, v2}, Lcom/android/calendar/vcal/a/a/k;->R(I)I

    move-result v3

    .line 1189
    if-eq v0, v3, :cond_1

    .line 1192
    add-int v4, v2, v3

    .line 1193
    add-int v0, v1, v3

    .line 1194
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v1, :cond_1

    .line 1195
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private J(I)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v2, -0x1

    .line 1204
    .line 1207
    const-string v1, "VALUE"

    invoke-virtual {p0, p1, v1, v4}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 1208
    if-eq v2, v3, :cond_6

    .line 1209
    add-int v1, p1, v3

    .line 1210
    add-int/2addr v0, v3

    move v3, v4

    .line 1213
    :goto_0
    if-eqz v3, :cond_0

    iget-object v5, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v5, :cond_0

    .line 1214
    iget-object v5, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v6, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v6, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1217
    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v5

    .line 1218
    add-int/2addr v1, v5

    .line 1219
    add-int/2addr v0, v5

    .line 1221
    const-string v5, "="

    invoke-virtual {p0, v1, v5, v4}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v4

    .line 1222
    if-eq v2, v4, :cond_4

    .line 1223
    if-nez v3, :cond_2

    move v0, v2

    .line 1249
    :cond_1
    :goto_1
    return v0

    .line 1226
    :cond_2
    add-int/2addr v1, v4

    .line 1227
    add-int/2addr v0, v4

    .line 1234
    :cond_3
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 1235
    add-int/2addr v1, v3

    .line 1236
    add-int/2addr v0, v3

    .line 1239
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->n(I)I

    move-result v3

    .line 1240
    if-ne v2, v3, :cond_5

    move v0, v2

    .line 1241
    goto :goto_1

    .line 1229
    :cond_4
    if-eqz v3, :cond_3

    move v0, v2

    .line 1230
    goto :goto_1

    .line 1243
    :cond_5
    add-int v2, v1, v3

    .line 1244
    add-int/2addr v0, v3

    .line 1245
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_1

    .line 1246
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move v3, v0

    move v1, p1

    goto :goto_0
.end method

.method private K(I)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v2, -0x1

    .line 1254
    .line 1257
    const-string v1, "ENCODING"

    invoke-virtual {p0, p1, v1, v4}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 1258
    if-eq v2, v3, :cond_6

    .line 1259
    add-int v1, p1, v3

    .line 1260
    add-int/2addr v0, v3

    move v3, v4

    .line 1263
    :goto_0
    if-eqz v3, :cond_0

    iget-object v5, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v5, :cond_0

    .line 1264
    iget-object v5, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v6, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v6, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1267
    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v5

    .line 1268
    add-int/2addr v1, v5

    .line 1269
    add-int/2addr v0, v5

    .line 1271
    const-string v5, "="

    invoke-virtual {p0, v1, v5, v4}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v4

    .line 1272
    if-eq v2, v4, :cond_4

    .line 1273
    if-nez v3, :cond_2

    move v0, v2

    .line 1299
    :cond_1
    :goto_1
    return v0

    .line 1276
    :cond_2
    add-int/2addr v1, v4

    .line 1277
    add-int/2addr v0, v4

    .line 1284
    :cond_3
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 1285
    add-int/2addr v1, v3

    .line 1286
    add-int/2addr v0, v3

    .line 1289
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->o(I)I

    move-result v3

    .line 1290
    if-ne v2, v3, :cond_5

    move v0, v2

    .line 1291
    goto :goto_1

    .line 1279
    :cond_4
    if-eqz v3, :cond_3

    move v0, v2

    .line 1280
    goto :goto_1

    .line 1293
    :cond_5
    add-int v2, v1, v3

    .line 1294
    add-int/2addr v0, v3

    .line 1295
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_1

    .line 1296
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move v3, v0

    move v1, p1

    goto :goto_0
.end method

.method private L(I)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    .line 1306
    const/4 v1, 0x0

    .line 1308
    const-string v2, "CHARSET"

    invoke-virtual {p0, p1, v2, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1309
    if-ne v0, v2, :cond_1

    .line 1344
    :cond_0
    :goto_0
    return v0

    .line 1312
    :cond_1
    add-int v3, p1, v2

    .line 1313
    add-int/2addr v1, v2

    .line 1314
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v2, :cond_2

    .line 1315
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v4, p1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1318
    :cond_2
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1319
    add-int/2addr v3, v2

    .line 1320
    add-int/2addr v1, v2

    .line 1322
    const-string v2, "="

    invoke-virtual {p0, v3, v2, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1323
    if-eq v0, v2, :cond_0

    .line 1326
    add-int/2addr v3, v2

    .line 1327
    add-int/2addr v1, v2

    .line 1329
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1330
    add-int/2addr v3, v2

    .line 1331
    add-int/2addr v1, v2

    .line 1334
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->p(I)I

    move-result v2

    .line 1335
    if-eq v0, v2, :cond_0

    .line 1338
    add-int v4, v3, v2

    .line 1339
    add-int v0, v1, v2

    .line 1340
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v1, :cond_0

    .line 1341
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private M(I)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    .line 1351
    const/4 v1, 0x0

    .line 1353
    const-string v2, "LANGUAGE"

    invoke-virtual {p0, p1, v2, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1354
    if-ne v0, v2, :cond_1

    .line 1389
    :cond_0
    :goto_0
    return v0

    .line 1357
    :cond_1
    add-int v3, p1, v2

    .line 1358
    add-int/2addr v1, v2

    .line 1359
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v2, :cond_2

    .line 1360
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v4, p1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1363
    :cond_2
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1364
    add-int/2addr v3, v2

    .line 1365
    add-int/2addr v1, v2

    .line 1367
    const-string v2, "="

    invoke-virtual {p0, v3, v2, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1368
    if-eq v0, v2, :cond_0

    .line 1371
    add-int/2addr v3, v2

    .line 1372
    add-int/2addr v1, v2

    .line 1374
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1375
    add-int/2addr v3, v2

    .line 1376
    add-int/2addr v1, v2

    .line 1379
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->q(I)I

    move-result v2

    .line 1380
    if-eq v0, v2, :cond_0

    .line 1383
    add-int v4, v3, v2

    .line 1384
    add-int v0, v1, v2

    .line 1385
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v1, :cond_0

    .line 1386
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private N(I)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    .line 1396
    const/4 v1, 0x0

    .line 1398
    const-string v2, "ROLE"

    invoke-virtual {p0, p1, v2, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1399
    if-ne v0, v2, :cond_1

    .line 1434
    :cond_0
    :goto_0
    return v0

    .line 1402
    :cond_1
    add-int v3, p1, v2

    .line 1403
    add-int/2addr v1, v2

    .line 1404
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v2, :cond_2

    .line 1405
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v4, p1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1408
    :cond_2
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1409
    add-int/2addr v3, v2

    .line 1410
    add-int/2addr v1, v2

    .line 1412
    const-string v2, "="

    invoke-virtual {p0, v3, v2, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1413
    if-eq v0, v2, :cond_0

    .line 1416
    add-int/2addr v3, v2

    .line 1417
    add-int/2addr v1, v2

    .line 1419
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1420
    add-int/2addr v3, v2

    .line 1421
    add-int/2addr v1, v2

    .line 1424
    invoke-direct {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->S(I)I

    move-result v2

    .line 1425
    if-eq v0, v2, :cond_0

    .line 1428
    add-int v4, v3, v2

    .line 1429
    add-int v0, v1, v2

    .line 1430
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v1, :cond_0

    .line 1431
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private O(I)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    .line 1441
    const/4 v1, 0x0

    .line 1443
    const-string v2, "STATUS"

    invoke-virtual {p0, p1, v2, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1444
    if-ne v0, v2, :cond_1

    .line 1479
    :cond_0
    :goto_0
    return v0

    .line 1447
    :cond_1
    add-int v3, p1, v2

    .line 1448
    add-int/2addr v1, v2

    .line 1449
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v2, :cond_2

    .line 1450
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v4, p1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1453
    :cond_2
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1454
    add-int/2addr v3, v2

    .line 1455
    add-int/2addr v1, v2

    .line 1457
    const-string v2, "="

    invoke-virtual {p0, v3, v2, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1458
    if-eq v0, v2, :cond_0

    .line 1461
    add-int/2addr v3, v2

    .line 1462
    add-int/2addr v1, v2

    .line 1464
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1465
    add-int/2addr v3, v2

    .line 1466
    add-int/2addr v1, v2

    .line 1469
    invoke-direct {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->T(I)I

    move-result v2

    .line 1470
    if-eq v0, v2, :cond_0

    .line 1473
    add-int v4, v3, v2

    .line 1474
    add-int v0, v1, v2

    .line 1475
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v1, :cond_0

    .line 1476
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private P(I)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 1487
    const/4 v1, 0x0

    .line 1489
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->e(I)I

    move-result v2

    .line 1490
    if-ne v0, v2, :cond_1

    .line 1525
    :cond_0
    :goto_0
    return v0

    .line 1493
    :cond_1
    add-int v3, p1, v2

    .line 1494
    add-int/2addr v1, v2

    .line 1495
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v2, :cond_2

    .line 1496
    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v4, p1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/android/calendar/vcal/a/b;->c(Ljava/lang/String;)V

    .line 1499
    :cond_2
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1500
    add-int/2addr v3, v2

    .line 1501
    add-int/2addr v1, v2

    .line 1503
    const-string v2, "="

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v2, v4}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v2

    .line 1504
    if-eq v0, v2, :cond_0

    .line 1507
    add-int/2addr v3, v2

    .line 1508
    add-int/2addr v1, v2

    .line 1510
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v2

    .line 1511
    add-int/2addr v3, v2

    .line 1512
    add-int/2addr v1, v2

    .line 1515
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->l(I)I

    move-result v2

    .line 1516
    if-eq v0, v2, :cond_0

    .line 1519
    add-int v4, v3, v2

    .line 1520
    add-int v0, v1, v2

    .line 1521
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v1, :cond_0

    .line 1522
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    iget-object v2, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/calendar/vcal/a/b;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private Q(I)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 1533
    .line 1535
    const-string v0, "WAVE"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1536
    if-eq v1, v0, :cond_1

    .line 1570
    :cond_0
    :goto_0
    return v0

    .line 1540
    :cond_1
    const-string v0, "PCM"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1541
    if-ne v1, v0, :cond_0

    .line 1545
    const-string v0, "VCARD"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1546
    if-ne v1, v0, :cond_0

    .line 1550
    const-string v0, "JPEG"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1551
    if-ne v1, v0, :cond_0

    .line 1555
    const-string v0, "IMAGE/JPEG"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1556
    if-ne v1, v0, :cond_0

    .line 1560
    const-string v0, "IMAGE/GIF"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1561
    if-ne v1, v0, :cond_0

    .line 1565
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->e(I)I

    move-result v0

    .line 1566
    if-ne v1, v0, :cond_0

    move v0, v1

    .line 1570
    goto :goto_0
.end method

.method private R(I)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1577
    .line 1579
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->Q(I)I

    move-result v0

    .line 1580
    if-eq v1, v0, :cond_1

    .line 1589
    :cond_0
    :goto_0
    return v0

    .line 1584
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->e(I)I

    move-result v0

    .line 1585
    if-ne v1, v0, :cond_0

    move v0, v1

    .line 1589
    goto :goto_0
.end method

.method private S(I)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 1596
    .line 1598
    const-string v0, "ATTENDEE"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1599
    if-eq v1, v0, :cond_1

    .line 1618
    :cond_0
    :goto_0
    return v0

    .line 1603
    :cond_1
    const-string v0, "ORGANIZER"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1604
    if-ne v1, v0, :cond_0

    .line 1608
    const-string v0, "OWNER"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1609
    if-ne v1, v0, :cond_0

    .line 1613
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->e(I)I

    move-result v0

    .line 1614
    if-ne v1, v0, :cond_0

    move v0, v1

    .line 1618
    goto :goto_0
.end method

.method private T(I)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 1626
    .line 1628
    const-string v0, "ACCEPTED"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1629
    if-eq v1, v0, :cond_1

    .line 1664
    :cond_0
    :goto_0
    return v0

    .line 1633
    :cond_1
    const-string v0, "NEED ACTION"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1634
    if-ne v1, v0, :cond_0

    .line 1638
    const-string v0, "TENTATIVE"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1639
    if-ne v1, v0, :cond_0

    .line 1642
    const-string v0, "CONFIRMED"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1643
    if-ne v1, v0, :cond_0

    .line 1646
    const-string v0, "DECLINED"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1647
    if-ne v1, v0, :cond_0

    .line 1650
    const-string v0, "COMPLETED"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1651
    if-ne v1, v0, :cond_0

    .line 1654
    const-string v0, "DELEGATED"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 1655
    if-ne v1, v0, :cond_0

    .line 1659
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->e(I)I

    move-result v0

    .line 1660
    if-ne v1, v0, :cond_0

    move v0, v1

    .line 1664
    goto :goto_0
.end method

.method private a(ILjava/lang/String;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    .line 454
    const/4 v0, 0x0

    .line 456
    invoke-virtual {p0, p1, p2, v4}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 457
    if-ne v2, v3, :cond_1

    move v0, v2

    .line 505
    :cond_0
    :goto_0
    return v0

    .line 460
    :cond_1
    add-int v1, p1, v3

    .line 461
    add-int/2addr v0, v3

    .line 462
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_2

    .line 463
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3, p2}, Lcom/android/calendar/vcal/a/b;->b(Ljava/lang/String;)V

    .line 466
    :cond_2
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->D(I)I

    move-result v3

    .line 467
    if-eq v2, v3, :cond_3

    .line 468
    add-int/2addr v1, v3

    .line 469
    add-int/2addr v0, v3

    .line 472
    :cond_3
    const-string v3, ":"

    invoke-virtual {p0, v1, v3, v4}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 473
    if-ne v2, v3, :cond_4

    move v0, v2

    .line 474
    goto :goto_0

    .line 476
    :cond_4
    add-int/2addr v1, v3

    .line 477
    add-int/2addr v0, v3

    .line 480
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->f(I)I

    move-result v3

    .line 481
    if-ne v2, v3, :cond_5

    move v0, v2

    .line 482
    goto :goto_0

    .line 484
    :cond_5
    add-int v4, v1, v3

    .line 485
    add-int/2addr v0, v3

    .line 486
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_6

    .line 487
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 488
    iget-object v5, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v5, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    iget-object v5, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v5, v3}, Lcom/android/calendar/vcal/a/b;->a(Ljava/util/List;)V

    .line 492
    :cond_6
    invoke-virtual {p0, v4}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    .line 493
    if-ne v2, v3, :cond_7

    move v0, v2

    .line 494
    goto :goto_0

    .line 496
    :cond_7
    add-int/2addr v0, v3

    .line 498
    const-string v3, "BEGIN"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 499
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v3, "VCALENDAR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 500
    iget v1, p0, Lcom/android/calendar/vcal/a/a/k;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/calendar/vcal/a/a/k;->m:I

    goto :goto_0

    :cond_8
    move v0, v2

    .line 502
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1673
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 1686
    :goto_0
    return v0

    .line 1677
    :cond_1
    sget-object v0, Lcom/android/calendar/vcal/a/a/k;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1678
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1679
    sget-object v1, Lcom/android/calendar/vcal/a/a/k;->l:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1680
    const-string v1, "X-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1681
    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1686
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1707
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 1708
    :cond_0
    const/4 p1, 0x0

    .line 1720
    :cond_1
    :goto_0
    return-object p1

    .line 1709
    :cond_2
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 1710
    :cond_3
    const-string p1, ""

    goto :goto_0

    .line 1712
    :cond_4
    sget-object v0, Lcom/android/calendar/vcal/a/a/k;->k:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1715
    const-string v0, "\\\\"

    const-string v1, "\n\r\n"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1716
    const-string v1, "\\;"

    const-string v2, ";"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1717
    const-string v1, "\\:"

    const-string v2, ":"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1718
    const-string v1, "\\,"

    const-string v2, ","

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1719
    const-string v1, "\n\r\n"

    const-string v2, "\\"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private s(I)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 108
    const/4 v0, 0x0

    .line 111
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->c(I)I

    move-result v1

    if-eq v2, v1, :cond_0

    .line 112
    add-int/2addr p1, v1

    .line 113
    add-int/2addr v0, v1

    goto :goto_0

    .line 116
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->t(I)I

    move-result v3

    .line 117
    if-eq v2, v3, :cond_1

    .line 118
    add-int v1, p1, v3

    .line 119
    add-int/2addr v0, v3

    .line 125
    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->c(I)I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 126
    add-int/2addr v1, v3

    .line 127
    add-int/2addr v0, v3

    goto :goto_1

    :cond_1
    move v0, v2

    .line 129
    :cond_2
    return v0
.end method

.method private t(I)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 137
    .line 140
    const-string v0, "BEGIN"

    invoke-virtual {p0, p1, v0, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 141
    if-ne v4, v0, :cond_1

    move v0, v4

    .line 290
    :cond_0
    :goto_0
    return v0

    .line 144
    :cond_1
    add-int v1, p1, v0

    .line 145
    add-int/2addr v0, v2

    .line 148
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 149
    add-int/2addr v1, v3

    .line 150
    add-int/2addr v0, v3

    .line 153
    const-string v3, ":"

    invoke-virtual {p0, v1, v3, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 154
    if-ne v4, v3, :cond_2

    move v0, v4

    .line 155
    goto :goto_0

    .line 157
    :cond_2
    add-int/2addr v1, v3

    .line 158
    add-int/2addr v0, v3

    .line 161
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 162
    add-int/2addr v1, v3

    .line 163
    add-int/2addr v0, v3

    .line 166
    const-string v3, "VCALENDAR"

    invoke-virtual {p0, v1, v3, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 167
    if-ne v4, v3, :cond_3

    move v0, v4

    .line 168
    goto :goto_0

    .line 170
    :cond_3
    add-int/2addr v1, v3

    .line 171
    add-int/2addr v0, v3

    .line 172
    iget v3, p0, Lcom/android/calendar/vcal/a/a/k;->m:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/calendar/vcal/a/a/k;->m:I

    .line 173
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_4

    .line 174
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    const-string v5, "VCALENDAR"

    invoke-interface {v3, v5}, Lcom/android/calendar/vcal/a/b;->a(Ljava/lang/String;)V

    .line 178
    :cond_4
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 179
    add-int/2addr v1, v3

    .line 180
    add-int/2addr v0, v3

    .line 183
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    .line 184
    if-ne v4, v3, :cond_5

    move v0, v4

    .line 185
    goto :goto_0

    .line 187
    :cond_5
    add-int/2addr v1, v3

    .line 188
    add-int/2addr v0, v3

    .line 189
    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v4, v3, :cond_6

    .line 190
    add-int/2addr v1, v3

    .line 191
    add-int/2addr v0, v3

    goto :goto_1

    .line 195
    :cond_6
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->u(I)I

    move-result v3

    .line 196
    if-ne v4, v3, :cond_7

    move v0, v4

    .line 197
    goto :goto_0

    .line 199
    :cond_7
    add-int/2addr v1, v3

    .line 200
    add-int/2addr v0, v3

    .line 203
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->v(I)I

    move-result v3

    .line 204
    if-ne v4, v3, :cond_8

    move v0, v4

    .line 205
    goto :goto_0

    .line 207
    :cond_8
    add-int/2addr v1, v3

    .line 208
    add-int/2addr v0, v3

    .line 211
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 212
    add-int/2addr v1, v3

    .line 213
    add-int/2addr v0, v3

    .line 216
    :goto_2
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v4, v3, :cond_9

    .line 217
    add-int/2addr v1, v3

    .line 218
    add-int/2addr v0, v3

    goto :goto_2

    :cond_9
    move v3, v1

    move v1, v0

    move v0, v2

    .line 221
    :goto_3
    iget v5, p0, Lcom/android/calendar/vcal/a/a/k;->m:I

    if-ge v0, v5, :cond_f

    .line 224
    const-string v5, "END"

    invoke-virtual {p0, v3, v5, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v5

    .line 225
    if-ne v4, v5, :cond_a

    move v0, v4

    .line 226
    goto/16 :goto_0

    .line 228
    :cond_a
    add-int/2addr v3, v5

    .line 229
    add-int/2addr v1, v5

    .line 232
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v5

    .line 233
    add-int/2addr v3, v5

    .line 234
    add-int/2addr v1, v5

    .line 238
    const-string v5, ":"

    invoke-virtual {p0, v3, v5, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v5

    .line 239
    if-ne v4, v5, :cond_b

    move v0, v4

    .line 240
    goto/16 :goto_0

    .line 242
    :cond_b
    add-int/2addr v3, v5

    .line 243
    add-int/2addr v1, v5

    .line 246
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v5

    .line 247
    add-int/2addr v3, v5

    .line 248
    add-int/2addr v1, v5

    .line 251
    const-string v5, "VCALENDAR"

    invoke-virtual {p0, v3, v5, v2}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v5

    .line 252
    if-ne v4, v5, :cond_c

    move v0, v4

    .line 253
    goto/16 :goto_0

    .line 255
    :cond_c
    add-int/2addr v3, v5

    .line 256
    add-int/2addr v1, v5

    .line 257
    iget-object v5, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v5, :cond_d

    .line 258
    iget-object v5, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v5}, Lcom/android/calendar/vcal/a/b;->c()V

    .line 262
    :cond_d
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v5

    .line 263
    add-int/2addr v3, v5

    .line 264
    add-int/2addr v1, v5

    .line 266
    iget v5, p0, Lcom/android/calendar/vcal/a/a/k;->m:I

    add-int/lit8 v5, v5, -0x1

    if-ge v0, v5, :cond_e

    .line 268
    :goto_4
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v5

    if-eq v4, v5, :cond_e

    .line 269
    add-int/2addr v3, v5

    .line 270
    add-int/2addr v1, v5

    goto :goto_4

    .line 221
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 277
    :cond_f
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v0

    .line 278
    if-ne v4, v0, :cond_10

    .line 279
    add-int/lit8 v3, v3, 0x2

    .line 280
    add-int/lit8 v1, v1, 0x2

    .line 283
    :cond_10
    add-int v2, v3, v0

    .line 284
    add-int/2addr v0, v1

    move v1, v2

    .line 285
    :goto_5
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v2

    if-eq v4, v2, :cond_0

    .line 286
    add-int/2addr v1, v2

    .line 287
    add-int/2addr v0, v2

    goto :goto_5
.end method

.method private u(I)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 297
    const/4 v0, 0x0

    .line 299
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v1, :cond_0

    .line 300
    iget-object v1, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v1}, Lcom/android/calendar/vcal/a/b;->d()V

    .line 302
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->w(I)I

    move-result v3

    .line 303
    if-ne v2, v3, :cond_2

    move v0, v2

    .line 333
    :cond_1
    return v0

    .line 306
    :cond_2
    add-int v1, p1, v3

    .line 307
    add-int/2addr v0, v3

    .line 308
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_3

    .line 309
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3}, Lcom/android/calendar/vcal/a/b;->e()V

    .line 314
    :cond_3
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_4

    .line 315
    add-int/2addr v1, v3

    .line 316
    add-int/2addr v0, v3

    goto :goto_0

    .line 319
    :cond_4
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_5

    .line 320
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3}, Lcom/android/calendar/vcal/a/b;->d()V

    .line 322
    :cond_5
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->w(I)I

    move-result v3

    .line 323
    if-eq v2, v3, :cond_1

    .line 326
    add-int/2addr v1, v3

    .line 327
    add-int/2addr v0, v3

    .line 328
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_3

    .line 329
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3}, Lcom/android/calendar/vcal/a/b;->e()V

    goto :goto_0
.end method

.method private v(I)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 340
    const/4 v0, 0x0

    .line 342
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->x(I)I

    move-result v3

    .line 343
    if-ne v2, v3, :cond_1

    move v0, v2

    .line 364
    :cond_0
    return v0

    .line 346
    :cond_1
    add-int v1, p1, v3

    .line 347
    add-int/2addr v0, v3

    .line 351
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 352
    add-int/2addr v1, v3

    .line 353
    add-int/2addr v0, v3

    goto :goto_0

    .line 356
    :cond_2
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->x(I)I

    move-result v3

    .line 357
    if-eq v2, v3, :cond_0

    .line 360
    add-int/2addr v1, v3

    .line 361
    add-int/2addr v0, v3

    goto :goto_0
.end method

.method private w(I)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 371
    .line 373
    const-string v0, "DAYLIGHT"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 374
    if-eq v1, v0, :cond_1

    .line 428
    :cond_0
    :goto_0
    return v0

    .line 378
    :cond_1
    const-string v0, "GEO"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 379
    if-ne v1, v0, :cond_0

    .line 383
    const-string v0, "PRODID"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 384
    if-ne v1, v0, :cond_0

    .line 388
    const-string v0, "TZ"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 389
    if-ne v1, v0, :cond_0

    .line 392
    const-string v0, "X-SD-VERN"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 393
    if-ne v1, v0, :cond_0

    .line 396
    const-string v0, "X-SD-FORMAT_VER"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 397
    if-ne v1, v0, :cond_0

    .line 400
    const-string v0, "X-SD-CATEGORIES"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 401
    if-ne v1, v0, :cond_0

    .line 404
    const-string v0, "X-SD-CLASS"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 405
    if-ne v1, v0, :cond_0

    .line 408
    const-string v0, "X-SD-DCREATED"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 409
    if-ne v1, v0, :cond_0

    .line 412
    const-string v0, "X-SD-CHAR_CODE"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 413
    if-ne v1, v0, :cond_0

    .line 416
    const-string v0, "X-SD-DESCRIPTION"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 417
    if-ne v1, v0, :cond_0

    .line 420
    const-string v0, "BEGIN"

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;)I

    move-result v0

    .line 421
    if-ne v1, v0, :cond_0

    .line 424
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->y(I)I

    move-result v0

    .line 425
    if-ne v1, v0, :cond_0

    move v0, v1

    .line 428
    goto :goto_0
.end method

.method private x(I)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 435
    .line 437
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->z(I)I

    move-result v0

    .line 438
    if-eq v1, v0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return v0

    .line 442
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->A(I)I

    move-result v0

    .line 443
    if-ne v1, v0, :cond_0

    move v0, v1

    .line 446
    goto :goto_0
.end method

.method private y(I)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, -0x1

    .line 512
    const/4 v0, 0x0

    .line 514
    const-string v1, "VERSION"

    invoke-virtual {p0, p1, v1, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 515
    if-ne v2, v3, :cond_0

    move v0, v2

    .line 554
    :goto_0
    return v0

    .line 518
    :cond_0
    add-int v1, p1, v3

    .line 519
    add-int/2addr v0, v3

    .line 520
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_1

    .line 521
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    const-string v4, "VERSION"

    invoke-interface {v3, v4}, Lcom/android/calendar/vcal/a/b;->b(Ljava/lang/String;)V

    .line 524
    :cond_1
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->D(I)I

    move-result v3

    .line 525
    if-eq v2, v3, :cond_2

    .line 526
    add-int/2addr v1, v3

    .line 527
    add-int/2addr v0, v3

    .line 530
    :cond_2
    const-string v3, ":"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 531
    if-ne v2, v3, :cond_3

    move v0, v2

    .line 532
    goto :goto_0

    .line 534
    :cond_3
    add-int/2addr v1, v3

    .line 535
    add-int/2addr v0, v3

    .line 537
    const-string v3, "1.0"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 538
    if-ne v2, v3, :cond_4

    move v0, v2

    .line 539
    goto :goto_0

    .line 541
    :cond_4
    add-int/2addr v1, v3

    .line 542
    add-int/2addr v0, v3

    .line 543
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_5

    .line 544
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 545
    const-string v4, "1.0"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    iget-object v4, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v4, v3}, Lcom/android/calendar/vcal/a/b;->a(Ljava/util/List;)V

    .line 548
    :cond_5
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v1

    .line 549
    if-ne v2, v1, :cond_6

    move v0, v2

    .line 550
    goto :goto_0

    .line 552
    :cond_6
    add-int/2addr v0, v1

    .line 554
    goto :goto_0
.end method

.method private z(I)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 562
    .line 564
    const-string v0, "BEGIN"

    invoke-virtual {p0, p1, v0, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v0

    .line 565
    if-ne v2, v0, :cond_1

    move v0, v2

    .line 689
    :cond_0
    :goto_0
    return v0

    .line 568
    :cond_1
    add-int v1, p1, v0

    .line 569
    add-int/2addr v0, v5

    .line 572
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 573
    add-int/2addr v1, v3

    .line 574
    add-int/2addr v0, v3

    .line 577
    const-string v3, ":"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 578
    if-ne v2, v3, :cond_2

    move v0, v2

    .line 579
    goto :goto_0

    .line 581
    :cond_2
    add-int/2addr v1, v3

    .line 582
    add-int/2addr v0, v3

    .line 585
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 586
    add-int/2addr v1, v3

    .line 587
    add-int/2addr v0, v3

    .line 590
    const-string v3, "VEVENT"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 591
    if-ne v2, v3, :cond_3

    move v0, v2

    .line 592
    goto :goto_0

    .line 594
    :cond_3
    add-int/2addr v1, v3

    .line 595
    add-int/2addr v0, v3

    .line 596
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_4

    .line 597
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    const-string v4, "VEVENT"

    invoke-interface {v3, v4}, Lcom/android/calendar/vcal/a/b;->a(Ljava/lang/String;)V

    .line 601
    :cond_4
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 602
    add-int/2addr v1, v3

    .line 603
    add-int/2addr v0, v3

    .line 606
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    .line 607
    if-ne v2, v3, :cond_5

    move v0, v2

    .line 608
    goto :goto_0

    .line 610
    :cond_5
    add-int/2addr v1, v3

    .line 611
    add-int/2addr v0, v3

    .line 612
    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_6

    .line 613
    add-int/2addr v1, v3

    .line 614
    add-int/2addr v0, v3

    goto :goto_1

    .line 617
    :cond_6
    invoke-direct {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->B(I)I

    move-result v3

    .line 618
    if-ne v2, v3, :cond_7

    move v0, v2

    .line 619
    goto :goto_0

    .line 621
    :cond_7
    add-int/2addr v1, v3

    .line 622
    add-int/2addr v0, v3

    .line 625
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 626
    add-int/2addr v1, v3

    .line 627
    add-int/2addr v0, v3

    .line 630
    :goto_2
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_8

    .line 631
    add-int/2addr v1, v3

    .line 632
    add-int/2addr v0, v3

    goto :goto_2

    .line 636
    :cond_8
    const-string v3, "END"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 637
    if-ne v2, v3, :cond_9

    move v0, v2

    .line 638
    goto :goto_0

    .line 640
    :cond_9
    add-int/2addr v1, v3

    .line 641
    add-int/2addr v0, v3

    .line 644
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 645
    add-int/2addr v1, v3

    .line 646
    add-int/2addr v0, v3

    .line 649
    const-string v3, ":"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 650
    if-ne v2, v3, :cond_a

    move v0, v2

    .line 651
    goto :goto_0

    .line 653
    :cond_a
    add-int/2addr v1, v3

    .line 654
    add-int/2addr v0, v3

    .line 657
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 658
    add-int/2addr v1, v3

    .line 659
    add-int/2addr v0, v3

    .line 662
    const-string v3, "VEVENT"

    invoke-virtual {p0, v1, v3, v5}, Lcom/android/calendar/vcal/a/a/k;->a(ILjava/lang/String;Z)I

    move-result v3

    .line 663
    if-ne v2, v3, :cond_b

    move v0, v2

    .line 664
    goto/16 :goto_0

    .line 666
    :cond_b
    add-int/2addr v1, v3

    .line 667
    add-int/2addr v0, v3

    .line 668
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    if-eqz v3, :cond_c

    .line 669
    iget-object v3, p0, Lcom/android/calendar/vcal/a/a/k;->b:Lcom/android/calendar/vcal/a/b;

    invoke-interface {v3}, Lcom/android/calendar/vcal/a/b;->c()V

    .line 673
    :cond_c
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->d(I)I

    move-result v3

    .line 674
    add-int/2addr v1, v3

    .line 675
    add-int/2addr v0, v3

    .line 678
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    .line 679
    if-ne v2, v3, :cond_d

    move v0, v2

    .line 680
    goto/16 :goto_0

    .line 682
    :cond_d
    add-int/2addr v1, v3

    .line 683
    add-int/2addr v0, v3

    .line 684
    :goto_3
    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/a/a/k;->a(I)I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 685
    add-int/2addr v1, v3

    .line 686
    add-int/2addr v0, v3

    goto :goto_3
.end method


# virtual methods
.method protected b(I)I
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/android/calendar/vcal/a/a/k;->s(I)I

    move-result v0

    return v0
.end method
