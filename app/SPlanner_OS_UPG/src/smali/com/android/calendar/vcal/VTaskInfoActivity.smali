.class public Lcom/android/calendar/vcal/VTaskInfoActivity;
.super Landroid/app/Activity;
.source "VTaskInfoActivity.java"


# static fields
.field private static h:I


# instance fields
.field public a:Ljava/util/ArrayList;

.field private b:Lcom/android/calendar/vcal/u;

.field private c:Landroid/content/ContentValues;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Landroid/database/Cursor;

.field private g:Lcom/android/calendar/vcal/ae;

.field private i:I

.field private j:I

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/vcal/VTaskInfoActivity;->h:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 53
    iput v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->d:I

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->e:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->a:Ljava/util/ArrayList;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->g:Lcom/android/calendar/vcal/ae;

    .line 61
    iput v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->i:I

    .line 62
    iput v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->j:I

    .line 439
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/android/calendar/vcal/VTaskInfoActivity;->h:I

    return v0
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 40
    sput p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->h:I

    return p0
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VTaskInfoActivity;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->i:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VTaskInfoActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/vcal/VTaskInfoActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->i()V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/vcal/VTaskInfoActivity;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->j:I

    return p1
.end method

.method private b()V
    .locals 1

    .prologue
    .line 131
    const v0, 0x7f0400be

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->setContentView(I)V

    .line 134
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->d()V

    .line 137
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->e()V

    .line 140
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->f()V

    .line 143
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->g()V

    .line 146
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->h()V

    .line 148
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->c()V

    .line 149
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/vcal/VTaskInfoActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->k()V

    return-void
.end method

.method static synthetic c(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->f:Landroid/database/Cursor;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const v2, 0x7f0f0242

    .line 152
    const v0, 0x7f12030f

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 153
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    const v0, 0x7f120310

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 155
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 156
    new-instance v1, Lcom/android/calendar/vcal/z;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/z;-><init>(Lcom/android/calendar/vcal/VTaskInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    const v0, 0x7f120311

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 166
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0163

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 167
    new-instance v1, Lcom/android/calendar/vcal/aa;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/aa;-><init>(Lcom/android/calendar/vcal/VTaskInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    return-void
.end method

.method static synthetic d(Lcom/android/calendar/vcal/VTaskInfoActivity;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->i:I

    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 177
    const v0, 0x7f120165

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 178
    iget-object v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->b:Lcom/android/calendar/vcal/u;

    iget v2, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->d:I

    invoke-virtual {v1, v2}, Lcom/android/calendar/vcal/u;->f(I)Ljava/lang/String;

    move-result-object v1

    .line 179
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 181
    const v2, 0x7f0f02bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 183
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 187
    const v0, 0x7f1202d4

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 188
    iget-object v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->b:Lcom/android/calendar/vcal/u;

    iget v2, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->d:I

    invoke-virtual {v1, v2}, Lcom/android/calendar/vcal/u;->g(I)I

    move-result v1

    .line 190
    packed-switch v1, :pswitch_data_0

    .line 206
    :goto_0
    return-void

    .line 192
    :pswitch_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 193
    const v1, 0x7f02017a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 197
    :pswitch_1
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 200
    :pswitch_2
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 201
    const v1, 0x7f020179

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic f(Lcom/android/calendar/vcal/VTaskInfoActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const v3, 0x7f120313

    const/4 v2, 0x0

    .line 209
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const v0, 0x7f120087

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->l:Landroid/widget/ImageView;

    .line 211
    const v0, 0x7f120314

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->k:Landroid/widget/TextView;

    .line 212
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->l:Landroid/widget/ImageView;

    invoke-static {p0, v2}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 213
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->e:Ljava/lang/String;

    .line 214
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    sput v2, Lcom/android/calendar/vcal/VTaskInfoActivity;->h:I

    .line 218
    iput v2, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->j:I

    iput v2, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->i:I

    .line 223
    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/calendar/vcal/ab;

    invoke-direct {v1, p0}, Lcom/android/calendar/vcal/ab;-><init>(Lcom/android/calendar/vcal/VTaskInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    return-void

    .line 220
    :cond_0
    invoke-virtual {p0, v3}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/calendar/vcal/VTaskInfoActivity;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->j:I

    return v0
.end method

.method private g()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 233
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->b:Lcom/android/calendar/vcal/u;

    iget v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->d:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/vcal/u;->h(I)Ljava/lang/Long;

    move-result-object v1

    .line 234
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->b:Lcom/android/calendar/vcal/u;

    iget v2, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->d:I

    invoke-virtual {v0, v2}, Lcom/android/calendar/vcal/u;->i(I)Ljava/lang/Long;

    move-result-object v2

    .line 239
    const v0, 0x7f120155

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 240
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f017f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 241
    if-nez v2, :cond_1

    .line 242
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f02d1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    :goto_0
    const v0, 0x7f120316

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 252
    const v0, 0x7f1200ee

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 253
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0400

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 254
    if-nez v1, :cond_2

    .line 255
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 265
    :goto_1
    if-eqz v2, :cond_0

    .line 266
    const v0, 0x7f1202d8

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 267
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 268
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 269
    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 270
    iget-wide v6, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 271
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 272
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-wide v6, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 274
    if-lt v1, v3, :cond_3

    .line 275
    sub-int/2addr v1, v3

    .line 276
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0120

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 277
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 278
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    :cond_0
    :goto_2
    return-void

    .line 244
    :cond_1
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 245
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 246
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 247
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, p0}, Lcom/android/calendar/vcal/x;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 258
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7, p0}, Lcom/android/calendar/vcal/x;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 281
    :cond_3
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method static synthetic h(Lcom/android/calendar/vcal/VTaskInfoActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->l:Landroid/widget/ImageView;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 287
    const v0, 0x7f12015e

    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 288
    const v1, 0x7f120317

    invoke-virtual {p0, v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 289
    iget-object v2, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->b:Lcom/android/calendar/vcal/u;

    iget v3, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->d:I

    invoke-virtual {v2, v3}, Lcom/android/calendar/vcal/u;->j(I)Ljava/lang/String;

    move-result-object v2

    .line 290
    if-eqz v2, :cond_0

    .line 291
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 294
    :cond_0
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 298
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->b:Lcom/android/calendar/vcal/u;

    iget v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->i:I

    iget-object v2, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/vcal/u;->a(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0410

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 303
    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 304
    return-void

    .line 301
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f01e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private j()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 307
    const/4 v6, 0x1

    .line 308
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 311
    if-eqz v1, :cond_2

    .line 313
    :try_start_0
    invoke-static {v1}, Lcom/android/calendar/task/ab;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->a:Ljava/util/ArrayList;

    .line 314
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 315
    invoke-static {v1}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 317
    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->f:Landroid/database/Cursor;

    .line 319
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 320
    :cond_0
    const-string v0, "VTaskInfoActivity"

    const-string v2, " no calendar account found "

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    const/4 v0, 0x0

    .line 327
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 331
    :goto_1
    return v0

    .line 323
    :cond_1
    :try_start_1
    new-instance v0, Lcom/android/calendar/vcal/ae;

    iget-object v2, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->f:Landroid/database/Cursor;

    invoke-direct {v0, p0, v2}, Lcom/android/calendar/vcal/ae;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->g:Lcom/android/calendar/vcal/ae;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v6

    goto :goto_0

    .line 327
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move v0, v6

    goto :goto_1
.end method

.method private k()V
    .locals 4

    .prologue
    .line 336
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 337
    const v1, 0x7f0f0428

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 338
    iget-object v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->g:Lcom/android/calendar/vcal/ae;

    sget v2, Lcom/android/calendar/vcal/VTaskInfoActivity;->h:I

    new-instance v3, Lcom/android/calendar/vcal/ac;

    invoke-direct {v3, p0}, Lcom/android/calendar/vcal/ac;-><init>(Lcom/android/calendar/vcal/VTaskInfoActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 374
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/vcal/ad;

    invoke-direct {v2, p0}, Lcom/android/calendar/vcal/ad;-><init>(Lcom/android/calendar/vcal/VTaskInfoActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 381
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 382
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 384
    :cond_0
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 435
    new-instance v0, Lcom/android/calendar/vcal/af;

    invoke-direct {v0, p0}, Lcom/android/calendar/vcal/af;-><init>(Lcom/android/calendar/vcal/VTaskInfoActivity;)V

    .line 436
    invoke-virtual {v0}, Lcom/android/calendar/vcal/af;->start()V

    .line 437
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 127
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 73
    const-string v1, "VCAL_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->c:Landroid/content/ContentValues;

    .line 76
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->c:Landroid/content/ContentValues;

    if-nez v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 79
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->finish()V

    .line 87
    :goto_0
    return-void

    .line 82
    :cond_0
    new-instance v0, Lcom/android/calendar/vcal/u;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->b:Lcom/android/calendar/vcal/u;

    .line 83
    iget-object v0, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->b:Lcom/android/calendar/vcal/u;

    iget-object v1, p0, Lcom/android/calendar/vcal/VTaskInfoActivity;->c:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Lcom/android/calendar/vcal/u;->a(Landroid/content/ContentValues;)V

    .line 85
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->b()V

    .line 86
    invoke-direct {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->l()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 101
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 102
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 91
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 96
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 93
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->finish()V

    .line 94
    const/4 v0, 0x1

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 114
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 115
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 109
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 122
    return-void
.end method
