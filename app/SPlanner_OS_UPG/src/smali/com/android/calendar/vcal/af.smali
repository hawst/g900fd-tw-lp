.class Lcom/android/calendar/vcal/af;
.super Ljava/lang/Thread;
.source "VTaskInfoActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/vcal/VTaskInfoActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/vcal/VTaskInfoActivity;)V
    .locals 1

    .prologue
    .line 440
    iput-object p1, p0, Lcom/android/calendar/vcal/af;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    .line 441
    const-class v0, Lcom/android/calendar/vcal/af;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 442
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 450
    const-string v0, ""

    .line 451
    iget-object v1, p0, Lcom/android/calendar/vcal/af;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-virtual {v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 454
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/vcal/af;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-virtual {v2}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 455
    if-eqz v2, :cond_0

    .line 456
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 460
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 461
    iget-object v0, p0, Lcom/android/calendar/vcal/af;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    const v1, 0x7f0f005e

    invoke-virtual {v0, v1}, Lcom/android/calendar/vcal/VTaskInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 463
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/vcal/af;->a:Lcom/android/calendar/vcal/VTaskInfoActivity;

    invoke-virtual {v1, v0}, Lcom/android/calendar/vcal/VTaskInfoActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 464
    return-void

    .line 458
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 0

    .prologue
    .line 446
    invoke-direct {p0}, Lcom/android/calendar/vcal/af;->a()V

    .line 447
    return-void
.end method
