.class public Lcom/android/calendar/vcal/a/a/e;
.super Ljava/lang/Object;
.source "Duration.java"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/vcal/a/a/e;->a:I

    .line 42
    return-void
.end method


# virtual methods
.method public a()J
    .locals 5

    .prologue
    .line 140
    iget v0, p0, Lcom/android/calendar/vcal/a/a/e;->a:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    .line 141
    const v2, 0x93a80

    iget v3, p0, Lcom/android/calendar/vcal/a/a/e;->b:I

    mul-int/2addr v2, v3

    const v3, 0x15180

    iget v4, p0, Lcom/android/calendar/vcal/a/a/e;->c:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/vcal/a/a/e;->d:I

    mul-int/lit16 v3, v3, 0xe10

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/vcal/a/a/e;->e:I

    mul-int/lit8 v3, v3, 0x3c

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/vcal/a/a/e;->f:I

    add-int/2addr v2, v3

    int-to-long v2, v2

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public a(J)J
    .locals 3

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/android/calendar/vcal/a/a/e;->a()J

    move-result-wide v0

    add-long/2addr v0, p1

    return-wide v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50
    iput v0, p0, Lcom/android/calendar/vcal/a/a/e;->a:I

    .line 51
    iput v1, p0, Lcom/android/calendar/vcal/a/a/e;->b:I

    .line 52
    iput v1, p0, Lcom/android/calendar/vcal/a/a/e;->c:I

    .line 53
    iput v1, p0, Lcom/android/calendar/vcal/a/a/e;->d:I

    .line 54
    iput v1, p0, Lcom/android/calendar/vcal/a/a/e;->e:I

    .line 55
    iput v1, p0, Lcom/android/calendar/vcal/a/a/e;->f:I

    .line 57
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 61
    if-ge v3, v0, :cond_1

    .line 121
    :cond_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 66
    const/16 v4, 0x2d

    if-ne v2, v4, :cond_3

    .line 67
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/calendar/vcal/a/a/e;->a:I

    .line 74
    :cond_2
    :goto_0
    if-lt v3, v0, :cond_0

    .line 78
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 79
    const/16 v4, 0x50

    if-eq v2, v4, :cond_4

    .line 80
    new-instance v1, Lcom/android/calendar/vcal/a/a/d;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duration.parse(str=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\') expected \'P\' at index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/android/calendar/vcal/a/a/d;-><init>(Ljava/lang/String;)V

    throw v1

    .line 70
    :cond_3
    const/16 v4, 0x2b

    if-eq v2, v4, :cond_2

    move v0, v1

    goto :goto_0

    .line 84
    :cond_4
    add-int/lit8 v0, v0, 0x1

    move v2, v0

    move v0, v1

    .line 87
    :goto_1
    if-ge v2, v3, :cond_0

    .line 88
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 89
    const/16 v5, 0x30

    if-lt v4, v5, :cond_6

    const/16 v5, 0x39

    if-gt v4, v5, :cond_6

    .line 90
    mul-int/lit8 v0, v0, 0xa

    .line 91
    add-int/lit8 v4, v4, -0x30

    add-int/2addr v0, v4

    .line 87
    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 93
    :cond_6
    const/16 v5, 0x57

    if-ne v4, v5, :cond_7

    .line 94
    iput v0, p0, Lcom/android/calendar/vcal/a/a/e;->b:I

    move v0, v1

    .line 95
    goto :goto_2

    .line 97
    :cond_7
    const/16 v5, 0x48

    if-ne v4, v5, :cond_8

    .line 98
    iput v0, p0, Lcom/android/calendar/vcal/a/a/e;->d:I

    move v0, v1

    .line 99
    goto :goto_2

    .line 101
    :cond_8
    const/16 v5, 0x4d

    if-ne v4, v5, :cond_9

    .line 102
    iput v0, p0, Lcom/android/calendar/vcal/a/a/e;->e:I

    move v0, v1

    .line 103
    goto :goto_2

    .line 105
    :cond_9
    const/16 v5, 0x53

    if-ne v4, v5, :cond_a

    .line 106
    iput v0, p0, Lcom/android/calendar/vcal/a/a/e;->f:I

    move v0, v1

    .line 107
    goto :goto_2

    .line 109
    :cond_a
    const/16 v5, 0x44

    if-ne v4, v5, :cond_b

    .line 110
    iput v0, p0, Lcom/android/calendar/vcal/a/a/e;->c:I

    move v0, v1

    .line 111
    goto :goto_2

    .line 113
    :cond_b
    const/16 v5, 0x54

    if-eq v4, v5, :cond_5

    .line 116
    new-instance v0, Lcom/android/calendar/vcal/a/a/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duration.parse(str=\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\') unexpected char \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\' at index="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/calendar/vcal/a/a/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method
