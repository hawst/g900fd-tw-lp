.class public Lcom/android/calendar/vcal/u;
.super Ljava/lang/Object;
.source "VCalManager.java"


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Ljava/util/ArrayList;

.field private e:Ljava/util/ArrayList;

.field private final f:Landroid/content/Context;

.field private g:Landroid/net/Uri;

.field private h:Ljava/util/HashMap;

.field private final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/android/calendar/vcal/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/vcal/u;->c:Ljava/lang/String;

    .line 85
    const-string v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/vcal/u;->a:Landroid/net/Uri;

    .line 86
    const-string v0, "content://com.android.calendar/maps"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/vcal/u;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;Z)V

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/calendar/vcal/u;-><init>(Landroid/content/Context;Landroid/net/Uri;Z)V

    .line 130
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Z)V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->h:Ljava/util/HashMap;

    .line 111
    iput-object p1, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    .line 112
    iput-object p2, p0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    .line 113
    iput-boolean p3, p0, Lcom/android/calendar/vcal/u;->i:Z

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->h:Ljava/util/HashMap;

    .line 96
    if-nez p1, :cond_0

    .line 97
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 99
    :cond_0
    iput-boolean p3, p0, Lcom/android/calendar/vcal/u;->i:Z

    .line 100
    iput-object p1, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    .line 101
    invoke-virtual {p0, p2}, Lcom/android/calendar/vcal/u;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/vcal/u;->h:Ljava/util/HashMap;

    .line 117
    if-nez p1, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 120
    :cond_0
    iput-boolean p2, p0, Lcom/android/calendar/vcal/u;->i:Z

    .line 121
    iput-object p1, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    .line 122
    return-void
.end method

.method static a(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1109
    .line 1117
    if-eqz p2, :cond_1

    .line 1118
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    move-wide p0, v0

    .line 1163
    :cond_0
    return-wide p0

    .line 1121
    :cond_1
    new-instance v2, Lcom/android/calendar/vcal/a/a/e;

    invoke-direct {v2}, Lcom/android/calendar/vcal/a/a/e;-><init>()V

    .line 1123
    if-eqz p3, :cond_2

    .line 1124
    invoke-virtual {v2, p3}, Lcom/android/calendar/vcal/a/a/e;->a(Ljava/lang/String;)V

    .line 1130
    :cond_2
    :try_start_0
    new-instance v0, Lcom/android/a/ad;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v0, p4, v3, v4, v5}, Lcom/android/a/ad;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1136
    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/android/a/ad;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1142
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1144
    const-string p5, "UTC"

    .line 1146
    :cond_3
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1148
    invoke-virtual {v1, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 1150
    new-instance v3, Lcom/android/calendar/vcal/a/a/f;

    invoke-direct {v3}, Lcom/android/calendar/vcal/a/a/f;-><init>()V

    .line 1151
    invoke-virtual {v3, v1, v0}, Lcom/android/calendar/vcal/a/a/f;->a(Landroid/text/format/Time;Lcom/android/a/ad;)J

    move-result-wide p0

    .line 1152
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    .line 1161
    :cond_4
    invoke-virtual {v2, p0, p1}, Lcom/android/calendar/vcal/a/a/e;->a(J)J

    move-result-wide v0

    goto :goto_0

    .line 1131
    :catch_0
    move-exception v0

    .line 1133
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/android/calendar/vcal/a/d;ILjava/lang/String;)Landroid/content/ContentValues;
    .locals 8

    .prologue
    const/16 v7, 0x2000

    const/4 v6, 0x0

    .line 768
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 770
    if-eqz p2, :cond_0

    .line 771
    const-string v0, "eventTimezone"

    invoke-virtual {v1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/vcal/a/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/a;

    .line 775
    iget-object v3, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 776
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 777
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "DESCRIPTION"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 778
    iget-object v3, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v7, :cond_2

    .line 779
    iget-object v3, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    .line 781
    :cond_2
    const-string v3, "body"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const-string v4, "\r\n"

    const-string v5, "\n"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 782
    :cond_3
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "DTEND"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 784
    :try_start_0
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 785
    const-string v0, "UTC"

    iput-object v0, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 786
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 787
    const-string v0, "due_date"

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 788
    :catch_0
    move-exception v0

    .line 789
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 791
    :cond_4
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "DTSTART"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 793
    :try_start_1
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 794
    const-string v0, "UTC"

    iput-object v0, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 795
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 796
    const-string v0, "start_date"

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 797
    :catch_1
    move-exception v0

    .line 798
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 801
    :cond_5
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "SUMMARY"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 802
    const-string v3, "subject"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const-string v4, "\r\n"

    const-string v5, "\n"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 804
    :cond_6
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "LOCATION"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 805
    const-string v3, "eventLocation"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const-string v4, "\r\n"

    const-string v5, "\n"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 806
    :cond_7
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "DUE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 808
    :try_start_2
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 809
    const-string v0, "UTC"

    iput-object v0, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 810
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 811
    const-string v0, "due_date"

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 812
    :catch_2
    move-exception v0

    .line 813
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 815
    :cond_8
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "RRULE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 816
    new-instance v3, Lcom/android/a/c;

    invoke-direct {v3}, Lcom/android/a/c;-><init>()V

    .line 818
    :try_start_3
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 819
    const-string v3, "rrule"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 820
    :catch_3
    move-exception v0

    .line 821
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 823
    :cond_9
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "COMPLETED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 825
    :try_start_4
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 826
    const-string v0, "lastDate"

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    .line 827
    :catch_4
    move-exception v0

    .line 828
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 830
    :cond_a
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "PRIORITY"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 832
    :try_start_5
    const-string v3, "importance"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    .line 833
    :catch_5
    move-exception v0

    .line 834
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 836
    :cond_b
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "DALARM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 838
    :try_start_6
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 839
    const-string v0, "UTC"

    iput-object v0, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 840
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 841
    const-string v0, "reminder_set"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 842
    const-string v0, "reminder_type"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 843
    const-string v0, "reminder_time"

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 844
    :catch_6
    move-exception v0

    .line 845
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 851
    :cond_c
    return-object v1
.end method

.method private a(Lcom/android/calendar/vcal/a/d;ILjava/lang/String;Ljava/util/ArrayList;)Landroid/content/ContentValues;
    .locals 10

    .prologue
    .line 932
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 933
    const/4 v0, 0x0

    .line 934
    const-string v1, "calendar_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 936
    iget-object v1, p1, Lcom/android/calendar/vcal/a/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/a;

    .line 937
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    if-eqz v4, :cond_16

    .line 938
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 939
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "DESCRIPTION"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 940
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x2000

    if-le v4, v5, :cond_0

    .line 941
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const/4 v5, 0x0

    const/16 v6, 0x2000

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    .line 943
    :cond_0
    const-string v4, "description"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const-string v5, "\r\n"

    const-string v6, "\n"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "\\\\n"

    const-string v6, "\\\n"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_1
    move v1, v0

    .line 1027
    goto :goto_0

    .line 945
    :cond_1
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "DTEND"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p1, Lcom/android/calendar/vcal/a/d;->a:Ljava/lang/String;

    const-string v6, "VTODO"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "COMPLETED"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 948
    :cond_2
    :try_start_0
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 949
    iget-boolean v0, v4, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_3

    .line 950
    const-string v0, "UTC"

    iput-object v0, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 951
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 953
    :cond_3
    const-string v0, "dtend"

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 957
    goto :goto_1

    .line 954
    :catch_0
    move-exception v0

    .line 956
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    .line 957
    goto :goto_1

    .line 958
    :cond_4
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "DTSTART"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p1, Lcom/android/calendar/vcal/a/d;->a:Ljava/lang/String;

    const-string v6, "VTODO"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "DUE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 961
    :cond_5
    :try_start_1
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 962
    iget-boolean v0, v4, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_6

    .line 963
    const-string v0, "UTC"

    iput-object v0, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 964
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 966
    :cond_6
    const-string v0, "dtstart"

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 968
    iget-boolean v0, v4, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_7

    .line 969
    const/4 v1, 0x1

    .line 972
    :cond_7
    const-string v4, "allDay"

    if-eqz v1, :cond_8

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v1

    .line 975
    goto/16 :goto_1

    .line 972
    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    .line 973
    :catch_1
    move-exception v0

    .line 974
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    .line 975
    goto/16 :goto_1

    .line 977
    :cond_9
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "SUMMARY"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_a

    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "SUBJECT"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 979
    :cond_a
    const-string v4, "title"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const-string v5, "\r\n"

    const-string v6, "\n"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "\\\\n"

    const-string v6, "\\\n"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_1

    .line 982
    :cond_b
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "LOCATION"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 983
    const-string v4, "eventLocation"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const-string v5, "\r\n"

    const-string v6, "\n"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "\\\\n"

    const-string v6, "\\\n"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_1

    .line 985
    :cond_c
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "GEO"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 986
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 987
    if-eqz v0, :cond_d

    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_d

    .line 988
    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    const-wide/high16 v8, 0x4018000000000000L    # 6.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 989
    const/4 v5, 0x1

    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    const-wide/high16 v8, 0x4018000000000000L    # 6.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 990
    const-string v5, "latitude"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 991
    const-string v4, "longitude"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_d
    move v0, v1

    .line 993
    goto/16 :goto_1

    :cond_e
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "DUE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 994
    const-string v4, "duration"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_1

    .line 995
    :cond_f
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "RRULE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 996
    new-instance v4, Lcom/android/a/c;

    invoke-direct {v4}, Lcom/android/a/c;-><init>()V

    .line 998
    :try_start_2
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 999
    const-string v4, "rrule"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    move v0, v1

    .line 1003
    goto/16 :goto_1

    .line 1000
    :catch_2
    move-exception v0

    .line 1001
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 1003
    :cond_10
    iget-object v5, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v6, "COMPLETED"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1005
    :try_start_3
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 1006
    const-string v0, "lastDate"

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move v0, v1

    .line 1009
    goto/16 :goto_1

    .line 1007
    :catch_3
    move-exception v0

    .line 1008
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    .line 1009
    goto/16 :goto_1

    .line 1010
    :cond_11
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "TZ"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1011
    invoke-direct {p0, p3, v0}, Lcom/android/calendar/vcal/u;->a(Ljava/lang/String;Lcom/android/calendar/vcal/a/a;)Ljava/lang/String;

    move-result-object p3

    move v0, v1

    goto/16 :goto_1

    .line 1012
    :cond_12
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "X-STICKER"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1014
    :try_start_4
    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1015
    const-string v4, "sticker_type"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    move v0, v1

    .line 1018
    goto/16 :goto_1

    .line 1016
    :catch_4
    move-exception v0

    .line 1017
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    move v0, v1

    .line 1018
    goto/16 :goto_1

    .line 1019
    :cond_13
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "X-MAP-IMAGE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1020
    const-string v4, "map"

    iget-object v0, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const/4 v5, 0x2

    invoke-static {v0, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move v0, v1

    goto/16 :goto_1

    .line 1021
    :cond_14
    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "DALARM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_15

    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "AALARM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_15

    iget-object v4, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v5, "MALARM"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 1024
    :cond_15
    invoke-direct {p0, v0, v2, p4}, Lcom/android/calendar/vcal/u;->a(Lcom/android/calendar/vcal/a/a;Landroid/content/ContentValues;Ljava/util/ArrayList;)V

    :cond_16
    move v0, v1

    goto/16 :goto_1

    .line 1028
    :cond_17
    if-eqz p3, :cond_19

    .line 1029
    const-string v0, "eventTimezone"

    if-eqz v1, :cond_18

    const-string p3, "UTC"

    :cond_18
    invoke-virtual {v2, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    :cond_19
    const-string v0, "dtstart"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1032
    const-string v1, "dtend"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 1033
    if-nez v0, :cond_1a

    .line 1034
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1035
    const-string v3, "dtstart"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1037
    :cond_1a
    if-eqz v1, :cond_1b

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-lez v1, :cond_1c

    .line 1038
    :cond_1b
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v4, 0x36ee80

    add-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1039
    const-string v1, "dtend"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1041
    :cond_1c
    const-string v0, "allDay"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1d

    .line 1042
    const-string v0, "allDay"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1045
    :cond_1d
    return-object v2
.end method

.method private static a(JLjava/lang/String;Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1057
    .line 1058
    if-eqz p2, :cond_0

    const-string v0, "UTC"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1059
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1065
    :goto_0
    invoke-virtual {v0, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 1066
    if-eqz p3, :cond_1

    .line 1067
    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1072
    :goto_1
    return-object v0

    .line 1062
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    goto :goto_0

    .line 1069
    :cond_1
    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/android/calendar/vcal/a/a;)Ljava/lang/String;
    .locals 12

    .prologue
    const v9, 0x7f090035

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 855
    iget-object v4, p2, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    .line 858
    invoke-static {}, Lcom/android/calendar/dz;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "+09:00"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 859
    const-string p1, "Asia/Seoul"

    .line 928
    :cond_0
    :goto_0
    return-object p1

    .line 862
    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 863
    const-string v0, "00:00"

    .line 867
    :goto_1
    new-array v2, v7, [Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v2, v1

    const-string v5, "0"

    aput-object v5, v2, v3

    .line 871
    const/4 v5, 0x0

    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 889
    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 890
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 896
    :goto_2
    if-eqz v0, :cond_0

    .line 897
    aget-object v2, v0, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 898
    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 899
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v5, 0x36ee80

    mul-int/2addr v2, v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v5, 0xea60

    mul-int/2addr v0, v5

    add-int/2addr v0, v2

    .line 900
    const-string v2, "-"

    invoke-virtual {v4, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 901
    mul-int/lit8 v0, v0, -0x1

    .line 903
    :cond_2
    invoke-static {v0}, Ljava/util/TimeZone;->getAvailableIDs(I)[Ljava/lang/String;

    move-result-object v7

    .line 904
    if-eqz v7, :cond_0

    .line 905
    iget-object v2, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    .line 907
    array-length v9, v7

    move v6, v1

    move v2, v1

    :goto_3
    if-ge v6, v9, :cond_4

    aget-object v4, v7, v6

    .line 908
    array-length v10, v8

    move v5, v1

    :goto_4
    if-ge v5, v10, :cond_3

    aget-object v11, v8, v5

    .line 909
    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    move v2, v3

    move-object p1, v4

    .line 915
    :cond_3
    if-eqz v2, :cond_b

    .line 918
    :cond_4
    if-nez p1, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 919
    :cond_5
    array-length v3, v8

    move v2, v1

    :goto_5
    if-ge v2, v3, :cond_0

    aget-object v1, v8, v2

    .line 920
    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    if-ne v0, v4, :cond_c

    move-object p1, v1

    .line 922
    goto/16 :goto_0

    .line 865
    :cond_6
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 872
    :catch_0
    move-exception v0

    .line 873
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 875
    array-length v4, v2

    move v0, v1

    :goto_6
    if-ge v0, v4, :cond_d

    aget-object v5, v2, v0

    .line 876
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 881
    :goto_7
    if-nez v3, :cond_0

    .line 883
    const-string p1, "Europe/London"

    goto/16 :goto_0

    .line 875
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 892
    :cond_8
    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 893
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v8, :cond_9

    .line 894
    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    :cond_9
    move-object v0, v2

    goto/16 :goto_2

    .line 908
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 907
    :cond_b
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_3

    .line 919
    :cond_c
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    :cond_d
    move v3, v1

    goto :goto_7
.end method

.method private a(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 336
    if-eqz p1, :cond_0

    const-string v0, "reminder_time"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 341
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 343
    const-string v3, "task_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 344
    const-string v0, "reminder_time"

    const-string v1, "reminder_time"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 345
    const-string v0, "reminder_type"

    const-string v1, "reminder_type"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 346
    const-string v0, "state"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 347
    const-string v0, "accountkey"

    const-string v1, "accountKey"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 349
    const-string v0, "start_date"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350
    const-string v0, "start_date"

    const-string v1, "start_date"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 352
    :cond_2
    const-string v0, "due_date"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 353
    const-string v0, "due_date"

    const-string v1, "due_date"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 355
    :cond_3
    const-string v0, "subject"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 356
    const-string v0, "subject"

    const-string v1, "subject"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hg;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0
.end method

.method private a(Landroid/net/Uri;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    .line 325
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 333
    :cond_0
    return-void

    .line 327
    :cond_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 329
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 330
    const-string v4, "event_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 331
    iget-object v4, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0
.end method

.method private a(Lcom/android/calendar/vcal/a/a/b;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 1409
    .line 1411
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const-string v0, "minutes"

    aput-object v0, v2, v6

    const-string v0, "method"

    aput-object v0, v2, v3

    .line 1415
    if-eqz p2, :cond_0

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    .line 1464
    :cond_0
    :goto_0
    return-void

    .line 1419
    :cond_1
    const-string v0, "event_id=?"

    .line 1420
    new-array v4, v6, [Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v0, v4, v1

    .line 1423
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "event_id=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1426
    if-eqz v1, :cond_0

    .line 1429
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v6, :cond_2

    .line 1430
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1434
    :cond_2
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1439
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1441
    :cond_3
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1442
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1444
    invoke-static {v2, v3}, Lcom/android/calendar/au;->a(II)Lcom/android/calendar/au;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1445
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1448
    new-instance v2, Lcom/android/calendar/vcal/v;

    invoke-direct {v2, p0}, Lcom/android/calendar/vcal/v;-><init>(Lcom/android/calendar/vcal/u;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1455
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1456
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 1457
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/android/calendar/au;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/android/calendar/au;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1462
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1460
    :cond_4
    :try_start_1
    iput-object v2, p1, Lcom/android/calendar/vcal/a/a/b;->t:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private a(Lcom/android/calendar/vcal/a/a/c;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1167
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/calendar/vcal/a/a/c;->c:Ljava/lang/String;

    .line 1171
    const-string v0, "complete"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1172
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p1, Lcom/android/calendar/vcal/a/a/c;->d:Z

    .line 1175
    const-string v0, "subject"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1176
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/calendar/vcal/a/a/c;->e:Ljava/lang/String;

    .line 1179
    const-string v0, "start_date"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1180
    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1181
    iput-object v6, p1, Lcom/android/calendar/vcal/a/a/c;->f:Ljava/lang/String;

    .line 1188
    :goto_1
    const-string v0, "due_date"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1189
    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1190
    iput-object v6, p1, Lcom/android/calendar/vcal/a/a/c;->g:Ljava/lang/String;

    .line 1197
    :goto_2
    const-string v0, "importance"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1198
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p1, Lcom/android/calendar/vcal/a/a/c;->h:I

    .line 1201
    const-string v0, "reminder_set"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1202
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_4

    :goto_3
    iput-boolean v1, p1, Lcom/android/calendar/vcal/a/a/c;->i:Z

    .line 1203
    const-string v0, "reminder_time"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1204
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1205
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 1206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1208
    :cond_0
    const-string v3, "UTC"

    invoke-static {v0, v1, v3, v2}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/calendar/vcal/a/a/c;->j:Ljava/lang/String;

    .line 1212
    const-string v0, "body"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1213
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/calendar/vcal/a/a/c;->k:Ljava/lang/String;

    .line 1214
    return-void

    :cond_1
    move v0, v2

    .line 1172
    goto :goto_0

    .line 1183
    :cond_2
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string v0, "UTC"

    invoke-static {v4, v5, v0, v2}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/calendar/vcal/a/a/c;->f:Ljava/lang/String;

    goto :goto_1

    .line 1192
    :cond_3
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string v0, "UTC"

    invoke-static {v4, v5, v0, v2}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/calendar/vcal/a/a/c;->g:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move v1, v2

    .line 1202
    goto :goto_3
.end method

.method private a(Lcom/android/calendar/vcal/a/a;Landroid/content/ContentValues;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 710
    iget-object v1, p1, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 713
    const/4 v1, -0x1

    .line 714
    iget-object v3, p1, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v4, "DALARM"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v0

    .line 724
    :cond_0
    :goto_0
    aget-object v3, v2, v0

    invoke-static {v3}, Lcom/android/calendar/vcal/u;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 751
    :cond_1
    :goto_1
    return-void

    .line 716
    :cond_2
    iget-object v3, p1, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v4, "AALARM"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 717
    const/4 v1, 0x1

    goto :goto_0

    .line 718
    :cond_3
    iget-object v3, p1, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v4, "MALARM"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 719
    const/4 v1, 0x2

    goto :goto_0

    .line 726
    :cond_4
    aget-object v2, v2, v0

    .line 729
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 730
    invoke-virtual {v3, v2}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 731
    const-string v2, "UTC"

    iput-object v2, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 732
    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 735
    const-string v0, "dtstart"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 737
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 740
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 741
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v0, v2

    div-int/lit8 v0, v0, 0x3c

    .line 743
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 744
    const-string v3, "minutes"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 745
    const-string v0, "method"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 749
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 750
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private b(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 425
    const-string v0, "rrule"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "rrule"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 427
    const-string v0, "duration"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    const-string v0, "dtend"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 433
    :cond_0
    const-string v0, "duration"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "dtend"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    const-string v0, "duration"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 437
    :cond_1
    const-string v0, "eventTimezone"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/calendar/vcal/u;->i:Z

    if-nez v0, :cond_2

    .line 438
    const-string v0, "eventTimezone"

    const-string v1, "+00:00"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 440
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 441
    const-string v1, "dtstart"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 442
    const-string v1, "eventTimezone"

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_2
    const-string v0, "due_date"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 448
    const-string v0, "utc_due_date"

    const-string v1, "due_date"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 450
    :cond_3
    return-void
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 754
    if-nez p0, :cond_0

    .line 764
    :goto_0
    return v0

    .line 758
    :cond_0
    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1}, Ljava/text/SimpleDateFormat;-><init>()V

    .line 759
    const-string v2, "yyyyMMdd"

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 760
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 764
    const/4 v0, 0x1

    goto :goto_0

    .line 761
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private d()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1076
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1079
    if-eqz v1, :cond_1

    .line 1080
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1081
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1082
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1087
    :goto_0
    return v0

    .line 1085
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1087
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1049
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1050
    :cond_0
    const/4 v0, 0x1

    .line 1052
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;ILjava/lang/String;I)Lcom/android/calendar/vcal/w;
    .locals 10

    .prologue
    .line 390
    .line 391
    const/4 v0, 0x0

    .line 392
    iget-object v1, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 394
    invoke-direct {p0, v0}, Lcom/android/calendar/vcal/u;->b(Landroid/content/ContentValues;)V

    .line 395
    iget-boolean v1, p0, Lcom/android/calendar/vcal/u;->i:Z

    if-eqz v1, :cond_0

    .line 396
    const-string v1, "accountKey"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 397
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    const-string v1, "_sync_dirty"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 399
    const-string v1, "groupId"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/vcal/u;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 402
    new-instance v0, Lcom/android/calendar/vcal/w;

    invoke-direct {v0, v1}, Lcom/android/calendar/vcal/w;-><init>(Landroid/net/Uri;)V

    goto :goto_0

    .line 404
    :cond_0
    const-string v1, "calendar_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 405
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 407
    const-string v2, "dtstart"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 408
    const-string v3, "dtend"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 409
    if-nez v2, :cond_1

    .line 410
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 412
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v3, v4, v8

    if-lez v3, :cond_4

    .line 413
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v8, 0x36ee80

    add-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v4, v0

    .line 415
    :goto_1
    new-instance v0, Lcom/android/calendar/vcal/w;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/vcal/w;-><init>(Landroid/net/Uri;JJ)V

    .line 416
    iget-object v2, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-direct {p0, v1, v2}, Lcom/android/calendar/vcal/u;->a(Landroid/net/Uri;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 420
    :cond_3
    return-object v0

    :cond_4
    move-object v4, v0

    goto :goto_1
.end method

.method public a()Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 187
    const/4 v1, 0x0

    move-object v0, p0

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/vcal/u;->a(ZJJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(ZJJ)Ljava/lang/String;
    .locals 12

    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 192
    :cond_0
    const/4 v0, 0x0

    .line 241
    :goto_0
    return-object v0

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    .line 196
    if-eqz v6, :cond_2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gez v0, :cond_3

    .line 197
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 201
    if-nez v10, :cond_4

    .line 202
    const/4 v0, 0x0

    goto :goto_0

    .line 204
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/vcal/u;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 206
    if-nez v4, :cond_5

    .line 207
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 208
    const/4 v0, 0x0

    goto :goto_0

    .line 212
    :cond_5
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_6

    .line 213
    const/4 v0, 0x0

    .line 240
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 241
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 215
    :cond_6
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_7

    .line 216
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 218
    :cond_7
    new-instance v2, Lcom/android/calendar/vcal/a/a/a;

    invoke-direct {v2}, Lcom/android/calendar/vcal/a/a/a;-><init>()V

    .line 219
    iget-boolean v0, p0, Lcom/android/calendar/vcal/u;->i:Z

    if-eqz v0, :cond_8

    .line 220
    new-instance v0, Lcom/android/calendar/vcal/a/a/c;

    invoke-direct {v0}, Lcom/android/calendar/vcal/a/a/c;-><init>()V

    .line 221
    invoke-direct {p0, v0, v10}, Lcom/android/calendar/vcal/u;->a(Lcom/android/calendar/vcal/a/a/c;Landroid/database/Cursor;)V

    .line 222
    invoke-virtual {v2, v0}, Lcom/android/calendar/vcal/a/a/a;->a(Lcom/android/calendar/vcal/a/a/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    :goto_1
    :try_start_2
    new-instance v0, Lcom/android/calendar/vcal/a/a/h;

    invoke-direct {v0}, Lcom/android/calendar/vcal/a/a/h;-><init>()V

    .line 234
    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/android/calendar/vcal/a/a/h;->a(Lcom/android/calendar/vcal/a/a/a;I)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 240
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 241
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 224
    :cond_8
    if-eqz p1, :cond_9

    move-object v1, p0

    move-object v3, v10

    move v5, p1

    move-wide v6, p2

    move-wide/from16 v8, p4

    .line 225
    :try_start_3
    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/vcal/u;->a(Lcom/android/calendar/vcal/a/a/a;Landroid/database/Cursor;Landroid/database/Cursor;ZJJ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 240
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 241
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    .line 227
    :cond_9
    :try_start_4
    invoke-virtual {p0, v2, v10, v4}, Lcom/android/calendar/vcal/u;->a(Lcom/android/calendar/vcal/a/a/a;Landroid/database/Cursor;Landroid/database/Cursor;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 236
    :catch_0
    move-exception v0

    .line 237
    const/4 v0, 0x0

    .line 240
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 241
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public a(I)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x0

    .line 626
    new-instance v0, Lcom/android/calendar/vcal/a/a/j;

    invoke-direct {v0}, Lcom/android/calendar/vcal/a/a/j;-><init>()V

    .line 627
    new-instance v1, Lcom/android/calendar/vcal/a/c;

    invoke-direct {v1}, Lcom/android/calendar/vcal/a/c;-><init>()V

    .line 629
    if-nez p1, :cond_1

    .line 705
    :cond_0
    :goto_0
    return-object v3

    .line 634
    :cond_1
    const-string v2, "\r\n"

    const-string v5, "\n"

    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\n"

    const-string v6, "\r\n"

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 637
    :try_start_0
    iget-boolean v5, p0, Lcom/android/calendar/vcal/u;->i:Z

    invoke-virtual {v0, v2, v1, v5}, Lcom/android/calendar/vcal/a/a/j;->a(Ljava/lang/String;Lcom/android/calendar/vcal/a/c;Z)Z
    :try_end_0
    .catch Lcom/android/calendar/vcal/a/a/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 647
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 652
    iget-object v0, v1, Lcom/android/calendar/vcal/a/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v3

    move v2, v4

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/d;

    .line 653
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 654
    iget-boolean v7, p0, Lcom/android/calendar/vcal/u;->i:Z

    if-eqz v7, :cond_5

    .line 655
    iget-object v7, v0, Lcom/android/calendar/vcal/a/d;->a:Ljava/lang/String;

    const-string v8, "VCALENDAR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 657
    iget-object v0, v0, Lcom/android/calendar/vcal/a/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/a;

    .line 658
    iget-object v7, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    if-eqz v7, :cond_c

    .line 659
    iget-object v7, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v8, "TZ"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 660
    iget-object v7, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    .line 661
    invoke-virtual {v7, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 662
    const-string v8, ":"

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 663
    if-eqz v0, :cond_c

    .line 664
    aget-object v8, v0, v10

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    .line 665
    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 666
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const v9, 0x36ee80

    mul-int/2addr v8, v9

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v9, 0xea60

    mul-int/2addr v0, v9

    add-int/2addr v0, v8

    .line 667
    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 668
    mul-int/lit8 v0, v0, -0x1

    .line 670
    :cond_2
    invoke-static {v0}, Ljava/util/TimeZone;->getAvailableIDs(I)[Ljava/lang/String;

    move-result-object v0

    .line 671
    if-eqz v0, :cond_c

    .line 672
    aget-object v0, v0, v10

    :goto_3
    move-object v1, v0

    .line 677
    goto :goto_2

    .line 638
    :catch_0
    move-exception v0

    .line 640
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    const v1, 0x7f0f02f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 641
    :catch_1
    move-exception v0

    .line 642
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    :goto_4
    move v2, v0

    .line 704
    goto/16 :goto_1

    .line 678
    :cond_4
    iget-object v7, v0, Lcom/android/calendar/vcal/a/d;->a:Ljava/lang/String;

    const-string v8, "VTODO"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 679
    invoke-static {v0, v10, v1}, Lcom/android/calendar/vcal/u;->a(Lcom/android/calendar/vcal/a/d;ILjava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 680
    iget-object v7, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 681
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 682
    goto :goto_4

    .line 685
    :cond_5
    iget-object v7, v0, Lcom/android/calendar/vcal/a/d;->a:Ljava/lang/String;

    const-string v8, "VCALENDAR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 688
    invoke-direct {p0}, Lcom/android/calendar/vcal/u;->d()I

    move-result v2

    .line 690
    iget-object v0, v0, Lcom/android/calendar/vcal/a/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/vcal/a/a;

    .line 691
    iget-object v7, v0, Lcom/android/calendar/vcal/a/a;->b:Ljava/lang/String;

    if-eqz v7, :cond_b

    .line 692
    iget-object v7, v0, Lcom/android/calendar/vcal/a/a;->a:Ljava/lang/String;

    const-string v8, "TZ"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 693
    invoke-direct {p0, v1, v0}, Lcom/android/calendar/vcal/u;->a(Ljava/lang/String;Lcom/android/calendar/vcal/a/a;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    move-object v1, v0

    .line 696
    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_4

    .line 697
    :cond_7
    iget-object v7, v0, Lcom/android/calendar/vcal/a/d;->a:Ljava/lang/String;

    const-string v8, "VEVENT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, v0, Lcom/android/calendar/vcal/a/d;->a:Ljava/lang/String;

    const-string v8, "VTODO"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 699
    :cond_8
    invoke-direct {p0, v0, v2, v1, v6}, Lcom/android/calendar/vcal/u;->a(Lcom/android/calendar/vcal/a/d;ILjava/lang/String;Ljava/util/ArrayList;)Landroid/content/ContentValues;

    move-result-object v0

    .line 700
    iget-object v7, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 701
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    move v0, v2

    goto :goto_4

    .line 705
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    goto/16 :goto_0

    :cond_b
    move-object v0, v1

    goto :goto_6

    :cond_c
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public a(Landroid/content/ContentValues;)V
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 614
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 615
    return-void
.end method

.method public a(Landroid/content/ContentValues;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 620
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 622
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    return-void
.end method

.method public a(Lcom/android/calendar/vcal/a/a/a;Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const-wide/16 v6, 0x0

    .line 1274
    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v8, v6

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/vcal/u;->a(Lcom/android/calendar/vcal/a/a/a;Landroid/database/Cursor;Landroid/database/Cursor;ZJJ)V

    .line 1275
    return-void
.end method

.method public a(Lcom/android/calendar/vcal/a/a/a;Landroid/database/Cursor;Landroid/database/Cursor;ZJJ)V
    .locals 17

    .prologue
    .line 1280
    const-string v2, "eventTimezone"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1281
    if-eqz v7, :cond_8

    .line 1286
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 1288
    invoke-static {v7}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    .line 1289
    invoke-virtual {v4, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    .line 1290
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1291
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1294
    if-gez v2, :cond_7

    .line 1295
    const/16 v2, 0x2d

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1300
    :goto_0
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, "%02d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    const v9, 0x36ee80

    div-int v9, v3, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v2, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1301
    const/16 v2, 0x3a

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1303
    const v2, 0xea60

    div-int v2, v3, v2

    .line 1304
    rem-int/lit8 v2, v2, 0x3c

    .line 1306
    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    .line 1307
    const/16 v3, 0x30

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1309
    :cond_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1311
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    iput-object v2, v0, Lcom/android/calendar/vcal/a/a/a;->a:Ljava/lang/String;

    .line 1317
    :goto_1
    new-instance v10, Lcom/android/calendar/vcal/a/a/b;

    invoke-direct {v10}, Lcom/android/calendar/vcal/a/a/b;-><init>()V

    .line 1320
    if-eqz p4, :cond_9

    move-wide/from16 v2, p5

    .line 1330
    :cond_1
    :goto_2
    const-string v4, "duration"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1331
    const-string v4, "rrule"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1332
    if-eqz p4, :cond_2

    .line 1333
    const-string v6, ""

    .line 1336
    :cond_2
    const-wide/16 v8, 0x0

    .line 1338
    :try_start_0
    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static/range {v2 .. v7}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    :try_end_0
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 1343
    :goto_3
    const-wide/16 v12, 0x0

    cmp-long v4, p7, v12

    if-nez v4, :cond_3

    if-eqz v6, :cond_3

    if-eqz v5, :cond_3

    .line 1344
    new-instance v4, Lcom/android/calendar/vcal/a/a/e;

    invoke-direct {v4}, Lcom/android/calendar/vcal/a/a/e;-><init>()V

    .line 1346
    :try_start_1
    invoke-virtual {v4, v5}, Lcom/android/calendar/vcal/a/a/e;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/calendar/vcal/a/a/d; {:try_start_1 .. :try_end_1} :catch_1

    .line 1351
    :goto_4
    invoke-virtual {v4}, Lcom/android/calendar/vcal/a/a/e;->a()J

    move-result-wide v12

    add-long p7, v2, v12

    .line 1354
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->s:Ljava/lang/String;

    .line 1355
    const-string v4, "description"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->d:Ljava/lang/String;

    .line 1358
    const-string v4, "UTC"

    const/4 v7, 0x0

    move-wide/from16 v0, p7

    invoke-static {v0, v1, v4, v7}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->e:Ljava/lang/String;

    .line 1359
    const-string v4, "UTC"

    const/4 v7, 0x1

    move-wide/from16 v0, p7

    invoke-static {v0, v1, v4, v7}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->g:Ljava/lang/String;

    .line 1360
    const-string v4, "UTC"

    const/4 v7, 0x0

    invoke-static {v2, v3, v4, v7}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->f:Ljava/lang/String;

    .line 1361
    const-string v4, "UTC"

    const/4 v7, 0x1

    invoke-static {v2, v3, v4, v7}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->h:Ljava/lang/String;

    .line 1362
    iput-object v5, v10, Lcom/android/calendar/vcal/a/a/b;->i:Ljava/lang/String;

    .line 1363
    const-string v4, "allDay"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_a

    const/4 v4, 0x1

    :goto_5
    iput-boolean v4, v10, Lcom/android/calendar/vcal/a/a/b;->j:Z

    .line 1365
    const-string v4, "eventLocation"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->p:Ljava/lang/String;

    .line 1367
    const-string v4, "latitude"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-float v4, v4

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    const-wide/high16 v14, 0x4018000000000000L    # 6.0

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    double-to-float v5, v12

    div-float/2addr v4, v5

    iput v4, v10, Lcom/android/calendar/vcal/a/a/b;->q:F

    .line 1369
    const-string v4, "longitude"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-float v4, v4

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    const-wide/high16 v14, 0x4018000000000000L    # 6.0

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    double-to-float v5, v12

    div-float/2addr v4, v5

    iput v4, v10, Lcom/android/calendar/vcal/a/a/b;->r:F

    .line 1371
    const-string v4, "hasAlarm"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->k:Ljava/lang/String;

    .line 1373
    const-string v4, "eventTimezone"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/android/calendar/vcal/a/a/b;->a:Ljava/lang/String;

    .line 1375
    iput-wide v2, v10, Lcom/android/calendar/vcal/a/a/b;->c:J

    .line 1379
    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-lez v2, :cond_4

    .line 1380
    const-string v2, "UTC"

    const/4 v3, 0x0

    invoke-static {v8, v9, v2, v3}, Lcom/android/calendar/vcal/u;->a(JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/android/calendar/vcal/a/a/b;->l:Ljava/lang/String;

    .line 1383
    :cond_4
    iput-object v6, v10, Lcom/android/calendar/vcal/a/a/b;->m:Ljava/lang/String;

    .line 1384
    const-string v2, "eventStatus"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/android/calendar/vcal/a/a/b;->n:Ljava/lang/String;

    .line 1386
    const-string v2, "title"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/android/calendar/vcal/a/a/b;->o:Ljava/lang/String;

    .line 1390
    :try_start_2
    const-string v2, "sticker_type"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v10, Lcom/android/calendar/vcal/a/a/b;->u:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1395
    :goto_6
    if-eqz p3, :cond_5

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 1396
    const-string v2, "map"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 1397
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/android/calendar/vcal/a/a/b;->v:Ljava/lang/String;

    .line 1400
    :cond_5
    iget-object v2, v10, Lcom/android/calendar/vcal/a/a/b;->k:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/calendar/vcal/u;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1401
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/android/calendar/vcal/u;->a(Lcom/android/calendar/vcal/a/a/b;Ljava/lang/String;)V

    .line 1403
    :cond_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/android/calendar/vcal/a/a/a;->a(Lcom/android/calendar/vcal/a/a/a;)V

    .line 1404
    return-void

    .line 1297
    :cond_7
    const/16 v2, 0x2b

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1313
    :cond_8
    const-string v2, "+00:00"

    move-object/from16 v0, p1

    iput-object v2, v0, Lcom/android/calendar/vcal/a/a/a;->a:Ljava/lang/String;

    goto/16 :goto_1

    .line 1324
    :cond_9
    const-string v2, "dtstart"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1325
    const-wide/16 p7, 0x0

    .line 1326
    const-string v4, "dtend"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1327
    const-string v4, "dtend"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p7

    goto/16 :goto_2

    .line 1339
    :catch_0
    move-exception v4

    .line 1340
    invoke-virtual {v4}, Lcom/android/calendar/vcal/a/a/d;->printStackTrace()V

    goto/16 :goto_3

    .line 1347
    :catch_1
    move-exception v7

    .line 1348
    invoke-virtual {v7}, Lcom/android/calendar/vcal/a/a/d;->printStackTrace()V

    goto/16 :goto_4

    .line 1363
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 1391
    :catch_2
    move-exception v2

    .line 1392
    const/4 v2, -0x1

    iput v2, v10, Lcom/android/calendar/vcal/a/a/b;->u:I

    goto/16 :goto_6
.end method

.method public a(ILjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 363
    const/4 v0, 0x0

    .line 366
    iget-object v1, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 368
    invoke-direct {p0, v0}, Lcom/android/calendar/vcal/u;->b(Landroid/content/ContentValues;)V

    .line 370
    const-string v4, "accountKey"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 371
    if-nez p1, :cond_0

    .line 372
    const-string v4, "accountName"

    const-string v5, "My task"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v4, "_sync_dirty"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 377
    :goto_1
    const-string v4, "groupId"

    const-string v5, "1"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v4, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/calendar/vcal/u;->a:Landroid/net/Uri;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 381
    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    move v1, v0

    .line 384
    goto :goto_0

    .line 375
    :cond_0
    const-string v4, "accountName"

    invoke-virtual {v0, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 386
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 462
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1467
    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "\\"

    aput-object v2, v1, v0

    const/4 v2, 0x1

    const-string v3, "/"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, ":"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "*"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "?"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "\""

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "<"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, ">"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "|"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, ";"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "\n"

    aput-object v3, v1, v2

    .line 1471
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 1472
    aget-object v2, v1, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    .line 1473
    const-string v0, "\\\\"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "[*]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "[?]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "<"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ">"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "[|]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1480
    :cond_0
    return-object p1

    .line 1471
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 295
    .line 299
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 300
    invoke-direct {p0, v0}, Lcom/android/calendar/vcal/u;->b(Landroid/content/ContentValues;)V

    .line 302
    iget-boolean v5, p0, Lcom/android/calendar/vcal/u;->i:Z

    if-eqz v5, :cond_1

    .line 303
    const-string v5, "accountKey"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 304
    const-string v5, "accountName"

    const-string v7, "My task"

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string v5, "_sync_dirty"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 306
    const-string v5, "groupId"

    const-string v7, "1"

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v5, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v7, Lcom/android/calendar/vcal/u;->a:Landroid/net/Uri;

    invoke-virtual {v5, v7, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    .line 308
    invoke-direct {p0, v5, v0}, Lcom/android/calendar/vcal/u;->a(Landroid/net/Uri;Landroid/content/ContentValues;)V

    move-object v0, v5

    .line 314
    :goto_1
    if-eqz v0, :cond_0

    move v3, v4

    .line 317
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 318
    goto :goto_0

    .line 310
    :cond_1
    iget-object v5, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v7, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v7, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    .line 311
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-direct {p0, v5, v0}, Lcom/android/calendar/vcal/u;->a(Landroid/net/Uri;Ljava/util/ArrayList;)V

    move-object v0, v5

    goto :goto_1

    .line 320
    :cond_2
    return v3
.end method

.method public c(I)Ljava/lang/Long;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 511
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 513
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v2, "dtstart"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 514
    if-nez v0, :cond_0

    move-object v0, v1

    .line 519
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v2, 0x0

    .line 1484
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 1516
    :cond_0
    :goto_0
    return-object v2

    .line 1489
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/vcal/u;->g:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1490
    if-eqz v1, :cond_0

    .line 1495
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1497
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v0, "yyMMdd_HHmm"

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1498
    const-string v0, "title"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1499
    const-string v3, "dtstart"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1502
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1503
    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    .line 1504
    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v4, v4, -0x3

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1506
    if-nez v0, :cond_2

    .line 1507
    const-string v0, ""

    .line 1509
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v6, :cond_3

    .line 1510
    const/4 v4, 0x0

    const/16 v5, 0xa

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1512
    :cond_3
    invoke-virtual {p0, v0}, Lcom/android/calendar/vcal/u;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1514
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".vcs"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1516
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public d(I)Ljava/lang/Long;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 542
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 544
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v2, "dtend"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 545
    if-nez v0, :cond_0

    move-object v0, v1

    .line 550
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public e(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 554
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v2, "allDay"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v2, "allDay"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 557
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 555
    goto :goto_0

    :cond_1
    move v0, v1

    .line 557
    goto :goto_0
.end method

.method public f(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1218
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v1, "subject"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1220
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(I)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1226
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1228
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v2, "importance"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1233
    :goto_0
    return v0

    .line 1229
    :catch_0
    move-exception v0

    move v0, v1

    .line 1230
    goto :goto_0

    :cond_0
    move v0, v1

    .line 1233
    goto :goto_0
.end method

.method public h(I)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1238
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v1, "start_date"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1240
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(I)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1245
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v1, "due_date"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1247
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1252
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v1, "body"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1254
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k(I)Z
    .locals 2

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1259
    iget-object v0, p0, Lcom/android/calendar/vcal/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v1, "reminder_set"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1260
    if-eqz v0, :cond_0

    .line 1261
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1263
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
