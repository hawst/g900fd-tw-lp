.class Lcom/android/calendar/dr;
.super Landroid/content/AsyncQueryHandler;
.source "FacebookEventSyncService.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/FacebookEventSyncService;


# direct methods
.method public constructor <init>(Lcom/android/calendar/FacebookEventSyncService;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/android/calendar/dr;->a:Lcom/android/calendar/FacebookEventSyncService;

    .line 407
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 408
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/android/calendar/dr;->a:Lcom/android/calendar/FacebookEventSyncService;

    invoke-static {v0, p1}, Lcom/android/calendar/FacebookEventSyncService;->a(Lcom/android/calendar/FacebookEventSyncService;I)I

    .line 412
    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 416
    invoke-static {}, Lcom/android/calendar/FacebookEventSyncService;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    const-string v0, "FBEventSyncService"

    const-string v1, "FacebookPartialQueryHandler"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :cond_0
    if-nez p3, :cond_2

    .line 433
    :cond_1
    :goto_0
    return-void

    .line 423
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/dr;->a:Lcom/android/calendar/FacebookEventSyncService;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncService;->a(Lcom/android/calendar/FacebookEventSyncService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 424
    new-instance v0, Lcom/android/calendar/dt;

    iget-object v1, p0, Lcom/android/calendar/dr;->a:Lcom/android/calendar/FacebookEventSyncService;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/dt;-><init>(Lcom/android/calendar/FacebookEventSyncService;Lcom/android/calendar/dq;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/calendar/dt;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 427
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 428
    invoke-static {}, Lcom/android/calendar/FacebookEventSyncService;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 429
    const-string v0, "FBEventSyncService"

    const-string v1, "FacebookPartialQueryHandler stopSelf"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/dr;->a:Lcom/android/calendar/FacebookEventSyncService;

    invoke-virtual {v0}, Lcom/android/calendar/FacebookEventSyncService;->stopSelf()V

    goto :goto_0
.end method
