.class public Lcom/android/calendar/cj;
.super Ljava/lang/Object;
.source "DeleteEventHelper.java"


# static fields
.field private static j:Landroid/database/Cursor;

.field private static final k:[Ljava/lang/String;


# instance fields
.field public a:Landroid/app/DialogFragment;

.field private b:Landroid/app/Activity;

.field private c:J

.field private d:J

.field private e:Lcom/android/calendar/as;

.field private f:Z

.field private g:Ljava/lang/Runnable;

.field private h:Ljava/lang/Runnable;

.field private i:Lcom/android/calendar/cl;

.field private l:I

.field private m:Lcom/android/calendar/ag;

.field private n:Lcom/android/calendar/cq;

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "attendeeName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "attendeeEmail"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "attendeeRelationship"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "attendeeStatus"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/cj;->k:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object v0, p0, Lcom/android/calendar/cj;->n:Lcom/android/calendar/cq;

    .line 118
    iput-object v0, p0, Lcom/android/calendar/cj;->a:Landroid/app/DialogFragment;

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/cj;->o:Z

    .line 162
    if-nez p1, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    iput-object p1, p0, Lcom/android/calendar/cj;->b:Landroid/app/Activity;

    .line 167
    iput-boolean p2, p0, Lcom/android/calendar/cj;->f:Z

    .line 169
    new-instance v0, Lcom/android/calendar/ck;

    iget-object v1, p0, Lcom/android/calendar/cj;->b:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/ck;-><init>(Lcom/android/calendar/cj;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/cj;->m:Lcom/android/calendar/ag;

    .line 182
    return-void
.end method

.method private static a([Ljava/lang/Long;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 680
    .line 681
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p0, v1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 682
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 683
    add-int/lit8 v0, v0, 0x1

    .line 681
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 685
    :cond_1
    return v0
.end method

.method public static a()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/android/calendar/cj;->j:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 69
    sput-object p0, Lcom/android/calendar/cj;->j:Landroid/database/Cursor;

    return-object p0
.end method

.method static synthetic a(Lcom/android/calendar/cj;)Lcom/android/calendar/as;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/calendar/cj;->e:Lcom/android/calendar/as;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/cj;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/android/calendar/cj;->g:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic a(Landroid/app/Activity;[Ljava/lang/Long;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    invoke-static {p0, p1, p2}, Lcom/android/calendar/cj;->b(Landroid/app/Activity;[Ljava/lang/Long;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I[Ljava/lang/Long;Z)V
    .locals 3

    .prologue
    .line 408
    iget-object v0, p0, Lcom/android/calendar/cj;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 409
    iget-object v1, p0, Lcom/android/calendar/cj;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 410
    if-eqz v1, :cond_0

    .line 411
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 413
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 417
    sparse-switch p1, :sswitch_data_0

    .line 438
    :cond_1
    :goto_0
    return-void

    .line 419
    :sswitch_0
    invoke-static {}, Lcom/android/calendar/co;->a()Lcom/android/calendar/co;

    move-result-object v1

    .line 420
    const-string v2, "dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 424
    :sswitch_1
    new-instance v0, Lcom/android/calendar/cm;

    iget-object v1, p0, Lcom/android/calendar/cj;->b:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/cm;-><init>(Lcom/android/calendar/cj;Landroid/app/Activity;)V

    .line 425
    if-eqz v0, :cond_1

    .line 426
    invoke-virtual {v0, p2}, Lcom/android/calendar/cm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 431
    :sswitch_2
    new-instance v1, Lcom/android/calendar/cr;

    invoke-direct {v1, p0, p2, p3}, Lcom/android/calendar/cr;-><init>(Lcom/android/calendar/cj;[Ljava/lang/Long;Z)V

    .line 432
    const-string v2, "dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 417
    nop

    :sswitch_data_0
    .sparse-switch
        0x3ed -> :sswitch_1
        0x3f2 -> :sswitch_0
        0x3f7 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic a(Lcom/android/calendar/cj;I[Ljava/lang/Long;Z)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/cj;->a(I[Ljava/lang/Long;Z)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/cj;)J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/android/calendar/cj;->c:J

    return-wide v0
.end method

.method private static b(Landroid/app/Activity;[Ljava/lang/Long;Z)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 619
    .line 620
    const-string v0, ","

    invoke-static {v0, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 621
    new-array v2, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v6

    .line 624
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "rrule"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IS NOT NULL AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "rrule"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != \'\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "rdate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IS NOT NULL AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "rdate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != \'\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 626
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 627
    if-eqz v1, :cond_c

    .line 628
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 629
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 631
    :goto_0
    array-length v1, p1

    .line 632
    invoke-static {p1}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;)I

    move-result v2

    .line 633
    sub-int v3, v1, v2

    .line 635
    if-ne v0, v7, :cond_0

    .line 636
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0144

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 674
    :goto_1
    return-object v0

    .line 639
    :cond_0
    if-le v0, v7, :cond_1

    .line 640
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0145

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 644
    :cond_1
    if-ne v1, v7, :cond_3

    .line 645
    invoke-static {p1}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;)I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0156

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0158

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 650
    :cond_3
    if-eqz p2, :cond_6

    .line 651
    if-ne v1, v2, :cond_4

    .line 652
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f014f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 653
    :cond_4
    if-nez v2, :cond_5

    .line 654
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f014e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 656
    :cond_5
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f014d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 660
    :cond_6
    if-ne v1, v2, :cond_7

    .line 661
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0151

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 663
    :cond_7
    if-nez v2, :cond_8

    .line 664
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0150

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 667
    :cond_8
    if-ne v3, v7, :cond_9

    if-ne v2, v7, :cond_9

    .line 668
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0155

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 669
    :cond_9
    if-ne v3, v7, :cond_a

    .line 670
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0154

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 671
    :cond_a
    if-ne v2, v7, :cond_b

    .line 672
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0153

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 674
    :cond_b
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0152

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-virtual {v0, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_c
    move v0, v6

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/cj;)J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/android/calendar/cj;->d:J

    return-wide v0
.end method

.method static synthetic d(Lcom/android/calendar/cj;)Lcom/android/calendar/cq;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/calendar/cj;->n:Lcom/android/calendar/cq;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/cj;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/calendar/cj;->g:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/cj;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/calendar/cj;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/cj;)Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/android/calendar/cj;->f:Z

    return v0
.end method

.method static synthetic h(Lcom/android/calendar/cj;)Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/android/calendar/cj;->o:Z

    return v0
.end method

.method static synthetic i(Lcom/android/calendar/cj;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/calendar/cj;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/cj;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/android/calendar/cj;->l:I

    return v0
.end method


# virtual methods
.method public a(JJJI)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 213
    new-instance v2, Lcom/android/calendar/cl;

    iget-object v3, p0, Lcom/android/calendar/cj;->b:Landroid/app/Activity;

    invoke-direct {v2, p0, v3}, Lcom/android/calendar/cl;-><init>(Lcom/android/calendar/cj;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/calendar/cj;->i:Lcom/android/calendar/cl;

    .line 214
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p5

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 215
    iget-object v2, p0, Lcom/android/calendar/cj;->m:Lcom/android/calendar/ag;

    iget-object v3, p0, Lcom/android/calendar/cj;->m:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v3

    invoke-static {}, Lcom/android/calendar/event/av;->a()[Ljava/lang/String;

    move-result-object v6

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    invoke-virtual/range {v2 .. v9}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iput-wide p1, p0, Lcom/android/calendar/cj;->c:J

    .line 218
    iput-wide p3, p0, Lcom/android/calendar/cj;->d:J

    .line 219
    move/from16 v0, p7

    iput v0, p0, Lcom/android/calendar/cj;->l:I

    .line 220
    return-void
.end method

.method public a(JJJILjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 223
    iput-object p8, p0, Lcom/android/calendar/cj;->g:Ljava/lang/Runnable;

    .line 224
    invoke-virtual/range {p0 .. p7}, Lcom/android/calendar/cj;->a(JJJI)V

    .line 225
    return-void
.end method

.method public a(JJLcom/android/calendar/as;I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 266
    iput p6, p0, Lcom/android/calendar/cj;->l:I

    .line 267
    iput-wide p1, p0, Lcom/android/calendar/cj;->c:J

    .line 268
    iput-wide p3, p0, Lcom/android/calendar/cj;->d:J

    .line 269
    iput-object p5, p0, Lcom/android/calendar/cj;->e:Lcom/android/calendar/as;

    .line 272
    sget-object v3, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    .line 273
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/calendar/cj;->e:Lcom/android/calendar/as;

    iget-wide v4, v1, Lcom/android/calendar/as;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 276
    iget-object v0, p0, Lcom/android/calendar/cj;->i:Lcom/android/calendar/cl;

    const/4 v1, 0x2

    sget-object v4, Lcom/android/calendar/cj;->k:[Ljava/lang/String;

    const-string v5, "event_id=?"

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/cl;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method public a(Lcom/android/calendar/cq;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/android/calendar/cj;->n:Lcom/android/calendar/cq;

    .line 325
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 195
    iput-boolean p1, p0, Lcom/android/calendar/cj;->o:Z

    .line 196
    return-void
.end method

.method public a([Ljava/lang/Long;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 295
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;ZLjava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 296
    return-void
.end method

.method public a([Ljava/lang/Long;ZLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;ZLjava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 307
    return-void
.end method

.method public a([Ljava/lang/Long;ZLjava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 318
    iput-object p3, p0, Lcom/android/calendar/cj;->g:Ljava/lang/Runnable;

    .line 319
    iput-object p4, p0, Lcom/android/calendar/cj;->h:Ljava/lang/Runnable;

    .line 320
    const/16 v0, 0x3f7

    invoke-direct {p0, v0, p1, p2}, Lcom/android/calendar/cj;->a(I[Ljava/lang/Long;Z)V

    .line 321
    return-void
.end method

.method public b()Lcom/android/calendar/ca;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/cj;->a:Landroid/app/DialogFragment;

    check-cast v0, Lcom/android/calendar/ca;

    return-object v0
.end method
