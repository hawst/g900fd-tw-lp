.class Lcom/android/calendar/bc;
.super Ljava/lang/Object;
.source "CalendarSearchView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/android/calendar/CalendarSearchView;


# direct methods
.method constructor <init>(Lcom/android/calendar/CalendarSearchView;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 344
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->d(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/SearchView$OnQueryTextListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->a(Lcom/android/calendar/CalendarSearchView;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 347
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->d(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/SearchView$OnQueryTextListener;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v1}, Lcom/android/calendar/CalendarSearchView;->e(Lcom/android/calendar/CalendarSearchView;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextChange(Ljava/lang/String;)Z

    .line 348
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->b(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/AutoCompleteTextView;->setCursorVisible(Z)V

    .line 356
    :cond_0
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 357
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/calendar/CalendarSearchView;->a(Lcom/android/calendar/CalendarSearchView;J)J

    .line 358
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->f(Lcom/android/calendar/CalendarSearchView;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 362
    :goto_1
    return-void

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->a(Lcom/android/calendar/CalendarSearchView;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    if-ne v0, v1, :cond_2

    if-nez p3, :cond_2

    .line 350
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->b(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setCursorVisible(Z)V

    goto :goto_0

    .line 352
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->d(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/SearchView$OnQueryTextListener;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextChange(Ljava/lang/String;)Z

    .line 353
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->b(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setCursorVisible(Z)V

    goto :goto_0

    .line 360
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/bc;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->f(Lcom/android/calendar/CalendarSearchView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
