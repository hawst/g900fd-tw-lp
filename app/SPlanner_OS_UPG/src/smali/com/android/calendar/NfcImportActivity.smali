.class public Lcom/android/calendar/NfcImportActivity;
.super Landroid/app/Activity;
.source "NfcImportActivity.java"


# instance fields
.field private a:Landroid/nfc/NdefRecord;

.field private b:Z

.field private c:I

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    iput-boolean v1, p0, Lcom/android/calendar/NfcImportActivity;->b:Z

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/NfcImportActivity;->c:I

    .line 47
    const-string v0, "My task"

    iput-object v0, p0, Lcom/android/calendar/NfcImportActivity;->d:Ljava/lang/String;

    .line 48
    iput v1, p0, Lcom/android/calendar/NfcImportActivity;->e:I

    .line 119
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/NfcImportActivity;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/android/calendar/NfcImportActivity;->b:Z

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/NfcImportActivity;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/android/calendar/NfcImportActivity;->c:I

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/NfcImportActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/calendar/NfcImportActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/NfcImportActivity;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/android/calendar/NfcImportActivity;->e:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 116
    new-instance v0, Lcom/android/calendar/ff;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/ff;-><init>(Lcom/android/calendar/NfcImportActivity;Lcom/android/calendar/fe;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/nfc/NdefRecord;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/calendar/NfcImportActivity;->a:Landroid/nfc/NdefRecord;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/calendar/ff;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 117
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 96
    if-ne p1, v1, :cond_0

    .line 97
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 98
    iget-boolean v0, p0, Lcom/android/calendar/NfcImportActivity;->b:Z

    if-eqz v0, :cond_1

    .line 99
    const-string v0, "EXTRA_TASK_SYNC_ID"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/NfcImportActivity;->d:Ljava/lang/String;

    .line 101
    const-string v0, "EXTRA_TASK_SYNC_ACCOUNT_KEY"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/NfcImportActivity;->e:I

    .line 107
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/NfcImportActivity;->a()V

    .line 108
    invoke-virtual {p0}, Lcom/android/calendar/NfcImportActivity;->finish()V

    .line 113
    :cond_0
    :goto_1
    return-void

    .line 104
    :cond_1
    const-string v0, "EXTRA_CALENDAR_ACCOUNT_ID"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/NfcImportActivity;->c:I

    goto :goto_0

    .line 110
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/NfcImportActivity;->finish()V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lcom/android/calendar/NfcImportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 58
    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 59
    const-string v1, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_3

    .line 63
    aget-object v0, v0, v4

    check-cast v0, Landroid/nfc/NdefMessage;

    invoke-virtual {v0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/NfcImportActivity;->finish()V

    .line 67
    :cond_1
    aget-object v0, v0, v4

    .line 68
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v2

    const-string v3, "UTF8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 70
    const-string v2, "text/x-vCalendar"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 71
    iput-boolean v4, p0, Lcom/android/calendar/NfcImportActivity;->b:Z

    .line 78
    :goto_0
    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 79
    invoke-virtual {p0}, Lcom/android/calendar/NfcImportActivity;->finish()V

    .line 82
    :cond_2
    iput-object v0, p0, Lcom/android/calendar/NfcImportActivity;->a:Landroid/nfc/NdefRecord;

    .line 84
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/SelectAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    const-string v1, "EXTRA_IS_TASK"

    iget-boolean v2, p0, Lcom/android/calendar/NfcImportActivity;->b:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0, v0, v5}, Lcom/android/calendar/NfcImportActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 92
    :cond_3
    :goto_1
    return-void

    .line 72
    :cond_4
    const-string v2, "text/x-vtodo"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 73
    iput-boolean v5, p0, Lcom/android/calendar/NfcImportActivity;->b:Z

    goto :goto_0

    .line 75
    :cond_5
    invoke-virtual {p0}, Lcom/android/calendar/NfcImportActivity;->finish()V

    goto :goto_0

    .line 90
    :cond_6
    invoke-virtual {p0}, Lcom/android/calendar/NfcImportActivity;->finish()V

    goto :goto_1
.end method
