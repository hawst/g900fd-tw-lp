.class public Lcom/android/calendar/common/extension/a;
.super Landroid/app/DatePickerDialog;
.source "DatePickerDialogSec.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Z

.field private c:Z

.field private d:Landroid/view/View;

.field private e:Lcom/android/calendar/common/extension/j;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/android/calendar/d/g;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;IIILjava/lang/String;ZZ)V
    .locals 7

    .prologue
    const v6, 0x7f1200a1

    const v5, 0x7f1200a0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-direct/range {p0 .. p5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 36
    iput-boolean v2, p0, Lcom/android/calendar/common/extension/a;->h:Z

    .line 43
    iput-object p1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    .line 44
    iput-boolean p7, p0, Lcom/android/calendar/common/extension/a;->c:Z

    .line 45
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/a;->g:Lcom/android/calendar/d/g;

    .line 46
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->g:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iput-boolean v1, p0, Lcom/android/calendar/common/extension/a;->h:Z

    .line 48
    new-instance v0, Lcom/android/calendar/common/extension/j;

    iget-object v3, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/calendar/common/extension/a;->g:Lcom/android/calendar/d/g;

    invoke-direct {v0, v3, v4}, Lcom/android/calendar/common/extension/j;-><init>(Landroid/content/Context;Lcom/android/calendar/d/g;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/a;->e:Lcom/android/calendar/common/extension/j;

    .line 51
    :cond_0
    if-eqz p7, :cond_5

    .line 52
    iput-boolean p8, p0, Lcom/android/calendar/common/extension/a;->b:Z

    .line 54
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 55
    const v3, 0x7f04002d

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 57
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "sec-roboto-light"

    invoke-static {v3, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 59
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 60
    new-instance v3, Lcom/android/calendar/common/extension/b;

    invoke-direct {v3, p0}, Lcom/android/calendar/common/extension/b;-><init>(Lcom/android/calendar/common/extension/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/calendar/common/extension/a;->b:Z

    invoke-virtual {v3, v4}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 81
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/calendar/common/extension/a;->b:Z

    if-nez v4, :cond_3

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    .line 82
    iget-boolean v1, p0, Lcom/android/calendar/common/extension/a;->b:Z

    if-eqz v1, :cond_4

    .line 83
    const v1, 0x7f020074

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 84
    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    const v3, 0x7f0f012a

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 89
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/a;->setCustomTitle(Landroid/view/View;)V

    .line 98
    :goto_2
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v0

    .line 99
    invoke-virtual {v0, v2}, Landroid/widget/CalendarView;->setShowWeekNumber(Z)V

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CalendarView;->setSelectedWeekBackgroundColor(I)V

    .line 101
    const v1, 0x7f0b0049

    invoke-virtual {v0, v1}, Landroid/widget/CalendarView;->setSelectedDateVerticalBar(I)V

    .line 102
    const v1, 0x7f100021

    invoke-virtual {v0, v1}, Landroid/widget/CalendarView;->setWeekDayTextAppearance(I)V

    .line 103
    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CalendarView;->setFirstDayOfWeek(I)V

    .line 104
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v2, 0x140

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0xf0

    if-ne v1, v2, :cond_2

    .line 107
    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->f(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_2

    .line 108
    const v1, 0x7f10001b

    invoke-virtual {v0, v1}, Landroid/widget/CalendarView;->setDateTextAppearance(I)V

    .line 113
    :cond_2
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    const v2, 0x7f0f0041

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/android/calendar/common/extension/a;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 114
    const/4 v0, -0x2

    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    const v2, 0x7f0f00a4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/android/calendar/common/extension/a;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 115
    return-void

    :cond_3
    move v1, v2

    .line 81
    goto/16 :goto_0

    .line 86
    :cond_4
    const v1, 0x7f02006d

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 87
    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    const v3, 0x7f0f0129

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 91
    :cond_5
    invoke-virtual {p0, p6}, Lcom/android/calendar/common/extension/a;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method private a(III)V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->e:Lcom/android/calendar/common/extension/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/common/extension/j;->a(III)Ljava/lang/String;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/common/extension/a;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/a;->b:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/common/extension/a;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/android/calendar/common/extension/a;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/common/extension/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 157
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CalendarView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 158
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/common/extension/a;->f:Landroid/widget/TextView;

    .line 160
    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 162
    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->f:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->f:Landroid/widget/TextView;

    const/16 v1, 0x32

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 166
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/a;->c:Z

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 168
    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v5, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 169
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v3, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v1, v4, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 170
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    const v1, 0x7f1200a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 171
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 172
    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v3, v3

    sub-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 173
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    const v1, 0x7f1200a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 175
    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 176
    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v3, v3

    sub-float v2, v3, v2

    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 177
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->f:Landroid/widget/TextView;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 181
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getMonth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/common/extension/a;->a(III)V

    .line 182
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 137
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    .line 138
    iget-object v0, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 139
    invoke-virtual {v1}, Landroid/widget/DatePicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 140
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 141
    const/4 v2, -0x2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 142
    iget-object v2, p0, Lcom/android/calendar/common/extension/a;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c02a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 143
    invoke-virtual {v1, v0}, Landroid/widget/DatePicker;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    invoke-virtual {v1}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CalendarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 145
    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 146
    iget-object v1, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/common/extension/a;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v1, v2, v4, v0, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 148
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 127
    invoke-super {p0, p1}, Landroid/app/DatePickerDialog;->onCreate(Landroid/os/Bundle;)V

    .line 128
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/a;->c:Z

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->a()V

    .line 131
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/a;->h:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 132
    invoke-direct {p0}, Lcom/android/calendar/common/extension/a;->b()V

    .line 134
    :cond_1
    return-void
.end method

.method public onDateChanged(Landroid/widget/DatePicker;III)V
    .locals 2

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/a;->h:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 121
    invoke-direct {p0, p2, p3, p4}, Lcom/android/calendar/common/extension/a;->a(III)V

    .line 123
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/DatePicker;->clearFocus()V

    .line 153
    invoke-super {p0}, Landroid/app/DatePickerDialog;->onStart()V

    .line 154
    return-void
.end method
