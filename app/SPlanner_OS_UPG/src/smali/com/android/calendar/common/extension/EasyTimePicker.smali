.class public Lcom/android/calendar/common/extension/EasyTimePicker;
.super Landroid/widget/TimePicker;
.source "EasyTimePicker.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/TimePicker;-><init>(Landroid/content/Context;)V

    .line 17
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/EasyTimePicker;->isEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/EasyTimePicker;->a:Z

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/EasyTimePicker;->isEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/EasyTimePicker;->a:Z

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/EasyTimePicker;->isEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/EasyTimePicker;->a:Z

    .line 27
    return-void
.end method


# virtual methods
.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/EasyTimePicker;->a:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-super {p0, p1}, Landroid/widget/TimePicker;->onInterceptHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/EasyTimePicker;->a:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-super {p0, p1}, Landroid/widget/TimePicker;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 49
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/android/calendar/common/extension/EasyTimePicker;->a:Z

    .line 32
    if-eqz p1, :cond_0

    .line 33
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/EasyTimePicker;->setAlpha(F)V

    .line 34
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/EasyTimePicker;->setDescendantFocusability(I)V

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/EasyTimePicker;->setWritingBuddyEnabled(Z)V

    .line 42
    :goto_0
    return-void

    .line 37
    :cond_0
    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/EasyTimePicker;->setDescendantFocusability(I)V

    .line 38
    const v0, 0x3ecccccd    # 0.4f

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/EasyTimePicker;->setAlpha(F)V

    .line 39
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/EasyTimePicker;->clearFocus()V

    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/EasyTimePicker;->setWritingBuddyEnabled(Z)V

    goto :goto_0
.end method
