.class Lcom/android/calendar/common/extension/g;
.super Ljava/lang/Object;
.source "DateTimePickerDialogSec.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/common/extension/c;


# direct methods
.method constructor <init>(Lcom/android/calendar/common/extension/c;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 346
    iget-object v0, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/c;->a()Landroid/widget/DatePicker;

    move-result-object v3

    .line 347
    if-nez v3, :cond_0

    .line 366
    :goto_0
    return-void

    .line 350
    :cond_0
    iget-object v4, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    iget-object v0, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-static {v0}, Lcom/android/calendar/common/extension/c;->c(Lcom/android/calendar/common/extension/c;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v4, v0}, Lcom/android/calendar/common/extension/c;->a(Lcom/android/calendar/common/extension/c;Z)Z

    .line 351
    invoke-virtual {v3}, Landroid/widget/DatePicker;->getRootView()Landroid/view/View;

    move-result-object v0

    const v4, 0x7f1200a1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 352
    iget-object v4, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-static {v4}, Lcom/android/calendar/common/extension/c;->c(Lcom/android/calendar/common/extension/c;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 353
    const v4, 0x7f020074

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 354
    iget-object v4, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-static {v4}, Lcom/android/calendar/common/extension/c;->d(Lcom/android/calendar/common/extension/c;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0f012a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 355
    iget-object v0, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-static {v0}, Lcom/android/calendar/common/extension/c;->e(Lcom/android/calendar/common/extension/c;)Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/DatePicker;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    iget-object v0, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-static {v0}, Lcom/android/calendar/common/extension/c;->d(Lcom/android/calendar/common/extension/c;)Landroid/content/Context;

    move-result-object v0

    const-string v4, "input_method"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 357
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v0, v4, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 363
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-static {v0}, Lcom/android/calendar/common/extension/c;->c(Lcom/android/calendar/common/extension/c;)Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 364
    iget-object v0, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-static {v0}, Lcom/android/calendar/common/extension/c;->c(Lcom/android/calendar/common/extension/c;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_3
    invoke-virtual {v3, v1}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    .line 365
    iget-object v0, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/c;->b()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 350
    goto :goto_1

    .line 360
    :cond_3
    const v4, 0x7f02006d

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 361
    iget-object v4, p0, Lcom/android/calendar/common/extension/g;->a:Lcom/android/calendar/common/extension/c;

    invoke-static {v4}, Lcom/android/calendar/common/extension/c;->d(Lcom/android/calendar/common/extension/c;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0f0129

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    move v1, v2

    .line 364
    goto :goto_3
.end method
