.class public Lcom/android/calendar/common/extension/c;
.super Landroid/app/AlertDialog;
.source "DateTimePickerDialogSec.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/DatePicker$OnDateChangedListener;
.implements Landroid/widget/TimePicker$OnTimeChangedListener;


# instance fields
.field private A:Landroid/view/inputmethod/InputMethodManager;

.field private B:Lcom/android/calendar/common/extension/j;

.field private C:Landroid/widget/TextView;

.field private D:Lcom/android/calendar/d/g;

.field private E:Z

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Z

.field private final I:Landroid/os/Handler;

.field private J:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private K:Landroid/view/View$OnClickListener;

.field private a:I

.field private final b:Landroid/content/Context;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/DatePicker;

.field private f:Landroid/widget/TimePicker;

.field private g:Landroid/widget/DatePicker;

.field private h:Landroid/widget/TimePicker;

.field private i:Landroid/widget/DatePicker;

.field private j:Landroid/widget/TimePicker;

.field private final k:Lcom/android/calendar/common/extension/i;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Landroid/widget/RelativeLayout;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/ImageButton;

.field private u:Landroid/content/res/Resources;

.field private v:Landroid/text/format/Time;

.field private w:Landroid/text/format/Time;

.field private x:J

.field private y:J

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/android/calendar/common/extension/i;IIIIIZZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 276
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/common/extension/c;->a:I

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->l:Z

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->m:Z

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->n:Z

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->o:Z

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->p:Z

    .line 81
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/common/extension/c;->x:J

    .line 82
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/common/extension/c;->y:J

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->B:Lcom/android/calendar/common/extension/j;

    .line 89
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->D:Lcom/android/calendar/d/g;

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->E:Z

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->F:Ljava/lang/String;

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->G:Ljava/lang/String;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->H:Z

    .line 95
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->I:Landroid/os/Handler;

    .line 106
    new-instance v0, Lcom/android/calendar/common/extension/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/common/extension/d;-><init>(Lcom/android/calendar/common/extension/c;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->J:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 342
    new-instance v0, Lcom/android/calendar/common/extension/g;

    invoke-direct {v0, p0}, Lcom/android/calendar/common/extension/g;-><init>(Lcom/android/calendar/common/extension/c;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->K:Landroid/view/View$OnClickListener;

    .line 278
    iput-object p1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    .line 279
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->u:Landroid/content/res/Resources;

    .line 280
    iput-object p3, p0, Lcom/android/calendar/common/extension/c;->k:Lcom/android/calendar/common/extension/i;

    .line 281
    iput-boolean p9, p0, Lcom/android/calendar/common/extension/c;->n:Z

    .line 282
    iput-boolean p10, p0, Lcom/android/calendar/common/extension/c;->o:Z

    .line 284
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->D:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/common/extension/c;->a(Z)V

    .line 288
    :cond_0
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 290
    const v1, 0x7f04002f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    .line 291
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/android/calendar/common/extension/c;->setView(Landroid/view/View;)V

    .line 293
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f1200a5

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/DatePicker;

    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    .line 294
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    iget-boolean v2, p0, Lcom/android/calendar/common/extension/c;->l:Z

    invoke-virtual {v1, v2}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 295
    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    iget-boolean v1, p0, Lcom/android/calendar/common/extension/c;->l:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    .line 296
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v1, p4, p5, p6, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 298
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v1

    .line 302
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CalendarView;->setShowWeekNumber(Z)V

    .line 304
    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/CalendarView;->setFirstDayOfWeek(I)V

    .line 305
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CalendarView;->setSelectedWeekBackgroundColor(I)V

    .line 306
    const v2, 0x7f0b0049

    invoke-virtual {v1, v2}, Landroid/widget/CalendarView;->setSelectedDateVerticalBar(I)V

    .line 307
    const v2, 0x7f100021

    invoke-virtual {v1, v2}, Landroid/widget/CalendarView;->setWeekDayTextAppearance(I)V

    .line 309
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f1200a6

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TimePicker;

    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    .line 310
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    iget-boolean v2, p0, Lcom/android/calendar/common/extension/c;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 311
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 312
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 313
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v1, p0}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 314
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 315
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->J:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 318
    :cond_1
    if-eqz p10, :cond_3

    .line 319
    const v1, 0x7f040030

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    .line 320
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/c;->setCustomTitle(Landroid/view/View;)V

    .line 328
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    .line 329
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->l:Z

    if-eqz v0, :cond_4

    .line 331
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    const v1, 0x7f020074

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v2, 0x7f0f012a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 338
    :goto_2
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v2, 0x7f0f0041

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/android/calendar/common/extension/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 339
    const/4 v0, -0x2

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v2, 0x7f0f00a4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/android/calendar/common/extension/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 340
    return-void

    .line 295
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 322
    :cond_3
    const v1, 0x7f04002d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    .line 323
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "sec-roboto-light"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 325
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/c;->setCustomTitle(Landroid/view/View;)V

    goto :goto_1

    .line 334
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    const v1, 0x7f02006d

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 335
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v2, 0x7f0f0129

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/common/extension/i;IIIIIZZLjava/lang/String;)V
    .locals 12

    .prologue
    .line 119
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/android/calendar/common/extension/c;-><init>(Landroid/content/Context;ILcom/android/calendar/common/extension/i;IIIIIZZLjava/lang/String;)V

    .line 121
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/common/extension/i;Landroid/text/format/Time;Landroid/text/format/Time;ZZI)V
    .locals 7

    .prologue
    .line 129
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/common/extension/c;->a:I

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->l:Z

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->m:Z

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->n:Z

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->o:Z

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->p:Z

    .line 81
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/common/extension/c;->x:J

    .line 82
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/common/extension/c;->y:J

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->B:Lcom/android/calendar/common/extension/j;

    .line 89
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->D:Lcom/android/calendar/d/g;

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->E:Z

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->F:Ljava/lang/String;

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->G:Ljava/lang/String;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->H:Z

    .line 95
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->I:Landroid/os/Handler;

    .line 106
    new-instance v0, Lcom/android/calendar/common/extension/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/common/extension/d;-><init>(Lcom/android/calendar/common/extension/c;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->J:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 342
    new-instance v0, Lcom/android/calendar/common/extension/g;

    invoke-direct {v0, p0}, Lcom/android/calendar/common/extension/g;-><init>(Lcom/android/calendar/common/extension/c;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->K:Landroid/view/View$OnClickListener;

    .line 130
    iput-object p1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->u:Landroid/content/res/Resources;

    .line 132
    iput-object p2, p0, Lcom/android/calendar/common/extension/c;->k:Lcom/android/calendar/common/extension/i;

    .line 133
    iput-boolean p6, p0, Lcom/android/calendar/common/extension/c;->m:Z

    .line 134
    iput-boolean p5, p0, Lcom/android/calendar/common/extension/c;->n:Z

    .line 135
    iput p7, p0, Lcom/android/calendar/common/extension/c;->a:I

    .line 137
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->D:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/common/extension/c;->a(Z)V

    .line 141
    :cond_0
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 142
    const v1, 0x7f04002f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    .line 143
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/android/calendar/common/extension/c;->setView(Landroid/view/View;)V

    .line 145
    const v1, 0x7f040030

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    .line 146
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/common/extension/c;->setCustomTitle(Landroid/view/View;)V

    .line 148
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->q:Landroid/widget/RelativeLayout;

    .line 149
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->q:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 150
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->u:Landroid/content/res/Resources;

    const v2, 0x7f0c00b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->u:Landroid/content/res/Resources;

    const v3, 0x7f0c00b5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 152
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->q:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 154
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->r:Landroid/widget/Button;

    .line 155
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->s:Landroid/widget/Button;

    .line 158
    iget v0, p0, Lcom/android/calendar/common/extension/c;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 159
    iget v4, p3, Landroid/text/format/Time;->year:I

    .line 160
    iget v3, p3, Landroid/text/format/Time;->month:I

    .line 161
    iget v2, p3, Landroid/text/format/Time;->monthDay:I

    .line 162
    iget v1, p3, Landroid/text/format/Time;->hour:I

    .line 163
    iget v0, p3, Landroid/text/format/Time;->minute:I

    .line 164
    invoke-virtual {p0, v4, v3, v2}, Lcom/android/calendar/common/extension/c;->a(III)V

    .line 165
    iget-object v5, p0, Lcom/android/calendar/common/extension/c;->r:Landroid/widget/Button;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setSelected(Z)V

    .line 166
    iget-object v5, p0, Lcom/android/calendar/common/extension/c;->s:Landroid/widget/Button;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setSelected(Z)V

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    .line 177
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const-string v6, "input_method"

    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->A:Landroid/view/inputmethod/InputMethodManager;

    .line 178
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    const v6, 0x7f1200a5

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    .line 179
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    iget-boolean v6, p0, Lcom/android/calendar/common/extension/c;->l:Z

    invoke-virtual {v0, v6}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 180
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->l:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v6, v0}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    .line 181
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0, v5, v4, v3, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 183
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v0

    .line 187
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/CalendarView;->setShowWeekNumber(Z)V

    .line 189
    iget-object v3, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/CalendarView;->setFirstDayOfWeek(I)V

    .line 190
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0048

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CalendarView;->setSelectedWeekBackgroundColor(I)V

    .line 191
    const v3, 0x7f0b0049

    invoke-virtual {v0, v3}, Landroid/widget/CalendarView;->setSelectedDateVerticalBar(I)V

    .line 192
    const v3, 0x7f100021

    invoke-virtual {v0, v3}, Landroid/widget/CalendarView;->setWeekDayTextAppearance(I)V

    .line 194
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    const v3, 0x7f1200a6

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    .line 195
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    iget-boolean v3, p0, Lcom/android/calendar/common/extension/c;->n:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 196
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 197
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 198
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0, p0}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 199
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->J:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 203
    :cond_1
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->z:Ljava/lang/String;

    .line 205
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->z:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    .line 206
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    iget v1, p3, Landroid/text/format/Time;->second:I

    iget v2, p3, Landroid/text/format/Time;->minute:I

    iget v3, p3, Landroid/text/format/Time;->hour:I

    iget v4, p3, Landroid/text/format/Time;->monthDay:I

    iget v5, p3, Landroid/text/format/Time;->month:I

    iget v6, p3, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 208
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/common/extension/c;->x:J

    .line 210
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->m:Z

    if-eqz v0, :cond_6

    .line 211
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setVisibility(I)V

    .line 212
    iget v0, p0, Lcom/android/calendar/common/extension/c;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 213
    iget v0, p3, Landroid/text/format/Time;->hour:I

    iget v1, p3, Landroid/text/format/Time;->minute:I

    iget-boolean v2, p0, Lcom/android/calendar/common/extension/c;->n:Z

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/calendar/common/extension/c;->a(IIZ)V

    .line 221
    :goto_2
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->z:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    .line 222
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    iget v1, p4, Landroid/text/format/Time;->second:I

    iget v2, p4, Landroid/text/format/Time;->minute:I

    iget v3, p4, Landroid/text/format/Time;->hour:I

    iget v4, p4, Landroid/text/format/Time;->monthDay:I

    iget v5, p4, Landroid/text/format/Time;->month:I

    iget v6, p4, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 224
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/common/extension/c;->y:J

    .line 226
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->r:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/common/extension/e;

    invoke-direct {v1, p0}, Lcom/android/calendar/common/extension/e;-><init>(Lcom/android/calendar/common/extension/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->s:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/common/extension/f;

    invoke-direct {v1, p0}, Lcom/android/calendar/common/extension/f;-><init>(Lcom/android/calendar/common/extension/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    .line 251
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->l:Z

    if-eqz v0, :cond_7

    .line 253
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    const v1, 0x7f020074

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 254
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v2, 0x7f0f012a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 260
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->r:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setHoverPopupType(I)V

    .line 262
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->s:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setHoverPopupType(I)V

    .line 265
    :cond_2
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v2, 0x7f0f0041

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/android/calendar/common/extension/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 266
    const/4 v0, -0x2

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v2, 0x7f0f00a4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/android/calendar/common/extension/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 267
    return-void

    .line 168
    :cond_3
    iget v4, p4, Landroid/text/format/Time;->year:I

    .line 169
    iget v3, p4, Landroid/text/format/Time;->month:I

    .line 170
    iget v2, p4, Landroid/text/format/Time;->monthDay:I

    .line 171
    iget v1, p4, Landroid/text/format/Time;->hour:I

    .line 172
    iget v0, p4, Landroid/text/format/Time;->minute:I

    .line 173
    invoke-virtual {p0, v4, v3, v2}, Lcom/android/calendar/common/extension/c;->b(III)V

    .line 174
    iget-object v5, p0, Lcom/android/calendar/common/extension/c;->s:Landroid/widget/Button;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setSelected(Z)V

    .line 175
    iget-object v5, p0, Lcom/android/calendar/common/extension/c;->r:Landroid/widget/Button;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setSelected(Z)V

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto/16 :goto_0

    .line 180
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 215
    :cond_5
    iget v0, p4, Landroid/text/format/Time;->hour:I

    iget v1, p4, Landroid/text/format/Time;->minute:I

    iget-boolean v2, p0, Lcom/android/calendar/common/extension/c;->n:Z

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/calendar/common/extension/c;->b(IIZ)V

    goto/16 :goto_2

    .line 218
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setVisibility(I)V

    goto/16 :goto_2

    .line 256
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    const v1, 0x7f02006d

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 257
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->t:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v2, 0x7f0f0129

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method static synthetic a(Lcom/android/calendar/common/extension/c;I)I
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/android/calendar/common/extension/c;->a:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/common/extension/c;)Landroid/widget/TimePicker;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    return-object v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->E:Z

    .line 753
    new-instance v0, Lcom/android/calendar/common/extension/j;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->D:Lcom/android/calendar/d/g;

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/common/extension/j;-><init>(Landroid/content/Context;Lcom/android/calendar/d/g;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->B:Lcom/android/calendar/common/extension/j;

    .line 754
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0127

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->G:Ljava/lang/String;

    .line 755
    if-eqz p1, :cond_0

    .line 756
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->F:Ljava/lang/String;

    .line 757
    const/4 v0, -0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->G:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->F:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/android/calendar/common/extension/c;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 759
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/common/extension/c;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/android/calendar/common/extension/c;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/common/extension/c;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/android/calendar/common/extension/c;->a:I

    return v0
.end method

.method private c()V
    .locals 9

    .prologue
    .line 462
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->k:Lcom/android/calendar/common/extension/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->clearFocus()V

    .line 464
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->clearFocus()V

    .line 465
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->k:Lcom/android/calendar/common/extension/i;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    iget-object v3, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    iget-object v4, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getMonth()I

    move-result v4

    iget-object v5, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v6}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v7}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-boolean v8, p0, Lcom/android/calendar/common/extension/c;->H:Z

    invoke-interface/range {v0 .. v8}, Lcom/android/calendar/common/extension/i;->a(Landroid/widget/DatePicker;Landroid/widget/TimePicker;IIIIIZ)V

    .line 469
    :cond_0
    return-void
.end method

.method private c(III)V
    .locals 2

    .prologue
    .line 713
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->C:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->B:Lcom/android/calendar/common/extension/j;

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->B:Lcom/android/calendar/common/extension/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/common/extension/j;->a(III)Ljava/lang/String;

    move-result-object v0

    .line 715
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->C:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 717
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/common/extension/c;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->l:Z

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/common/extension/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    .line 472
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->clearFocus()V

    .line 474
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->clearFocus()V

    .line 475
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    .line 476
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    .line 477
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    .line 478
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 479
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 481
    iget v0, p0, Lcom/android/calendar/common/extension/c;->a:I

    const/4 v6, 0x1

    if-ne v0, v6, :cond_2

    move-object v0, p0

    .line 482
    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/common/extension/c;->b(IIIII)V

    .line 487
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->k:Lcom/android/calendar/common/extension/i;

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->k:Lcom/android/calendar/common/extension/i;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    iget-object v3, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    iget-object v4, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    iget-boolean v5, p0, Lcom/android/calendar/common/extension/c;->H:Z

    invoke-interface/range {v0 .. v5}, Lcom/android/calendar/common/extension/i;->a(Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Z)V

    .line 490
    :cond_1
    return-void

    .line 483
    :cond_2
    iget v0, p0, Lcom/android/calendar/common/extension/c;->a:I

    const/4 v6, 0x2

    if-ne v0, v6, :cond_0

    move-object v0, p0

    .line 484
    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/common/extension/c;->c(IIIII)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/common/extension/c;)Landroid/widget/DatePicker;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    return-object v0
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 720
    .line 722
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->C:Landroid/widget/TextView;

    .line 723
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/c;->a()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CalendarView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 724
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 725
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->C:Landroid/widget/TextView;

    const/16 v2, 0x32

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 726
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->C:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 727
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->C:Landroid/widget/TextView;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 732
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "font_scale"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 736
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    if-eqz v1, :cond_0

    const v1, 0x3f9851ec    # 1.19f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 737
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 738
    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v4, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 739
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 740
    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 741
    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v3, v3

    sub-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 742
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 743
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    const v1, 0x7f1200a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 744
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 745
    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v3, v3

    sub-float v2, v3, v2

    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 746
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 748
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/c;->a()Landroid/widget/DatePicker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/common/extension/c;->a()Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getMonth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/calendar/common/extension/c;->a()Landroid/widget/DatePicker;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/common/extension/c;->c(III)V

    .line 749
    return-void

    .line 733
    :catch_0
    move-exception v0

    .line 734
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/common/extension/c;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->p:Z

    return v0
.end method


# virtual methods
.method public a()Landroid/widget/DatePicker;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    return-object v0
.end method

.method public a(I)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 370
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->A:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->A:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/common/extension/c;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 371
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->A:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/android/calendar/common/extension/c;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 374
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    if-nez v1, :cond_2

    .line 435
    :cond_1
    :goto_0
    return-void

    .line 378
    :cond_2
    iput p1, p0, Lcom/android/calendar/common/extension/c;->a:I

    .line 380
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->clearFocus()V

    .line 381
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    .line 382
    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    .line 383
    iget-object v3, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    .line 386
    iget-boolean v4, p0, Lcom/android/calendar/common/extension/c;->m:Z

    if-eqz v4, :cond_a

    .line 387
    iget-object v4, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v4}, Landroid/widget/TimePicker;->clearFocus()V

    .line 388
    iget-object v4, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v4}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 389
    iget-object v5, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 392
    :goto_1
    iget v6, p0, Lcom/android/calendar/common/extension/c;->a:I

    if-ne v6, v9, :cond_6

    .line 393
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    if-nez v6, :cond_3

    .line 394
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    iget v6, v6, Landroid/text/format/Time;->year:I

    iget-object v7, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    iget v7, v7, Landroid/text/format/Time;->month:I

    iget-object v8, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    iget v8, v8, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/calendar/common/extension/c;->a(III)V

    .line 397
    :cond_3
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    if-nez v6, :cond_4

    .line 398
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    iget v6, v6, Landroid/text/format/Time;->hour:I

    iget-object v7, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    iget v7, v7, Landroid/text/format/Time;->minute:I

    iget-boolean v8, p0, Lcom/android/calendar/common/extension/c;->n:Z

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/calendar/common/extension/c;->a(IIZ)V

    .line 401
    :cond_4
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->r:Landroid/widget/Button;

    invoke-virtual {v6, v9}, Landroid/widget/Button;->setSelected(Z)V

    .line 402
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->s:Landroid/widget/Button;

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setSelected(Z)V

    move-object v0, p0

    .line 403
    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/common/extension/c;->c(IIIII)V

    .line 405
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->m:Z

    if-eqz v0, :cond_5

    .line 406
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/common/extension/c;->a(IIIII)V

    goto/16 :goto_0

    .line 409
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/common/extension/c;->a(IIIII)V

    goto/16 :goto_0

    .line 414
    :cond_6
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    if-nez v6, :cond_7

    .line 415
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    iget v6, v6, Landroid/text/format/Time;->year:I

    iget-object v7, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    iget v7, v7, Landroid/text/format/Time;->month:I

    iget-object v8, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    iget v8, v8, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/calendar/common/extension/c;->b(III)V

    .line 418
    :cond_7
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    if-nez v6, :cond_8

    .line 419
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    iget v6, v6, Landroid/text/format/Time;->hour:I

    iget-object v7, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    iget v7, v7, Landroid/text/format/Time;->minute:I

    iget-boolean v8, p0, Lcom/android/calendar/common/extension/c;->n:Z

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/calendar/common/extension/c;->b(IIZ)V

    .line 422
    :cond_8
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->s:Landroid/widget/Button;

    invoke-virtual {v6, v9}, Landroid/widget/Button;->setSelected(Z)V

    .line 423
    iget-object v6, p0, Lcom/android/calendar/common/extension/c;->r:Landroid/widget/Button;

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setSelected(Z)V

    move-object v0, p0

    .line 424
    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/common/extension/c;->b(IIIII)V

    .line 426
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->m:Z

    if-eqz v0, :cond_9

    .line 427
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/common/extension/c;->a(IIIII)V

    goto/16 :goto_0

    .line 431
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/common/extension/c;->a(IIIII)V

    goto/16 :goto_0

    :cond_a
    move v5, v0

    move v4, v0

    goto/16 :goto_1
.end method

.method public a(III)V
    .locals 2

    .prologue
    .line 575
    new-instance v0, Landroid/widget/DatePicker;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/DatePicker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    .line 576
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    invoke-virtual {v0, p1, p2, p3, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 578
    return-void
.end method

.method public a(IIIII)V
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 532
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 533
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 534
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->requestFocus()Z

    .line 535
    return-void
.end method

.method public a(IIZ)V
    .locals 2

    .prologue
    .line 586
    new-instance v0, Landroid/widget/TimePicker;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TimePicker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    .line 587
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 588
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 589
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 590
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v3, -0x2

    const/4 v4, 0x0

    .line 655
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    if-nez v0, :cond_1

    .line 710
    :cond_0
    :goto_0
    return-void

    .line 659
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->u:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 660
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->u:Landroid/content/res/Resources;

    const v2, 0x7f0c02a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 662
    iget-boolean v2, p0, Lcom/android/calendar/common/extension/c;->m:Z

    if-eqz v2, :cond_4

    .line 663
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 664
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 665
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 666
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 669
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->u:Landroid/content/res/Resources;

    const v2, 0x7f0c02a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 672
    :cond_2
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 673
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v1, v0}, Landroid/widget/DatePicker;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 681
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->I:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/common/extension/h;

    invoke-direct {v1, p0}, Lcom/android/calendar/common/extension/h;-><init>(Lcom/android/calendar/common/extension/c;)V

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 699
    :goto_2
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->p:Z

    if-nez v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CalendarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 702
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 703
    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 704
    iget-boolean v2, p0, Lcom/android/calendar/common/extension/c;->o:Z

    if-eqz v2, :cond_5

    .line 705
    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    invoke-virtual {v2, v1, v4, v0, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 675
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->d:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 676
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 677
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 678
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 679
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v1, v0}, Landroid/widget/DatePicker;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 693
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 694
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 695
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 696
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v1, v0}, Landroid/widget/DatePicker;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 707
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v1, v2, v4, v0, v4}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_0
.end method

.method public b(III)V
    .locals 2

    .prologue
    .line 581
    new-instance v0, Landroid/widget/DatePicker;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/DatePicker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    .line 582
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    invoke-virtual {v0, p1, p2, p3, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 583
    return-void
.end method

.method public b(IIIII)V
    .locals 7

    .prologue
    .line 542
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    if-eqz v0, :cond_1

    .line 547
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 548
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 551
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    const/4 v1, 0x0

    move v2, p5

    move v3, p4

    move v4, p3

    move v5, p2

    move v6, p1

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 552
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->v:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/common/extension/c;->x:J

    .line 553
    return-void
.end method

.method public b(IIZ)V
    .locals 2

    .prologue
    .line 593
    new-instance v0, Landroid/widget/TimePicker;

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TimePicker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    .line 594
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 595
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 596
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 597
    return-void
.end method

.method public c(IIIII)V
    .locals 7

    .prologue
    .line 561
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 567
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 570
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    const/4 v1, 0x0

    move v2, p5

    move v3, p4

    move v4, p3

    move v5, p2

    move v6, p1

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 571
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->w:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/common/extension/c;->y:J

    .line 572
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 440
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 441
    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    .line 442
    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    .line 443
    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->g:Landroid/widget/DatePicker;

    .line 444
    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->h:Landroid/widget/TimePicker;

    .line 445
    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->i:Landroid/widget/DatePicker;

    .line 446
    iput-object v1, p0, Lcom/android/calendar/common/extension/c;->j:Landroid/widget/TimePicker;

    .line 447
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 459
    :goto_0
    return-void

    .line 449
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->H:Z

    .line 450
    const/4 v0, -0x3

    if-ne p2, v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->E:Z

    if-eqz v0, :cond_1

    .line 451
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->H:Z

    .line 453
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->o:Z

    if-eqz v0, :cond_2

    .line 454
    invoke-direct {p0}, Lcom/android/calendar/common/extension/c;->d()V

    goto :goto_0

    .line 456
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/common/extension/c;->c()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 643
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 645
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->b:Landroid/content/Context;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/common/extension/c;->p:Z

    .line 647
    invoke-virtual {p0}, Lcom/android/calendar/common/extension/c;->b()V

    .line 648
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->E:Z

    if-eqz v0, :cond_0

    .line 649
    invoke-direct {p0}, Lcom/android/calendar/common/extension/c;->e()V

    .line 652
    :cond_0
    return-void
.end method

.method public onDateChanged(Landroid/widget/DatePicker;III)V
    .locals 2

    .prologue
    .line 505
    iget-boolean v0, p0, Lcom/android/calendar/common/extension/c;->E:Z

    if-eqz v0, :cond_1

    .line 506
    invoke-direct {p0, p2, p3, p4}, Lcom/android/calendar/common/extension/c;->c(III)V

    .line 510
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, p4, v1}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 629
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 630
    const-string v0, "year"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 631
    const-string v1, "month"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 632
    const-string v2, "day"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 633
    iget-object v3, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v3, v0, v1, v2, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 634
    const-string v0, "hour"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 635
    const-string v1, "minute"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 636
    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    const-string v3, "is24hour"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 637
    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 638
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 639
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 617
    invoke-super {p0}, Landroid/app/AlertDialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 618
    const-string v1, "year"

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getYear()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 619
    const-string v1, "month"

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 620
    const-string v1, "day"

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->e:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 621
    const-string v1, "hour"

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v2}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 622
    const-string v1, "minute"

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v2}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 623
    const-string v1, "is24hour"

    iget-object v2, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v2}, Landroid/widget/TimePicker;->is24HourView()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 624
    return-object v0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 494
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 495
    invoke-direct {p0}, Lcom/android/calendar/common/extension/c;->c()V

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 498
    iget-object v0, p0, Lcom/android/calendar/common/extension/c;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/common/extension/c;->J:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 500
    :cond_1
    invoke-super {p0}, Landroid/app/AlertDialog;->onStop()V

    .line 501
    return-void
.end method

.method public onTimeChanged(Landroid/widget/TimePicker;II)V
    .locals 0

    .prologue
    .line 515
    return-void
.end method
