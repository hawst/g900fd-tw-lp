.class public Lcom/android/calendar/common/extension/j;
.super Ljava/lang/Object;
.source "LunarDateToStingConv.java"


# static fields
.field private static e:Lcom/android/calendar/d/a/a;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/common/extension/j;->e:Lcom/android/calendar/d/a/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/d/g;)V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 22
    new-instance v1, Ljava/lang/String;

    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    iput-object v1, p0, Lcom/android/calendar/common/extension/j;->c:Ljava/lang/String;

    .line 23
    const v1, 0x7f0f0137

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/common/extension/j;->d:Ljava/lang/String;

    .line 24
    const v1, 0x7f0f0127

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/common/extension/j;->b:Ljava/lang/String;

    .line 25
    const v1, 0x7f0f0126

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/extension/j;->a:Ljava/lang/String;

    .line 26
    if-eqz p2, :cond_0

    .line 27
    invoke-virtual {p2}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/common/extension/j;->e:Lcom/android/calendar/d/a/a;

    .line 29
    :cond_0
    return-void
.end method


# virtual methods
.method public a(III)Ljava/lang/String;
    .locals 5

    .prologue
    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    sget-object v0, Lcom/android/calendar/common/extension/j;->e:Lcom/android/calendar/d/a/a;

    if-eqz v0, :cond_0

    .line 34
    sget-object v0, Lcom/android/calendar/common/extension/j;->e:Lcom/android/calendar/d/a/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 36
    sget-object v0, Lcom/android/calendar/common/extension/j;->e:Lcom/android/calendar/d/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/d/a/a;->b()I

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v2

    .line 37
    sget-object v0, Lcom/android/calendar/common/extension/j;->e:Lcom/android/calendar/d/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/d/a/a;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 39
    sget-object v0, Lcom/android/calendar/common/extension/j;->e:Lcom/android/calendar/d/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/d/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/common/extension/j;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    const-string v0, "DMY"

    iget-object v4, p0, Lcom/android/calendar/common/extension/j;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 41
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/common/extension/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/j;->a:Ljava/lang/String;

    goto :goto_0

    .line 43
    :cond_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/common/extension/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
