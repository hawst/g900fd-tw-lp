.class public abstract Lcom/android/calendar/common/a/a;
.super Ljava/lang/Object;
.source "BaseActionModeListener.java"

# interfaces
.implements Lcom/android/calendar/common/a/d;


# instance fields
.field private a:Landroid/content/Context;

.field protected b:I

.field protected c:Landroid/view/Menu;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/CheckBox;

.field private f:Landroid/widget/ImageView;

.field private final g:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/common/a/a;->b:I

    .line 139
    new-instance v0, Lcom/android/calendar/common/a/c;

    invoke-direct {v0, p0}, Lcom/android/calendar/common/a/c;-><init>(Lcom/android/calendar/common/a/a;)V

    iput-object v0, p0, Lcom/android/calendar/common/a/a;->g:Landroid/view/View$OnClickListener;

    .line 50
    iput-object p1, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/common/a/a;->b:I

    .line 139
    new-instance v0, Lcom/android/calendar/common/a/c;

    invoke-direct {v0, p0}, Lcom/android/calendar/common/a/c;-><init>(Lcom/android/calendar/common/a/a;)V

    iput-object v0, p0, Lcom/android/calendar/common/a/a;->g:Landroid/view/View$OnClickListener;

    .line 54
    iput-object p1, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    .line 55
    iput p2, p0, Lcom/android/calendar/common/a/a;->b:I

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/common/a/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/common/a/a;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->e:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method protected a(Z)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 127
    return-void
.end method

.method protected b(Z)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 131
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 132
    return-void
.end method

.method protected c(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 135
    iget-object v3, p0, Lcom/android/calendar/common/a/a;->e:Landroid/widget/CheckBox;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->f:Landroid/widget/ImageView;

    if-nez p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 137
    return-void

    :cond_0
    move v0, v2

    .line 135
    goto :goto_0

    :cond_1
    move v1, v2

    .line 136
    goto :goto_1
.end method

.method public i()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 105
    invoke-virtual {p0}, Lcom/android/calendar/common/a/a;->a()I

    move-result v0

    .line 106
    const-string v1, ""

    .line 107
    iget v1, p0, Lcom/android/calendar/common/a/a;->b:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 108
    iget-object v1, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    const v2, 0x7f0f0029

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 112
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/common/a/a;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 113
    return-void

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    const v2, 0x7f0f0026

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    iget v0, p0, Lcom/android/calendar/common/a/a;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4

    .prologue
    .line 60
    iput-object p2, p0, Lcom/android/calendar/common/a/a;->c:Landroid/view/Menu;

    .line 61
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/android/calendar/common/a/a;->f()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    .line 64
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/common/a/a;->a(Landroid/view/ActionMode;Landroid/view/Menu;)V

    .line 67
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    const v1, 0x7f10000e

    invoke-virtual {v0, v1}, Landroid/content/Context;->setTheme(I)V

    .line 68
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040093

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 69
    const v0, 0x7f1202a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/common/a/a;->d:Landroid/widget/LinearLayout;

    .line 70
    const v0, 0x7f1202a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/common/a/a;->e:Landroid/widget/CheckBox;

    .line 71
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->d:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/android/calendar/common/a/a;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    invoke-virtual {p1, v1}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 78
    const v0, 0x7f1202a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/common/a/a;->f:Landroid/widget/ImageView;

    .line 79
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->f:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/calendar/common/a/b;

    invoke-direct {v1, p0, p1}, Lcom/android/calendar/common/a/b;-><init>(Lcom/android/calendar/common/a/a;Landroid/view/ActionMode;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    invoke-virtual {p0}, Lcom/android/calendar/common/a/a;->i()V

    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/common/a/a;->c:Landroid/view/Menu;

    .line 123
    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 94
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/calendar/common/a/a;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v2

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/common/a/a;->g()V

    .line 98
    invoke-virtual {p0}, Lcom/android/calendar/common/a/a;->i()V

    .line 99
    iget-object v0, p0, Lcom/android/calendar/common/a/a;->e:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/android/calendar/common/a/a;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
