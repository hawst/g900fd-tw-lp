.class Lcom/android/calendar/common/extension/b;
.super Ljava/lang/Object;
.source "DatePickerDialogSec.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/common/extension/a;


# direct methods
.method constructor <init>(Lcom/android/calendar/common/extension/a;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v3

    .line 64
    iget-object v4, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    iget-object v0, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-static {v0}, Lcom/android/calendar/common/extension/a;->a(Lcom/android/calendar/common/extension/a;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v4, v0}, Lcom/android/calendar/common/extension/a;->a(Lcom/android/calendar/common/extension/a;Z)Z

    .line 65
    invoke-virtual {v3}, Landroid/widget/DatePicker;->getRootView()Landroid/view/View;

    move-result-object v0

    const v4, 0x7f1200a1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 66
    iget-object v4, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-static {v4}, Lcom/android/calendar/common/extension/a;->a(Lcom/android/calendar/common/extension/a;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 67
    const v4, 0x7f020074

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 68
    iget-object v4, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-static {v4}, Lcom/android/calendar/common/extension/a;->b(Lcom/android/calendar/common/extension/a;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0f012a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-static {v0}, Lcom/android/calendar/common/extension/a;->b(Lcom/android/calendar/common/extension/a;)Landroid/content/Context;

    move-result-object v0

    const-string v4, "input_method"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 70
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v0, v4, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 75
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-static {v0}, Lcom/android/calendar/common/extension/a;->a(Lcom/android/calendar/common/extension/a;)Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 76
    iget-object v0, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-static {v0}, Lcom/android/calendar/common/extension/a;->a(Lcom/android/calendar/common/extension/a;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    .line 77
    return-void

    :cond_0
    move v0, v2

    .line 64
    goto :goto_0

    .line 72
    :cond_1
    const v4, 0x7f02006d

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 73
    iget-object v4, p0, Lcom/android/calendar/common/extension/b;->a:Lcom/android/calendar/common/extension/a;

    invoke-static {v4}, Lcom/android/calendar/common/extension/a;->b(Lcom/android/calendar/common/extension/a;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0f0129

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 76
    goto :goto_2
.end method
