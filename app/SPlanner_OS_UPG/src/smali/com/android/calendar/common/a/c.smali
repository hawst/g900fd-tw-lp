.class Lcom/android/calendar/common/a/c;
.super Ljava/lang/Object;
.source "BaseActionModeListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/common/a/a;


# direct methods
.method constructor <init>(Lcom/android/calendar/common/a/a;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-static {v0}, Lcom/android/calendar/common/a/a;->b(Lcom/android/calendar/common/a/a;)Landroid/widget/CheckBox;

    move-result-object v0

    if-nez v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-static {v0}, Lcom/android/calendar/common/a/a;->b(Lcom/android/calendar/common/a/a;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->performClick()Z

    .line 147
    iget-object v0, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-static {v0}, Lcom/android/calendar/common/a/a;->b(Lcom/android/calendar/common/a/a;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 149
    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/a/a;->b()V

    .line 154
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/a/a;->i()V

    .line 155
    iget-object v0, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/a/a;->g()V

    .line 156
    iget-object v0, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/a/a;->d()V

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/common/a/c;->a:Lcom/android/calendar/common/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/a/a;->c()V

    goto :goto_1
.end method
