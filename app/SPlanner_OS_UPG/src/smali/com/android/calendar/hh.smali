.class public Lcom/android/calendar/hh;
.super Ljava/lang/Object;
.source "TaskEventModel.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public A:Z

.field public B:Ljava/lang/String;

.field public C:Z

.field public D:Ljava/lang/String;

.field public E:J

.field public F:I

.field public G:Z

.field public H:Z

.field public I:Z

.field public a:Ljava/lang/String;

.field public b:J

.field public c:J

.field public d:I

.field public e:I

.field public f:Ljava/lang/String;

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:I

.field public k:Ljava/lang/Long;

.field public l:Ljava/lang/Long;

.field public m:Z

.field public n:Z

.field public o:I

.field public p:I

.field public q:I

.field public r:J

.field public s:J

.field public t:Z

.field public u:I

.field public v:J

.field public w:I

.field public x:I

.field public y:I

.field public z:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v2, p0, Lcom/android/calendar/hh;->a:Ljava/lang/String;

    .line 28
    iput-wide v6, p0, Lcom/android/calendar/hh;->b:J

    .line 29
    iput-wide v6, p0, Lcom/android/calendar/hh;->c:J

    .line 30
    iput v1, p0, Lcom/android/calendar/hh;->d:I

    .line 31
    iput v1, p0, Lcom/android/calendar/hh;->e:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/hh;->f:Ljava/lang/String;

    .line 33
    iput v1, p0, Lcom/android/calendar/hh;->g:I

    .line 35
    iput-object v2, p0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    .line 36
    iput-object v2, p0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    .line 37
    iput v3, p0, Lcom/android/calendar/hh;->j:I

    .line 39
    iput-object v2, p0, Lcom/android/calendar/hh;->k:Ljava/lang/Long;

    .line 40
    iput-object v2, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    .line 42
    iput-boolean v1, p0, Lcom/android/calendar/hh;->m:Z

    .line 43
    iput-boolean v1, p0, Lcom/android/calendar/hh;->n:Z

    .line 48
    iput v3, p0, Lcom/android/calendar/hh;->o:I

    .line 56
    iput v1, p0, Lcom/android/calendar/hh;->p:I

    .line 57
    iput v1, p0, Lcom/android/calendar/hh;->q:I

    .line 58
    iput-wide v4, p0, Lcom/android/calendar/hh;->r:J

    .line 59
    iput-wide v4, p0, Lcom/android/calendar/hh;->s:J

    .line 61
    iput-boolean v1, p0, Lcom/android/calendar/hh;->t:Z

    .line 62
    iput v1, p0, Lcom/android/calendar/hh;->u:I

    .line 63
    iput-wide v4, p0, Lcom/android/calendar/hh;->v:J

    .line 64
    iput v1, p0, Lcom/android/calendar/hh;->w:I

    .line 65
    iput v1, p0, Lcom/android/calendar/hh;->x:I

    .line 66
    iput v1, p0, Lcom/android/calendar/hh;->y:I

    .line 67
    iput-wide v4, p0, Lcom/android/calendar/hh;->z:J

    .line 70
    iput-boolean v1, p0, Lcom/android/calendar/hh;->A:Z

    .line 71
    iput-object v2, p0, Lcom/android/calendar/hh;->B:Ljava/lang/String;

    .line 74
    iput-boolean v1, p0, Lcom/android/calendar/hh;->C:Z

    .line 75
    iput-object v2, p0, Lcom/android/calendar/hh;->D:Ljava/lang/String;

    .line 76
    iput-wide v4, p0, Lcom/android/calendar/hh;->E:J

    .line 82
    iput v1, p0, Lcom/android/calendar/hh;->F:I

    .line 84
    iput-boolean v1, p0, Lcom/android/calendar/hh;->G:Z

    .line 89
    iput-boolean v3, p0, Lcom/android/calendar/hh;->I:Z

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    .line 96
    invoke-direct {p0, p1}, Lcom/android/calendar/hh;-><init>(Landroid/content/Context;)V

    .line 99
    if-nez p2, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    const-string v0, "title"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_2

    .line 105
    iput-object v0, p0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    .line 108
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_3

    .line 111
    const-string v1, "selected"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/hh;->b:J

    .line 112
    const-string v1, "copyEvent"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/hh;->m:Z

    .line 115
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/hh;->m:Z

    if-ne v0, v4, :cond_4

    iget-wide v0, p0, Lcom/android/calendar/hh;->b:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_4

    .line 116
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/hh;->F:I

    .line 124
    :goto_1
    const-string v0, "action_memo_id"

    invoke-virtual {p2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/hh;->E:J

    .line 125
    iget-wide v0, p0, Lcom/android/calendar/hh;->E:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 126
    iput-boolean v4, p0, Lcom/android/calendar/hh;->C:Z

    .line 128
    const-string v0, "action_event_title"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/hh;->D:Ljava/lang/String;

    .line 129
    iget-object v0, p0, Lcom/android/calendar/hh;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/android/calendar/hh;->D:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/hh;->D:Ljava/lang/String;

    goto :goto_0

    .line 117
    :cond_4
    iget-boolean v0, p0, Lcom/android/calendar/hh;->m:Z

    if-eq v0, v4, :cond_5

    iget-wide v0, p0, Lcom/android/calendar/hh;->b:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_5

    .line 118
    iput v4, p0, Lcom/android/calendar/hh;->F:I

    goto :goto_1

    .line 120
    :cond_5
    iput v5, p0, Lcom/android/calendar/hh;->F:I

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 137
    iput-object v3, p0, Lcom/android/calendar/hh;->a:Ljava/lang/String;

    .line 138
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/hh;->b:J

    .line 139
    iput v2, p0, Lcom/android/calendar/hh;->d:I

    .line 140
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/hh;->f:Ljava/lang/String;

    .line 142
    iput-object v3, p0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    .line 143
    iput-object v3, p0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    .line 145
    iput-object v3, p0, Lcom/android/calendar/hh;->k:Ljava/lang/Long;

    .line 146
    iput-object v3, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    .line 148
    iput v6, p0, Lcom/android/calendar/hh;->j:I

    .line 149
    iput v6, p0, Lcom/android/calendar/hh;->o:I

    .line 151
    iput v2, p0, Lcom/android/calendar/hh;->p:I

    .line 152
    iput v2, p0, Lcom/android/calendar/hh;->q:I

    .line 153
    iput-wide v4, p0, Lcom/android/calendar/hh;->r:J

    .line 154
    iput-wide v4, p0, Lcom/android/calendar/hh;->s:J

    .line 155
    iput-boolean v2, p0, Lcom/android/calendar/hh;->t:Z

    .line 157
    iput v2, p0, Lcom/android/calendar/hh;->u:I

    .line 158
    iput-wide v4, p0, Lcom/android/calendar/hh;->v:J

    .line 159
    iput v2, p0, Lcom/android/calendar/hh;->w:I

    .line 160
    iput v2, p0, Lcom/android/calendar/hh;->x:I

    .line 162
    iput-boolean v2, p0, Lcom/android/calendar/hh;->H:Z

    .line 164
    iput v2, p0, Lcom/android/calendar/hh;->y:I

    .line 165
    iput-wide v4, p0, Lcom/android/calendar/hh;->z:J

    .line 167
    return-void
.end method

.method public a(Lcom/android/calendar/hh;Z)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 171
    if-nez p1, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v0

    .line 175
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 176
    goto :goto_0

    .line 179
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 180
    goto :goto_0

    .line 183
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    iget-object v3, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 185
    goto :goto_0

    .line 188
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    if-eqz v2, :cond_5

    move v0, v1

    .line 189
    goto :goto_0

    .line 190
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    if-eqz v2, :cond_6

    iget-object v2, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    if-nez v2, :cond_6

    move v0, v1

    .line 191
    goto :goto_0

    .line 196
    :cond_6
    iget-wide v2, p0, Lcom/android/calendar/hh;->r:J

    iget-wide v4, p1, Lcom/android/calendar/hh;->r:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 197
    goto :goto_0

    .line 201
    :cond_7
    iget v2, p0, Lcom/android/calendar/hh;->d:I

    iget v3, p1, Lcom/android/calendar/hh;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 202
    goto :goto_0

    .line 205
    :cond_8
    iget v2, p0, Lcom/android/calendar/hh;->o:I

    iget v3, p1, Lcom/android/calendar/hh;->o:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 206
    goto :goto_0

    .line 209
    :cond_9
    iget v2, p0, Lcom/android/calendar/hh;->p:I

    iget v3, p1, Lcom/android/calendar/hh;->p:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 210
    goto :goto_0

    .line 213
    :cond_a
    iget-boolean v2, p0, Lcom/android/calendar/hh;->C:Z

    if-eqz v2, :cond_b

    move v0, v1

    .line 214
    goto :goto_0

    .line 217
    :cond_b
    iget v2, p1, Lcom/android/calendar/hh;->F:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_c

    iget v2, p1, Lcom/android/calendar/hh;->F:I

    if-nez v2, :cond_0

    :cond_c
    if-nez p2, :cond_0

    move v0, v1

    .line 219
    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    const/4 v0, 0x1

    .line 230
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
