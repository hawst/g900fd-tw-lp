.class public Lcom/android/calendar/SelectAccountActivity;
.super Landroid/app/Activity;
.source "SelectAccountActivity.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;


# instance fields
.field private c:Z

.field private d:Lcom/android/calendar/gp;

.field private e:Landroid/database/Cursor;

.field private f:Ljava/util/ArrayList;

.field private g:Lcom/android/calendar/gq;

.field private h:Landroid/database/Cursor;

.field private i:Ljava/util/ArrayList;

.field private j:Ljava/util/ArrayList;

.field private k:Landroid/widget/ListAdapter;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:I

.field private o:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "calendar_displayName"

    aput-object v1, v0, v4

    const-string v1, "ownerAccount"

    aput-object v1, v0, v5

    const-string v1, "calendar_color"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "account_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/SelectAccountActivity;->a:[Ljava/lang/String;

    .line 63
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_sync_account"

    aput-object v1, v0, v4

    const-string v1, "_sync_account_key"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/SelectAccountActivity;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 84
    iput-boolean v2, p0, Lcom/android/calendar/SelectAccountActivity;->c:Z

    .line 86
    iput-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->d:Lcom/android/calendar/gp;

    .line 90
    iput-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->g:Lcom/android/calendar/gq;

    .line 94
    iput-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->k:Landroid/widget/ListAdapter;

    .line 96
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/SelectAccountActivity;->l:I

    .line 97
    const-string v0, "My task"

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->m:Ljava/lang/String;

    .line 98
    iput v2, p0, Lcom/android/calendar/SelectAccountActivity;->n:I

    .line 100
    iput-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->o:Landroid/app/AlertDialog;

    .line 270
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/SelectAccountActivity;I)I
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/android/calendar/SelectAccountActivity;->n:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/SelectAccountActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/android/calendar/SelectAccountActivity;->m:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->o:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 174
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 175
    iget-boolean v1, p0, Lcom/android/calendar/SelectAccountActivity;->c:Z

    if-eqz v1, :cond_1

    .line 176
    const v1, 0x7f0f0428

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 180
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->k:Landroid/widget/ListAdapter;

    const/4 v2, 0x0

    new-instance v3, Lcom/android/calendar/gn;

    invoke-direct {v3, p0}, Lcom/android/calendar/gn;-><init>(Lcom/android/calendar/SelectAccountActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 198
    new-instance v1, Lcom/android/calendar/go;

    invoke-direct {v1, p0}, Lcom/android/calendar/go;-><init>(Lcom/android/calendar/SelectAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 206
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->o:Landroid/app/AlertDialog;

    goto :goto_0

    .line 178
    :cond_1
    const v1, 0x7f0f01a8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/SelectAccountActivity;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/android/calendar/SelectAccountActivity;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/SelectAccountActivity;I)I
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/android/calendar/SelectAccountActivity;->l:I

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/SelectAccountActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 210
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 211
    iget-boolean v1, p0, Lcom/android/calendar/SelectAccountActivity;->c:Z

    if-eqz v1, :cond_0

    .line 212
    const-string v1, "EXTRA_TASK_SYNC_ID"

    iget-object v2, p0, Lcom/android/calendar/SelectAccountActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    const-string v1, "EXTRA_TASK_SYNC_ACCOUNT_KEY"

    iget v2, p0, Lcom/android/calendar/SelectAccountActivity;->n:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 217
    :goto_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/SelectAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 218
    return-void

    .line 215
    :cond_0
    const-string v1, "EXTRA_CALENDAR_ACCOUNT_ID"

    iget v2, p0, Lcom/android/calendar/SelectAccountActivity;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/SelectAccountActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 221
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 222
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/SelectAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 223
    return-void
.end method

.method static synthetic d(Lcom/android/calendar/SelectAccountActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/SelectAccountActivity;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/calendar/SelectAccountActivity;->b()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/SelectAccountActivity;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/calendar/SelectAccountActivity;->c()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 166
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 167
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 104
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/android/calendar/SelectAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 107
    const-string v1, "EXTRA_IS_TASK"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/SelectAccountActivity;->c:Z

    .line 109
    iget-boolean v0, p0, Lcom/android/calendar/SelectAccountActivity;->c:Z

    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {p0}, Lcom/android/calendar/SelectAccountActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/fv;->g:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/SelectAccountActivity;->b:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    .line 112
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v10, :cond_4

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->i:Ljava/util/ArrayList;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->j:Ljava/util/ArrayList;

    .line 118
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 122
    iget-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->j:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 125
    new-instance v0, Lcom/android/calendar/gq;

    iget-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->h:Landroid/database/Cursor;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/gq;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->g:Lcom/android/calendar/gq;

    .line 126
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->g:Lcom/android/calendar/gq;

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->k:Landroid/widget/ListAdapter;

    .line 127
    invoke-direct {p0}, Lcom/android/calendar/SelectAccountActivity;->a()V

    .line 154
    :goto_1
    return-void

    .line 131
    :cond_1
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 132
    invoke-virtual {p0}, Lcom/android/calendar/SelectAccountActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/android/calendar/SelectAccountActivity;->a:[Ljava/lang/String;

    const-string v7, "calendar_access_level>=500 AND account_name!=\'docomo\' AND deleted!=1"

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    .line 137
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v10, :cond_4

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->f:Ljava/util/ArrayList;

    .line 141
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 142
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 143
    iget-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->f:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 134
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/SelectAccountActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/android/calendar/SelectAccountActivity;->a:[Ljava/lang/String;

    const-string v7, "calendar_access_level>=500"

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    goto :goto_2

    .line 145
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 146
    new-instance v0, Lcom/android/calendar/gp;

    iget-object v1, p0, Lcom/android/calendar/SelectAccountActivity;->e:Landroid/database/Cursor;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/gp;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->d:Lcom/android/calendar/gp;

    .line 147
    iget-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->d:Lcom/android/calendar/gp;

    iput-object v0, p0, Lcom/android/calendar/SelectAccountActivity;->k:Landroid/widget/ListAdapter;

    .line 148
    invoke-direct {p0}, Lcom/android/calendar/SelectAccountActivity;->a()V

    goto :goto_1

    .line 152
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/SelectAccountActivity;->b()V

    .line 153
    invoke-virtual {p0}, Lcom/android/calendar/SelectAccountActivity;->finish()V

    goto :goto_1
.end method
