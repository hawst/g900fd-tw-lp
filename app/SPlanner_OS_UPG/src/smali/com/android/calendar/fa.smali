.class Lcom/android/calendar/fa;
.super Ljava/lang/Object;
.source "MiniCalendarFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/ez;


# direct methods
.method constructor <init>(Lcom/android/calendar/ez;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/android/calendar/fa;->a:Lcom/android/calendar/ez;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/format/Time;

    .line 142
    iget v1, v0, Landroid/text/format/Time;->year:I

    const/16 v2, 0x76e

    if-lt v1, v2, :cond_0

    iget v1, v0, Landroid/text/format/Time;->year:I

    const/16 v2, 0x7f4

    if-le v1, v2, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    sput v3, Lcom/android/calendar/ez;->a:I

    .line 147
    iget-object v1, p0, Lcom/android/calendar/fa;->a:Lcom/android/calendar/ez;

    invoke-static {v1, v0}, Lcom/android/calendar/ez;->a(Lcom/android/calendar/ez;Landroid/text/format/Time;)V

    .line 148
    iget-object v1, p0, Lcom/android/calendar/fa;->a:Lcom/android/calendar/ez;

    invoke-static {v1}, Lcom/android/calendar/ez;->a(Lcom/android/calendar/ez;)Landroid/app/Activity;

    move-result-object v1

    const-string v2, "%Y"

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 149
    sput-boolean v3, Lcom/android/calendar/month/by;->ao:Z

    .line 150
    iget-object v0, p0, Lcom/android/calendar/fa;->a:Lcom/android/calendar/ez;

    invoke-static {v0}, Lcom/android/calendar/ez;->b(Lcom/android/calendar/ez;)V

    goto :goto_0
.end method
