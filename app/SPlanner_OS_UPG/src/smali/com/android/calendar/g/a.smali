.class public Lcom/android/calendar/g/a;
.super Ljava/lang/Object;
.source "PackageUtils.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/android/calendar/g/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/g/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/android/calendar/g/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lcom/android/calendar/g/a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public static a(Lcom/android/calendar/g/c;Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 43
    if-nez p2, :cond_0

    sget-object v0, Lcom/android/calendar/g/a;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    sget-object v0, Lcom/android/calendar/g/a;->b:Ljava/lang/String;

    invoke-interface {p0, v0}, Lcom/android/calendar/g/c;->a(Ljava/lang/String;)V

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    new-instance v0, Lcom/android/calendar/g/b;

    invoke-direct {v0, p1, p0}, Lcom/android/calendar/g/b;-><init>(Landroid/content/Context;Lcom/android/calendar/g/c;)V

    .line 49
    invoke-virtual {v0}, Lcom/android/calendar/g/b;->start()V

    goto :goto_0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/android/calendar/g/a;->b:Ljava/lang/String;

    return-object v0
.end method
