.class public Lcom/android/calendar/g/d;
.super Ljava/lang/Object;
.source "ScreenUtils.java"


# static fields
.field private static a:I

.field private static b:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/g/d;->a:I

    .line 14
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Lcom/android/calendar/g/d;->b:Landroid/graphics/Point;

    return-void
.end method

.method public static a(Landroid/app/Activity;)Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 31
    sget-object v0, Lcom/android/calendar/g/d;->b:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/g/d;->b:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-lez v0, :cond_0

    .line 32
    sget-object v0, Lcom/android/calendar/g/d;->b:Landroid/graphics/Point;

    .line 38
    :goto_0
    return-object v0

    .line 34
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 35
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 36
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 37
    sput-object v1, Lcom/android/calendar/g/d;->b:Landroid/graphics/Point;

    .line 38
    sget-object v0, Lcom/android/calendar/g/d;->b:Landroid/graphics/Point;

    goto :goto_0
.end method
