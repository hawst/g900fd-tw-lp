.class public Lcom/android/calendar/g/a/a;
.super Ljava/lang/Object;
.source "HashByLongBoolean.java"


# instance fields
.field private a:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    .line 36
    invoke-direct {p0}, Lcom/android/calendar/g/a/a;->g()V

    .line 37
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    .line 44
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 51
    invoke-direct {p0}, Lcom/android/calendar/g/a/a;->g()V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 54
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(JZ)V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 76
    invoke-direct {p0}, Lcom/android/calendar/g/a/a;->g()V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/android/calendar/g/a/a;->d()[J

    move-result-object v0

    .line 140
    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 143
    :goto_0
    return v0

    :cond_0
    array-length v0, v0

    goto :goto_0
.end method

.method public b(J)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 114
    iget-object v1, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    if-nez v1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public c()Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 154
    const/4 v0, 0x0

    .line 162
    :goto_0
    return-object v0

    .line 156
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 157
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 158
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 162
    goto :goto_0
.end method

.method public c(J)Z
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/android/calendar/g/a/a;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/android/calendar/g/a/a;->f()Lcom/android/calendar/g/a/a;

    move-result-object v0

    return-object v0
.end method

.method public d()[J
    .locals 8

    .prologue
    .line 171
    .line 172
    invoke-virtual {p0}, Lcom/android/calendar/g/a/a;->c()Ljava/util/ArrayList;

    move-result-object v3

    .line 173
    if-nez v3, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 181
    :goto_0
    return-object v0

    .line 176
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [J

    .line 177
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 178
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    .line 179
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v1, v2

    .line 178
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 181
    goto :goto_0
.end method

.method public e()[Ljava/lang/Long;
    .locals 5

    .prologue
    .line 190
    .line 191
    invoke-virtual {p0}, Lcom/android/calendar/g/a/a;->c()Ljava/util/ArrayList;

    move-result-object v3

    .line 192
    if-nez v3, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 200
    :goto_0
    return-object v0

    .line 195
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/Long;

    .line 196
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 197
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    .line 198
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    aput-object v0, v1, v2

    .line 197
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 200
    goto :goto_0
.end method

.method public f()Lcom/android/calendar/g/a/a;
    .locals 7

    .prologue
    .line 205
    new-instance v0, Lcom/android/calendar/g/a/a;

    invoke-direct {v0}, Lcom/android/calendar/g/a/a;-><init>()V

    .line 206
    invoke-virtual {p0}, Lcom/android/calendar/g/a/a;->d()[J

    move-result-object v2

    .line 207
    if-eqz v2, :cond_0

    array-length v1, v2

    if-nez v1, :cond_1

    .line 214
    :cond_0
    return-object v0

    .line 210
    :cond_1
    array-length v3, v2

    .line 211
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 212
    aget-wide v4, v2, v1

    const/4 v6, 0x1

    invoke-virtual {v0, v4, v5, v6}, Lcom/android/calendar/g/a/a;->a(JZ)V

    .line 211
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
