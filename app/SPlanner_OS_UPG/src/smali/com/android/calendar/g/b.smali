.class Lcom/android/calendar/g/b;
.super Ljava/lang/Thread;
.source "PackageUtils.java"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;

.field private b:Lcom/android/calendar/g/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/g/c;)V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/android/calendar/g/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 59
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/calendar/g/b;->a:Ljava/lang/ref/WeakReference;

    .line 60
    iput-object p2, p0, Lcom/android/calendar/g/b;->b:Lcom/android/calendar/g/c;

    .line 61
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 65
    iget-object v0, p0, Lcom/android/calendar/g/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 66
    invoke-static {}, Lcom/android/calendar/g/a;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "context ref is null"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/g/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/g/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/g/a;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :cond_2
    :goto_1
    invoke-static {}, Lcom/android/calendar/g/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 81
    iget-object v0, p0, Lcom/android/calendar/g/b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0f005e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/g/a;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 84
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/g/b;->b:Lcom/android/calendar/g/c;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/android/calendar/g/b;->b:Lcom/android/calendar/g/c;

    invoke-static {}, Lcom/android/calendar/g/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/calendar/g/c;->a(Ljava/lang/String;)V

    .line 86
    iput-object v3, p0, Lcom/android/calendar/g/b;->b:Lcom/android/calendar/g/c;

    .line 87
    iput-object v3, p0, Lcom/android/calendar/g/b;->a:Ljava/lang/ref/WeakReference;

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    goto :goto_1
.end method
