.class public Lcom/android/calendar/g/h;
.super Ljava/lang/Object;
.source "WindowUtils.java"


# direct methods
.method public static a(Landroid/view/Window;)V
    .locals 1

    .prologue
    .line 13
    if-nez p0, :cond_0

    .line 17
    :goto_0
    return-void

    .line 16
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/Window;->requestFeature(I)Z

    goto :goto_0
.end method

.method public static b(Landroid/view/Window;)V
    .locals 1

    .prologue
    .line 20
    if-nez p0, :cond_0

    .line 25
    :goto_0
    return-void

    .line 23
    :cond_0
    const/high16 v0, 0x280000

    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method public static c(Landroid/view/Window;)V
    .locals 1

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 33
    :goto_0
    return-void

    .line 31
    :cond_0
    const/high16 v0, 0x280000

    invoke-virtual {p0, v0}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method public static d(Landroid/view/Window;)V
    .locals 1

    .prologue
    .line 43
    if-nez p0, :cond_0

    .line 47
    :goto_0
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method
