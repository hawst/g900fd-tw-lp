.class Lcom/android/calendar/y;
.super Ljava/lang/Object;
.source "AllInOneActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/AllInOneActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/AllInOneActivity;)V
    .locals 0

    .prologue
    .line 1092
    iput-object p1, p0, Lcom/android/calendar/y;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 11

    .prologue
    const/16 v10, 0x35

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1095
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1096
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1098
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1099
    invoke-virtual {p1, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1101
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1102
    iget-object v2, p0, Lcom/android/calendar/y;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1104
    iget-object v3, p0, Lcom/android/calendar/y;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 1105
    iget-object v4, p0, Lcom/android/calendar/y;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v4}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1106
    const v5, 0x7f0c00a1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 1107
    const v6, 0x7f0c00a2

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 1108
    iget-object v6, p0, Lcom/android/calendar/y;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v6}, Lcom/android/calendar/hj;->v(Landroid/content/Context;)I

    move-result v6

    .line 1109
    iget-object v7, p0, Lcom/android/calendar/y;->a:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v7}, Lcom/android/calendar/AllInOneActivity;->g(Lcom/android/calendar/AllInOneActivity;)I

    move-result v7

    if-ne v7, v9, :cond_0

    .line 1111
    aget v0, v0, v8

    sub-int v0, v2, v0

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    sub-int/2addr v0, v5

    sub-int v1, v6, v4

    invoke-virtual {v3, v10, v0, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 1119
    :goto_0
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1120
    return v9

    .line 1115
    :cond_0
    aget v0, v0, v8

    sub-int v0, v2, v0

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    add-int/2addr v0, v5

    sub-int v1, v6, v4

    invoke-virtual {v3, v10, v0, v1}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0
.end method
