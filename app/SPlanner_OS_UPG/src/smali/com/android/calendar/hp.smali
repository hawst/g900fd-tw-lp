.class Lcom/android/calendar/hp;
.super Ljava/lang/Object;
.source "WeatherSettingDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/hm;


# direct methods
.method constructor <init>(Lcom/android/calendar/hm;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/android/calendar/hp;->a:Lcom/android/calendar/hm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 94
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/hp;->a:Lcom/android/calendar/hm;

    invoke-virtual {v1, v0}, Lcom/android/calendar/hm;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/hp;->a:Lcom/android/calendar/hm;

    invoke-static {v0}, Lcom/android/calendar/hm;->a(Lcom/android/calendar/hm;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/android/calendar/hp;->a:Lcom/android/calendar/hm;

    invoke-static {v0}, Lcom/android/calendar/hm;->b(Lcom/android/calendar/hm;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_weather_settings"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 107
    :goto_1
    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 110
    :cond_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/hp;->a:Lcom/android/calendar/hm;

    invoke-static {v0}, Lcom/android/calendar/hm;->b(Lcom/android/calendar/hm;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_weather_settings"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_1
.end method
