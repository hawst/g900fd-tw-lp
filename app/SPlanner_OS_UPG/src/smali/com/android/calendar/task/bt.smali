.class Lcom/android/calendar/task/bt;
.super Landroid/content/AsyncQueryHandler;
.source "TaskWindowAdapter.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/task/bq;


# direct methods
.method public constructor <init>(Lcom/android/calendar/task/bq;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    .line 575
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 576
    return-void
.end method

.method private a(Lcom/android/calendar/task/bu;Landroid/database/Cursor;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 722
    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v0}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v3

    monitor-enter v3

    .line 723
    :try_start_0
    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 725
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    iget v2, p1, Lcom/android/calendar/task/bu;->f:I

    invoke-static {v0, v2}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;I)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 731
    if-nez v0, :cond_1

    .line 732
    new-instance v0, Lcom/android/calendar/task/bs;

    iget-object v2, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v2}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/calendar/task/bs;-><init>(Landroid/content/Context;)V

    move-object v2, v0

    .line 738
    :goto_0
    iget v0, p1, Lcom/android/calendar/task/bu;->c:I

    iput v0, v2, Lcom/android/calendar/task/bs;->d:I

    .line 739
    iget v0, p1, Lcom/android/calendar/task/bu;->d:I

    iput v0, v2, Lcom/android/calendar/task/bs;->e:I

    .line 740
    iput-object p2, v2, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    .line 741
    iget-object v0, v2, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, v2}, Lcom/android/calendar/task/ai;->a(Lcom/android/calendar/task/bs;)V

    .line 742
    iget-object v0, v2, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0}, Lcom/android/calendar/task/ai;->getCount()I

    move-result v0

    iput v0, v2, Lcom/android/calendar/task/bs;->g:I

    .line 745
    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v0}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v4, p1, Lcom/android/calendar/task/bu;->d:I

    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v0}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->d:I

    if-gt v4, v0, :cond_2

    .line 747
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v0}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 748
    iget v0, v2, Lcom/android/calendar/task/bs;->g:I

    add-int/2addr v0, v1

    move v1, v0

    .line 758
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/calendar/task/bq;->c(Lcom/android/calendar/task/bq;I)I

    .line 759
    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v0}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 760
    iget-object v4, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->j(Lcom/android/calendar/task/bq;)I

    move-result v4

    iput v4, v0, Lcom/android/calendar/task/bs;->f:I

    .line 761
    iget-object v4, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    iget v0, v0, Lcom/android/calendar/task/bs;->g:I

    invoke-static {v4, v0}, Lcom/android/calendar/task/bq;->d(Lcom/android/calendar/task/bq;I)I

    goto :goto_2

    .line 765
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 766
    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 734
    :cond_1
    :try_start_3
    iget v1, v0, Lcom/android/calendar/task/bs;->g:I

    neg-int v1, v1

    move-object v2, v0

    goto :goto_0

    .line 755
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v0}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_1

    .line 763
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;Lcom/android/calendar/task/bs;)Lcom/android/calendar/task/bs;

    .line 764
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return v1
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 20

    .prologue
    .line 581
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v18

    monitor-enter v18

    .line 582
    :try_start_0
    check-cast p2, Lcom/android/calendar/task/bu;

    .line 589
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->c(Lcom/android/calendar/task/bq;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p3, :cond_0

    .line 590
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 591
    monitor-exit v18

    .line 713
    :goto_0
    return-void

    .line 595
    :cond_0
    if-nez p3, :cond_1

    .line 596
    monitor-exit v18

    goto :goto_0

    .line 712
    :catchall_0
    move-exception v4

    monitor-exit v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 598
    :cond_1
    :try_start_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v19

    .line 599
    if-gtz v19, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/task/bu;->f:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_b

    .line 600
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/task/bt;->a(Lcom/android/calendar/task/bu;Landroid/database/Cursor;)I

    move-result v4

    .line 601
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/calendar/task/bu;->b:Landroid/text/format/Time;

    if-nez v5, :cond_a

    .line 602
    if-eqz v4, :cond_3

    .line 603
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v5}, Lcom/android/calendar/task/bq;->d(Lcom/android/calendar/task/bq;)Lcom/android/calendar/task/TaskListView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/android/calendar/task/TaskListView;->a(I)V

    .line 615
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->f(Lcom/android/calendar/task/bq;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_6

    .line 616
    const/4 v4, 0x0

    .line 617
    const/4 v5, -0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 618
    :cond_4
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 619
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v5}, Lcom/android/calendar/task/bq;->f(Lcom/android/calendar/task/bq;)J

    move-result-wide v6

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_4

    .line 621
    const/4 v4, 0x1

    .line 626
    :cond_5
    if-nez v4, :cond_6

    .line 627
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    const-wide/16 v6, -0x1

    invoke-static {v4, v6, v7}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;J)J

    .line 630
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->f(Lcom/android/calendar/task/bq;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 631
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;J)J

    .line 636
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    new-instance v5, Lcom/android/calendar/task/ah;

    invoke-direct {v5}, Lcom/android/calendar/task/ah;-><init>()V

    invoke-static {v4, v5}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;Lcom/android/calendar/task/ah;)Lcom/android/calendar/task/ah;

    .line 638
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/calendar/task/bq;->d(I)J

    move-result-wide v8

    .line 639
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->g(Lcom/android/calendar/task/bq;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 640
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v4

    const-wide/16 v6, 0x2

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    .line 651
    :cond_7
    :goto_2
    if-eqz v19, :cond_c

    .line 653
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->h(Lcom/android/calendar/task/bq;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/task/bu;

    .line 654
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;I)I

    .line 691
    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->h(Lcom/android/calendar/task/bq;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 692
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 693
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/task/bu;

    .line 694
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    iget v7, v4, Lcom/android/calendar/task/bu;->c:I

    iget v8, v4, Lcom/android/calendar/task/bu;->d:I

    invoke-static {v6, v7, v8}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;II)Z

    move-result v6

    if-nez v6, :cond_f

    .line 699
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v5, v4}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;Lcom/android/calendar/task/bu;)V

    .line 709
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-virtual {v4}, Lcom/android/calendar/task/bq;->notifyDataSetChanged()V

    .line 710
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    if-nez v19, :cond_10

    const/4 v4, 0x1

    :goto_5
    invoke-static {v5, v4}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;Z)V

    .line 711
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 712
    monitor-exit v18

    goto/16 :goto_0

    .line 606
    :cond_a
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/task/bu;->b:Landroid/text/format/Time;

    .line 607
    new-instance v8, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v5}, Lcom/android/calendar/task/bq;->e(Lcom/android/calendar/task/bq;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v8, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 608
    invoke-virtual {v8, v4}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 609
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bq;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v4

    const-wide/16 v6, 0x400

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    move-object/from16 v5, p0

    move-object v9, v8

    invoke-virtual/range {v4 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto/16 :goto_1

    .line 645
    :cond_b
    if-eqz p3, :cond_7

    .line 646
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 656
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->h(Lcom/android/calendar/task/bq;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/task/bu;

    .line 657
    if-eqz v4, :cond_8

    .line 659
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v5}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_e

    .line 660
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v5}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/calendar/task/bs;

    .line 661
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v6}, Lcom/android/calendar/task/bq;->b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/calendar/task/bs;

    .line 662
    iget v7, v5, Lcom/android/calendar/task/bs;->d:I

    add-int/lit8 v7, v7, -0x1

    iget v8, v4, Lcom/android/calendar/task/bu;->d:I

    if-gt v7, v8, :cond_d

    iget v7, v4, Lcom/android/calendar/task/bu;->c:I

    iget v8, v5, Lcom/android/calendar/task/bs;->d:I

    if-ge v7, v8, :cond_d

    .line 663
    iget v7, v4, Lcom/android/calendar/task/bu;->c:I

    iput v7, v5, Lcom/android/calendar/task/bs;->d:I

    .line 665
    :cond_d
    iget v5, v4, Lcom/android/calendar/task/bu;->c:I

    iget v7, v6, Lcom/android/calendar/task/bs;->e:I

    add-int/lit8 v7, v7, 0x1

    if-gt v5, v7, :cond_e

    iget v5, v6, Lcom/android/calendar/task/bs;->e:I

    iget v7, v4, Lcom/android/calendar/task/bu;->d:I

    if-ge v5, v7, :cond_e

    .line 666
    iget v5, v4, Lcom/android/calendar/task/bu;->d:I

    iput v5, v6, Lcom/android/calendar/task/bs;->e:I

    .line 672
    :cond_e
    iget v5, v4, Lcom/android/calendar/task/bu;->f:I

    packed-switch v5, :pswitch_data_0

    .line 684
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->i(Lcom/android/calendar/task/bq;)I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_8

    .line 686
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bt;->a:Lcom/android/calendar/task/bq;

    invoke-static {v4}, Lcom/android/calendar/task/bq;->h(Lcom/android/calendar/task/bq;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    goto/16 :goto_3

    .line 674
    :pswitch_0
    iget v5, v4, Lcom/android/calendar/task/bu;->c:I

    add-int/lit8 v5, v5, -0x3c

    iput v5, v4, Lcom/android/calendar/task/bu;->c:I

    goto :goto_6

    .line 677
    :pswitch_1
    iget v5, v4, Lcom/android/calendar/task/bu;->d:I

    add-int/lit8 v5, v5, 0x3c

    iput v5, v4, Lcom/android/calendar/task/bu;->d:I

    goto :goto_6

    .line 680
    :pswitch_2
    iget v5, v4, Lcom/android/calendar/task/bu;->c:I

    add-int/lit8 v5, v5, -0x1e

    iput v5, v4, Lcom/android/calendar/task/bu;->c:I

    .line 681
    iget v5, v4, Lcom/android/calendar/task/bu;->d:I

    add-int/lit8 v5, v5, 0x1e

    iput v5, v4, Lcom/android/calendar/task/bu;->d:I

    goto :goto_6

    .line 703
    :cond_f
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 710
    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 672
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
