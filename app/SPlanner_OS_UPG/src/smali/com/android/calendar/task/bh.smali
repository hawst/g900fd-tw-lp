.class Lcom/android/calendar/task/bh;
.super Landroid/app/DialogFragment;
.source "TaskDeleteFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/task/az;


# direct methods
.method private constructor <init>(Lcom/android/calendar/task/az;)V
    .locals 0

    .prologue
    .line 413
    iput-object p1, p0, Lcom/android/calendar/task/bh;->a:Lcom/android/calendar/task/az;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/task/az;Lcom/android/calendar/task/ba;)V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0, p1}, Lcom/android/calendar/task/bh;-><init>(Lcom/android/calendar/task/az;)V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 416
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/android/calendar/task/bh;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 417
    iget-object v0, p0, Lcom/android/calendar/task/bh;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->f(Lcom/android/calendar/task/az;)Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f040033

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 418
    const v0, 0x7f1200b9

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 419
    const v2, 0x7f0f03d0

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(I)V

    .line 420
    new-instance v2, Lcom/android/calendar/task/bi;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/task/bi;-><init>(Lcom/android/calendar/task/bh;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 427
    iget-object v0, p0, Lcom/android/calendar/task/bh;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->i(Lcom/android/calendar/task/az;)Ljava/lang/String;

    move-result-object v0

    .line 428
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/task/bh;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 429
    const v2, 0x7f0f0159

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 430
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 431
    const v0, 0x7f0f0149

    new-instance v2, Lcom/android/calendar/task/bj;

    invoke-direct {v2, p0}, Lcom/android/calendar/task/bj;-><init>(Lcom/android/calendar/task/bh;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 439
    const/high16 v0, 0x1040000

    new-instance v2, Lcom/android/calendar/task/bk;

    invoke-direct {v2, p0}, Lcom/android/calendar/task/bk;-><init>(Lcom/android/calendar/task/bh;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 446
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 455
    invoke-virtual {p0}, Lcom/android/calendar/task/bh;->dismiss()V

    .line 456
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 457
    return-void
.end method
