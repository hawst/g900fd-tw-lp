.class Lcom/android/calendar/task/bd;
.super Ljava/lang/Object;
.source "TaskDeleteFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/task/az;


# direct methods
.method constructor <init>(Lcom/android/calendar/task/az;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 230
    const v0, 0x7f1201e3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 232
    if-nez p3, :cond_0

    .line 233
    iget-object v1, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v1}, Lcom/android/calendar/task/az;->d(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    .line 234
    :goto_0
    iget-object v4, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v4}, Lcom/android/calendar/task/az;->d(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 235
    iget-object v4, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-virtual {v4, v1}, Lcom/android/calendar/task/az;->b(Z)V

    .line 237
    :cond_0
    if-ne p3, v2, :cond_1

    .line 238
    iget-object v1, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v1}, Lcom/android/calendar/task/az;->g(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    .line 239
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v4}, Lcom/android/calendar/task/az;->g(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 240
    iget-object v4, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-virtual {v4, v1}, Lcom/android/calendar/task/az;->c(Z)V

    .line 242
    :cond_1
    if-eqz v0, :cond_3

    .line 243
    iget-object v1, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v1}, Lcom/android/calendar/task/az;->h(Lcom/android/calendar/task/az;)Lcom/android/calendar/task/TaskDeleteView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getHeaderViewsCount()I

    move-result v4

    .line 244
    iget-object v1, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v1}, Lcom/android/calendar/task/az;->h(Lcom/android/calendar/task/az;)Lcom/android/calendar/task/TaskDeleteView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/au;

    .line 246
    invoke-virtual {v1}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v5

    .line 247
    sub-int v4, p3, v4

    invoke-virtual {v1, v4}, Lcom/android/calendar/task/au;->d(I)J

    move-result-wide v6

    .line 248
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_6

    .line 249
    :goto_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 251
    iget-object v1, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v1}, Lcom/android/calendar/task/az;->c(Lcom/android/calendar/task/az;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 252
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 254
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v1, v0}, Lcom/android/calendar/task/az;->a(Lcom/android/calendar/task/az;Z)V

    .line 256
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->e(Lcom/android/calendar/task/az;)V

    .line 257
    iget-object v0, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->f(Lcom/android/calendar/task/az;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 258
    iget-object v0, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->f(Lcom/android/calendar/task/az;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/TaskDeleteActivity;

    iget-object v1, p0, Lcom/android/calendar/task/bd;->a:Lcom/android/calendar/task/az;

    invoke-virtual {v1}, Lcom/android/calendar/task/az;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/TaskDeleteActivity;->a(I)V

    .line 259
    return-void

    :cond_4
    move v1, v3

    .line 233
    goto/16 :goto_0

    :cond_5
    move v1, v3

    .line 238
    goto/16 :goto_1

    :cond_6
    move v2, v3

    .line 248
    goto :goto_2
.end method
