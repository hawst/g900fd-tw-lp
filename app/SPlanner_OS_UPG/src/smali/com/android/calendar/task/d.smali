.class Lcom/android/calendar/task/d;
.super Landroid/widget/ArrayAdapter;
.source "GroupChangeOrderFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;


# instance fields
.field final synthetic a:Lcom/android/calendar/task/a;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/task/a;Landroid/content/Context;II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 235
    iput-object p1, p0, Lcom/android/calendar/task/d;->a:Lcom/android/calendar/task/a;

    .line 236
    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 232
    iput v0, p0, Lcom/android/calendar/task/d;->b:I

    .line 233
    iput v0, p0, Lcom/android/calendar/task/d;->c:I

    .line 237
    iput p3, p0, Lcom/android/calendar/task/d;->b:I

    .line 238
    iput p4, p0, Lcom/android/calendar/task/d;->c:I

    .line 239
    return-void
.end method


# virtual methods
.method public allowDrag(I)Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x1

    return v0
.end method

.method public allowDrop(II)Z
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x1

    return v0
.end method

.method public dropDone(II)V
    .locals 2

    .prologue
    .line 275
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/d;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/e;

    .line 281
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/calendar/task/d;->setNotifyOnChange(Z)V

    .line 283
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/d;->remove(Ljava/lang/Object;)V

    .line 284
    invoke-virtual {p0, v0, p2}, Lcom/android/calendar/task/d;->insert(Ljava/lang/Object;I)V

    .line 286
    invoke-virtual {p0}, Lcom/android/calendar/task/d;->notifyDataSetChanged()V

    .line 287
    iget-object v0, p0, Lcom/android/calendar/task/d;->a:Lcom/android/calendar/task/a;

    invoke-static {v0}, Lcom/android/calendar/task/a;->b(Lcom/android/calendar/task/a;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 288
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 243
    .line 244
    if-nez p2, :cond_0

    .line 245
    iget-object v0, p0, Lcom/android/calendar/task/d;->a:Lcom/android/calendar/task/a;

    invoke-static {v0}, Lcom/android/calendar/task/a;->b(Lcom/android/calendar/task/a;)Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/task/d;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 250
    :cond_0
    const v0, 0x7f1201e4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 251
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/d;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/e;

    .line 252
    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    .line 261
    :cond_1
    :goto_0
    return-object p2

    .line 255
    :cond_2
    invoke-virtual {v1}, Lcom/android/calendar/task/e;->b()Ljava/lang/String;

    move-result-object v1

    .line 256
    if-eqz v1, :cond_3

    const-string v2, "Default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 257
    iget-object v1, p0, Lcom/android/calendar/task/d;->a:Lcom/android/calendar/task/a;

    invoke-static {v1}, Lcom/android/calendar/task/a;->b(Lcom/android/calendar/task/a;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f009c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 259
    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    iget v1, p0, Lcom/android/calendar/task/d;->c:I

    add-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method
