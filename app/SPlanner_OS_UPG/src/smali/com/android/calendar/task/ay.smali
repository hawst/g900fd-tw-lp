.class Lcom/android/calendar/task/ay;
.super Ljava/lang/Object;
.source "TaskDeleteAdapter.java"


# instance fields
.field a:J

.field b:Landroid/text/format/Time;

.field c:I

.field d:I

.field e:Ljava/lang/String;

.field f:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput p1, p0, Lcom/android/calendar/task/ay;->f:I

    .line 189
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 211
    if-ne p0, p1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return v0

    .line 213
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 214
    goto :goto_0

    .line 215
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 216
    goto :goto_0

    .line 217
    :cond_3
    check-cast p1, Lcom/android/calendar/task/ay;

    .line 218
    iget v2, p0, Lcom/android/calendar/task/ay;->d:I

    iget v3, p1, Lcom/android/calendar/task/ay;->d:I

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lcom/android/calendar/task/ay;->a:J

    iget-wide v4, p1, Lcom/android/calendar/task/ay;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget v2, p0, Lcom/android/calendar/task/ay;->f:I

    iget v3, p1, Lcom/android/calendar/task/ay;->f:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/android/calendar/task/ay;->c:I

    iget v3, p1, Lcom/android/calendar/task/ay;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/calendar/task/ay;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/calendar/task/ay;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 221
    goto :goto_0

    .line 224
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/task/ay;->b:Landroid/text/format/Time;

    if-eqz v2, :cond_6

    .line 225
    iget-object v2, p0, Lcom/android/calendar/task/ay;->b:Landroid/text/format/Time;

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p1, Lcom/android/calendar/task/ay;->b:Landroid/text/format/Time;

    invoke-virtual {v4, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 226
    goto :goto_0

    .line 229
    :cond_6
    iget-object v2, p1, Lcom/android/calendar/task/ay;->b:Landroid/text/format/Time;

    if-eqz v2, :cond_0

    move v0, v1

    .line 230
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 193
    .line 195
    iget v0, p0, Lcom/android/calendar/task/ay;->d:I

    add-int/lit8 v0, v0, 0x1f

    .line 196
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/android/calendar/task/ay;->a:J

    iget-wide v4, p0, Lcom/android/calendar/task/ay;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 197
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/android/calendar/task/ay;->f:I

    add-int/2addr v0, v1

    .line 198
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/android/calendar/task/ay;->c:I

    add-int/2addr v0, v1

    .line 199
    iget-object v1, p0, Lcom/android/calendar/task/ay;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 200
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/android/calendar/task/ay;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/task/ay;->b:Landroid/text/format/Time;

    if-eqz v1, :cond_1

    .line 203
    iget-object v1, p0, Lcom/android/calendar/task/ay;->b:Landroid/text/format/Time;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 204
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 206
    :cond_1
    return v0
.end method
