.class public Lcom/android/calendar/task/o;
.super Landroid/app/DialogFragment;
.source "GroupDeleteFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 314
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/android/calendar/task/o;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 319
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 320
    const v0, 0x7f0f0146

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 322
    invoke-virtual {p0}, Lcom/android/calendar/task/o;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0147

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f02e3

    new-instance v3, Lcom/android/calendar/task/q;

    invoke-direct {v3, p0}, Lcom/android/calendar/task/q;-><init>(Lcom/android/calendar/task/o;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/task/p;

    invoke-direct {v3, p0}, Lcom/android/calendar/task/p;-><init>(Lcom/android/calendar/task/o;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 336
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
