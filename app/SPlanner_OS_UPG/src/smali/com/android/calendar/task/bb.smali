.class Lcom/android/calendar/task/bb;
.super Ljava/lang/Object;
.source "TaskDeleteFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/task/az;


# direct methods
.method constructor <init>(Lcom/android/calendar/task/az;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->c(Lcom/android/calendar/task/az;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->d(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->d(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 196
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-static {v1}, Lcom/android/calendar/task/az;->d(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 197
    iget-object v1, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/az;->b(Z)V

    .line 198
    iget-object v0, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->e(Lcom/android/calendar/task/az;)V

    .line 199
    iget-object v0, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->f(Lcom/android/calendar/task/az;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 200
    iget-object v0, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->f(Lcom/android/calendar/task/az;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/TaskDeleteActivity;

    iget-object v1, p0, Lcom/android/calendar/task/bb;->a:Lcom/android/calendar/task/az;

    invoke-virtual {v1}, Lcom/android/calendar/task/az;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/TaskDeleteActivity;->a(I)V

    .line 201
    return-void

    .line 195
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
