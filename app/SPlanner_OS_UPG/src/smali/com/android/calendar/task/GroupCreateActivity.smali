.class public Lcom/android/calendar/task/GroupCreateActivity;
.super Lcom/android/calendar/b;
.source "GroupCreateActivity.java"


# static fields
.field public static b:Z


# instance fields
.field private c:J

.field private d:Lcom/android/calendar/task/f;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/task/GroupCreateActivity;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    .line 35
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->c:J

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    .line 71
    const-string v1, "GroupId"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->c:J

    .line 73
    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 131
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 132
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->invalidateOptionsMenu()V

    .line 133
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v4, 0x7f12006b

    .line 45
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/GroupCreateActivity;->setContentView(I)V

    .line 49
    invoke-direct {p0}, Lcom/android/calendar/task/GroupCreateActivity;->b()V

    .line 51
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->e:Z

    .line 53
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/f;

    iput-object v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    .line 55
    iget-object v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/android/calendar/task/f;

    iget-wide v2, p0, Lcom/android/calendar/task/GroupCreateActivity;->c:J

    invoke-direct {v0, v2, v3}, Lcom/android/calendar/task/f;-><init>(J)V

    iput-object v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    .line 58
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    invoke-virtual {v0, v4, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 60
    iget-object v1, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 61
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 63
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f1200d4

    const/4 v4, 0x4

    const/4 v3, 0x1

    const v2, 0x7f1200d5

    .line 77
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 78
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 79
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0f0397

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 80
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 81
    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->e:Z

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02005c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 83
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020057

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 88
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 90
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 91
    return v3

    .line 85
    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 86
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 105
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 125
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    .line 107
    :sswitch_0
    iget-object v1, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    if-eqz v1, :cond_1

    .line 108
    sput-boolean v0, Lcom/android/calendar/task/GroupCreateActivity;->b:Z

    .line 109
    iget-object v1, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    invoke-virtual {v1}, Lcom/android/calendar/task/f;->b()V

    .line 111
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->finish()V

    .line 112
    iget-object v1, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    invoke-virtual {v1}, Lcom/android/calendar/task/f;->c()V

    goto :goto_0

    .line 118
    :sswitch_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/calendar/task/GroupCreateActivity;->b:Z

    .line 119
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupCreateActivity;->finish()V

    .line 120
    iget-object v1, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    invoke-virtual {v1}, Lcom/android/calendar/task/f;->c()V

    goto :goto_0

    .line 105
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f1200d4 -> :sswitch_1
        0x7f1200d5 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 96
    const v0, 0x7f1200d5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 98
    iget-object v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    invoke-virtual {v0}, Lcom/android/calendar/task/f;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/GroupCreateActivity;->d:Lcom/android/calendar/task/f;

    invoke-virtual {v0}, Lcom/android/calendar/task/f;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 100
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 98
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
