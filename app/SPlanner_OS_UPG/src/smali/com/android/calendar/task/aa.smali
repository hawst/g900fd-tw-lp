.class public Lcom/android/calendar/task/aa;
.super Ljava/lang/Object;
.source "Task.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field private static q:Ljava/lang/String;


# instance fields
.field public b:J

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field public f:I

.field public g:Z

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:I

.field public m:Ljava/lang/String;

.field public n:J

.field public o:J

.field public p:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "utc_due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "groupName"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "reminder_time"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "start_date"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "utc_start_date"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "_sync_account"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/task/aa;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/database/Cursor;)Lcom/android/calendar/task/aa;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 126
    new-instance v2, Lcom/android/calendar/task/aa;

    invoke-direct {v2}, Lcom/android/calendar/task/aa;-><init>()V

    .line 128
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/task/aa;->b:J

    .line 129
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    .line 130
    iget-object v3, v2, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 131
    :cond_0
    sget-object v3, Lcom/android/calendar/task/aa;->q:Ljava/lang/String;

    iput-object v3, v2, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    .line 133
    :cond_1
    const/4 v3, 0x2

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/task/aa;->d:J

    .line 134
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 135
    if-eqz v3, :cond_2

    .line 136
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/task/aa;->e:J

    .line 140
    :goto_0
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/task/aa;->e:J

    .line 141
    const/4 v3, 0x4

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/android/calendar/task/aa;->f:I

    .line 142
    const/4 v3, 0x5

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_3

    :goto_1
    iput-boolean v0, v2, Lcom/android/calendar/task/aa;->g:Z

    .line 143
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/android/calendar/task/aa;->h:I

    .line 144
    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/android/calendar/task/aa;->i:I

    .line 145
    const/16 v0, 0x8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/calendar/task/aa;->j:Ljava/lang/String;

    .line 146
    const/16 v0, 0xe

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/calendar/task/aa;->k:Ljava/lang/String;

    .line 147
    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/android/calendar/task/aa;->l:I

    .line 148
    const/16 v0, 0xa

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/calendar/task/aa;->m:Ljava/lang/String;

    .line 149
    const/16 v0, 0xb

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/android/calendar/task/aa;->n:J

    .line 150
    const/16 v0, 0xc

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/android/calendar/task/aa;->o:J

    .line 151
    const/16 v0, 0xd

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/android/calendar/task/aa;->p:J

    .line 152
    return-object v2

    .line 138
    :cond_2
    iget-wide v4, v2, Lcom/android/calendar/task/aa;->d:J

    iput-wide v4, v2, Lcom/android/calendar/task/aa;->e:J

    goto :goto_0

    :cond_3
    move v0, v1

    .line 142
    goto :goto_1
.end method

.method public static a(Lcom/android/calendar/hc;)Lcom/android/calendar/task/aa;
    .locals 4

    .prologue
    .line 176
    new-instance v0, Lcom/android/calendar/task/aa;

    invoke-direct {v0}, Lcom/android/calendar/task/aa;-><init>()V

    .line 178
    iget-wide v2, p0, Lcom/android/calendar/hc;->a:J

    iput-wide v2, v0, Lcom/android/calendar/task/aa;->b:J

    .line 179
    iget-object v1, p0, Lcom/android/calendar/hc;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    .line 180
    iget-object v1, v0, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 181
    :cond_0
    sget-object v1, Lcom/android/calendar/task/aa;->q:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    .line 183
    :cond_1
    iget-wide v2, p0, Lcom/android/calendar/hc;->b:J

    iput-wide v2, v0, Lcom/android/calendar/task/aa;->d:J

    .line 184
    iget-wide v2, p0, Lcom/android/calendar/hc;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 185
    if-eqz v1, :cond_2

    .line 186
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/task/aa;->e:J

    .line 190
    :goto_0
    iget-wide v2, p0, Lcom/android/calendar/hc;->c:J

    iput-wide v2, v0, Lcom/android/calendar/task/aa;->e:J

    .line 191
    iget v1, p0, Lcom/android/calendar/hc;->e:I

    iput v1, v0, Lcom/android/calendar/task/aa;->h:I

    .line 192
    iget v1, p0, Lcom/android/calendar/hc;->g:I

    iput v1, v0, Lcom/android/calendar/task/aa;->f:I

    .line 193
    iget-boolean v1, p0, Lcom/android/calendar/hc;->f:Z

    iput-boolean v1, v0, Lcom/android/calendar/task/aa;->g:Z

    .line 194
    iget v1, p0, Lcom/android/calendar/hc;->k:I

    iput v1, v0, Lcom/android/calendar/task/aa;->i:I

    .line 195
    iget-object v1, p0, Lcom/android/calendar/hc;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/task/aa;->j:Ljava/lang/String;

    .line 196
    iget-object v1, p0, Lcom/android/calendar/hc;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/task/aa;->k:Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcom/android/calendar/hc;->l:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/task/aa;->m:Ljava/lang/String;

    .line 198
    return-object v0

    .line 188
    :cond_2
    iget-wide v2, v0, Lcom/android/calendar/task/aa;->d:J

    iput-wide v2, v0, Lcom/android/calendar/task/aa;->e:J

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 202
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 203
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/hc;

    .line 204
    invoke-static {v0}, Lcom/android/calendar/task/aa;->a(Lcom/android/calendar/hc;)Lcom/android/calendar/task/aa;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 206
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/util/ArrayList;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-static {p0, p1, p2, v0, v0}, Lcom/android/calendar/task/aa;->a(Ljava/util/ArrayList;Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public static a(Ljava/util/ArrayList;Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 88
    if-eqz p2, :cond_0

    if-nez p0, :cond_2

    .line 89
    :cond_0
    const-string v0, "Task"

    const-string v1, "buildTasksFromCursor: null cursor or null tasks list!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_1
    return-void

    .line 92
    :cond_2
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 94
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 95
    if-eqz v0, :cond_1

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 100
    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/aa;->q:Ljava/lang/String;

    .line 104
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    .line 105
    :cond_3
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 110
    :goto_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 111
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 113
    :cond_4
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    invoke-static {p2}, Lcom/android/calendar/task/aa;->a(Landroid/database/Cursor;)Lcom/android/calendar/task/aa;

    move-result-object v1

    .line 115
    if-eqz p3, :cond_5

    .line 116
    iget-wide v2, v1, Lcom/android/calendar/task/aa;->e:J

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 117
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 121
    :cond_5
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 107
    :cond_6
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/android/calendar/task/aa;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/aa;->k:Ljava/lang/String;

    const-string v1, "task_personal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    const/4 v0, 0x0

    .line 214
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/calendar/task/aa;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n subject = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n dueDate = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/calendar/task/aa;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n utcDueDate = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/calendar/task/aa;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n importance = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/task/aa;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n complete = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/calendar/task/aa;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n reminderType = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/task/aa;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n accountKey = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/task/aa;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n accountName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/aa;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n syncAccount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/aa;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n groupId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/task/aa;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n groupName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/aa;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n reminderTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/calendar/task/aa;->n:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n startDate = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/calendar/task/aa;->o:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \n utcStartDate = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/calendar/task/aa;->p:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
