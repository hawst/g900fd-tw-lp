.class Lcom/android/calendar/task/bf;
.super Landroid/os/AsyncTask;
.source "TaskDeleteFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/task/az;

.field private b:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Lcom/android/calendar/task/az;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 812
    iput-object p1, p0, Lcom/android/calendar/task/bf;->a:Lcom/android/calendar/task/az;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 810
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    .line 813
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Lcom/android/calendar/task/az;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    .line 814
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    const v1, 0x7f0f01cf

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 815
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p2}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 816
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 817
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 818
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 819
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Long;)Ljava/lang/Integer;
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 823
    if-nez p1, :cond_0

    .line 824
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 847
    :goto_0
    return-object v0

    .line 826
    :cond_0
    array-length v0, p1

    if-le v0, v1, :cond_2

    move v0, v1

    .line 829
    :goto_1
    array-length v7, p1

    move v6, v2

    move v4, v2

    :goto_2
    if-ge v6, v7, :cond_4

    aget-object v3, p1, v6

    .line 830
    sget-object v8, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 831
    iget-object v8, p0, Lcom/android/calendar/task/bf;->a:Lcom/android/calendar/task/az;

    invoke-static {v8}, Lcom/android/calendar/task/az;->l(Lcom/android/calendar/task/az;)Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, v3, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 833
    if-gtz v3, :cond_3

    move v3, v4

    .line 829
    :cond_1
    :goto_3
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    goto :goto_2

    :cond_2
    move v0, v2

    .line 826
    goto :goto_1

    .line 838
    :cond_3
    if-eqz v0, :cond_5

    .line 839
    const/4 v3, 0x2

    new-array v8, v3, [Ljava/lang/Integer;

    add-int/lit8 v3, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v2

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v1

    invoke-virtual {p0, v8}, Lcom/android/calendar/task/bf;->publishProgress([Ljava/lang/Object;)V

    .line 842
    :goto_4
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 843
    const-string v4, "GATE"

    const-string v8, "<GATE-M>TASK_DELETED</GATE-M>"

    invoke-static {v4, v8}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_4
    move-object v0, v5

    .line 847
    goto :goto_0

    :cond_5
    move v3, v4

    goto :goto_4
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 863
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getMax()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 864
    iget-object v0, p0, Lcom/android/calendar/task/bf;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->f(Lcom/android/calendar/task/az;)Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/task/bg;

    invoke-direct {v1, p0}, Lcom/android/calendar/task/bg;-><init>(Lcom/android/calendar/task/bf;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 871
    iget-object v0, p0, Lcom/android/calendar/task/bf;->a:Lcom/android/calendar/task/az;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/task/az;->a(Lcom/android/calendar/task/az;Lcom/android/calendar/task/bf;)Lcom/android/calendar/task/bf;

    .line 872
    return-void
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 852
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 859
    :cond_0
    :goto_0
    return-void

    .line 855
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/task/bf;->a:Lcom/android/calendar/task/az;

    invoke-static {v0}, Lcom/android/calendar/task/az;->f(Lcom/android/calendar/task/az;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 856
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 858
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/bf;->b:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 809
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/android/calendar/task/bf;->a([Ljava/lang/Long;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 809
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/task/bf;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 809
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/task/bf;->a([Ljava/lang/Integer;)V

    return-void
.end method
