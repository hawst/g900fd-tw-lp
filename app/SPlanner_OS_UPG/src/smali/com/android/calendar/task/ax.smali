.class Lcom/android/calendar/task/ax;
.super Landroid/content/AsyncQueryHandler;
.source "TaskDeleteAdapter.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/task/au;


# direct methods
.method public constructor <init>(Lcom/android/calendar/task/au;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 575
    iput-object p1, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    .line 576
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 577
    return-void
.end method

.method private a(Lcom/android/calendar/task/ay;Landroid/database/Cursor;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 712
    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v0}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v3

    monitor-enter v3

    .line 713
    :try_start_0
    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 715
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    iget v2, p1, Lcom/android/calendar/task/ay;->f:I

    invoke-static {v0, v2}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;I)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 721
    if-nez v0, :cond_1

    .line 722
    new-instance v0, Lcom/android/calendar/task/bs;

    iget-object v2, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v2}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/calendar/task/bs;-><init>(Landroid/content/Context;)V

    move-object v2, v0

    .line 731
    :goto_0
    iget v0, p1, Lcom/android/calendar/task/ay;->c:I

    iput v0, v2, Lcom/android/calendar/task/bs;->d:I

    .line 732
    iget v0, p1, Lcom/android/calendar/task/ay;->d:I

    iput v0, v2, Lcom/android/calendar/task/bs;->e:I

    .line 733
    iput-object p2, v2, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    .line 734
    iget-object v0, v2, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, v2}, Lcom/android/calendar/task/ai;->a(Lcom/android/calendar/task/bs;)V

    .line 735
    iget-object v0, v2, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0}, Lcom/android/calendar/task/ai;->getCount()I

    move-result v0

    iput v0, v2, Lcom/android/calendar/task/bs;->g:I

    .line 738
    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v0}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v4, p1, Lcom/android/calendar/task/ay;->d:I

    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v0}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->d:I

    if-gt v4, v0, :cond_2

    .line 740
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v0}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 741
    iget v0, v2, Lcom/android/calendar/task/bs;->g:I

    add-int/2addr v0, v1

    move v1, v0

    .line 749
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/calendar/task/au;->c(Lcom/android/calendar/task/au;I)I

    .line 750
    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v0}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 751
    iget-object v4, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->j(Lcom/android/calendar/task/au;)I

    move-result v4

    iput v4, v0, Lcom/android/calendar/task/bs;->f:I

    .line 752
    iget-object v4, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    iget v0, v0, Lcom/android/calendar/task/bs;->g:I

    invoke-static {v4, v0}, Lcom/android/calendar/task/au;->d(Lcom/android/calendar/task/au;I)I

    goto :goto_2

    .line 756
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 757
    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 727
    :cond_1
    :try_start_3
    iget v1, v0, Lcom/android/calendar/task/bs;->g:I

    neg-int v1, v1

    move-object v2, v0

    goto :goto_0

    .line 745
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v0}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_1

    .line 754
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;Lcom/android/calendar/task/bs;)Lcom/android/calendar/task/bs;

    .line 755
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return v1
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 20

    .prologue
    .line 581
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v18

    monitor-enter v18

    .line 582
    :try_start_0
    check-cast p2, Lcom/android/calendar/task/ay;

    .line 583
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->c(Lcom/android/calendar/task/au;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p3, :cond_0

    .line 584
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 585
    monitor-exit v18

    .line 703
    :goto_0
    return-void

    .line 587
    :cond_0
    if-nez p3, :cond_1

    .line 588
    monitor-exit v18

    goto :goto_0

    .line 702
    :catchall_0
    move-exception v4

    monitor-exit v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 590
    :cond_1
    :try_start_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v19

    .line 591
    if-gtz v19, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/task/ay;->f:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_c

    .line 592
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/task/ax;->a(Lcom/android/calendar/task/ay;Landroid/database/Cursor;)I

    move-result v4

    .line 593
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/calendar/task/ay;->b:Landroid/text/format/Time;

    if-nez v5, :cond_3

    .line 594
    if-eqz v4, :cond_3

    .line 595
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v5}, Lcom/android/calendar/task/au;->d(Lcom/android/calendar/task/au;)Lcom/android/calendar/task/TaskDeleteView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/android/calendar/task/TaskDeleteView;->b(I)V

    .line 598
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v4}, Lcom/android/calendar/task/au;->notifyDataSetChanged()V

    .line 601
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->e(Lcom/android/calendar/task/au;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_6

    .line 602
    const/4 v4, 0x0

    .line 603
    const/4 v5, -0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 604
    :cond_4
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 605
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v5}, Lcom/android/calendar/task/au;->e(Lcom/android/calendar/task/au;)J

    move-result-wide v6

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_4

    .line 607
    const/4 v4, 0x1

    .line 612
    :cond_5
    if-nez v4, :cond_6

    .line 613
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    const-wide/16 v6, -0x1

    invoke-static {v4, v6, v7}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;J)J

    .line 616
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->e(Lcom/android/calendar/task/au;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 617
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;J)J

    .line 622
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    new-instance v5, Lcom/android/calendar/task/ah;

    invoke-direct {v5}, Lcom/android/calendar/task/ah;-><init>()V

    invoke-static {v4, v5}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;Lcom/android/calendar/task/ah;)Lcom/android/calendar/task/ah;

    .line 624
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/calendar/task/au;->d(I)J

    move-result-wide v8

    .line 625
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->f(Lcom/android/calendar/task/au;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 626
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v4

    const-wide/16 v6, 0x2

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    .line 636
    :cond_7
    :goto_1
    if-eqz v19, :cond_d

    .line 637
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;I)I

    .line 676
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->g(Lcom/android/calendar/task/au;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 677
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 678
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/task/ay;

    .line 679
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    iget v7, v4, Lcom/android/calendar/task/ay;->c:I

    iget v8, v4, Lcom/android/calendar/task/ay;->d:I

    invoke-static {v6, v7, v8}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;II)Z

    move-result v6

    if-nez v6, :cond_10

    .line 681
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v5, v4}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;Lcom/android/calendar/task/ay;)V

    .line 688
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    const v6, 0x7f1200dc

    invoke-virtual {v4, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;Landroid/view/View;)Landroid/view/View;

    .line 689
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->i(Lcom/android/calendar/task/au;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 690
    if-gtz v19, :cond_11

    .line 691
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/task/TaskDeleteActivity;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/android/calendar/task/TaskDeleteActivity;->a(Z)V

    .line 692
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->i(Lcom/android/calendar/task/au;)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 698
    :cond_a
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    iget-object v4, v4, Lcom/android/calendar/task/au;->b:Lcom/android/calendar/task/aw;

    if-eqz v4, :cond_b

    .line 699
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    iget-object v4, v4, Lcom/android/calendar/task/au;->b:Lcom/android/calendar/task/aw;

    invoke-interface {v4}, Lcom/android/calendar/task/aw;->a()V

    .line 700
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/android/calendar/task/au;->b:Lcom/android/calendar/task/aw;

    .line 702
    :cond_b
    monitor-exit v18

    goto/16 :goto_0

    .line 631
    :cond_c
    if-eqz p3, :cond_7

    .line 632
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 639
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->g(Lcom/android/calendar/task/au;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/task/ay;

    .line 640
    if-eqz v4, :cond_8

    .line 642
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v5}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_f

    .line 643
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v5}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/calendar/task/bs;

    .line 644
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v6}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/calendar/task/bs;

    .line 646
    iget v7, v5, Lcom/android/calendar/task/bs;->d:I

    add-int/lit8 v7, v7, -0x1

    iget v8, v4, Lcom/android/calendar/task/ay;->d:I

    if-gt v7, v8, :cond_e

    iget v7, v4, Lcom/android/calendar/task/ay;->c:I

    iget v8, v5, Lcom/android/calendar/task/bs;->d:I

    if-ge v7, v8, :cond_e

    .line 647
    iget v7, v4, Lcom/android/calendar/task/ay;->c:I

    iput v7, v5, Lcom/android/calendar/task/bs;->d:I

    .line 650
    :cond_e
    iget v5, v4, Lcom/android/calendar/task/ay;->c:I

    iget v7, v6, Lcom/android/calendar/task/bs;->e:I

    add-int/lit8 v7, v7, 0x1

    if-gt v5, v7, :cond_f

    iget v5, v6, Lcom/android/calendar/task/bs;->e:I

    iget v7, v4, Lcom/android/calendar/task/ay;->d:I

    if-ge v5, v7, :cond_f

    .line 651
    iget v5, v4, Lcom/android/calendar/task/ay;->d:I

    iput v5, v6, Lcom/android/calendar/task/bs;->e:I

    .line 657
    :cond_f
    iget v5, v4, Lcom/android/calendar/task/ay;->f:I

    packed-switch v5, :pswitch_data_0

    .line 669
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->h(Lcom/android/calendar/task/au;)I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_8

    .line 671
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->g(Lcom/android/calendar/task/au;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    goto/16 :goto_2

    .line 659
    :pswitch_0
    iget v5, v4, Lcom/android/calendar/task/ay;->c:I

    add-int/lit8 v5, v5, -0x3c

    iput v5, v4, Lcom/android/calendar/task/ay;->c:I

    goto :goto_5

    .line 662
    :pswitch_1
    iget v5, v4, Lcom/android/calendar/task/ay;->d:I

    add-int/lit8 v5, v5, 0x3c

    iput v5, v4, Lcom/android/calendar/task/ay;->d:I

    goto :goto_5

    .line 665
    :pswitch_2
    iget v5, v4, Lcom/android/calendar/task/ay;->c:I

    add-int/lit8 v5, v5, -0x1e

    iput v5, v4, Lcom/android/calendar/task/ay;->c:I

    .line 666
    iget v5, v4, Lcom/android/calendar/task/ay;->d:I

    add-int/lit8 v5, v5, 0x1e

    iput v5, v4, Lcom/android/calendar/task/ay;->d:I

    goto :goto_5

    .line 685
    :cond_10
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_3

    .line 694
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/au;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/task/TaskDeleteActivity;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/android/calendar/task/TaskDeleteActivity;->a(Z)V

    .line 695
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ax;->a:Lcom/android/calendar/task/au;

    invoke-static {v4}, Lcom/android/calendar/task/au;->i(Lcom/android/calendar/task/au;)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 657
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
