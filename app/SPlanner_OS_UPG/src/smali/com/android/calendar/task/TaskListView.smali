.class public Lcom/android/calendar/task/TaskListView;
.super Lcom/sec/android/touchwiz/widget/TwExpandableListView;
.source "TaskListView.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/android/calendar/task/bq;

.field private c:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Landroid/text/format/Time;

.field private f:Z

.field private g:I

.field private h:Lcom/android/calendar/task/bp;

.field private i:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/TaskListView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/task/TaskListView;->g:I

    .line 74
    new-instance v0, Lcom/android/calendar/task/bp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/task/bp;-><init>(Lcom/android/calendar/task/TaskListView;Lcom/android/calendar/task/bn;)V

    iput-object v0, p0, Lcom/android/calendar/task/TaskListView;->h:Lcom/android/calendar/task/bp;

    .line 76
    new-instance v0, Lcom/android/calendar/task/bn;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/bn;-><init>(Lcom/android/calendar/task/TaskListView;)V

    iput-object v0, p0, Lcom/android/calendar/task/TaskListView;->i:Ljava/lang/Runnable;

    .line 86
    invoke-direct {p0, p1}, Lcom/android/calendar/task/TaskListView;->a(Landroid/content/Context;)V

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/TaskListView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/task/TaskListView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/android/calendar/task/TaskListView;->d:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0a0007

    .line 90
    iput-object p1, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    .line 91
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->i:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/TaskListView;->d:Ljava/lang/String;

    .line 92
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/task/TaskListView;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/task/TaskListView;->e:Landroid/text/format/Time;

    .line 93
    invoke-virtual {p0, p0}, Lcom/android/calendar/task/TaskListView;->setOnItemClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;)V

    .line 94
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->setChoiceMode(I)V

    .line 96
    new-instance v0, Lcom/android/calendar/task/bq;

    invoke-static {p1, v4}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v1

    invoke-direct {v0, p1, p0, v1}, Lcom/android/calendar/task/bq;-><init>(Landroid/content/Context;Lcom/android/calendar/task/TaskListView;Z)V

    iput-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    .line 98
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/task/bq;->a(J)V

    .line 99
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->setCacheColorHint(I)V

    .line 101
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/TaskListView;->f:Z

    .line 104
    invoke-virtual {p0, v5}, Lcom/android/calendar/task/TaskListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 105
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->setDividerHeight(I)V

    .line 107
    invoke-virtual {p0, p0}, Lcom/android/calendar/task/TaskListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 109
    invoke-virtual {p0, v5}, Lcom/android/calendar/task/TaskListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 110
    new-instance v0, Lcom/android/calendar/task/bo;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/bo;-><init>(Lcom/android/calendar/task/TaskListView;)V

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->setOnChildClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;)V

    .line 129
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/task/TaskListView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/task/TaskListView;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->e:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/task/TaskListView;)Lcom/android/calendar/task/bq;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/task/TaskListView;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/android/calendar/task/TaskListView;->f:Z

    return v0
.end method

.method private i(I)V
    .locals 3

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskListView;->getFirstVisibleView()Landroid/view/View;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_2

    .line 272
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 273
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 276
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 277
    add-int v2, v0, p1

    iget v0, v1, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_1

    iget v0, v1, Landroid/graphics/Rect;->top:I

    neg-int v0, v0

    :goto_0
    invoke-virtual {p0, v2, v0}, Lcom/android/calendar/task/TaskListView;->setSelectionFromTop(II)V

    .line 299
    :cond_0
    :goto_1
    return-void

    .line 277
    :cond_1
    iget v0, v1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 292
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskListView;->getSelectedItemPosition()I

    move-result v0

    if-ltz v0, :cond_0

    .line 297
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskListView;->getSelectedItemPosition()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->setSelection(I)V

    goto :goto_1
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 261
    invoke-direct {p0, p1}, Lcom/android/calendar/task/TaskListView;->i(I)V

    .line 262
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskListView;->getSelectedItemPosition()I

    move-result v0

    .line 263
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 264
    add-int/2addr v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/task/TaskListView;->setSelectionFromTop(II)V

    .line 266
    :cond_0
    return-void
.end method

.method public declared-synchronized a(II)V
    .locals 2

    .prologue
    .line 140
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->setExpandableListAnimationEnabled(Z)V

    move v0, p1

    .line 141
    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 142
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->expandGroup(I)Z

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->setExpandableListAnimationEnabled(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    monitor-exit p0

    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 132
    if-eqz p1, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskListView;->expandAll()V

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v1}, Lcom/android/calendar/task/bq;->getGroupCount()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/task/TaskListView;->a(II)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 463
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/bq;->d(I)J

    move-result-wide v6

    .line 464
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Lcom/android/calendar/task/bq;->c()J

    move-result-wide v0

    .line 470
    const-wide/16 v2, -0x1

    cmp-long v2, v6, v2

    if-eqz v2, :cond_1

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/task/TaskListView;->f:Z

    if-nez v0, :cond_1

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 472
    const-wide/16 v2, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v9}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    .line 475
    :cond_1
    return-void
.end method

.method public c(I)V
    .locals 17

    .prologue
    .line 480
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/bq;->d(I)J

    move-result-wide v9

    .line 481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v2}, Lcom/android/calendar/task/bq;->c()J

    move-result-wide v2

    .line 487
    const-wide/16 v4, -0x1

    cmp-long v4, v9, v4

    if-eqz v4, :cond_1

    cmp-long v2, v2, v9

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/task/TaskListView;->f:Z

    if-nez v2, :cond_1

    .line 488
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 489
    const-wide/16 v4, 0x8

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    .line 492
    :cond_1
    return-void
.end method

.method public d(I)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 497
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/bq;->d(I)J

    move-result-wide v6

    .line 498
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Lcom/android/calendar/task/bq;->c()J

    move-result-wide v0

    .line 504
    const-wide/16 v2, -0x1

    cmp-long v2, v6, v2

    if-eqz v2, :cond_1

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/task/TaskListView;->f:Z

    if-nez v0, :cond_1

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 506
    const-wide/32 v2, 0x200000

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v9}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    .line 509
    :cond_1
    return-void
.end method

.method public e(I)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 512
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/bq;->d(I)J

    move-result-wide v6

    .line 514
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 516
    const-wide/16 v2, 0x10

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v9}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    .line 519
    :cond_0
    return-void
.end method

.method public f(I)V
    .locals 4

    .prologue
    .line 522
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/bq;->d(I)J

    move-result-wide v0

    .line 524
    sget-object v2, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 525
    iget-object v1, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object v0

    .line 526
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 527
    const-string v2, "text/x-vtodo"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 528
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 529
    const-string v0, "theme"

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 531
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.CHOOSER"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 532
    const-string v2, "android.intent.extra.INTENT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 533
    const-string v1, "android.intent.extra.TITLE"

    iget-object v2, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    const v3, 0x7f0f03d8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 534
    iget-object v1, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 535
    return-void
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/bq;->e(I)V

    .line 539
    return-void
.end method

.method public getCompletedTaskCount()I
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Lcom/android/calendar/task/bq;->e()I

    move-result v0

    return v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskListView;->getFirstVisibleView()Landroid/view/View;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 222
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFirstVisibleView()Landroid/view/View;
    .locals 5

    .prologue
    .line 226
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 227
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskListView;->getChildCount()I

    move-result v3

    .line 228
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 229
    invoke-virtual {p0, v1}, Lcom/android/calendar/task/TaskListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 230
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 231
    iget v4, v2, Landroid/graphics/Rect;->top:I

    if-ltz v4, :cond_0

    .line 235
    :goto_1
    return-object v0

    .line 228
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 235
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSelectedTaskId()J
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Lcom/android/calendar/task/bq;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedViewHolder()Lcom/android/calendar/task/ah;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Lcom/android/calendar/task/bq;->b()Lcom/android/calendar/task/ah;

    move-result-object v0

    return-object v0
.end method

.method public getTaskCount()I
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Lcom/android/calendar/task/bq;->d()I

    move-result v0

    return v0
.end method

.method public h(I)V
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/bq;->f(I)V

    .line 543
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 321
    if-eqz p3, :cond_1

    .line 322
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 323
    invoke-virtual {p2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 325
    check-cast p3, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandableListContextMenuInfo;

    .line 326
    if-eqz p3, :cond_1

    .line 327
    iget-wide v0, p3, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v0, v1}, Lcom/android/calendar/task/TaskListView;->getPackedPositionGroup(J)I

    move-result v3

    .line 328
    iget-wide v0, p3, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v0, v1}, Lcom/android/calendar/task/TaskListView;->getPackedPositionChild(J)I

    move-result v4

    .line 330
    const/4 v0, -0x1

    if-eq v4, v0, :cond_1

    .line 332
    if-lez v3, :cond_2

    move v0, v2

    move v1, v2

    .line 333
    :goto_0
    if-ge v0, v3, :cond_0

    .line 334
    add-int/lit8 v1, v1, 0x1

    .line 335
    iget-object v5, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v5, v0}, Lcom/android/calendar/task/bq;->getChildrenCount(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 333
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 337
    :cond_0
    add-int/lit8 v0, v4, 0x1

    add-int/2addr v0, v1

    .line 342
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/bq;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v3

    .line 343
    if-nez v3, :cond_3

    .line 400
    :cond_1
    :goto_2
    return-void

    .line 339
    :cond_2
    add-int/lit8 v0, v4, 0x1

    goto :goto_1

    .line 347
    :cond_3
    iget-object v1, v3, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/ai;->g(I)I

    move-result v1

    .line 348
    iget-object v4, v3, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    if-eqz v4, :cond_6

    iget-object v4, v3, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v4, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 350
    iget-object v1, v3, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    const-string v4, "subject"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 351
    iget-object v4, v3, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 352
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 353
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0f02bf

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 355
    :cond_5
    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 366
    :cond_6
    const/4 v1, 0x3

    const v4, 0x7f0f0143

    invoke-interface {p1, v2, v1, v2, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 367
    iget-object v4, p0, Lcom/android/calendar/task/TaskListView;->h:Lcom/android/calendar/task/bp;

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 369
    const/4 v1, 0x1

    const v4, 0x7f0f0187

    invoke-interface {p1, v2, v1, v2, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 370
    iget-object v4, p0, Lcom/android/calendar/task/TaskListView;->h:Lcom/android/calendar/task/bp;

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 372
    const v1, 0x7f0f01d6

    invoke-interface {p1, v2, v6, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 373
    iget-object v4, p0, Lcom/android/calendar/task/TaskListView;->h:Lcom/android/calendar/task/bp;

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 375
    const/4 v1, 0x4

    const v4, 0x7f0f03d8

    invoke-interface {p1, v2, v1, v2, v4}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 376
    iget-object v4, p0, Lcom/android/calendar/task/TaskListView;->h:Lcom/android/calendar/task/bp;

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 378
    iget v1, p0, Lcom/android/calendar/task/TaskListView;->g:I

    if-ne v1, v7, :cond_1

    .line 379
    iget-object v1, v3, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/ai;->d(I)I

    move-result v1

    .line 382
    iget-object v4, v3, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v4, v0}, Lcom/android/calendar/task/ai;->a(I)I

    move-result v4

    add-int/lit8 v5, v0, -0x1

    if-eq v4, v5, :cond_1

    .line 383
    if-ltz v1, :cond_7

    iget-object v3, v3, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Lcom/android/calendar/task/ai;->d(I)I

    move-result v0

    if-gt v1, v0, :cond_7

    .line 387
    const v0, 0x7f0f0246

    invoke-interface {p1, v2, v7, v2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 388
    iget-object v3, p0, Lcom/android/calendar/task/TaskListView;->h:Lcom/android/calendar/task/bp;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 390
    :cond_7
    if-lez v1, :cond_1

    .line 392
    const/4 v0, 0x6

    const v1, 0x7f0f0458

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 393
    iget-object v1, p0, Lcom/android/calendar/task/TaskListView;->h:Lcom/android/calendar/task/bp;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_2
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 165
    invoke-super {p0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->onDetachedFromWindow()V

    .line 166
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Lcom/android/calendar/task/bq;->a()V

    .line 167
    return-void
.end method

.method public onItemClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v4, 0x0

    .line 172
    cmp-long v0, p4, v8

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Lcom/android/calendar/task/bq;->c()J

    move-result-wide v0

    .line 174
    iget-object v2, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v2, p2}, Lcom/android/calendar/task/bq;->a(Landroid/view/View;)V

    .line 175
    iget-object v2, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v2}, Lcom/android/calendar/task/bq;->c()J

    move-result-wide v6

    .line 177
    cmp-long v2, v6, v8

    if-eqz v2, :cond_1

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/task/TaskListView;->f:Z

    if-nez v0, :cond_1

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 179
    const-wide/16 v2, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v9}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    .line 183
    :cond_1
    return-void
.end method

.method public setListBy(I)V
    .locals 1

    .prologue
    .line 315
    iput p1, p0, Lcom/android/calendar/task/TaskListView;->g:I

    .line 316
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/bq;->g(I)V

    .line 317
    return-void
.end method

.method public setSelectedTaskId(J)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/android/calendar/task/TaskListView;->b:Lcom/android/calendar/task/bq;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/task/bq;->a(J)V

    .line 256
    return-void
.end method
