.class Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;
.super Ljava/lang/Object;
.source "GroupDeleteFragment.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:I

.field b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 383
    new-instance v0, Lcom/android/calendar/task/r;

    invoke-direct {v0}, Lcom/android/calendar/task/r;-><init>()V

    sput-object v0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/util/HashMap;)V
    .locals 0

    .prologue
    .line 393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394
    iput p1, p0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->a:I

    .line 395
    iput-object p2, p0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->b:Ljava/util/HashMap;

    .line 396
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->a:I

    .line 401
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->b:Ljava/util/HashMap;

    .line 402
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/calendar/task/k;)V
    .locals 0

    .prologue
    .line 367
    invoke-direct {p0, p1}, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 379
    iget v0, p0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 380
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->b:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 381
    return-void
.end method
