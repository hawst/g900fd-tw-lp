.class public Lcom/android/calendar/task/ab;
.super Ljava/lang/Object;
.source "TaskAccount.java"


# static fields
.field public static final h:Landroid/net/Uri;

.field private static final i:Ljava/lang/String;

.field private static final j:[I


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/android/calendar/task/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/ab;->i:Ljava/lang/String;

    .line 22
    const/16 v0, 0x13

    new-array v0, v0, [I

    sput-object v0, Lcom/android/calendar/task/ab;->j:[I

    .line 32
    const-string v0, "content://com.android.calendar/TasksAccounts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/ab;->h:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v1, p0, Lcom/android/calendar/task/ab;->a:I

    .line 24
    iput-object v0, p0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/android/calendar/task/ab;->c:I

    .line 26
    iput-object v0, p0, Lcom/android/calendar/task/ab;->d:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/android/calendar/task/ab;->e:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/android/calendar/task/ab;->f:I

    .line 29
    iput-object v0, p0, Lcom/android/calendar/task/ab;->g:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/task/ab;->a(Landroid/content/res/Resources;)V

    .line 118
    if-gtz p1, :cond_0

    .line 119
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 122
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    rem-int/lit8 v1, p1, 0x13

    aget v0, v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 67
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    new-instance v1, Lcom/android/calendar/task/ab;

    invoke-direct {v1}, Lcom/android/calendar/task/ab;-><init>()V

    .line 69
    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 70
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/android/calendar/task/ab;->a:I

    .line 71
    const-string v2, "_sync_account"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 72
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    .line 73
    const-string v2, "_sync_account_key"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 74
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/android/calendar/task/ab;->c:I

    .line 75
    const-string v2, "_sync_account_type"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 76
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/task/ab;->d:Ljava/lang/String;

    .line 77
    const-string v2, "url"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 78
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/task/ab;->e:Ljava/lang/String;

    .line 79
    const-string v2, "selected"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 80
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/android/calendar/task/ab;->f:I

    .line 81
    const-string v2, "displayName"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 82
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/task/ab;->g:Ljava/lang/String;

    .line 83
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)V
    .locals 4

    .prologue
    .line 91
    sget-object v1, Lcom/android/calendar/task/ab;->j:[I

    monitor-enter v1

    .line 92
    :try_start_0
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v2, 0x0

    const v3, 0x7f0b00ea

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 93
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v2, 0x1

    const v3, 0x7f0b00eb

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 94
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v2, 0x2

    const v3, 0x7f0b00ec

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 95
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v2, 0x3

    const v3, 0x7f0b00ed

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 96
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v2, 0x4

    const v3, 0x7f0b00ee

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 97
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v2, 0x5

    const v3, 0x7f0b00ef

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 98
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v2, 0x6

    const v3, 0x7f0b00f0

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 99
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/4 v2, 0x7

    const v3, 0x7f0b00f1

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 100
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0x8

    const v3, 0x7f0b00f2

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 101
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0x9

    const v3, 0x7f0b00f3

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 102
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0xa

    const v3, 0x7f0b00f4

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 103
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0xb

    const v3, 0x7f0b00f5

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 104
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0xc

    const v3, 0x7f0b00f6

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 105
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0xd

    const v3, 0x7f0b00f7

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 106
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0xe

    const v3, 0x7f0b00f8

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 107
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0xf

    const v3, 0x7f0b00f9

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 108
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0x10

    const v3, 0x7f0b00fa

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 109
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0x11

    const v3, 0x7f0b00fb

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 110
    sget-object v0, Lcom/android/calendar/task/ab;->j:[I

    const/16 v2, 0x12

    const v3, 0x7f0b00fc

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v2

    .line 111
    monitor-exit v1

    .line 113
    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
