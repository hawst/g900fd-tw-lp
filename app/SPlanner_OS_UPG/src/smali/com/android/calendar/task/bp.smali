.class Lcom/android/calendar/task/bp;
.super Ljava/lang/Object;
.source "TaskListView.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/task/TaskListView;


# direct methods
.method private constructor <init>(Lcom/android/calendar/task/TaskListView;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/task/TaskListView;Lcom/android/calendar/task/bn;)V
    .locals 0

    .prologue
    .line 402
    invoke-direct {p0, p1}, Lcom/android/calendar/task/bp;-><init>(Lcom/android/calendar/task/TaskListView;)V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 407
    :try_start_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandableListContextMenuInfo;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    iget-wide v4, v0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v4, v5}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->getPackedPositionChild(J)I

    move-result v3

    .line 412
    iget-wide v4, v0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v4, v5}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->getPackedPositionGroup(J)I

    move-result v4

    .line 414
    if-lez v4, :cond_1

    move v0, v2

    .line 415
    :goto_0
    if-ge v2, v4, :cond_0

    .line 416
    add-int/lit8 v0, v0, 0x1

    .line 417
    iget-object v5, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-static {v5}, Lcom/android/calendar/task/TaskListView;->d(Lcom/android/calendar/task/TaskListView;)Lcom/android/calendar/task/bq;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/android/calendar/task/bq;->getChildrenCount(I)I

    move-result v5

    add-int/2addr v0, v5

    .line 415
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 408
    :catch_0
    move-exception v0

    move v0, v2

    .line 456
    :goto_1
    return v0

    .line 419
    :cond_0
    add-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    .line 424
    :goto_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 456
    goto :goto_1

    .line 421
    :cond_1
    add-int/lit8 v0, v3, 0x1

    goto :goto_2

    .line 426
    :pswitch_0
    iget-object v2, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/TaskListView;->b(I)V

    move v0, v1

    .line 427
    goto :goto_1

    .line 430
    :pswitch_1
    iget-object v2, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/TaskListView;->c(I)V

    move v0, v1

    .line 431
    goto :goto_1

    .line 434
    :pswitch_2
    iget-object v2, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/TaskListView;->d(I)V

    move v0, v1

    .line 435
    goto :goto_1

    .line 438
    :pswitch_3
    iget-object v2, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/TaskListView;->e(I)V

    move v0, v1

    .line 439
    goto :goto_1

    .line 442
    :pswitch_4
    iget-object v2, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/TaskListView;->f(I)V

    move v0, v1

    .line 443
    goto :goto_1

    .line 446
    :pswitch_5
    iget-object v2, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/TaskListView;->g(I)V

    move v0, v1

    .line 447
    goto :goto_1

    .line 450
    :pswitch_6
    iget-object v2, p0, Lcom/android/calendar/task/bp;->a:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/TaskListView;->h(I)V

    move v0, v1

    .line 451
    goto :goto_1

    .line 424
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
