.class Lcom/android/calendar/task/at;
.super Lcom/android/calendar/task/ai;
.source "TaskChangeOrderFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;


# instance fields
.field private final k:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/android/calendar/task/ai;-><init>(Landroid/content/Context;)V

    .line 232
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    .line 236
    return-void
.end method

.method private a(Lcom/android/calendar/task/an;Z)V
    .locals 6

    .prologue
    .line 332
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 333
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 334
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 335
    iget-wide v2, v0, Lcom/android/calendar/task/an;->f:J

    iget-wide v4, p1, Lcom/android/calendar/task/an;->g:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 336
    if-eqz p2, :cond_1

    .line 337
    iget-wide v2, p1, Lcom/android/calendar/task/an;->f:J

    iput-wide v2, v0, Lcom/android/calendar/task/an;->f:J

    .line 338
    :cond_1
    iget v2, v0, Lcom/android/calendar/task/an;->d:I

    if-lez v2, :cond_0

    .line 339
    iget v2, v0, Lcom/android/calendar/task/an;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/android/calendar/task/an;->d:I

    .line 340
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/android/calendar/task/at;->a(Lcom/android/calendar/task/an;Z)V

    goto :goto_0

    .line 344
    :cond_2
    return-void
.end method

.method private p(I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 291
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 297
    iget-object v1, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v4

    move v1, v4

    .line 298
    :goto_1
    if-ge v5, v6, :cond_3

    .line 299
    invoke-virtual {p0, v5}, Lcom/android/calendar/task/at;->o(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 301
    const/4 v1, -0x1

    move v2, v1

    move v3, v4

    .line 305
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/an;

    iput v2, v1, Lcom/android/calendar/task/an;->e:I

    .line 298
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v3

    goto :goto_1

    .line 303
    :cond_2
    add-int/lit8 v2, v1, 0x1

    move v3, v2

    move v2, v1

    goto :goto_2

    .line 309
    :cond_3
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/task/at;->a(Lcom/android/calendar/task/an;Z)V

    .line 312
    const/4 v1, 0x0

    .line 314
    add-int/lit8 v2, p1, 0x1

    .line 315
    add-int/lit8 v3, p1, -0x1

    .line 316
    iget-object v5, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_5

    invoke-virtual {p0, v2}, Lcom/android/calendar/task/at;->o(I)Z

    move-result v5

    if-nez v5, :cond_5

    .line 317
    iget-object v1, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/an;

    .line 322
    :cond_4
    :goto_3
    if-nez v1, :cond_6

    .line 323
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/android/calendar/task/an;->f:J

    .line 324
    iput v4, v0, Lcom/android/calendar/task/an;->d:I

    goto :goto_0

    .line 318
    :cond_5
    if-ltz v3, :cond_4

    invoke-virtual {p0, v3}, Lcom/android/calendar/task/at;->o(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 319
    iget-object v1, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/an;

    goto :goto_3

    .line 326
    :cond_6
    iget-wide v2, v1, Lcom/android/calendar/task/an;->f:J

    iput-wide v2, v0, Lcom/android/calendar/task/an;->f:J

    .line 327
    iget v1, v1, Lcom/android/calendar/task/an;->d:I

    iput v1, v0, Lcom/android/calendar/task/an;->d:I

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/android/calendar/task/bs;)V
    .locals 3

    .prologue
    .line 255
    invoke-super {p0, p1}, Lcom/android/calendar/task/ai;->a(Lcom/android/calendar/task/bs;)V

    .line 257
    iget-object v0, p0, Lcom/android/calendar/task/at;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 259
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 262
    :cond_0
    return-void
.end method

.method public allowDrag(I)Z
    .locals 1

    .prologue
    .line 266
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/at;->e(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public allowDrop(II)Z
    .locals 3

    .prologue
    .line 271
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/at;->a(I)I

    move-result v0

    .line 272
    add-int/lit8 v1, v0, 0x1

    .line 273
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/at;->b(I)I

    move-result v2

    add-int/2addr v0, v2

    .line 274
    if-lt p2, v1, :cond_0

    if-gt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dropDone(II)V
    .locals 2

    .prologue
    .line 279
    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/task/at;->allowDrop(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    :goto_0
    return-void

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 283
    iget-object v1, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 284
    invoke-direct {p0, p2}, Lcom/android/calendar/task/at;->p(I)V

    .line 286
    invoke-virtual {p0}, Lcom/android/calendar/task/at;->notifyDataSetChanged()V

    .line 287
    iget-object v0, p0, Lcom/android/calendar/task/at;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 240
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/at;->j(I)I

    move-result v0

    .line 241
    invoke-super {p0, v0, p2, p3}, Lcom/android/calendar/task/ai;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 242
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/at;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    const/high16 v0, 0x9000000

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 250
    :goto_0
    return-object v1

    .line 245
    :cond_0
    const v0, 0x7f1202e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 246
    const v0, 0x7f1202ea

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 247
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/at;->m(I)I

    move-result v0

    .line 248
    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/task/at;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public j(I)I
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 353
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    :cond_0
    move v2, v3

    .line 367
    :cond_1
    :goto_0
    return v2

    .line 356
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 357
    iget-object v1, p0, Lcom/android/calendar/task/at;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 358
    const/4 v1, 0x0

    move v2, v1

    .line 360
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 361
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/an;

    .line 362
    if-eq v1, v0, :cond_1

    .line 364
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 365
    goto :goto_1

    :cond_3
    move v2, v3

    .line 367
    goto :goto_0
.end method

.method public k(I)J
    .locals 2

    .prologue
    .line 402
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 403
    :cond_0
    const-wide/16 v0, -0x1

    .line 404
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    iget-wide v0, v0, Lcom/android/calendar/task/an;->g:J

    goto :goto_0
.end method

.method public l(I)J
    .locals 2

    .prologue
    .line 414
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 415
    :cond_0
    const-wide/16 v0, -0x1

    .line 416
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    iget-wide v0, v0, Lcom/android/calendar/task/an;->f:J

    goto :goto_0
.end method

.method public m(I)I
    .locals 1

    .prologue
    .line 426
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 427
    :cond_0
    const/4 v0, -0x1

    .line 428
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    iget v0, v0, Lcom/android/calendar/task/an;->d:I

    goto :goto_0
.end method

.method public n(I)I
    .locals 1

    .prologue
    .line 438
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 439
    :cond_0
    const/4 v0, -0x1

    .line 440
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    iget v0, v0, Lcom/android/calendar/task/an;->e:I

    goto :goto_0
.end method

.method public o(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 450
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    move v0, v2

    .line 452
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/at;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    if-ne v0, v1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method
