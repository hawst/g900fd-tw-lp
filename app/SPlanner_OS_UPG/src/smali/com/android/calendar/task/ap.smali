.class public Lcom/android/calendar/task/ap;
.super Landroid/app/Fragment;
.source "TaskChangeOrderFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Landroid/view/View;

.field private c:Lcom/sec/android/touchwiz/widget/TwListView;

.field private d:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

.field private e:Lcom/android/calendar/task/at;

.field private f:Landroid/app/Activity;

.field private g:Landroid/content/AsyncQueryHandler;

.field private h:I

.field private i:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "utc_due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "previousId"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "parentId"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "task_order"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "groupName"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/task/ap;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 208
    new-instance v0, Lcom/android/calendar/task/ar;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/task/ar;-><init>(Lcom/android/calendar/task/ap;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/task/ap;->i:Landroid/database/ContentObserver;

    .line 230
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/ap;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/calendar/task/ap;->f:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/task/ap;)Lcom/android/calendar/task/at;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    return-object v0
.end method

.method private d()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 174
    iget-object v0, p0, Lcom/android/calendar/task/ap;->g:Landroid/content/AsyncQueryHandler;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/android/calendar/task/ap;->g:Landroid/content/AsyncQueryHandler;

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 176
    iget-object v0, p0, Lcom/android/calendar/task/ap;->g:Landroid/content/AsyncQueryHandler;

    sget-object v3, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/task/ap;->a:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/calendar/task/ap;->f:Landroid/app/Activity;

    invoke-static {v5}, Lcom/android/calendar/hj;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "group_order ASC, task_order ASC"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/android/calendar/task/ap;->d()V

    .line 159
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 168
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/android/calendar/task/ap;->a()V

    .line 171
    :cond_0
    return-void
.end method

.method public b()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 182
    iget-object v0, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v0}, Lcom/android/calendar/task/at;->getCount()I

    move-result v8

    .line 183
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v8, :cond_1

    .line 184
    iget-object v0, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v0, v7}, Lcom/android/calendar/task/at;->k(I)J

    move-result-wide v0

    .line 185
    iget-object v3, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v3, v7}, Lcom/android/calendar/task/at;->o(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 186
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 187
    const-string v3, "task_order"

    iget-object v5, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v5, v7}, Lcom/android/calendar/task/at;->n(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 188
    const-string v3, "parentId"

    iget-object v5, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v5, v7}, Lcom/android/calendar/task/at;->l(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 190
    sget-object v3, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 191
    iget-object v0, p0, Lcom/android/calendar/task/ap;->g:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x1

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 183
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 194
    :cond_1
    return-void
.end method

.method public c()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 197
    .line 198
    iget-object v1, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v1}, Lcom/android/calendar/task/at;->getCount()I

    move-result v2

    move v1, v0

    .line 199
    :goto_0
    if-ge v1, v2, :cond_0

    .line 200
    iget-object v3, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v3, v1}, Lcom/android/calendar/task/at;->j(I)I

    move-result v3

    if-eq v3, v1, :cond_1

    .line 201
    const/4 v0, 0x1

    .line 205
    :cond_0
    return v0

    .line 199
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 163
    const-wide/16 v0, 0x80

    return-wide v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 133
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 134
    new-instance v0, Lcom/android/calendar/task/at;

    iget-object v1, p0, Lcom/android/calendar/task/ap;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/task/at;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    .line 135
    iget-object v0, p0, Lcom/android/calendar/task/ap;->d:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    const v1, 0x7f02004f

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandleDrawable(I)V

    .line 136
    iget-object v0, p0, Lcom/android/calendar/task/ap;->d:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    iget v1, p0, Lcom/android/calendar/task/ap;->h:I

    invoke-virtual {v0, v2, v2, v1, v2}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandlePadding(IIII)V

    .line 137
    iget-object v0, p0, Lcom/android/calendar/task/ap;->c:Lcom/sec/android/touchwiz/widget/TwListView;

    iget-object v1, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    iget-object v0, p0, Lcom/android/calendar/task/ap;->d:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    iget-object v1, p0, Lcom/android/calendar/task/ap;->e:Lcom/android/calendar/task/at;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V

    .line 139
    iget-object v0, p0, Lcom/android/calendar/task/ap;->d:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndMode(Z)V

    .line 140
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 104
    iput-object p1, p0, Lcom/android/calendar/task/ap;->f:Landroid/app/Activity;

    .line 105
    iget-object v0, p0, Lcom/android/calendar/task/ap;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/task/ap;->h:I

    .line 107
    new-instance v0, Lcom/android/calendar/task/aq;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/task/aq;-><init>(Lcom/android/calendar/task/ap;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/task/ap;->g:Landroid/content/AsyncQueryHandler;

    .line 119
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 123
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 124
    const v0, 0x7f0400a9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/ap;->b:Landroid/view/View;

    .line 125
    iget-object v0, p0, Lcom/android/calendar/task/ap;->b:Landroid/view/View;

    const v1, 0x7f1200df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwListView;

    iput-object v0, p0, Lcom/android/calendar/task/ap;->c:Lcom/sec/android/touchwiz/widget/TwListView;

    .line 126
    new-instance v0, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    iget-object v1, p0, Lcom/android/calendar/task/ap;->f:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/task/ap;->c:Lcom/sec/android/touchwiz/widget/TwListView;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;-><init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwListView;)V

    iput-object v0, p0, Lcom/android/calendar/task/ap;->d:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    .line 128
    iget-object v0, p0, Lcom/android/calendar/task/ap;->b:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 153
    iget-object v0, p0, Lcom/android/calendar/task/ap;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/ap;->i:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 154
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 144
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 145
    iget-object v0, p0, Lcom/android/calendar/task/ap;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/task/ap;->i:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 146
    invoke-virtual {p0}, Lcom/android/calendar/task/ap;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    invoke-direct {p0}, Lcom/android/calendar/task/ap;->d()V

    .line 148
    :cond_0
    return-void
.end method
