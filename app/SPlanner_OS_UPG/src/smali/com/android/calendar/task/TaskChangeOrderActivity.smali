.class public Lcom/android/calendar/task/TaskChangeOrderActivity;
.super Lcom/android/calendar/b;
.source "TaskChangeOrderActivity.java"


# instance fields
.field private b:Lcom/android/calendar/task/ap;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 96
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->invalidateOptionsMenu()V

    .line 97
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f12006b

    .line 37
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->setContentView(I)V

    .line 40
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->c:Z

    .line 42
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ap;

    iput-object v0, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->b:Lcom/android/calendar/task/ap;

    .line 43
    iget-object v0, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->b:Lcom/android/calendar/task/ap;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/android/calendar/task/ap;

    invoke-direct {v0}, Lcom/android/calendar/task/ap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->b:Lcom/android/calendar/task/ap;

    .line 45
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->b:Lcom/android/calendar/task/ap;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 47
    iget-object v1, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->b:Lcom/android/calendar/task/ap;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 48
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 50
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f1200d4

    const/4 v3, 0x4

    const v2, 0x7f1200d5

    .line 54
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 55
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 56
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0f0398

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 57
    invoke-static {p0}, Lcom/android/calendar/hj;->u(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->c:Z

    if-eqz v0, :cond_1

    .line 58
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02005c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 59
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020057

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 64
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 65
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 66
    const/4 v0, 0x1

    return v0

    .line 61
    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 62
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 77
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 89
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 79
    :sswitch_0
    iget-object v1, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->b:Lcom/android/calendar/task/ap;

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->b:Lcom/android/calendar/task/ap;

    invoke-virtual {v1}, Lcom/android/calendar/task/ap;->b()V

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->finish()V

    goto :goto_0

    .line 86
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskChangeOrderActivity;->finish()V

    goto :goto_0

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f1200d4 -> :sswitch_1
        0x7f1200d5 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 71
    const v0, 0x7f1200d5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/TaskChangeOrderActivity;->b:Lcom/android/calendar/task/ap;

    invoke-virtual {v1}, Lcom/android/calendar/task/ap;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 72
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
