.class Lcom/android/calendar/task/bo;
.super Ljava/lang/Object;
.source "TaskListView.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnChildClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/task/TaskListView;


# direct methods
.method constructor <init>(Lcom/android/calendar/task/TaskListView;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/android/calendar/task/bo;->a:Lcom/android/calendar/task/TaskListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Lcom/sec/android/touchwiz/widget/TwExpandableListView;Landroid/view/View;IIJ)Z
    .locals 19

    .prologue
    .line 115
    const-wide/16 v2, -0x1

    cmp-long v2, p5, v2

    if-eqz v2, :cond_1

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/bo;->a:Lcom/android/calendar/task/TaskListView;

    invoke-static {v2}, Lcom/android/calendar/task/TaskListView;->d(Lcom/android/calendar/task/TaskListView;)Lcom/android/calendar/task/bq;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/task/bq;->c()J

    move-result-wide v2

    .line 117
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bo;->a:Lcom/android/calendar/task/TaskListView;

    invoke-static {v4}, Lcom/android/calendar/task/TaskListView;->d(Lcom/android/calendar/task/TaskListView;)Lcom/android/calendar/task/bq;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/android/calendar/task/bq;->a(Landroid/view/View;)V

    .line 118
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/bo;->a:Lcom/android/calendar/task/TaskListView;

    invoke-static {v4}, Lcom/android/calendar/task/TaskListView;->d(Lcom/android/calendar/task/TaskListView;)Lcom/android/calendar/task/bq;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/calendar/task/bq;->c()J

    move-result-wide v6

    .line 120
    const-wide/16 v4, -0x1

    cmp-long v4, v6, v4

    if-eqz v4, :cond_1

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/bo;->a:Lcom/android/calendar/task/TaskListView;

    invoke-static {v2}, Lcom/android/calendar/task/TaskListView;->e(Lcom/android/calendar/task/TaskListView;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 121
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/bo;->a:Lcom/android/calendar/task/TaskListView;

    invoke-static {v2}, Lcom/android/calendar/task/TaskListView;->a(Lcom/android/calendar/task/TaskListView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 122
    const-wide/16 v4, 0x2

    const-wide/16 v8, -0x1

    const-wide/16 v10, -0x1

    const/4 v12, -0x2

    const/4 v13, -0x2

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    const/16 v18, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v18}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJZ)V

    .line 126
    :cond_1
    const/4 v2, 0x0

    return v2
.end method
