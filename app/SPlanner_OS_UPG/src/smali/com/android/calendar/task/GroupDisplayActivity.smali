.class public Lcom/android/calendar/task/GroupDisplayActivity;
.super Lcom/android/calendar/b;
.source "GroupDisplayActivity.java"


# instance fields
.field private b:Lcom/android/calendar/task/u;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 95
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->invalidateOptionsMenu()V

    .line 96
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f12006b

    .line 37
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/GroupDisplayActivity;->setContentView(I)V

    .line 41
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/GroupDisplayActivity;->c:Z

    .line 43
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/u;

    iput-object v0, p0, Lcom/android/calendar/task/GroupDisplayActivity;->b:Lcom/android/calendar/task/u;

    .line 45
    iget-object v0, p0, Lcom/android/calendar/task/GroupDisplayActivity;->b:Lcom/android/calendar/task/u;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/android/calendar/task/u;

    invoke-direct {v0}, Lcom/android/calendar/task/u;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/GroupDisplayActivity;->b:Lcom/android/calendar/task/u;

    .line 48
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/android/calendar/task/GroupDisplayActivity;->b:Lcom/android/calendar/task/u;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 50
    iget-object v1, p0, Lcom/android/calendar/task/GroupDisplayActivity;->b:Lcom/android/calendar/task/u;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 51
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 53
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f1200d4

    const/4 v4, 0x4

    const/4 v3, 0x1

    const v2, 0x7f1200d5

    .line 57
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 58
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 59
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0f0398

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 60
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 61
    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/task/GroupDisplayActivity;->c:Z

    if-eqz v0, :cond_1

    .line 62
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02005c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 63
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020057

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 68
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 70
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 71
    return v3

    .line 65
    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 66
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 76
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 88
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 78
    :sswitch_0
    iget-object v1, p0, Lcom/android/calendar/task/GroupDisplayActivity;->b:Lcom/android/calendar/task/u;

    if-eqz v1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/android/calendar/task/GroupDisplayActivity;->b:Lcom/android/calendar/task/u;

    invoke-virtual {v1}, Lcom/android/calendar/task/u;->b()V

    .line 81
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->finish()V

    goto :goto_0

    .line 85
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDisplayActivity;->finish()V

    goto :goto_0

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f1200d4 -> :sswitch_1
        0x7f1200d5 -> :sswitch_0
    .end sparse-switch
.end method
