.class public Lcom/android/calendar/task/bq;
.super Landroid/widget/BaseExpandableListAdapter;
.source "TaskWindowAdapter.java"

# interfaces
.implements Lcom/android/calendar/hb;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private c:Landroid/content/Context;

.field private d:Lcom/android/calendar/task/bt;

.field private e:Lcom/android/calendar/task/TaskListView;

.field private f:I

.field private g:I

.field private h:Lcom/android/calendar/task/bs;

.field private final i:Ljava/util/LinkedList;

.field private final j:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final k:Z

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Ljava/lang/Runnable;

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:J

.field private r:I

.field private s:Lcom/android/calendar/task/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    const-class v0, Lcom/android/calendar/task/bq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/bq;->a:Ljava/lang/String;

    .line 96
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "utc_due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "previousId"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "parentId"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "task_order"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "groupName"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/task/bq;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/task/TaskListView;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 262
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 132
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    .line 134
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/bq;->j:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 143
    new-instance v0, Lcom/android/calendar/task/br;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/br;-><init>(Lcom/android/calendar/task/bq;)V

    iput-object v0, p0, Lcom/android/calendar/task/bq;->n:Ljava/lang/Runnable;

    .line 156
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/task/bq;->q:J

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/task/bq;->r:I

    .line 318
    iput-object v2, p0, Lcom/android/calendar/task/bq;->s:Lcom/android/calendar/task/ah;

    .line 263
    iput-object p1, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    .line 265
    iget-object v0, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/bq;->k:Z

    .line 267
    iget-object v0, p0, Lcom/android/calendar/task/bq;->n:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/bq;->l:Ljava/lang/String;

    .line 268
    iput-object p2, p0, Lcom/android/calendar/task/bq;->e:Lcom/android/calendar/task/TaskListView;

    .line 269
    new-instance v0, Lcom/android/calendar/task/bt;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/task/bt;-><init>(Lcom/android/calendar/task/bq;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/task/bq;->d:Lcom/android/calendar/task/bt;

    .line 270
    iput-boolean p3, p0, Lcom/android/calendar/task/bq;->m:Z

    .line 272
    iput-object v2, p0, Lcom/android/calendar/task/bq;->p:Ljava/lang/String;

    .line 273
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;I)I
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lcom/android/calendar/task/bq;->g:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;J)J
    .locals 1

    .prologue
    .line 57
    iput-wide p1, p0, Lcom/android/calendar/task/bq;->q:J

    return-wide p1
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;Lcom/android/calendar/task/ah;)Lcom/android/calendar/task/ah;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/android/calendar/task/bq;->s:Lcom/android/calendar/task/ah;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;Lcom/android/calendar/task/bs;)Lcom/android/calendar/task/bs;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/android/calendar/task/bq;->h:Lcom/android/calendar/task/bs;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/android/calendar/task/bq;->l:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;Lcom/android/calendar/task/bu;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/android/calendar/task/bq;->a(Lcom/android/calendar/task/bu;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;Z)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/android/calendar/task/bq;->a(Z)V

    return-void
.end method

.method private a(Lcom/android/calendar/task/bu;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 528
    iget-object v0, p0, Lcom/android/calendar/task/bq;->d:Lcom/android/calendar/task/bt;

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/bt;->cancelOperation(I)V

    .line 532
    sget-object v3, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    .line 533
    iget-object v0, p0, Lcom/android/calendar/task/bq;->d:Lcom/android/calendar/task/bt;

    sget-object v4, Lcom/android/calendar/task/bq;->b:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/calendar/task/bq;->f()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    iget v2, p0, Lcom/android/calendar/task/bq;->r:I

    invoke-static {v2}, Lcom/android/calendar/task/bq;->i(I)Ljava/lang/String;

    move-result-object v7

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/task/bt;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 538
    iget-object v0, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1200dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 541
    if-eqz v0, :cond_1

    .line 543
    const v1, 0x7f1200dd

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 544
    const v2, 0x7f0201ce

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 546
    const v1, 0x7f1200de

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 547
    const v2, 0x7f0f02dc

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 549
    iget-boolean v2, p0, Lcom/android/calendar/task/bq;->k:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/hj;->u(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 550
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 551
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 554
    :cond_0
    if-eqz p1, :cond_2

    .line 555
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 562
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->g(Landroid/content/Context;)I

    move-result v0

    .line 563
    iget-boolean v2, p0, Lcom/android/calendar/task/bq;->k:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    .line 564
    const-string v0, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 565
    if-eqz v0, :cond_1

    .line 566
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 570
    :cond_1
    return-void

    .line 557
    :cond_2
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 558
    iget-object v0, p0, Lcom/android/calendar/task/bq;->e:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v0, v3}, Lcom/android/calendar/task/TaskListView;->setVisibility(I)V

    .line 559
    iget-object v0, p0, Lcom/android/calendar/task/bq;->e:Lcom/android/calendar/task/TaskListView;

    invoke-virtual {v0, v3}, Lcom/android/calendar/task/TaskListView;->a(Z)V

    goto :goto_0
.end method

.method private a(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 485
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v2

    .line 486
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    monitor-exit v2

    move v0, v1

    .line 489
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->d:I

    if-gt v0, p1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->e:I

    if-gt p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    monitor-exit v2

    goto :goto_0

    .line 490
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    .line 489
    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/task/bq;II)Z
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/task/bq;->a(II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/task/bq;I)Lcom/android/calendar/task/bs;
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/android/calendar/task/bq;->h(I)Lcom/android/calendar/task/bs;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/task/bq;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/task/bq;I)I
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lcom/android/calendar/task/bq;->f:I

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/task/bq;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/android/calendar/task/bq;->o:Z

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/task/bq;I)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/android/calendar/task/bq;->f:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/task/bq;->f:I

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/task/bq;)Lcom/android/calendar/task/TaskListView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/task/bq;->e:Lcom/android/calendar/task/TaskListView;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/task/bq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/task/bq;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/task/bq;)J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/android/calendar/task/bq;->q:J

    return-wide v0
.end method

.method private f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/task/bq;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/android/calendar/task/bq;->m:Z

    return v0
.end method

.method private h(I)Lcom/android/calendar/task/bs;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 435
    iget-object v3, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v3

    .line 437
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 438
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    const/4 v4, 0x5

    if-lt v2, v4, :cond_3

    .line 439
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    .line 440
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 441
    iget v2, p0, Lcom/android/calendar/task/bq;->f:I

    iget v4, v0, Lcom/android/calendar/task/bs;->g:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/android/calendar/task/bq;->f:I

    .line 448
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 449
    iget-object v1, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 450
    iget-object v1, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 452
    :cond_1
    monitor-exit v3

    .line 476
    :goto_1
    return-object v0

    .line 442
    :cond_2
    if-nez p1, :cond_0

    .line 443
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 444
    iget v2, p0, Lcom/android/calendar/task/bq;->f:I

    iget v4, v0, Lcom/android/calendar/task/bs;->g:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/android/calendar/task/bq;->f:I

    .line 446
    const/4 v2, 0x0

    iput v2, v0, Lcom/android/calendar/task/bs;->g:I

    goto :goto_0

    .line 477
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 456
    :cond_3
    :try_start_1
    iget v2, p0, Lcom/android/calendar/task/bq;->f:I

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    if-ne p1, v2, :cond_6

    .line 457
    :cond_4
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/calendar/task/bq;->f:I

    move-object v2, v0

    .line 461
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 462
    if-eqz v0, :cond_8

    .line 464
    iget-object v2, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 465
    iget v2, v0, Lcom/android/calendar/task/bs;->g:I

    add-int/2addr v1, v2

    move v2, v1

    move-object v1, v0

    .line 468
    :goto_3
    if-nez v0, :cond_7

    .line 470
    if-eqz v1, :cond_5

    .line 471
    const/4 v0, 0x0

    iput-object v0, v1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    .line 472
    iput v2, v1, Lcom/android/calendar/task/bs;->g:I

    :cond_5
    move-object v0, v1

    .line 476
    :cond_6
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_7
    move v5, v2

    move-object v2, v1

    move v1, v5

    goto :goto_2

    :cond_8
    move v5, v1

    move-object v1, v2

    move v2, v5

    goto :goto_3
.end method

.method static synthetic h(Lcom/android/calendar/task/bq;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/task/bq;->j:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/task/bq;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/android/calendar/task/bq;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/task/bq;->g:I

    return v0
.end method

.method private static i(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 996
    .line 997
    packed-switch p0, :pswitch_data_0

    .line 1017
    const-string v0, "utc_due_date ASC, importance DESC,group_order ASC, task_order ASC"

    .line 1020
    :goto_0
    return-object v0

    .line 999
    :pswitch_0
    const-string v0, "utc_due_date ASC, importance DESC,group_order ASC, task_order ASC"

    goto :goto_0

    .line 1002
    :pswitch_1
    const-string v0, "importance DESC, utc_due_date ASC,group_order ASC, task_order ASC"

    goto :goto_0

    .line 1005
    :pswitch_2
    const-string v0, "utc_due_date ASC, importance DESC, group_order ASC,task_order ASC"

    goto :goto_0

    .line 1008
    :pswitch_3
    const-string v0, "utc_due_date ASC, importance DESC, group_order ASC,task_order ASC"

    goto :goto_0

    .line 1011
    :pswitch_4
    const-string v0, "utc_due_date ASC, importance DESC, group_order ASC, task_order ASC"

    goto :goto_0

    .line 1014
    :pswitch_5
    const-string v0, "group_order ASC, task_order ASC"

    goto :goto_0

    .line 997
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic j(Lcom/android/calendar/task/bq;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/android/calendar/task/bq;->f:I

    return v0
.end method


# virtual methods
.method public a(I)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 823
    iget-boolean v1, p0, Lcom/android/calendar/task/bq;->k:Z

    if-nez v1, :cond_1

    .line 832
    :cond_0
    :goto_0
    return v0

    .line 827
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/bq;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v1

    .line 828
    if-eqz v1, :cond_0

    .line 829
    iget-object v2, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget v3, v1, Lcom/android/calendar/task/bs;->f:I

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, Lcom/android/calendar/task/ai;->a(I)I

    move-result v2

    .line 830
    if-eq v2, v0, :cond_0

    iget v0, v1, Lcom/android/calendar/task/bs;->f:I

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/task/bq;->o:Z

    .line 427
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/calendar/task/bq;->h(I)Lcom/android/calendar/task/bs;

    .line 428
    invoke-virtual {p0}, Lcom/android/calendar/task/bq;->notifyDataSetInvalidated()V

    .line 429
    iget-object v0, p0, Lcom/android/calendar/task/bq;->d:Lcom/android/calendar/task/bt;

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/android/calendar/task/bq;->d:Lcom/android/calendar/task/bt;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/bt;->cancelOperation(I)V

    .line 432
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 812
    iput-wide p1, p0, Lcom/android/calendar/task/bq;->q:J

    .line 813
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/task/bq;->s:Lcom/android/calendar/task/ah;

    .line 814
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 794
    if-eqz p1, :cond_0

    .line 795
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 796
    instance-of v1, v0, Lcom/android/calendar/task/ah;

    if-eqz v1, :cond_0

    .line 797
    check-cast v0, Lcom/android/calendar/task/ah;

    iput-object v0, p0, Lcom/android/calendar/task/bq;->s:Lcom/android/calendar/task/ah;

    .line 798
    iget-object v0, p0, Lcom/android/calendar/task/bq;->s:Lcom/android/calendar/task/ah;

    iget-wide v0, v0, Lcom/android/calendar/task/ah;->d:J

    iput-wide v0, p0, Lcom/android/calendar/task/bq;->q:J

    .line 801
    :cond_0
    return-void
.end method

.method public b(I)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 838
    if-ltz p1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/task/bq;->k:Z

    if-nez v1, :cond_1

    .line 845
    :cond_0
    :goto_0
    return v0

    .line 841
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/bq;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v1

    .line 842
    if-eqz v1, :cond_0

    .line 843
    iget-object v0, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget v1, v1, Lcom/android/calendar/task/bs;->f:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ai;->b(I)I

    move-result v0

    goto :goto_0
.end method

.method public b()Lcom/android/calendar/task/ah;
    .locals 1

    .prologue
    .line 804
    iget-object v0, p0, Lcom/android/calendar/task/bq;->s:Lcom/android/calendar/task/ah;

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 808
    iget-wide v0, p0, Lcom/android/calendar/task/bq;->q:J

    return-wide v0
.end method

.method protected c(I)Lcom/android/calendar/task/bs;
    .locals 5

    .prologue
    .line 334
    iget-object v1, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v1

    .line 335
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/bq;->h:Lcom/android/calendar/task/bs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/bq;->h:Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->f:I

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/bq;->h:Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->f:I

    iget-object v2, p0, Lcom/android/calendar/task/bq;->h:Lcom/android/calendar/task/bs;

    iget v2, v2, Lcom/android/calendar/task/bs;->g:I

    add-int/2addr v0, v2

    if-ge p1, v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/android/calendar/task/bq;->h:Lcom/android/calendar/task/bs;

    monitor-exit v1

    .line 347
    :goto_0
    return-object v0

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 340
    iget v3, v0, Lcom/android/calendar/task/bs;->f:I

    if-gt v3, p1, :cond_1

    iget v3, v0, Lcom/android/calendar/task/bs;->f:I

    iget v4, v0, Lcom/android/calendar/task/bs;->g:I

    add-int/2addr v3, v4

    if-ge p1, v3, :cond_1

    .line 342
    iput-object v0, p0, Lcom/android/calendar/task/bq;->h:Lcom/android/calendar/task/bs;

    .line 343
    monitor-exit v1

    goto :goto_0

    .line 346
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 5

    .prologue
    .line 849
    const/4 v1, 0x0

    .line 850
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v2

    .line 851
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 852
    iget-object v0, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    .line 853
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 854
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 856
    goto :goto_0

    .line 857
    :cond_0
    monitor-exit v2

    .line 858
    return v1

    .line 857
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public d(I)J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 372
    if-gez p1, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-wide v0

    .line 375
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/bq;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v2

    .line 376
    if-eqz v2, :cond_0

    .line 377
    iget-object v0, v2, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ai;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public e()I
    .locals 6

    .prologue
    .line 862
    const/4 v1, 0x0

    .line 863
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v2

    .line 864
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 865
    iget-object v4, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    .line 866
    if-eqz v4, :cond_1

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 867
    const/4 v0, -0x1

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    .line 868
    :cond_0
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 869
    const/4 v1, 0x5

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 871
    const/4 v5, 0x1

    if-ne v1, v5, :cond_0

    .line 872
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    .line 876
    goto :goto_0

    .line 877
    :cond_3
    monitor-exit v2

    .line 878
    return v1

    .line 877
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e(I)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 910
    .line 911
    const-wide/16 v2, -0x1

    .line 914
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/bq;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v1

    .line 915
    if-eqz v1, :cond_1

    .line 916
    iget-object v0, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ai;->getItemId(I)J

    move-result-wide v4

    .line 917
    iget-object v0, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ai;->d(I)I

    move-result v6

    .line 920
    const/4 v0, 0x0

    :goto_0
    iget-object v7, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v7}, Lcom/android/calendar/task/ai;->getCount()I

    move-result v7

    if-ge v0, v7, :cond_0

    .line 921
    iget-object v7, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v7, v0}, Lcom/android/calendar/task/ai;->d(I)I

    move-result v7

    if-ne v6, v7, :cond_3

    .line 922
    iget-object v7, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v7, v0}, Lcom/android/calendar/task/ai;->getItemId(I)J

    move-result-wide v8

    cmp-long v7, v4, v8

    if-nez v7, :cond_2

    .line 929
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 930
    const-string v1, "parentId"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 932
    sget-object v1, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 933
    iget-object v2, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v0, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 935
    :cond_1
    return-void

    .line 925
    :cond_2
    iget-object v2, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/ai;->getItemId(I)J

    move-result-wide v2

    .line 920
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public f(I)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 941
    .line 945
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/bq;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v4

    .line 946
    if-eqz v4, :cond_1

    .line 947
    iget-object v0, v4, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ai;->getItemId(I)J

    move-result-wide v6

    .line 948
    iget-object v0, v4, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ai;->c(I)J

    move-result-wide v2

    .line 949
    iget-object v0, v4, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ai;->d(I)I

    move-result v5

    .line 952
    if-ne v5, v10, :cond_2

    .line 953
    const-wide/16 v2, 0x0

    .line 964
    :cond_0
    :goto_0
    iget-object v0, v4, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    add-int/lit8 v4, v5, -0x1

    invoke-virtual {v0, p1, v4}, Lcom/android/calendar/task/ai;->a(II)V

    .line 965
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v10}, Landroid/content/ContentValues;-><init>(I)V

    .line 966
    const-string v4, "parentId"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 967
    const-string v2, "unindent"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 969
    sget-object v1, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v1, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 970
    iget-object v2, p0, Lcom/android/calendar/task/bq;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v0, v11, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 972
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 955
    :goto_1
    iget-object v8, v4, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v8}, Lcom/android/calendar/task/ai;->getCount()I

    move-result v8

    if-ge v0, v8, :cond_0

    .line 956
    add-int/lit8 v8, v5, -0x1

    iget-object v9, v4, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v9, v0}, Lcom/android/calendar/task/ai;->d(I)I

    move-result v9

    if-ne v8, v9, :cond_3

    iget-object v8, v4, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v8, v0}, Lcom/android/calendar/task/ai;->getItemId(I)J

    move-result-wide v8

    cmp-long v8, v2, v8

    if-nez v8, :cond_3

    .line 958
    iget-object v2, v4, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/ai;->c(I)J

    move-result-wide v2

    goto :goto_0

    .line 955
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public g(I)V
    .locals 0

    .prologue
    .line 989
    iput p1, p0, Lcom/android/calendar/task/bq;->r:I

    .line 990
    return-void
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1027
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 5

    .prologue
    .line 1032
    const/4 v0, 0x0

    .line 1034
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1035
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 1036
    iget-object v4, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v4}, Lcom/android/calendar/task/ai;->a()I

    move-result v4

    .line 1037
    add-int/2addr v1, v4

    .line 1038
    if-le v1, p1, :cond_0

    .line 1039
    iget-object v0, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    sub-int/2addr v1, v4

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ai;->h(I)I

    move-result v0

    int-to-long v0, v0

    monitor-exit v2

    .line 1044
    :goto_0
    return-wide v0

    .line 1043
    :cond_1
    monitor-exit v2

    .line 1044
    int-to-long v0, p2

    goto :goto_0

    .line 1043
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1050
    const/4 v0, 0x0

    .line 1052
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1053
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 1054
    iget-object v4, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v4}, Lcom/android/calendar/task/ai;->a()I

    move-result v4

    .line 1055
    add-int/2addr v1, v4

    .line 1056
    if-le v1, p1, :cond_0

    .line 1057
    iget-object v3, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget-object v0, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    sub-int/2addr v1, v4

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ai;->i(I)I

    move-result v0

    add-int/2addr v0, p2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0, p4, p5}, Lcom/android/calendar/task/ai;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    monitor-exit v2

    .line 1064
    :goto_0
    return-object v0

    .line 1063
    :cond_1
    monitor-exit v2

    .line 1064
    const/4 v0, 0x0

    goto :goto_0

    .line 1063
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getChildrenCount(I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1069
    .line 1071
    iget-object v3, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v3

    .line 1072
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 1073
    iget-object v5, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v5}, Lcom/android/calendar/task/ai;->a()I

    move-result v5

    .line 1074
    add-int/2addr v2, v5

    .line 1075
    if-le v2, p1, :cond_0

    .line 1076
    iget-object v0, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    sub-int v1, v2, v5

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ai;->h(I)I

    move-result v0

    monitor-exit v3

    .line 1081
    :goto_0
    return v0

    .line 1080
    :cond_1
    monitor-exit v3

    move v0, v1

    .line 1081
    goto :goto_0

    .line 1080
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1087
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 4

    .prologue
    .line 1092
    const/4 v0, 0x0

    .line 1093
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1094
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 1095
    iget-object v0, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0}, Lcom/android/calendar/task/ai;->a()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1096
    goto :goto_0

    .line 1097
    :cond_0
    monitor-exit v2

    .line 1098
    return v1

    .line 1097
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 1103
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 1109
    const/4 v0, 0x0

    .line 1111
    iget-object v2, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1112
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/task/bq;->i:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 1113
    iget-object v4, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v4}, Lcom/android/calendar/task/ai;->a()I

    move-result v4

    .line 1114
    iget-object v5, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/calendar/task/ai;->a(Ljava/lang/Boolean;)V

    .line 1115
    add-int/2addr v1, v4

    .line 1116
    if-le v1, p1, :cond_0

    .line 1117
    iget-object v3, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget-object v0, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    sub-int/2addr v1, v4

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ai;->i(I)I

    move-result v0

    invoke-virtual {v3, v0, p3, p4}, Lcom/android/calendar/task/ai;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    monitor-exit v2

    .line 1123
    :goto_0
    return-object v0

    .line 1122
    :cond_1
    monitor-exit v2

    .line 1123
    const/4 v0, 0x0

    goto :goto_0

    .line 1122
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 1129
    const/4 v0, 0x1

    return v0
.end method
