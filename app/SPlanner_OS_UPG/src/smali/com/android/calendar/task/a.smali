.class public Lcom/android/calendar/task/a;
.super Landroid/app/Fragment;
.source "GroupChangeOrderFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# static fields
.field public static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private c:Landroid/view/View;

.field private d:Lcom/sec/android/touchwiz/widget/TwListView;

.field private e:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

.field private f:Lcom/android/calendar/task/d;

.field private g:Landroid/app/Activity;

.field private h:Landroid/content/AsyncQueryHandler;

.field private i:Landroid/database/Cursor;

.field private j:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/a;->a:Landroid/net/Uri;

    .line 58
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_accountId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "groupName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "group_order"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/task/a;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/task/a;->c:Landroid/view/View;

    .line 195
    new-instance v0, Lcom/android/calendar/task/c;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/task/c;-><init>(Lcom/android/calendar/task/a;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/task/a;->j:Landroid/database/ContentObserver;

    .line 77
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/a;)Lcom/android/calendar/task/d;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    return-object v0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/calendar/task/a;->i:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/a;->i:Landroid/database/Cursor;

    if-eq p1, v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/android/calendar/task/a;->i:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 96
    :cond_0
    if-nez p1, :cond_2

    .line 97
    iput-object p1, p0, Lcom/android/calendar/task/a;->i:Landroid/database/Cursor;

    .line 98
    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0}, Lcom/android/calendar/task/d;->clear()V

    .line 115
    :cond_1
    return-void

    .line 102
    :cond_2
    iput-object p1, p0, Lcom/android/calendar/task/a;->i:Landroid/database/Cursor;

    .line 103
    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0}, Lcom/android/calendar/task/d;->clear()V

    .line 105
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 107
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 109
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 110
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 111
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 113
    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    new-instance v1, Lcom/android/calendar/task/e;

    invoke-direct/range {v1 .. v7}, Lcom/android/calendar/task/e;-><init>(JJLjava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/d;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/task/a;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/calendar/task/a;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/task/a;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/calendar/task/a;->g:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lcom/android/calendar/task/a;->h:Landroid/content/AsyncQueryHandler;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/android/calendar/task/a;->h:Landroid/content/AsyncQueryHandler;

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 178
    iget-object v0, p0, Lcom/android/calendar/task/a;->h:Landroid/content/AsyncQueryHandler;

    sget-object v3, Lcom/android/calendar/task/a;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/task/a;->b:[Ljava/lang/String;

    const-string v7, "_accountId ASC, group_order ASC"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 190
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/android/calendar/task/a;->a()V

    .line 193
    :cond_0
    return-void
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 208
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0}, Lcom/android/calendar/task/d;->getCount()I

    move-result v0

    if-ge v7, v0, :cond_0

    .line 209
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 210
    const-string v0, "group_order"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 212
    sget-object v1, Lcom/android/calendar/task/a;->a:Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0, v7}, Lcom/android/calendar/task/d;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/e;

    invoke-virtual {v0}, Lcom/android/calendar/task/e;->a()J

    move-result-wide v8

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 214
    iget-object v0, p0, Lcom/android/calendar/task/a;->h:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x1

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 208
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 216
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 219
    move v1, v2

    .line 221
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0}, Lcom/android/calendar/task/d;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/d;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/e;

    invoke-virtual {v0}, Lcom/android/calendar/task/e;->c()J

    move-result-wide v4

    int-to-long v6, v1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    .line 223
    const/4 v2, 0x1

    .line 227
    :cond_0
    return v2

    .line 221
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 185
    const-wide/16 v0, 0x80

    return-wide v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v5, 0x7f02004f

    const/4 v4, 0x0

    .line 143
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 145
    invoke-virtual {p0}, Lcom/android/calendar/task/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 148
    new-instance v1, Lcom/android/calendar/task/d;

    iget-object v2, p0, Lcom/android/calendar/task/a;->g:Landroid/app/Activity;

    const v3, 0x7f040060

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/android/calendar/task/d;-><init>(Lcom/android/calendar/task/a;Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    .line 149
    iget-object v0, p0, Lcom/android/calendar/task/a;->e:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    const/16 v1, 0xa

    invoke-virtual {v0, v4, v4, v1, v4}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandlePadding(IIII)V

    .line 150
    iget-object v0, p0, Lcom/android/calendar/task/a;->e:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, v5}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandleDrawable(I)V

    .line 151
    iget-object v0, p0, Lcom/android/calendar/task/a;->d:Lcom/sec/android/touchwiz/widget/TwListView;

    iget-object v1, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 152
    iget-object v0, p0, Lcom/android/calendar/task/a;->e:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    iget-object v1, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V

    .line 153
    iget-object v0, p0, Lcom/android/calendar/task/a;->e:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndMode(Z)V

    .line 154
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 82
    iput-object p1, p0, Lcom/android/calendar/task/a;->g:Landroid/app/Activity;

    .line 83
    new-instance v0, Lcom/android/calendar/task/b;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/task/b;-><init>(Lcom/android/calendar/task/a;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/task/a;->h:Landroid/content/AsyncQueryHandler;

    .line 90
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 130
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 134
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 135
    const v0, 0x7f040059

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/a;->c:Landroid/view/View;

    .line 136
    iget-object v0, p0, Lcom/android/calendar/task/a;->c:Landroid/view/View;

    const v1, 0x7f1200df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwListView;

    iput-object v0, p0, Lcom/android/calendar/task/a;->d:Lcom/sec/android/touchwiz/widget/TwListView;

    .line 137
    new-instance v0, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    iget-object v1, p0, Lcom/android/calendar/task/a;->g:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/task/a;->d:Lcom/sec/android/touchwiz/widget/TwListView;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;-><init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwListView;)V

    iput-object v0, p0, Lcom/android/calendar/task/a;->e:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    .line 138
    iget-object v0, p0, Lcom/android/calendar/task/a;->c:Landroid/view/View;

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 120
    iget-object v0, p0, Lcom/android/calendar/task/a;->i:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/android/calendar/task/a;->f:Lcom/android/calendar/task/d;

    invoke-virtual {v0}, Lcom/android/calendar/task/d;->clear()V

    .line 122
    iget-object v0, p0, Lcom/android/calendar/task/a;->i:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/task/a;->i:Landroid/database/Cursor;

    .line 125
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/calendar/task/a;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/a;->j:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 171
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 172
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 158
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 159
    iget-object v0, p0, Lcom/android/calendar/task/a;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/task/a;->a:Landroid/net/Uri;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/calendar/task/a;->j:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 162
    invoke-virtual {p0}, Lcom/android/calendar/task/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/android/calendar/task/a;->h:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x2

    sget-object v3, Lcom/android/calendar/task/a;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/task/a;->b:[Ljava/lang/String;

    const-string v7, "_accountId ASC, group_order ASC"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    return-void
.end method
