.class public Lcom/android/calendar/task/TaskDeleteView;
.super Landroid/widget/ListView;
.source "TaskDeleteView.java"


# instance fields
.field private a:Lcom/android/calendar/task/au;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Landroid/text/format/Time;

.field private e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Lcom/android/calendar/task/bl;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/bl;-><init>(Lcom/android/calendar/task/TaskDeleteView;)V

    iput-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->e:Ljava/lang/Runnable;

    .line 58
    invoke-direct {p0, p1}, Lcom/android/calendar/task/TaskDeleteView;->a(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/TaskDeleteView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/task/TaskDeleteView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/android/calendar/task/TaskDeleteView;->c:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 62
    iput-object p1, p0, Lcom/android/calendar/task/TaskDeleteView;->b:Landroid/content/Context;

    .line 63
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->e:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->c:Ljava/lang/String;

    .line 64
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/task/TaskDeleteView;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->d:Landroid/text/format/Time;

    .line 67
    invoke-virtual {p0, v4}, Lcom/android/calendar/task/TaskDeleteView;->setVerticalScrollBarEnabled(Z)V

    .line 68
    new-instance v0, Lcom/android/calendar/task/au;

    const v1, 0x7f0a0007

    invoke-static {p1, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v1

    invoke-direct {v0, p1, p0, v1}, Lcom/android/calendar/task/au;-><init>(Landroid/content/Context;Lcom/android/calendar/task/TaskDeleteView;Z)V

    iput-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    .line 70
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/task/au;->a(J)V

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskDeleteView;->setCacheColorHint(I)V

    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskDeleteView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 75
    invoke-virtual {p0, v4}, Lcom/android/calendar/task/TaskDeleteView;->setDividerHeight(I)V

    .line 76
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/task/TaskDeleteView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/task/TaskDeleteView;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->d:Landroid/text/format/Time;

    return-object v0
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteView;->getFirstVisibleView()Landroid/view/View;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_2

    .line 177
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 178
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 181
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskDeleteView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 182
    add-int v2, v0, p1

    iget v0, v1, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_1

    iget v0, v1, Landroid/graphics/Rect;->top:I

    neg-int v0, v0

    :goto_0
    invoke-virtual {p0, v2, v0}, Lcom/android/calendar/task/TaskDeleteView;->setSelectionFromTop(II)V

    .line 204
    :cond_0
    :goto_1
    return-void

    .line 182
    :cond_1
    iget v0, v1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteView;->getSelectedItemPosition()I

    move-result v0

    if-ltz v0, :cond_0

    .line 202
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteView;->getSelectedItemPosition()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskDeleteView;->setSelection(I)V

    goto :goto_1
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_0

    .line 144
    iget-object v1, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget v0, v0, Lcom/android/calendar/task/bs;->f:I

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/ai;->f(I)I

    move-result v0

    .line 146
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 208
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v0}, Lcom/android/calendar/task/au;->b()V

    .line 209
    return-void
.end method

.method public a(Landroid/text/format/Time;JLjava/lang/String;Z)V
    .locals 6

    .prologue
    .line 85
    if-nez p1, :cond_0

    .line 86
    iget-object p1, p0, Lcom/android/calendar/task/TaskDeleteView;->d:Landroid/text/format/Time;

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->d:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 94
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->d:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/task/TaskDeleteView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->d:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 99
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    iget-object v1, p0, Lcom/android/calendar/task/TaskDeleteView;->d:Landroid/text/format/Time;

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/task/au;->a(Landroid/text/format/Time;JLjava/lang/String;Z)V

    .line 100
    return-void
.end method

.method public a(Z)V
    .locals 6

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    iget-object v1, p0, Lcom/android/calendar/task/TaskDeleteView;->d:Landroid/text/format/Time;

    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/task/au;->a(Landroid/text/format/Time;JLjava/lang/String;Z)V

    .line 104
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/android/calendar/task/TaskDeleteView;->c(I)V

    .line 163
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteView;->getSelectedItemPosition()I

    move-result v0

    .line 164
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 165
    add-int/2addr v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/task/TaskDeleteView;->setSelectionFromTop(II)V

    .line 167
    :cond_0
    return-void
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteView;->getFirstVisibleView()Landroid/view/View;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskDeleteView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 121
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFirstVisibleView()Landroid/view/View;
    .locals 5

    .prologue
    .line 125
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 126
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteView;->getChildCount()I

    move-result v3

    .line 127
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 128
    invoke-virtual {p0, v1}, Lcom/android/calendar/task/TaskDeleteView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 129
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 130
    iget v4, v2, Landroid/graphics/Rect;->top:I

    if-ltz v4, :cond_0

    .line 134
    :goto_1
    return-object v0

    .line 127
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 134
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSelectedTaskId()J
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v0}, Lcom/android/calendar/task/au;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedViewHolder()Lcom/android/calendar/task/ah;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v0}, Lcom/android/calendar/task/au;->c()Lcom/android/calendar/task/ah;

    move-result-object v0

    return-object v0
.end method

.method public getTaskDeleteAdapter()Lcom/android/calendar/task/au;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/widget/ListView;->onDetachedFromWindow()V

    .line 81
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v0}, Lcom/android/calendar/task/au;->a()V

    .line 82
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 275
    check-cast p1, Lcom/android/calendar/task/TaskDeleteView$SavedCheckState;

    .line 276
    invoke-virtual {p1}, Lcom/android/calendar/task/TaskDeleteView$SavedCheckState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 278
    iget-object v1, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    iget v2, p1, Lcom/android/calendar/task/TaskDeleteView$SavedCheckState;->a:I

    if-ne v2, v0, :cond_1

    :goto_0
    iput-boolean v0, v1, Lcom/android/calendar/task/au;->a:Z

    .line 280
    iget-object v0, p1, Lcom/android/calendar/task/TaskDeleteView$SavedCheckState;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    iget-object v1, p1, Lcom/android/calendar/task/TaskDeleteView$SavedCheckState;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/au;->a(Ljava/util/HashMap;)V

    .line 283
    :cond_0
    return-void

    .line 278
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 262
    invoke-super {p0}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 264
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    if-nez v0, :cond_0

    .line 265
    const/4 v0, 0x0

    .line 270
    :goto_0
    return-object v0

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v0}, Lcom/android/calendar/task/au;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 268
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v1}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v3

    .line 270
    new-instance v1, Lcom/android/calendar/task/TaskDeleteView$SavedCheckState;

    invoke-direct {v1, v2, v0, v3}, Lcom/android/calendar/task/TaskDeleteView$SavedCheckState;-><init>(Landroid/os/Parcelable;ILjava/util/HashMap;)V

    move-object v0, v1

    goto :goto_0

    .line 267
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setListBy(I)V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/au;->e(I)V

    .line 213
    return-void
.end method

.method public setSelectedTaskId(J)V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteView;->a:Lcom/android/calendar/task/au;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/task/au;->a(J)V

    .line 155
    return-void
.end method
