.class public Lcom/android/calendar/task/am;
.super Ljava/lang/Object;
.source "TaskByDayAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/task/ai;

.field private final b:J


# direct methods
.method private constructor <init>(Lcom/android/calendar/task/ai;J)V
    .locals 0

    .prologue
    .line 999
    iput-object p1, p0, Lcom/android/calendar/task/am;->a:Lcom/android/calendar/task/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1000
    iput-wide p2, p0, Lcom/android/calendar/task/am;->b:J

    .line 1001
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/task/ai;JLcom/android/calendar/task/aj;)V
    .locals 0

    .prologue
    .line 996
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/task/am;-><init>(Lcom/android/calendar/task/ai;J)V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1004
    invoke-virtual {p1, v2}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 1005
    iget-object v0, p0, Lcom/android/calendar/task/am;->a:Lcom/android/calendar/task/ai;

    iget-object v0, v0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    const-string v1, "preferences_list_by"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 1008
    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    .line 1009
    iget-object v0, p0, Lcom/android/calendar/task/am;->a:Lcom/android/calendar/task/ai;

    iget-wide v2, p0, Lcom/android/calendar/task/am;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/task/ai;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010
    iget-object v0, p0, Lcom/android/calendar/task/am;->a:Lcom/android/calendar/task/ai;

    iget-wide v2, p0, Lcom/android/calendar/task/am;->b:J

    invoke-virtual {v0, p2, v2, v3}, Lcom/android/calendar/task/ai;->a(ZJ)V

    .line 1017
    :goto_0
    return-void

    .line 1012
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/am;->a:Lcom/android/calendar/task/ai;

    iget-wide v2, p0, Lcom/android/calendar/task/am;->b:J

    invoke-static {v0, p2, v2, v3}, Lcom/android/calendar/task/ai;->a(Lcom/android/calendar/task/ai;ZJ)V

    goto :goto_0

    .line 1015
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/am;->a:Lcom/android/calendar/task/ai;

    iget-wide v2, p0, Lcom/android/calendar/task/am;->b:J

    invoke-static {v0, p2, v2, v3}, Lcom/android/calendar/task/ai;->a(Lcom/android/calendar/task/ai;ZJ)V

    goto :goto_0
.end method
