.class Lcom/android/calendar/task/g;
.super Lcom/android/calendar/ag;
.source "GroupCreateFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/task/f;


# direct methods
.method constructor <init>(Lcom/android/calendar/task/f;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 96
    if-nez p3, :cond_0

    .line 120
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0, p3}, Lcom/android/calendar/task/f;->a(Lcom/android/calendar/task/f;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 100
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lcom/android/calendar/task/f;->a(Lcom/android/calendar/task/f;J)J

    .line 101
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->a(Lcom/android/calendar/task/f;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 103
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->b(Lcom/android/calendar/task/f;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->b(Lcom/android/calendar/task/f;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v1}, Lcom/android/calendar/task/f;->b(Lcom/android/calendar/task/f;)Landroid/database/Cursor;

    move-result-object v1

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 106
    iget-object v2, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v2}, Lcom/android/calendar/task/f;->b(Lcom/android/calendar/task/f;)Landroid/database/Cursor;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v3}, Lcom/android/calendar/task/f;->b(Lcom/android/calendar/task/f;)Landroid/database/Cursor;

    move-result-object v3

    const-string v4, "groupName"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 107
    iget-object v3, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v3}, Lcom/android/calendar/task/f;->a(Lcom/android/calendar/task/f;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v3, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v3}, Lcom/android/calendar/task/f;->c(Lcom/android/calendar/task/f;)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0, v2}, Lcom/android/calendar/task/f;->a(Lcom/android/calendar/task/f;Ljava/lang/String;)Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->d(Lcom/android/calendar/task/f;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->d(Lcom/android/calendar/task/f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->e(Lcom/android/calendar/task/f;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v1}, Lcom/android/calendar/task/f;->d(Lcom/android/calendar/task/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->e(Lcom/android/calendar/task/f;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v1}, Lcom/android/calendar/task/f;->d(Lcom/android/calendar/task/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->b(Lcom/android/calendar/task/f;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/task/g;->a:Lcom/android/calendar/task/f;

    invoke-static {v0}, Lcom/android/calendar/task/f;->f(Lcom/android/calendar/task/f;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto/16 :goto_0
.end method
