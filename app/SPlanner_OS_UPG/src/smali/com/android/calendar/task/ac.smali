.class public Lcom/android/calendar/task/ac;
.super Landroid/widget/ResourceCursorAdapter;
.source "TaskAdapter.java"


# static fields
.field public static a:I

.field public static b:I


# instance fields
.field public c:Z

.field private d:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Landroid/content/res/Resources;

.field private g:Lcom/android/calendar/d/g;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    sput v0, Lcom/android/calendar/task/ac;->a:I

    .line 63
    sput v0, Lcom/android/calendar/task/ac;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 78
    invoke-direct {p0, p1, p2, v3, v2}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;Z)V

    .line 80
    iput-object p1, p0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/ac;->f:Landroid/content/res/Resources;

    .line 82
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/ac;->g:Lcom/android/calendar/d/g;

    .line 83
    iget-object v0, p0, Lcom/android/calendar/task/ac;->f:Landroid/content/res/Resources;

    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/ac;->e:Ljava/lang/String;

    .line 84
    invoke-static {p1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/ac;->h:Ljava/lang/String;

    .line 86
    iput-boolean v2, p0, Lcom/android/calendar/task/ac;->c:Z

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/ac;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 306
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 307
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 308
    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 309
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 310
    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 311
    new-instance v1, Lcom/android/calendar/task/ag;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/task/ag;-><init>(Lcom/android/calendar/task/ac;Lcom/android/calendar/task/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 312
    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 313
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/ac;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/android/calendar/task/ac;->a(Landroid/view/View;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 317
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 16

    .prologue
    .line 91
    const/4 v2, 0x0

    .line 95
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 96
    instance-of v3, v3, Lcom/android/calendar/task/ah;

    if-eqz v3, :cond_0

    .line 97
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/task/ah;

    .line 100
    :cond_0
    if-nez v2, :cond_12

    .line 101
    new-instance v3, Lcom/android/calendar/task/ah;

    invoke-direct {v3}, Lcom/android/calendar/task/ah;-><init>()V

    .line 102
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 103
    const v2, 0x7f12002d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    .line 104
    const v2, 0x7f120155

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/task/ah;->b:Landroid/widget/TextView;

    .line 105
    const v2, 0x7f1201df

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/task/ah;->c:Landroid/widget/TextView;

    .line 106
    const v2, 0x7f1202e8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v3, Lcom/android/calendar/task/ah;->f:Landroid/widget/CheckBox;

    .line 107
    iget-object v2, v3, Lcom/android/calendar/task/ah;->f:Landroid/widget/CheckBox;

    if-eqz v2, :cond_1

    .line 111
    :cond_1
    const v2, 0x7f120043

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    .line 112
    const v2, 0x7f1202eb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/task/ah;->g:Landroid/widget/ImageView;

    .line 113
    const v2, 0x7f1202b7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/task/ah;->h:Landroid/widget/ImageView;

    move-object v9, v3

    .line 116
    :goto_0
    iget-object v2, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, v9, Lcom/android/calendar/task/ah;->b:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, v9, Lcom/android/calendar/task/ah;->c:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, v9, Lcom/android/calendar/task/ah;->f:Landroid/widget/CheckBox;

    if-eqz v2, :cond_2

    iget-object v2, v9, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, v9, Lcom/android/calendar/task/ah;->g:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    iget-object v2, v9, Lcom/android/calendar/task/ah;->h:Landroid/widget/ImageView;

    if-nez v2, :cond_3

    .line 303
    :cond_2
    :goto_1
    return-void

    .line 120
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/android/calendar/task/ad;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/android/calendar/task/ad;-><init>(Lcom/android/calendar/task/ac;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 131
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/task/ac;->c:Z

    if-eqz v2, :cond_4

    .line 132
    iget-object v3, v9, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    .line 133
    if-eqz v3, :cond_4

    .line 134
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 135
    sget v4, Lcom/android/calendar/task/ac;->b:I

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 136
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    const-string v3, "preferences_list_by"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v10

    .line 142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/hj;->t(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 143
    iget-object v2, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c01de

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 147
    :cond_5
    iget-object v11, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    .line 148
    iget-object v12, v9, Lcom/android/calendar/task/ah;->b:Landroid/widget/TextView;

    .line 149
    iget-object v13, v9, Lcom/android/calendar/task/ah;->c:Landroid/widget/TextView;

    .line 151
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v9, Lcom/android/calendar/task/ah;->d:J

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    const/4 v3, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v2

    .line 155
    iget-object v3, v9, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 158
    iget-object v2, v9, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 159
    iget-object v2, v9, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 160
    iget-object v2, v9, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 161
    iget-object v2, v9, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 162
    iget-object v2, v9, Lcom/android/calendar/task/ah;->e:Landroid/widget/TextView;

    new-instance v3, Lcom/android/calendar/task/af;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v3, v0, v1}, Lcom/android/calendar/task/af;-><init>(Lcom/android/calendar/task/ac;Landroid/database/Cursor;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 166
    :cond_6
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 167
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_8

    .line 168
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ac;->e:Ljava/lang/String;

    .line 170
    :cond_8
    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    :goto_2
    const/4 v2, 0x2

    if-ne v10, v2, :cond_e

    .line 196
    iget-object v2, v9, Lcom/android/calendar/task/ah;->b:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    :goto_3
    const/4 v2, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 204
    packed-switch v2, :pswitch_data_0

    .line 221
    :goto_4
    const/4 v2, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 223
    packed-switch v2, :pswitch_data_1

    .line 234
    iget-object v2, v9, Lcom/android/calendar/task/ah;->g:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 239
    :goto_5
    const/16 v2, 0xd

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 240
    if-eqz v2, :cond_9

    const-string v3, "Default"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 244
    :cond_9
    const/4 v3, 0x5

    if-eq v10, v3, :cond_f

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/task/ac;->g:Lcom/android/calendar/d/g;

    invoke-virtual {v3}, Lcom/android/calendar/d/g;->i()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 247
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 248
    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    :goto_6
    iget-object v2, v9, Lcom/android/calendar/task/ah;->f:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 260
    const/4 v2, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_10

    const/4 v2, 0x1

    .line 261
    :goto_7
    iget-object v3, v9, Lcom/android/calendar/task/ah;->f:Landroid/widget/CheckBox;

    invoke-static {v3}, Lcom/android/calendar/dz;->a(Landroid/widget/CompoundButton;)Z

    move-result v3

    .line 264
    if-eqz v2, :cond_11

    .line 266
    iget-object v2, v9, Lcom/android/calendar/task/ah;->f:Landroid/widget/CheckBox;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 267
    iget-object v2, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ac;->f:Landroid/content/res/Resources;

    const v5, 0x7f0b001b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 268
    iget-object v2, v9, Lcom/android/calendar/task/ah;->b:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ac;->f:Landroid/content/res/Resources;

    const v5, 0x7f0b000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 269
    if-nez v3, :cond_a

    .line 270
    iget-object v2, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    iget-object v3, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    or-int/lit8 v3, v3, 0x10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 280
    :cond_a
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 281
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/calendar/task/ac;->b(Landroid/view/View;)V

    .line 282
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/calendar/task/ac;->b(Landroid/view/View;)V

    .line 283
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/calendar/task/ac;->b(Landroid/view/View;)V

    .line 284
    invoke-direct/range {p0 .. p1}, Lcom/android/calendar/task/ac;->a(Landroid/view/View;)V

    .line 291
    new-instance v2, Lcom/android/calendar/task/ae;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v0, v1}, Lcom/android/calendar/task/ae;-><init>(Lcom/android/calendar/task/ac;Landroid/view/View;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/16 :goto_1

    .line 177
    :cond_b
    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 178
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ac;->h:Ljava/lang/String;

    invoke-static {v4, v2, v3}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;J)J

    move-result-wide v14

    .line 180
    sub-long v4, v2, v14

    const-wide v6, -0x1f3a565e880L

    cmp-long v4, v4, v6

    if-gez v4, :cond_d

    .line 181
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 182
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x76e

    invoke-virtual/range {v2 .. v8}, Landroid/text/format/Time;->set(IIIIII)V

    .line 183
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 190
    :cond_c
    :goto_9
    sub-long/2addr v2, v14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ac;->d:Landroid/content/Context;

    const/4 v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 184
    :cond_d
    sub-long v4, v2, v14

    const-wide v6, 0x1ec4d45f520L

    cmp-long v4, v4, v6

    if-lez v4, :cond_c

    .line 185
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 186
    const/4 v3, 0x0

    const/16 v4, 0x3b

    const/16 v5, 0x17

    const/16 v6, 0x1f

    const/16 v7, 0xb

    const/16 v8, 0x7f4

    invoke-virtual/range {v2 .. v8}, Landroid/text/format/Time;->set(IIIIII)V

    .line 187
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    goto :goto_9

    .line 198
    :cond_e
    iget-object v2, v9, Lcom/android/calendar/task/ah;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 206
    :pswitch_0
    iget-object v2, v9, Lcom/android/calendar/task/ah;->h:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 207
    iget-object v2, v9, Lcom/android/calendar/task/ah;->h:Landroid/widget/ImageView;

    const v3, 0x7f02013c

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 210
    :pswitch_1
    iget-object v2, v9, Lcom/android/calendar/task/ah;->h:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 213
    :pswitch_2
    iget-object v2, v9, Lcom/android/calendar/task/ah;->h:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 214
    iget-object v2, v9, Lcom/android/calendar/task/ah;->h:Landroid/widget/ImageView;

    const v3, 0x7f02013b

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 225
    :pswitch_3
    iget-object v2, v9, Lcom/android/calendar/task/ah;->g:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 230
    :pswitch_4
    iget-object v2, v9, Lcom/android/calendar/task/ah;->g:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 231
    iget-object v2, v9, Lcom/android/calendar/task/ah;->g:Landroid/widget/ImageView;

    const v3, 0x7f0200b6

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_5

    .line 250
    :cond_f
    const/16 v2, 0x8

    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 260
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 273
    :cond_11
    iget-object v2, v9, Lcom/android/calendar/task/ah;->f:Landroid/widget/CheckBox;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 274
    iget-object v2, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ac;->f:Landroid/content/res/Resources;

    const v5, 0x7f0b0138

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 275
    iget-object v2, v9, Lcom/android/calendar/task/ah;->b:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ac;->f:Landroid/content/res/Resources;

    const v5, 0x7f0b0136

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 276
    if-nez v3, :cond_a

    .line 277
    iget-object v2, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    iget-object v3, v9, Lcom/android/calendar/task/ah;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    and-int/lit8 v3, v3, -0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto/16 :goto_8

    :cond_12
    move-object v9, v2

    goto/16 :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 223
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
