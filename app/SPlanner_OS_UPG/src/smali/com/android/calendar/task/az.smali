.class public Lcom/android/calendar/task/az;
.super Landroid/app/Fragment;
.source "TaskDeleteFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/android/calendar/ap;
.implements Lcom/android/calendar/task/aw;


# instance fields
.field private a:Lcom/android/calendar/task/TaskDeleteView;

.field private b:Landroid/app/Activity;

.field private c:Landroid/text/format/Time;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:Z

.field private g:Lcom/android/calendar/al;

.field private h:Lcom/android/calendar/detail/EventInfoFragment;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Lcom/android/calendar/aq;

.field private l:Landroid/content/ContentResolver;

.field private m:Ljava/util/ArrayList;

.field private n:Lcom/android/calendar/task/bf;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/CheckBox;

.field private r:Landroid/widget/CheckBox;

.field private s:Landroid/view/accessibility/AccessibilityManager;

.field private t:I

.field private u:Landroid/view/View;

.field private v:I

.field private w:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 130
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/task/az;-><init>(J)V

    .line 131
    return-void
.end method

.method public constructor <init>(J)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 104
    iput-object v1, p0, Lcom/android/calendar/task/az;->k:Lcom/android/calendar/aq;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    .line 107
    iput-object v1, p0, Lcom/android/calendar/task/az;->n:Lcom/android/calendar/task/bf;

    .line 112
    iput-object v1, p0, Lcom/android/calendar/task/az;->s:Landroid/view/accessibility/AccessibilityManager;

    .line 113
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/task/az;->t:I

    .line 115
    iput-object v1, p0, Lcom/android/calendar/task/az;->u:Landroid/view/View;

    .line 119
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/task/az;->v:I

    .line 121
    new-instance v0, Lcom/android/calendar/task/ba;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/ba;-><init>(Lcom/android/calendar/task/az;)V

    iput-object v0, p0, Lcom/android/calendar/task/az;->w:Ljava/lang/Runnable;

    .line 134
    iput-wide p1, p0, Lcom/android/calendar/task/az;->e:J

    .line 135
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/az;->c:Landroid/text/format/Time;

    .line 136
    iget-wide v0, p0, Lcom/android/calendar/task/az;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/android/calendar/task/az;->c:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/az;->c:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/android/calendar/task/az;->e:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/task/az;Lcom/android/calendar/task/bf;)Lcom/android/calendar/task/bf;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/android/calendar/task/az;->n:Lcom/android/calendar/task/bf;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/az;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/task/az;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/android/calendar/task/az;->d:Ljava/lang/String;

    return-object p1
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 461
    iget-object v0, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 462
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 463
    if-eqz v1, :cond_0

    .line 464
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 466
    :cond_0
    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 469
    sparse-switch p1, :sswitch_data_0

    .line 489
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 473
    :sswitch_1
    new-instance v0, Lcom/android/calendar/task/bf;

    invoke-virtual {p0}, Lcom/android/calendar/task/az;->e()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/task/bf;-><init>(Lcom/android/calendar/task/az;I)V

    iput-object v0, p0, Lcom/android/calendar/task/az;->n:Lcom/android/calendar/task/bf;

    .line 474
    iget-object v0, p0, Lcom/android/calendar/task/az;->n:Lcom/android/calendar/task/bf;

    if-eqz v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Long;

    .line 476
    iget-object v1, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 477
    iget-object v1, p0, Lcom/android/calendar/task/az;->n:Lcom/android/calendar/task/bf;

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/bf;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 481
    :sswitch_2
    new-instance v1, Lcom/android/calendar/task/bh;

    invoke-direct {v1, p0, v3}, Lcom/android/calendar/task/bh;-><init>(Lcom/android/calendar/task/az;Lcom/android/calendar/task/ba;)V

    .line 482
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/DialogFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_1

    .line 483
    const-string v2, "dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 469
    :sswitch_data_0
    .sparse-switch
        0x3ed -> :sswitch_1
        0x3f2 -> :sswitch_0
        0x3f7 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic a(Lcom/android/calendar/task/az;I)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/android/calendar/task/az;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/az;Z)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/android/calendar/task/az;->d(Z)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/task/az;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->c:Landroid/text/format/Time;

    return-object v0
.end method

.method private b(Lcom/android/calendar/aq;)V
    .locals 13

    .prologue
    const v12, 0x7f120030

    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 537
    iget-wide v0, p1, Lcom/android/calendar/aq;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 564
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/task/az;->f:Z

    if-eqz v0, :cond_0

    .line 543
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 544
    if-nez v0, :cond_2

    .line 547
    iput-object p1, p0, Lcom/android/calendar/task/az;->k:Lcom/android/calendar/aq;

    goto :goto_0

    .line 551
    :cond_2
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v11

    .line 553
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 554
    :cond_3
    iget-wide v0, p1, Lcom/android/calendar/aq;->p:J

    long-to-int v8, v0

    .line 557
    :goto_1
    new-instance v0, Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    iget-wide v2, p1, Lcom/android/calendar/aq;->c:J

    iget-object v4, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v4, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-object v6, p1, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v6, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-direct/range {v0 .. v10}, Lcom/android/calendar/detail/EventInfoFragment;-><init>(Landroid/content/Context;JJJIZI)V

    iput-object v0, p0, Lcom/android/calendar/task/az;->h:Lcom/android/calendar/detail/EventInfoFragment;

    .line 560
    iget-object v0, p0, Lcom/android/calendar/task/az;->h:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v11, v12, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 561
    iget-object v0, p0, Lcom/android/calendar/task/az;->g:Lcom/android/calendar/al;

    iget-object v1, p0, Lcom/android/calendar/task/az;->h:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0, v12, v1}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 562
    invoke-virtual {v11}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    :cond_4
    move v8, v9

    goto :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/task/az;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->s:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private d(Z)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 709
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/au;

    .line 712
    if-eqz p1, :cond_2

    .line 713
    invoke-virtual {v0}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v5

    .line 714
    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getCount()I

    move-result v6

    .line 715
    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getHeaderViewsCount()I

    move-result v7

    .line 716
    const/4 v2, 0x1

    move v4, v3

    .line 718
    :goto_0
    sub-int v1, v6, v7

    if-ge v4, v1, :cond_1

    .line 719
    invoke-virtual {v0, v4}, Lcom/android/calendar/task/au;->getItemViewType(I)I

    move-result v1

    if-nez v1, :cond_3

    .line 720
    invoke-virtual {v0, v4}, Lcom/android/calendar/task/au;->d(I)J

    move-result-wide v8

    .line 721
    const-wide/16 v10, -0x1

    cmp-long v1, v8, v10

    if-eqz v1, :cond_3

    .line 722
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 723
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    move v1, v3

    .line 718
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_0

    .line 729
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 734
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/calendar/task/au;->a:Z

    .line 735
    return-void

    .line 731
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method static synthetic e(Lcom/android/calendar/task/az;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/calendar/task/az;->k()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/task/az;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/task/az;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->r:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/task/az;)Lcom/android/calendar/task/TaskDeleteView;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/task/az;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/calendar/task/az;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/task/az;)I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/android/calendar/task/az;->v:I

    return v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 528
    iget-object v0, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    const-string v1, "preferences_list_by"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/task/az;->t:I

    .line 530
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    iget v1, p0, Lcom/android/calendar/task/az;->t:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/TaskDeleteView;->setListBy(I)V

    .line 531
    return-void
.end method

.method static synthetic k(Lcom/android/calendar/task/az;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->g:Lcom/android/calendar/al;

    return-object v0
.end method

.method private k()V
    .locals 14

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 738
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/au;

    .line 741
    invoke-virtual {v0}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v6

    .line 742
    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getCount()I

    move-result v7

    .line 743
    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getHeaderViewsCount()I

    move-result v8

    move v4, v3

    move v2, v5

    .line 746
    :goto_0
    sub-int v1, v7, v8

    if-ge v4, v1, :cond_1

    .line 747
    invoke-virtual {v0, v4}, Lcom/android/calendar/task/au;->getItemViewType(I)I

    move-result v1

    if-nez v1, :cond_2

    .line 748
    invoke-virtual {v0, v4}, Lcom/android/calendar/task/au;->d(I)J

    move-result-wide v10

    .line 750
    invoke-virtual {v0, v4}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v1

    .line 751
    if-eqz v1, :cond_2

    .line 752
    iget-object v9, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v9, v4}, Lcom/android/calendar/task/ai;->g(I)I

    move-result v9

    .line 753
    iget-object v12, v1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v12, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 754
    iget-object v9, v1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    const-string v12, "complete"

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 755
    iget-object v1, v1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 757
    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-eqz v9, :cond_2

    if-ne v1, v5, :cond_2

    .line 758
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 759
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move v1, v3

    .line 746
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_0

    .line 766
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/az;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 767
    return-void

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private l()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 770
    const/4 v6, 0x0

    .line 772
    sget-object v1, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    .line 773
    const-string v3, "complete=1 AND selected=1 AND groupSelected=1 AND deleted = 0"

    .line 776
    iget-object v0, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 778
    if-eqz v1, :cond_0

    .line 779
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 780
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 783
    :goto_0
    return v0

    :cond_0
    move v0, v6

    goto :goto_0
.end method

.method static synthetic l(Lcom/android/calendar/task/az;)Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/task/az;->l:Landroid/content/ContentResolver;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 877
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->d()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 878
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0158

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 882
    :goto_0
    return-object v0

    .line 879
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 880
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f014f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 882
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0151

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/calendar/task/az;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 332
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getTaskDeleteAdapter()Lcom/android/calendar/task/au;

    move-result-object v5

    .line 333
    invoke-virtual {v5}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    move v2, v3

    .line 339
    :goto_0
    invoke-virtual {v5}, Lcom/android/calendar/task/au;->getCount()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 340
    invoke-virtual {v5, v2}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v6

    .line 341
    if-nez v6, :cond_1

    .line 339
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 344
    :cond_1
    iget-object v1, v6, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget-object v1, v1, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v3

    .line 347
    :goto_1
    if-ge v4, v7, :cond_0

    .line 348
    iget-object v1, v6, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget-object v1, v1, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/an;

    iget-wide v8, v1, Lcom/android/calendar/task/an;->g:J

    .line 349
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 350
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 357
    :cond_3
    invoke-virtual {v5}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v1

    .line 358
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 359
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 361
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 362
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 522
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 523
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->b()V

    .line 525
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/android/calendar/task/az;->o:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 303
    iget-object v0, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 304
    invoke-direct {p0}, Lcom/android/calendar/task/az;->l()I

    move-result v0

    if-lez v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/android/calendar/task/az;->p:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 306
    iget-object v0, p0, Lcom/android/calendar/task/az;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 310
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getSelectedTaskId()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 311
    invoke-direct {p0, p1}, Lcom/android/calendar/task/az;->d(Z)V

    .line 312
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getSelectedTaskId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/task/TaskDeleteView;->setSelectedTaskId(J)V

    .line 314
    :cond_0
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    iget-object v1, p0, Lcom/android/calendar/task/az;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/TaskDeleteView;->removeHeaderView(Landroid/view/View;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/TaskDeleteView;->a(Z)V

    .line 513
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 12

    .prologue
    .line 642
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getCount()I

    move-result v3

    .line 643
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getHeaderViewsCount()I

    move-result v4

    .line 644
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getFirstVisiblePosition()I

    move-result v5

    .line 645
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/au;

    .line 647
    invoke-virtual {v0}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v6

    .line 649
    invoke-virtual {v0, p1}, Lcom/android/calendar/task/au;->a(Z)V

    .line 651
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    sub-int v1, v3, v4

    if-ge v2, v1, :cond_2

    .line 652
    invoke-virtual {v0, v2}, Lcom/android/calendar/task/au;->getItemViewType(I)I

    move-result v1

    if-nez v1, :cond_1

    .line 653
    invoke-virtual {v0, v2}, Lcom/android/calendar/task/au;->d(I)J

    move-result-wide v8

    .line 654
    const-wide/16 v10, -0x1

    cmp-long v1, v8, v10

    if-eqz v1, :cond_1

    .line 655
    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    add-int v7, v2, v4

    sub-int/2addr v7, v5

    invoke-virtual {v1, v7}, Lcom/android/calendar/task/TaskDeleteView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 656
    if-eqz v1, :cond_0

    .line 657
    const v7, 0x7f1201e3

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 658
    if-eqz v1, :cond_0

    .line 659
    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 663
    :cond_0
    if-eqz p1, :cond_1

    .line 664
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 651
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 668
    :cond_2
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 606
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/au;

    .line 608
    invoke-virtual {v0}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v2

    .line 610
    iget-object v0, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 612
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 613
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 615
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 616
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 617
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 618
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 619
    iget-object v1, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 624
    const/16 v0, 0x3f7

    invoke-direct {p0, v0}, Lcom/android/calendar/task/az;->a(I)V

    .line 626
    :cond_2
    return-void
.end method

.method public c(Z)V
    .locals 12

    .prologue
    .line 672
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getCount()I

    move-result v3

    .line 673
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getHeaderViewsCount()I

    move-result v4

    .line 674
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getFirstVisiblePosition()I

    move-result v5

    .line 675
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/au;

    .line 677
    invoke-virtual {v0}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v6

    .line 679
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    sub-int v1, v3, v4

    if-ge v2, v1, :cond_3

    .line 680
    invoke-virtual {v0, v2}, Lcom/android/calendar/task/au;->getItemViewType(I)I

    move-result v1

    if-nez v1, :cond_1

    .line 681
    invoke-virtual {v0, v2}, Lcom/android/calendar/task/au;->d(I)J

    move-result-wide v8

    .line 683
    invoke-virtual {v0, v2}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v1

    .line 684
    if-eqz v1, :cond_1

    .line 685
    iget-object v7, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v7, v2}, Lcom/android/calendar/task/ai;->g(I)I

    move-result v7

    .line 686
    iget-object v10, v1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v10, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 687
    iget-object v7, v1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    const-string v10, "complete"

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 688
    iget-object v1, v1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 690
    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_1

    const/4 v7, 0x1

    if-ne v1, v7, :cond_1

    .line 691
    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    add-int v7, v2, v4

    sub-int/2addr v7, v5

    invoke-virtual {v1, v7}, Lcom/android/calendar/task/TaskDeleteView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 692
    if-eqz v1, :cond_0

    .line 693
    const v7, 0x7f1201e3

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 694
    if-eqz v1, :cond_0

    .line 695
    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 698
    :cond_0
    if-eqz p1, :cond_2

    .line 699
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 679
    :cond_1
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 701
    :cond_2
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 706
    :cond_3
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 10

    .prologue
    .line 787
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/au;

    .line 789
    invoke-virtual {v0}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v3

    .line 790
    const/4 v1, 0x0

    .line 792
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 793
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 795
    iget-boolean v0, v0, Lcom/android/calendar/task/au;->a:Z

    if-eqz v0, :cond_2

    .line 796
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    .line 806
    :cond_0
    return v2

    .line 799
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 800
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 801
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 802
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 803
    add-int/lit8 v2, v2, 0x1

    move v0, v2

    :goto_1
    move v2, v0

    .line 805
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v2, v1

    goto :goto_0
.end method

.method public f()Lcom/android/calendar/task/au;
    .locals 1

    .prologue
    .line 887
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    .line 888
    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/au;

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 517
    const-wide/16 v0, 0x80

    return-wide v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public i()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 904
    iget-object v0, p0, Lcom/android/calendar/task/az;->u:Landroid/view/View;

    if-nez v0, :cond_1

    .line 914
    :cond_0
    :goto_0
    return-void

    .line 907
    :cond_1
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 909
    if-eqz v0, :cond_0

    .line 910
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 911
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 912
    iget-object v1, p0, Lcom/android/calendar/task/az;->u:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const-wide/16 v6, -0x1

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 275
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 276
    const/4 v2, 0x0

    .line 277
    if-eqz p1, :cond_3

    .line 278
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/au;

    .line 280
    const-string v3, "key_restore_instance_id"

    invoke-virtual {p1, v3, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 281
    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 282
    iget-object v3, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v3, v4, v5}, Lcom/android/calendar/task/TaskDeleteView;->setSelectedTaskId(J)V

    .line 284
    :cond_0
    const-string v3, "select_header_check_state"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v3

    .line 285
    if-eqz v3, :cond_1

    .line 286
    iget-object v4, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    aget-boolean v5, v3, v1

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 287
    iget-object v4, p0, Lcom/android/calendar/task/az;->r:Landroid/widget/CheckBox;

    aget-boolean v3, v3, v8

    invoke-virtual {v4, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 289
    :cond_1
    const-string v3, "event_ids_to_delete"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    .line 290
    if-eqz v3, :cond_3

    array-length v4, v3

    if-lez v4, :cond_3

    .line 291
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_2

    .line 292
    invoke-virtual {v0}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v2

    .line 293
    iget-object v4, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    aget-wide v6, v3, v1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    aget-wide v4, v3, v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 296
    :cond_2
    invoke-virtual {v0, v2}, Lcom/android/calendar/task/au;->a(Ljava/util/HashMap;)V

    .line 299
    :cond_3
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 150
    iget-object v0, p0, Lcom/android/calendar/task/az;->w:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/az;->d:Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lcom/android/calendar/task/az;->c:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/task/az;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 152
    iput-object p1, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    .line 153
    iget-object v0, p0, Lcom/android/calendar/task/az;->k:Lcom/android/calendar/aq;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/android/calendar/task/az;->k:Lcom/android/calendar/aq;

    invoke-direct {p0, v0}, Lcom/android/calendar/task/az;->b(Lcom/android/calendar/aq;)V

    .line 155
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/task/az;->k:Lcom/android/calendar/aq;

    .line 157
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 896
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 898
    iget-boolean v0, p0, Lcom/android/calendar/task/az;->j:Z

    if-eqz v0, :cond_0

    .line 899
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->i()V

    .line 901
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 161
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 162
    iget-object v0, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/az;->g:Lcom/android/calendar/al;

    .line 163
    iget-object v0, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    const v1, 0x7f0a0007

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/az;->f:Z

    .line 165
    iget-object v0, p0, Lcom/android/calendar/task/az;->b:Landroid/app/Activity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/az;->j:Z

    .line 166
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/task/az;->s:Landroid/view/accessibility/AccessibilityManager;

    .line 168
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/az;->l:Landroid/content/ContentResolver;

    .line 169
    if-eqz p1, :cond_0

    .line 170
    const-string v0, "key_restore_time"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 171
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 172
    iget-object v2, p0, Lcom/android/calendar/task/az;->c:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 175
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const v5, 0x7f1201e1

    const v4, 0x7f04005c

    const/4 v3, 0x0

    .line 180
    const v0, 0x7f0400ab

    invoke-virtual {p1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 182
    const v0, 0x7f1202cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/TaskDeleteView;

    iput-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    .line 183
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/calendar/task/TaskDeleteView;->setClickable(Z)V

    .line 184
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/calendar/task/TaskDeleteView;->setChoiceMode(I)V

    .line 186
    invoke-virtual {p1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/az;->o:Landroid/view/View;

    .line 187
    iget-object v0, p0, Lcom/android/calendar/task/az;->o:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    .line 189
    iget-object v0, p0, Lcom/android/calendar/task/az;->o:Landroid/view/View;

    new-instance v2, Lcom/android/calendar/task/bb;

    invoke-direct {v2, p0}, Lcom/android/calendar/task/bb;-><init>(Lcom/android/calendar/task/az;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    invoke-virtual {p1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/az;->p:Landroid/view/View;

    .line 205
    iget-object v0, p0, Lcom/android/calendar/task/az;->p:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/task/az;->r:Landroid/widget/CheckBox;

    .line 206
    iget-object v0, p0, Lcom/android/calendar/task/az;->p:Landroid/view/View;

    const v2, 0x7f1201e0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 207
    const v2, 0x7f0f016a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 209
    iget-object v0, p0, Lcom/android/calendar/task/az;->p:Landroid/view/View;

    new-instance v2, Lcom/android/calendar/task/bc;

    invoke-direct {v2, p0}, Lcom/android/calendar/task/bc;-><init>(Lcom/android/calendar/task/az;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    iget-object v2, p0, Lcom/android/calendar/task/az;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/android/calendar/task/TaskDeleteView;->addHeaderView(Landroid/view/View;)V

    .line 221
    invoke-direct {p0}, Lcom/android/calendar/task/az;->l()I

    move-result v0

    if-lez v0, :cond_1

    .line 222
    iget-boolean v0, p0, Lcom/android/calendar/task/az;->j:Z

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/android/calendar/task/az;->o:Landroid/view/View;

    const v2, 0x7f1201e2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    iget-object v2, p0, Lcom/android/calendar/task/az;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/android/calendar/task/TaskDeleteView;->addHeaderView(Landroid/view/View;)V

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    iget-object v2, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v2}, Lcom/android/calendar/task/TaskDeleteView;->getTaskDeleteAdapter()Lcom/android/calendar/task/au;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/task/TaskDeleteView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 228
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    new-instance v2, Lcom/android/calendar/task/bd;

    invoke-direct {v2, p0}, Lcom/android/calendar/task/bd;-><init>(Lcom/android/calendar/task/az;)V

    invoke-virtual {v0, v2}, Lcom/android/calendar/task/TaskDeleteView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 262
    iget-boolean v0, p0, Lcom/android/calendar/task/az;->f:Z

    if-nez v0, :cond_2

    .line 263
    const v0, 0x7f120030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 266
    :cond_2
    iput-object v1, p0, Lcom/android/calendar/task/az;->u:Landroid/view/View;

    .line 267
    iget-boolean v0, p0, Lcom/android/calendar/task/az;->j:Z

    if-eqz v0, :cond_3

    .line 268
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->i()V

    .line 270
    :cond_3
    return-object v1
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 318
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 319
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    invoke-virtual {p0}, Lcom/android/calendar/task/az;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 322
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/task/az;->j()V

    .line 323
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    iget-object v1, p0, Lcom/android/calendar/task/az;->c:Landroid/text/format/Time;

    const-wide/16 v2, -0x1

    iget-object v4, p0, Lcom/android/calendar/task/az;->i:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/task/TaskDeleteView;->a(Landroid/text/format/Time;JLjava/lang/String;Z)V

    .line 324
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->a()V

    .line 325
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteView;->getTaskDeleteAdapter()Lcom/android/calendar/task/au;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/aw;)V

    .line 326
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 366
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 367
    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    if-nez v1, :cond_1

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/task/az;->g:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    .line 371
    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 372
    iget-object v1, p0, Lcom/android/calendar/task/az;->c:Landroid/text/format/Time;

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 373
    const-string v1, "key_restore_time"

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 376
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getSelectedTaskId()J

    move-result-wide v2

    .line 377
    cmp-long v1, v2, v4

    if-ltz v1, :cond_3

    .line 378
    const-string v1, "key_restore_instance_id"

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 380
    :cond_3
    const/4 v1, 0x2

    new-array v1, v1, [Z

    .line 381
    iget-object v2, p0, Lcom/android/calendar/task/az;->q:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 382
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/task/az;->r:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    aput-boolean v3, v1, v2

    .line 383
    const-string v2, "select_header_check_state"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 384
    iget-object v1, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 385
    iget-object v1, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [J

    .line 386
    iget-object v1, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    .line 387
    :goto_1
    if-ge v1, v3, :cond_4

    .line 388
    iget-object v0, p0, Lcom/android/calendar/task/az;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 387
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 390
    :cond_4
    const-string v0, "event_ids_to_delete"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 575
    iget-object v0, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    iget-object v1, p0, Lcom/android/calendar/task/az;->a:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v1}, Lcom/android/calendar/task/TaskDeleteView;->getHeaderViewsCount()I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/TaskDeleteView;->a(I)I

    move-result v0

    .line 578
    if-nez v0, :cond_1

    .line 602
    :cond_0
    :goto_0
    return-void

    .line 582
    :cond_1
    iget v1, p0, Lcom/android/calendar/task/az;->v:I

    if-eq v1, v0, :cond_0

    .line 583
    iput v0, p0, Lcom/android/calendar/task/az;->v:I

    .line 584
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/task/az;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 585
    iget v1, p0, Lcom/android/calendar/task/az;->v:I

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 586
    iget-object v1, p0, Lcom/android/calendar/task/az;->g:Lcom/android/calendar/al;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/android/calendar/al;->a(J)V

    .line 590
    iget-boolean v0, p0, Lcom/android/calendar/task/az;->j:Z

    if-nez v0, :cond_0

    .line 591
    new-instance v0, Lcom/android/calendar/task/be;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/be;-><init>(Lcom/android/calendar/task/az;)V

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 567
    return-void
.end method
