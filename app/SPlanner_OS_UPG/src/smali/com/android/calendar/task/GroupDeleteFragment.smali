.class public Lcom/android/calendar/task/GroupDeleteFragment;
.super Landroid/app/Fragment;
.source "GroupDeleteFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/android/calendar/ap;


# static fields
.field public static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static c:I

.field private static d:I


# instance fields
.field private e:Landroid/view/View;

.field private f:Landroid/widget/ListView;

.field private g:Lcom/android/calendar/task/i;

.field private h:Landroid/app/Activity;

.field private i:Lcom/android/calendar/ag;

.field private j:Landroid/database/Cursor;

.field private k:Landroid/widget/CheckBox;

.field private l:Landroid/view/accessibility/AccessibilityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/GroupDeleteFragment;->a:Landroid/net/Uri;

    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_accountId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "groupName"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/task/GroupDeleteFragment;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 69
    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->e:Landroid/view/View;

    .line 76
    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->l:Landroid/view/accessibility/AccessibilityManager;

    .line 80
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/GroupDeleteFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->j:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/GroupDeleteFragment;)Lcom/android/calendar/task/i;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    return-object v0
.end method

.method private a(J)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 361
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v0

    sput v0, Lcom/android/calendar/task/GroupDeleteFragment;->c:I

    .line 362
    sget-object v0, Lcom/android/calendar/task/GroupDeleteFragment;->a:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 363
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    sget v1, Lcom/android/calendar/task/GroupDeleteFragment;->c:I

    const-wide/16 v6, 0x0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 365
    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v2

    .line 253
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    .line 255
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v0}, Lcom/android/calendar/task/i;->a()Ljava/util/HashMap;

    move-result-object v3

    .line 256
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/i;->a(Z)V

    .line 257
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    move v0, v1

    .line 259
    :goto_0
    if-ge v0, v2, :cond_1

    .line 260
    iget-object v4, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v4, v0, p1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 261
    iget-object v4, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    sub-int v5, v0, v1

    invoke-virtual {v4, v5}, Lcom/android/calendar/task/i;->getItemId(I)J

    move-result-wide v4

    .line 262
    if-eqz p1, :cond_0

    .line 263
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 266
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/task/GroupDeleteFragment;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/task/GroupDeleteFragment;->e()V

    return-void
.end method

.method static synthetic c(Lcom/android/calendar/task/GroupDeleteFragment;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/task/GroupDeleteFragment;->f()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 153
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->k:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 154
    invoke-direct {p0, v0}, Lcom/android/calendar/task/GroupDeleteFragment;->a(Z)V

    .line 155
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->l:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->k:Landroid/widget/CheckBox;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 160
    return-void

    .line 152
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 239
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    .line 240
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    .line 242
    :goto_0
    if-ge v0, v1, :cond_0

    .line 243
    iget-object v2, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 242
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 247
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 248
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 249
    return-void
.end method

.method private h()V
    .locals 8

    .prologue
    .line 269
    const/4 v1, 0x1

    .line 271
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v4

    .line 272
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v3

    .line 273
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v0}, Lcom/android/calendar/task/i;->a()Ljava/util/HashMap;

    move-result-object v5

    move v2, v3

    .line 275
    :goto_0
    if-ge v2, v4, :cond_2

    .line 276
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    sub-int v6, v2, v3

    invoke-virtual {v0, v6}, Lcom/android/calendar/task/i;->getItemId(I)J

    move-result-wide v6

    .line 277
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 278
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 279
    :cond_0
    const/4 v0, 0x0

    .line 283
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->k:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 284
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/i;->a(Z)V

    .line 285
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 286
    return-void

    .line 275
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 216
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    if-nez v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    sget v0, Lcom/android/calendar/task/GroupDeleteFragment;->d:I

    invoke-static {v0}, Lcom/android/calendar/ag;->a(I)I

    .line 221
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v0

    sput v0, Lcom/android/calendar/task/GroupDeleteFragment;->d:I

    .line 222
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    sget v1, Lcom/android/calendar/task/GroupDeleteFragment;->d:I

    sget-object v3, Lcom/android/calendar/task/GroupDeleteFragment;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/task/GroupDeleteFragment;->b:[Ljava/lang/String;

    const-string v7, "_accountId"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 233
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteFragment;->a()V

    .line 236
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 10

    .prologue
    .line 289
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v0}, Lcom/android/calendar/task/i;->a()Ljava/util/HashMap;

    move-result-object v3

    .line 290
    const/4 v2, 0x0

    .line 292
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 293
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 295
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 297
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 298
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x1

    .line 303
    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 307
    new-instance v0, Lcom/android/calendar/task/o;

    invoke-direct {v0}, Lcom/android/calendar/task/o;-><init>()V

    .line 308
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->h:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "DeleteConfirm"

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/task/o;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method public d()V
    .locals 8

    .prologue
    .line 341
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v0}, Lcom/android/calendar/task/i;->a()Ljava/util/HashMap;

    move-result-object v2

    .line 343
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 344
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 346
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 348
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 349
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/task/GroupDeleteFragment;->a(J)V

    goto :goto_0

    .line 354
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_2

    .line 356
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 358
    :cond_2
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 228
    const-wide/16 v0, 0x80

    return-wide v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 164
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 165
    new-instance v0, Lcom/android/calendar/task/i;

    iget-object v2, p0, Lcom/android/calendar/task/GroupDeleteFragment;->h:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/android/calendar/task/i;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    .line 166
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 167
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 169
    if-eqz p1, :cond_2

    .line 170
    const-string v0, "check_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;

    .line 172
    if-eqz v0, :cond_0

    .line 173
    iget v2, v0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->a:I

    if-ne v2, v1, :cond_1

    .line 174
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v2, v1}, Lcom/android/calendar/task/i;->a(Z)V

    .line 175
    iget-object v2, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    iget-object v0, v0, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Lcom/android/calendar/task/i;->a(Ljava/util/HashMap;)V

    .line 176
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 185
    :cond_0
    :goto_1
    return-void

    .line 173
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/task/n;

    invoke-direct {v1, p0}, Lcom/android/calendar/task/n;-><init>(Lcom/android/calendar/task/GroupDeleteFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 85
    iput-object p1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->h:Landroid/app/Activity;

    .line 86
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->h:Landroid/app/Activity;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->l:Landroid/view/accessibility/AccessibilityManager;

    .line 88
    new-instance v0, Lcom/android/calendar/task/k;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/task/k;-><init>(Lcom/android/calendar/task/GroupDeleteFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    .line 95
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 109
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 110
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 114
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 115
    const v0, 0x7f04005b

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->e:Landroid/view/View;

    .line 116
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->e:Landroid/view/View;

    const v1, 0x7f1200df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    .line 117
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 119
    const v0, 0x7f04005c

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 120
    const v0, 0x7f1201e1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->k:Landroid/widget/CheckBox;

    .line 122
    new-instance v0, Lcom/android/calendar/task/l;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/l;-><init>(Lcom/android/calendar/task/GroupDeleteFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    new-instance v0, Lcom/android/calendar/task/m;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/m;-><init>(Lcom/android/calendar/task/GroupDeleteFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 146
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 148
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->e:Landroid/view/View;

    return-object v0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 100
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->j:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/i;->a(Landroid/database/Cursor;)V

    .line 102
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->j:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 103
    iput-object v1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->j:Landroid/database/Cursor;

    .line 105
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 188
    if-nez p3, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/android/calendar/task/GroupDeleteFragment;->e()V

    .line 191
    :cond_0
    const v0, 0x7f1201e3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 192
    if-nez v0, :cond_1

    .line 201
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    .line 195
    iget-object v2, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v2}, Lcom/android/calendar/task/i;->a()Ljava/util/HashMap;

    move-result-object v2

    .line 196
    iget-object v3, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    sub-int v1, p3, v1

    invoke-virtual {v3, v1}, Lcom/android/calendar/task/i;->getItemId(I)J

    move-result-wide v4

    .line 197
    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 198
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 200
    invoke-direct {p0}, Lcom/android/calendar/task/GroupDeleteFragment;->h()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 205
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 206
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v0

    sput v0, Lcom/android/calendar/task/GroupDeleteFragment;->d:I

    .line 209
    const-string v5, "_id!=1"

    .line 210
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->i:Lcom/android/calendar/ag;

    sget v1, Lcom/android/calendar/task/GroupDeleteFragment;->d:I

    sget-object v3, Lcom/android/calendar/task/GroupDeleteFragment;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/task/GroupDeleteFragment;->b:[Ljava/lang/String;

    const-string v7, "_accountId"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 407
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 409
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v0}, Lcom/android/calendar/task/i;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 410
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteFragment;->g:Lcom/android/calendar/task/i;

    invoke-virtual {v1}, Lcom/android/calendar/task/i;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 412
    const-string v2, "check_state"

    new-instance v3, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;

    invoke-direct {v3, v0, v1}, Lcom/android/calendar/task/GroupDeleteFragment$SavedCheckState;-><init>(ILjava/util/HashMap;)V

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 413
    return-void

    .line 409
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
