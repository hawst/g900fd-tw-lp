.class Lcom/android/calendar/task/af;
.super Ljava/lang/Object;
.source "TaskAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/task/ac;

.field private b:Landroid/database/Cursor;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/android/calendar/task/ac;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 367
    iput-object p1, p0, Lcom/android/calendar/task/af;->a:Lcom/android/calendar/task/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/task/af;->c:Z

    .line 368
    iput-object p2, p0, Lcom/android/calendar/task/af;->b:Landroid/database/Cursor;

    .line 369
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 424
    const-string v6, ""

    .line 425
    iget-object v0, p0, Lcom/android/calendar/task/af;->b:Landroid/database/Cursor;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 426
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 427
    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 428
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 430
    iget-object v0, p0, Lcom/android/calendar/task/af;->a:Lcom/android/calendar/task/ac;

    invoke-static {v0}, Lcom/android/calendar/task/ac;->a(Lcom/android/calendar/task/ac;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "_id"

    aput-object v7, v2, v5

    const-string v5, "displayName"

    aput-object v5, v2, v8

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 432
    if-nez v1, :cond_0

    .line 441
    :goto_0
    return-object v6

    .line 436
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 439
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v6, v0

    .line 441
    goto :goto_0

    .line 439
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move-object v0, v6

    goto :goto_1
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x32

    const/4 v4, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 373
    iget-object v0, p0, Lcom/android/calendar/task/af;->a:Lcom/android/calendar/task/ac;

    invoke-static {v0}, Lcom/android/calendar/task/ac;->a(Lcom/android/calendar/task/ac;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->c(Landroid/content/Context;)Z

    move-result v0

    .line 374
    iget-object v1, p0, Lcom/android/calendar/task/af;->a:Lcom/android/calendar/task/ac;

    invoke-static {v1}, Lcom/android/calendar/task/ac;->a(Lcom/android/calendar/task/ac;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/dz;->d(Landroid/content/Context;)Z

    move-result v1

    .line 376
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 420
    :cond_0
    :goto_0
    return v5

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/af;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/af;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 384
    iget-object v1, p0, Lcom/android/calendar/task/af;->a:Lcom/android/calendar/task/ac;

    invoke-static {v1}, Lcom/android/calendar/task/ac;->a(Lcom/android/calendar/task/ac;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 386
    const-string v2, "translationX"

    new-array v3, v4, [F

    aput v1, v3, v5

    aput v7, v3, v6

    invoke-static {p1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 387
    const-string v3, "translationX"

    new-array v4, v4, [F

    aput v7, v4, v5

    aput v1, v4, v6

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 389
    iget-object v3, p0, Lcom/android/calendar/task/af;->b:Landroid/database/Cursor;

    invoke-interface {v3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 391
    const v0, 0x7f120043

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 392
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 398
    :pswitch_1
    sget v3, Lcom/android/calendar/task/ac;->a:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/android/calendar/task/ac;->a:I

    .line 399
    sget v3, Lcom/android/calendar/task/ac;->a:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_0

    .line 400
    iget-boolean v3, p0, Lcom/android/calendar/task/af;->c:Z

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 401
    sput v5, Lcom/android/calendar/task/ac;->a:I

    .line 402
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 403
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 404
    iput-boolean v6, p0, Lcom/android/calendar/task/af;->c:Z

    .line 405
    invoke-direct {p0}, Lcom/android/calendar/task/af;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 394
    :pswitch_2
    sget v0, Lcom/android/calendar/task/ac;->a:I

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 395
    sput v5, Lcom/android/calendar/task/ac;->a:I

    goto/16 :goto_0

    .line 410
    :pswitch_3
    sput v5, Lcom/android/calendar/task/ac;->a:I

    .line 411
    iget-boolean v3, p0, Lcom/android/calendar/task/af;->c:Z

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    .line 413
    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 414
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 415
    iput-boolean v5, p0, Lcom/android/calendar/task/af;->c:Z

    .line 416
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 392
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
