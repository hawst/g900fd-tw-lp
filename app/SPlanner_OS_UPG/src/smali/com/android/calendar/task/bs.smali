.class Lcom/android/calendar/task/bs;
.super Ljava/lang/Object;
.source "TaskWindowAdapter.java"


# instance fields
.field b:Landroid/database/Cursor;

.field c:Lcom/android/calendar/task/ai;

.field d:I

.field e:I

.field f:I

.field g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    new-instance v0, Lcom/android/calendar/task/ai;

    invoke-direct {v0, p1}, Lcom/android/calendar/task/ai;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    .line 241
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 247
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    iget v2, p0, Lcom/android/calendar/task/bs;->d:I

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 250
    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 251
    const-string v2, "Start:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    iget v2, p0, Lcom/android/calendar/task/bs;->e:I

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 253
    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 254
    const-string v2, " End:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const-string v0, " Offset:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/calendar/task/bs;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 256
    const-string v0, " Size:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/calendar/task/bs;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 257
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
