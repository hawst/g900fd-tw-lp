.class public Lcom/android/calendar/task/au;
.super Landroid/widget/BaseAdapter;
.source "TaskDeleteAdapter.java"

# interfaces
.implements Lcom/android/calendar/hb;


# static fields
.field private static final c:Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field public a:Z

.field protected b:Lcom/android/calendar/task/aw;

.field private e:Landroid/content/Context;

.field private f:Lcom/android/calendar/task/ax;

.field private g:Lcom/android/calendar/task/TaskDeleteView;

.field private h:Landroid/view/View;

.field private i:I

.field private j:I

.field private k:Lcom/android/calendar/task/bs;

.field private final l:Ljava/util/LinkedList;

.field private final m:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final n:Z

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Ljava/lang/Runnable;

.field private r:Z

.field private s:Ljava/lang/String;

.field private t:J

.field private u:I

.field private v:Ljava/util/HashMap;

.field private w:Lcom/android/calendar/task/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55
    const-class v0, Lcom/android/calendar/task/au;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/au;->c:Ljava/lang/String;

    .line 92
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "utc_due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "previousId"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "parentId"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "task_order"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "groupName"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/task/au;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/task/TaskDeleteView;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 243
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 132
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    .line 134
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/au;->m:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 144
    new-instance v0, Lcom/android/calendar/task/av;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/av;-><init>(Lcom/android/calendar/task/au;)V

    iput-object v0, p0, Lcom/android/calendar/task/au;->q:Ljava/lang/Runnable;

    .line 157
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/task/au;->t:J

    .line 159
    iput v3, p0, Lcom/android/calendar/task/au;->u:I

    .line 161
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/au;->v:Ljava/util/HashMap;

    .line 162
    iput-boolean v3, p0, Lcom/android/calendar/task/au;->a:Z

    .line 169
    iput-object v2, p0, Lcom/android/calendar/task/au;->b:Lcom/android/calendar/task/aw;

    .line 353
    iput-object v2, p0, Lcom/android/calendar/task/au;->w:Lcom/android/calendar/task/ah;

    .line 244
    iput-object p1, p0, Lcom/android/calendar/task/au;->e:Landroid/content/Context;

    .line 246
    iget-object v0, p0, Lcom/android/calendar/task/au;->e:Landroid/content/Context;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/au;->n:Z

    .line 248
    iget-object v0, p0, Lcom/android/calendar/task/au;->q:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/au;->o:Ljava/lang/String;

    .line 249
    iput-object p2, p0, Lcom/android/calendar/task/au;->g:Lcom/android/calendar/task/TaskDeleteView;

    .line 250
    new-instance v0, Lcom/android/calendar/task/ax;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/task/ax;-><init>(Lcom/android/calendar/task/au;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/task/au;->f:Lcom/android/calendar/task/ax;

    .line 252
    iput-boolean p3, p0, Lcom/android/calendar/task/au;->p:Z

    .line 253
    iput-object v2, p0, Lcom/android/calendar/task/au;->s:Ljava/lang/String;

    .line 254
    iget-object v0, p0, Lcom/android/calendar/task/au;->e:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f1200dc

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/au;->h:Landroid/view/View;

    .line 255
    return-void
.end method

.method private a(Landroid/text/format/Time;)I
    .locals 2

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/android/calendar/task/au;->b(Landroid/text/format/Time;)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_0

    .line 361
    iget v1, v0, Lcom/android/calendar/task/bs;->f:I

    iget-object v0, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ai;->a(Landroid/text/format/Time;)I

    move-result v0

    add-int/2addr v0, v1

    .line 363
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/task/au;I)I
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/android/calendar/task/au;->j:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/task/au;J)J
    .locals 1

    .prologue
    .line 53
    iput-wide p1, p0, Lcom/android/calendar/task/au;->t:J

    return-wide p1
.end method

.method static synthetic a(Lcom/android/calendar/task/au;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/calendar/task/au;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/task/au;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/calendar/task/au;->h:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/au;Lcom/android/calendar/task/ah;)Lcom/android/calendar/task/ah;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/calendar/task/au;->w:Lcom/android/calendar/task/ah;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/au;Lcom/android/calendar/task/bs;)Lcom/android/calendar/task/bs;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/calendar/task/au;->k:Lcom/android/calendar/task/bs;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/au;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/calendar/task/au;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/au;Lcom/android/calendar/task/ay;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/ay;)V

    return-void
.end method

.method private a(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 521
    iget-object v2, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    monitor-enter v2

    .line 522
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523
    monitor-exit v2

    move v0, v1

    .line 525
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->d:I

    if-gt v0, p1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->e:I

    if-gt p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    monitor-exit v2

    goto :goto_0

    .line 526
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    .line 525
    goto :goto_1
.end method

.method private a(IILandroid/text/format/Time;Ljava/lang/String;I)Z
    .locals 2

    .prologue
    .line 531
    iget-object v1, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    monitor-enter v1

    .line 532
    :try_start_0
    new-instance v0, Lcom/android/calendar/task/ay;

    invoke-direct {v0, p5}, Lcom/android/calendar/task/ay;-><init>(I)V

    .line 533
    iput-object p3, v0, Lcom/android/calendar/task/ay;->b:Landroid/text/format/Time;

    .line 534
    iput p1, v0, Lcom/android/calendar/task/ay;->c:I

    .line 535
    iput p2, v0, Lcom/android/calendar/task/ay;->d:I

    .line 536
    iput-object p4, v0, Lcom/android/calendar/task/ay;->e:Ljava/lang/String;

    .line 537
    invoke-direct {p0, v0}, Lcom/android/calendar/task/au;->a(Lcom/android/calendar/task/ay;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 538
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/android/calendar/task/au;II)Z
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/task/au;->a(II)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/android/calendar/task/ay;)Z
    .locals 2

    .prologue
    .line 542
    iget-object v0, p0, Lcom/android/calendar/task/au;->s:Ljava/lang/String;

    iput-object v0, p1, Lcom/android/calendar/task/ay;->e:Ljava/lang/String;

    .line 544
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 545
    iget-object v0, p0, Lcom/android/calendar/task/au;->m:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 546
    iget-object v1, p0, Lcom/android/calendar/task/au;->m:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 547
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 548
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 549
    invoke-direct {p0, p1}, Lcom/android/calendar/task/au;->b(Lcom/android/calendar/task/ay;)V

    .line 551
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private b(Landroid/text/format/Time;)Lcom/android/calendar/task/bs;
    .locals 5

    .prologue
    .line 388
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 389
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 390
    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v0, v1}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    .line 391
    iget-object v2, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    monitor-enter v2

    .line 392
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 393
    iget v4, v0, Lcom/android/calendar/task/bs;->d:I

    if-gt v4, v1, :cond_0

    iget v4, v0, Lcom/android/calendar/task/bs;->e:I

    if-gt v1, v4, :cond_0

    .line 394
    monitor-exit v2

    .line 398
    :goto_0
    return-object v0

    .line 397
    :cond_1
    monitor-exit v2

    .line 398
    const/4 v0, 0x0

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/android/calendar/task/au;I)Lcom/android/calendar/task/bs;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/android/calendar/task/au;->f(I)Lcom/android/calendar/task/bs;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/task/au;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    return-object v0
.end method

.method private b(Lcom/android/calendar/task/ay;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 564
    iget-object v0, p0, Lcom/android/calendar/task/au;->f:Lcom/android/calendar/task/ax;

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ax;->cancelOperation(I)V

    .line 568
    sget-object v3, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    .line 569
    iget-object v0, p0, Lcom/android/calendar/task/au;->f:Lcom/android/calendar/task/ax;

    sget-object v4, Lcom/android/calendar/task/au;->d:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/calendar/task/au;->g()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    iget v2, p0, Lcom/android/calendar/task/au;->u:I

    invoke-static {v2}, Lcom/android/calendar/task/au;->g(I)Ljava/lang/String;

    move-result-object v7

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/task/ax;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/task/au;I)I
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/android/calendar/task/au;->i:I

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/task/au;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/android/calendar/task/au;->r:Z

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/task/au;I)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/android/calendar/task/au;->i:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/task/au;->i:I

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/task/au;)Lcom/android/calendar/task/TaskDeleteView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/calendar/task/au;->g:Lcom/android/calendar/task/TaskDeleteView;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/task/au;)J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/android/calendar/task/au;->t:J

    return-wide v0
.end method

.method private f(I)Lcom/android/calendar/task/bs;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 471
    iget-object v3, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    monitor-enter v3

    .line 473
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 474
    iget-object v2, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    const/4 v4, 0x5

    if-lt v2, v4, :cond_3

    .line 475
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    .line 476
    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 477
    iget v2, p0, Lcom/android/calendar/task/au;->i:I

    iget v4, v0, Lcom/android/calendar/task/bs;->g:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/android/calendar/task/au;->i:I

    .line 484
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 485
    iget-object v1, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 486
    iget-object v1, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 488
    :cond_1
    monitor-exit v3

    .line 512
    :goto_1
    return-object v0

    .line 478
    :cond_2
    if-nez p1, :cond_0

    .line 479
    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 480
    iget v2, p0, Lcom/android/calendar/task/au;->i:I

    iget v4, v0, Lcom/android/calendar/task/bs;->g:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/android/calendar/task/au;->i:I

    .line 482
    const/4 v2, 0x0

    iput v2, v0, Lcom/android/calendar/task/bs;->g:I

    goto :goto_0

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 492
    :cond_3
    :try_start_1
    iget v2, p0, Lcom/android/calendar/task/au;->i:I

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    if-ne p1, v2, :cond_6

    .line 493
    :cond_4
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/calendar/task/au;->i:I

    move-object v2, v0

    .line 497
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 498
    if-eqz v0, :cond_8

    .line 500
    iget-object v2, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 501
    iget v2, v0, Lcom/android/calendar/task/bs;->g:I

    add-int/2addr v1, v2

    move v2, v1

    move-object v1, v0

    .line 504
    :goto_3
    if-nez v0, :cond_7

    .line 506
    if-eqz v1, :cond_5

    .line 507
    const/4 v0, 0x0

    iput-object v0, v1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    .line 508
    iput v2, v1, Lcom/android/calendar/task/bs;->g:I

    :cond_5
    move-object v0, v1

    .line 512
    :cond_6
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_7
    move v5, v2

    move-object v2, v1

    move v1, v5

    goto :goto_2

    :cond_8
    move v5, v1

    move-object v1, v2

    move v2, v5

    goto :goto_3
.end method

.method static synthetic f(Lcom/android/calendar/task/au;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/android/calendar/task/au;->p:Z

    return v0
.end method

.method private g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/android/calendar/task/au;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static g(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 974
    .line 975
    packed-switch p0, :pswitch_data_0

    .line 995
    const-string v0, "utc_due_date ASC, importance DESC, task_order ASC"

    .line 998
    :goto_0
    return-object v0

    .line 977
    :pswitch_0
    const-string v0, "utc_due_date ASC, importance DESC, task_order ASC"

    goto :goto_0

    .line 980
    :pswitch_1
    const-string v0, "importance DESC, task_order ASC"

    goto :goto_0

    .line 983
    :pswitch_2
    const-string v0, "utc_due_date ASC, importance DESC, task_order ASC"

    goto :goto_0

    .line 986
    :pswitch_3
    const-string v0, "utc_due_date ASC, importance DESC, task_order ASC"

    goto :goto_0

    .line 989
    :pswitch_4
    const-string v0, "utc_due_date ASC, importance DESC, task_order ASC"

    goto :goto_0

    .line 992
    :pswitch_5
    const-string v0, "group_order ASC, task_order ASC"

    goto :goto_0

    .line 975
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic g(Lcom/android/calendar/task/au;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/calendar/task/au;->m:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/task/au;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/android/calendar/task/au;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/task/au;->j:I

    return v0
.end method

.method static synthetic i(Lcom/android/calendar/task/au;)Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/calendar/task/au;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/task/au;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/android/calendar/task/au;->i:I

    return v0
.end method


# virtual methods
.method public a(I)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 813
    iget-boolean v1, p0, Lcom/android/calendar/task/au;->n:Z

    if-nez v1, :cond_1

    .line 822
    :cond_0
    :goto_0
    return v0

    .line 817
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v1

    .line 818
    if-eqz v1, :cond_0

    .line 819
    iget-object v2, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget v3, v1, Lcom/android/calendar/task/bs;->f:I

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, Lcom/android/calendar/task/ai;->a(I)I

    move-result v2

    .line 820
    if-eq v2, v0, :cond_0

    iget v0, v1, Lcom/android/calendar/task/bs;->f:I

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 462
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/task/au;->r:Z

    .line 463
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/calendar/task/au;->f(I)Lcom/android/calendar/task/bs;

    .line 464
    invoke-virtual {p0}, Lcom/android/calendar/task/au;->notifyDataSetChanged()V

    .line 465
    iget-object v0, p0, Lcom/android/calendar/task/au;->f:Lcom/android/calendar/task/ax;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/android/calendar/task/au;->f:Lcom/android/calendar/task/ax;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ax;->cancelOperation(I)V

    .line 468
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 803
    iput-wide p1, p0, Lcom/android/calendar/task/au;->t:J

    .line 804
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/task/au;->w:Lcom/android/calendar/task/ah;

    .line 805
    return-void
.end method

.method public a(Landroid/text/format/Time;JLjava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 416
    if-eqz p4, :cond_0

    .line 417
    iput-object p4, p0, Lcom/android/calendar/task/au;->s:Ljava/lang/String;

    .line 425
    :cond_0
    if-nez p1, :cond_6

    .line 426
    new-instance v3, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/task/au;->e:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 427
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 430
    :goto_0
    invoke-virtual {v3, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-wide v4, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    .line 432
    if-nez p5, :cond_5

    invoke-direct {p0, v0, v0}, Lcom/android/calendar/task/au;->a(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 435
    iget v0, p0, Lcom/android/calendar/task/au;->u:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/calendar/task/au;->u:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/android/calendar/task/au;->u:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 459
    :cond_1
    :goto_1
    return-void

    .line 440
    :cond_2
    invoke-direct {p0, v3}, Lcom/android/calendar/task/au;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 441
    if-lez v0, :cond_3

    .line 442
    iget-object v1, p0, Lcom/android/calendar/task/au;->g:Lcom/android/calendar/task/TaskDeleteView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/TaskDeleteView;->setSelection(I)V

    .line 444
    :cond_3
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/task/au;->o:Ljava/lang/String;

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 445
    if-eqz v3, :cond_4

    .line 446
    invoke-virtual {v4, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 450
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/task/au;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x400

    const-wide/16 v6, -0x1

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto :goto_1

    .line 457
    :cond_5
    const v1, 0x24dc87

    const v2, 0x259d23

    const/4 v5, 0x2

    move-object v0, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/task/au;->a(IILandroid/text/format/Time;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_6
    move-object v3, p1

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/android/calendar/task/aw;)V
    .locals 1

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/android/calendar/task/au;->b:Lcom/android/calendar/task/aw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    monitor-exit p0

    return-void

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 843
    iput-object p1, p0, Lcom/android/calendar/task/au;->v:Ljava/util/HashMap;

    .line 844
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 851
    iput-boolean p1, p0, Lcom/android/calendar/task/au;->a:Z

    .line 852
    iget-object v0, p0, Lcom/android/calendar/task/au;->v:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 853
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 827
    if-ltz p1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/task/au;->n:Z

    if-nez v1, :cond_1

    .line 834
    :cond_0
    :goto_0
    return v0

    .line 830
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v1

    .line 831
    if-eqz v1, :cond_0

    .line 832
    iget-object v0, v1, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget v1, v1, Lcom/android/calendar/task/bs;->f:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ai;->b(I)I

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/android/calendar/task/au;->q:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 782
    return-void
.end method

.method public c()Lcom/android/calendar/task/ah;
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lcom/android/calendar/task/au;->w:Lcom/android/calendar/task/ah;

    return-object v0
.end method

.method protected c(I)Lcom/android/calendar/task/bs;
    .locals 5

    .prologue
    .line 368
    iget-object v1, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    monitor-enter v1

    .line 369
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/au;->k:Lcom/android/calendar/task/bs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/au;->k:Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->f:I

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/au;->k:Lcom/android/calendar/task/bs;

    iget v0, v0, Lcom/android/calendar/task/bs;->f:I

    iget-object v2, p0, Lcom/android/calendar/task/au;->k:Lcom/android/calendar/task/bs;

    iget v2, v2, Lcom/android/calendar/task/bs;->g:I

    add-int/2addr v0, v2

    if-ge p1, v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/android/calendar/task/au;->k:Lcom/android/calendar/task/bs;

    monitor-exit v1

    .line 381
    :goto_0
    return-object v0

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/bs;

    .line 374
    iget v3, v0, Lcom/android/calendar/task/bs;->f:I

    if-gt v3, p1, :cond_1

    iget v3, v0, Lcom/android/calendar/task/bs;->f:I

    iget v4, v0, Lcom/android/calendar/task/bs;->g:I

    add-int/2addr v3, v4

    if-ge p1, v3, :cond_1

    .line 376
    iput-object v0, p0, Lcom/android/calendar/task/au;->k:Lcom/android/calendar/task/bs;

    .line 377
    monitor-exit v1

    goto :goto_0

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 799
    iget-wide v0, p0, Lcom/android/calendar/task/au;->t:J

    return-wide v0
.end method

.method public d(I)J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 404
    if-gez p1, :cond_1

    .line 411
    :cond_0
    :goto_0
    return-wide v0

    .line 407
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v2

    .line 408
    if-eqz v2, :cond_0

    .line 409
    iget-object v0, v2, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ai;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public e()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/android/calendar/task/au;->v:Ljava/util/HashMap;

    return-object v0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 967
    iput p1, p0, Lcom/android/calendar/task/au;->u:I

    .line 968
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 847
    iget-boolean v0, p0, Lcom/android/calendar/task/au;->a:Z

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 293
    .line 294
    iget-object v1, p0, Lcom/android/calendar/task/au;->l:Ljava/util/LinkedList;

    monitor-enter v1

    .line 295
    :try_start_0
    iget v0, p0, Lcom/android/calendar/task/au;->i:I

    .line 296
    monitor-exit v1

    .line 297
    return v0

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 302
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_0

    .line 304
    iget-object v1, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget v0, v0, Lcom/android/calendar/task/bs;->f:I

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/ai;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 306
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 318
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_0

    .line 320
    iget v1, v0, Lcom/android/calendar/task/bs;->f:I

    sub-int v1, p1, v1

    shl-int/lit8 v1, v1, 0x14

    iget v0, v0, Lcom/android/calendar/task/bs;->d:I

    add-int/2addr v0, v1

    int-to-long v0, v0

    .line 322
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 272
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 273
    if-eqz v0, :cond_0

    .line 274
    iget-object v1, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget v0, v0, Lcom/android/calendar/task/bs;->f:I

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/ai;->getItemViewType(I)I

    move-result v0

    .line 276
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 329
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_1

    .line 331
    iget v1, v0, Lcom/android/calendar/task/bs;->f:I

    sub-int v1, p1, v1

    .line 332
    iget-object v0, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/calendar/task/ai;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 343
    :goto_0
    iget-boolean v1, p0, Lcom/android/calendar/task/au;->n:Z

    if-nez v1, :cond_0

    .line 350
    :cond_0
    return-object v0

    .line 336
    :cond_1
    sget-object v0, Lcom/android/calendar/task/au;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BUG: getAdapterInfoByPosition returned null!!! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/task/au;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 338
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bug! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x3

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 283
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/au;->c(I)Lcom/android/calendar/task/bs;

    move-result-object v0

    .line 284
    if-eqz v0, :cond_0

    .line 285
    iget-object v1, v0, Lcom/android/calendar/task/bs;->c:Lcom/android/calendar/task/ai;

    iget v0, v0, Lcom/android/calendar/task/bs;->f:I

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/ai;->isEnabled(I)Z

    move-result v0

    .line 287
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
