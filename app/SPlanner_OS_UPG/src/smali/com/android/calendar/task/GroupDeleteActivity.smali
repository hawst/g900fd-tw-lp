.class public Lcom/android/calendar/task/GroupDeleteActivity;
.super Lcom/android/calendar/b;
.source "GroupDeleteActivity.java"


# instance fields
.field private b:Lcom/android/calendar/task/GroupDeleteFragment;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f12006b

    .line 37
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/GroupDeleteActivity;->setContentView(I)V

    .line 41
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/GroupDeleteActivity;->c:Z

    .line 43
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/GroupDeleteFragment;

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteActivity;->b:Lcom/android/calendar/task/GroupDeleteFragment;

    .line 45
    iget-object v0, p0, Lcom/android/calendar/task/GroupDeleteActivity;->b:Lcom/android/calendar/task/GroupDeleteFragment;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/android/calendar/task/GroupDeleteFragment;

    invoke-direct {v0}, Lcom/android/calendar/task/GroupDeleteFragment;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/GroupDeleteActivity;->b:Lcom/android/calendar/task/GroupDeleteFragment;

    .line 48
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteActivity;->b:Lcom/android/calendar/task/GroupDeleteFragment;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 50
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteActivity;->b:Lcom/android/calendar/task/GroupDeleteFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 51
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 53
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f1200d4

    const/4 v4, 0x4

    const/4 v3, 0x1

    const v2, 0x7f1200d5

    .line 57
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 58
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 59
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0f014a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 60
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 61
    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/task/GroupDeleteActivity;->c:Z

    if-eqz v0, :cond_1

    .line 62
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02005c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 63
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020057

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 69
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 71
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 72
    return v3

    .line 65
    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 66
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 86
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 97
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    .line 89
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/task/GroupDeleteActivity;->finish()V

    goto :goto_0

    .line 92
    :sswitch_1
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteActivity;->b:Lcom/android/calendar/task/GroupDeleteFragment;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteActivity;->b:Lcom/android/calendar/task/GroupDeleteFragment;

    invoke-virtual {v1}, Lcom/android/calendar/task/GroupDeleteFragment;->c()V

    goto :goto_0

    .line 86
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f1200d4 -> :sswitch_0
        0x7f1200d5 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 77
    const v0, 0x7f1200d5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/android/calendar/task/GroupDeleteActivity;->b:Lcom/android/calendar/task/GroupDeleteFragment;

    invoke-virtual {v1}, Lcom/android/calendar/task/GroupDeleteFragment;->b()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 81
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
