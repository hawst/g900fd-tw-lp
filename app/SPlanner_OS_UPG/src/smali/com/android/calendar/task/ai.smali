.class public Lcom/android/calendar/task/ai;
.super Landroid/widget/BaseAdapter;
.source "TaskByDayAdapter.java"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcom/android/calendar/task/ac;

.field protected c:Ljava/util/ArrayList;

.field protected d:I

.field protected e:Landroid/text/format/Time;

.field protected f:Ljava/lang/String;

.field protected g:I

.field protected final h:Ljava/util/ArrayList;

.field protected final i:Ljava/util/ArrayList;

.field protected j:Z

.field private k:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/ai;->h:Ljava/util/ArrayList;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/ai;->i:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Lcom/android/calendar/task/aj;

    invoke-direct {v0, p0}, Lcom/android/calendar/task/aj;-><init>(Lcom/android/calendar/task/ai;)V

    iput-object v0, p0, Lcom/android/calendar/task/ai;->k:Ljava/lang/Runnable;

    .line 103
    iput-object p1, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    .line 104
    new-instance v0, Lcom/android/calendar/task/ac;

    const v1, 0x7f0400b2

    invoke-direct {v0, p1, v1}, Lcom/android/calendar/task/ac;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    .line 105
    iget-object v0, p0, Lcom/android/calendar/task/ai;->k:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    .line 106
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/ai;ZJ)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/task/ai;->b(ZJ)V

    return-void
.end method

.method private a(Ljava/lang/Boolean;Lcom/android/calendar/task/ao;)V
    .locals 3

    .prologue
    .line 986
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 987
    iget-object v1, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 989
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 990
    iget-object v1, p2, Lcom/android/calendar/task/ao;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 994
    :goto_0
    return-void

    .line 992
    :cond_0
    iget-object v0, p2, Lcom/android/calendar/task/ao;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private b()V
    .locals 0

    .prologue
    .line 1201
    invoke-direct {p0}, Lcom/android/calendar/task/ai;->c()V

    .line 1202
    invoke-direct {p0}, Lcom/android/calendar/task/ai;->e()V

    .line 1203
    invoke-direct {p0}, Lcom/android/calendar/task/ai;->d()V

    .line 1204
    return-void
.end method

.method private b(ZJ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1021
    new-instance v1, Landroid/content/ContentValues;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 1022
    const-string v2, "complete"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1023
    const-string v0, "date_completed"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1024
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1025
    iget-object v2, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1026
    return-void

    .line 1022
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 1207
    const/4 v0, 0x0

    .line 1209
    iget-object v1, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1218
    :goto_0
    return-void

    .line 1212
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 1213
    iget v3, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    .line 1214
    :cond_1
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 1215
    goto :goto_1

    .line 1217
    :cond_2
    iput v1, p0, Lcom/android/calendar/task/ai;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 1225
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1245
    :goto_0
    return-void

    .line 1228
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1232
    iget-object v7, p0, Lcom/android/calendar/task/ai;->i:Ljava/util/ArrayList;

    monitor-enter v7

    .line 1233
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v4, v2

    move v1, v5

    move v3, v2

    .line 1234
    :goto_1
    if-ge v4, v6, :cond_2

    .line 1235
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 1236
    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    if-nez v0, :cond_1

    .line 1237
    add-int/lit8 v0, v3, 0x1

    move v8, v1

    move v1, v0

    move v0, v8

    .line 1234
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 1238
    :cond_1
    add-int/lit8 v0, v1, 0x1

    if-eq v1, v5, :cond_3

    .line 1239
    iget-object v1, p0, Lcom/android/calendar/task/ai;->i:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 1240
    goto :goto_2

    .line 1243
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/ai;->i:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1244
    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method private e()V
    .locals 6

    .prologue
    .line 1261
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1275
    :goto_0
    return-void

    .line 1265
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/task/ai;->h:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1266
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1267
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1268
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    .line 1269
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 1270
    iget v4, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    .line 1271
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/ai;->h:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1268
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1274
    :cond_3
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1221
    iget v0, p0, Lcom/android/calendar/task/ai;->g:I

    return v0
.end method

.method public a(I)I
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 111
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    move v0, v2

    .line 120
    :goto_0
    return v0

    :cond_1
    move v1, p1

    .line 115
    :goto_1
    if-ltz v1, :cond_4

    .line 116
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 117
    if-eqz v0, :cond_3

    iget v3, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    :cond_2
    move v0, v1

    .line 118
    goto :goto_0

    .line 115
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 120
    goto :goto_0
.end method

.method public a(Landroid/text/format/Time;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1105
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1129
    :cond_0
    :goto_0
    return v1

    .line 1108
    :cond_1
    invoke-virtual {p1, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 1109
    iget-wide v4, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    .line 1110
    const/16 v3, 0x3e8

    .line 1112
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v1

    .line 1113
    :goto_1
    if-ge v1, v5, :cond_2

    .line 1114
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 1115
    iget v6, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 1116
    iget v0, v0, Lcom/android/calendar/task/an;->h:I

    sub-int v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1117
    if-eqz v0, :cond_0

    .line 1120
    if-ge v0, v3, :cond_3

    move v2, v0

    move v0, v1

    .line 1113
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 1129
    goto :goto_0

    :cond_3
    move v0, v2

    move v2, v3

    goto :goto_2
.end method

.method public a(ILjava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const v4, 0x24dc87

    const v3, 0x7f0f017d

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 828
    const-string v0, ""

    .line 830
    iget-object v1, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    const-string v2, "preferences_list_by"

    invoke-static {v1, v2, v8}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 834
    if-nez v1, :cond_1

    .line 835
    packed-switch p1, :pswitch_data_0

    move-object p2, v0

    .line 982
    :cond_0
    :goto_0
    return-object p2

    .line 837
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 840
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 843
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 846
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 849
    :pswitch_4
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 855
    :cond_1
    if-ne v1, v6, :cond_2

    .line 856
    packed-switch p1, :pswitch_data_1

    move-object p2, v0

    .line 867
    goto :goto_0

    .line 858
    :pswitch_5
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0346

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 861
    :pswitch_6
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0348

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 864
    :pswitch_7
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0347

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 869
    :cond_2
    if-ne v1, v7, :cond_8

    .line 871
    if-ne p1, v6, :cond_3

    .line 872
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 873
    :cond_3
    if-ge p1, v4, :cond_4

    .line 876
    const-string p2, ""

    goto/16 :goto_0

    .line 882
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/task/ai;->k:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 883
    iget-object v1, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 884
    iput-object v0, p0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    .line 885
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    .line 891
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    .line 892
    invoke-virtual {v1, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v2

    invoke-static {v0}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 894
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 895
    :cond_6
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v0

    .line 896
    iget-object v2, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    invoke-virtual {v2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_7

    .line 897
    iget-object v2, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7b2

    if-eq v2, v3, :cond_7

    .line 898
    iget-object v0, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 899
    iget-object v2, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    iput v6, v2, Landroid/text/format/Time;->hour:I

    .line 900
    iget-object v2, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    iput-boolean v8, v2, Landroid/text/format/Time;->allDay:Z

    .line 901
    iget-object v2, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    invoke-virtual {v2, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 906
    :cond_7
    iget-object v2, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-static {v0, v1, v2, v7}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 908
    :cond_8
    const/4 v2, 0x3

    if-ne v1, v2, :cond_e

    .line 910
    if-ne p1, v6, :cond_9

    .line 911
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 912
    :cond_9
    if-ge p1, v4, :cond_a

    .line 915
    const-string p2, ""

    goto/16 :goto_0

    .line 921
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/task/ai;->k:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 922
    iget-object v1, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 923
    iput-object v0, p0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    .line 924
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    .line 930
    :cond_b
    iget-object v1, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    .line 931
    invoke-virtual {v1, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v2

    invoke-static {v0}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 932
    add-int/lit8 v4, p1, 0x6

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v4

    invoke-static {v0}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v0

    sub-long v0, v4, v0

    .line 933
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v4

    if-nez v4, :cond_c

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 934
    :cond_c
    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v2

    .line 935
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v0

    .line 938
    :cond_d
    iget-object v4, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-static {v2, v3, v4, v7}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 939
    iget-object v3, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-static {v0, v1, v3, v7}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 940
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 943
    :cond_e
    const/4 v2, 0x4

    if-ne v1, v2, :cond_14

    .line 944
    if-ne p1, v6, :cond_f

    .line 945
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 946
    :cond_f
    if-ge p1, v4, :cond_10

    .line 949
    const-string p2, ""

    goto/16 :goto_0

    .line 955
    :cond_10
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/task/ai;->k:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    .line 956
    iget-object v0, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 957
    iput-object v2, p0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    .line 958
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    .line 963
    :cond_11
    iget-object v3, p0, Lcom/android/calendar/task/ai;->e:Landroid/text/format/Time;

    .line 964
    invoke-virtual {v3, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    move-result-wide v0

    invoke-static {v2}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v0, v4

    .line 965
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v4

    if-nez v4, :cond_12

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 966
    :cond_12
    invoke-static {v3, p1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v0

    invoke-static {v2}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 967
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v0

    .line 970
    :cond_13
    iget-object v2, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-static {v0, v1, v2, v6}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 972
    :cond_14
    const/4 v2, 0x5

    if-ne v1, v2, :cond_16

    .line 973
    const-string v0, "My task"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 974
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 975
    :cond_15
    const-string v0, "Default"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 976
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_16
    move-object p2, v0

    goto/16 :goto_0

    .line 835
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 856
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 200
    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    if-nez v1, :cond_0

    .line 201
    iput p2, v0, Lcom/android/calendar/task/an;->d:I

    .line 204
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 207
    const v0, 0x7f12003b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 208
    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 210
    mul-int/lit8 v2, p2, 0x28

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 211
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 213
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/task/bs;)V
    .locals 2

    .prologue
    .line 381
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->b(Lcom/android/calendar/task/bs;)V

    .line 382
    invoke-direct {p0}, Lcom/android/calendar/task/ai;->b()V

    .line 383
    iget-object v0, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    iget-object v1, p1, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/android/calendar/task/ac;->changeCursor(Landroid/database/Cursor;)V

    .line 384
    return-void
.end method

.method public a(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 1197
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/ai;->j:Z

    .line 1198
    return-void
.end method

.method public a(ZJ)V
    .locals 4

    .prologue
    .line 1043
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1044
    const v0, 0x7f0f0249

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1046
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0247

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f02e3

    new-instance v3, Lcom/android/calendar/task/al;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/android/calendar/task/al;-><init>(Lcom/android/calendar/task/ai;ZJ)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/task/ak;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/android/calendar/task/ak;-><init>(Lcom/android/calendar/task/ai;ZJ)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1058
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1059
    return-void

    .line 1046
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0248

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(J)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1029
    .line 1030
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1031
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 1032
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1033
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    iget-wide v4, v0, Lcom/android/calendar/task/an;->f:J

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    .line 1034
    const/4 v0, 0x1

    .line 1039
    :goto_1
    return v0

    .line 1032
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1183
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)I
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 126
    const/4 v0, -0x1

    .line 136
    :goto_0
    return v0

    .line 128
    :cond_0
    const/4 v1, 0x0

    .line 129
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 130
    add-int/lit8 v0, p1, 0x1

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 131
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    if-eqz v0, :cond_1

    move v0, v1

    .line 132
    goto :goto_0

    .line 134
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 130
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 136
    goto :goto_0
.end method

.method public b(Lcom/android/calendar/task/bs;)V
    .locals 3

    .prologue
    .line 387
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    const-string v1, "preferences_list_by"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 390
    if-nez v0, :cond_1

    .line 391
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->c(Lcom/android/calendar/task/bs;)V

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 392
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 393
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->d(Lcom/android/calendar/task/bs;)V

    goto :goto_0

    .line 394
    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 395
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->e(Lcom/android/calendar/task/bs;)V

    goto :goto_0

    .line 396
    :cond_3
    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 397
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->h(Lcom/android/calendar/task/bs;)V

    goto :goto_0

    .line 398
    :cond_4
    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 399
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->f(Lcom/android/calendar/task/bs;)V

    goto :goto_0

    .line 400
    :cond_5
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 401
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->g(Lcom/android/calendar/task/bs;)V

    goto :goto_0
.end method

.method public c(I)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 174
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 176
    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    if-nez v1, :cond_0

    .line 177
    iget-wide v0, v0, Lcom/android/calendar/task/an;->f:J

    .line 182
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    .line 179
    goto :goto_0

    :cond_1
    move-wide v0, v2

    .line 182
    goto :goto_0
.end method

.method public c(Lcom/android/calendar/task/bs;)V
    .locals 28

    .prologue
    .line 409
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    move-object/from16 v20, v0

    .line 410
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 412
    const/4 v7, 0x0

    .line 413
    const/4 v6, 0x0

    .line 414
    const/4 v5, 0x0

    .line 415
    const/4 v4, 0x0

    .line 416
    const/4 v3, 0x0

    .line 422
    new-instance v22, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 423
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 424
    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 425
    move-object/from16 v0, v22

    iget-wide v10, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v10, v11}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/ai;->d:I

    .line 427
    const/4 v2, 0x0

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    move/from16 v17, v5

    move/from16 v18, v6

    move/from16 v19, v7

    :goto_0
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 430
    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 433
    const/16 v2, 0xb

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v26, v0

    .line 436
    const/4 v2, 0x3

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 437
    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 438
    move-object/from16 v0, v22

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v12

    .line 440
    const/4 v2, 0x3

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 441
    if-nez v19, :cond_0

    .line 442
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/task/ai;->d:I

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    :cond_0
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/task/ai;->d:I

    move-wide/from16 v6, v24

    move-wide/from16 v8, v26

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    add-int/lit8 v19, v19, 0x1

    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    move/from16 v6, v18

    move/from16 v7, v19

    .line 427
    :goto_1
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    move/from16 v17, v5

    move/from16 v18, v6

    move/from16 v19, v7

    goto :goto_0

    .line 449
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/task/ai;->d:I

    if-ge v12, v2, :cond_3

    .line 450
    if-nez v15, :cond_2

    .line 451
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x5

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    :cond_2
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v24

    move-wide/from16 v8, v26

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    add-int/lit8 v15, v15, 0x1

    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    move/from16 v6, v18

    move/from16 v7, v19

    .line 455
    goto :goto_1

    .line 456
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/task/ai;->d:I

    if-ne v12, v2, :cond_5

    .line 457
    if-nez v18, :cond_4

    .line 458
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    :cond_4
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v24

    move-wide/from16 v8, v26

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    add-int/lit8 v18, v18, 0x1

    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    move/from16 v6, v18

    move/from16 v7, v19

    .line 462
    goto/16 :goto_1

    .line 463
    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/task/ai;->d:I

    add-int/lit8 v2, v2, 0x7

    if-gt v12, v2, :cond_7

    .line 464
    if-nez v17, :cond_6

    .line 465
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x3

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    :cond_6
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v24

    move-wide/from16 v8, v26

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    add-int/lit8 v17, v17, 0x1

    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    move/from16 v6, v18

    move/from16 v7, v19

    .line 469
    goto/16 :goto_1

    .line 470
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/task/ai;->d:I

    add-int/lit8 v2, v2, 0x7

    if-lt v12, v2, :cond_c

    .line 471
    if-nez v16, :cond_8

    .line 472
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x4

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    :cond_8
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v24

    move-wide/from16 v8, v26

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    add-int/lit8 v16, v16, 0x1

    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    move/from16 v6, v18

    move/from16 v7, v19

    .line 476
    goto/16 :goto_1

    .line 481
    :cond_9
    if-lez v19, :cond_a

    .line 482
    const/4 v2, 0x0

    :goto_2
    add-int/lit8 v3, v19, 0x1

    if-ge v2, v3, :cond_a

    .line 483
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 484
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 482
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 488
    :cond_a
    if-lez v15, :cond_b

    .line 489
    const/4 v2, 0x0

    :goto_3
    add-int/lit8 v3, v15, 0x1

    if-ge v2, v3, :cond_b

    .line 490
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 491
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 489
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 495
    :cond_b
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    .line 496
    return-void

    :cond_c
    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    move/from16 v6, v18

    move/from16 v7, v19

    goto/16 :goto_1
.end method

.method public d(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 186
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 188
    iget v2, v0, Lcom/android/calendar/task/an;->a:I

    if-nez v2, :cond_0

    .line 189
    iget v0, v0, Lcom/android/calendar/task/an;->d:I

    .line 194
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 191
    goto :goto_0

    :cond_1
    move v0, v1

    .line 194
    goto :goto_0
.end method

.method public d(Lcom/android/calendar/task/bs;)V
    .locals 24

    .prologue
    .line 502
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    move-object/from16 v18, v0

    .line 503
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 505
    const/4 v5, 0x0

    .line 506
    const/4 v4, 0x0

    .line 507
    const/4 v3, 0x0

    .line 512
    const/4 v2, 0x0

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    move/from16 v17, v5

    :goto_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 515
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 518
    const/16 v2, 0xb

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v22, v0

    .line 521
    const/4 v2, 0x4

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 523
    if-nez v2, :cond_1

    .line 524
    if-nez v17, :cond_0

    .line 525
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 527
    :cond_0
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v6, v20

    move-wide/from16 v8, v22

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 528
    add-int/lit8 v17, v17, 0x1

    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    .line 512
    :goto_1
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    move/from16 v17, v5

    goto :goto_0

    .line 530
    :cond_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 531
    if-nez v16, :cond_2

    .line 532
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 535
    :cond_2
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v6, v20

    move-wide/from16 v8, v22

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 536
    add-int/lit8 v16, v16, 0x1

    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    .line 537
    goto :goto_1

    .line 538
    :cond_3
    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 539
    if-nez v15, :cond_4

    .line 540
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 542
    :cond_4
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v6, v20

    move-wide/from16 v8, v22

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 543
    add-int/lit8 v15, v15, 0x1

    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    .line 544
    goto :goto_1

    .line 547
    :cond_5
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    .line 548
    return-void

    :cond_6
    move v3, v15

    move/from16 v4, v16

    move/from16 v5, v17

    goto/16 :goto_1
.end method

.method public e(Lcom/android/calendar/task/bs;)V
    .locals 26

    .prologue
    .line 554
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    move-object/from16 v19, v0

    .line 555
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 557
    const/4 v3, 0x0

    .line 558
    const/4 v2, -0x1

    .line 564
    new-instance v21, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 565
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 566
    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 567
    move-object/from16 v0, v21

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/task/ai;->d:I

    .line 569
    const/16 v17, 0x0

    move v14, v2

    move/from16 v18, v3

    :goto_0
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 572
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 575
    const/16 v2, 0xb

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v24, v0

    .line 578
    const/4 v2, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 579
    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 580
    move-object/from16 v0, v21

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 582
    const/4 v3, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 583
    if-nez v18, :cond_0

    .line 584
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/task/ai;->d:I

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 587
    :cond_0
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/task/ai;->d:I

    move-wide/from16 v6, v22

    move-wide/from16 v8, v24

    move/from16 v13, v17

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    add-int/lit8 v3, v18, 0x1

    move v2, v14

    .line 569
    :goto_1
    add-int/lit8 v17, v17, 0x1

    move v14, v2

    move/from16 v18, v3

    goto :goto_0

    .line 593
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/task/bs;->d:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 595
    if-eq v4, v14, :cond_4

    .line 596
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    move v12, v4

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v4

    .line 601
    :goto_2
    new-instance v6, Lcom/android/calendar/task/an;

    const/4 v7, 0x0

    const/4 v8, -0x1

    const-string v9, ""

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-wide/from16 v10, v22

    move-wide/from16 v12, v24

    move/from16 v16, v4

    invoke-direct/range {v6 .. v17}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v3, v18

    goto :goto_1

    .line 605
    :cond_2
    if-lez v18, :cond_3

    .line 606
    const/4 v2, 0x0

    :goto_3
    add-int/lit8 v3, v18, 0x1

    if-ge v2, v3, :cond_3

    .line 607
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 608
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 606
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 612
    :cond_3
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    .line 613
    return-void

    :cond_4
    move v2, v14

    goto :goto_2
.end method

.method public e(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 227
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->getItemViewType(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->getItemViewType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1139
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    move v0, v1

    .line 1153
    :goto_0
    return v0

    .line 1143
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1144
    if-lt p1, v0, :cond_3

    move v0, v1

    .line 1145
    goto :goto_0

    .line 1147
    :cond_2
    add-int/lit8 p1, p1, -0x1

    :cond_3
    if-ltz p1, :cond_4

    .line 1148
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 1149
    iget v2, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1150
    iget v0, v0, Lcom/android/calendar/task/an;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1153
    goto :goto_0
.end method

.method public f(Lcom/android/calendar/task/bs;)V
    .locals 26

    .prologue
    .line 619
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    move-object/from16 v17, v0

    .line 620
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 621
    const/4 v4, 0x0

    .line 622
    const/4 v3, -0x1

    .line 628
    new-instance v19, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 629
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 630
    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 631
    move-object/from16 v0, v19

    iget-wide v8, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/ai;->d:I

    .line 633
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v20

    .line 635
    const/4 v2, 0x0

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    :goto_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 638
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 641
    const/16 v2, 0xb

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v24, v0

    .line 644
    const/4 v2, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 645
    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 646
    move-object/from16 v0, v19

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 648
    const/4 v3, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 649
    if-nez v16, :cond_0

    .line 650
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/task/ai;->d:I

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    :cond_0
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/task/ai;->d:I

    move-wide/from16 v6, v22

    move-wide/from16 v8, v24

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 654
    add-int/lit8 v16, v16, 0x1

    move v3, v15

    move/from16 v4, v16

    .line 635
    :goto_1
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    goto :goto_0

    .line 659
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/task/bs;->d:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 661
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 663
    invoke-virtual {v3, v12}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 665
    iget v2, v3, Landroid/text/format/Time;->weekDay:I

    .line 666
    sub-int v2, v2, v20

    .line 667
    if-eqz v2, :cond_3

    .line 668
    if-gez v2, :cond_2

    .line 669
    add-int/lit8 v2, v2, 0x7

    .line 671
    :cond_2
    iget v4, v3, Landroid/text/format/Time;->monthDay:I

    sub-int v2, v4, v2

    iput v2, v3, Landroid/text/format/Time;->monthDay:I

    .line 674
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 675
    iget-wide v2, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    .line 677
    add-int/lit8 v2, v4, 0x6

    if-gt v12, v2, :cond_5

    .line 678
    if-eq v4, v15, :cond_4

    .line 679
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v15, v4

    .line 684
    :cond_4
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v22

    move-wide/from16 v8, v24

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    move v3, v15

    move/from16 v4, v16

    goto :goto_1

    .line 689
    :cond_6
    if-lez v16, :cond_7

    .line 690
    const/4 v2, 0x0

    :goto_2
    add-int/lit8 v3, v16, 0x1

    if-ge v2, v3, :cond_7

    .line 691
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 692
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 690
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 696
    :cond_7
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    .line 697
    return-void
.end method

.method public g(I)I
    .locals 2

    .prologue
    .line 1164
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    if-ltz p1, :cond_1

    .line 1165
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 1166
    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    if-nez v1, :cond_0

    .line 1167
    iget v0, v0, Lcom/android/calendar/task/an;->i:I

    .line 1178
    :goto_0
    return v0

    .line 1169
    :cond_0
    add-int/lit8 v0, p1, 0x1

    .line 1170
    iget-object v1, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1171
    invoke-virtual {p0, v0}, Lcom/android/calendar/task/ai;->g(I)I

    move-result v0

    .line 1172
    if-ltz v0, :cond_1

    .line 1173
    neg-int v0, v0

    goto :goto_0

    .line 1178
    :cond_1
    const/high16 v0, -0x80000000

    goto :goto_0
.end method

.method public g(Lcom/android/calendar/task/bs;)V
    .locals 24

    .prologue
    .line 703
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    move-object/from16 v17, v0

    .line 704
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 705
    const/4 v4, 0x0

    .line 706
    const/4 v3, -0x1

    .line 712
    new-instance v19, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/ai;->f:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 713
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 714
    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 715
    move-object/from16 v0, v19

    iget-wide v8, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/ai;->d:I

    .line 717
    const/4 v2, 0x0

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    :goto_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 720
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 723
    const/16 v2, 0xb

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v22, v0

    .line 726
    const/4 v2, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 727
    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 728
    move-object/from16 v0, v19

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 730
    const/4 v3, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 731
    if-nez v16, :cond_0

    .line 732
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/task/ai;->d:I

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    :cond_0
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/task/ai;->d:I

    move-wide/from16 v6, v20

    move-wide/from16 v8, v22

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 736
    add-int/lit8 v16, v16, 0x1

    move v3, v15

    move/from16 v4, v16

    .line 717
    :goto_1
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    goto :goto_0

    .line 741
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/task/bs;->d:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 743
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 745
    invoke-virtual {v2, v12}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 746
    const/4 v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 747
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 748
    iget-wide v6, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    .line 750
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v2

    .line 752
    add-int/2addr v2, v4

    if-gt v12, v2, :cond_3

    .line 753
    if-eq v4, v15, :cond_2

    .line 754
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const-string v5, ""

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v15, v4

    .line 759
    :cond_2
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v6, v20

    move-wide/from16 v8, v22

    move v13, v14

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move v3, v15

    move/from16 v4, v16

    goto :goto_1

    .line 764
    :cond_4
    if-lez v16, :cond_5

    .line 765
    const/4 v2, 0x0

    :goto_2
    add-int/lit8 v3, v16, 0x1

    if-ge v2, v3, :cond_5

    .line 766
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 767
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 765
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 771
    :cond_5
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    .line 772
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 146
    :goto_0
    return v0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    invoke-virtual {v0}, Lcom/android/calendar/task/ac;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 152
    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-object v0

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    iget v0, v0, Lcom/android/calendar/task/an;->i:I

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/ac;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ac;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 164
    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 165
    :cond_0
    neg-int v0, p1

    int-to-long v0, v0

    .line 170
    :goto_0
    return-wide v0

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    iget v0, v0, Lcom/android/calendar/task/an;->i:I

    invoke-virtual {v1, v0}, Lcom/android/calendar/task/ac;->getItemId(I)J

    move-result-wide v0

    goto :goto_0

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/ac;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    const/16 v4, 0x8

    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 231
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 234
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/task/ac;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 368
    :goto_0
    return-object v2

    .line 235
    :catch_0
    move-exception v0

    .line 236
    const-string v0, "TaskByDayAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Day adapter couldn\'t move cursor to position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/calendar/task/ai;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mRowInfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    new-instance v2, Landroid/view/View;

    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 244
    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    if-eq v1, v10, :cond_2

    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    if-ne v1, v7, :cond_5

    .line 245
    :cond_2
    instance-of v1, p2, Landroid/widget/LinearLayout;

    if-nez v1, :cond_11

    move-object v3, v2

    .line 251
    :goto_1
    if-eqz v3, :cond_10

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 254
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 255
    instance-of v6, v1, Lcom/android/calendar/task/ao;

    if-eqz v6, :cond_10

    .line 257
    check-cast v1, Lcom/android/calendar/task/ao;

    move-object v2, v1

    move-object v1, v3

    .line 261
    :goto_2
    if-nez v2, :cond_f

    .line 264
    new-instance v3, Lcom/android/calendar/task/ao;

    invoke-direct {v3}, Lcom/android/calendar/task/ao;-><init>()V

    .line 265
    iget-object v1, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    const v2, 0x7f0400ad

    invoke-static {v1, v2, p3, v5}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 266
    const v1, 0x7f1202cf

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v3, Lcom/android/calendar/task/ao;->a:Landroid/widget/ImageView;

    .line 267
    iget-object v1, v3, Lcom/android/calendar/task/ao;->a:Landroid/widget/ImageView;

    const/high16 v6, 0x9000000

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setId(I)V

    .line 268
    const v1, 0x7f12002a

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/android/calendar/task/ao;->b:Landroid/widget/TextView;

    .line 269
    invoke-virtual {v2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 272
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    instance-of v1, v1, Lcom/android/calendar/task/TaskDeleteActivity;

    if-nez v1, :cond_3

    .line 273
    iget-boolean v1, p0, Lcom/android/calendar/task/ai;->j:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1, v3}, Lcom/android/calendar/task/ai;->a(Ljava/lang/Boolean;Lcom/android/calendar/task/ao;)V

    .line 293
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/an;

    iget v6, v1, Lcom/android/calendar/task/an;->b:I

    iget-object v1, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/an;

    iget-object v1, v1, Lcom/android/calendar/task/an;->c:Ljava/lang/String;

    invoke-virtual {p0, v6, v1}, Lcom/android/calendar/task/ai;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 294
    iget-object v6, v3, Lcom/android/calendar/task/ao;->b:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    iget-object v1, v3, Lcom/android/calendar/task/ao;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 296
    iget-object v1, v3, Lcom/android/calendar/task/ao;->b:Landroid/widget/TextView;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 298
    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    if-ne v0, v7, :cond_4

    .line 299
    iget-object v0, v3, Lcom/android/calendar/task/ao;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 304
    :goto_4
    const v0, 0x7f020192

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 301
    :cond_4
    iget-object v0, v3, Lcom/android/calendar/task/ao;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 306
    :cond_5
    iget v1, v0, Lcom/android/calendar/task/an;->a:I

    if-nez v1, :cond_e

    .line 307
    instance-of v1, p2, Landroid/widget/RelativeLayout;

    if-nez v1, :cond_6

    move-object p2, v2

    .line 311
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/task/ai;->b:Lcom/android/calendar/task/ac;

    iget v3, v0, Lcom/android/calendar/task/an;->i:I

    invoke-virtual {v1, v3, p2, p3}, Lcom/android/calendar/task/ac;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 312
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/ah;

    .line 314
    invoke-virtual {p0, p1}, Lcom/android/calendar/task/ai;->d(I)I

    move-result v6

    invoke-virtual {p0, v3, v6}, Lcom/android/calendar/task/ai;->a(Landroid/view/View;I)V

    .line 316
    iget-object v6, v1, Lcom/android/calendar/task/ah;->f:Landroid/widget/CheckBox;

    .line 317
    new-instance v1, Lcom/android/calendar/task/am;

    iget-wide v8, v0, Lcom/android/calendar/task/an;->g:J

    invoke-direct {v1, p0, v8, v9, v2}, Lcom/android/calendar/task/am;-><init>(Lcom/android/calendar/task/ai;JLcom/android/calendar/task/aj;)V

    invoke-virtual {v6, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 320
    const v0, 0x7f1202ed

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 321
    add-int/lit8 v0, p1, 0x1

    iget-object v2, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_a

    .line 322
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 323
    if-eqz v0, :cond_9

    iget v2, v0, Lcom/android/calendar/task/an;->a:I

    if-eq v2, v10, :cond_7

    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    if-ne v0, v10, :cond_9

    :cond_7
    move v0, v4

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 330
    :goto_6
    const v0, 0x7f1202ea

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 331
    const v1, 0x7f1201e3

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 333
    iget-object v2, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    instance-of v2, v2, Lcom/android/calendar/task/TaskDeleteActivity;

    if-eqz v2, :cond_d

    .line 334
    invoke-virtual {v6, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 335
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 336
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/android/calendar/task/ai;->a:Landroid/content/Context;

    check-cast v0, Lcom/android/calendar/task/TaskDeleteActivity;

    invoke-virtual {v0}, Lcom/android/calendar/task/TaskDeleteActivity;->b()Lcom/android/calendar/task/au;

    move-result-object v2

    .line 344
    if-eqz v2, :cond_8

    .line 345
    invoke-virtual {v2}, Lcom/android/calendar/task/au;->e()Ljava/util/HashMap;

    move-result-object v0

    .line 346
    invoke-virtual {v2, p1}, Lcom/android/calendar/task/au;->d(I)J

    move-result-wide v6

    .line 348
    if-eqz v0, :cond_8

    const-wide/16 v8, -0x1

    cmp-long v4, v6, v8

    if-eqz v4, :cond_8

    .line 349
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 350
    if-eqz v0, :cond_b

    .line 351
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_8
    :goto_7
    move-object v2, v3

    .line 368
    goto/16 :goto_0

    :cond_9
    move v0, v5

    .line 323
    goto :goto_5

    .line 327
    :cond_a
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 353
    :cond_b
    invoke-virtual {v2}, Lcom/android/calendar/task/au;->f()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 354
    invoke-virtual {v1, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_7

    .line 356
    :cond_c
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_7

    .line 362
    :cond_d
    invoke-virtual {v6, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 363
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 364
    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_7

    .line 371
    :cond_e
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown event type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_f
    move-object v3, v2

    move-object v2, v1

    goto/16 :goto_3

    :cond_10
    move-object v1, v2

    goto/16 :goto_2

    :cond_11
    move-object v3, p2

    goto/16 :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x3

    return v0
.end method

.method public h(I)I
    .locals 2

    .prologue
    .line 1250
    iget-object v1, p0, Lcom/android/calendar/task/ai;->i:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1251
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1252
    monitor-exit v1

    .line 1254
    if-nez v0, :cond_0

    .line 1255
    const/4 v0, 0x0

    .line 1257
    :goto_0
    return v0

    .line 1252
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1257
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public h(Lcom/android/calendar/task/bs;)V
    .locals 22

    .prologue
    .line 778
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/calendar/task/bs;->b:Landroid/database/Cursor;

    move-object/from16 v17, v0

    .line 779
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 780
    const-wide/16 v14, -0x1

    .line 784
    const-string v2, ""

    .line 789
    const/4 v2, 0x0

    move/from16 v16, v2

    move-wide v2, v14

    :goto_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 792
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 800
    const/16 v4, 0x9

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 801
    const/16 v4, 0xd

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 807
    cmp-long v4, v14, v2

    if-eqz v4, :cond_1

    .line 808
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 813
    :goto_1
    const/16 v2, 0xb

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v8, v2

    .line 816
    const/16 v2, 0xe

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 819
    const/16 v2, 0xc

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 822
    new-instance v2, Lcom/android/calendar/task/an;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    const/4 v12, 0x0

    move-wide/from16 v6, v20

    move/from16 v13, v16

    invoke-direct/range {v2 .. v13}, Lcom/android/calendar/task/an;-><init>(IILjava/lang/String;JJIIII)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 789
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    move-wide v2, v14

    goto :goto_0

    .line 824
    :cond_0
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    .line 825
    return-void

    :cond_1
    move-wide v14, v2

    goto :goto_1
.end method

.method public i(I)I
    .locals 2

    .prologue
    .line 1280
    iget-object v1, p0, Lcom/android/calendar/task/ai;->h:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1281
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/task/ai;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1282
    monitor-exit v1

    .line 1284
    if-nez v0, :cond_0

    .line 1285
    const/4 v0, 0x0

    .line 1287
    :goto_0
    return v0

    .line 1282
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1287
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1188
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1189
    iget-object v0, p0, Lcom/android/calendar/task/ai;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/an;

    .line 1190
    iget v0, v0, Lcom/android/calendar/task/an;->a:I

    if-nez v0, :cond_0

    move v0, v1

    .line 1192
    :goto_0
    return v0

    .line 1190
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1192
    goto :goto_0
.end method
