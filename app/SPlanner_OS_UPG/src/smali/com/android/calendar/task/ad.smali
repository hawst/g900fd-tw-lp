.class Lcom/android/calendar/task/ad;
.super Ljava/lang/Object;
.source "TaskAdapter.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/android/calendar/task/ac;


# direct methods
.method constructor <init>(Lcom/android/calendar/task/ac;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/android/calendar/task/ad;->b:Lcom/android/calendar/task/ac;

    iput-object p2, p0, Lcom/android/calendar/task/ad;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    const v2, 0x7f120043

    .line 123
    iget-object v0, p0, Lcom/android/calendar/task/ad;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 124
    iget-object v0, p0, Lcom/android/calendar/task/ad;->b:Lcom/android/calendar/task/ac;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/calendar/task/ac;->c:Z

    .line 125
    iget-object v0, p0, Lcom/android/calendar/task/ad;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 126
    iget-object v1, p0, Lcom/android/calendar/task/ad;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sput v1, Lcom/android/calendar/task/ac;->b:I

    .line 127
    iget-object v1, p0, Lcom/android/calendar/task/ad;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 128
    iget-object v1, p0, Lcom/android/calendar/task/ad;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    return-void
.end method
