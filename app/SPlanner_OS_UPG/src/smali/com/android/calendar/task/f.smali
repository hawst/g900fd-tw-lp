.class public Lcom/android/calendar/task/f;
.super Landroid/app/Fragment;
.source "GroupCreateFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# static fields
.field public static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static c:I

.field private static d:I

.field private static e:I


# instance fields
.field private f:Landroid/view/View;

.field private g:Landroid/app/Activity;

.field private h:Lcom/android/calendar/ag;

.field private i:Landroid/database/Cursor;

.field private j:Landroid/view/inputmethod/InputMethodManager;

.field private k:J

.field private l:Ljava/lang/String;

.field private m:J

.field private n:J

.field private o:Ljava/util/HashSet;

.field private p:Landroid/widget/EditText;

.field private q:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/task/f;->a:Landroid/net/Uri;

    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_accountId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "groupName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "selected"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "group_order"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/task/f;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 66
    iput-object v2, p0, Lcom/android/calendar/task/f;->f:Landroid/view/View;

    .line 72
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/task/f;->k:J

    .line 73
    iput-object v2, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    .line 74
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/android/calendar/task/f;->m:J

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/task/f;->n:J

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/f;->o:Ljava/util/HashSet;

    .line 78
    iput-object v2, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    .line 82
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 66
    iput-object v2, p0, Lcom/android/calendar/task/f;->f:Landroid/view/View;

    .line 72
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/task/f;->k:J

    .line 73
    iput-object v2, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    .line 74
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/android/calendar/task/f;->m:J

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/task/f;->n:J

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/f;->o:Ljava/util/HashSet;

    .line 78
    iput-object v2, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    .line 85
    iput-wide p1, p0, Lcom/android/calendar/task/f;->k:J

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/f;J)J
    .locals 1

    .prologue
    .line 46
    iput-wide p1, p0, Lcom/android/calendar/task/f;->n:J

    return-wide p1
.end method

.method static synthetic a(Lcom/android/calendar/task/f;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/android/calendar/task/f;->i:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/f;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/task/f;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/task/f;->o:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/task/f;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/task/f;->i:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/task/f;)J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/android/calendar/task/f;->k:J

    return-wide v0
.end method

.method static synthetic d(Lcom/android/calendar/task/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/task/f;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/task/f;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/task/f;->g:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 186
    iget-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    sget v0, Lcom/android/calendar/task/f;->e:I

    invoke-static {v0}, Lcom/android/calendar/ag;->a(I)I

    .line 188
    iget-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v0

    sput v0, Lcom/android/calendar/task/f;->e:I

    .line 189
    iget-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    sget v1, Lcom/android/calendar/task/f;->e:I

    sget-object v3, Lcom/android/calendar/task/f;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/task/f;->b:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 201
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 202
    invoke-virtual {p0}, Lcom/android/calendar/task/f;->a()V

    .line 204
    :cond_0
    return-void
.end method

.method public b()V
    .locals 10

    .prologue
    .line 207
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 209
    iget-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    .line 214
    :cond_1
    const-string v0, "groupName"

    iget-object v1, p0, Lcom/android/calendar/task/f;->l:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-wide v0, p0, Lcom/android/calendar/task/f;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 217
    const-string v0, "_accountId"

    iget-wide v2, p0, Lcom/android/calendar/task/f;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 218
    const-string v0, "selected"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 219
    const-string v0, "group_order"

    iget-wide v2, p0, Lcom/android/calendar/task/f;->n:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 221
    iget-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v0

    sput v0, Lcom/android/calendar/task/f;->c:I

    .line 222
    iget-object v1, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    sget v2, Lcom/android/calendar/task/f;->c:I

    const/4 v3, 0x0

    sget-object v4, Lcom/android/calendar/task/f;->a:Landroid/net/Uri;

    const-wide/16 v6, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;J)V

    .line 226
    iget-object v0, p0, Lcom/android/calendar/task/f;->g:Landroid/app/Activity;

    const v1, 0x7f0f021c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 233
    :goto_0
    return-void

    .line 228
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v0

    sput v0, Lcom/android/calendar/task/f;->d:I

    .line 229
    sget-object v0, Lcom/android/calendar/task/f;->a:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/calendar/task/f;->k:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 230
    iget-object v1, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    sget v2, Lcom/android/calendar/task/f;->d:I

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 231
    iget-object v0, p0, Lcom/android/calendar/task/f;->g:Landroid/app/Activity;

    const v1, 0x7f0f021f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/android/calendar/task/f;->j:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 241
    iget-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 243
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 248
    const/4 v0, 0x0

    .line 250
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 255
    iget-object v0, p0, Lcom/android/calendar/task/f;->o:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 256
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, 0x1

    .line 259
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 196
    const-wide/16 v0, 0x80

    return-wide v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 169
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 172
    iget-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v0

    sput v0, Lcom/android/calendar/task/f;->e:I

    .line 173
    iget-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    sget v1, Lcom/android/calendar/task/f;->e:I

    sget-object v3, Lcom/android/calendar/task/f;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/task/f;->b:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 91
    iput-object p1, p0, Lcom/android/calendar/task/f;->g:Landroid/app/Activity;

    .line 92
    iget-object v0, p0, Lcom/android/calendar/task/f;->g:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/calendar/task/f;->j:Landroid/view/inputmethod/InputMethodManager;

    .line 93
    new-instance v0, Lcom/android/calendar/task/g;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/task/g;-><init>(Lcom/android/calendar/task/f;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/task/f;->h:Lcom/android/calendar/ag;

    .line 122
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 137
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 141
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 142
    new-instance v1, Lcom/android/calendar/en;

    iget-object v0, p0, Lcom/android/calendar/task/f;->g:Landroid/app/Activity;

    invoke-direct {v1, v0}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    .line 143
    const v0, 0x7f04005a

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/f;->f:Landroid/view/View;

    .line 144
    iget-object v0, p0, Lcom/android/calendar/task/f;->f:Landroid/view/View;

    const v2, 0x7f1201df

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    .line 145
    iget-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 146
    iget-object v0, p0, Lcom/android/calendar/task/f;->p:Landroid/widget/EditText;

    new-instance v1, Lcom/android/calendar/task/h;

    invoke-direct {v1, p0}, Lcom/android/calendar/task/h;-><init>(Lcom/android/calendar/task/f;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 162
    iget-object v0, p0, Lcom/android/calendar/task/f;->f:Landroid/view/View;

    const v1, 0x7f120018

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/f;->q:Landroid/view/View;

    .line 164
    iget-object v0, p0, Lcom/android/calendar/task/f;->f:Landroid/view/View;

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 127
    iget-object v0, p0, Lcom/android/calendar/task/f;->i:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/android/calendar/task/f;->i:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/task/f;->i:Landroid/database/Cursor;

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/task/f;->c()V

    .line 132
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 180
    iget-object v0, p0, Lcom/android/calendar/task/f;->g:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v0

    .line 181
    iget-object v1, p0, Lcom/android/calendar/task/f;->q:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 182
    return-void
.end method
