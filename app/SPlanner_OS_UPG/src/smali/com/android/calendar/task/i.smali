.class public Lcom/android/calendar/task/i;
.super Landroid/widget/BaseAdapter;
.source "GroupDeleteAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field public a:Ljava/util/ArrayList;

.field private b:Landroid/database/Cursor;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/content/Context;

.field private h:Ljava/util/HashMap;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/i;->a:Ljava/util/ArrayList;

    .line 38
    iput v1, p0, Lcom/android/calendar/task/i;->c:I

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/i;->h:Ljava/util/HashMap;

    .line 45
    iput-boolean v1, p0, Lcom/android/calendar/task/i;->i:Z

    .line 60
    iput-object p1, p0, Lcom/android/calendar/task/i;->g:Landroid/content/Context;

    .line 61
    invoke-direct {p0, p2}, Lcom/android/calendar/task/i;->b(Landroid/database/Cursor;)V

    .line 62
    return-void
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 7

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/task/i;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/i;->b:Landroid/database/Cursor;

    if-eq p1, v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/android/calendar/task/i;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 68
    :cond_0
    if-nez p1, :cond_1

    .line 69
    iput-object p1, p0, Lcom/android/calendar/task/i;->b:Landroid/database/Cursor;

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/task/i;->c:I

    .line 71
    iget-object v0, p0, Lcom/android/calendar/task/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 94
    :goto_0
    return-void

    .line 75
    :cond_1
    iput-object p1, p0, Lcom/android/calendar/task/i;->b:Landroid/database/Cursor;

    .line 76
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/task/i;->d:I

    .line 77
    const-string v0, "groupName"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/task/i;->e:I

    .line 78
    const-string v0, "_accountId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/task/i;->f:I

    .line 80
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 82
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 84
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    iget v0, p0, Lcom/android/calendar/task/i;->d:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 86
    iget v0, p0, Lcom/android/calendar/task/i;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 87
    iget v0, p0, Lcom/android/calendar/task/i;->f:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 89
    new-instance v0, Lcom/android/calendar/task/j;

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/task/j;-><init>(JLjava/lang/String;J)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 92
    :cond_2
    iput-object v6, p0, Lcom/android/calendar/task/i;->a:Ljava/util/ArrayList;

    .line 93
    iget-object v0, p0, Lcom/android/calendar/task/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/task/i;->c:I

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/android/calendar/task/i;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/calendar/task/i;->b(Landroid/database/Cursor;)V

    .line 98
    invoke-virtual {p0}, Lcom/android/calendar/task/i;->notifyDataSetChanged()V

    .line 99
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/android/calendar/task/i;->h:Ljava/util/HashMap;

    .line 160
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/android/calendar/task/i;->i:Z

    .line 168
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/android/calendar/task/i;->i:Z

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/android/calendar/task/i;->c:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/android/calendar/task/i;->c:I

    if-lt p1, v0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 139
    :goto_0
    return-object v0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/j;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 143
    iget v0, p0, Lcom/android/calendar/task/i;->c:I

    if-lt p1, v0, :cond_0

    .line 144
    const-wide/16 v0, 0x0

    .line 146
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/j;

    iget-wide v0, v0, Lcom/android/calendar/task/j;->a:J

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 102
    iget v0, p0, Lcom/android/calendar/task/i;->c:I

    if-lt p1, v0, :cond_0

    .line 103
    const/4 p2, 0x0

    .line 127
    :goto_0
    return-object p2

    .line 107
    :cond_0
    if-nez p2, :cond_1

    .line 108
    iget-object v0, p0, Lcom/android/calendar/task/i;->g:Landroid/content/Context;

    const v1, 0x7f04005f

    const/4 v2, 0x0

    invoke-static {v0, v1, p3, v2}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/j;

    iget-object v1, v0, Lcom/android/calendar/task/j;->b:Ljava/lang/String;

    .line 115
    const v0, 0x7f1201df

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 116
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    const v0, 0x7f1201e3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 119
    iget-object v2, p0, Lcom/android/calendar/task/i;->h:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/calendar/task/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/j;

    iget-wide v4, v1, Lcom/android/calendar/task/j;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 120
    if-eqz v1, :cond_2

    .line 121
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 126
    :goto_1
    invoke-virtual {p2}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 123
    :cond_2
    iget-boolean v1, p0, Lcom/android/calendar/task/i;->i:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x1

    return v0
.end method
