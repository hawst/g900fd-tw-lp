.class public Lcom/android/calendar/task/s;
.super Landroid/widget/BaseAdapter;
.source "GroupDisplayAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field public a:Ljava/util/ArrayList;

.field private c:Landroid/content/Context;

.field private d:Landroid/content/res/Resources;

.field private e:Landroid/database/Cursor;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_sync_account"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_sync_account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "selected"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/task/s;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/task/s;->f:I

    .line 86
    invoke-direct {p0, p2}, Lcom/android/calendar/task/s;->b(Landroid/database/Cursor;)V

    .line 87
    iput-object p1, p0, Lcom/android/calendar/task/s;->c:Landroid/content/Context;

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/task/s;->d:Landroid/content/res/Resources;

    .line 89
    return-void
.end method

.method private static a(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 236
    const-string v6, ""

    .line 238
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_2

    .line 239
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/task/s;->b:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id asc"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 243
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 244
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    const-string v0, "displayName"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 246
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, v6

    .line 250
    :goto_0
    if-eqz v1, :cond_0

    .line 251
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 253
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v6

    goto :goto_0

    :cond_2
    move-object v0, v6

    goto :goto_1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 375
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-ge v1, v0, :cond_1

    .line 376
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 377
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->d(Lcom/android/calendar/task/t;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/calendar/task/s;->a(J)V

    .line 375
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 380
    :cond_1
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 354
    const/4 v3, 0x1

    move v1, v2

    .line 356
    :goto_0
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-ge v1, v0, :cond_3

    .line 357
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->d(Lcom/android/calendar/task/t;)J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    if-nez v0, :cond_1

    .line 359
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->c(Lcom/android/calendar/task/t;)Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 366
    :goto_1
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-ge v2, v0, :cond_2

    .line 367
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->d(Lcom/android/calendar/task/t;)J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 369
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0, v1}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;Z)Z

    .line 366
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 356
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 372
    :cond_2
    return-void

    :cond_3
    move v1, v3

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;J)I
    .locals 7

    .prologue
    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 263
    const/4 v6, 0x1

    .line 265
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_2

    .line 266
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/task/s;->b:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id asc"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 270
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 271
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    const-string v0, "selected"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 273
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move v0, v6

    .line 277
    :goto_0
    if-eqz v1, :cond_0

    .line 278
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1
.end method

.method private b(II)V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v0, v1}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;Z)Z

    .line 338
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->d(Lcom/android/calendar/task/t;)J

    move-result-wide v0

    .line 339
    invoke-direct {p0, v0, v1}, Lcom/android/calendar/task/s;->a(J)V

    .line 340
    return-void

    .line 336
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 20

    .prologue
    .line 92
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/s;->e:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/s;->e:Landroid/database/Cursor;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_0

    .line 93
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/s;->e:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 95
    :cond_0
    if-nez p1, :cond_1

    .line 96
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/s;->e:Landroid/database/Cursor;

    .line 97
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/s;->f:I

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 139
    :goto_0
    return-void

    .line 103
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/s;->e:Landroid/database/Cursor;

    .line 104
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/s;->g:I

    .line 105
    const-string v2, "groupName"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/s;->h:I

    .line 107
    const-string v2, "selected"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/s;->i:I

    .line 108
    const-string v2, "_accountId"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/s;->j:I

    .line 110
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 112
    const/4 v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 113
    const-wide/16 v2, -0x1

    .line 115
    :goto_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 116
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/task/s;->g:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 117
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/task/s;->h:I

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 118
    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/task/s;->j:I

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 119
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/task/s;->i:I

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-eqz v9, :cond_2

    const/4 v9, 0x1

    move/from16 v18, v9

    .line 121
    :goto_2
    cmp-long v9, v7, v2

    if-eqz v9, :cond_4

    .line 123
    new-instance v2, Lcom/android/calendar/task/t;

    const/4 v3, 0x3

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/android/calendar/task/t;-><init>(IJLjava/lang/String;JZ)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v10, Lcom/android/calendar/task/t;

    const/4 v11, 0x2

    const/4 v14, 0x0

    const/16 v17, 0x0

    move-wide v12, v4

    move-wide v15, v7

    invoke-direct/range {v10 .. v17}, Lcom/android/calendar/task/t;-><init>(IJLjava/lang/String;JZ)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v2, Lcom/android/calendar/task/t;

    const/4 v3, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/android/calendar/task/t;-><init>(IJLjava/lang/String;JZ)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-wide v10, v7

    .line 132
    :goto_3
    new-instance v2, Lcom/android/calendar/task/t;

    const/4 v3, 0x0

    move/from16 v9, v18

    invoke-direct/range {v2 .. v9}, Lcom/android/calendar/task/t;-><init>(IJLjava/lang/String;JZ)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-wide v2, v10

    .line 133
    goto :goto_1

    .line 119
    :cond_2
    const/4 v9, 0x0

    move/from16 v18, v9

    goto :goto_2

    .line 135
    :cond_3
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/task/s;->f:I

    .line 138
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/task/s;->a()V

    goto/16 :goto_0

    :cond_4
    move-wide v10, v2

    goto :goto_3
.end method

.method private c(II)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 343
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->d(Lcom/android/calendar/task/t;)J

    move-result-wide v4

    move v1, v2

    .line 345
    :goto_0
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-ge v1, v0, :cond_3

    .line 346
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->d(Lcom/android/calendar/task/t;)J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    if-eqz p2, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v0, v3}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;Z)Z

    .line 345
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v3, v2

    .line 348
    goto :goto_1

    .line 351
    :cond_3
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-lt p1, v0, :cond_0

    .line 311
    const/4 v0, 0x0

    .line 313
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->b(Lcom/android/calendar/task/t;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->d(Lcom/android/calendar/task/t;)J

    move-result-wide v0

    .line 258
    invoke-static {p1, v0, v1}, Lcom/android/calendar/task/s;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    .line 326
    if-nez v0, :cond_1

    .line 327
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/task/s;->b(II)V

    .line 332
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/task/s;->notifyDataSetChanged()V

    .line 333
    return-void

    .line 328
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 329
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/task/s;->c(II)V

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/android/calendar/task/s;->b(Landroid/database/Cursor;)V

    .line 143
    invoke-virtual {p0}, Lcom/android/calendar/task/s;->notifyDataSetChanged()V

    .line 144
    return-void
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-lt p1, v0, :cond_0

    .line 318
    const/4 v0, 0x0

    .line 320
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;I)I
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->d(Lcom/android/calendar/task/t;)J

    move-result-wide v0

    .line 285
    invoke-static {p1, v0, v1}, Lcom/android/calendar/task/s;->b(Landroid/content/Context;J)I

    move-result v0

    return v0
.end method

.method public c(I)I
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->c(Lcom/android/calendar/task/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-lt p1, v0, :cond_0

    .line 296
    const/4 v0, 0x0

    .line 299
    :goto_0
    return-object v0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 303
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-lt p1, v0, :cond_0

    .line 304
    const-wide/16 v0, 0x0

    .line 306
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->e(Lcom/android/calendar/task/t;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 147
    iget v0, p0, Lcom/android/calendar/task/s;->f:I

    if-lt p1, v0, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 227
    :goto_0
    return-object v0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    if-ne v0, v7, :cond_6

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/task/s;->c:Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, Lcom/android/calendar/task/s;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 162
    iget-object v0, p0, Lcom/android/calendar/task/s;->c:Landroid/content/Context;

    const v1, 0x7f04005e

    invoke-static {v0, v1, p3, v5}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 163
    const v0, 0x7f12002d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 164
    iget-object v1, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/t;

    invoke-static {v1}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v1

    if-ne v1, v4, :cond_4

    .line 165
    if-eqz v3, :cond_3

    const-string v1, "My Task"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    iget-object v1, p0, Lcom/android/calendar/task/s;->d:Landroid/content/res/Resources;

    const v3, 0x7f0f02c0

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    move-object v0, v2

    .line 178
    goto :goto_0

    .line 168
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/task/s;->d:Landroid/content/res/Resources;

    const v4, 0x7f0f0221

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 170
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/t;

    invoke-static {v1}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 171
    if-eqz v3, :cond_5

    const-string v1, "My Task"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 172
    iget-object v1, p0, Lcom/android/calendar/task/s;->d:Landroid/content/res/Resources;

    const v3, 0x7f0f02bf

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 174
    :cond_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 180
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    if-ne v0, v6, :cond_d

    .line 182
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->b(Lcom/android/calendar/task/t;)Ljava/lang/String;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_e

    const-string v1, "Default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 184
    iget-object v0, p0, Lcom/android/calendar/task/s;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 186
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->c(Lcom/android/calendar/task/t;)Z

    move-result v4

    .line 197
    iget-object v0, p0, Lcom/android/calendar/task/s;->c:Landroid/content/Context;

    const v1, 0x7f04005f

    invoke-static {v0, v1, p3, v5}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 198
    const v0, 0x7f1201df

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 199
    iget-object v1, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/t;

    invoke-static {v1}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v1

    if-ne v1, v6, :cond_c

    .line 200
    iget-object v1, p0, Lcom/android/calendar/task/s;->d:Landroid/content/res/Resources;

    const v2, 0x7f0f03b8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    :cond_8
    :goto_3
    const v0, 0x7f1201e3

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 210
    if-eqz v0, :cond_9

    .line 211
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 215
    :cond_9
    iget-object v1, p0, Lcom/android/calendar/task/s;->c:Landroid/content/Context;

    invoke-virtual {p0, v1, p1}, Lcom/android/calendar/task/s;->b(Landroid/content/Context;I)I

    move-result v1

    if-nez v1, :cond_b

    .line 216
    if-eqz v0, :cond_a

    .line 218
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 219
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 220
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 221
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setLongClickable(Z)V

    .line 223
    :cond_a
    invoke-virtual {v3, v5}, Landroid/view/View;->setClickable(Z)V

    .line 226
    :cond_b
    invoke-virtual {v3}, Landroid/view/View;->invalidate()V

    move-object v0, v3

    .line 227
    goto/16 :goto_0

    .line 201
    :cond_c
    iget-object v1, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/t;

    invoke-static {v1}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v1

    if-nez v1, :cond_8

    .line 202
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 230
    :cond_d
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown group type:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/android/calendar/task/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/t;

    invoke-static {v0}, Lcom/android/calendar/task/t;->a(Lcom/android/calendar/task/t;)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    move-object v2, v0

    goto/16 :goto_2
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x1

    return v0
.end method
