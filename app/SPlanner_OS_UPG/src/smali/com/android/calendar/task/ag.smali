.class Lcom/android/calendar/task/ag;
.super Ljava/lang/Object;
.source "TaskAdapter.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/task/ac;


# direct methods
.method private constructor <init>(Lcom/android/calendar/task/ac;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/android/calendar/task/ag;->a:Lcom/android/calendar/task/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/task/ac;Lcom/android/calendar/task/ad;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/android/calendar/task/ag;-><init>(Lcom/android/calendar/task/ac;)V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 323
    const v0, 0x7f12002d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 324
    const v1, 0x7f120155

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 325
    const v2, 0x7f1201df

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 327
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    :cond_0
    move v0, v3

    .line 358
    :goto_0
    return v0

    .line 331
    :cond_1
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/widget/TextView;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v1}, Lcom/android/calendar/hj;->a(Landroid/widget/TextView;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v2}, Lcom/android/calendar/hj;->a(Landroid/widget/TextView;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 332
    :cond_2
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 334
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    .line 335
    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 336
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    .line 337
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v1, ""

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 338
    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 339
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    .line 342
    :cond_3
    invoke-virtual {p2, v5}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v1, p0, Lcom/android/calendar/task/ag;->a:Lcom/android/calendar/task/ac;

    invoke-static {v1}, Lcom/android/calendar/task/ac;->a(Lcom/android/calendar/task/ac;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 344
    sget-object v2, Lcom/android/calendar/hj;->r:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/task/ag;->a:Lcom/android/calendar/task/ac;

    invoke-static {v1}, Lcom/android/calendar/task/ac;->a(Lcom/android/calendar/task/ac;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-static {v2, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 347
    :cond_4
    invoke-virtual {p2}, Landroid/widget/HoverPopupWindow;->getContent()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 348
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 349
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v6, 0x21

    invoke-interface {v2, v5, v3, v0, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 351
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v4

    .line 353
    goto :goto_0

    .line 355
    :cond_5
    invoke-virtual {p2}, Landroid/widget/HoverPopupWindow;->dismiss()V

    move v0, v3

    .line 358
    goto/16 :goto_0
.end method
