.class public Lcom/android/calendar/task/TaskDeleteActivity;
.super Lcom/android/calendar/b;
.source "TaskDeleteActivity.java"


# instance fields
.field private b:Lcom/android/calendar/task/az;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    invoke-virtual {v0, p1}, Lcom/android/calendar/task/az;->a(Z)V

    .line 135
    return-void
.end method

.method public b()Lcom/android/calendar/task/au;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    invoke-virtual {v0}, Lcom/android/calendar/task/az;->f()Lcom/android/calendar/task/au;

    move-result-object v0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 116
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteActivity;->invalidateOptionsMenu()V

    .line 117
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f12006b

    .line 40
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskDeleteActivity;->setContentView(I)V

    .line 44
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/az;

    iput-object v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    .line 46
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/android/calendar/task/az;

    invoke-direct {v0}, Lcom/android/calendar/task/az;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    .line 49
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 51
    iget-object v1, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 52
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 55
    :cond_0
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->c:Z

    .line 56
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f1200d4

    const/4 v4, 0x4

    const/4 v3, 0x1

    const v2, 0x7f1200d5

    .line 66
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 67
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 68
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0f014a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 69
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 70
    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->c:Z

    if-eqz v0, :cond_1

    .line 71
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02005c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 72
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020057

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 77
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/task/TaskDeleteActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 79
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    invoke-virtual {v0}, Lcom/android/calendar/task/az;->e()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/task/TaskDeleteActivity;->a(I)V

    .line 80
    return v3

    .line 74
    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 75
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Lcom/android/calendar/b;->onDestroy()V

    .line 61
    invoke-static {p0}, Lcom/android/calendar/al;->b(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 99
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 110
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    .line 102
    :sswitch_0
    invoke-static {p0}, Lcom/android/calendar/hj;->p(Landroid/content/Context;)V

    .line 103
    const/4 v0, 0x1

    goto :goto_1

    .line 105
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    invoke-virtual {v0}, Lcom/android/calendar/task/az;->c()V

    goto :goto_0

    .line 99
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f1200d4 -> :sswitch_0
        0x7f1200d5 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 85
    const v0, 0x7f1200d5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 87
    const/4 v0, 0x0

    .line 88
    iget-object v2, p0, Lcom/android/calendar/task/TaskDeleteActivity;->b:Lcom/android/calendar/task/az;

    invoke-virtual {v2}, Lcom/android/calendar/task/az;->e()I

    move-result v2

    .line 89
    if-lez v2, :cond_0

    .line 90
    const/4 v0, 0x1

    .line 92
    :cond_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 94
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 122
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 127
    return-void
.end method
