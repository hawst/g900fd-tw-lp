.class public Lcom/android/calendar/task/x;
.super Landroid/app/DialogFragment;
.source "GroupDisplayFragment.java"


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 269
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 272
    iput-wide p1, p0, Lcom/android/calendar/task/x;->a:J

    .line 273
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/task/x;)J
    .locals 2

    .prologue
    .line 264
    iget-wide v0, p0, Lcom/android/calendar/task/x;->a:J

    return-wide v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/android/calendar/task/x;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 278
    if-eqz p1, :cond_0

    .line 279
    const-string v1, "groupId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    const-string v1, "groupId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/task/x;->a:J

    .line 284
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 285
    const v0, 0x7f0f0146

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 287
    invoke-virtual {p0}, Lcom/android/calendar/task/x;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0147

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f02e3

    new-instance v3, Lcom/android/calendar/task/z;

    invoke-direct {v3, p0}, Lcom/android/calendar/task/z;-><init>(Lcom/android/calendar/task/x;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/task/y;

    invoke-direct {v3, p0}, Lcom/android/calendar/task/y;-><init>(Lcom/android/calendar/task/x;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 301
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 306
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 307
    const-string v0, "groupId"

    iget-wide v2, p0, Lcom/android/calendar/task/x;->a:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 308
    return-void
.end method
