.class public Lcom/android/calendar/hi;
.super Landroid/content/AsyncQueryHandler;
.source "TodayEventCounter.java"


# instance fields
.field private a:Landroid/app/Activity;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 50
    iput-object p2, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    .line 51
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/hi;->cancelOperation(I)V

    .line 60
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/calendar/hi;->cancelOperation(I)V

    .line 61
    iput v1, p0, Lcom/android/calendar/hi;->b:I

    .line 62
    iput v1, p0, Lcom/android/calendar/hi;->c:I

    .line 63
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 66
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 68
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/android/calendar/hi;->e()Landroid/net/Uri;

    move-result-object v3

    invoke-static {}, Lcom/android/calendar/dh;->j()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/android/calendar/hi;->d()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/hi;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    const-string v0, "visible=1"

    .line 76
    iget-object v1, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v1

    .line 77
    if-eqz v1, :cond_0

    .line 78
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    return-object v0
.end method

.method private e()Landroid/net/Uri;
    .locals 5

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    .line 88
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v2

    .line 89
    if-eqz v2, :cond_1

    .line 90
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 94
    :goto_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 95
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 96
    if-eqz v2, :cond_0

    .line 97
    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 98
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 100
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 101
    const-wide/32 v2, 0x5265c00

    add-long/2addr v2, v0

    .line 102
    sget-object v4, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 103
    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 104
    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 105
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 92
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v1, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 110
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    .line 111
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v4

    .line 112
    if-eqz v4, :cond_1

    .line 113
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 117
    :goto_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 118
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 119
    if-eqz v4, :cond_0

    .line 120
    iput-object v3, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 121
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 123
    :cond_0
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 124
    const-wide/32 v6, 0x5265c00

    add-long/2addr v6, v4

    .line 125
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-static {v0, v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v5

    .line 127
    sget-object v3, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    .line 128
    invoke-virtual {p0, v1}, Lcom/android/calendar/hi;->cancelOperation(I)V

    .line 129
    sget-object v4, Lcom/android/calendar/hc;->o:[Ljava/lang/String;

    move-object v0, p0

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/hi;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void

    .line 115
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/android/calendar/hi;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 134
    return-void
.end method

.method private h()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 138
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    .line 139
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v2

    .line 140
    if-eqz v2, :cond_2

    .line 141
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 145
    :goto_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 146
    if-eqz v2, :cond_0

    .line 147
    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 148
    invoke-virtual {v0, v8}, Landroid/text/format/Time;->normalize(Z)J

    .line 150
    :cond_0
    invoke-virtual {v0, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    const/4 v6, 0x4

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    .line 153
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    iget v1, p0, Lcom/android/calendar/hi;->b:I

    if-nez v1, :cond_3

    iget v1, p0, Lcom/android/calendar/hi;->c:I

    if-nez v1, :cond_3

    .line 155
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    iget-object v1, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    const v2, 0x7f0f02d2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 143
    :cond_2
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 159
    iget v2, p0, Lcom/android/calendar/hi;->b:I

    if-lez v2, :cond_4

    .line 160
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    const v2, 0x7f0e0001

    iget v3, p0, Lcom/android/calendar/hi;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/calendar/hi;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 162
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    :cond_4
    iget v2, p0, Lcom/android/calendar/hi;->c:I

    if-lez v2, :cond_1

    .line 165
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    const v2, 0x7f0e0003

    iget v3, p0, Lcom/android/calendar/hi;->c:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/calendar/hi;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/calendar/hi;->b()V

    .line 55
    invoke-direct {p0}, Lcom/android/calendar/hi;->c()V

    .line 56
    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1, p2, p3}, Landroid/content/AsyncQueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 176
    if-nez p3, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_5

    .line 181
    :try_start_0
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hi;->b:I

    .line 182
    iget-object v0, p0, Lcom/android/calendar/hi;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 183
    invoke-direct {p0}, Lcom/android/calendar/hi;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    :cond_2
    :goto_1
    if-eqz p3, :cond_0

    .line 193
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 185
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/android/calendar/hi;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 192
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_4

    .line 193
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 187
    :cond_5
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 188
    :try_start_2
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hi;->c:I

    .line 189
    invoke-direct {p0}, Lcom/android/calendar/hi;->g()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
