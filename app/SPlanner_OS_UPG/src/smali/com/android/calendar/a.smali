.class public abstract Lcom/android/calendar/a;
.super Landroid/app/Activity;
.source "AbstractAllInOneActivity.java"


# instance fields
.field protected a:Landroid/view/View;

.field protected b:Landroid/widget/ImageView;

.field protected c:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 45
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/android/calendar/a;->d()V

    .line 32
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    .line 33
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 34
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->bringChildToFront(Landroid/view/View;)V

    .line 39
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 67
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 94
    :cond_0
    return-void
.end method

.method public a(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 61
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 50
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->bringChildToFront(Landroid/view/View;)V

    .line 52
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 88
    :cond_0
    return-void
.end method

.method public b(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/android/calendar/a;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 82
    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->bringChildToFront(Landroid/view/View;)V

    .line 73
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/android/calendar/a;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 100
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/android/calendar/a;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 25
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 26
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 28
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/android/calendar/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    .line 107
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/a;->d()V

    .line 108
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 109
    return-void
.end method
