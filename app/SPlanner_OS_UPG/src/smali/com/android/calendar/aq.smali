.class public Lcom/android/calendar/aq;
.super Ljava/lang/Object;
.source "CalendarController.java"


# instance fields
.field public a:J

.field public b:I

.field public c:J

.field public d:Landroid/text/format/Time;

.field public e:Landroid/text/format/Time;

.field public f:Landroid/text/format/Time;

.field public g:Ljava/util/Calendar;

.field public h:Ljava/util/Calendar;

.field public i:I

.field public j:I

.field public k:Ljava/lang/String;

.field public l:Landroid/content/ComponentName;

.field public m:Z

.field public n:I

.field public o:Ljava/lang/String;

.field public p:J

.field public q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/aq;->o:Ljava/lang/String;

    return-void
.end method

.method public static a(IZ)J
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    .line 339
    if-eqz p1, :cond_0

    const-wide/16 v0, 0x100

    .line 341
    :goto_0
    packed-switch p0, :pswitch_data_0

    .line 355
    :pswitch_0
    const-string v2, "CalendarController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown attendee response "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    or-long/2addr v0, v6

    .line 359
    :goto_1
    return-wide v0

    .line 339
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 343
    :pswitch_1
    or-long/2addr v0, v6

    .line 344
    goto :goto_1

    .line 346
    :pswitch_2
    const-wide/16 v2, 0x2

    or-long/2addr v0, v2

    .line 347
    goto :goto_1

    .line 349
    :pswitch_3
    const-wide/16 v2, 0x4

    or-long/2addr v0, v2

    .line 350
    goto :goto_1

    .line 352
    :pswitch_4
    const-wide/16 v2, 0x8

    or-long/2addr v0, v2

    .line 353
    goto :goto_1

    .line 341
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 316
    iget-wide v2, p0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 317
    const-string v1, "CalendarController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "illegal call to getResponse , wrong event type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/android/calendar/aq;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :goto_0
    :pswitch_0
    return v0

    .line 321
    :cond_0
    iget-wide v2, p0, Lcom/android/calendar/aq;->p:J

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v1, v2

    .line 322
    packed-switch v1, :pswitch_data_0

    .line 332
    :pswitch_1
    const-string v2, "CalendarController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown attendee response "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 326
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 328
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 330
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
