.class public Lcom/android/calendar/h/c;
.super Ljava/lang/Object;
.source "WeatherManager.java"


# static fields
.field private static g:Lcom/android/calendar/h/c;

.field private static r:Landroid/text/format/Time;

.field private static s:Landroid/text/format/Time;

.field private static t:I

.field private static final w:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lcom/android/calendar/h/b;

.field private j:Lcom/android/calendar/h/a;

.field private k:Landroid/util/SparseIntArray;

.field private l:Landroid/util/SparseArray;

.field private m:Ljava/lang/ref/SoftReference;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:I

.field private final u:[Ljava/lang/String;

.field private v:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/h/c;->g:Lcom/android/calendar/h/c;

    .line 56
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/h/c;->t:I

    .line 202
    const-string v0, "Location=\"%s\""

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "cityId:current"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/h/c;->w:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "accuweather"

    iput-object v0, p0, Lcom/android/calendar/h/c;->h:Ljava/lang/String;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/h/c;->q:I

    .line 58
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TODAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ONEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "TWODAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "THREEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "FOURDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "FIVEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SIXDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "TODAY_DATE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "TODAY_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "TEMP_SCALE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "TODAY_WEATHER_TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "NAME"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "TODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "TODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ONEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ONEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "TWODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "TWODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "THREEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "THREEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "FOURDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "FOURDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "FIVEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "FIVEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "SIXDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "SIXDAY_LOW_TEMP"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/calendar/h/c;->u:[Ljava/lang/String;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/h/c;->v:Ljava/util/ArrayList;

    .line 127
    invoke-direct {p0}, Lcom/android/calendar/h/c;->h()V

    .line 128
    return-void
.end method

.method private static a(II)I
    .locals 1

    .prologue
    .line 400
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 410
    :cond_0
    :goto_0
    return p1

    .line 403
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 404
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 405
    invoke-static {v0}, Lcom/android/calendar/h/c;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 407
    if-ne p0, v0, :cond_0

    .line 408
    add-int/lit8 p1, p1, 0x64

    goto :goto_0
.end method

.method public static a(Landroid/text/format/Time;)I
    .locals 4

    .prologue
    .line 309
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-wide v2, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    return v0
.end method

.method private a(Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 184
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/calendar/h/c;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 185
    const-string v1, "START"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 186
    const-string v1, "PACKAGE"

    const-string v2, "com.android.calendar"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    const-string v1, "CP"

    iget-object v2, p0, Lcom/android/calendar/h/c;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    return-object v0
.end method

.method private a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 392
    invoke-direct {p0, p2}, Lcom/android/calendar/h/c;->b(I)I

    move-result v0

    .line 393
    const-string v1, "Cmaweather"

    iget-object v2, p0, Lcom/android/calendar/h/c;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 394
    invoke-static {p2, v0}, Lcom/android/calendar/h/c;->a(II)I

    move-result v0

    .line 396
    :cond_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/android/calendar/h/c;->b(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/h/c;Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/h/c;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a()Lcom/android/calendar/h/c;
    .locals 2

    .prologue
    .line 116
    const-class v1, Lcom/android/calendar/h/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/calendar/h/c;->g:Lcom/android/calendar/h/c;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lcom/android/calendar/h/c;

    invoke-direct {v0}, Lcom/android/calendar/h/c;-><init>()V

    sput-object v0, Lcom/android/calendar/h/c;->g:Lcom/android/calendar/h/c;

    .line 119
    :cond_0
    sget-object v0, Lcom/android/calendar/h/c;->g:Lcom/android/calendar/h/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/database/Cursor;II)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v5, 0xc

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 290
    if-ne p1, v5, :cond_0

    .line 291
    const/16 p1, 0x8

    .line 294
    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 295
    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "\u2109"

    .line 297
    :goto_0
    const-string v2, "999"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "%d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "%d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 305
    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 295
    :cond_2
    const-string v0, "\u2103"

    goto :goto_0

    .line 301
    :cond_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 302
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 303
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v4, "%d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v1, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/h/c;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/calendar/h/c;->v:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 231
    if-nez p1, :cond_0

    .line 248
    :goto_0
    return-void

    .line 236
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 237
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 239
    invoke-direct {p0, p1}, Lcom/android/calendar/h/c;->c(Landroid/database/Cursor;)V

    .line 240
    invoke-direct {p0, p1}, Lcom/android/calendar/h/c;->d(Landroid/database/Cursor;)V

    .line 241
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/h/c;->q:I

    .line 242
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/h/c;->o:Ljava/lang/String;

    .line 243
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/h/c;->p:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic a(Lcom/android/calendar/h/c;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/android/calendar/h/c;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private static a(I)Z
    .locals 2

    .prologue
    .line 343
    invoke-static {}, Lcom/android/calendar/h/c;->i()I

    move-result v0

    .line 344
    sub-int v0, p0, v0

    .line 345
    if-ltz v0, :cond_0

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 385
    iget-object v1, p0, Lcom/android/calendar/h/c;->k:Landroid/util/SparseIntArray;

    if-nez v1, :cond_0

    .line 388
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/calendar/h/c;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;)I
    .locals 4

    .prologue
    .line 251
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 252
    const/4 v1, 0x7

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 254
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 255
    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 257
    invoke-static {v0}, Lcom/android/calendar/h/c;->a(Landroid/text/format/Time;)I

    move-result v0

    return v0
.end method

.method private b(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 414
    invoke-direct {p0}, Lcom/android/calendar/h/c;->l()Lcom/android/calendar/h/d;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/h/d;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 150
    invoke-static {}, Lcom/android/calendar/dz;->y()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "accuweather"

    :cond_0
    return-object v0
.end method

.method private c(I)I
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/android/calendar/h/c;->j:Lcom/android/calendar/h/a;

    invoke-virtual {v0, p1}, Lcom/android/calendar/h/a;->a(I)I

    move-result v0

    return v0
.end method

.method private c(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 425
    invoke-direct {p0, p2}, Lcom/android/calendar/h/c;->b(I)I

    move-result v0

    .line 426
    const-string v1, "Cmaweather"

    iget-object v2, p0, Lcom/android/calendar/h/c;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 427
    invoke-static {p2, v0}, Lcom/android/calendar/h/c;->a(II)I

    move-result v0

    .line 429
    :cond_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/android/calendar/h/c;->f(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 261
    invoke-static {p1}, Lcom/android/calendar/h/c;->b(Landroid/database/Cursor;)I

    move-result v1

    .line 262
    iget-object v0, p0, Lcom/android/calendar/h/c;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 267
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_0

    .line 268
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 269
    iget-object v4, p0, Lcom/android/calendar/h/c;->k:Landroid/util/SparseIntArray;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v4, v1, v3}, Landroid/util/SparseIntArray;->append(II)V

    .line 267
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 271
    :cond_0
    return-void
.end method

.method private d(I)I
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/android/calendar/h/c;->j:Lcom/android/calendar/h/a;

    invoke-virtual {v0, p1}, Lcom/android/calendar/h/a;->b(I)I

    move-result v0

    return v0
.end method

.method private static d(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 433
    if-nez p0, :cond_0

    .line 434
    const/4 v0, 0x0

    .line 436
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 274
    invoke-static {p1}, Lcom/android/calendar/h/c;->b(Landroid/database/Cursor;)I

    move-result v1

    .line 275
    iget-object v0, p0, Lcom/android/calendar/h/c;->l:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 280
    const/16 v0, 0xc

    :goto_0
    const/16 v2, 0x18

    if-gt v0, v2, :cond_0

    .line 281
    add-int/lit8 v2, v0, 0x1

    invoke-static {p1, v0, v2}, Lcom/android/calendar/h/c;->a(Landroid/database/Cursor;II)Ljava/lang/String;

    move-result-object v3

    .line 282
    iget-object v4, p0, Lcom/android/calendar/h/c;->l:Landroid/util/SparseArray;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v4, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 280
    add-int/lit8 v0, v0, 0x2

    move v1, v2

    goto :goto_0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/h/c;->l:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/h/c;->n:Ljava/lang/String;

    .line 287
    return-void
.end method

.method private e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 440
    invoke-direct {p0, p2}, Lcom/android/calendar/h/c;->c(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/android/calendar/h/c;->d(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private f(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 444
    invoke-direct {p0, p2}, Lcom/android/calendar/h/c;->d(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/android/calendar/h/c;->d(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static g()Z
    .locals 2

    .prologue
    .line 456
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getHours()I

    move-result v0

    mul-int/lit8 v0, v0, 0x64

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getMinutes()I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    const/16 v1, 0x23a

    if-lt v0, v1, :cond_0

    const/16 v1, 0x726

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 131
    invoke-static {}, Lcom/android/calendar/h/c;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/h/c;->h:Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/android/calendar/h/c;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/h/g;->a(Ljava/lang/String;)Lcom/android/calendar/h/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/h/c;->i:Lcom/android/calendar/h/b;

    .line 133
    iget-object v0, p0, Lcom/android/calendar/h/c;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/h/g;->b(Ljava/lang/String;)Lcom/android/calendar/h/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/h/c;->j:Lcom/android/calendar/h/a;

    .line 136
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/h/c;->k:Landroid/util/SparseIntArray;

    .line 137
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/h/c;->l:Landroid/util/SparseArray;

    .line 139
    iget-object v0, p0, Lcom/android/calendar/h/c;->i:Lcom/android/calendar/h/b;

    iget-object v0, v0, Lcom/android/calendar/h/b;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/h/c;->a:Ljava/lang/String;

    .line 140
    iget-object v0, p0, Lcom/android/calendar/h/c;->i:Lcom/android/calendar/h/b;

    iget-object v0, v0, Lcom/android/calendar/h/b;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/h/c;->b:Ljava/lang/String;

    .line 141
    iget-object v0, p0, Lcom/android/calendar/h/c;->i:Lcom/android/calendar/h/b;

    iget-object v0, v0, Lcom/android/calendar/h/b;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/h/c;->c:Ljava/lang/String;

    .line 142
    iget-object v0, p0, Lcom/android/calendar/h/c;->i:Lcom/android/calendar/h/b;

    iget-object v0, v0, Lcom/android/calendar/h/b;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/h/c;->d:Ljava/lang/String;

    .line 143
    iget-object v0, p0, Lcom/android/calendar/h/c;->i:Lcom/android/calendar/h/b;

    iget-object v0, v0, Lcom/android/calendar/h/b;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/h/c;->e:Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lcom/android/calendar/h/c;->i:Lcom/android/calendar/h/b;

    iget-object v0, v0, Lcom/android/calendar/h/b;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/h/c;->f:Ljava/lang/String;

    .line 146
    invoke-static {}, Lcom/android/calendar/h/c;->k()V

    .line 147
    return-void
.end method

.method private static i()I
    .locals 1

    .prologue
    .line 349
    invoke-static {}, Lcom/android/calendar/h/c;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 350
    sget-object v0, Lcom/android/calendar/h/c;->r:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 351
    sget-object v0, Lcom/android/calendar/h/c;->r:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/h/c;->a(Landroid/text/format/Time;)I

    move-result v0

    sput v0, Lcom/android/calendar/h/c;->t:I

    .line 353
    :cond_0
    sget v0, Lcom/android/calendar/h/c;->t:I

    return v0
.end method

.method private static j()Z
    .locals 2

    .prologue
    .line 357
    sget-object v0, Lcom/android/calendar/h/c;->s:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 358
    sget v0, Lcom/android/calendar/h/c;->t:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/android/calendar/h/c;->s:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    sget-object v1, Lcom/android/calendar/h/c;->r:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/calendar/h/c;->s:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->month:I

    sget-object v1, Lcom/android/calendar/h/c;->r:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/calendar/h/c;->s:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    sget-object v1, Lcom/android/calendar/h/c;->r:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    if-ne v0, v1, :cond_0

    .line 362
    const/4 v0, 0x1

    .line 364
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static k()V
    .locals 1

    .prologue
    .line 368
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    sput-object v0, Lcom/android/calendar/h/c;->r:Landroid/text/format/Time;

    .line 369
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    sput-object v0, Lcom/android/calendar/h/c;->s:Landroid/text/format/Time;

    .line 370
    sget-object v0, Lcom/android/calendar/h/c;->r:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 371
    sget-object v0, Lcom/android/calendar/h/c;->r:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/h/c;->a(Landroid/text/format/Time;)I

    move-result v0

    sput v0, Lcom/android/calendar/h/c;->t:I

    .line 372
    return-void
.end method

.method private declared-synchronized l()Lcom/android/calendar/h/d;
    .locals 2

    .prologue
    .line 418
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/h/c;->m:Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/h/c;->m:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 419
    :cond_0
    new-instance v0, Ljava/lang/ref/SoftReference;

    new-instance v1, Lcom/android/calendar/h/d;

    invoke-direct {v1, p0}, Lcom/android/calendar/h/d;-><init>(Lcom/android/calendar/h/c;)V

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/calendar/h/c;->m:Ljava/lang/ref/SoftReference;

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/h/c;->m:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/h/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 418
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;Landroid/text/format/Time;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 375
    invoke-static {p2}, Lcom/android/calendar/h/c;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 376
    invoke-static {v0}, Lcom/android/calendar/h/c;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/h/c;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 160
    const-string v0, "WeatherManager"

    const-string v1, "startCurrentLocationWeatherDataService"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    if-nez p1, :cond_0

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/h/c;->a(Z)Landroid/content/Intent;

    move-result-object v0

    .line 167
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/h/f;)V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/android/calendar/h/c;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 194
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/h/f;

    .line 195
    if-ne v0, p1, :cond_0

    .line 200
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/h/c;->v:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Landroid/content/res/Resources;Landroid/text/format/Time;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 380
    invoke-static {p2}, Lcom/android/calendar/h/c;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 381
    invoke-static {v0}, Lcom/android/calendar/h/c;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/h/c;->c(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/text/format/Time;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 321
    invoke-static {p1}, Lcom/android/calendar/h/c;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 322
    invoke-static {v0}, Lcom/android/calendar/h/c;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/h/c;->l:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 204
    if-nez p1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    new-instance v0, Lcom/android/calendar/h/e;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/h/e;-><init>(Lcom/android/calendar/h/c;Landroid/content/ContentResolver;)V

    .line 209
    const/4 v1, -0x1

    iget-object v3, p0, Lcom/android/calendar/h/c;->i:Lcom/android/calendar/h/b;

    iget-object v3, v3, Lcom/android/calendar/h/b;->g:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/calendar/h/c;->u:[Ljava/lang/String;

    sget-object v5, Lcom/android/calendar/h/c;->w:Ljava/lang/String;

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/h/e;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/android/calendar/h/c;->j:Lcom/android/calendar/h/a;

    invoke-virtual {v0}, Lcom/android/calendar/h/a;->a()I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/android/calendar/h/c;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/h/c;->n:Ljava/lang/String;

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/android/calendar/h/c;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/h/c;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/android/calendar/h/c;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/h/c;->p:Ljava/lang/String;

    goto :goto_0
.end method
