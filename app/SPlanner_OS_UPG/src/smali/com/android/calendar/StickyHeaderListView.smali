.class public Lcom/android/calendar/StickyHeaderListView;
.super Landroid/widget/FrameLayout;
.source "StickyHeaderListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field protected a:Z

.field protected b:Z

.field protected c:Landroid/content/Context;

.field protected d:Landroid/widget/Adapter;

.field protected e:Lcom/android/calendar/hb;

.field protected f:Landroid/view/View;

.field protected g:Landroid/view/View;

.field protected h:Landroid/widget/ListView;

.field protected i:Landroid/widget/AbsListView$OnScrollListener;

.field protected j:I

.field protected k:I

.field protected l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 174
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput-boolean v1, p0, Lcom/android/calendar/StickyHeaderListView;->a:Z

    .line 53
    iput-boolean v1, p0, Lcom/android/calendar/StickyHeaderListView;->b:Z

    .line 55
    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->c:Landroid/content/Context;

    .line 56
    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->d:Landroid/widget/Adapter;

    .line 57
    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->e:Lcom/android/calendar/hb;

    .line 58
    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    .line 59
    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->g:Landroid/view/View;

    .line 60
    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    .line 61
    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->i:Landroid/widget/AbsListView$OnScrollListener;

    .line 67
    iput v2, p0, Lcom/android/calendar/StickyHeaderListView;->j:I

    .line 69
    iput v2, p0, Lcom/android/calendar/StickyHeaderListView;->k:I

    .line 70
    iput v1, p0, Lcom/android/calendar/StickyHeaderListView;->l:I

    .line 175
    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->c:Landroid/content/Context;

    .line 178
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 307
    invoke-virtual {p0}, Lcom/android/calendar/StickyHeaderListView;->getChildCount()I

    move-result v3

    move v1, v2

    .line 308
    :goto_0
    if-ge v1, v3, :cond_1

    .line 309
    invoke-virtual {p0, v1}, Lcom/android/calendar/StickyHeaderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 310
    instance-of v4, v0, Landroid/widget/ListView;

    if-eqz v4, :cond_0

    .line 311
    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/android/calendar/StickyHeaderListView;->setListView(Landroid/widget/ListView;)V

    .line 308
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    if-nez v0, :cond_2

    .line 317
    new-instance v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/StickyHeaderListView;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/android/calendar/StickyHeaderListView;->setListView(Landroid/widget/ListView;)V

    .line 321
    :cond_2
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/StickyHeaderListView;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->g:Landroid/view/View;

    .line 322
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/16 v3, 0x30

    invoke-direct {v0, v1, v5, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 324
    iget-object v1, p0, Lcom/android/calendar/StickyHeaderListView;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 327
    iput-boolean v5, p0, Lcom/android/calendar/StickyHeaderListView;->a:Z

    .line 328
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 216
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->d:Landroid/widget/Adapter;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    if-eqz v2, :cond_0

    .line 217
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/calendar/StickyHeaderListView;->setAdapter(Landroid/widget/Adapter;)V

    .line 220
    :cond_0
    iget v2, p0, Lcom/android/calendar/StickyHeaderListView;->l:I

    sub-int v3, p1, v2

    .line 222
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->d:Landroid/widget/Adapter;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->e:Lcom/android/calendar/hb;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/calendar/StickyHeaderListView;->b:Z

    if-nez v2, :cond_2

    .line 277
    :cond_1
    :goto_0
    return-void

    .line 227
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->e:Lcom/android/calendar/hb;

    invoke-interface {v2, v3}, Lcom/android/calendar/hb;->a(I)I

    move-result v4

    .line 230
    iget v2, p0, Lcom/android/calendar/StickyHeaderListView;->j:I

    if-eq v4, v2, :cond_8

    .line 233
    const/4 v2, -0x1

    if-ne v4, v2, :cond_5

    .line 235
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/android/calendar/StickyHeaderListView;->removeView(Landroid/view/View;)V

    .line 236
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->g:Landroid/view/View;

    iput-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    move v2, v1

    .line 251
    :goto_1
    iput v4, p0, Lcom/android/calendar/StickyHeaderListView;->j:I

    .line 252
    add-int/2addr v2, v4

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/calendar/StickyHeaderListView;->k:I

    .line 258
    :goto_2
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 259
    iget v2, p0, Lcom/android/calendar/StickyHeaderListView;->k:I

    sub-int/2addr v2, v3

    add-int/lit8 v3, v2, -0x1

    .line 260
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 261
    if-nez v2, :cond_3

    .line 262
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 264
    :cond_3
    iget-object v4, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    invoke-virtual {v4, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 265
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    if-gt v4, v2, :cond_6

    .line 266
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 267
    iget-object v4, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    sub-int v2, v3, v2

    int-to-float v2, v2

    invoke-virtual {v4, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 271
    :cond_4
    :goto_3
    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/StickyHeaderListView;->addView(Landroid/view/View;)V

    .line 274
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 240
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->e:Lcom/android/calendar/hb;

    invoke-interface {v2, v4}, Lcom/android/calendar/hb;->b(I)I

    move-result v2

    .line 241
    iget-object v5, p0, Lcom/android/calendar/StickyHeaderListView;->d:Landroid/widget/Adapter;

    iget v6, p0, Lcom/android/calendar/StickyHeaderListView;->l:I

    add-int/2addr v6, v4

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    invoke-interface {v5, v6, v7, v8}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 242
    if-eqz v5, :cond_7

    .line 243
    iget-object v6, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getWidth()I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget-object v7, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/ListView;->getHeight()I

    move-result v7

    const/high16 v8, -0x80000000

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/view/View;->measure(II)V

    .line 246
    iget-object v6, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    invoke-virtual {p0, v6}, Lcom/android/calendar/StickyHeaderListView;->removeView(Landroid/view/View;)V

    .line 247
    iput-object v5, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    goto/16 :goto_1

    .line 268
    :cond_6
    if-eqz v2, :cond_4

    .line 269
    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->f:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto/16 :goto_2
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 290
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 291
    iget-boolean v0, p0, Lcom/android/calendar/StickyHeaderListView;->a:Z

    if-nez v0, :cond_0

    .line 292
    invoke-direct {p0}, Lcom/android/calendar/StickyHeaderListView;->a()V

    .line 294
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/StickyHeaderListView;->b:Z

    .line 295
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 281
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 282
    iget-boolean v0, p0, Lcom/android/calendar/StickyHeaderListView;->a:Z

    if-nez v0, :cond_0

    .line 283
    invoke-direct {p0}, Lcom/android/calendar/StickyHeaderListView;->a()V

    .line 285
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/StickyHeaderListView;->b:Z

    .line 286
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0, p2}, Lcom/android/calendar/StickyHeaderListView;->a(I)V

    .line 208
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->i:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->i:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 211
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->i:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->i:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 191
    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 116
    if-eqz p1, :cond_0

    .line 117
    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->d:Landroid/widget/Adapter;

    .line 122
    :cond_0
    return-void
.end method

.method public setIndexer(Lcom/android/calendar/hb;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->e:Lcom/android/calendar/hb;

    .line 132
    return-void
.end method

.method public setListView(Landroid/widget/ListView;)V
    .locals 1

    .prologue
    .line 140
    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    .line 141
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 142
    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/StickyHeaderListView;->l:I

    .line 143
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->i:Landroid/widget/AbsListView$OnScrollListener;

    .line 155
    return-void
.end method
