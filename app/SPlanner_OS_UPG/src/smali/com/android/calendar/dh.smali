.class public Lcom/android/calendar/dh;
.super Ljava/lang/Object;
.source "Event.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static N:Ljava/lang/String;

.field private static O:I

.field private static R:Z

.field private static S:Ljava/util/Comparator;

.field public static final a:[Ljava/lang/String;


# instance fields
.field public A:Ljava/lang/String;

.field public B:I

.field public C:F

.field public D:F

.field public E:F

.field public F:F

.field public G:Lcom/android/calendar/dh;

.field public H:Lcom/android/calendar/dh;

.field public I:Lcom/android/calendar/dh;

.field public J:Lcom/android/calendar/dh;

.field public K:Z

.field public L:Ljava/lang/String;

.field public M:Z

.field private P:I

.field private Q:I

.field public b:J

.field public c:I

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:J

.field public n:J

.field public o:Z

.field public p:Z

.field public q:J

.field public r:J

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:I

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:J

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "displayColor"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "event_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "end"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "startDay"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "endDay"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "startMinute"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "endMinute"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "rdate"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "guestsCanModify"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "allDay=1 OR (end-begin)>=86400000 AS dispAllday"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "calendar_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "sticker_group"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "filepath"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/dh;->a:[Ljava/lang/String;

    .line 199
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/dh;->R:Z

    .line 670
    new-instance v0, Lcom/android/calendar/di;

    invoke-direct {v0}, Lcom/android/calendar/di;-><init>()V

    sput-object v0, Lcom/android/calendar/dh;->S:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/dh;->M:Z

    return-void
.end method

.method public static a(J)I
    .locals 6

    .prologue
    const/16 v1, 0x40

    .line 733
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 734
    const-wide/16 v2, 0x1

    shl-long/2addr v2, v0

    and-long/2addr v2, p0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 738
    :goto_1
    return v0

    .line 733
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 738
    goto :goto_1
.end method

.method private static a(Lcom/android/calendar/dh;Ljava/util/Iterator;J)J
    .locals 4

    .prologue
    .line 698
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 699
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 700
    iget v1, v0, Lcom/android/calendar/dh;->j:I

    iget v2, p0, Lcom/android/calendar/dh;->i:I

    if-ge v1, v2, :cond_0

    .line 701
    const-wide/16 v2, 0x1

    invoke-virtual {v0}, Lcom/android/calendar/dh;->c()I

    move-result v0

    shl-long v0, v2, v0

    const-wide/16 v2, -0x1

    xor-long/2addr v0, v2

    and-long/2addr p2, v0

    .line 702
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 705
    :cond_1
    return-wide p2
.end method

.method private static a(Lcom/android/calendar/dh;Ljava/util/Iterator;JJ)J
    .locals 8

    .prologue
    .line 715
    iget v0, p0, Lcom/android/calendar/dh;->k:I

    int-to-long v2, v0

    .line 719
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 720
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 722
    iget v1, v0, Lcom/android/calendar/dh;->l:I

    iget v4, v0, Lcom/android/calendar/dh;->k:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    const-wide/32 v6, 0xea60

    div-long v6, p2, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 724
    iget v1, v0, Lcom/android/calendar/dh;->k:I

    int-to-long v6, v1

    add-long/2addr v4, v6

    cmp-long v1, v4, v2

    if-lez v1, :cond_1

    iget v1, v0, Lcom/android/calendar/dh;->i:I

    iget v4, p0, Lcom/android/calendar/dh;->i:I

    if-eq v1, v4, :cond_0

    .line 725
    :cond_1
    const-wide/16 v4, 0x1

    invoke-virtual {v0}, Lcom/android/calendar/dh;->c()I

    move-result v0

    shl-long v0, v4, v0

    const-wide/16 v4, -0x1

    xor-long/2addr v0, v4

    and-long/2addr p4, v0

    .line 726
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 729
    :cond_2
    return-wide p4
.end method

.method private static final a(Landroid/content/ContentResolver;[Ljava/lang/String;IILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 391
    const-string v3, "visible=?"

    .line 392
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "1"

    aput-object v0, v4, v8

    .line 393
    const-string v5, "begin ASC"

    .line 395
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_BY_DAY_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 396
    int-to-long v6, p2

    invoke-static {v1, v6, v7}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 397
    int-to-long v6, p3

    invoke-static {v1, v6, v7}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 398
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    if-nez p6, :cond_2

    :goto_1
    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 402
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 403
    if-eqz p5, :cond_0

    array-length v0, p5

    if-lez v0, :cond_0

    .line 404
    array-length v0, p5

    add-int/lit8 v0, v0, 0x1

    invoke-static {p5, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 405
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v4, v4, v8

    aput-object v4, v0, v2

    move-object v4, v0

    goto :goto_0

    :cond_2
    move-object v5, p6

    .line 410
    goto :goto_1
.end method

.method public static final a()Lcom/android/calendar/dh;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 271
    new-instance v0, Lcom/android/calendar/dh;

    invoke-direct {v0}, Lcom/android/calendar/dh;-><init>()V

    .line 273
    iput-wide v4, v0, Lcom/android/calendar/dh;->b:J

    .line 274
    iput-object v2, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    .line 275
    iput v1, v0, Lcom/android/calendar/dh;->c:I

    .line 276
    iput-object v2, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    .line 277
    iput-boolean v1, v0, Lcom/android/calendar/dh;->f:Z

    .line 278
    iput v1, v0, Lcom/android/calendar/dh;->i:I

    .line 279
    iput v1, v0, Lcom/android/calendar/dh;->j:I

    .line 280
    iput v1, v0, Lcom/android/calendar/dh;->k:I

    .line 281
    iput v1, v0, Lcom/android/calendar/dh;->l:I

    .line 282
    iput-wide v4, v0, Lcom/android/calendar/dh;->m:J

    .line 283
    iput-wide v4, v0, Lcom/android/calendar/dh;->n:J

    .line 284
    iput-boolean v1, v0, Lcom/android/calendar/dh;->o:Z

    .line 285
    iput-boolean v1, v0, Lcom/android/calendar/dh;->p:Z

    .line 286
    iput-wide v4, v0, Lcom/android/calendar/dh;->q:J

    .line 287
    iput v1, v0, Lcom/android/calendar/dh;->B:I

    .line 288
    iput-wide v4, v0, Lcom/android/calendar/dh;->r:J

    .line 289
    iput-object v2, v0, Lcom/android/calendar/dh;->s:Ljava/lang/String;

    .line 290
    iput-object v2, v0, Lcom/android/calendar/dh;->t:Ljava/lang/String;

    .line 291
    iput-object v2, v0, Lcom/android/calendar/dh;->u:Ljava/lang/String;

    .line 292
    iput v1, v0, Lcom/android/calendar/dh;->v:I

    .line 293
    iput-object v2, v0, Lcom/android/calendar/dh;->w:Ljava/lang/String;

    .line 294
    iput-object v2, v0, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    .line 295
    iput-wide v4, v0, Lcom/android/calendar/dh;->y:J

    .line 296
    iput v1, v0, Lcom/android/calendar/dh;->z:I

    .line 297
    iput-object v2, v0, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    .line 298
    iput-boolean v1, v0, Lcom/android/calendar/dh;->M:Z

    .line 299
    return-object v0
.end method

.method private static a(Landroid/database/Cursor;Z)Lcom/android/calendar/dh;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v8, 0x19

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 491
    new-instance v3, Lcom/android/calendar/dh;

    invoke-direct {v3}, Lcom/android/calendar/dh;-><init>()V

    .line 493
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/android/calendar/dh;->b:J

    .line 494
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    .line 495
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    .line 496
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/android/calendar/dh;->f:Z

    .line 497
    const/16 v0, 0x11

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->g:Ljava/lang/String;

    .line 498
    const/16 v0, 0x12

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lcom/android/calendar/dh;->h:Z

    .line 499
    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->s:Ljava/lang/String;

    .line 500
    const/16 v0, 0x18

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    .line 501
    const/16 v0, 0x1a

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->L:Ljava/lang/String;

    .line 503
    iget-object v0, v3, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 504
    :cond_0
    sget-object v0, Lcom/android/calendar/dh;->N:Ljava/lang/String;

    iput-object v0, v3, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    .line 507
    :cond_1
    invoke-interface {p0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 514
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/hj;->b(I)I

    move-result v0

    iput v0, v3, Lcom/android/calendar/dh;->c:I

    .line 519
    :goto_2
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 520
    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 522
    iput-wide v4, v3, Lcom/android/calendar/dh;->m:J

    .line 523
    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lcom/android/calendar/dh;->i:I

    .line 525
    iput-wide v6, v3, Lcom/android/calendar/dh;->n:J

    .line 526
    const/16 v0, 0xa

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lcom/android/calendar/dh;->j:I

    .line 527
    if-nez p1, :cond_2

    .line 528
    const/16 v0, 0xb

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lcom/android/calendar/dh;->k:I

    .line 529
    const/16 v0, 0xc

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lcom/android/calendar/dh;->l:I

    .line 531
    :cond_2
    const/16 v0, 0xd

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    iput-boolean v0, v3, Lcom/android/calendar/dh;->o:Z

    .line 534
    const/16 v0, 0xe

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->t:Ljava/lang/String;

    .line 535
    const/16 v0, 0xf

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->u:Ljava/lang/String;

    .line 536
    iget-object v0, v3, Lcom/android/calendar/dh;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v3, Lcom/android/calendar/dh;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 537
    :cond_3
    iput-boolean v1, v3, Lcom/android/calendar/dh;->p:Z

    .line 542
    :goto_4
    const/16 v0, 0x8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/android/calendar/dh;->q:J

    .line 543
    const/16 v0, 0x10

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lcom/android/calendar/dh;->B:I

    .line 544
    const/16 v0, 0x14

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/android/calendar/dh;->r:J

    .line 545
    const/16 v0, 0x15

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lcom/android/calendar/dh;->v:I

    .line 546
    const/16 v0, 0x16

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->w:Ljava/lang/String;

    .line 548
    const/16 v0, 0x17

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v4, v10

    if-lez v0, :cond_a

    move v0, v1

    :goto_5
    iput-boolean v0, v3, Lcom/android/calendar/dh;->K:Z

    .line 549
    invoke-interface {p0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 550
    iput-wide v10, v3, Lcom/android/calendar/dh;->y:J

    .line 551
    iput v2, v3, Lcom/android/calendar/dh;->z:I

    .line 552
    const/4 v0, 0x0

    iput-object v0, v3, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    .line 560
    :goto_6
    iput-boolean v2, v3, Lcom/android/calendar/dh;->M:Z

    .line 561
    sget-boolean v0, Lcom/android/calendar/dh;->R:Z

    if-eqz v0, :cond_4

    .line 562
    const-string v0, "setLunar"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 563
    const/4 v4, -0x1

    if-eq v0, v4, :cond_4

    .line 564
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_c

    :goto_7
    iput-boolean v1, v3, Lcom/android/calendar/dh;->M:Z

    .line 568
    :cond_4
    return-object v3

    :cond_5
    move v0, v2

    .line 496
    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 498
    goto/16 :goto_1

    .line 516
    :cond_7
    sget v0, Lcom/android/calendar/dh;->O:I

    iput v0, v3, Lcom/android/calendar/dh;->c:I

    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 531
    goto/16 :goto_3

    .line 539
    :cond_9
    iput-boolean v2, v3, Lcom/android/calendar/dh;->p:Z

    goto :goto_4

    :cond_a
    move v0, v2

    .line 548
    goto :goto_5

    .line 554
    :cond_b
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/android/calendar/dh;->y:J

    .line 555
    const/16 v0, 0x1b

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lcom/android/calendar/dh;->z:I

    .line 556
    const/16 v0, 0x1c

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    goto :goto_6

    :cond_c
    move v1, v2

    .line 564
    goto :goto_7
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;IIILjava/util/concurrent/atomic/AtomicInteger;)V
    .locals 11

    .prologue
    .line 312
    const/4 v8, 0x0

    .line 313
    const/4 v7, 0x0

    .line 315
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 317
    add-int v0, p2, p3

    add-int/lit8 v3, v0, -0x1

    .line 330
    :try_start_0
    invoke-static {p0}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v2

    .line 332
    const-string v1, "dispAllday=0"

    .line 333
    const-string v0, "dispAllday=1"

    .line 334
    if-eqz v2, :cond_0

    .line 335
    const-string v2, " AND selfAttendeeStatus!=2"

    .line 337
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 338
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 340
    :cond_0
    invoke-static {p0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 341
    invoke-static {p0, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 343
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/dh;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "begin ASC, end DESC, title ASC"

    move v2, p2

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/dh;->a(Landroid/content/ContentResolver;[Ljava/lang/String;IILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 345
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/dh;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "startDay ASC, endDay DESC, title ASC"

    move v2, p2

    move-object v4, v9

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/dh;->a(Landroid/content/ContentResolver;[Ljava/lang/String;IILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 350
    :try_start_2
    invoke-virtual/range {p5 .. p5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v0

    if-eq p4, v0, :cond_3

    .line 358
    if-eqz v10, :cond_1

    .line 359
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 361
    :cond_1
    if-eqz v1, :cond_2

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 368
    :cond_2
    :goto_0
    return-void

    .line 354
    :cond_3
    const/4 v9, 0x0

    move-object v4, p1

    move-object v5, v10

    move-object v6, p0

    move v7, p2

    move v8, v3

    :try_start_3
    invoke-static/range {v4 .. v9}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;IIZ)V

    .line 355
    const/4 v9, 0x0

    move-object v4, p1

    move-object v5, v1

    move-object v6, p0

    move v7, p2

    move v8, v3

    invoke-static/range {v4 .. v9}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;IIZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 358
    if-eqz v10, :cond_4

    .line 359
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 361
    :cond_4
    if-eqz v1, :cond_2

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 358
    :catchall_0
    move-exception v0

    move-object v1, v7

    move-object v2, v8

    :goto_1
    if-eqz v2, :cond_5

    .line 359
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 361
    :cond_5
    if-eqz v1, :cond_6

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 358
    :catchall_1
    move-exception v0

    move-object v1, v7

    move-object v2, v10

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v2, v10

    goto :goto_1
.end method

.method public static a(Ljava/util/ArrayList;J)V
    .locals 1

    .prologue
    .line 587
    if-nez p0, :cond_0

    .line 594
    :goto_0
    return-void

    .line 592
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;JZ)V

    .line 593
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;JZ)V

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;JZ)V
    .locals 15

    .prologue
    .line 599
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 600
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 602
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gez v2, :cond_9

    .line 603
    const-wide/16 v4, 0x0

    .line 606
    :goto_0
    const-wide/16 v6, 0x0

    .line 607
    const/4 v3, 0x0

    .line 613
    if-eqz p3, :cond_1

    .line 620
    :goto_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v8, v3

    :cond_0
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 622
    invoke-virtual {v2}, Lcom/android/calendar/dh;->e()Z

    move-result v3

    move/from16 v0, p3

    if-ne v3, v0, :cond_0

    .line 626
    if-nez p3, :cond_2

    .line 627
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-static/range {v2 .. v7}, Lcom/android/calendar/dh;->a(Lcom/android/calendar/dh;Ljava/util/Iterator;JJ)J

    move-result-wide v6

    .line 635
    :goto_3
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 636
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/calendar/dh;

    .line 637
    invoke-virtual {v3, v8}, Lcom/android/calendar/dh;->b(I)V

    goto :goto_4

    .line 616
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 617
    sget-object v8, Lcom/android/calendar/dh;->S:Ljava/util/Comparator;

    invoke-static {v2, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object p0, v2

    goto :goto_1

    .line 630
    :cond_2
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-static {v2, v3, v6, v7}, Lcom/android/calendar/dh;->a(Lcom/android/calendar/dh;Ljava/util/Iterator;J)J

    move-result-wide v6

    goto :goto_3

    .line 639
    :cond_3
    const/4 v8, 0x0

    .line 640
    const-wide/16 v6, 0x0

    .line 641
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 646
    :cond_4
    invoke-static {v6, v7}, Lcom/android/calendar/dh;->a(J)I

    move-result v3

    .line 647
    const/16 v12, 0x40

    if-ne v3, v12, :cond_5

    .line 648
    const/16 v3, 0x3f

    .line 650
    :cond_5
    const-wide/16 v12, 0x1

    shl-long/2addr v12, v3

    or-long/2addr v6, v12

    .line 651
    invoke-virtual {v2, v3}, Lcom/android/calendar/dh;->a(I)V

    .line 652
    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 654
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 655
    if-ge v8, v2, :cond_8

    :goto_5
    move v8, v2

    .line 658
    goto :goto_2

    .line 659
    :cond_6
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 660
    invoke-virtual {v2, v8}, Lcom/android/calendar/dh;->b(I)V

    goto :goto_6

    .line 662
    :cond_7
    return-void

    :cond_8
    move v2, v8

    goto :goto_5

    :cond_9
    move-wide/from16 v4, p1

    goto/16 :goto_0
.end method

.method public static a(Ljava/util/ArrayList;Landroid/content/Context;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 458
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 484
    :goto_0
    return-void

    .line 462
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 463
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 465
    invoke-static {}, Lcom/android/calendar/dh;->a()Lcom/android/calendar/dh;

    .line 466
    invoke-static {}, Lcom/android/calendar/dh;->a()Lcom/android/calendar/dh;

    move-result-object v2

    .line 467
    add-int/lit8 v3, p2, 0x4

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 468
    const/16 v3, 0xc8

    iput v3, v2, Lcom/android/calendar/dh;->v:I

    .line 469
    const-string v3, "My Calendar"

    iput-object v3, v2, Lcom/android/calendar/dh;->L:Ljava/lang/String;

    .line 470
    const-string v3, "Local"

    iput-object v3, v2, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    .line 471
    const/16 v3, 0xb5

    const/16 v4, 0x51

    const/16 v5, 0x5d

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    iput v3, v2, Lcom/android/calendar/dh;->c:I

    .line 472
    add-int/lit8 v3, p2, 0x4

    iput v3, v2, Lcom/android/calendar/dh;->j:I

    .line 473
    const/16 v3, 0x3c0

    iput v3, v2, Lcom/android/calendar/dh;->l:I

    .line 474
    const/16 v3, 0x10

    iput v3, v0, Landroid/text/format/Time;->hour:I

    .line 475
    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/dh;->n:J

    .line 476
    add-int/lit8 v3, p2, 0x4

    iput v3, v2, Lcom/android/calendar/dh;->i:I

    .line 477
    const/16 v3, 0x30c

    iput v3, v2, Lcom/android/calendar/dh;->k:I

    .line 478
    const/16 v3, 0xd

    iput v3, v0, Landroid/text/format/Time;->hour:I

    .line 479
    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/dh;->m:J

    .line 480
    const-string v0, "UTC"

    iput-object v0, v2, Lcom/android/calendar/dh;->s:Ljava/lang/String;

    .line 481
    const v0, 0x7f0f0181

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    .line 482
    const v0, 0x7f0f0180

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    .line 483
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;IIZ)V
    .locals 2

    .prologue
    .line 425
    if-eqz p1, :cond_0

    if-nez p0, :cond_2

    .line 426
    :cond_0
    const-string v0, "CalEvent"

    const-string v1, "buildEventsFromCursor: null cursor or null events list!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    :cond_1
    :goto_0
    return-void

    .line 430
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 431
    if-eqz v0, :cond_1

    .line 435
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 436
    const v1, 0x7f0f02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/calendar/dh;->N:Ljava/lang/String;

    .line 437
    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/dh;->O:I

    .line 442
    const/4 v0, -0x1

    :try_start_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 443
    :cond_3
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 444
    invoke-static {p1, p5}, Lcom/android/calendar/dh;->a(Landroid/database/Cursor;Z)Lcom/android/calendar/dh;

    move-result-object v0

    .line 445
    iget v1, v0, Lcom/android/calendar/dh;->i:I

    if-gt v1, p4, :cond_3

    iget v1, v0, Lcom/android/calendar/dh;->j:I

    if-lt v1, p3, :cond_3

    .line 448
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 450
    :catch_0
    move-exception v0

    .line 451
    if-eqz p1, :cond_1

    .line 452
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static a(Lcom/android/calendar/dh;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 742
    iget-object v2, p0, Lcom/android/calendar/dh;->w:Ljava/lang/String;

    .line 743
    iget-object v3, p0, Lcom/android/calendar/dh;->g:Ljava/lang/String;

    .line 744
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    .line 745
    iget-boolean v4, p0, Lcom/android/calendar/dh;->h:Z

    .line 746
    iget v2, p0, Lcom/android/calendar/dh;->v:I

    const/16 v5, 0x1f4

    if-lt v2, v5, :cond_1

    move v2, v0

    .line 747
    :goto_0
    if-eqz v2, :cond_2

    if-nez v3, :cond_0

    if-eqz v4, :cond_2

    :cond_0
    invoke-static {p0}, Lcom/android/calendar/dh;->b(Lcom/android/calendar/dh;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 746
    goto :goto_0

    :cond_2
    move v0, v1

    .line 747
    goto :goto_1
.end method

.method private static a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 889
    array-length v0, p0

    .line 890
    add-int/lit8 v1, v0, 0x1

    new-array v1, v1, [Ljava/lang/String;

    .line 891
    invoke-static {p0, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 892
    const-string v2, "setLunar"

    aput-object v2, v1, v0

    .line 893
    return-object v1
.end method

.method public static b(Lcom/android/calendar/dh;)Z
    .locals 1

    .prologue
    .line 751
    iget-boolean v0, p0, Lcom/android/calendar/dh;->K:Z

    return v0
.end method

.method public static j()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 897
    sget-object v0, Lcom/android/calendar/dh;->a:[Ljava/lang/String;

    .line 898
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v1

    .line 899
    invoke-virtual {v1}, Lcom/android/calendar/d/g;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 900
    invoke-static {v0}, Lcom/android/calendar/dh;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 902
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 822
    iput p1, p0, Lcom/android/calendar/dh;->P:I

    .line 823
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 807
    iget-object v0, p0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 812
    iget-object v1, p0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 813
    iget-object v1, p0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 814
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 815
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 818
    :cond_0
    return-object v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 830
    iput p1, p0, Lcom/android/calendar/dh;->Q:I

    .line 831
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 826
    iget v0, p0, Lcom/android/calendar/dh;->P:I

    return v0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 206
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 207
    new-instance v0, Lcom/android/calendar/dh;

    invoke-direct {v0}, Lcom/android/calendar/dh;-><init>()V

    .line 209
    iget-object v1, p0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    iput-object v1, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    .line 210
    iget v1, p0, Lcom/android/calendar/dh;->c:I

    iput v1, v0, Lcom/android/calendar/dh;->c:I

    .line 211
    iget-object v1, p0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    iput-object v1, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    .line 212
    iget-boolean v1, p0, Lcom/android/calendar/dh;->f:Z

    iput-boolean v1, v0, Lcom/android/calendar/dh;->f:Z

    .line 213
    iget v1, p0, Lcom/android/calendar/dh;->i:I

    iput v1, v0, Lcom/android/calendar/dh;->i:I

    .line 214
    iget v1, p0, Lcom/android/calendar/dh;->j:I

    iput v1, v0, Lcom/android/calendar/dh;->j:I

    .line 215
    iget v1, p0, Lcom/android/calendar/dh;->k:I

    iput v1, v0, Lcom/android/calendar/dh;->k:I

    .line 216
    iget v1, p0, Lcom/android/calendar/dh;->l:I

    iput v1, v0, Lcom/android/calendar/dh;->l:I

    .line 217
    iget-wide v2, p0, Lcom/android/calendar/dh;->m:J

    iput-wide v2, v0, Lcom/android/calendar/dh;->m:J

    .line 218
    iget-wide v2, p0, Lcom/android/calendar/dh;->n:J

    iput-wide v2, v0, Lcom/android/calendar/dh;->n:J

    .line 219
    iget-boolean v1, p0, Lcom/android/calendar/dh;->o:Z

    iput-boolean v1, v0, Lcom/android/calendar/dh;->o:Z

    .line 220
    iget-boolean v1, p0, Lcom/android/calendar/dh;->p:Z

    iput-boolean v1, v0, Lcom/android/calendar/dh;->p:Z

    .line 221
    iget-wide v2, p0, Lcom/android/calendar/dh;->q:J

    iput-wide v2, v0, Lcom/android/calendar/dh;->q:J

    .line 222
    iget v1, p0, Lcom/android/calendar/dh;->B:I

    iput v1, v0, Lcom/android/calendar/dh;->B:I

    .line 223
    iget-object v1, p0, Lcom/android/calendar/dh;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/dh;->g:Ljava/lang/String;

    .line 224
    iget-boolean v1, p0, Lcom/android/calendar/dh;->h:Z

    iput-boolean v1, v0, Lcom/android/calendar/dh;->h:Z

    .line 225
    iget-wide v2, p0, Lcom/android/calendar/dh;->r:J

    iput-wide v2, v0, Lcom/android/calendar/dh;->r:J

    .line 226
    iget-object v1, p0, Lcom/android/calendar/dh;->s:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/dh;->s:Ljava/lang/String;

    .line 227
    iget-object v1, p0, Lcom/android/calendar/dh;->t:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/dh;->t:Ljava/lang/String;

    .line 228
    iget-object v1, p0, Lcom/android/calendar/dh;->u:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/dh;->u:Ljava/lang/String;

    .line 229
    iget v1, p0, Lcom/android/calendar/dh;->v:I

    iput v1, v0, Lcom/android/calendar/dh;->v:I

    .line 230
    iget-object v1, p0, Lcom/android/calendar/dh;->w:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/dh;->w:Ljava/lang/String;

    .line 231
    iget-object v1, p0, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    .line 232
    iget-wide v2, p0, Lcom/android/calendar/dh;->y:J

    iput-wide v2, v0, Lcom/android/calendar/dh;->y:J

    .line 233
    iget v1, p0, Lcom/android/calendar/dh;->z:I

    iput v1, v0, Lcom/android/calendar/dh;->z:I

    .line 234
    iget-object v1, p0, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    .line 235
    iget-boolean v1, p0, Lcom/android/calendar/dh;->M:Z

    iput-boolean v1, v0, Lcom/android/calendar/dh;->M:Z

    .line 236
    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 834
    iget v0, p0, Lcom/android/calendar/dh;->Q:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 855
    iget-boolean v0, p0, Lcom/android/calendar/dh;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/dh;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/calendar/dh;->j:I

    iget v1, p0, Lcom/android/calendar/dh;->i:I

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 4

    .prologue
    .line 859
    iget-wide v0, p0, Lcom/android/calendar/dh;->n:J

    iget-wide v2, p0, Lcom/android/calendar/dh;->m:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 863
    iget-object v0, p0, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    const-string v1, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    const-string v1, "com.sec.android.app.snsaccountfacebook.account_type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 4

    .prologue
    .line 868
    iget-wide v0, p0, Lcom/android/calendar/dh;->y:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 872
    iget v0, p0, Lcom/android/calendar/dh;->z:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
