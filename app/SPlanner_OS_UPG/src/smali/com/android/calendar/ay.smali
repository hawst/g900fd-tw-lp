.class Lcom/android/calendar/ay;
.super Ljava/lang/Object;
.source "CalendarSearchView.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/CalendarSearchView;


# direct methods
.method constructor <init>(Lcom/android/calendar/CalendarSearchView;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/android/calendar/ay;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v2

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/ay;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->a(Lcom/android/calendar/CalendarSearchView;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->d()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 111
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_2

    .line 112
    iget-object v0, p0, Lcom/android/calendar/ay;->a:Lcom/android/calendar/CalendarSearchView;

    iget-object v1, p0, Lcom/android/calendar/ay;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v1}, Lcom/android/calendar/CalendarSearchView;->b(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/CalendarSearchView;->setSelection(I)V

    goto :goto_0

    .line 115
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 116
    iget-object v0, p0, Lcom/android/calendar/ay;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/CalendarSearchView;->setSelection(I)V

    goto :goto_0
.end method
