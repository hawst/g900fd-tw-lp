.class Lcom/android/calendar/bu;
.super Landroid/app/DialogFragment;
.source "CopyEventToSolarHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/bm;

.field private b:Landroid/database/MatrixCursor;

.field private c:Lcom/android/calendar/bo;


# direct methods
.method public constructor <init>(Lcom/android/calendar/bm;Landroid/database/MatrixCursor;)V
    .locals 1

    .prologue
    .line 164
    iput-object p1, p0, Lcom/android/calendar/bu;->a:Lcom/android/calendar/bm;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 165
    iput-object p2, p0, Lcom/android/calendar/bu;->b:Landroid/database/MatrixCursor;

    .line 166
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/bu;->setRetainInstance(Z)V

    .line 167
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/bu;)Lcom/android/calendar/bo;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/bu;->c:Lcom/android/calendar/bo;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/bu;)Landroid/database/MatrixCursor;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/bu;->b:Landroid/database/MatrixCursor;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 171
    new-instance v0, Lcom/android/calendar/bo;

    invoke-virtual {p0}, Lcom/android/calendar/bu;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/bu;->b:Landroid/database/MatrixCursor;

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/bo;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/bu;->c:Lcom/android/calendar/bo;

    .line 173
    iget-object v0, p0, Lcom/android/calendar/bu;->b:Landroid/database/MatrixCursor;

    invoke-virtual {v0}, Landroid/database/MatrixCursor;->moveToFirst()Z

    .line 174
    iget-object v0, p0, Lcom/android/calendar/bu;->a:Lcom/android/calendar/bm;

    iget-object v1, p0, Lcom/android/calendar/bu;->b:Landroid/database/MatrixCursor;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/android/calendar/bm;->a(Lcom/android/calendar/bm;J)J

    .line 175
    iget-object v0, p0, Lcom/android/calendar/bu;->a:Lcom/android/calendar/bm;

    iget-object v1, p0, Lcom/android/calendar/bu;->b:Landroid/database/MatrixCursor;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/bm;->a(Lcom/android/calendar/bm;Ljava/lang/String;)Ljava/lang/String;

    .line 177
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/bu;->a:Lcom/android/calendar/bm;

    invoke-static {v1}, Lcom/android/calendar/bm;->b(Lcom/android/calendar/bm;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 178
    const v1, 0x7f0f018c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 179
    iget-object v1, p0, Lcom/android/calendar/bu;->c:Lcom/android/calendar/bo;

    invoke-static {}, Lcom/android/calendar/bm;->a()I

    move-result v2

    new-instance v3, Lcom/android/calendar/bv;

    invoke-direct {v3, p0}, Lcom/android/calendar/bv;-><init>(Lcom/android/calendar/bu;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 191
    const v1, 0x104000a

    new-instance v2, Lcom/android/calendar/bw;

    invoke-direct {v2, p0}, Lcom/android/calendar/bw;-><init>(Lcom/android/calendar/bu;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 202
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/bx;

    invoke-direct {v2, p0}, Lcom/android/calendar/bx;-><init>(Lcom/android/calendar/bu;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 209
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/android/calendar/bu;->dismiss()V

    .line 215
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 216
    return-void
.end method
