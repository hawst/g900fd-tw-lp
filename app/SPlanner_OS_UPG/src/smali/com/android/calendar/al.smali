.class public Lcom/android/calendar/al;
.super Ljava/lang/Object;
.source "CalendarController.java"


# static fields
.field private static c:Z

.field private static j:Ljava/util/WeakHashMap;


# instance fields
.field a:Landroid/os/Handler;

.field private final b:Landroid/content/Context;

.field private final d:Ljava/util/LinkedHashMap;

.field private final e:Ljava/util/LinkedList;

.field private final f:Ljava/util/LinkedHashMap;

.field private g:Landroid/util/Pair;

.field private h:Landroid/util/Pair;

.field private volatile i:I

.field private final k:Ljava/util/WeakHashMap;

.field private l:I

.field private m:I

.field private n:I

.field private o:J

.field private final p:Landroid/text/format/Time;

.field private q:J

.field private r:I

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Landroid/widget/Toast;

.field private final w:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/android/calendar/al;->j:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/al;->a:Landroid/os/Handler;

    .line 98
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/al;->d:Ljava/util/LinkedHashMap;

    .line 100
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/al;->e:Ljava/util/LinkedList;

    .line 101
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/al;->f:Ljava/util/LinkedHashMap;

    .line 105
    iput v3, p0, Lcom/android/calendar/al;->i:I

    .line 110
    new-instance v0, Ljava/util/WeakHashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/WeakHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/al;->k:Ljava/util/WeakHashMap;

    .line 112
    iput v2, p0, Lcom/android/calendar/al;->l:I

    .line 113
    iput v2, p0, Lcom/android/calendar/al;->m:I

    .line 114
    iput v2, p0, Lcom/android/calendar/al;->n:I

    .line 115
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/al;->o:J

    .line 116
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    .line 117
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/al;->q:J

    .line 118
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/al;->r:I

    .line 119
    iput v4, p0, Lcom/android/calendar/al;->s:I

    .line 120
    iput-boolean v3, p0, Lcom/android/calendar/al;->t:Z

    .line 121
    iput-boolean v3, p0, Lcom/android/calendar/al;->u:Z

    .line 124
    new-instance v0, Lcom/android/calendar/am;

    invoke-direct {v0, p0}, Lcom/android/calendar/am;-><init>(Lcom/android/calendar/al;)V

    iput-object v0, p0, Lcom/android/calendar/al;->w:Ljava/lang/Runnable;

    .line 466
    iput-object p1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    .line 467
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/al;->c:Z

    .line 468
    iget-object v0, p0, Lcom/android/calendar/al;->w:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 469
    iget-object v0, p0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 470
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-string v1, "preferred_detailedView"

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/al;->m:I

    .line 473
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-string v1, "preferences_filter"

    invoke-static {v0, v1, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/al;->s:I

    .line 477
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/al;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/android/calendar/al;
    .locals 4

    .prologue
    .line 439
    sget-object v2, Lcom/android/calendar/al;->j:Ljava/util/WeakHashMap;

    monitor-enter v2

    .line 440
    const/4 v1, 0x0

    .line 441
    :try_start_0
    sget-object v0, Lcom/android/calendar/al;->j:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 442
    if-eqz v0, :cond_1

    .line 443
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/al;

    .line 446
    :goto_0
    if-nez v0, :cond_0

    .line 447
    new-instance v0, Lcom/android/calendar/al;

    invoke-direct {v0, p0}, Lcom/android/calendar/al;-><init>(Landroid/content/Context;)V

    .line 448
    sget-object v1, Lcom/android/calendar/al;->j:Ljava/util/WeakHashMap;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    :cond_0
    monitor-exit v2

    return-object v0

    .line 451
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private a(JJII)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1270
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1271
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/agenda/AgendaSDExportActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1272
    const-string v1, "beginTime"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1273
    const-string v1, "endTime"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1275
    if-ne p5, v3, :cond_1

    .line 1276
    const-string v1, "sd_export_type"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1287
    :cond_0
    :goto_0
    const-string v1, "sd_export_path"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1289
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1290
    return-void

    .line 1277
    :cond_1
    if-ne p5, v4, :cond_2

    .line 1278
    const-string v1, "sd_export_type"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 1279
    :cond_2
    if-ne p5, v5, :cond_3

    .line 1280
    const-string v1, "sd_export_type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 1281
    :cond_3
    if-ne p5, v6, :cond_4

    .line 1282
    const-string v1, "sd_export_type"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 1283
    :cond_4
    const/4 v1, 0x6

    if-ne p5, v1, :cond_0

    .line 1284
    const-string v1, "sd_export_type"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private a(JJIJLjava/lang/String;)V
    .locals 3

    .prologue
    .line 1230
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1231
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/agenda/AgendaDeleteActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1232
    const-string v1, "beginTime"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1233
    const-string v1, "endTime"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1234
    const-string v1, "queryDelete"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1235
    const-string v1, "search_type_for_delete"

    invoke-virtual {v0, v1, p6, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1236
    const-string v1, "delete_type"

    invoke-direct {p0, p5}, Lcom/android/calendar/al;->d(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1237
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1238
    return-void
.end method

.method private a(JJJIZLcom/android/calendar/aq;)V
    .locals 3

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/CalendarSearchActivity;

    if-eqz v0, :cond_0

    .line 1126
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p9, v1}, Lcom/android/calendar/hj;->a(Landroid/app/Activity;Lcom/android/calendar/aq;Z)V

    .line 1148
    :goto_0
    return-void

    .line 1129
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1130
    if-eqz p8, :cond_1

    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    :goto_1
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1131
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1132
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1133
    const-string v0, "beginTime"

    invoke-virtual {v1, v0, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1134
    const-string v0, "endTime"

    invoke-virtual {v1, v0, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1136
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1138
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/util/AndroidRuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1139
    :catch_0
    move-exception v0

    .line 1145
    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v2, 0x10000000

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1146
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1130
    :cond_1
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_1
.end method

.method private a(JJJZ)V
    .locals 11

    .prologue
    .line 1208
    const/4 v1, 0x0

    const/4 v8, -0x1

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/android/calendar/al;->a(Landroid/app/Activity;JJJIZ)V

    .line 1209
    return-void
.end method

.method private a(JJJZJZ)V
    .locals 3

    .prologue
    .line 1151
    if-eqz p10, :cond_0

    .line 1152
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1153
    const-string v1, "task"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1154
    const-string v1, "selected"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1155
    const-string v1, "action_view_focus"

    long-to-int v2, p8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1156
    const-string v1, "copyEvent"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1157
    const-string v1, "launch_from_inside"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1158
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1159
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1160
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1174
    :goto_0
    iput-wide p1, p0, Lcom/android/calendar/al;->o:J

    .line 1175
    return-void

    .line 1162
    :cond_0
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1163
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.EDIT"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1164
    const-string v0, "beginTime"

    invoke-virtual {v1, v0, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1165
    const-string v0, "endTime"

    invoke-virtual {v1, v0, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1166
    const-string v0, "action_view_focus"

    long-to-int v2, p8

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1167
    const-string v0, "copyEvent"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1168
    const-string v0, "launch_from_inside"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1169
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1170
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1171
    const-string v0, "editMode"

    invoke-virtual {v1, v0, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1172
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(JJJZZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1178
    if-eqz p8, :cond_0

    .line 1179
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1180
    const-string v1, "task"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1181
    const-string v1, "selected"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1182
    const-string v1, "copyEvent"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1183
    const-string v1, "launch_from_inside"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1184
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1185
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1197
    :goto_0
    iput-wide p1, p0, Lcom/android/calendar/al;->o:J

    .line 1198
    return-void

    .line 1187
    :cond_0
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1188
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.EDIT"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1189
    const-string v0, "beginTime"

    invoke-virtual {v1, v0, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1190
    const-string v0, "endTime"

    invoke-virtual {v1, v0, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1191
    const-string v0, "copyEvent"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1192
    const-string v0, "launch_from_inside"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1193
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1194
    const-string v0, "editMode"

    invoke-virtual {v1, v0, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1195
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(JJZZI)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1109
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1110
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1111
    if-eqz p6, :cond_1

    .line 1112
    const-string v1, "task"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1116
    :cond_0
    :goto_0
    const-string v1, "launch_from_inside"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1117
    const-string v1, "beginTime"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1118
    const-string v1, "endTime"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1119
    const-string v1, "allDay"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1120
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/calendar/al;->o:J

    .line 1121
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1122
    return-void

    .line 1113
    :cond_1
    if-ne p7, v3, :cond_0

    .line 1114
    const-string v1, "event_create"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private a(Landroid/app/Activity;JJJIZ)V
    .locals 10

    .prologue
    .line 1212
    .line 1213
    if-nez p1, :cond_4

    .line 1214
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1215
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    move-object v2, v0

    .line 1220
    :goto_0
    if-nez p9, :cond_2

    .line 1221
    new-instance v1, Lcom/android/calendar/cj;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v1, v2, v0}, Lcom/android/calendar/cj;-><init>(Landroid/app/Activity;Z)V

    move-wide v2, p4

    move-wide/from16 v4, p6

    move-wide v6, p2

    move/from16 v8, p8

    .line 1222
    invoke-virtual/range {v1 .. v8}, Lcom/android/calendar/cj;->a(JJJI)V

    .line 1227
    :cond_0
    :goto_2
    return-void

    .line 1221
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1224
    :cond_2
    new-instance v1, Lcom/android/calendar/cz;

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-direct {v1, v2, v0}, Lcom/android/calendar/cz;-><init>(Landroid/app/Activity;Z)V

    .line 1225
    invoke-virtual {v1, p2, p3}, Lcom/android/calendar/cz;->a(J)V

    goto :goto_2

    .line 1224
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    move-object v2, p1

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/al;Lcom/android/calendar/aq;JJ)V
    .locals 0

    .prologue
    .line 71
    invoke-direct/range {p0 .. p5}, Lcom/android/calendar/al;->a(Lcom/android/calendar/aq;JJ)V

    return-void
.end method

.method private a(Lcom/android/calendar/aq;JJ)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1293
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-nez v0, :cond_1

    .line 1335
    :cond_0
    :goto_0
    return-void

    .line 1296
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 1301
    sub-long v4, p4, p2

    const-wide/32 v6, 0x36ee80

    div-long/2addr v4, v6

    long-to-int v1, v4

    .line 1304
    if-ne v1, v3, :cond_2

    move v1, v2

    .line 1310
    :goto_1
    iget-boolean v4, p1, Lcom/android/calendar/aq;->q:Z

    if-eqz v4, :cond_5

    .line 1314
    :goto_2
    iget-object v4, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-static {v4}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v4

    if-nez v4, :cond_4

    .line 1315
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    sget-object v5, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    .line 1317
    if-nez v4, :cond_0

    .line 1318
    invoke-static {}, Lcom/android/calendar/hj;->g()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1319
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const v1, 0x7f0f02fe

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 1307
    goto :goto_1

    .line 1323
    :cond_3
    new-instance v2, Lcom/android/calendar/event/ld;

    invoke-direct {v2, v1, v3}, Lcom/android/calendar/event/ld;-><init>(ZZ)V

    .line 1326
    :try_start_0
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/android/calendar/event/ld;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1328
    :catch_0
    move-exception v0

    .line 1329
    const-string v0, "CalendarController"

    const-string v1, "Fail to launch quick add dialog"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1333
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/al;->i()V

    goto :goto_0

    :cond_5
    move v3, v2

    goto :goto_2
.end method

.method static synthetic b(Lcom/android/calendar/al;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 460
    sget-object v1, Lcom/android/calendar/al;->j:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 461
    :try_start_0
    sget-object v0, Lcom/android/calendar/al;->j:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    monitor-exit v1

    .line 463
    return-void

    .line 462
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d(I)I
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 1246
    const/4 v4, 0x0

    .line 1247
    if-ne p1, v0, :cond_1

    .line 1248
    const/4 v0, 0x5

    .line 1258
    :cond_0
    :goto_0
    return v0

    .line 1249
    :cond_1
    if-eq p1, v1, :cond_0

    .line 1251
    if-ne p1, v2, :cond_2

    move v0, v1

    .line 1252
    goto :goto_0

    .line 1253
    :cond_2
    if-ne p1, v3, :cond_3

    move v0, v2

    .line 1254
    goto :goto_0

    .line 1255
    :cond_3
    const/4 v0, 0x6

    if-ne p1, v0, :cond_4

    move v0, v3

    .line 1256
    goto :goto_0

    :cond_4
    move v0, v4

    goto :goto_0
.end method

.method private e(I)V
    .locals 3

    .prologue
    .line 1262
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1263
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/agenda/AgendaSDImportActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1264
    const-string v1, "sd_import_path"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1266
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1267
    return-void
.end method

.method private n()V
    .locals 1

    .prologue
    .line 1071
    sget-boolean v0, Lcom/android/calendar/al;->c:Z

    if-eqz v0, :cond_0

    .line 1072
    invoke-direct {p0}, Lcom/android/calendar/al;->o()V

    .line 1076
    :goto_0
    return-void

    .line 1074
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/al;->p()V

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 1079
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1080
    new-instance v1, Lcom/android/calendar/selectcalendars/ba;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Lcom/android/calendar/selectcalendars/ba;-><init>(Z)V

    .line 1082
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "selectCalendar"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1087
    :cond_0
    :goto_0
    return-void

    .line 1083
    :catch_0
    move-exception v0

    .line 1084
    const-string v0, "CalendarController"

    const-string v1, "Fail to launch ExpandableSelectCalendarsFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 1090
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1091
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1092
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1093
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 1096
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1097
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/preference/CalendarSettingsActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1099
    sget-boolean v1, Lcom/android/calendar/al;->c:Z

    if-nez v1, :cond_0

    .line 1100
    const-string v1, ":android:show_fragment"

    const-string v2, "com.android.calendar.preference.AllCalendarPreferences"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1103
    :cond_0
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1104
    iget-object v1, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1105
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 978
    monitor-enter p0

    .line 979
    :try_start_0
    iget v0, p0, Lcom/android/calendar/al;->i:I

    if-lez v0, :cond_0

    .line 981
    iget-object v0, p0, Lcom/android/calendar/al;->e:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/android/calendar/al;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 986
    :goto_0
    monitor-exit p0

    .line 987
    return-void

    .line 983
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/al;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 984
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    goto :goto_0

    .line 986
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 1032
    iput p1, p0, Lcom/android/calendar/al;->r:I

    .line 1033
    return-void
.end method

.method public a(ILcom/android/calendar/ap;)V
    .locals 2

    .prologue
    .line 943
    monitor-enter p0

    .line 944
    :try_start_0
    iget v0, p0, Lcom/android/calendar/al;->i:I

    if-lez v0, :cond_0

    .line 945
    iget-object v0, p0, Lcom/android/calendar/al;->f:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 949
    :goto_0
    monitor-exit p0

    .line 950
    return-void

    .line 947
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/al;->d:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 949
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 1016
    return-void
.end method

.method public a(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 964
    monitor-enter p0

    .line 965
    :try_start_0
    iget v0, p0, Lcom/android/calendar/al;->i:I

    if-lez v0, :cond_1

    .line 967
    iget-object v0, p0, Lcom/android/calendar/al;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 974
    :cond_0
    :goto_0
    monitor-exit p0

    .line 975
    return-void

    .line 969
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/al;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 970
    iget-object v0, p0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    .line 971
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    goto :goto_0

    .line 974
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Object;JJJJIIJ)V
    .locals 18

    .prologue
    .line 501
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/aq;->a(IZ)J

    move-result-wide v12

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move/from16 v10, p10

    move/from16 v11, p11

    move-wide/from16 v14, p12

    invoke-virtual/range {v0 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJZ)V

    .line 503
    return-void
.end method

.method public a(Ljava/lang/Object;JJJJIIJJ)V
    .locals 18

    .prologue
    .line 546
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move/from16 v10, p10

    move/from16 v11, p11

    move-wide/from16 v12, p12

    move-wide/from16 v14, p14

    invoke-virtual/range {v0 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJZ)V

    .line 548
    return-void
.end method

.method public a(Ljava/lang/Object;JJJJIIJJZ)V
    .locals 6

    .prologue
    .line 553
    new-instance v2, Lcom/android/calendar/aq;

    invoke-direct {v2}, Lcom/android/calendar/aq;-><init>()V

    .line 554
    iput-wide p2, v2, Lcom/android/calendar/aq;->a:J

    .line 555
    const-wide/16 v4, 0x8

    cmp-long v3, p2, v4

    if-eqz v3, :cond_0

    const-wide/16 v4, 0x4

    cmp-long v3, p2, v4

    if-nez v3, :cond_1

    .line 556
    :cond_0
    const/4 v3, 0x0

    iput v3, v2, Lcom/android/calendar/aq;->b:I

    .line 558
    :cond_1
    iput-wide p4, v2, Lcom/android/calendar/aq;->c:J

    .line 559
    iget-object v3, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/calendar/al;->w:Ljava/lang/Runnable;

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    .line 560
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v4, v2, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    .line 561
    const-wide/16 v4, 0x2

    cmp-long v4, p2, v4

    if-nez v4, :cond_2

    .line 562
    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v4

    .line 563
    invoke-virtual {v4, p6, p7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 564
    iput-object v4, v2, Lcom/android/calendar/aq;->g:Ljava/util/Calendar;

    .line 565
    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v3

    .line 566
    invoke-virtual {v3, p8, p9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 567
    iput-object v3, v2, Lcom/android/calendar/aq;->h:Ljava/util/Calendar;

    .line 569
    :cond_2
    iget-object v3, v2, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v3, p6, p7}, Landroid/text/format/Time;->set(J)V

    .line 570
    const-wide/16 v4, -0x1

    cmp-long v3, p14, v4

    if-eqz v3, :cond_3

    .line 571
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/calendar/al;->w:Ljava/lang/Runnable;

    invoke-static {v4, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v3, v2, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    .line 572
    iget-object v3, v2, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    move-wide/from16 v0, p14

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 576
    :goto_0
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/calendar/al;->w:Ljava/lang/Runnable;

    invoke-static {v4, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v3, v2, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    .line 577
    iget-object v3, v2, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v3, p8, p9}, Landroid/text/format/Time;->set(J)V

    .line 578
    move/from16 v0, p10

    iput v0, v2, Lcom/android/calendar/aq;->i:I

    .line 579
    move/from16 v0, p11

    iput v0, v2, Lcom/android/calendar/aq;->j:I

    .line 580
    move-wide/from16 v0, p12

    iput-wide v0, v2, Lcom/android/calendar/aq;->p:J

    .line 581
    move/from16 v0, p16

    iput-boolean v0, v2, Lcom/android/calendar/aq;->q:Z

    .line 582
    invoke-virtual {p0, p1, v2}, Lcom/android/calendar/al;->a(Ljava/lang/Object;Lcom/android/calendar/aq;)V

    .line 583
    return-void

    .line 574
    :cond_3
    iget-object v3, v2, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    iput-object v3, v2, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;JJJJIIJZ)V
    .locals 18

    .prologue
    .line 507
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/aq;->a(IZ)J

    move-result-wide v12

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move/from16 v10, p10

    move/from16 v11, p11

    move-wide/from16 v14, p12

    move/from16 v16, p14

    invoke-virtual/range {v0 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJZ)V

    .line 510
    return-void
.end method

.method public a(Ljava/lang/Object;JLandroid/text/format/Time;I)V
    .locals 14

    .prologue
    .line 594
    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v9, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p2

    move-object/from16 v4, p4

    move/from16 v8, p5

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 595
    return-void
.end method

.method public a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V
    .locals 14

    .prologue
    .line 609
    const-wide/16 v10, 0x2

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p4

    move-wide/from16 v7, p6

    move/from16 v9, p8

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 611
    return-void
.end method

.method public a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V
    .locals 14

    .prologue
    .line 645
    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p4

    move-wide/from16 v7, p6

    move/from16 v9, p8

    move-wide/from16 v10, p9

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 647
    return-void
.end method

.method public a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 626
    new-instance v0, Lcom/android/calendar/aq;

    invoke-direct {v0}, Lcom/android/calendar/aq;-><init>()V

    .line 627
    iput-wide p2, v0, Lcom/android/calendar/aq;->a:J

    .line 628
    iput-object p4, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    .line 629
    iput-object p4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    .line 630
    iput-object p5, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    .line 631
    iput-wide p6, v0, Lcom/android/calendar/aq;->c:J

    .line 632
    iput p8, v0, Lcom/android/calendar/aq;->b:I

    .line 633
    iput-object v1, v0, Lcom/android/calendar/aq;->k:Ljava/lang/String;

    .line 634
    iput-object v1, v0, Lcom/android/calendar/aq;->l:Landroid/content/ComponentName;

    .line 635
    const-wide/16 v2, 0x2

    iput-wide v2, v0, Lcom/android/calendar/aq;->p:J

    .line 636
    iput-boolean p9, v0, Lcom/android/calendar/aq;->q:Z

    .line 637
    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/al;->a(Ljava/lang/Object;Lcom/android/calendar/aq;)V

    .line 638
    return-void
.end method

.method public a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V
    .locals 16

    .prologue
    .line 651
    const/4 v12, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-wide/from16 v7, p7

    move/from16 v9, p9

    move-wide/from16 v10, p10

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-virtual/range {v0 .. v14}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    .line 652
    return-void
.end method

.method public a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V
    .locals 4

    .prologue
    .line 656
    new-instance v1, Lcom/android/calendar/aq;

    invoke-direct {v1}, Lcom/android/calendar/aq;-><init>()V

    .line 657
    iput-wide p2, v1, Lcom/android/calendar/aq;->a:J

    .line 658
    iput-object p4, v1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    .line 659
    iput-object p6, v1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    .line 660
    iput-object p5, v1, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    .line 661
    iput-wide p7, v1, Lcom/android/calendar/aq;->c:J

    .line 662
    iput p9, v1, Lcom/android/calendar/aq;->b:I

    .line 663
    move-object/from16 v0, p13

    iput-object v0, v1, Lcom/android/calendar/aq;->k:Ljava/lang/String;

    .line 664
    move-object/from16 v0, p14

    iput-object v0, v1, Lcom/android/calendar/aq;->l:Landroid/content/ComponentName;

    .line 665
    iput-wide p10, v1, Lcom/android/calendar/aq;->p:J

    .line 666
    move/from16 v0, p12

    iput-boolean v0, v1, Lcom/android/calendar/aq;->q:Z

    .line 667
    invoke-virtual {p0, p1, v1}, Lcom/android/calendar/al;->a(Ljava/lang/Object;Lcom/android/calendar/aq;)V

    .line 668
    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/android/calendar/aq;)V
    .locals 20

    .prologue
    .line 677
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->k:Ljava/util/WeakHashMap;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 678
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/aq;->a:J

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 933
    :cond_0
    :goto_0
    return-void

    .line 686
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/al;->l:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/al;->n:I

    .line 689
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/aq;->b:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_e

    .line 690
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/al;->m:I

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/calendar/aq;->b:I

    .line 691
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/al;->m:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/al;->l:I

    .line 711
    :cond_2
    :goto_1
    const-wide/16 v2, 0x0

    .line 712
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    if-eqz v4, :cond_4

    .line 713
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v2

    .line 714
    if-lez v2, :cond_11

    .line 715
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/16 v3, 0x1f

    const/16 v4, 0xb

    const/16 v5, 0x7f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/text/format/Time;->set(III)V

    .line 716
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 721
    :cond_3
    :goto_2
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 725
    :cond_4
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    if-eqz v4, :cond_13

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_13

    .line 726
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-static {v4}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v4

    .line 727
    if-lez v4, :cond_12

    .line 728
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    const/16 v5, 0x1f

    const/16 v6, 0xb

    const/16 v7, 0x7f4

    invoke-virtual {v4, v5, v6, v7}, Landroid/text/format/Time;->set(III)V

    .line 729
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 734
    :cond_5
    :goto_3
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v6, 0x2

    cmp-long v4, v4, v6

    if-eqz v4, :cond_6

    .line 735
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 750
    :cond_6
    :goto_4
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v6, 0x400

    cmp-long v4, v4, v6

    if-nez v4, :cond_7

    .line 751
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/aq;->p:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/calendar/al;->q:J

    .line 755
    :cond_7
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_9

    .line 756
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v2

    if-eqz v2, :cond_16

    :cond_8
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    if-eqz v2, :cond_16

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7b2

    if-ne v2, v3, :cond_16

    .line 765
    :cond_9
    :goto_5
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/hj;->d(Landroid/text/format/Time;)V

    .line 766
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/hj;->d(Landroid/text/format/Time;)V

    .line 777
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0xd

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 780
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_17

    .line 781
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->c:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/al;->o:J

    .line 787
    :cond_a
    :goto_6
    const/4 v4, 0x0

    .line 788
    monitor-enter p0

    .line 789
    :try_start_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/al;->i:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/al;->i:I

    .line 795
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    if-eqz v2, :cond_b

    .line 797
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/android/calendar/ap;

    .line 798
    if-eqz v2, :cond_b

    invoke-interface {v2}, Lcom/android/calendar/ap;->g()J

    move-result-wide v6

    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/android/calendar/aq;->a:J

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/al;->e:Ljava/util/LinkedList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    iget-object v5, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v5}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 800
    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Lcom/android/calendar/ap;->a(Lcom/android/calendar/aq;)V

    .line 801
    const/4 v4, 0x1

    .line 804
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 805
    :cond_c
    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 806
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 807
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 808
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v6, v3, :cond_c

    .line 812
    :cond_d
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/ap;

    .line 813
    if-eqz v2, :cond_33

    invoke-interface {v2}, Lcom/android/calendar/ap;->g()J

    move-result-wide v8

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/android/calendar/aq;->a:J

    and-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-eqz v3, :cond_33

    .line 815
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/al;->e:Ljava/util/LinkedList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 818
    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Lcom/android/calendar/ap;->a(Lcom/android/calendar/aq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 819
    const/4 v2, 0x1

    :goto_8
    move v4, v2

    .line 821
    goto :goto_7

    .line 692
    :cond_e
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/aq;->b:I

    if-nez v2, :cond_f

    .line 693
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/al;->l:I

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/calendar/aq;->b:I

    goto/16 :goto_1

    .line 694
    :cond_f
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/aq;->b:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_2

    .line 695
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/aq;->b:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/al;->l:I

    .line 697
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/aq;->b:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_10

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/aq;->b:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_10

    invoke-static {}, Lcom/android/calendar/hj;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/aq;->b:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 699
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/al;->l:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/al;->m:I

    goto/16 :goto_1

    .line 717
    :cond_11
    if-gez v2, :cond_3

    .line 718
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v5, 0x76e

    invoke-virtual {v2, v3, v4, v5}, Landroid/text/format/Time;->set(III)V

    .line 719
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    goto/16 :goto_2

    .line 730
    :cond_12
    if-gez v4, :cond_5

    .line 731
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x76e

    invoke-virtual {v4, v5, v6, v7}, Landroid/text/format/Time;->set(III)V

    .line 732
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    goto/16 :goto_3

    .line 738
    :cond_13
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_15

    .line 741
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 742
    cmp-long v6, v4, v2

    if-ltz v6, :cond_14

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-eqz v6, :cond_15

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_15

    .line 744
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 747
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    move-object/from16 v0, p2

    iput-object v4, v0, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    goto/16 :goto_4

    .line 760
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    move-object/from16 v0, p2

    iput-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    goto/16 :goto_5

    .line 783
    :cond_17
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/al;->o:J

    goto/16 :goto_6

    .line 823
    :cond_18
    :try_start_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/al;->i:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/al;->i:I

    .line 825
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/al;->i:I

    if-nez v2, :cond_1e

    .line 828
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->e:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lez v2, :cond_1b

    .line 829
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->e:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_19
    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 830
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/al;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 831
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    if-eqz v5, :cond_19

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    iget-object v5, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v2, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 832
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    goto :goto_9

    .line 852
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 835
    :cond_1a
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->e:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 838
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->h:Landroid/util/Pair;

    if-eqz v2, :cond_1c

    .line 839
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->h:Landroid/util/Pair;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    .line 840
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/al;->h:Landroid/util/Pair;

    .line 842
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->size()I

    move-result v2

    if-lez v2, :cond_1e

    .line 843
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 844
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/al;->d:Ljava/util/LinkedHashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 848
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->clear()V

    .line 852
    :cond_1e
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 854
    if-nez v4, :cond_0

    .line 856
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x40

    cmp-long v2, v2, v4

    if-nez v2, :cond_1f

    .line 857
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/al;->q()V

    goto/16 :goto_0

    .line 862
    :cond_1f
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x2000

    cmp-long v2, v2, v4

    if-nez v2, :cond_20

    .line 863
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/al;->n()V

    goto/16 :goto_0

    .line 867
    :cond_20
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-nez v2, :cond_21

    const-wide/16 v6, -0x1

    .line 868
    :goto_b
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_23

    .line 869
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->p:J

    const-wide/16 v8, 0x10

    cmp-long v2, v2, v8

    if-nez v2, :cond_22

    const/4 v8, 0x1

    :goto_c
    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/android/calendar/aq;->q:Z

    const/4 v10, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v10}, Lcom/android/calendar/al;->a(JJZZI)V

    goto/16 :goto_0

    .line 867
    :cond_21
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    goto :goto_b

    .line 869
    :cond_22
    const/4 v8, 0x0

    goto :goto_c

    .line 872
    :cond_23
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x2

    cmp-long v2, v2, v4

    if-nez v2, :cond_24

    .line 873
    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/android/calendar/aq;->c:J

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    invoke-virtual/range {p2 .. p2}, Lcom/android/calendar/aq;->a()I

    move-result v16

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lcom/android/calendar/aq;->q:Z

    move/from16 v17, v0

    move-object/from16 v9, p0

    move-wide v14, v6

    move-object/from16 v18, p2

    invoke-direct/range {v9 .. v18}, Lcom/android/calendar/al;->a(JJJIZLcom/android/calendar/aq;)V

    goto/16 :goto_0

    .line 875
    :cond_24
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x8

    cmp-long v2, v2, v4

    if-nez v2, :cond_25

    .line 876
    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/android/calendar/aq;->c:J

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    const/16 v16, 0x1

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/android/calendar/aq;->p:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lcom/android/calendar/aq;->q:Z

    move/from16 v19, v0

    move-object/from16 v9, p0

    move-wide v14, v6

    invoke-direct/range {v9 .. v19}, Lcom/android/calendar/al;->a(JJJZJZ)V

    goto/16 :goto_0

    .line 878
    :cond_25
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x4

    cmp-long v2, v2, v4

    if-nez v2, :cond_26

    .line 879
    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/android/calendar/aq;->c:J

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    const/16 v16, 0x0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/android/calendar/aq;->p:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lcom/android/calendar/aq;->q:Z

    move/from16 v19, v0

    move-object/from16 v9, p0

    move-wide v14, v6

    invoke-direct/range {v9 .. v19}, Lcom/android/calendar/al;->a(JJJZJZ)V

    goto/16 :goto_0

    .line 881
    :cond_26
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v4, 0x200000

    cmp-long v2, v2, v4

    if-nez v2, :cond_27

    .line 882
    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/android/calendar/aq;->c:J

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    const/16 v16, 0x0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lcom/android/calendar/aq;->q:Z

    move/from16 v17, v0

    move-object/from16 v9, p0

    move-wide v14, v6

    invoke-direct/range {v9 .. v17}, Lcom/android/calendar/al;->a(JJJZZ)V

    goto/16 :goto_0

    .line 884
    :cond_27
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x10

    cmp-long v2, v2, v4

    if-nez v2, :cond_28

    .line 885
    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/android/calendar/aq;->c:J

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lcom/android/calendar/aq;->q:Z

    move/from16 v16, v0

    move-object/from16 v9, p0

    move-wide v14, v6

    invoke-direct/range {v9 .. v16}, Lcom/android/calendar/al;->a(JJJZ)V

    goto/16 :goto_0

    .line 887
    :cond_28
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x4000

    cmp-long v2, v2, v4

    if-nez v2, :cond_2b

    .line 888
    sget-boolean v2, Lcom/android/calendar/al;->c:Z

    if-eqz v2, :cond_2a

    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v2

    if-nez v2, :cond_29

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 889
    :cond_29
    new-instance v2, Lcom/android/calendar/an;

    const-wide/16 v4, 0xfa

    const-wide/16 v6, 0xfa

    move-object/from16 v3, p0

    move-object/from16 v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/android/calendar/an;-><init>(Lcom/android/calendar/al;JJLcom/android/calendar/aq;)V

    invoke-virtual {v2}, Lcom/android/calendar/an;->start()Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 900
    :cond_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/al;->a:Landroid/os/Handler;

    new-instance v3, Lcom/android/calendar/ao;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1, v6, v7}, Lcom/android/calendar/ao;-><init>(Lcom/android/calendar/al;Lcom/android/calendar/aq;J)V

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 909
    :cond_2b
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v4, 0x8000

    cmp-long v2, v2, v4

    if-nez v2, :cond_2c

    .line 910
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, p2

    iget v8, v0, Lcom/android/calendar/aq;->b:I

    move-object/from16 v0, p2

    iget-wide v9, v0, Lcom/android/calendar/aq;->p:J

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/android/calendar/aq;->k:Ljava/lang/String;

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v11}, Lcom/android/calendar/al;->a(JJIJLjava/lang/String;)V

    goto/16 :goto_0

    .line 912
    :cond_2c
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v4, 0x400000

    cmp-long v2, v2, v4

    if-nez v2, :cond_2d

    .line 913
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/al;->e(I)V

    goto/16 :goto_0

    .line 915
    :cond_2d
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v4, 0x800000

    cmp-long v2, v2, v4

    if-nez v2, :cond_2e

    .line 916
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/al;->e(I)V

    goto/16 :goto_0

    .line 918
    :cond_2e
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v4, 0x1000000

    cmp-long v2, v2, v4

    if-nez v2, :cond_2f

    .line 919
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, p2

    iget v8, v0, Lcom/android/calendar/aq;->b:I

    const/4 v9, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/android/calendar/al;->a(JJII)V

    goto/16 :goto_0

    .line 922
    :cond_2f
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v4, 0x2000000

    cmp-long v2, v2, v4

    if-nez v2, :cond_30

    .line 923
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, p2

    iget v8, v0, Lcom/android/calendar/aq;->b:I

    const/4 v9, 0x2

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/android/calendar/al;->a(JJII)V

    goto/16 :goto_0

    .line 926
    :cond_30
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v4, 0x40000000

    cmp-long v2, v2, v4

    if-nez v2, :cond_32

    .line 927
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->p:J

    const-wide/16 v8, 0x10

    cmp-long v2, v2, v8

    if-nez v2, :cond_31

    const/4 v8, 0x1

    :goto_d
    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/android/calendar/aq;->q:Z

    const/4 v10, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v10}, Lcom/android/calendar/al;->a(JJZZI)V

    goto/16 :goto_0

    :cond_31
    const/4 v8, 0x0

    goto :goto_d

    .line 929
    :cond_32
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/aq;->a:J

    const-wide v4, 0x80000000L

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 930
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/al;->j()V

    goto/16 :goto_0

    :cond_33
    move v2, v4

    goto/16 :goto_8
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1445
    iput-boolean p1, p0, Lcom/android/calendar/al;->t:Z

    .line 1446
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 998
    iget-object v0, p0, Lcom/android/calendar/al;->p:Landroid/text/format/Time;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 1049
    iput p1, p0, Lcom/android/calendar/al;->s:I

    .line 1050
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-string v1, "preferences_filter"

    invoke-static {v0, v1, p1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1053
    return-void
.end method

.method public b(ILcom/android/calendar/ap;)V
    .locals 2

    .prologue
    .line 953
    monitor-enter p0

    .line 954
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 955
    iget v0, p0, Lcom/android/calendar/al;->i:I

    if-lez v0, :cond_0

    .line 956
    new-instance v0, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/calendar/al;->h:Landroid/util/Pair;

    .line 960
    :goto_0
    monitor-exit p0

    .line 961
    return-void

    .line 958
    :cond_0
    new-instance v0, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/calendar/al;->g:Landroid/util/Pair;

    goto :goto_0

    .line 960
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 1389
    iput-wide p1, p0, Lcom/android/calendar/al;->o:J

    .line 1390
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 1451
    iput-boolean p1, p0, Lcom/android/calendar/al;->u:Z

    .line 1452
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 1006
    iget-wide v0, p0, Lcom/android/calendar/al;->q:J

    return-wide v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 1384
    iput p1, p0, Lcom/android/calendar/al;->l:I

    .line 1385
    return-void
.end method

.method c(Landroid/content/Context;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ShowToast"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 481
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 482
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 483
    new-instance v1, Landroid/text/format/Time;

    const-string v2, "UTC"

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 484
    const v2, 0x24dc87

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 485
    invoke-virtual {v1, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 486
    const v4, 0x259d23

    invoke-static {v1, v4}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 487
    invoke-virtual {v1, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 488
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 489
    const-string v6, "UTC"

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 490
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 491
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 492
    invoke-virtual {v1, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 493
    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 494
    const v2, 0x7f0f0486

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v8

    aput-object v1, v2, v7

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 495
    invoke-static {p1, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/al;->v:Landroid/widget/Toast;

    .line 497
    :cond_0
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1023
    iget v0, p0, Lcom/android/calendar/al;->r:I

    return v0
.end method

.method public e()I
    .locals 3

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-string v1, "preferences_filter"

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 1059
    iget-wide v0, p0, Lcom/android/calendar/al;->o:J

    return-wide v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 1063
    iget v0, p0, Lcom/android/calendar/al;->l:I

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 1067
    iget v0, p0, Lcom/android/calendar/al;->n:I

    return v0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 1338
    iget-object v0, p0, Lcom/android/calendar/al;->v:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1339
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/calendar/al;->c(Landroid/content/Context;)V

    .line 1341
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/al;->v:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 1342
    iget-object v0, p0, Lcom/android/calendar/al;->v:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1344
    :cond_1
    return-void
.end method

.method public j()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1347
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-nez v0, :cond_0

    .line 1363
    :goto_0
    return-void

    .line 1350
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 1352
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1353
    invoke-virtual {p0}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1355
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v1, Landroid/text/format/Time;->year:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Landroid/text/format/Time;->month:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1356
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1357
    iget-object v3, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    const-class v4, Lcom/android/calendar/handwriting/MonthHandwritingActivity;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1358
    const/high16 v3, 0x20000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1359
    const-string v3, "filename"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1361
    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1362
    invoke-virtual {v0, v5, v5}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method public k()V
    .locals 7

    .prologue
    .line 1366
    iget-object v0, p0, Lcom/android/calendar/al;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 1367
    const-string v0, "CalendarController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Refreshing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " accounts"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1369
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    .line 1370
    const-string v3, "tasks"

    .line 1371
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 1372
    const-string v4, "CalendarController"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1373
    const-string v4, "CalendarController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Refreshing calendars for: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1375
    :cond_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1376
    const-string v5, "force"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1377
    aget-object v5, v1, v0

    invoke-static {v5, v2, v4}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1378
    aget-object v5, v1, v0

    invoke-static {v5, v3, v4}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1371
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1380
    :cond_1
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 1442
    iget-boolean v0, p0, Lcom/android/calendar/al;->t:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 1448
    iget-boolean v0, p0, Lcom/android/calendar/al;->u:Z

    return v0
.end method
