.class Lcom/android/calendar/ch;
.super Ljava/lang/Object;
.source "DeleteEventDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/ca;


# direct methods
.method constructor <init>(Lcom/android/calendar/ca;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const v4, 0x7f1200b9

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 489
    iget-object v1, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->f(Lcom/android/calendar/ca;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/calendar/ca;->a(Lcom/android/calendar/ca;I)I

    .line 490
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0, p2}, Lcom/android/calendar/ca;->b(Lcom/android/calendar/ca;I)I

    .line 494
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-virtual {v0}, Lcom/android/calendar/ca;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 495
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 499
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->g(Lcom/android/calendar/ca;)I

    move-result v0

    if-ne v0, v5, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 503
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->a(Lcom/android/calendar/ca;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v1}, Lcom/android/calendar/ca;->g(Lcom/android/calendar/ca;)I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 504
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->h(Lcom/android/calendar/ca;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 505
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->h(Lcom/android/calendar/ca;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 506
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->h(Lcom/android/calendar/ca;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 507
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0, v2}, Lcom/android/calendar/ca;->a(Lcom/android/calendar/ca;Z)Z

    goto :goto_0

    .line 508
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->i(Lcom/android/calendar/ca;)Lcom/android/calendar/as;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/calendar/as;->v:Z

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->h(Lcom/android/calendar/ca;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 510
    iget-object v0, p0, Lcom/android/calendar/ch;->a:Lcom/android/calendar/ca;

    invoke-static {v0}, Lcom/android/calendar/ca;->h(Lcom/android/calendar/ca;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0
.end method
