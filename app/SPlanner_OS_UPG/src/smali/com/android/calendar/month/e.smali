.class Lcom/android/calendar/month/e;
.super Landroid/widget/BaseAdapter;
.source "EventInfoListFragment.java"


# instance fields
.field protected a:Ljava/util/ArrayList;

.field protected b:Ljava/util/ArrayList;

.field protected c:Landroid/content/Context;

.field final synthetic d:Lcom/android/calendar/month/a;


# direct methods
.method public constructor <init>(Lcom/android/calendar/month/a;Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 432
    iput-object p1, p0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 427
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    .line 428
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/e;->b:Ljava/util/ArrayList;

    .line 433
    iput-object p3, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    .line 434
    iput-object p4, p0, Lcom/android/calendar/month/e;->b:Ljava/util/ArrayList;

    .line 435
    iput-object p2, p0, Lcom/android/calendar/month/e;->c:Landroid/content/Context;

    .line 436
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/month/e;ZLandroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 425
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/month/e;->a(ZLandroid/widget/TextView;)V

    return-void
.end method

.method private a(ZLandroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 760
    if-eqz p1, :cond_0

    .line 761
    iget-object v0, p0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-virtual {v0}, Lcom/android/calendar/month/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0088

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 762
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x10

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 767
    :goto_0
    return-void

    .line 764
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-virtual {v0}, Lcom/android/calendar/month/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 765
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    and-int/lit8 v0, v0, -0x11

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    .line 440
    return-void
.end method

.method public a(ZJ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 770
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 771
    const-string v2, "complete"

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 772
    const-string v0, "date_completed"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 773
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 774
    iget-object v2, p0, Lcom/android/calendar/month/e;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 775
    return-void

    .line 771
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 447
    invoke-virtual {p0}, Lcom/android/calendar/month/e;->getCount()I

    move-result v2

    .line 449
    if-ge v2, v1, :cond_1

    .line 456
    :cond_0
    :goto_0
    return v0

    .line 451
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt p1, v3, :cond_0

    .line 453
    if-ge p1, v2, :cond_0

    move v0, v1

    .line 454
    goto :goto_0
.end method

.method public b(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/android/calendar/month/e;->b:Ljava/util/ArrayList;

    .line 444
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 461
    const/4 v0, 0x0

    .line 462
    iget-object v1, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 463
    iget-object v1, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 465
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/e;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 466
    iget-object v1, p0, Lcom/android/calendar/month/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 468
    :cond_1
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 473
    invoke-virtual {p0}, Lcom/android/calendar/month/e;->getCount()I

    move-result v1

    .line 475
    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 484
    :cond_0
    :goto_0
    return-object v0

    .line 478
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p1, v2, :cond_2

    .line 479
    iget-object v0, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 481
    :cond_2
    if-ge p1, v1, :cond_0

    .line 482
    iget-object v0, p0, Lcom/android/calendar/month/e;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/month/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 489
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19

    .prologue
    .line 494
    const/4 v4, 0x0

    .line 495
    const/4 v3, 0x0

    .line 497
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_0

    .line 498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-virtual {v5}, Lcom/android/calendar/month/a;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;Landroid/app/Activity;)Landroid/app/Activity;

    .line 501
    :cond_0
    if-eqz p2, :cond_1c

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1c

    .line 505
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 506
    instance-of v5, v2, Lcom/android/calendar/month/g;

    if-eqz v5, :cond_1c

    .line 508
    check-cast v2, Lcom/android/calendar/month/g;

    move-object/from16 v3, p2

    .line 512
    :goto_0
    if-nez v2, :cond_1b

    .line 513
    new-instance v3, Lcom/android/calendar/month/g;

    const/4 v2, 0x0

    invoke-direct {v3, v2}, Lcom/android/calendar/month/g;-><init>(Lcom/android/calendar/month/b;)V

    .line 514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v2

    const v4, 0x7f040098

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-static {v2, v4, v0, v5}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 516
    const v2, 0x7f120087

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lcom/android/calendar/month/g;->a:Landroid/view/View;

    .line 517
    const v2, 0x7f1202b5

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v3, Lcom/android/calendar/month/g;->b:Landroid/widget/CheckBox;

    .line 518
    const v2, 0x7f120039

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/month/g;->c:Landroid/widget/ImageView;

    .line 519
    const v2, 0x7f12003a

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/month/g;->d:Landroid/widget/TextView;

    .line 520
    const v2, 0x7f12002d

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/month/g;->e:Landroid/widget/TextView;

    .line 521
    const v2, 0x7f12003c

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/month/g;->f:Landroid/widget/TextView;

    .line 522
    const v2, 0x7f1202b6

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/month/g;->g:Landroid/widget/ImageView;

    .line 523
    const v2, 0x7f1202b7

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/month/g;->h:Landroid/widget/ImageView;

    .line 524
    const v2, 0x7f120042

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/month/g;->i:Landroid/widget/ImageView;

    .line 526
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v13, v3

    move-object v14, v4

    .line 529
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-virtual {v2}, Lcom/android/calendar/month/a;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_2

    .line 739
    :cond_1
    :goto_2
    return-object v14

    .line 533
    :cond_2
    iget-object v3, v13, Lcom/android/calendar/month/g;->a:Landroid/view/View;

    .line 534
    iget-object v6, v13, Lcom/android/calendar/month/g;->e:Landroid/widget/TextView;

    .line 535
    iget-object v0, v13, Lcom/android/calendar/month/g;->f:Landroid/widget/TextView;

    move-object/from16 v16, v0

    .line 536
    iget-object v0, v13, Lcom/android/calendar/month/g;->g:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    .line 537
    iget-object v9, v13, Lcom/android/calendar/month/g;->h:Landroid/widget/ImageView;

    .line 538
    iget-object v0, v13, Lcom/android/calendar/month/g;->i:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    .line 539
    iget-object v10, v13, Lcom/android/calendar/month/g;->b:Landroid/widget/CheckBox;

    .line 540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 542
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/e;->getCount()I

    move-result v2

    move/from16 v0, p1

    if-le v2, v0, :cond_1

    if-ltz p1, :cond_1

    .line 547
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->c:Landroid/content/Context;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->c:Landroid/content/Context;

    instance-of v2, v2, Lcom/android/calendar/AllInOneActivity;

    if-eqz v2, :cond_3

    .line 548
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->c(Lcom/android/calendar/month/a;)Lcom/android/calendar/al;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/al;->g()I

    .line 550
    :cond_3
    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/month/e;->a(I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 551
    iget-object v2, v13, Lcom/android/calendar/month/g;->c:Landroid/widget/ImageView;

    if-eqz v2, :cond_4

    .line 552
    iget-object v2, v13, Lcom/android/calendar/month/g;->c:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 554
    :cond_4
    iget-object v2, v13, Lcom/android/calendar/month/g;->b:Landroid/widget/CheckBox;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 555
    iget-object v2, v13, Lcom/android/calendar/month/g;->d:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 556
    iget-object v2, v13, Lcom/android/calendar/month/g;->f:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 557
    iget-object v2, v13, Lcom/android/calendar/month/g;->i:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 561
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/month/e;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/task/aa;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v8, v2

    .line 566
    :goto_3
    if-eqz v8, :cond_1

    .line 569
    iget-object v2, v8, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, v8, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_6

    .line 570
    iget-object v2, v8, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/android/calendar/month/a;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 576
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v2

    iget v4, v8, Lcom/android/calendar/task/aa;->i:I

    invoke-static {v2, v4}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 579
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    iget-wide v4, v8, Lcom/android/calendar/task/aa;->b:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v3}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Lcom/android/calendar/month/a;->a(JLandroid/app/Activity;)Z

    move-result v2

    iput-boolean v2, v8, Lcom/android/calendar/task/aa;->g:Z

    .line 580
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 581
    iget-boolean v2, v8, Lcom/android/calendar/task/aa;->g:Z

    invoke-virtual {v10, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 582
    new-instance v2, Lcom/android/calendar/month/f;

    iget-wide v4, v8, Lcom/android/calendar/task/aa;->b:J

    const/4 v7, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/calendar/month/f;-><init>(Lcom/android/calendar/month/e;JLandroid/widget/TextView;Lcom/android/calendar/month/b;)V

    invoke-virtual {v10, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 583
    invoke-virtual {v8}, Lcom/android/calendar/task/aa;->a()Z

    move-result v2

    if-nez v2, :cond_5

    .line 584
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 587
    :cond_5
    iget-boolean v2, v8, Lcom/android/calendar/task/aa;->g:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/android/calendar/month/e;->a(ZLandroid/widget/TextView;)V

    .line 589
    iget v2, v8, Lcom/android/calendar/task/aa;->h:I

    if-nez v2, :cond_7

    .line 590
    const/16 v2, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 595
    :goto_5
    iget v2, v8, Lcom/android/calendar/task/aa;->f:I

    if-nez v2, :cond_8

    .line 596
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 597
    const v2, 0x7f02013c

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 604
    :goto_6
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v14, v2}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_2

    .line 562
    :catch_0
    move-exception v2

    .line 563
    const/4 v2, 0x0

    move-object v8, v2

    goto/16 :goto_3

    .line 572
    :cond_6
    const-string v2, ""

    invoke-static {v6, v2}, Lcom/android/calendar/month/a;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 592
    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 598
    :cond_8
    iget v2, v8, Lcom/android/calendar/task/aa;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    .line 599
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 600
    const v2, 0x7f02013b

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_6

    .line 602
    :cond_9
    const/16 v2, 0x8

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_6

    .line 607
    :cond_a
    iget-object v2, v13, Lcom/android/calendar/month/g;->d:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 608
    iget-object v2, v13, Lcom/android/calendar/month/g;->f:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 609
    iget-object v2, v13, Lcom/android/calendar/month/g;->h:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 610
    iget-object v2, v13, Lcom/android/calendar/month/g;->b:Landroid/widget/CheckBox;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 611
    iget-object v2, v13, Lcom/android/calendar/month/g;->b:Landroid/widget/CheckBox;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 615
    :try_start_1
    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/month/e;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v15, v2

    .line 620
    :goto_7
    iget-object v2, v13, Lcom/android/calendar/month/g;->c:Landroid/widget/ImageView;

    if-eqz v2, :cond_c

    if-eqz v15, :cond_c

    .line 621
    iget-object v2, v13, Lcom/android/calendar/month/g;->c:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 622
    invoke-static {}, Lcom/android/calendar/dz;->x()Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->c:Landroid/content/Context;

    if-eqz v2, :cond_c

    .line 623
    const/4 v2, 0x0

    .line 624
    iget-wide v8, v15, Lcom/android/calendar/dh;->r:J

    const-wide/16 v10, 0x2

    cmp-long v5, v8, v10

    if-nez v5, :cond_10

    .line 625
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/e;->c:Landroid/content/Context;

    iget-object v2, v15, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v7, v2}, Lcom/android/calendar/eb;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 631
    :cond_b
    :goto_8
    if-eqz v2, :cond_c

    .line 632
    iget-object v5, v13, Lcom/android/calendar/month/g;->c:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 633
    iget-object v5, v13, Lcom/android/calendar/month/g;->c:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 634
    iget-object v2, v13, Lcom/android/calendar/month/g;->d:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 638
    :cond_c
    if-eqz v15, :cond_1

    .line 641
    iget v2, v15, Lcom/android/calendar/dh;->c:I

    .line 642
    if-nez v2, :cond_d

    .line 643
    const v2, 0x7f0b0073

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 645
    :cond_d
    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 651
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/android/calendar/month/e;->a(ZLandroid/widget/TextView;)V

    .line 652
    iget-object v2, v15, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_11

    iget-object v2, v15, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_11

    .line 653
    iget-object v2, v15, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-static {v6, v2}, Lcom/android/calendar/month/a;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 660
    :goto_9
    const/4 v10, 0x0

    .line 661
    iget-boolean v2, v15, Lcom/android/calendar/dh;->f:Z

    if-eqz v2, :cond_e

    .line 662
    const/4 v10, 0x1

    .line 665
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v3

    iget-wide v4, v15, Lcom/android/calendar/dh;->m:J

    iget-wide v6, v15, Lcom/android/calendar/dh;->n:J

    iget-boolean v8, v15, Lcom/android/calendar/dh;->f:Z

    const/4 v9, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->d(Lcom/android/calendar/month/a;)Lcom/android/calendar/d/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->c()Z

    move-result v2

    if-eqz v2, :cond_12

    iget-boolean v2, v15, Lcom/android/calendar/dh;->M:Z

    if-eqz v2, :cond_12

    const/4 v12, 0x1

    :goto_a
    invoke-static/range {v3 .. v12}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJZZZZZ)Ljava/lang/String;

    move-result-object v2

    .line 669
    iget-object v3, v15, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    if-eqz v3, :cond_13

    iget-object v3, v15, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-eqz v3, :cond_13

    .line 670
    iget-object v3, v15, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/android/calendar/month/a;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 671
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 676
    :goto_b
    iget-boolean v3, v15, Lcom/android/calendar/dh;->o:Z

    if-eqz v3, :cond_14

    .line 677
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 683
    :goto_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    iget-wide v4, v15, Lcom/android/calendar/dh;->y:J

    iget-object v6, v15, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v3, v0, v4, v5, v6}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;Landroid/widget/ImageView;JLjava/lang/String;)V

    .line 686
    const-string v3, ""

    .line 687
    iget-boolean v4, v15, Lcom/android/calendar/dh;->f:Z

    if-eqz v4, :cond_15

    .line 688
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-virtual {v2}, Lcom/android/calendar/month/a;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f018b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 689
    iget-object v3, v13, Lcom/android/calendar/month/g;->d:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lcom/android/calendar/month/a;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 731
    :cond_f
    :goto_d
    iget v2, v15, Lcom/android/calendar/dh;->B:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1a

    .line 732
    const v2, 0x3ecccccd    # 0.4f

    invoke-virtual {v14, v2}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_2

    .line 616
    :catch_1
    move-exception v2

    .line 617
    const/4 v2, 0x0

    move-object v15, v2

    goto/16 :goto_7

    .line 627
    :cond_10
    iget-boolean v5, v15, Lcom/android/calendar/dh;->K:Z

    if-eqz v5, :cond_b

    invoke-static {}, Lcom/android/calendar/dz;->p()Z

    move-result v5

    if-nez v5, :cond_b

    .line 628
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/e;->c:Landroid/content/Context;

    iget-wide v8, v15, Lcom/android/calendar/dh;->b:J

    invoke-virtual {v2, v5, v8, v9}, Lcom/android/calendar/eb;->a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto/16 :goto_8

    .line 655
    :cond_11
    const-string v2, ""

    invoke-static {v6, v2}, Lcom/android/calendar/month/a;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 665
    :cond_12
    const/4 v12, 0x0

    goto/16 :goto_a

    .line 673
    :cond_13
    const/16 v3, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_b

    .line 679
    :cond_14
    const/16 v3, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_c

    .line 692
    :cond_15
    const-string v4, " - "

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 693
    const/4 v4, 0x0

    aget-object v4, v2, v4

    .line 694
    array-length v5, v2

    const/4 v6, 0x2

    if-ne v5, v6, :cond_16

    const/4 v5, 0x1

    aget-object v2, v2, v5

    .line 695
    :goto_e
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 697
    iget v6, v15, Lcom/android/calendar/dh;->i:I

    iget v7, v15, Lcom/android/calendar/dh;->j:I

    if-eq v6, v7, :cond_19

    .line 698
    new-instance v6, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v7}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 699
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v7}, Lcom/android/calendar/month/a;->e(Lcom/android/calendar/month/a;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 700
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v7}, Lcom/android/calendar/month/a;->e(Lcom/android/calendar/month/a;)J

    move-result-wide v8

    iget-wide v6, v6, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v6

    .line 702
    iget v7, v15, Lcom/android/calendar/dh;->i:I

    if-ne v6, v7, :cond_17

    .line 703
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lcom/android/calendar/agenda/r;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_f
    move-object v2, v3

    .line 724
    :goto_10
    iget-object v3, v13, Lcom/android/calendar/month/g;->d:Landroid/widget/TextView;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 726
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_f

    .line 727
    iget-object v3, v13, Lcom/android/calendar/month/g;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_d

    .line 694
    :cond_16
    const/4 v5, 0x0

    aget-object v2, v2, v5

    goto :goto_e

    .line 707
    :cond_17
    iget v4, v15, Lcom/android/calendar/dh;->j:I

    if-ne v6, v4, :cond_18

    .line 708
    sget-object v4, Lcom/android/calendar/agenda/r;->a:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_f

    .line 713
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-virtual {v2}, Lcom/android/calendar/month/a;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f018b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_f

    .line 716
    :cond_19
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v6, Lcom/android/calendar/agenda/r;->a:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 722
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/e;->d:Lcom/android/calendar/month/a;

    invoke-static {v3}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v3

    const v6, 0x7f0f0036

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    const/4 v4, 0x1

    aput-object v2, v7, v4

    invoke-virtual {v3, v6, v7}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_10

    .line 734
    :cond_1a
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v14, v2}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_2

    :cond_1b
    move-object v13, v2

    move-object v14, v3

    goto/16 :goto_1

    :cond_1c
    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_0
.end method
