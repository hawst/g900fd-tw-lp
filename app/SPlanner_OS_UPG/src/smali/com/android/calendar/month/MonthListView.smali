.class public Lcom/android/calendar/month/MonthListView;
.super Landroid/widget/ListView;
.source "MonthListView.java"


# instance fields
.field final a:I

.field b:Lcom/android/calendar/month/k;

.field c:I

.field d:I

.field e:Landroid/app/Activity;

.field private f:Lcom/android/calendar/al;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 37
    const/16 v0, 0x32

    iput v0, p0, Lcom/android/calendar/month/MonthListView;->a:I

    .line 55
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthListView;->a(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/16 v0, 0x32

    iput v0, p0, Lcom/android/calendar/month/MonthListView;->a:I

    .line 60
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthListView;->a(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/16 v0, 0x32

    iput v0, p0, Lcom/android/calendar/month/MonthListView;->a:I

    .line 65
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthListView;->a(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 69
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/MonthListView;->f:Lcom/android/calendar/al;

    .line 70
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthListView;->g:Z

    .line 72
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/android/calendar/month/MonthListView;->e:Landroid/app/Activity;

    .line 74
    iget-object v0, p0, Lcom/android/calendar/month/MonthListView;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 75
    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/calendar/month/k;

    if-eqz v1, :cond_0

    .line 78
    check-cast v0, Lcom/android/calendar/month/k;

    iput-object v0, p0, Lcom/android/calendar/month/MonthListView;->b:Lcom/android/calendar/month/k;

    .line 80
    :cond_0
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthListView;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthListView;->f:Lcom/android/calendar/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/MonthListView;->f:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 139
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->draw(Landroid/graphics/Canvas;)V

    .line 141
    :cond_1
    return-void
.end method

.method protected layoutChildren()V
    .locals 2

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthListView;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthListView;->f:Lcom/android/calendar/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/MonthListView;->f:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 146
    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    .line 148
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x32

    const/4 v0, 0x1

    .line 95
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 96
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 97
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 99
    if-nez v1, :cond_1

    .line 100
    iput v3, p0, Lcom/android/calendar/month/MonthListView;->c:I

    .line 101
    iput v4, p0, Lcom/android/calendar/month/MonthListView;->d:I

    .line 122
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 104
    :cond_1
    const/4 v5, 0x2

    if-ne v1, v5, :cond_0

    .line 105
    iget v1, p0, Lcom/android/calendar/month/MonthListView;->c:I

    sub-int v1, v3, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 106
    iget v1, p0, Lcom/android/calendar/month/MonthListView;->d:I

    sub-int v1, v4, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 108
    if-gt v3, v6, :cond_2

    if-le v1, v6, :cond_3

    :cond_2
    move v1, v0

    .line 112
    :goto_1
    if-eqz v1, :cond_0

    .line 113
    if-le v3, v6, :cond_4

    .line 114
    iput-boolean v0, p0, Lcom/android/calendar/month/MonthListView;->h:Z

    goto :goto_0

    :cond_3
    move v1, v2

    .line 108
    goto :goto_1

    .line 116
    :cond_4
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthListView;->h:Z

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 87
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthListView;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthListView;->b:Lcom/android/calendar/month/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/MonthListView;->b:Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    :cond_0
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public sendAccessibilityEvent(I)V
    .locals 1

    .prologue
    .line 129
    const/16 v0, 0x1000

    if-ne p1, v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method
