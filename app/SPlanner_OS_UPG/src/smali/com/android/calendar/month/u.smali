.class Lcom/android/calendar/month/u;
.super Ljava/lang/Object;
.source "MonthByWeekFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/k;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/k;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 264
    monitor-enter p0

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->b(Lcom/android/calendar/month/k;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    monitor-exit p0

    .line 279
    :goto_0
    return-void

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->c(Lcom/android/calendar/month/k;)V

    .line 271
    iget-object v0, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    iget-object v1, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    invoke-static {v1}, Lcom/android/calendar/month/k;->d(Lcom/android/calendar/month/k;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/month/k;->a(Lcom/android/calendar/month/k;Landroid/net/Uri;)Landroid/net/Uri;

    .line 272
    iget-object v0, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->f(Lcom/android/calendar/month/k;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    invoke-static {v3}, Lcom/android/calendar/month/k;->e(Lcom/android/calendar/month/k;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {}, Lcom/android/calendar/dh;->j()[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    invoke-virtual {v5}, Lcom/android/calendar/month/k;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "startDay,startMinute,allDay DESC,title"

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string v0, "MonthFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    const-string v0, "MonthFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Started loader with uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/u;->a:Lcom/android/calendar/month/k;

    invoke-static {v2}, Lcom/android/calendar/month/k;->e(Lcom/android/calendar/month/k;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :cond_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
