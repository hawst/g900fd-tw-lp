.class public Lcom/android/calendar/month/cv;
.super Landroid/view/View;
.source "YearZoomInView.java"


# static fields
.field private static U:Z


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:Z

.field private P:Z

.field private Q:Landroid/graphics/drawable/Drawable;

.field private R:Landroid/graphics/drawable/Drawable;

.field private S:I

.field private T:[Ljava/lang/String;

.field private V:Lcom/android/calendar/ej;

.field private W:Landroid/graphics/drawable/Drawable;

.field protected a:[I

.field private final aa:Landroid/content/Context;

.field b:Z

.field protected c:Lcom/android/calendar/d/g;

.field protected d:Lcom/android/calendar/d/a/a;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/text/format/Time;

.field private g:Landroid/text/format/Time;

.field private h:Landroid/text/format/Time;

.field private i:Lcom/android/calendar/month/cg;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cv;->e:Landroid/graphics/Paint;

    .line 122
    iput-boolean v1, p0, Lcom/android/calendar/month/cv;->O:Z

    .line 124
    iput-boolean v1, p0, Lcom/android/calendar/month/cv;->P:Z

    .line 154
    iput-boolean v1, p0, Lcom/android/calendar/month/cv;->b:Z

    .line 170
    iput-object p1, p0, Lcom/android/calendar/month/cv;->aa:Landroid/content/Context;

    .line 171
    iget-object v0, p0, Lcom/android/calendar/month/cv;->aa:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->S:I

    .line 173
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 174
    const v0, 0x7f020113

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cv;->W:Landroid/graphics/drawable/Drawable;

    .line 176
    const v0, 0x7f0c03d3

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->j:I

    .line 177
    const v0, 0x7f0c03c9

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->l:I

    .line 178
    const v0, 0x7f0c03c6

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->k:I

    .line 180
    const v0, 0x7f0c03ca

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->m:I

    .line 181
    const v0, 0x7f0c03c7

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->n:I

    .line 182
    const v0, 0x7f0c03c8

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->o:I

    .line 183
    const v0, 0x7f0c03d4

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->q:I

    .line 184
    const v0, 0x7f0c03c4

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->r:I

    .line 185
    const v0, 0x7f0c03c5

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->s:I

    .line 186
    const v0, 0x7f0c03ce

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->p:I

    .line 187
    const v0, 0x7f0c03cf

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->t:I

    .line 188
    const v0, 0x7f0c03d0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->u:I

    .line 189
    const v0, 0x7f0c03d1

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->v:I

    .line 191
    const v0, 0x7f0d002c

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->H:I

    .line 195
    const v0, 0x7f0d002b

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->I:I

    .line 200
    const v0, 0x7f0c03c2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->w:I

    .line 201
    const v0, 0x7f0c03c3

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->x:I

    .line 202
    const v0, 0x7f0c03c1

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->y:I

    .line 203
    const v0, 0x7f0c03cc

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->z:I

    .line 204
    const v0, 0x7f0c03d2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->A:I

    .line 205
    const v0, 0x7f0c03cd

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->B:I

    .line 206
    const v0, 0x7f0c03d6

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->C:I

    .line 207
    const v0, 0x7f0c03d7

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->D:I

    .line 208
    const v0, 0x7f0c03d8

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->E:I

    .line 209
    const v0, 0x7f0c03d5

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->F:I

    .line 211
    const v0, 0x7f0b012a

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->J:I

    .line 212
    const v0, 0x7f0b0123

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->K:I

    .line 213
    const v0, 0x7f0b00ce

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->L:I

    .line 216
    const v0, 0x7f0b00e6

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->M:I

    .line 217
    const v0, 0x7f0b0129

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cv;->N:I

    .line 219
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 220
    iget-object v0, p0, Lcom/android/calendar/month/cv;->aa:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 221
    iget v0, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v3, 0x1e0

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/month/cv;->b:Z

    .line 223
    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->a()V

    .line 225
    const v0, 0x7f0200ae

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cv;->Q:Landroid/graphics/drawable/Drawable;

    .line 226
    const v0, 0x7f0200ad

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cv;->R:Landroid/graphics/drawable/Drawable;

    .line 227
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 229
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/cv;->f:Landroid/text/format/Time;

    .line 231
    iget-object v1, p0, Lcom/android/calendar/month/cv;->aa:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 232
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/cv;->g:Landroid/text/format/Time;

    .line 237
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/month/cv;->g:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 238
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/cv;->h:Landroid/text/format/Time;

    .line 239
    iget-object v0, p0, Lcom/android/calendar/month/cv;->h:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 241
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cv;->c:Lcom/android/calendar/d/g;

    .line 242
    iget-object v0, p0, Lcom/android/calendar/month/cv;->c:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/android/calendar/month/cv;->c:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cv;->d:Lcom/android/calendar/d/a/a;

    .line 244
    iget-object v0, p0, Lcom/android/calendar/month/cv;->aa:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->s(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cv;->P:Z

    .line 247
    :cond_0
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/android/calendar/month/cv;->c:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    invoke-static {p1}, Lcom/android/calendar/ej;->a(Landroid/content/Context;)Lcom/android/calendar/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cv;->V:Lcom/android/calendar/ej;

    .line 252
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 221
    goto :goto_0

    .line 234
    :cond_3
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/cv;->g:Landroid/text/format/Time;

    goto :goto_1
.end method

.method private a(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;II)V
    .locals 7

    .prologue
    .line 399
    iget-object v0, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v0, p2, p3}, Lcom/android/calendar/month/cg;->b(II)Z

    move-result v0

    .line 402
    iget-object v1, p0, Lcom/android/calendar/month/cv;->f:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/month/cv;->f:Landroid/text/format/Time;

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 404
    iget v2, p0, Lcom/android/calendar/month/cv;->G:I

    iget-object v3, p0, Lcom/android/calendar/month/cv;->f:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->month:I

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    if-ne v1, v2, :cond_0

    .line 408
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    iget-object v2, p0, Lcom/android/calendar/month/cv;->g:Landroid/text/format/Time;

    invoke-virtual {v1, p2, p3, v2}, Lcom/android/calendar/month/cg;->a(IILandroid/text/format/Time;)Z

    move-result v1

    .line 409
    if-eqz p2, :cond_1

    .line 410
    iget v2, p0, Lcom/android/calendar/month/cv;->k:I

    iget v3, p0, Lcom/android/calendar/month/cv;->n:I

    add-int/2addr v2, v3

    mul-int/2addr v2, p2

    add-int/2addr p7, v2

    .line 413
    :cond_1
    if-eqz p3, :cond_2

    .line 414
    add-int/lit8 v2, p3, -0x1

    iget v3, p0, Lcom/android/calendar/month/cv;->l:I

    iget v4, p0, Lcom/android/calendar/month/cv;->m:I

    add-int/2addr v3, v4

    mul-int/2addr v2, v3

    add-int/2addr p6, v2

    .line 418
    :cond_2
    iget v2, p0, Lcom/android/calendar/month/cv;->I:I

    int-to-float v2, v2

    invoke-virtual {p5, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 419
    iget v2, p0, Lcom/android/calendar/month/cv;->K:I

    invoke-virtual {p5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 421
    const/4 v2, 0x1

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {p5, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 423
    sget-boolean v2, Lcom/android/calendar/month/cv;->U:Z

    if-eqz v2, :cond_3

    .line 424
    const/4 v2, 0x0

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {p5, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 427
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/month/cv;->a:[I

    add-int/lit8 v3, p3, -0x1

    aget v2, v2, v3

    invoke-virtual {p5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 428
    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p5, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 431
    iget-object v2, p0, Lcom/android/calendar/month/cv;->d:Lcom/android/calendar/d/a/a;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/calendar/month/cv;->c:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/calendar/month/cv;->c:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 433
    iget-object v2, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->a()I

    move-result v2

    .line 434
    iget-object v3, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v3}, Lcom/android/calendar/month/cg;->b()I

    move-result v3

    .line 435
    iget-object v4, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v4, p2, p3}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v4

    .line 436
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 437
    invoke-virtual {v5, v4, v3, v2}, Landroid/text/format/Time;->set(III)V

    .line 439
    iget-object v2, p0, Lcom/android/calendar/month/cv;->d:Lcom/android/calendar/d/a/a;

    invoke-virtual {v2, v5}, Lcom/android/calendar/d/a/a;->a(Landroid/text/format/Time;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/android/calendar/month/cv;->P:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/calendar/month/cv;->d:Lcom/android/calendar/d/a/a;

    invoke-virtual {v2, v5}, Lcom/android/calendar/d/a/a;->c(Landroid/text/format/Time;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 440
    :cond_4
    iget v2, p0, Lcom/android/calendar/month/cv;->M:I

    invoke-virtual {p5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 445
    :cond_5
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, "JAPAN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 446
    iget-object v2, p0, Lcom/android/calendar/month/cv;->c:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->e()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 447
    iget-object v2, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->a()I

    move-result v2

    .line 448
    iget-object v3, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v3}, Lcom/android/calendar/month/cg;->b()I

    move-result v3

    .line 449
    iget-object v4, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v4, p2, p3}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v4

    .line 450
    invoke-static {v2, v3, v4}, Lcom/android/calendar/hj;->a(III)I

    move-result v5

    .line 451
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 452
    invoke-virtual {v6, v4, v3, v2}, Landroid/text/format/Time;->set(III)V

    .line 453
    iput v5, v6, Landroid/text/format/Time;->weekDay:I

    .line 454
    iget-object v2, p0, Lcom/android/calendar/month/cv;->V:Lcom/android/calendar/ej;

    invoke-virtual {v2, v6}, Lcom/android/calendar/ej;->a(Landroid/text/format/Time;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/android/calendar/month/cv;->V:Lcom/android/calendar/ej;

    invoke-virtual {v2, v6}, Lcom/android/calendar/ej;->b(Landroid/text/format/Time;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 455
    :cond_6
    iget v2, p0, Lcom/android/calendar/month/cv;->M:I

    invoke-virtual {p5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 461
    :cond_7
    iget v2, p0, Lcom/android/calendar/month/cv;->k:I

    add-int/2addr v2, p7

    iget v3, p0, Lcom/android/calendar/month/cv;->k:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/month/cv;->I:I

    div-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    .line 463
    if-eqz v0, :cond_9

    .line 464
    if-eqz v1, :cond_8

    .line 465
    iget-object v0, p0, Lcom/android/calendar/month/cv;->Q:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/android/calendar/month/cv;->C:I

    sub-int v1, p6, v1

    iget v3, p0, Lcom/android/calendar/month/cv;->E:I

    sub-int v3, v2, v3

    iget v4, p0, Lcom/android/calendar/month/cv;->D:I

    add-int/2addr v4, p6

    iget v5, p0, Lcom/android/calendar/month/cv;->F:I

    add-int/2addr v5, v2

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 467
    iget-object v0, p0, Lcom/android/calendar/month/cv;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 468
    iget v0, p0, Lcom/android/calendar/month/cv;->N:I

    invoke-virtual {p5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 470
    :cond_8
    iget-boolean v0, p0, Lcom/android/calendar/month/cv;->O:Z

    if-eqz v0, :cond_b

    .line 471
    const-string v0, "%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v4, p2, p3}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    int-to-float v1, p6

    int-to-float v2, v2

    invoke-virtual {p4, v0, v1, v2, p5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 477
    :cond_9
    :goto_0
    sget-boolean v0, Lcom/android/calendar/month/cv;->U:Z

    if-eqz v0, :cond_a

    .line 478
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p5, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 486
    :cond_a
    return-void

    .line 473
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v0, p2, p3}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    int-to-float v1, p6

    int-to-float v2, v2

    invoke-virtual {p4, v0, v1, v2, p5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 269
    iget-object v0, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    if-eqz v0, :cond_9

    .line 270
    iget-object v5, p0, Lcom/android/calendar/month/cv;->e:Landroid/graphics/Paint;

    .line 272
    new-instance v1, Landroid/text/format/Time;

    const-string v0, "UTC"

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 273
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->b()I

    move-result v2

    iget-object v3, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v3}, Lcom/android/calendar/month/cg;->a()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/text/format/Time;->set(III)V

    .line 274
    iget-object v0, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v0}, Lcom/android/calendar/month/cg;->f()V

    .line 275
    iget v0, p0, Lcom/android/calendar/month/cv;->G:I

    if-lez v0, :cond_0

    .line 276
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/calendar/month/cv;->G:I

    if-ge v0, v2, :cond_0

    .line 277
    iget-object v2, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->e()V

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :cond_0
    const/4 v8, 0x0

    .line 288
    const/4 v2, 0x0

    .line 291
    const/4 v0, 0x1

    .line 293
    iget-boolean v3, p0, Lcom/android/calendar/month/cv;->b:Z

    if-eqz v3, :cond_1

    .line 294
    const/4 v0, 0x3

    .line 296
    :cond_1
    iget v3, p0, Lcom/android/calendar/month/cv;->S:I

    add-int/lit8 v3, v3, 0x6

    rem-int/lit8 v3, v3, 0x7

    .line 298
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/String;

    iput-object v4, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    .line 300
    invoke-direct {p0, v3}, Lcom/android/calendar/month/cv;->setYearWeekday(I)V

    .line 302
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 303
    iget-wide v10, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v10, v11}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v9

    .line 307
    iget-object v1, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/android/calendar/month/cg;->b(I)I

    move-result v10

    .line 309
    iget-object v1, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v1}, Lcom/android/calendar/month/cg;->d()I

    move-result v1

    .line 310
    iget-object v3, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v3, v1}, Lcom/android/calendar/month/cg;->a(I)I

    move-result v1

    add-int/lit8 v11, v1, 0x1

    .line 312
    iget v1, p0, Lcom/android/calendar/month/cv;->w:I

    add-int v7, v8, v1

    .line 314
    const/4 v1, 0x4

    if-gt v11, v1, :cond_2

    .line 315
    iget v1, p0, Lcom/android/calendar/month/cv;->u:I

    add-int/2addr v1, v2

    move v6, v1

    .line 326
    :goto_1
    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 327
    iget v1, p0, Lcom/android/calendar/month/cv;->H:I

    int-to-float v1, v1

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 328
    const/4 v1, 0x1

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 329
    iget v1, p0, Lcom/android/calendar/month/cv;->J:I

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 330
    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 333
    const/16 v3, 0xa

    .line 334
    iget-object v1, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v1}, Lcom/android/calendar/month/cg;->b()I

    move-result v1

    const/4 v2, 0x3

    const/4 v4, 0x1

    invoke-static {v1, v2, v4}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-le v1, v2, :cond_4

    .line 337
    const/16 v1, 0xb

    .line 346
    :goto_2
    const-string v2, ""

    .line 347
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 348
    iget-object v2, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->b()I

    move-result v2

    const/4 v4, 0x3

    const/4 v12, 0x0

    invoke-static {v2, v4, v12}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v2

    .line 353
    :goto_3
    iget v4, p0, Lcom/android/calendar/month/cv;->z:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v8

    add-int/2addr v1, v4

    iget v4, p0, Lcom/android/calendar/month/cv;->j:I

    div-int/lit8 v4, v4, 0x3

    add-int/2addr v1, v4

    int-to-float v1, v1

    iget v4, p0, Lcom/android/calendar/month/cv;->p:I

    mul-int/lit8 v4, v4, 0x2

    div-int/lit8 v4, v4, 0x3

    add-int/2addr v4, v6

    int-to-float v4, v4

    invoke-virtual {p1, v2, v1, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 358
    iget v1, p0, Lcom/android/calendar/month/cv;->p:I

    add-int v12, v6, v1

    .line 359
    const v1, -0xb4b4b5

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 360
    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 361
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 362
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 363
    iget v1, p0, Lcom/android/calendar/month/cv;->z:I

    add-int/2addr v1, v8

    add-int/2addr v1, v3

    int-to-float v1, v1

    int-to-float v2, v12

    iget v4, p0, Lcom/android/calendar/month/cv;->j:I

    add-int/2addr v4, v8

    iget v13, p0, Lcom/android/calendar/month/cv;->z:I

    mul-int/lit8 v13, v13, 0x3

    sub-int/2addr v4, v13

    iget v13, p0, Lcom/android/calendar/month/cv;->B:I

    add-int/2addr v4, v13

    add-int/2addr v3, v4

    int-to-float v3, v3

    add-int/2addr v0, v12

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 366
    iget v0, p0, Lcom/android/calendar/month/cv;->s:I

    add-int/2addr v12, v0

    .line 369
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 370
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 371
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 374
    sub-int v0, v9, v10

    .line 377
    sub-int v1, v9, v10

    .line 379
    const/4 v0, 0x0

    move v2, v7

    :goto_4
    const/4 v3, 0x7

    if-ge v0, v3, :cond_7

    .line 380
    iget v3, p0, Lcom/android/calendar/month/cv;->I:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 381
    iget-object v3, p0, Lcom/android/calendar/month/cv;->a:[I

    aget v3, v3, v0

    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 382
    iget-object v3, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    aget-object v3, v3, v0

    iget v4, p0, Lcom/android/calendar/month/cv;->y:I

    add-int/2addr v2, v4

    int-to-float v4, v2

    iget v7, p0, Lcom/android/calendar/month/cv;->x:I

    add-int/2addr v7, v6

    int-to-float v7, v7

    invoke-virtual {p1, v3, v4, v7, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 379
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 316
    :cond_2
    const/4 v1, 0x6

    if-lt v11, v1, :cond_3

    .line 317
    iget v1, p0, Lcom/android/calendar/month/cv;->v:I

    add-int/2addr v1, v2

    .line 318
    iget v2, p0, Lcom/android/calendar/month/cv;->o:I

    iput v2, p0, Lcom/android/calendar/month/cv;->n:I

    move v6, v1

    goto/16 :goto_1

    .line 320
    :cond_3
    iget v1, p0, Lcom/android/calendar/month/cv;->t:I

    add-int/2addr v1, v2

    move v6, v1

    goto/16 :goto_1

    .line 338
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v1}, Lcom/android/calendar/month/cg;->b()I

    move-result v1

    const/4 v2, 0x3

    const/4 v4, 0x1

    invoke-static {v1, v2, v4}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_5

    .line 339
    const/16 v1, 0x9

    goto/16 :goto_2

    .line 343
    :cond_5
    const/4 v1, 0x6

    goto/16 :goto_2

    .line 350
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->b()I

    move-result v2

    const/4 v4, 0x3

    const/4 v12, 0x1

    invoke-static {v2, v4, v12}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 387
    :cond_7
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cv;->O:Z

    .line 388
    const/4 v2, 0x0

    :goto_5
    if-ge v2, v11, :cond_9

    .line 389
    const/4 v3, 0x1

    :goto_6
    const/16 v0, 0x8

    if-ge v3, v0, :cond_8

    .line 390
    iget-object v5, p0, Lcom/android/calendar/month/cv;->e:Landroid/graphics/Paint;

    iget v0, p0, Lcom/android/calendar/month/cv;->r:I

    add-int/2addr v0, v8

    iget v4, p0, Lcom/android/calendar/month/cv;->q:I

    add-int v6, v0, v4

    iget v0, p0, Lcom/android/calendar/month/cv;->A:I

    add-int v7, v12, v0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/month/cv;->a(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;II)V

    .line 392
    add-int/lit8 v1, v1, 0x1

    .line 389
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 388
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 396
    :cond_9
    return-void
.end method

.method private setYearWeekday(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 491
    const-string v0, "persist.sys.language"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 492
    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0x6

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f012e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 494
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0x7

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f012f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 495
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0x8

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0130

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 496
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0x9

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0131

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 497
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0xa

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0132

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 498
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0xb

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0133

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 499
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0xc

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0134

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 523
    :goto_0
    return-void

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 506
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    const/4 v1, 0x1

    add-int/lit8 v2, p1, 0x2

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 509
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    const/4 v1, 0x2

    add-int/lit8 v2, p1, 0x3

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 512
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    const/4 v1, 0x3

    add-int/lit8 v2, p1, 0x4

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 515
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    const/4 v1, 0x4

    add-int/lit8 v2, p1, 0x5

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 518
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    const/4 v1, 0x5

    add-int/lit8 v2, p1, 0x6

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 521
    iget-object v0, p0, Lcom/android/calendar/month/cv;->T:[Ljava/lang/String;

    const/4 v1, 0x6

    add-int/lit8 v2, p1, 0x7

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 540
    const/4 v0, 0x3

    new-array v1, v0, [I

    .line 543
    const-string v0, "XXXXXXR"

    .line 545
    const/4 v2, 0x0

    iget v3, p0, Lcom/android/calendar/month/cv;->K:I

    aput v3, v1, v2

    .line 546
    const/4 v2, 0x1

    iget v3, p0, Lcom/android/calendar/month/cv;->L:I

    aput v3, v1, v2

    .line 547
    const/4 v2, 0x2

    iget v3, p0, Lcom/android/calendar/month/cv;->M:I

    aput v3, v1, v2

    .line 550
    :try_start_0
    invoke-static {}, Lcom/android/calendar/dz;->e()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 554
    :goto_0
    iget v2, p0, Lcom/android/calendar/month/cv;->S:I

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cv;->a:[I

    .line 555
    return-void

    .line 551
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 258
    sget-boolean v0, Lcom/android/calendar/month/cv;->U:Z

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/android/calendar/month/cv;->W:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 260
    iget-object v0, p0, Lcom/android/calendar/month/cv;->W:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 264
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cv;->a(Landroid/graphics/Canvas;)V

    .line 265
    return-void
.end method

.method public setCurrentMonth(I)V
    .locals 0

    .prologue
    .line 536
    iput p1, p0, Lcom/android/calendar/month/cv;->G:I

    .line 537
    return-void
.end method

.method public setHoveredTime(Landroid/text/format/Time;)V
    .locals 5

    .prologue
    .line 527
    if-eqz p1, :cond_0

    .line 528
    iget-object v0, p0, Lcom/android/calendar/month/cv;->f:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 529
    new-instance v0, Lcom/android/calendar/month/cg;

    iget v1, p1, Landroid/text/format/Time;->year:I

    iget v2, p1, Landroid/text/format/Time;->month:I

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    iget v4, p0, Lcom/android/calendar/month/cv;->S:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendar/month/cg;-><init>(IIII)V

    iput-object v0, p0, Lcom/android/calendar/month/cv;->i:Lcom/android/calendar/month/cg;

    .line 530
    iget-object v0, p0, Lcom/android/calendar/month/cv;->h:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 531
    invoke-virtual {p0}, Lcom/android/calendar/month/cv;->invalidate()V

    .line 533
    :cond_0
    return-void
.end method
