.class Lcom/android/calendar/month/bv;
.super Ljava/lang/Object;
.source "MonthWeekEventsView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/bo;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bo;)V
    .locals 0

    .prologue
    .line 3663
    iput-object p1, p0, Lcom/android/calendar/month/bv;->a:Lcom/android/calendar/month/bo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 3680
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 3672
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/bo;->ah:Z

    .line 3673
    iget-object v0, p0, Lcom/android/calendar/month/bv;->a:Lcom/android/calendar/month/bo;

    iget-object v1, p0, Lcom/android/calendar/month/bv;->a:Lcom/android/calendar/month/bo;

    iget-object v1, v1, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/month/bo;->c(Lcom/android/calendar/month/bo;I)I

    .line 3674
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Lcom/android/calendar/month/bo;->b(F)F

    .line 3675
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 3685
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 3666
    iget-object v0, p0, Lcom/android/calendar/month/bv;->a:Lcom/android/calendar/month/bo;

    iget-object v1, p0, Lcom/android/calendar/month/bv;->a:Lcom/android/calendar/month/bo;

    invoke-static {v1}, Lcom/android/calendar/month/bo;->c(Lcom/android/calendar/month/bo;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/month/bo;->c(Lcom/android/calendar/month/bo;I)I

    .line 3667
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/calendar/month/bo;->b(F)F

    .line 3668
    return-void
.end method
