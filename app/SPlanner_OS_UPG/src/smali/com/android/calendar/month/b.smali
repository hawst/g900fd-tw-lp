.class Lcom/android/calendar/month/b;
.super Ljava/lang/Object;
.source "EventInfoListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/a;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/a;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/android/calendar/month/b;->a:Lcom/android/calendar/month/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 190
    iget-object v0, p0, Lcom/android/calendar/month/b;->a:Lcom/android/calendar/month/a;

    invoke-static {v0}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->r(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 192
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.EDIT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    iget-object v3, p0, Lcom/android/calendar/month/b;->a:Lcom/android/calendar/month/a;

    invoke-static {v3}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 194
    const-string v3, "beginTime"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 196
    const-string v0, "allDay"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 197
    const-string v0, "launch_from_inside"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 198
    iget-object v0, p0, Lcom/android/calendar/month/b;->a:Lcom/android/calendar/month/a;

    invoke-static {v0}, Lcom/android/calendar/month/a;->a(Lcom/android/calendar/month/a;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 199
    iget-object v0, p0, Lcom/android/calendar/month/b;->a:Lcom/android/calendar/month/a;

    invoke-virtual {v0}, Lcom/android/calendar/month/a;->dismissAllowingStateLoss()V

    .line 200
    return-void
.end method
