.class public Lcom/android/calendar/month/cs;
.super Landroid/app/DialogFragment;
.source "YearZoomInFragment.java"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/text/format/Time;

.field private h:I

.field private i:I

.field private j:Landroid/content/res/Resources;

.field private k:Lcom/android/calendar/month/cv;

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 45
    iput v0, p0, Lcom/android/calendar/month/cs;->a:I

    .line 47
    iput v0, p0, Lcom/android/calendar/month/cs;->b:I

    .line 49
    iput v1, p0, Lcom/android/calendar/month/cs;->c:I

    .line 51
    iput v1, p0, Lcom/android/calendar/month/cs;->d:I

    .line 53
    iput v1, p0, Lcom/android/calendar/month/cs;->e:I

    .line 55
    iput v1, p0, Lcom/android/calendar/month/cs;->f:I

    .line 57
    iput-object v2, p0, Lcom/android/calendar/month/cs;->g:Landroid/text/format/Time;

    .line 59
    iput v0, p0, Lcom/android/calendar/month/cs;->h:I

    .line 61
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/cs;->i:I

    .line 65
    iput-object v2, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    .line 67
    iput-boolean v1, p0, Lcom/android/calendar/month/cs;->l:Z

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 45
    iput v0, p0, Lcom/android/calendar/month/cs;->a:I

    .line 47
    iput v0, p0, Lcom/android/calendar/month/cs;->b:I

    .line 49
    iput v1, p0, Lcom/android/calendar/month/cs;->c:I

    .line 51
    iput v1, p0, Lcom/android/calendar/month/cs;->d:I

    .line 53
    iput v1, p0, Lcom/android/calendar/month/cs;->e:I

    .line 55
    iput v1, p0, Lcom/android/calendar/month/cs;->f:I

    .line 57
    iput-object v2, p0, Lcom/android/calendar/month/cs;->g:Landroid/text/format/Time;

    .line 59
    iput v0, p0, Lcom/android/calendar/month/cs;->h:I

    .line 61
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/cs;->i:I

    .line 65
    iput-object v2, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    .line 67
    iput-boolean v1, p0, Lcom/android/calendar/month/cs;->l:Z

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 75
    iput-boolean p2, p0, Lcom/android/calendar/month/cs;->l:Z

    .line 76
    const v1, 0x7f0c03d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cs;->e:I

    .line 77
    const v1, 0x7f0c03cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cs;->f:I

    .line 78
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/month/cs;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/calendar/month/cs;->g:Landroid/text/format/Time;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/android/calendar/month/cs;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_0

    .line 212
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 214
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 215
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 217
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 218
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 220
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 223
    const v2, 0x7f100045

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 224
    const v2, 0x800033

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 226
    const v2, 0x3ecccccd    # 0.4f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 227
    iget v2, p0, Lcom/android/calendar/month/cs;->e:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 228
    iget v2, p0, Lcom/android/calendar/month/cs;->f:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 230
    iget v2, p0, Lcom/android/calendar/month/cs;->a:I

    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 231
    iget v2, p0, Lcom/android/calendar/month/cs;->b:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 233
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 235
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/month/cs;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/android/calendar/month/cs;->h:I

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/month/cs;)Lcom/android/calendar/month/cv;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 267
    iput p1, p0, Lcom/android/calendar/month/cs;->h:I

    .line 269
    return-void
.end method

.method public a(Landroid/content/Context;II)V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 238
    iput p2, p0, Lcom/android/calendar/month/cs;->a:I

    .line 239
    iput p3, p0, Lcom/android/calendar/month/cs;->b:I

    .line 241
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cs;->j:Landroid/content/res/Resources;

    .line 243
    iget-object v0, p0, Lcom/android/calendar/month/cs;->j:Landroid/content/res/Resources;

    const v1, 0x7f0c03d9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cs;->c:I

    .line 244
    iget-object v0, p0, Lcom/android/calendar/month/cs;->j:Landroid/content/res/Resources;

    const v1, 0x7f0c03da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cs;->d:I

    .line 245
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/month/cs;->i:I

    .line 247
    iget v0, p0, Lcom/android/calendar/month/cs;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 248
    iget v0, p0, Lcom/android/calendar/month/cs;->a:I

    iget v1, p0, Lcom/android/calendar/month/cs;->c:I

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/cs;->a:I

    .line 249
    iget v0, p0, Lcom/android/calendar/month/cs;->b:I

    iget v1, p0, Lcom/android/calendar/month/cs;->d:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/cs;->b:I

    .line 250
    iget v0, p0, Lcom/android/calendar/month/cs;->h:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/month/cs;->h:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/month/cs;->h:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/month/cs;->h:I

    if-ne v0, v2, :cond_1

    .line 251
    :cond_0
    iget v0, p0, Lcom/android/calendar/month/cs;->a:I

    iget v1, p0, Lcom/android/calendar/month/cs;->c:I

    mul-int/lit8 v1, v1, 0x9

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/cs;->a:I

    .line 260
    :cond_1
    :goto_0
    return-void

    .line 253
    :cond_2
    iget v0, p0, Lcom/android/calendar/month/cs;->a:I

    iget v1, p0, Lcom/android/calendar/month/cs;->c:I

    mul-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/cs;->a:I

    .line 254
    iget v0, p0, Lcom/android/calendar/month/cs;->b:I

    iget v1, p0, Lcom/android/calendar/month/cs;->d:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/cs;->b:I

    .line 256
    iget v0, p0, Lcom/android/calendar/month/cs;->h:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/android/calendar/month/cs;->h:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/android/calendar/month/cs;->h:I

    if-ne v0, v2, :cond_1

    .line 257
    :cond_3
    iget v0, p0, Lcom/android/calendar/month/cs;->a:I

    iget v1, p0, Lcom/android/calendar/month/cs;->c:I

    mul-int/lit8 v1, v1, 0x7

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/cs;->a:I

    goto :goto_0
.end method

.method public a(Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/android/calendar/month/cs;->g:Landroid/text/format/Time;

    .line 264
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 176
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 177
    invoke-direct {p0}, Lcom/android/calendar/month/cs;->a()V

    .line 178
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 84
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/cs;->setStyle(II)V

    .line 95
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/cs;->setStyle(II)V

    .line 96
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 97
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/cs;->g:Landroid/text/format/Time;

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 170
    :goto_0
    return-object v0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    if-nez v0, :cond_1

    .line 108
    new-instance v0, Lcom/android/calendar/month/cv;

    invoke-virtual {p0}, Lcom/android/calendar/month/cs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/calendar/month/cv;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    iget-object v1, p0, Lcom/android/calendar/month/cs;->g:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/cv;->setHoveredTime(Landroid/text/format/Time;)V

    .line 112
    iget-object v0, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    iget v1, p0, Lcom/android/calendar/month/cs;->h:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/cv;->setCurrentMonth(I)V

    .line 114
    iget-object v0, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    new-instance v1, Lcom/android/calendar/month/ct;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/ct;-><init>(Lcom/android/calendar/month/cs;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/cv;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 143
    iget-object v0, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    new-instance v1, Lcom/android/calendar/month/cu;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/cu;-><init>(Lcom/android/calendar/month/cs;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/cv;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 170
    iget-object v0, p0, Lcom/android/calendar/month/cs;->k:Lcom/android/calendar/month/cv;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 204
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 205
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 199
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 200
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 191
    iget-boolean v0, p0, Lcom/android/calendar/month/cs;->l:Z

    if-nez v0, :cond_0

    .line 192
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 194
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 183
    iget-boolean v0, p0, Lcom/android/calendar/month/cs;->l:Z

    if-nez v0, :cond_0

    .line 184
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 186
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 90
    return-void
.end method
