.class Lcom/android/calendar/month/cp;
.super Ljava/lang/Object;
.source "YearView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/cl;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/cl;)V
    .locals 0

    .prologue
    .line 1770
    iput-object p1, p0, Lcom/android/calendar/month/cp;->a:Lcom/android/calendar/month/cl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1773
    iget-object v0, p0, Lcom/android/calendar/month/cp;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v0}, Lcom/android/calendar/month/cl;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1774
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1775
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1776
    const-string v2, "YearViewZoomIn"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1779
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1780
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1782
    :cond_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1786
    :goto_0
    return-void

    .line 1783
    :catch_0
    move-exception v0

    goto :goto_0
.end method
