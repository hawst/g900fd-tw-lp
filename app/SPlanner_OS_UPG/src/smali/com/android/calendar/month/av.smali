.class public Lcom/android/calendar/month/av;
.super Landroid/app/Fragment;
.source "MonthEventListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/android/calendar/ap;


# static fields
.field protected static final a:Ljava/lang/String;

.field private static r:Z

.field private static v:Ljava/lang/String;


# instance fields
.field private b:Landroid/text/format/Time;

.field private c:Landroid/text/format/Time;

.field private d:Landroid/content/AsyncQueryHandler;

.field private e:Ljava/util/ArrayList;

.field private f:Landroid/os/Handler;

.field private g:Landroid/net/Uri;

.field private h:Landroid/net/Uri;

.field private i:Ljava/util/ArrayList;

.field private j:Landroid/widget/ListView;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/android/calendar/month/aj;

.field private m:Lcom/android/calendar/al;

.field private n:Lcom/android/calendar/month/ay;

.field private o:Landroid/content/Context;

.field private p:I

.field private q:J

.field private s:Lcom/android/calendar/gr;

.field private t:Ljava/lang/Runnable;

.field private u:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/android/calendar/month/av;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/month/av;->a:Ljava/lang/String;

    .line 533
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/month/av;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 135
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 76
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    .line 78
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    .line 102
    new-instance v0, Lcom/android/calendar/month/ay;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/ay;-><init>(Lcom/android/calendar/month/av;Lcom/android/calendar/month/aw;)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    .line 129
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/month/av;->q:J

    .line 148
    new-instance v0, Lcom/android/calendar/month/aw;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/aw;-><init>(Lcom/android/calendar/month/av;)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->t:Ljava/lang/Runnable;

    .line 159
    new-instance v0, Lcom/android/calendar/month/ax;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/ax;-><init>(Lcom/android/calendar/month/av;)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->u:Ljava/lang/Runnable;

    .line 136
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 137
    return-void
.end method

.method public constructor <init>(J)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 139
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 76
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    .line 78
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    .line 102
    new-instance v0, Lcom/android/calendar/month/ay;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/ay;-><init>(Lcom/android/calendar/month/av;Lcom/android/calendar/month/aw;)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    .line 129
    iput-wide v2, p0, Lcom/android/calendar/month/av;->q:J

    .line 148
    new-instance v0, Lcom/android/calendar/month/aw;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/aw;-><init>(Lcom/android/calendar/month/av;)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->t:Ljava/lang/Runnable;

    .line 159
    new-instance v0, Lcom/android/calendar/month/ax;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/ax;-><init>(Lcom/android/calendar/month/av;)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->u:Ljava/lang/Runnable;

    .line 140
    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 146
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/month/av;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/calendar/month/av;->g:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/month/av;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->t:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/month/av;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/calendar/month/av;->e:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(Landroid/text/format/Time;)V
    .locals 2

    .prologue
    .line 704
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 705
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 706
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 724
    :cond_0
    return-void
.end method

.method private a(Landroid/text/format/Time;Z)V
    .locals 4

    .prologue
    .line 317
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 318
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 319
    iget-object v1, p0, Lcom/android/calendar/month/av;->l:Lcom/android/calendar/month/aj;

    if-eqz v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/android/calendar/month/av;->l:Lcom/android/calendar/month/aj;

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/aj;->a(I)V

    .line 323
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->a()V

    .line 324
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/month/av;Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/android/calendar/month/av;->a(Landroid/text/format/Time;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/month/av;Lcom/android/calendar/month/ba;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/ba;)V

    return-void
.end method

.method private a(Lcom/android/calendar/month/ba;)V
    .locals 6

    .prologue
    .line 676
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 677
    iget-wide v2, p0, Lcom/android/calendar/month/av;->q:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x258

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 702
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    iput-wide v0, p0, Lcom/android/calendar/month/av;->q:J

    .line 683
    iget-object v0, p0, Lcom/android/calendar/month/av;->i:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/month/av;->p:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/ba;

    .line 684
    if-eqz v0, :cond_0

    .line 688
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    .line 689
    iget-object v0, p0, Lcom/android/calendar/month/av;->o:Landroid/content/Context;

    const-string v1, "enterprise_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 690
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/RestrictionPolicy;->isShareListAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/android/calendar/month/av;->s:Lcom/android/calendar/gr;

    if-eqz v0, :cond_0

    .line 694
    iget v0, p1, Lcom/android/calendar/month/ba;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 695
    iget-object v0, p0, Lcom/android/calendar/month/av;->s:Lcom/android/calendar/gr;

    iget-wide v2, p1, Lcom/android/calendar/month/ba;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/gr;->a(J)V

    goto :goto_0

    .line 697
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/av;->s:Lcom/android/calendar/gr;

    iget-wide v2, p1, Lcom/android/calendar/month/ba;->f:J

    const-wide/16 v4, -0x1

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/gr;->a(J)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/month/av;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/calendar/month/av;->h:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic b(Lcom/android/calendar/month/av;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/android/calendar/month/av;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/month/av;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/calendar/month/av;->i:Ljava/util/ArrayList;

    return-object p1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 180
    iget-object v1, p0, Lcom/android/calendar/month/av;->u:Ljava/lang/Runnable;

    monitor-enter v1

    .line 181
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/av;->f:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/av;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 182
    iget-object v0, p0, Lcom/android/calendar/month/av;->d:Landroid/content/AsyncQueryHandler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 183
    sget-object v0, Lcom/android/calendar/month/av;->a:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    sget-object v0, Lcom/android/calendar/month/av;->a:Ljava/lang/String;

    const-string v2, "Stopped loader from loading"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_0
    monitor-exit v1

    .line 187
    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c(Lcom/android/calendar/month/av;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/calendar/month/av;->c()V

    return-void
.end method

.method static synthetic d(Lcom/android/calendar/month/av;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/calendar/month/av;->e()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private d()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 336
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 337
    if-nez v0, :cond_0

    .line 344
    :cond_0
    return v1
.end method

.method private e()Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 371
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 372
    iget-object v1, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/month/av;->t:Ljava/lang/Runnable;

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 373
    iget-object v1, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    add-int/lit8 v2, v0, -0x1

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 374
    iget-object v1, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 375
    iget-object v1, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    add-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 376
    iget-object v0, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 377
    sget-object v4, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 378
    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 379
    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 380
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/month/av;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->g:Landroid/net/Uri;

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 384
    const-string v0, "visible=1"

    .line 385
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v1

    .line 387
    if-eqz v1, :cond_0

    .line 388
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 390
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 393
    :cond_1
    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/month/av;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/calendar/month/av;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/month/av;)Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->d:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method private h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 483
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/month/av;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/calendar/month/av;->h()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/month/av;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->h:Landroid/net/Uri;

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    const/4 v6, 0x0

    .line 487
    iget-object v2, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    iget-wide v4, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 488
    iget-object v3, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/month/av;->t:Ljava/lang/Runnable;

    invoke-static {v4, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 489
    iget-object v3, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    add-int/lit8 v4, v2, -0x1

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 490
    iget-object v3, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 491
    iget-object v3, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    add-int/lit8 v2, v2, 0x1

    invoke-static {v3, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 492
    iget-object v2, p0, Lcom/android/calendar/month/av;->c:Landroid/text/format/Time;

    invoke-virtual {v2, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 494
    iget-object v6, p0, Lcom/android/calendar/month/av;->o:Landroid/content/Context;

    if-nez v6, :cond_0

    .line 495
    const-string v0, ""

    .line 504
    :goto_0
    return-object v0

    .line 499
    :cond_0
    iget-object v6, p0, Lcom/android/calendar/month/av;->o:Landroid/content/Context;

    invoke-static {v6}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-wide v2, v0

    .line 504
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/month/av;->o:Landroid/content/Context;

    invoke-static {v4, v2, v3, v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-wide v0, v2

    move-wide v2, v4

    goto :goto_1
.end method

.method static synthetic j(Lcom/android/calendar/month/av;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/calendar/month/av;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/month/av;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/month/av;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/month/av;)Lcom/android/calendar/month/aj;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->l:Lcom/android/calendar/month/aj;

    return-object v0
.end method

.method static synthetic n(Lcom/android/calendar/month/av;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/android/calendar/month/av;->p:I

    return v0
.end method

.method static synthetic o(Lcom/android/calendar/month/av;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/av;->m:Lcom/android/calendar/al;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Lcom/android/calendar/month/av;->f:Landroid/os/Handler;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/month/av;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    invoke-static {}, Lcom/android/calendar/month/by;->v()V

    .line 331
    iget-object v0, p0, Lcom/android/calendar/month/av;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/av;->u:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 333
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 355
    iget-wide v2, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x20

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 356
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 357
    iget-object v2, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    iget-wide v4, p1, Lcom/android/calendar/aq;->p:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    :goto_0
    invoke-direct {p0, v2, v0}, Lcom/android/calendar/month/av;->a(Landroid/text/format/Time;Z)V

    .line 368
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 357
    goto :goto_0

    .line 359
    :cond_2
    iget-wide v2, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v4, 0x800

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 360
    iget-object v2, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    iget-wide v4, p1, Lcom/android/calendar/aq;->p:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    :goto_2
    invoke-direct {p0, v2, v0}, Lcom/android/calendar/month/av;->a(Landroid/text/format/Time;Z)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 361
    :cond_4
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/32 v2, 0x10000

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 362
    :cond_5
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->a()V

    goto :goto_1
.end method

.method public a(Lcom/android/calendar/month/ba;Z)V
    .locals 17

    .prologue
    .line 727
    if-nez p1, :cond_0

    .line 759
    :goto_0
    return-void

    .line 731
    :cond_0
    const/4 v14, 0x0

    .line 736
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/month/ba;->c:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 737
    const/4 v14, 0x1

    .line 740
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    .line 742
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 743
    iput-object v2, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 744
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/month/ba;->h:J

    invoke-virtual {v6, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 745
    const/4 v3, 0x1

    invoke-virtual {v6, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 747
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    .line 748
    iput-object v2, v7, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 749
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/month/ba;->i:J

    invoke-virtual {v7, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 750
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 752
    if-nez p2, :cond_2

    .line 753
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/av;->m:Lcom/android/calendar/al;

    const-wide/16 v4, 0x8

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/month/ba;->f:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 756
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/av;->m:Lcom/android/calendar/al;

    const-wide/32 v4, 0x200000

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/month/ba;->f:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 349
    const-wide/32 v0, 0x108a0

    return-wide v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 305
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 306
    new-instance v0, Lcom/android/calendar/month/az;

    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/az;-><init>(Lcom/android/calendar/month/av;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->d:Landroid/content/AsyncQueryHandler;

    .line 307
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 191
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 193
    iput-object p1, p0, Lcom/android/calendar/month/av;->o:Landroid/content/Context;

    .line 194
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/av;->f:Landroid/os/Handler;

    .line 195
    new-instance v0, Lcom/android/calendar/month/aj;

    invoke-direct {v0, p1}, Lcom/android/calendar/month/aj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->l:Lcom/android/calendar/month/aj;

    .line 196
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/av;->m:Lcom/android/calendar/al;

    .line 197
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/month/av;->r:Z

    .line 199
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/android/calendar/month/av;->l:Lcom/android/calendar/month/aj;

    if-eqz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/android/calendar/month/av;->l:Lcom/android/calendar/month/aj;

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/aj;->a(I)V

    .line 204
    :cond_0
    new-instance v0, Lcom/android/calendar/gr;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/android/calendar/gr;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/android/calendar/month/av;->s:Lcom/android/calendar/gr;

    .line 205
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 7

    .prologue
    const v6, 0x7f0f0187

    const v5, 0x7f0f0143

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 537
    if-eqz p3, :cond_0

    .line 538
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    iput v0, p0, Lcom/android/calendar/month/av;->p:I

    .line 539
    iget v0, p0, Lcom/android/calendar/month/av;->p:I

    iget-object v1, p0, Lcom/android/calendar/month/av;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 541
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/av;->i:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/month/av;->p:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/ba;

    .line 542
    iget-object v1, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    invoke-direct {p0, v1}, Lcom/android/calendar/month/av;->a(Landroid/text/format/Time;)V

    .line 543
    iget-object v1, v0, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 544
    iget-object v1, v0, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    sput-object v1, Lcom/android/calendar/month/av;->v:Ljava/lang/String;

    .line 547
    iget v1, v0, Lcom/android/calendar/month/ba;->c:I

    if-ne v1, v2, :cond_3

    .line 550
    const/4 v0, 0x6

    invoke-interface {p1, v3, v0, v3, v5}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 551
    iget-object v1, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 553
    const/4 v0, 0x4

    invoke-interface {p1, v3, v0, v3, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 554
    iget-object v1, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 556
    invoke-static {}, Lcom/android/calendar/dz;->F()Z

    move-result v0

    if-nez v0, :cond_2

    .line 557
    const/4 v0, 0x5

    const v1, 0x7f0f01d6

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 558
    iget-object v1, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 561
    :cond_2
    const/4 v0, 0x7

    const v1, 0x7f0f03d8

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 562
    iget-object v1, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 563
    :cond_3
    iget v1, v0, Lcom/android/calendar/month/ba;->c:I

    if-ne v1, v4, :cond_0

    .line 566
    iget-boolean v1, v0, Lcom/android/calendar/month/ba;->p:Z

    if-eqz v1, :cond_4

    .line 567
    invoke-interface {p1, v3, v2, v3, v5}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 568
    iget-object v2, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 571
    :cond_4
    iget-boolean v0, v0, Lcom/android/calendar/month/ba;->q:Z

    if-eqz v0, :cond_5

    .line 572
    invoke-interface {p1, v3, v3, v3, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 573
    iget-object v1, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 576
    :cond_5
    invoke-static {}, Lcom/android/calendar/dz;->F()Z

    move-result v0

    if-nez v0, :cond_6

    .line 577
    const v0, 0x7f0f01d6

    invoke-interface {p1, v3, v4, v3, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 578
    iget-object v1, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 581
    :cond_6
    const/4 v0, 0x3

    const v1, 0x7f0f03d8

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 582
    iget-object v1, p0, Lcom/android/calendar/month/av;->n:Lcom/android/calendar/month/ay;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 209
    iget-object v0, p0, Lcom/android/calendar/month/av;->t:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 211
    const v0, 0x7f040031

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 212
    const v0, 0x7f1200aa

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/month/av;->j:Landroid/widget/ListView;

    .line 215
    sget-boolean v0, Lcom/android/calendar/month/av;->r:Z

    if-eqz v0, :cond_0

    .line 216
    const v0, 0x7f1200ad

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/month/av;->k:Landroid/widget/TextView;

    .line 217
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->fontScale:F

    .line 219
    float-to-double v0, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v4

    if-lez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/android/calendar/month/av;->k:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    invoke-direct {p0, v0}, Lcom/android/calendar/month/av;->a(Landroid/text/format/Time;)V

    .line 227
    iget-object v0, p0, Lcom/android/calendar/month/av;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/month/av;->l:Lcom/android/calendar/month/aj;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 228
    iget-object v0, p0, Lcom/android/calendar/month/av;->j:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 230
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 231
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 233
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/av;->m:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    const/4 v0, 0x2

    if-ne v1, v0, :cond_1

    .line 249
    :cond_1
    sget-boolean v0, Lcom/android/calendar/month/av;->r:Z

    if-eqz v0, :cond_2

    .line 250
    const v0, 0x7f120173

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 251
    const v1, 0x7f120176

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 252
    iget-object v3, p0, Lcom/android/calendar/month/av;->o:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/dz;->g(Landroid/content/Context;)I

    move-result v3

    .line 254
    if-nez v3, :cond_2

    .line 255
    const-string v3, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v3}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 256
    if-eqz v3, :cond_2

    .line 257
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 258
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 264
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 265
    iget-object v1, p0, Lcom/android/calendar/month/av;->j:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 266
    iget-object v0, p0, Lcom/android/calendar/month/av;->j:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 269
    iget-object v0, p0, Lcom/android/calendar/month/av;->j:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/av;->registerForContextMenu(Landroid/view/View;)V

    .line 270
    return-object v2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 18

    .prologue
    .line 509
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 510
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 511
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/av;->i:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/android/calendar/month/ba;

    .line 512
    iget v2, v4, Landroid/graphics/Rect;->left:I

    add-int/lit8 v12, v2, 0xa

    .line 515
    iget v2, v4, Landroid/graphics/Rect;->top:I

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v4

    div-int/lit8 v13, v2, 0x2

    .line 517
    iget v2, v3, Lcom/android/calendar/month/ba;->c:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    .line 519
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/av;->b:Landroid/text/format/Time;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/month/av;->a(Landroid/text/format/Time;)V

    .line 520
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/av;->m:Lcom/android/calendar/al;

    const-wide/16 v4, 0x2

    iget-wide v6, v3, Lcom/android/calendar/month/ba;->f:J

    iget-wide v8, v3, Lcom/android/calendar/month/ba;->h:J

    iget-wide v10, v3, Lcom/android/calendar/month/ba;->i:J

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    .line 531
    :goto_0
    return-void

    .line 523
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/av;->m:Lcom/android/calendar/al;

    const-wide/16 v4, 0x2

    iget-wide v6, v3, Lcom/android/calendar/month/ba;->f:J

    const-wide/16 v8, -0x1

    iget-wide v10, v3, Lcom/android/calendar/month/ba;->x:J

    const-wide/16 v14, -0x1

    const/16 v16, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJZ)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 311
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 312
    invoke-virtual {p0}, Lcom/android/calendar/month/av;->a()V

    .line 313
    return-void
.end method
