.class Lcom/android/calendar/month/bc;
.super Ljava/lang/Object;
.source "MonthHoverEventFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/bb;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bb;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 318
    iget-object v0, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-virtual {v0}, Lcom/android/calendar/month/bb;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_1

    .line 320
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 321
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 322
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 323
    if-eqz v2, :cond_0

    if-nez v3, :cond_2

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->b(Lcom/android/calendar/month/bb;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->a(Lcom/android/calendar/month/bb;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 376
    :cond_1
    :goto_0
    return-void

    .line 327
    :cond_2
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 328
    iget-object v0, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->c(Lcom/android/calendar/month/bb;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 329
    iget-object v0, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->d(Lcom/android/calendar/month/bb;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 330
    iget-object v5, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v5}, Lcom/android/calendar/month/bb;->e(Lcom/android/calendar/month/bb;)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x41200000    # 10.0f

    mul-float/2addr v0, v6

    add-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 335
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->d(Lcom/android/calendar/month/bb;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 336
    iget-object v0, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->d(Lcom/android/calendar/month/bb;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v0

    .line 337
    if-eqz v0, :cond_7

    .line 338
    iget v5, v0, Landroid/graphics/Rect;->right:I

    iget v6, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    .line 339
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    iget v7, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    .line 341
    iget-object v7, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v7}, Lcom/android/calendar/month/bb;->f(Lcom/android/calendar/month/bb;)I

    move-result v7

    div-int/lit8 v8, v3, 0x2

    sub-int/2addr v7, v8

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 342
    iget-object v7, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v7}, Lcom/android/calendar/month/bb;->g(Lcom/android/calendar/month/bb;)I

    move-result v7

    div-int/lit8 v8, v2, 0x2

    sub-int/2addr v7, v8

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 344
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    if-gez v7, :cond_3

    .line 345
    iput v10, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 346
    :cond_3
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    if-gez v7, :cond_4

    .line 347
    iput v10, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 349
    :cond_4
    iget-object v7, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v7}, Lcom/android/calendar/month/bb;->h(Lcom/android/calendar/month/bb;)Lcom/android/calendar/al;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/calendar/al;->m()Z

    move-result v7

    if-nez v7, :cond_a

    .line 350
    iget-object v7, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v7}, Lcom/android/calendar/month/bb;->i(Lcom/android/calendar/month/bb;)I

    move-result v7

    if-eq v7, v9, :cond_9

    .line 351
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v8, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v8

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 352
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v8, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v8

    invoke-static {}, Lcom/android/calendar/month/bb;->a()I

    move-result v8

    div-int/lit8 v8, v8, 0x4

    sub-int/2addr v7, v8

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 359
    :cond_5
    :goto_2
    iget-object v7, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v7}, Lcom/android/calendar/month/bb;->h(Lcom/android/calendar/month/bb;)Lcom/android/calendar/al;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/calendar/al;->m()Z

    move-result v7

    if-nez v7, :cond_6

    .line 360
    iget v7, v0, Landroid/graphics/Rect;->left:I

    if-nez v7, :cond_6

    .line 361
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v7, v3

    if-le v7, v5, :cond_6

    .line 362
    sub-int v3, v5, v3

    iput v3, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 366
    :cond_6
    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-nez v0, :cond_7

    .line 367
    iget v0, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/2addr v0, v2

    if-le v0, v6, :cond_7

    .line 368
    sub-int v0, v6, v2

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 374
    :cond_7
    invoke-virtual {v1, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto/16 :goto_0

    .line 332
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->e(Lcom/android/calendar/month/bb;)I

    move-result v0

    sub-int/2addr v0, v2

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    goto/16 :goto_1

    .line 354
    :cond_9
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-static {}, Lcom/android/calendar/month/bb;->a()I

    move-result v8

    div-int/lit8 v8, v8, 0x4

    add-int/2addr v7, v8

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_2

    .line 356
    :cond_a
    iget-object v7, p0, Lcom/android/calendar/month/bc;->a:Lcom/android/calendar/month/bb;

    invoke-static {v7}, Lcom/android/calendar/month/bb;->i(Lcom/android/calendar/month/bb;)I

    move-result v7

    if-eq v7, v9, :cond_5

    .line 357
    iget v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-static {}, Lcom/android/calendar/month/bb;->a()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_2
.end method
