.class Lcom/android/calendar/month/cu;
.super Ljava/lang/Object;
.source "YearZoomInFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/cs;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/cs;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/android/calendar/month/cu;->a:Lcom/android/calendar/month/cs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 146
    new-instance v1, Lcom/android/calendar/month/cl;

    iget-object v2, p0, Lcom/android/calendar/month/cu;->a:Lcom/android/calendar/month/cs;

    invoke-virtual {v2}, Lcom/android/calendar/month/cs;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/calendar/month/cl;-><init>(Landroid/app/Activity;)V

    .line 147
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 148
    packed-switch v2, :pswitch_data_0

    .line 166
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    :pswitch_1
    return v0

    .line 152
    :pswitch_2
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 153
    iget-object v3, p0, Lcom/android/calendar/month/cu;->a:Lcom/android/calendar/month/cs;

    invoke-static {v3}, Lcom/android/calendar/month/cs;->a(Lcom/android/calendar/month/cs;)Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 154
    iget-object v3, p0, Lcom/android/calendar/month/cu;->a:Lcom/android/calendar/month/cs;

    invoke-static {v3}, Lcom/android/calendar/month/cs;->a(Lcom/android/calendar/month/cs;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->monthDay:I

    iget-object v4, p0, Lcom/android/calendar/month/cu;->a:Lcom/android/calendar/month/cs;

    invoke-static {v4}, Lcom/android/calendar/month/cs;->b(Lcom/android/calendar/month/cs;)I

    move-result v4

    iget v5, v2, Landroid/text/format/Time;->year:I

    invoke-virtual {v2, v3, v4, v5}, Landroid/text/format/Time;->set(III)V

    .line 155
    iget-object v3, p0, Lcom/android/calendar/month/cu;->a:Lcom/android/calendar/month/cs;

    invoke-static {v3}, Lcom/android/calendar/month/cs;->c(Lcom/android/calendar/month/cs;)Lcom/android/calendar/month/cv;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 156
    iget-object v3, p0, Lcom/android/calendar/month/cu;->a:Lcom/android/calendar/month/cs;

    invoke-virtual {v3}, Lcom/android/calendar/month/cs;->dismiss()V

    .line 158
    :cond_0
    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/calendar/month/cl;->a(Landroid/text/format/Time;IZ)V

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
