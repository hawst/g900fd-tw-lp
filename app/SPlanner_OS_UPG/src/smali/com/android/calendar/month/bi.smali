.class Lcom/android/calendar/month/bi;
.super Ljava/lang/Object;
.source "MonthHoverEventFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/android/calendar/month/bh;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bh;J)V
    .locals 0

    .prologue
    .line 874
    iput-object p1, p0, Lcom/android/calendar/month/bi;->b:Lcom/android/calendar/month/bh;

    iput-wide p2, p0, Lcom/android/calendar/month/bi;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 877
    iget-object v0, p0, Lcom/android/calendar/month/bi;->b:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->o(Lcom/android/calendar/month/bb;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/bi;->b:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->p(Lcom/android/calendar/month/bb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 878
    iget-object v0, p0, Lcom/android/calendar/month/bi;->b:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->q(Lcom/android/calendar/month/bb;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 879
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 880
    iget-object v1, p0, Lcom/android/calendar/month/bi;->b:Lcom/android/calendar/month/bh;

    iget-object v1, v1, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->q(Lcom/android/calendar/month/bb;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 881
    invoke-static {v0}, Lcom/android/calendar/month/h;->c(Landroid/text/format/Time;)V

    .line 887
    :cond_0
    :goto_0
    return-void

    .line 884
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/bi;->b:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-virtual {v0}, Lcom/android/calendar/month/bb;->dismissAllowingStateLoss()V

    .line 885
    iget-object v0, p0, Lcom/android/calendar/month/bi;->b:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->h(Lcom/android/calendar/month/bb;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x2

    iget-wide v6, p0, Lcom/android/calendar/month/bi;->a:J

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v9}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    goto :goto_0
.end method
