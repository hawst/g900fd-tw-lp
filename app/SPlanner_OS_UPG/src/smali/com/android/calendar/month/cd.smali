.class public Lcom/android/calendar/month/cd;
.super Landroid/view/View;
.source "SimpleWeekView.java"


# static fields
.field protected static ai:I

.field protected static aj:I

.field protected static ak:I

.field protected static al:I

.field protected static am:I

.field protected static an:I

.field protected static ao:I

.field protected static ap:I

.field protected static aq:I

.field protected static ar:I

.field protected static au:F

.field public static bN:Z


# instance fields
.field private a:I

.field protected aA:Landroid/graphics/drawable/Drawable;

.field protected aB:Landroid/graphics/drawable/Drawable;

.field protected aC:Landroid/graphics/drawable/Drawable;

.field protected aD:Landroid/graphics/drawable/Drawable;

.field protected aE:I

.field protected aF:Lcom/android/calendar/hl;

.field protected aG:Lcom/android/calendar/d/a/a;

.field protected aH:I

.field protected aI:Lcom/android/calendar/d/g;

.field aJ:Z

.field protected aK:[Landroid/graphics/Bitmap;

.field protected aL:[Ljava/lang/String;

.field protected aM:[Z

.field protected aN:I

.field protected aO:I

.field protected aP:I

.field protected aQ:I

.field protected aR:I

.field protected aS:I

.field protected aT:Z

.field protected aU:Z

.field protected aV:Z

.field protected aW:I

.field protected aX:I

.field protected aY:I

.field protected aZ:I

.field protected as:[Z

.field protected at:[I

.field protected av:I

.field protected aw:Landroid/graphics/Rect;

.field protected ax:Landroid/graphics/Paint;

.field protected ay:Landroid/graphics/Paint;

.field protected az:Landroid/graphics/Paint;

.field protected bA:Z

.field protected bB:I

.field protected bC:Landroid/graphics/drawable/ColorDrawable;

.field protected bD:Ljava/util/List;

.field protected bE:Ljava/util/List;

.field protected bF:Lcom/android/calendar/ej;

.field protected bG:I

.field bH:Ljava/lang/String;

.field protected final bI:Landroid/content/Context;

.field protected final bJ:Z

.field protected final bK:Z

.field protected final bL:Z

.field protected final bM:Z

.field bO:Landroid/text/format/Time;

.field protected ba:I

.field protected bb:I

.field protected bc:I

.field protected bd:I

.field protected be:Ljava/lang/String;

.field protected bf:[I

.field protected bg:[I

.field protected bh:I

.field protected bi:I

.field protected bj:I

.field protected bk:I

.field protected bl:I

.field protected bm:I

.field protected bn:I

.field protected bo:I

.field protected bp:I

.field protected bq:I

.field protected br:I

.field protected bs:I

.field protected bt:I

.field protected bu:I

.field protected bv:I

.field protected bw:I

.field protected bx:Z

.field protected by:Z

.field protected bz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/month/cd;->ai:I

    .line 136
    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/month/cd;->aj:I

    .line 148
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/month/cd;->ak:I

    .line 150
    const/16 v0, 0x10

    sput v0, Lcom/android/calendar/month/cd;->al:I

    .line 152
    const/16 v0, 0xc

    sput v0, Lcom/android/calendar/month/cd;->am:I

    .line 154
    const/16 v0, 0x12

    sput v0, Lcom/android/calendar/month/cd;->an:I

    .line 156
    const/4 v0, 0x2

    sput v0, Lcom/android/calendar/month/cd;->ao:I

    .line 158
    const/4 v0, 0x4

    sput v0, Lcom/android/calendar/month/cd;->ap:I

    .line 160
    const/16 v0, 0x17

    sput v0, Lcom/android/calendar/month/cd;->aq:I

    .line 164
    const/16 v0, 0x18

    sput v0, Lcom/android/calendar/month/cd;->ar:I

    .line 171
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/month/cd;->au:F

    .line 705
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/cd;->bN:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0b0123

    const v3, 0x7f0b0120

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 337
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 174
    iput v2, p0, Lcom/android/calendar/month/cd;->av:I

    .line 176
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    .line 178
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    .line 199
    iput v2, p0, Lcom/android/calendar/month/cd;->aH:I

    .line 205
    iput-boolean v2, p0, Lcom/android/calendar/month/cd;->aJ:Z

    .line 216
    iput v1, p0, Lcom/android/calendar/month/cd;->aN:I

    .line 219
    iput v1, p0, Lcom/android/calendar/month/cd;->aO:I

    .line 222
    iput v1, p0, Lcom/android/calendar/month/cd;->aP:I

    .line 226
    iput v1, p0, Lcom/android/calendar/month/cd;->aQ:I

    .line 232
    sget v0, Lcom/android/calendar/month/cd;->ai:I

    iput v0, p0, Lcom/android/calendar/month/cd;->aS:I

    .line 235
    iput-boolean v2, p0, Lcom/android/calendar/month/cd;->aT:Z

    .line 238
    iput-boolean v2, p0, Lcom/android/calendar/month/cd;->aU:Z

    .line 241
    iput-boolean v2, p0, Lcom/android/calendar/month/cd;->aV:Z

    .line 244
    iput v1, p0, Lcom/android/calendar/month/cd;->aW:I

    .line 246
    iput v1, p0, Lcom/android/calendar/month/cd;->aX:I

    .line 249
    iput v1, p0, Lcom/android/calendar/month/cd;->aY:I

    .line 252
    iput v2, p0, Lcom/android/calendar/month/cd;->aZ:I

    .line 255
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/month/cd;->ba:I

    .line 258
    iget v0, p0, Lcom/android/calendar/month/cd;->ba:I

    iput v0, p0, Lcom/android/calendar/month/cd;->bb:I

    .line 261
    iput v1, p0, Lcom/android/calendar/month/cd;->bc:I

    .line 264
    iput v1, p0, Lcom/android/calendar/month/cd;->bd:I

    .line 268
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cd;->be:Ljava/lang/String;

    .line 310
    iput-boolean v2, p0, Lcom/android/calendar/month/cd;->bz:Z

    .line 312
    iput-boolean v2, p0, Lcom/android/calendar/month/cd;->bA:Z

    .line 318
    iput-object v5, p0, Lcom/android/calendar/month/cd;->bD:Ljava/util/List;

    .line 320
    iput-object v5, p0, Lcom/android/calendar/month/cd;->bE:Ljava/util/List;

    .line 325
    iput v2, p0, Lcom/android/calendar/month/cd;->bG:I

    .line 331
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cd;->bJ:Z

    .line 332
    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cd;->bK:Z

    .line 333
    invoke-static {}, Lcom/android/calendar/dz;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cd;->bL:Z

    .line 334
    invoke-static {}, Lcom/android/calendar/dz;->u()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cd;->bM:Z

    .line 443
    iput v1, p0, Lcom/android/calendar/month/cd;->a:I

    .line 1223
    iput-object v5, p0, Lcom/android/calendar/month/cd;->bO:Landroid/text/format/Time;

    .line 338
    iput-object p1, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    .line 340
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cd;->aI:Lcom/android/calendar/d/g;

    .line 341
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cd;->aG:Lcom/android/calendar/d/a/a;

    .line 344
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cd;->bx:Z

    .line 345
    invoke-direct {p0}, Lcom/android/calendar/month/cd;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cd;->bz:Z

    .line 348
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->D(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cd;->by:Z

    .line 349
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 351
    const v1, 0x7f0b0095

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bh:I

    .line 352
    const v1, 0x7f0b00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bi:I

    .line 353
    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bj:I

    .line 354
    const v1, 0x7f0b00a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bk:I

    .line 355
    const v1, 0x7f0b0093

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bl:I

    .line 357
    iget-boolean v1, p0, Lcom/android/calendar/month/cd;->bx:Z

    if-eqz v1, :cond_4

    .line 358
    const v1, 0x7f0b00b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bm:I

    .line 359
    const v1, 0x7f0b0092

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bn:I

    .line 365
    :goto_0
    const v1, 0x7f020103

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/cd;->aA:Landroid/graphics/drawable/Drawable;

    .line 366
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bo:I

    .line 367
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bs:I

    .line 369
    iget-boolean v1, p0, Lcom/android/calendar/month/cd;->bz:Z

    if-eqz v1, :cond_5

    .line 370
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bq:I

    .line 371
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bt:I

    .line 372
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->br:I

    .line 373
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bu:I

    .line 381
    :goto_1
    const v1, 0x7f0b00ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bv:I

    .line 383
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/cd;->bC:Landroid/graphics/drawable/ColorDrawable;

    .line 385
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 387
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x320

    if-ne v2, v3, :cond_0

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0x140

    if-ne v1, v2, :cond_0

    .line 389
    const/16 v1, 0x30

    sput v1, Lcom/android/calendar/month/cd;->ar:I

    .line 392
    :cond_0
    iget-boolean v1, p0, Lcom/android/calendar/month/cd;->bx:Z

    if-eqz v1, :cond_1

    .line 393
    const v1, 0x7f0201c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/cd;->aB:Landroid/graphics/drawable/Drawable;

    .line 394
    const v1, 0x7f0201a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/cd;->aC:Landroid/graphics/drawable/Drawable;

    .line 395
    const v1, 0x7f0200bc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/cd;->aD:Landroid/graphics/drawable/Drawable;

    .line 396
    const v1, 0x7f0b0130

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->aE:I

    .line 397
    const v1, 0x7f0b012f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cd;->bB:I

    .line 400
    :cond_1
    sget v0, Lcom/android/calendar/month/cd;->au:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 401
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/android/calendar/month/cd;->au:F

    .line 402
    sget v0, Lcom/android/calendar/month/cd;->au:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 403
    sget v0, Lcom/android/calendar/month/cd;->ai:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/cd;->au:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/cd;->ai:I

    .line 404
    sget v0, Lcom/android/calendar/month/cd;->aj:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/cd;->au:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/cd;->aj:I

    .line 405
    sget v0, Lcom/android/calendar/month/cd;->al:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/cd;->au:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/cd;->al:I

    .line 406
    sget v0, Lcom/android/calendar/month/cd;->an:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/cd;->au:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/cd;->an:I

    .line 407
    sget v0, Lcom/android/calendar/month/cd;->ao:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/cd;->au:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/cd;->ao:I

    .line 408
    sget v0, Lcom/android/calendar/month/cd;->ap:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/cd;->au:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/cd;->ap:I

    .line 410
    sget v0, Lcom/android/calendar/month/cd;->am:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/cd;->au:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/cd;->am:I

    .line 414
    :cond_2
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 415
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 416
    invoke-static {p1}, Lcom/android/calendar/ej;->a(Landroid/content/Context;)Lcom/android/calendar/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cd;->bF:Lcom/android/calendar/ej;

    .line 421
    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/month/cd;->b()V

    .line 422
    return-void

    .line 361
    :cond_4
    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bm:I

    .line 362
    const v1, 0x7f0b0092

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bn:I

    goto/16 :goto_0

    .line 375
    :cond_5
    const v1, 0x7f0b00ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bq:I

    .line 376
    const v1, 0x7f0b00cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bt:I

    .line 377
    const v1, 0x7f0b00e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->br:I

    .line 378
    const v1, 0x7f0b00e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/cd;->bu:I

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1227
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1229
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1230
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1232
    packed-switch v1, :pswitch_data_0

    .line 1246
    iget-object v1, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1251
    :goto_0
    return-object v0

    .line 1234
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1238
    :pswitch_1
    iget-object v1, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1242
    :pswitch_2
    iget-object v1, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1232
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 426
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 428
    :try_start_0
    invoke-static {}, Lcom/android/calendar/dz;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 432
    :goto_0
    return-object v0

    .line 429
    :catch_0
    move-exception v0

    .line 430
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v3, 0x5

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 1046
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bD:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1138
    :cond_0
    return-void

    .line 1051
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-eqz v0, :cond_2

    .line 1052
    const/4 v0, 0x7

    move v2, v0

    .line 1057
    :goto_0
    iget v0, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v1, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v9, v0, v1

    .line 1059
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v0, :cond_d

    move v0, v4

    .line 1062
    :goto_1
    const/16 v1, 0x8

    new-array v10, v1, [I

    move v1, v4

    .line 1066
    :goto_2
    array-length v6, v10

    if-ge v1, v6, :cond_3

    .line 1067
    aput v4, v10, v1

    .line 1066
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    move v2, v3

    .line 1054
    goto :goto_0

    .line 1069
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/month/cd;->bD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v1, v0

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1070
    add-int/lit8 v6, v1, 0x1

    .line 1071
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_4

    move v1, v6

    .line 1072
    goto :goto_3

    .line 1075
    :cond_4
    iget v1, p0, Lcom/android/calendar/month/cd;->bb:I

    mul-int/lit8 v1, v1, 0x2

    .line 1078
    iget-boolean v7, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-eqz v7, :cond_5

    .line 1079
    iget v1, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v7, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v7, v7, 0x2

    sub-int/2addr v1, v7

    mul-int/2addr v1, v6

    iget v7, p0, Lcom/android/calendar/month/cd;->bb:I

    div-int/2addr v1, v7

    iget v7, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v1, v7

    add-int/lit8 v1, v1, 0x1

    move v7, v1

    .line 1084
    :goto_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v8, v4

    .line 1085
    :goto_5
    if-ge v8, v12, :cond_6

    if-ge v8, v2, :cond_6

    .line 1089
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/dh;

    .line 1090
    iget v1, v1, Lcom/android/calendar/dh;->c:I

    invoke-virtual {p0, p1, v7, v8, v1}, Lcom/android/calendar/month/cd;->b(Landroid/graphics/Canvas;III)V

    .line 1091
    aget v1, v10, v6

    add-int/lit8 v1, v1, 0x1

    aput v1, v10, v6

    .line 1085
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_5

    .line 1081
    :cond_5
    mul-int/lit8 v7, v6, 0x2

    add-int/lit8 v7, v7, 0x1

    mul-int/2addr v7, v9

    div-int v1, v7, v1

    iget v7, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v1, v7

    sget v7, Lcom/android/calendar/month/cd;->ar:I

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v1, v7

    move v7, v1

    goto :goto_4

    :cond_6
    move v1, v6

    .line 1093
    goto :goto_3

    .line 1095
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bE:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1100
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v0, :cond_8

    move v5, v4

    .line 1103
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1104
    add-int/lit8 v5, v5, 0x1

    .line 1105
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_9

    .line 1109
    aget v1, v10, v5

    if-ge v1, v3, :cond_0

    .line 1112
    iget v1, p0, Lcom/android/calendar/month/cd;->bb:I

    mul-int/lit8 v1, v1, 0x2

    .line 1115
    iget-boolean v6, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-eqz v6, :cond_a

    .line 1116
    iget v1, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v6, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v6, v6, 0x2

    sub-int/2addr v1, v6

    mul-int/2addr v1, v5

    iget v6, p0, Lcom/android/calendar/month/cd;->bb:I

    div-int/2addr v1, v6

    iget v6, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v1, v6

    add-int/lit8 v1, v1, 0x1

    move v6, v1

    .line 1121
    :goto_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v7, v4

    .line 1122
    :goto_7
    if-ge v7, v11, :cond_9

    aget v1, v10, v5

    add-int/2addr v1, v7

    if-ge v1, v2, :cond_9

    .line 1126
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/task/aa;

    .line 1128
    iget v12, v1, Lcom/android/calendar/task/aa;->i:I

    if-gez v12, :cond_b

    .line 1129
    iget-object v1, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    .line 1133
    :goto_8
    aget v12, v10, v5

    add-int/2addr v12, v7

    invoke-virtual {p0, p1, v6, v12, v1}, Lcom/android/calendar/month/cd;->b(Landroid/graphics/Canvas;III)V

    .line 1122
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_7

    .line 1118
    :cond_a
    mul-int/lit8 v6, v5, 0x2

    add-int/lit8 v6, v6, 0x1

    mul-int/2addr v6, v9

    div-int v1, v6, v1

    iget v6, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v1, v6

    sget v6, Lcom/android/calendar/month/cd;->ar:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v1, v6

    move v6, v1

    goto :goto_6

    .line 1131
    :cond_b
    iget-object v12, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    iget v1, v1, Lcom/android/calendar/task/aa;->i:I

    invoke-static {v12, v1}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    goto :goto_8

    :cond_c
    move v1, v6

    goto/16 :goto_3

    :cond_d
    move v0, v5

    goto/16 :goto_1
.end method


# virtual methods
.method public a(F)Landroid/text/format/Time;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 784
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v1, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/cd;->bb:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v0, v1

    .line 785
    :goto_0
    int-to-float v1, v0

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_2

    .line 786
    :cond_0
    const/4 v0, 0x0

    .line 803
    :goto_1
    return-object v0

    .line 784
    :cond_1
    iget v0, p0, Lcom/android/calendar/month/cd;->av:I

    goto :goto_0

    .line 789
    :cond_2
    int-to-float v1, v0

    sub-float v1, p1, v1

    iget v2, p0, Lcom/android/calendar/month/cd;->ba:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/cd;->aR:I

    sub-int v0, v2, v0

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-int v0, v0

    .line 790
    iget v1, p0, Lcom/android/calendar/month/cd;->aN:I

    add-int/2addr v1, v0

    .line 792
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/cd;->be:Ljava/lang/String;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 793
    iget v2, p0, Lcom/android/calendar/month/cd;->aQ:I

    const/16 v3, 0xddc

    if-ne v2, v3, :cond_3

    .line 795
    const v2, 0x253d8c    # 3.419992E-39f

    if-ne v1, v2, :cond_3

    .line 796
    const/4 v1, 0x0

    const/16 v2, 0x7b2

    invoke-virtual {v0, v4, v1, v2}, Landroid/text/format/Time;->set(III)V

    .line 797
    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    goto :goto_1

    .line 802
    :cond_3
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    goto :goto_1
.end method

.method protected a()V
    .locals 7

    .prologue
    const/4 v0, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 708
    new-array v1, v0, [I

    .line 709
    new-array v2, v0, [I

    .line 711
    const-string v0, "XXXXXXR"

    .line 713
    iget v3, p0, Lcom/android/calendar/month/cd;->bo:I

    aput v3, v1, v4

    .line 714
    iget v3, p0, Lcom/android/calendar/month/cd;->bq:I

    aput v3, v1, v5

    .line 715
    iget v3, p0, Lcom/android/calendar/month/cd;->br:I

    aput v3, v1, v6

    .line 717
    iget v3, p0, Lcom/android/calendar/month/cd;->bs:I

    aput v3, v2, v4

    .line 718
    iget v3, p0, Lcom/android/calendar/month/cd;->bt:I

    aput v3, v2, v5

    .line 719
    iget v3, p0, Lcom/android/calendar/month/cd;->bu:I

    aput v3, v2, v6

    .line 722
    :try_start_0
    invoke-static {}, Lcom/android/calendar/dz;->e()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 727
    :goto_0
    iget v3, p0, Lcom/android/calendar/month/cd;->aZ:I

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/cd;->bf:[I

    .line 728
    iget v1, p0, Lcom/android/calendar/month/cd;->aZ:I

    invoke-static {v0, v2, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cd;->bg:[I

    .line 729
    return-void

    .line 723
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1005
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-eqz v0, :cond_1

    .line 1006
    const/16 v0, 0x20

    new-array v1, v0, [F

    .line 1011
    const/16 v2, 0x1c

    .line 1012
    const/4 v0, 0x1

    const/4 v3, 0x0

    aput v3, v1, v7

    .line 1013
    const/4 v3, 0x2

    iget v4, p0, Lcom/android/calendar/month/cd;->aS:I

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    aput v4, v1, v0

    .line 1014
    const/4 v4, 0x3

    iget v0, p0, Lcom/android/calendar/month/cd;->aR:I

    int-to-float v0, v0

    aput v0, v1, v3

    .line 1015
    const/4 v0, 0x4

    iget v3, p0, Lcom/android/calendar/month/cd;->aS:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    aput v3, v1, v4

    .line 1018
    iget v3, p0, Lcom/android/calendar/month/cd;->aS:I

    .line 1020
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1021
    iget v4, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v5, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    div-int/lit8 v5, v0, 0x4

    sub-int/2addr v5, v7

    mul-int/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/month/cd;->bb:I

    div-int/2addr v4, v5

    iget v5, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v4, v5

    .line 1023
    add-int/lit8 v5, v0, 0x1

    int-to-float v6, v4

    aput v6, v1, v0

    .line 1024
    add-int/lit8 v0, v5, 0x1

    int-to-float v6, v7

    aput v6, v1, v5

    .line 1025
    add-int/lit8 v5, v0, 0x1

    int-to-float v4, v4

    aput v4, v1, v0

    .line 1026
    add-int/lit8 v0, v5, 0x1

    int-to-float v4, v3

    aput v4, v1, v5

    goto :goto_0

    .line 1028
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/month/cd;->bB:I

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1029
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/month/cd;->ak:I

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1030
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7, v2, v0}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 1043
    :cond_1
    return-void
.end method

.method a(Landroid/graphics/Paint;IZ)V
    .locals 1

    .prologue
    .line 996
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/cd;->bf:[I

    aget v0, v0, p2

    :goto_0
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 997
    return-void

    .line 996
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bg:[I

    aget v0, v0, p2

    goto :goto_0
.end method

.method public a(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 19

    .prologue
    .line 445
    const-string v2, "week"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 446
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string v3, "You must specify the week number for this view"

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 448
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/month/cd;->setTag(Ljava/lang/Object;)V

    .line 449
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/month/cd;->be:Ljava/lang/String;

    .line 451
    const-string v2, "height"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 452
    const-string v2, "height"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aS:I

    .line 453
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->aS:I

    sget v3, Lcom/android/calendar/month/cd;->aj:I

    if-ge v2, v3, :cond_1

    .line 454
    sget v2, Lcom/android/calendar/month/cd;->aj:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aS:I

    .line 457
    :cond_1
    const-string v2, "selected_day"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 458
    const-string v2, "selected_day"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aW:I

    .line 461
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->aW:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1c

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cd;->aU:Z

    .line 462
    const-string v2, "num_days"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 463
    const-string v2, "num_days"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->ba:I

    .line 465
    :cond_3
    const-string v2, "show_wk_num"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 466
    const-string v2, "show_wk_num"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_1d

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cd;->aT:Z

    .line 468
    :cond_4
    const-string v2, "mini_mode_service"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 469
    const-string v2, "mini_mode_service"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_1e

    .line 470
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cd;->bA:Z

    .line 471
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cd;->aT:Z

    .line 478
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 479
    const-string v3, "preferences_islarm_correction"

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 480
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->bG:I

    .line 482
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->ba:I

    add-int/lit8 v2, v2, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->bb:I

    .line 484
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->a:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cd;->bb:I

    if-eq v2, v3, :cond_20

    .line 485
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->bb:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->a:I

    .line 488
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->bb:I

    new-array v2, v2, [Z

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cd;->aM:[Z

    .line 490
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->bb:I

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    .line 491
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->bb:I

    new-array v2, v2, [Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cd;->aK:[Landroid/graphics/Bitmap;

    .line 492
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->bb:I

    new-array v2, v2, [Z

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cd;->as:[Z

    .line 494
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bJ:Z

    if-eqz v2, :cond_6

    .line 495
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->bb:I

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cd;->at:[I

    .line 499
    :cond_6
    new-instance v2, Lcom/android/calendar/hl;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cd;->bb:I

    invoke-direct {v2, v3}, Lcom/android/calendar/hl;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    .line 513
    :goto_4
    const-string v2, "week"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aQ:I

    .line 514
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->aQ:I

    invoke-static {v2}, Lcom/android/calendar/hj;->a(I)I

    move-result v2

    .line 515
    new-instance v5, Landroid/text/format/Time;

    move-object/from16 v0, p2

    invoke-direct {v5, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 517
    invoke-static {v5, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 519
    const-string v2, "week_start"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 520
    const-string v2, "week_start"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aZ:I

    .line 523
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->aW:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cd;->aZ:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aX:I

    .line 524
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->aX:I

    if-gez v2, :cond_8

    .line 525
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->aX:I

    add-int/lit8 v2, v2, 0x7

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aX:I

    .line 528
    :cond_8
    const/4 v2, 0x0

    .line 529
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v3, :cond_2e

    .line 531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    iget-object v2, v2, Lcom/android/calendar/hl;->a:[Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-boolean v4, v2, v3

    .line 533
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bJ:Z

    if-eqz v2, :cond_9

    .line 534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->at:[I

    const/4 v3, 0x0

    const/4 v4, -0x1

    aput v4, v2, v3

    .line 537
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/calendar/month/cd;->bz:Z

    invoke-static {v6, v7, v4, v8}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;Z)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 539
    const/4 v2, 0x1

    move v3, v2

    .line 542
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v2, :cond_22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    :goto_6
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cd;->bH:Ljava/lang/String;

    .line 548
    iget v2, v5, Landroid/text/format/Time;->weekDay:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cd;->aZ:I

    if-eq v2, v4, :cond_b

    .line 549
    iget v2, v5, Landroid/text/format/Time;->weekDay:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cd;->aZ:I

    sub-int/2addr v2, v4

    .line 550
    if-gez v2, :cond_a

    .line 551
    add-int/lit8 v2, v2, 0x7

    .line 553
    :cond_a
    iget v4, v5, Landroid/text/format/Time;->monthDay:I

    sub-int v2, v4, v2

    iput v2, v5, Landroid/text/format/Time;->monthDay:I

    .line 554
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 559
    :cond_b
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v2, v6, v8

    if-nez v2, :cond_23

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bJ:Z

    if-nez v2, :cond_c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bK:Z

    if-eqz v2, :cond_23

    .line 560
    :cond_c
    invoke-static {v5}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v6

    iget-wide v8, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aN:I

    .line 565
    :goto_7
    iget v2, v5, Landroid/text/format/Time;->month:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aO:I

    .line 568
    new-instance v6, Landroid/text/format/Time;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 569
    invoke-virtual {v6}, Landroid/text/format/Time;->setToNow()V

    .line 570
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cd;->aV:Z

    .line 571
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aY:I

    .line 573
    const-string v2, "focus_month"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    const-string v2, "focus_month"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v4, v2

    .line 576
    :goto_8
    const-string v2, "is_weather_enabled"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 577
    const-string v2, "is_weather_enabled"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_25

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cd;->aJ:Z

    .line 580
    :cond_d
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v2

    const-string v7, "JAPAN"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 581
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->c()Z

    move-result v8

    .line 582
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->e()Z

    move-result v9

    .line 583
    const-string v2, "Asia/Taipei"

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 584
    const-string v2, "ar"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    .line 586
    :goto_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->bb:I

    if-ge v3, v2, :cond_2b

    .line 587
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bJ:Z

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bK:Z

    if-eqz v2, :cond_f

    .line 588
    :cond_e
    iget v2, v5, Landroid/text/format/Time;->month:I

    const/16 v13, 0xb

    if-ne v2, v13, :cond_f

    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    const/16 v13, 0x20

    if-ne v2, v13, :cond_f

    .line 589
    iget v2, v5, Landroid/text/format/Time;->year:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v5, Landroid/text/format/Time;->year:I

    .line 590
    const/4 v2, 0x0

    iput v2, v5, Landroid/text/format/Time;->month:I

    .line 591
    const/4 v2, 0x1

    iput v2, v5, Landroid/text/format/Time;->monthDay:I

    .line 595
    :cond_f
    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    const/4 v13, 0x1

    if-ne v2, v13, :cond_10

    .line 596
    iget v2, v5, Landroid/text/format/Time;->month:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aO:I

    .line 600
    :cond_10
    if-eqz v10, :cond_11

    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    const/4 v13, 0x4

    invoke-virtual {v5, v13}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v13

    if-le v2, v13, :cond_11

    .line 601
    const/4 v2, 0x1

    iput v2, v5, Landroid/text/format/Time;->monthDay:I

    .line 602
    iget v2, v5, Landroid/text/format/Time;->month:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v5, Landroid/text/format/Time;->month:I

    .line 604
    :cond_11
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aM:[Z

    iget v2, v5, Landroid/text/format/Time;->month:I

    if-ne v2, v4, :cond_26

    const/4 v2, 0x1

    :goto_b
    aput-boolean v2, v13, v3

    .line 605
    iget v2, v5, Landroid/text/format/Time;->year:I

    iget v13, v6, Landroid/text/format/Time;->year:I

    if-ne v2, v13, :cond_12

    iget v2, v5, Landroid/text/format/Time;->yearDay:I

    iget v13, v6, Landroid/text/format/Time;->yearDay:I

    if-ne v2, v13, :cond_12

    .line 606
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cd;->aV:Z

    .line 607
    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/month/cd;->aY:I

    .line 610
    :cond_12
    if-eqz v8, :cond_13

    .line 612
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aG:Lcom/android/calendar/d/a/a;

    iget v13, v5, Landroid/text/format/Time;->year:I

    iget v14, v5, Landroid/text/format/Time;->month:I

    iget v15, v5, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v2, v13, v14, v15}, Lcom/android/calendar/d/a/a;->a(III)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 620
    :goto_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    iget-object v2, v2, Lcom/android/calendar/hl;->c:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aG:Lcom/android/calendar/d/a/a;

    invoke-virtual {v13}, Lcom/android/calendar/d/a/a;->e()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v2, v3

    .line 621
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    iget-object v2, v2, Lcom/android/calendar/hl;->d:[Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aG:Lcom/android/calendar/d/a/a;

    invoke-virtual {v13}, Lcom/android/calendar/d/a/a;->d()Z

    move-result v13

    aput-boolean v13, v2, v3

    .line 623
    if-eqz v9, :cond_13

    .line 624
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    iget-object v2, v2, Lcom/android/calendar/hl;->a:[Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aG:Lcom/android/calendar/d/a/a;

    invoke-virtual {v13, v5}, Lcom/android/calendar/d/a/a;->b(Landroid/text/format/Time;)Z

    move-result v13

    aput-boolean v13, v2, v3

    .line 625
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    iget-object v2, v2, Lcom/android/calendar/hl;->b:[Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aG:Lcom/android/calendar/d/a/a;

    invoke-virtual {v13, v5}, Lcom/android/calendar/d/a/a;->c(Landroid/text/format/Time;)Z

    move-result v13

    aput-boolean v13, v2, v3

    .line 626
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bJ:Z

    if-eqz v2, :cond_13

    .line 627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->at:[I

    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v13

    invoke-virtual {v13, v12, v5}, Lcom/android/calendar/eb;->a(Landroid/content/ContentResolver;Landroid/text/format/Time;)I

    move-result v13

    aput v13, v2, v3

    .line 633
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bL:Z

    if-eqz v2, :cond_28

    .line 634
    iget v2, v5, Landroid/text/format/Time;->year:I

    iget v13, v5, Landroid/text/format/Time;->month:I

    iget v14, v5, Landroid/text/format/Time;->monthDay:I

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/month/cd;->bG:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v2, v13, v14, v15, v0}, Lcom/android/calendar/hj;->a(IIIII)Ljava/lang/String;

    move-result-object v2

    .line 635
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    iget-object v13, v13, Lcom/android/calendar/hl;->c:[Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " ( "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, " ) "

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v3

    .line 642
    :cond_14
    :goto_d
    if-eqz v7, :cond_16

    .line 643
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->e()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 644
    iget v2, v5, Landroid/text/format/Time;->year:I

    iget v13, v5, Landroid/text/format/Time;->month:I

    iget v14, v5, Landroid/text/format/Time;->monthDay:I

    invoke-static {v2, v13, v14}, Lcom/android/calendar/hj;->a(III)I

    move-result v2

    .line 645
    iput v2, v5, Landroid/text/format/Time;->weekDay:I

    .line 646
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->bF:Lcom/android/calendar/ej;

    invoke-virtual {v2, v5}, Lcom/android/calendar/ej;->a(Landroid/text/format/Time;)Z

    move-result v2

    if-nez v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->bF:Lcom/android/calendar/ej;

    invoke-virtual {v2, v5}, Lcom/android/calendar/ej;->b(Landroid/text/format/Time;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 648
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->as:[Z

    const/4 v13, 0x1

    aput-boolean v13, v2, v3

    .line 653
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->aJ:Z

    if-eqz v2, :cond_17

    .line 654
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v2

    .line 655
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aK:[Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v2, v14, v5}, Lcom/android/calendar/h/c;->a(Landroid/content/res/Resources;Landroid/text/format/Time;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v13, v3

    .line 658
    :cond_17
    if-eqz v11, :cond_29

    .line 659
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v13

    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    iget v0, v5, Landroid/text/format/Time;->monthDay:I

    move/from16 v17, v0

    add-int/lit8 v18, v17, 0x1

    move/from16 v0, v18

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v2, v3

    .line 665
    :goto_e
    iget v2, v5, Landroid/text/format/Time;->year:I

    const/16 v13, 0x7b0

    if-ne v2, v13, :cond_18

    iget v2, v5, Landroid/text/format/Time;->month:I

    const/16 v13, 0x8

    if-ne v2, v13, :cond_18

    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    const/16 v13, 0x20

    if-eq v2, v13, :cond_1a

    :cond_18
    iget v2, v5, Landroid/text/format/Time;->year:I

    const/16 v13, 0x770

    if-ne v2, v13, :cond_19

    iget v2, v5, Landroid/text/format/Time;->month:I

    const/16 v13, 0xa

    if-ne v2, v13, :cond_19

    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    const/16 v13, 0x20

    if-eq v2, v13, :cond_1a

    :cond_19
    iget v2, v5, Landroid/text/format/Time;->year:I

    const/16 v13, 0x78b

    if-ne v2, v13, :cond_1b

    iget v2, v5, Landroid/text/format/Time;->month:I

    const/16 v13, 0xb

    if-ne v2, v13, :cond_1b

    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    const/16 v13, 0x21

    if-ne v2, v13, :cond_1b

    .line 668
    :cond_1a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aM:[Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aM:[Z

    aget-boolean v2, v2, v3

    if-nez v2, :cond_2a

    const/4 v2, 0x1

    :goto_f
    aput-boolean v2, v13, v3

    .line 669
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v2, v3

    .line 672
    :cond_1b
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 586
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    .line 461
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 466
    :cond_1d
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 473
    :cond_1e
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cd;->bA:Z

    goto/16 :goto_2

    .line 482
    :cond_1f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cd;->ba:I

    goto/16 :goto_3

    .line 501
    :cond_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aM:[Z

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([ZZ)V

    .line 502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aK:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 504
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->as:[Z

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([ZZ)V

    .line 506
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bJ:Z

    if-eqz v2, :cond_21

    .line 507
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->at:[I

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 510
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    invoke-virtual {v2}, Lcom/android/calendar/hl;->a()V

    goto/16 :goto_4

    .line 542
    :cond_22
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/month/cd;->bz:Z

    invoke-static {v6, v7, v2, v4}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;Z)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 562
    :cond_23
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-wide v8, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aN:I

    goto/16 :goto_7

    .line 573
    :cond_24
    const/4 v2, -0x1

    move v4, v2

    goto/16 :goto_8

    .line 577
    :cond_25
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 604
    :cond_26
    const/4 v2, 0x0

    goto/16 :goto_b

    .line 613
    :catch_0
    move-exception v2

    .line 614
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 615
    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    if-nez v2, :cond_27

    .line 616
    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v5, Landroid/text/format/Time;->monthDay:I

    .line 618
    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aG:Lcom/android/calendar/d/a/a;

    iget v13, v5, Landroid/text/format/Time;->year:I

    iget v14, v5, Landroid/text/format/Time;->month:I

    iget v15, v5, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v2, v13, v14, v15}, Lcom/android/calendar/d/a/a;->a(III)V

    goto/16 :goto_c

    .line 636
    :cond_28
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cd;->bM:Z

    if-eqz v2, :cond_14

    .line 637
    iget v2, v5, Landroid/text/format/Time;->year:I

    iget v13, v5, Landroid/text/format/Time;->month:I

    iget v14, v5, Landroid/text/format/Time;->monthDay:I

    const/4 v15, 0x2

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v2, v13, v14, v15, v0}, Lcom/android/calendar/hj;->a(IIIII)Ljava/lang/String;

    move-result-object v2

    .line 638
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cd;->aF:Lcom/android/calendar/hl;

    iget-object v13, v13, Lcom/android/calendar/hl;->c:[Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " ( "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, " ) "

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v3

    goto/16 :goto_d

    .line 661
    :cond_29
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    iget v13, v5, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v14, v13, 0x1

    iput v14, v5, Landroid/text/format/Time;->monthDay:I

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v2, v3

    goto/16 :goto_e

    .line 668
    :cond_2a
    const/4 v2, 0x0

    goto/16 :goto_f

    .line 676
    :cond_2b
    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2c

    .line 677
    iget v2, v5, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v5, Landroid/text/format/Time;->monthDay:I

    .line 678
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 680
    :cond_2c
    iget v2, v5, Landroid/text/format/Time;->month:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cd;->aP:I

    .line 682
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/cd;->a()V

    .line 685
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->bH:Ljava/lang/String;

    if-eqz v2, :cond_2d

    .line 686
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cd;->bH:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/calendar/month/cd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cd;->bH:Ljava/lang/String;

    .line 689
    :cond_2d
    return-void

    :cond_2e
    move v3, v2

    goto/16 :goto_5
.end method

.method protected b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 735
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 736
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 737
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/cd;->al:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 738
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 740
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    .line 741
    iget-object v0, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 742
    iget-object v0, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 743
    iget-object v0, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/cd;->al:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 744
    iget-object v0, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/month/cd;->bi:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 745
    iget-object v0, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 746
    iget-object v0, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 747
    return-void
.end method

.method protected b(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 856
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->bx:Z

    if-eqz v0, :cond_3

    .line 857
    iget v0, p0, Lcom/android/calendar/month/cd;->aX:I

    .line 858
    iget-boolean v1, p0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v1, :cond_0

    .line 859
    add-int/lit8 v0, v0, 0x1

    .line 862
    :cond_0
    iget-boolean v1, p0, Lcom/android/calendar/month/cd;->aU:Z

    if-eqz v1, :cond_1

    .line 863
    iget-boolean v1, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-eqz v1, :cond_4

    .line 864
    iget v1, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/android/calendar/month/cd;->bb:I

    div-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v1, v2

    .line 865
    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v3, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/android/calendar/month/cd;->bb:I

    div-int/2addr v0, v2

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v0, v2

    .line 867
    iget-object v2, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iput v4, v2, Landroid/graphics/Rect;->top:I

    .line 868
    iget-object v2, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/cd;->aS:I

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 869
    iget-object v2, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->left:I

    .line 870
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 872
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 873
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/month/cd;->aE:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 875
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 890
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->aV:Z

    if-eqz v0, :cond_2

    .line 891
    iget v0, p0, Lcom/android/calendar/month/cd;->bb:I

    mul-int/lit8 v0, v0, 0x2

    .line 892
    iget v1, p0, Lcom/android/calendar/month/cd;->aY:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v3, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    mul-int/2addr v1, v2

    div-int v0, v1, v0

    iget v1, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v0, v1

    .line 895
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iput v4, v1, Landroid/graphics/Rect;->top:I

    .line 896
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/month/cd;->aS:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 898
    iget-boolean v1, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-eqz v1, :cond_5

    .line 899
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 900
    iget-object v2, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    sub-int v3, v0, v1

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 901
    iget-object v2, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    add-int/2addr v0, v1

    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 903
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aD:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 904
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 915
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v0, :cond_3

    .line 916
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 917
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/cd;->aS:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 918
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/cd;->av:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 919
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/cd;->ba:I

    add-int/lit8 v2, v2, 0x1

    div-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 921
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 922
    iget-object v0, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/month/cd;->bn:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 924
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 927
    :cond_3
    return-void

    .line 877
    :cond_4
    iget v1, p0, Lcom/android/calendar/month/cd;->bb:I

    mul-int/lit8 v1, v1, 0x2

    .line 878
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v3, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    mul-int/2addr v0, v2

    div-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v0, v1

    .line 880
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    const/4 v2, 0x5

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 881
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/month/cd;->aS:I

    add-int/lit8 v2, v2, -0x3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 882
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    sget v2, Lcom/android/calendar/month/cd;->ar:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 883
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    sget v2, Lcom/android/calendar/month/cd;->ar:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 885
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aC:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 886
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aC:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 906
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/month/cd;->aB:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 907
    iget-object v2, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    sub-int v3, v0, v1

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 908
    iget-object v2, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    add-int/2addr v0, v1

    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 910
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aB:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/month/cd;->aw:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 911
    iget-object v0, p0, Lcom/android/calendar/month/cd;->aB:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_1
.end method

.method protected b(Landroid/graphics/Canvas;III)V
    .locals 5

    .prologue
    .line 1142
    iget-boolean v0, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-eqz v0, :cond_1

    .line 1143
    const/16 v0, 0x8

    .line 1144
    const/4 v1, 0x6

    if-ne p3, v1, :cond_0

    .line 1145
    const/4 v0, 0x7

    .line 1148
    :cond_0
    mul-int/lit8 v1, p3, 0x9

    add-int/2addr v1, p2

    .line 1149
    iget-object v2, p0, Lcom/android/calendar/month/cd;->bC:Landroid/graphics/drawable/ColorDrawable;

    iget v3, p0, Lcom/android/calendar/month/cd;->aS:I

    add-int/lit8 v3, v3, -0x9

    add-int/2addr v0, v1

    iget v4, p0, Lcom/android/calendar/month/cd;->aS:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v1, v3, v0, v4}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1150
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bC:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 1151
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bC:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 1158
    :goto_0
    return-void

    .line 1153
    :cond_1
    mul-int/lit8 v0, p3, 0x5

    add-int/2addr v0, p2

    .line 1154
    iget-object v1, p0, Lcom/android/calendar/month/cd;->bC:Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/android/calendar/month/cd;->aS:I

    add-int/lit8 v2, v2, -0x3

    add-int/lit8 v3, v0, 0x4

    iget v4, p0, Lcom/android/calendar/month/cd;->aS:I

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1155
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bC:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 1156
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bC:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public c(F)I
    .locals 3

    .prologue
    .line 808
    iget v0, p0, Lcom/android/calendar/month/cd;->av:I

    .line 812
    int-to-float v1, v0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    .line 813
    int-to-float p1, v0

    .line 819
    :cond_0
    :goto_0
    int-to-float v1, v0

    sub-float v1, p1, v1

    iget v2, p0, Lcom/android/calendar/month/cd;->ba:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/cd;->aR:I

    sub-int v0, v2, v0

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-int v0, v0

    .line 820
    iget v1, p0, Lcom/android/calendar/month/cd;->aN:I

    add-int/2addr v0, v1

    .line 822
    return v0

    .line 814
    :cond_1
    iget v1, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    .line 815
    iget v1, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v2, p0, Lcom/android/calendar/month/cd;->av:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-float p1, v1

    goto :goto_0
.end method

.method protected c(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 935
    iget v0, p0, Lcom/android/calendar/month/cd;->aS:I

    sget v1, Lcom/android/calendar/month/cd;->al:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    sget v1, Lcom/android/calendar/month/cd;->ak:I

    sub-int/2addr v0, v1

    .line 936
    iget v7, p0, Lcom/android/calendar/month/cd;->bb:I

    .line 939
    mul-int/lit8 v8, v7, 0x2

    .line 943
    iget v1, p0, Lcom/android/calendar/month/cd;->aX:I

    .line 945
    iget-boolean v4, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-eqz v4, :cond_0

    .line 946
    add-int/lit8 v0, v0, 0x3

    .line 947
    iget-object v4, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    sget v5, Lcom/android/calendar/month/cd;->aq:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 950
    :cond_0
    iget-boolean v4, p0, Lcom/android/calendar/month/cd;->aT:Z

    if-eqz v4, :cond_6

    .line 951
    iget-object v4, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    sget v5, Lcom/android/calendar/month/cd;->am:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 952
    iget-object v4, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 953
    iget-object v4, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 954
    iget-object v4, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 955
    iget-object v4, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 956
    iget-object v4, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    iget v5, p0, Lcom/android/calendar/month/cd;->bm:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 957
    iget v4, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v5, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    div-int/2addr v4, v8

    iget v5, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v4, v5

    .line 958
    iget-object v5, p0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    aget-object v5, v5, v3

    int-to-float v4, v4

    add-int/lit8 v6, v0, -0x2

    int-to-float v6, v6

    iget-object v9, p0, Lcom/android/calendar/month/cd;->ax:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v4, v6, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 963
    add-int/lit8 v1, v1, 0x1

    move v4, v2

    .line 966
    :goto_0
    iget-object v5, p0, Lcom/android/calendar/month/cd;->aM:[Z

    aget-boolean v6, v5, v4

    .line 967
    iget-object v9, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    if-eqz v6, :cond_4

    iget v5, p0, Lcom/android/calendar/month/cd;->bi:I

    :goto_1
    invoke-virtual {v9, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 968
    iget-object v5, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 969
    iget-object v5, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move v5, v4

    move v4, v6

    .line 970
    :goto_2
    if-ge v5, v7, :cond_5

    .line 971
    sub-int v6, v5, v2

    .line 972
    iget-object v9, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-virtual {p0, v9, v6, v4}, Lcom/android/calendar/month/cd;->a(Landroid/graphics/Paint;IZ)V

    .line 974
    iget-object v9, p0, Lcom/android/calendar/month/cd;->aM:[Z

    aget-boolean v9, v9, v5

    if-eq v9, v4, :cond_1

    .line 975
    iget-object v4, p0, Lcom/android/calendar/month/cd;->aM:[Z

    aget-boolean v4, v4, v5

    .line 976
    iget-object v9, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-virtual {p0, v9, v6, v4}, Lcom/android/calendar/month/cd;->a(Landroid/graphics/Paint;IZ)V

    .line 979
    :cond_1
    iget-boolean v9, p0, Lcom/android/calendar/month/cd;->bA:Z

    if-nez v9, :cond_2

    .line 980
    iget-boolean v9, p0, Lcom/android/calendar/month/cd;->aU:Z

    if-eqz v9, :cond_2

    if-ne v1, v5, :cond_2

    .line 981
    iget-object v9, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    iget v10, p0, Lcom/android/calendar/month/cd;->bv:I

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 982
    iget-object v9, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 986
    :cond_2
    mul-int/lit8 v9, v5, 0x2

    add-int/lit8 v9, v9, 0x1

    iget v10, p0, Lcom/android/calendar/month/cd;->aR:I

    iget v11, p0, Lcom/android/calendar/month/cd;->av:I

    mul-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    mul-int/2addr v9, v10

    div-int/2addr v9, v8

    iget v10, p0, Lcom/android/calendar/month/cd;->av:I

    add-int/2addr v9, v10

    .line 987
    iget-object v10, p0, Lcom/android/calendar/month/cd;->aL:[Ljava/lang/String;

    aget-object v10, v10, v5

    int-to-float v9, v9

    int-to-float v11, v0

    iget-object v12, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v9, v11, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 989
    iget-boolean v9, p0, Lcom/android/calendar/month/cd;->aU:Z

    if-eqz v9, :cond_3

    if-ne v1, v5, :cond_3

    .line 990
    iget-object v9, p0, Lcom/android/calendar/month/cd;->az:Landroid/graphics/Paint;

    invoke-virtual {p0, v9, v6, v4}, Lcom/android/calendar/month/cd;->a(Landroid/graphics/Paint;IZ)V

    .line 970
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 967
    :cond_4
    iget v5, p0, Lcom/android/calendar/month/cd;->bj:I

    goto :goto_1

    .line 993
    :cond_5
    return-void

    :cond_6
    move v2, v3

    move v4, v3

    goto :goto_0
.end method

.method public getFirstJulianDay()I
    .locals 1

    .prologue
    .line 773
    iget v0, p0, Lcom/android/calendar/month/cd;->aN:I

    return v0
.end method

.method public getFirstMonth()I
    .locals 1

    .prologue
    .line 755
    iget v0, p0, Lcom/android/calendar/month/cd;->aO:I

    return v0
.end method

.method public getLastMonth()I
    .locals 1

    .prologue
    .line 764
    iget v0, p0, Lcom/android/calendar/month/cd;->aP:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 843
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/cd;->b(Landroid/graphics/Canvas;)V

    .line 844
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/cd;->c(Landroid/graphics/Canvas;)V

    .line 845
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/cd;->a(Landroid/graphics/Canvas;)V

    .line 846
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cd;->d(Landroid/graphics/Canvas;)V

    .line 847
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1204
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1205
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1206
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1220
    :goto_0
    return v0

    .line 1208
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    .line 1209
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/cd;->a(F)Landroid/text/format/Time;

    move-result-object v0

    .line 1210
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/calendar/month/cd;->bO:Landroid/text/format/Time;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/month/cd;->bO:Landroid/text/format/Time;

    invoke-static {v0, v1}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v1

    if-eqz v1, :cond_3

    .line 1211
    :cond_2
    invoke-virtual {v0, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1212
    iget-object v1, p0, Lcom/android/calendar/month/cd;->bI:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v6, 0x10

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    .line 1213
    const v2, 0x8000

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 1215
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1216
    invoke-virtual {p0, v2}, Lcom/android/calendar/month/cd;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1217
    iput-object v0, p0, Lcom/android/calendar/month/cd;->bO:Landroid/text/format/Time;

    :cond_3
    move v0, v7

    .line 1220
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 1197
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/cd;->aS:I

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/cd;->setMeasuredDimension(II)V

    .line 1198
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 1192
    iput p1, p0, Lcom/android/calendar/month/cd;->aR:I

    .line 1193
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 693
    const/16 v0, 0x40

    if-ne p1, v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/android/calendar/month/cd;->bH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/cd;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 696
    sget-boolean v0, Lcom/android/calendar/month/cd;->bN:Z

    if-eqz v0, :cond_0

    .line 697
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/cd;->bN:Z

    .line 698
    const/4 v0, 0x1

    .line 701
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method public setDayEvents(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 1161
    iput-object p1, p0, Lcom/android/calendar/month/cd;->bD:Ljava/util/List;

    .line 1162
    if-nez p1, :cond_1

    .line 1173
    :cond_0
    :goto_0
    return-void

    .line 1165
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/cd;->ba:I

    if-eq v0, v1, :cond_0

    .line 1166
    const-string v0, "MonthView"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1167
    const-string v0, "MonthView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Events size must be same as days displayed: size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " days="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/month/cd;->ba:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 1170
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/month/cd;->bD:Ljava/util/List;

    goto :goto_0
.end method

.method public setDayTasks(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 1176
    iput-object p1, p0, Lcom/android/calendar/month/cd;->bE:Ljava/util/List;

    .line 1177
    if-nez p1, :cond_1

    .line 1188
    :cond_0
    :goto_0
    return-void

    .line 1180
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/cd;->ba:I

    if-eq v0, v1, :cond_0

    .line 1181
    const-string v0, "MonthView"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1182
    const-string v0, "MonthView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Events size must be same as days displayed: size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " days="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/month/cd;->ba:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 1185
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/month/cd;->bE:Ljava/util/List;

    goto :goto_0
.end method
