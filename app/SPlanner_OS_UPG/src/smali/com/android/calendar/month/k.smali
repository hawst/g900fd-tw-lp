.class public Lcom/android/calendar/month/k;
.super Lcom/android/calendar/month/by;
.source "MonthByWeekFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/android/calendar/ap;


# static fields
.field public static A:Z

.field static B:Ljava/lang/Runnable;

.field private static aA:F

.field private static aF:Z

.field public static n:I

.field static q:Z

.field public static r:Z

.field public static s:I

.field public static t:I

.field public static u:I

.field public static v:I

.field public static w:Z

.field public static y:Z

.field public static z:Z


# instance fields
.field protected C:Lcom/android/calendar/month/cc;

.field D:Lcom/android/calendar/month/ad;

.field E:Z

.field F:F

.field G:Z

.field H:Ljava/lang/Runnable;

.field protected a:Z

.field private aB:Lcom/android/calendar/al;

.field private aC:Lcom/android/calendar/AllInOneActivity;

.field private aD:Landroid/widget/Toast;

.field private aE:I

.field private aG:Z

.field private aH:Z

.field private aI:Z

.field private aJ:I

.field private aK:I

.field private aL:I

.field private aM:I

.field private aN:Ljava/lang/Runnable;

.field private aO:Ljava/lang/Runnable;

.field private aP:Ljava/lang/Runnable;

.field private aQ:Landroid/view/animation/Animation$AnimationListener;

.field private aR:Ljava/lang/Runnable;

.field private aS:Landroid/view/animation/Animation$AnimationListener;

.field private aT:Landroid/content/BroadcastReceiver;

.field private as:Landroid/content/AsyncQueryHandler;

.field private at:Landroid/net/Uri;

.field private au:Landroid/net/Uri;

.field private av:Landroid/view/GestureDetector;

.field private aw:Landroid/text/format/Time;

.field private ax:Landroid/text/format/Time;

.field private volatile ay:Z

.field private az:Z

.field protected b:Z

.field protected c:Z

.field protected d:I

.field protected e:I

.field public f:Landroid/view/animation/Animation;

.field public g:Landroid/view/animation/Animation;

.field public h:Landroid/util/LruCache;

.field protected i:Landroid/view/ViewStub;

.field protected j:Landroid/view/View;

.field protected k:Lcom/android/calendar/month/MonthEventList;

.field protected l:Landroid/view/View;

.field protected m:Lcom/android/calendar/month/MonthDayHeader;

.field o:Z

.field p:J

.field x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 152
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/month/k;->aA:F

    .line 186
    sput v2, Lcom/android/calendar/month/k;->n:I

    .line 193
    sput-boolean v1, Lcom/android/calendar/month/k;->q:Z

    .line 195
    sput-boolean v2, Lcom/android/calendar/month/k;->aF:Z

    .line 197
    sput-boolean v2, Lcom/android/calendar/month/k;->r:Z

    .line 211
    sput-boolean v1, Lcom/android/calendar/month/k;->w:Z

    .line 234
    sput-boolean v1, Lcom/android/calendar/month/k;->y:Z

    .line 237
    sput-boolean v1, Lcom/android/calendar/month/k;->z:Z

    .line 240
    sput-boolean v1, Lcom/android/calendar/month/k;->A:Z

    .line 569
    new-instance v0, Lcom/android/calendar/month/w;

    invoke-direct {v0}, Lcom/android/calendar/month/w;-><init>()V

    sput-object v0, Lcom/android/calendar/month/k;->B:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 897
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/month/k;-><init>(JZ)V

    .line 898
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 901
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/month/by;-><init>(J)V

    .line 144
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aw:Landroid/text/format/Time;

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->ay:Z

    .line 150
    iput-boolean v1, p0, Lcom/android/calendar/month/k;->az:Z

    .line 184
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/month/k;->aE:I

    .line 188
    iput-boolean v1, p0, Lcom/android/calendar/month/k;->o:Z

    .line 199
    iput-boolean v1, p0, Lcom/android/calendar/month/k;->aG:Z

    .line 218
    iput-boolean v1, p0, Lcom/android/calendar/month/k;->aH:Z

    .line 220
    iput-boolean v1, p0, Lcom/android/calendar/month/k;->aI:Z

    .line 223
    const/16 v0, 0x12

    iput v0, p0, Lcom/android/calendar/month/k;->aJ:I

    .line 225
    const/16 v0, 0x1b

    iput v0, p0, Lcom/android/calendar/month/k;->aK:I

    .line 228
    iput v1, p0, Lcom/android/calendar/month/k;->aL:I

    .line 230
    iput v1, p0, Lcom/android/calendar/month/k;->aM:I

    .line 232
    iput-boolean v1, p0, Lcom/android/calendar/month/k;->x:Z

    .line 244
    new-instance v0, Lcom/android/calendar/month/l;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/l;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aN:Ljava/lang/Runnable;

    .line 261
    new-instance v0, Lcom/android/calendar/month/u;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/u;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    .line 282
    new-instance v0, Lcom/android/calendar/month/v;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/v;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aP:Ljava/lang/Runnable;

    .line 1054
    new-instance v0, Lcom/android/calendar/month/y;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/y;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aQ:Landroid/view/animation/Animation$AnimationListener;

    .line 1193
    new-instance v0, Lcom/android/calendar/month/ag;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/ag;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->C:Lcom/android/calendar/month/cc;

    .line 1370
    sget-object v0, Lcom/android/calendar/month/ad;->a:Lcom/android/calendar/month/ad;

    iput-object v0, p0, Lcom/android/calendar/month/k;->D:Lcom/android/calendar/month/ad;

    .line 1372
    iput-boolean v1, p0, Lcom/android/calendar/month/k;->E:Z

    .line 1374
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/month/k;->F:F

    .line 1454
    new-instance v0, Lcom/android/calendar/month/z;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/z;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aR:Ljava/lang/Runnable;

    .line 2269
    iput-boolean v1, p0, Lcom/android/calendar/month/k;->G:Z

    .line 2460
    new-instance v0, Lcom/android/calendar/month/q;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/q;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->H:Ljava/lang/Runnable;

    .line 2868
    new-instance v0, Lcom/android/calendar/month/s;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/s;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aS:Landroid/view/animation/Animation$AnimationListener;

    .line 3007
    new-instance v0, Lcom/android/calendar/month/t;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/t;-><init>(Lcom/android/calendar/month/k;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aT:Landroid/content/BroadcastReceiver;

    .line 902
    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    .line 903
    if-eqz v0, :cond_0

    .line 904
    iput-boolean p3, p0, Lcom/android/calendar/month/k;->a:Z

    .line 906
    :cond_0
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    .line 375
    iget-object v1, p0, Lcom/android/calendar/month/k;->aP:Ljava/lang/Runnable;

    monitor-enter v1

    .line 376
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/k;->aP:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 377
    iget-object v0, p0, Lcom/android/calendar/month/k;->as:Landroid/content/AsyncQueryHandler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 378
    const-string v0, "MonthFragment"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    const-string v0, "MonthFragment"

    const-string v2, "Stopped loader from loading"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :cond_0
    monitor-exit v1

    .line 382
    return-void

    .line 381
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private B()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 881
    iget-object v1, p0, Lcom/android/calendar/month/k;->ax:Landroid/text/format/Time;

    if-nez v1, :cond_1

    .line 893
    :cond_0
    :goto_0
    return v0

    .line 884
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v1

    .line 885
    if-eqz v1, :cond_2

    .line 886
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/k;->ax:Landroid/text/format/Time;

    .line 890
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/month/k;->ax:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 891
    iget-object v1, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 892
    iget-object v2, p0, Lcom/android/calendar/month/k;->ax:Landroid/text/format/Time;

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/month/k;->ax:Landroid/text/format/Time;

    iget-wide v4, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 893
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 888
    :cond_2
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/k;->ax:Landroid/text/format/Time;

    goto :goto_1
.end method

.method private C()Landroid/view/View;
    .locals 4

    .prologue
    .line 2428
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthListView;->getChildCount()I

    move-result v2

    .line 2430
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2431
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/bo;

    .line 2432
    invoke-virtual {v0}, Lcom/android/calendar/month/bo;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2436
    :goto_1
    return-object v0

    .line 2430
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2436
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IIII)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2758
    if-lt p1, p3, :cond_1

    if-gt p1, p4, :cond_1

    if-lt p2, p3, :cond_1

    if-gt p2, p4, :cond_1

    .line 2769
    :cond_0
    :goto_0
    return v0

    .line 2763
    :cond_1
    sub-int v1, p1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2764
    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    .line 2766
    const/16 v0, 0x1f

    if-gt v1, v0, :cond_2

    .line 2767
    const/16 v0, 0x190

    goto :goto_0

    .line 2769
    :cond_2
    const/16 v0, 0x2bc

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/month/k;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/android/calendar/month/k;->at:Landroid/net/Uri;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Lcom/android/calendar/task/aa;)Lcom/android/calendar/month/ba;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2651
    new-instance v0, Lcom/android/calendar/month/ba;

    invoke-direct {v0}, Lcom/android/calendar/month/ba;-><init>()V

    .line 2653
    const/4 v1, 0x2

    iput v1, v0, Lcom/android/calendar/month/ba;->c:I

    .line 2655
    iget-wide v2, p1, Lcom/android/calendar/task/aa;->b:J

    iput-wide v2, v0, Lcom/android/calendar/month/ba;->f:J

    .line 2656
    iget-object v1, p1, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    .line 2658
    iget v1, p1, Lcom/android/calendar/task/aa;->i:I

    .line 2659
    if-gez v1, :cond_0

    .line 2660
    invoke-static {p0, v4}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/month/ba;->e:I

    .line 2665
    :goto_0
    iget-wide v2, p1, Lcom/android/calendar/task/aa;->d:J

    iput-wide v2, v0, Lcom/android/calendar/month/ba;->x:J

    .line 2666
    iget-wide v2, p1, Lcom/android/calendar/task/aa;->e:J

    iput-wide v2, v0, Lcom/android/calendar/month/ba;->y:J

    .line 2667
    iget v1, p1, Lcom/android/calendar/task/aa;->f:I

    iput v1, v0, Lcom/android/calendar/month/ba;->z:I

    .line 2668
    iget-boolean v1, p1, Lcom/android/calendar/task/aa;->g:Z

    iput-boolean v1, v0, Lcom/android/calendar/month/ba;->A:Z

    .line 2669
    iget v1, p1, Lcom/android/calendar/task/aa;->h:I

    iput v1, v0, Lcom/android/calendar/month/ba;->B:I

    .line 2670
    iget-object v1, p1, Lcom/android/calendar/task/aa;->m:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/month/ba;->D:Ljava/lang/String;

    .line 2672
    invoke-virtual {p1}, Lcom/android/calendar/task/aa;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2673
    iput-boolean v4, v0, Lcom/android/calendar/month/ba;->E:Z

    .line 2678
    :goto_1
    return-object v0

    .line 2662
    :cond_0
    invoke-static {p0, v1}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/month/ba;->e:I

    goto :goto_0

    .line 2675
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/calendar/month/ba;->E:Z

    goto :goto_1
.end method

.method public static a(Lcom/android/calendar/dh;)Lcom/android/calendar/month/ba;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2570
    new-instance v4, Lcom/android/calendar/month/ba;

    invoke-direct {v4}, Lcom/android/calendar/month/ba;-><init>()V

    .line 2572
    iput v1, v4, Lcom/android/calendar/month/ba;->c:I

    .line 2574
    iget-wide v6, p0, Lcom/android/calendar/dh;->b:J

    iput-wide v6, v4, Lcom/android/calendar/month/ba;->f:J

    .line 2575
    iget-object v0, p0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    .line 2577
    iget-object v0, p0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    .line 2578
    if-nez v0, :cond_2

    .line 2579
    const/4 v0, 0x0

    iput-object v0, v4, Lcom/android/calendar/month/ba;->g:Ljava/lang/String;

    .line 2584
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/dh;->f:Z

    iput-boolean v0, v4, Lcom/android/calendar/month/ba;->l:Z

    .line 2585
    iget v0, p0, Lcom/android/calendar/dh;->c:I

    iput v0, v4, Lcom/android/calendar/month/ba;->e:I

    .line 2586
    iget-wide v6, p0, Lcom/android/calendar/dh;->m:J

    iput-wide v6, v4, Lcom/android/calendar/month/ba;->h:J

    .line 2587
    iget-wide v6, p0, Lcom/android/calendar/dh;->n:J

    iput-wide v6, v4, Lcom/android/calendar/month/ba;->i:J

    .line 2588
    iget v0, p0, Lcom/android/calendar/dh;->i:I

    iput v0, v4, Lcom/android/calendar/month/ba;->j:I

    .line 2589
    iget v0, p0, Lcom/android/calendar/dh;->j:I

    iput v0, v4, Lcom/android/calendar/month/ba;->k:I

    .line 2590
    iget v0, p0, Lcom/android/calendar/dh;->B:I

    iput v0, v4, Lcom/android/calendar/month/ba;->w:I

    .line 2592
    iget v0, v4, Lcom/android/calendar/month/ba;->j:I

    iget v3, v4, Lcom/android/calendar/month/ba;->k:I

    if-eq v0, v3, :cond_3

    .line 2594
    iput-boolean v1, v4, Lcom/android/calendar/month/ba;->m:Z

    .line 2599
    :goto_1
    iget-boolean v0, p0, Lcom/android/calendar/dh;->o:Z

    iput-boolean v0, v4, Lcom/android/calendar/month/ba;->n:Z

    .line 2600
    iget-boolean v0, p0, Lcom/android/calendar/dh;->p:Z

    iput-boolean v0, v4, Lcom/android/calendar/month/ba;->o:Z

    .line 2602
    iget-object v0, p0, Lcom/android/calendar/dh;->w:Ljava/lang/String;

    .line 2604
    if-eqz v0, :cond_4

    .line 2609
    :goto_2
    iget-object v3, p0, Lcom/android/calendar/dh;->g:Ljava/lang/String;

    .line 2610
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    .line 2611
    iget-boolean v5, p0, Lcom/android/calendar/dh;->h:Z

    .line 2612
    iget v0, p0, Lcom/android/calendar/dh;->v:I

    const/16 v6, 0x1f4

    if-lt v0, v6, :cond_5

    move v0, v1

    .line 2613
    :goto_3
    iget-boolean v6, p0, Lcom/android/calendar/dh;->K:Z

    .line 2615
    if-eqz v0, :cond_6

    if-nez v3, :cond_0

    if-eqz v5, :cond_6

    :cond_0
    if-nez v6, :cond_6

    move v3, v1

    :goto_4
    iput-boolean v3, v4, Lcom/android/calendar/month/ba;->q:Z

    .line 2616
    if-eqz v0, :cond_7

    if-nez v6, :cond_7

    :goto_5
    iput-boolean v1, v4, Lcom/android/calendar/month/ba;->p:Z

    .line 2618
    invoke-virtual {p0}, Lcom/android/calendar/dh;->g()Z

    move-result v0

    iput-boolean v0, v4, Lcom/android/calendar/month/ba;->r:Z

    .line 2619
    iget-object v0, p0, Lcom/android/calendar/dh;->L:Ljava/lang/String;

    iput-object v0, v4, Lcom/android/calendar/month/ba;->C:Ljava/lang/String;

    .line 2620
    iget-wide v0, p0, Lcom/android/calendar/dh;->r:J

    iput-wide v0, v4, Lcom/android/calendar/month/ba;->u:J

    .line 2621
    iget-boolean v0, p0, Lcom/android/calendar/dh;->K:Z

    iput-boolean v0, v4, Lcom/android/calendar/month/ba;->v:Z

    .line 2623
    invoke-virtual {p0}, Lcom/android/calendar/dh;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2624
    invoke-virtual {p0}, Lcom/android/calendar/dh;->h()Z

    move-result v0

    iput-boolean v0, v4, Lcom/android/calendar/month/ba;->s:Z

    .line 2625
    iget-wide v0, p0, Lcom/android/calendar/dh;->y:J

    iput-wide v0, v4, Lcom/android/calendar/month/ba;->t:J

    .line 2628
    :cond_1
    return-object v4

    .line 2581
    :cond_2
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/android/calendar/month/ba;->g:Ljava/lang/String;

    goto :goto_0

    .line 2596
    :cond_3
    iput-boolean v2, v4, Lcom/android/calendar/month/ba;->m:Z

    goto :goto_1

    .line 2607
    :cond_4
    const-string v0, ""

    goto :goto_2

    :cond_5
    move v0, v2

    .line 2612
    goto :goto_3

    :cond_6
    move v3, v2

    .line 2615
    goto :goto_4

    :cond_7
    move v1, v2

    .line 2616
    goto :goto_5
.end method

.method static synthetic a(Lcom/android/calendar/month/k;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->aN:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static a(Landroid/os/Handler;J)V
    .locals 1

    .prologue
    .line 579
    sget-object v0, Lcom/android/calendar/month/k;->B:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 580
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/k;->r:Z

    .line 581
    sget-object v0, Lcom/android/calendar/month/k;->B:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 582
    return-void
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 8

    .prologue
    .line 2551
    if-nez p0, :cond_1

    .line 2552
    const-string v0, "MonthFragment"

    const-string v1, "buildMonthListItemsFromCursor: null cursor or null month events list!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2567
    :cond_0
    return-void

    .line 2556
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 2558
    if-eqz p1, :cond_0

    .line 2559
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2560
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2561
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    invoke-static {v0}, Lcom/android/calendar/month/k;->a(Lcom/android/calendar/dh;)Lcom/android/calendar/month/ba;

    move-result-object v0

    .line 2562
    iget-wide v4, v0, Lcom/android/calendar/month/ba;->f:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 2563
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2560
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/ArrayList;IILandroid/content/Context;)V
    .locals 3

    .prologue
    .line 2633
    if-nez p0, :cond_1

    .line 2634
    const-string v0, "MonthFragment"

    const-string v1, "buildMonthListItemsFromCursor: null cursor or null month events list!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2648
    :cond_0
    return-void

    .line 2638
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 2640
    if-eqz p1, :cond_0

    .line 2641
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2642
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2643
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/aa;

    invoke-static {p4, v0}, Lcom/android/calendar/month/k;->a(Landroid/content/Context;Lcom/android/calendar/task/aa;)Lcom/android/calendar/month/ba;

    move-result-object v0

    .line 2644
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2642
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private a(Landroid/text/format/Time;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1044
    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    mul-int/lit8 v1, v1, 0xc

    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->month:I

    add-int/2addr v1, v2

    .line 1045
    iget v2, p1, Landroid/text/format/Time;->year:I

    mul-int/lit8 v2, v2, 0xc

    iget v3, p1, Landroid/text/format/Time;->month:I

    add-int/2addr v2, v3

    .line 1047
    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-le v1, v0, :cond_0

    .line 1048
    const/4 v0, 0x0

    .line 1051
    :cond_0
    return v0
.end method

.method static synthetic a(Lcom/android/calendar/month/k;Z)Z
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/android/calendar/month/k;->aG:Z

    return p1
.end method

.method private a(Lcom/tss21/calenarlib/c;Lcom/tss21/calenarlib/c;IIII)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2233
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2247
    :cond_0
    :goto_0
    return v0

    .line 2237
    :cond_1
    mul-int/lit8 v1, p3, 0x64

    add-int/2addr v1, p4

    .line 2238
    mul-int/lit8 v2, p5, 0x64

    add-int/2addr v2, p6

    .line 2240
    iget v3, p1, Lcom/tss21/calenarlib/c;->b:I

    mul-int/lit8 v3, v3, 0x64

    iget v4, p1, Lcom/tss21/calenarlib/c;->c:I

    add-int/2addr v3, v4

    .line 2241
    iget v4, p2, Lcom/tss21/calenarlib/c;->b:I

    mul-int/lit8 v4, v4, 0x64

    iget v5, p2, Lcom/tss21/calenarlib/c;->c:I

    add-int/2addr v4, v5

    .line 2243
    if-gt v1, v4, :cond_0

    if-lt v2, v3, :cond_0

    .line 2244
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/month/k;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/android/calendar/month/k;->au:Landroid/net/Uri;

    return-object p1
.end method

.method public static b(Z)V
    .locals 0

    .prologue
    .line 2709
    sput-boolean p0, Lcom/android/calendar/month/k;->q:Z

    .line 2710
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/month/k;)Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->ay:Z

    return v0
.end method

.method private c(I)V
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 2440
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 2441
    new-instance v4, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v1, v11}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2442
    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 2443
    iget v1, v4, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v1, p1

    iput v1, v4, Landroid/text/format/Time;->monthDay:I

    .line 2444
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 2446
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->l()V

    .line 2447
    invoke-static {v4}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 2448
    iget-object v1, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->i()V

    .line 2450
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x10

    move-object v5, v4

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 2452
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/month/k;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/calendar/month/k;->z()V

    return-void
.end method

.method static synthetic d(Lcom/android/calendar/month/k;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/calendar/month/k;->x()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/month/k;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->at:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/month/k;)Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->as:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/month/k;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/calendar/month/k;->A()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/month/k;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->au:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/month/k;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/calendar/month/k;->y()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/month/k;)Lcom/android/calendar/AllInOneActivity;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/month/k;)Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->aG:Z

    return v0
.end method

.method static synthetic l(Lcom/android/calendar/month/k;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/month/k;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->aR:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic n(Lcom/android/calendar/month/k;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic o(Lcom/android/calendar/month/k;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->aP:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic p(Lcom/android/calendar/month/k;)Landroid/view/animation/Animation$AnimationListener;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/month/k;->aQ:Landroid/view/animation/Animation$AnimationListener;

    return-object v0
.end method

.method static synthetic q(Lcom/android/calendar/month/k;)I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/android/calendar/month/k;->aM:I

    return v0
.end method

.method static synthetic r(Lcom/android/calendar/month/k;)I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/android/calendar/month/k;->aL:I

    return v0
.end method

.method static synthetic s()Z
    .locals 1

    .prologue
    .line 103
    sget-boolean v0, Lcom/android/calendar/month/k;->aF:Z

    return v0
.end method

.method private x()Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 309
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/cd;

    .line 310
    if-eqz v0, :cond_0

    .line 311
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v0

    .line 312
    iput v0, p0, Lcom/android/calendar/month/k;->d:I

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, p0, Lcom/android/calendar/month/k;->d:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 316
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 317
    iget v2, p0, Lcom/android/calendar/month/k;->d:I

    iget v3, p0, Lcom/android/calendar/month/k;->Q:I

    mul-int/lit8 v3, v3, 0x7

    add-int/2addr v2, v3

    iput v2, p0, Lcom/android/calendar/month/k;->e:I

    .line 319
    iget-object v2, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v3, p0, Lcom/android/calendar/month/k;->e:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 320
    iget-object v2, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-virtual {v2, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 323
    sget-object v4, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 324
    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 325
    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 326
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x1

    .line 341
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/cd;

    .line 342
    if-eqz v0, :cond_0

    .line 343
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v0

    .line 344
    iput v0, p0, Lcom/android/calendar/month/k;->d:I

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, p0, Lcom/android/calendar/month/k;->d:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 348
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 349
    iget v0, p0, Lcom/android/calendar/month/k;->d:I

    iget v1, p0, Lcom/android/calendar/month/k;->Q:I

    add-int/lit8 v1, v1, 0x2

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/k;->e:I

    .line 351
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, p0, Lcom/android/calendar/month/k;->e:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 352
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 356
    iget-boolean v6, p0, Lcom/android/calendar/month/k;->aI:Z

    if-eqz v6, :cond_1

    move-wide v0, v2

    .line 361
    :goto_0
    iget-object v4, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v4, v2, v3, v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-wide v2, v4

    goto :goto_0
.end method

.method private z()V
    .locals 3

    .prologue
    .line 365
    iget-object v1, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    monitor-enter v1

    .line 366
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 367
    iget-object v0, p0, Lcom/android/calendar/month/k;->as:Landroid/content/AsyncQueryHandler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 368
    const-string v0, "MonthFragment"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    const-string v0, "MonthFragment"

    const-string v2, "Stopped loader from loading"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_0
    monitor-exit v1

    .line 372
    return-void

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 331
    const-string v0, "visible=1"

    .line 332
    iget-boolean v1, p0, Lcom/android/calendar/month/k;->b:Z

    if-eqz v1, :cond_0

    .line 333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 335
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 336
    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 2726
    iget-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    if-eqz v0, :cond_1

    .line 2727
    iget-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0, p1}, Lcom/android/calendar/month/MonthEventList;->setVisibility(I)V

    .line 2728
    iget-object v0, p0, Lcom/android/calendar/month/k;->i:Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    .line 2729
    iget-object v0, p0, Lcom/android/calendar/month/k;->i:Landroid/view/ViewStub;

    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2743
    :cond_0
    :goto_0
    return-void

    .line 2732
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/k;->i:Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 2733
    iget-object v0, p0, Lcom/android/calendar/month/k;->i:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2734
    const v1, 0x7f12009d

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/month/MonthEventList;

    iput-object v1, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    .line 2736
    const v1, 0x7f12009c

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->j:Landroid/view/View;

    .line 2738
    iget-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    if-eqz v0, :cond_0

    .line 2739
    iget-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0, p1}, Lcom/android/calendar/month/MonthEventList;->setVisibility(I)V

    goto :goto_0
.end method

.method a(ILandroid/text/format/Time;Landroid/text/format/Time;)V
    .locals 4

    .prologue
    const/16 v3, 0xb

    const/4 v2, 0x1

    .line 535
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 536
    invoke-virtual {v0, p2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 537
    if-gez p1, :cond_4

    .line 538
    iget v1, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 542
    :goto_0
    iput v2, v0, Landroid/text/format/Time;->monthDay:I

    .line 543
    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 545
    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 546
    iget v1, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 554
    :cond_0
    iget v1, v0, Landroid/text/format/Time;->month:I

    if-gez v1, :cond_1

    .line 555
    const/4 v1, 0x0

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 558
    :cond_1
    iget v1, v0, Landroid/text/format/Time;->month:I

    if-le v1, v3, :cond_2

    .line 559
    iput v3, v0, Landroid/text/format/Time;->month:I

    .line 562
    :cond_2
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    .line 564
    iget v1, p2, Landroid/text/format/Time;->monthDay:I

    if-le v1, v0, :cond_3

    .line 565
    iput v0, p3, Landroid/text/format/Time;->monthDay:I

    .line 567
    :cond_3
    return-void

    .line 540
    :cond_4
    iget v1, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/text/format/Time;->month:I

    goto :goto_0
.end method

.method public a(J)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 2781
    iget-object v0, p0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2866
    :cond_0
    :goto_0
    return-void

    .line 2786
    :cond_1
    iget v0, p0, Lcom/android/calendar/month/k;->aL:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/calendar/month/k;->aM:I

    if-nez v0, :cond_3

    .line 2787
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->aL:I

    .line 2788
    iget-object v0, p0, Lcom/android/calendar/month/k;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->aM:I

    .line 2792
    :cond_3
    const/4 v2, 0x5

    move v5, v3

    move v1, v3

    .line 2795
    :goto_1
    const/4 v0, 0x6

    if-ge v5, v0, :cond_5

    .line 2796
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v5}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/bo;

    .line 2798
    if-eqz v0, :cond_0

    .line 2802
    invoke-virtual {v0}, Lcom/android/calendar/month/bo;->getTop()I

    move-result v6

    if-gez v6, :cond_4

    .line 2803
    add-int/lit8 v0, v1, 0x1

    .line 2795
    :goto_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_1

    .line 2807
    :cond_4
    invoke-virtual {v0}, Lcom/android/calendar/month/bo;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2808
    sub-int v0, v5, v1

    move v2, v0

    .line 2813
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthEventList;->getCounts()I

    move-result v5

    .line 2815
    iget-object v0, p0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2817
    iget-object v1, p0, Lcom/android/calendar/month/k;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2820
    iget-boolean v6, p0, Lcom/android/calendar/month/k;->x:Z

    if-eqz v6, :cond_7

    .line 2822
    if-lez v5, :cond_6

    .line 2823
    iget-object v2, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v2}, Lcom/android/calendar/month/MonthListView;->getHeight()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x6

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2824
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2826
    iget v0, p0, Lcom/android/calendar/month/k;->aM:I

    iget v2, p0, Lcom/android/calendar/month/k;->aL:I

    mul-int/lit8 v2, v2, 0x5

    div-int/lit8 v2, v2, 0x6

    add-int/2addr v0, v2

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2835
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1}, Lcom/android/calendar/month/MonthListView;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    move v0, v3

    .line 2857
    :goto_4
    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1}, Lcom/android/calendar/month/MonthListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 2859
    new-instance v1, Lcom/android/calendar/month/ah;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/ah;-><init>(Lcom/android/calendar/month/k;)V

    .line 2860
    iget-object v0, p0, Lcom/android/calendar/month/k;->aS:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/ah;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2861
    invoke-virtual {v1, p1, p2}, Lcom/android/calendar/month/ah;->setDuration(J)V

    .line 2862
    if-lez v5, :cond_9

    move v0, v4

    :goto_5
    iget-boolean v2, p0, Lcom/android/calendar/month/k;->x:Z

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/month/ah;->a(ZZ)V

    .line 2863
    iget-object v0, p0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2865
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->x:Z

    if-nez v0, :cond_a

    :goto_6
    iput-boolean v4, p0, Lcom/android/calendar/month/k;->x:Z

    goto/16 :goto_0

    .line 2829
    :cond_6
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2830
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2832
    iget v0, p0, Lcom/android/calendar/month/k;->aM:I

    iget v2, p0, Lcom/android/calendar/month/k;->aL:I

    mul-int/lit8 v2, v2, 0x5

    div-int/lit8 v2, v2, 0x6

    add-int/2addr v0, v2

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_3

    .line 2840
    :cond_7
    if-lez v5, :cond_8

    .line 2841
    iget-object v6, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v6}, Lcom/android/calendar/month/MonthListView;->getHeight()I

    move-result v6

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2842
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2844
    iget v0, p0, Lcom/android/calendar/month/k;->aM:I

    iget v6, p0, Lcom/android/calendar/month/k;->aL:I

    mul-int/lit8 v6, v6, 0x5

    div-int/lit8 v6, v6, 0x6

    add-int/2addr v0, v6

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2853
    :goto_7
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1}, Lcom/android/calendar/month/MonthListView;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2854
    iget v0, p0, Lcom/android/calendar/month/k;->aL:I

    neg-int v0, v0

    mul-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x6

    goto :goto_4

    .line 2847
    :cond_8
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2848
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2850
    iget v0, p0, Lcom/android/calendar/month/k;->aM:I

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_7

    :cond_9
    move v0, v3

    .line 2862
    goto :goto_5

    :cond_a
    move v4, v3

    .line 2865
    goto :goto_6

    :cond_b
    move v0, v1

    goto/16 :goto_2
.end method

.method a(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2251
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2252
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2253
    new-instance v1, Landroid/text/format/Time;

    const-string v2, "UTC"

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2254
    const v2, 0x24dc87

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 2255
    invoke-virtual {v1, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 2256
    const v4, 0x259d23

    invoke-static {v1, v4}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 2257
    invoke-virtual {v1, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 2258
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 2259
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 2260
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 2261
    invoke-virtual {v1, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 2262
    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 2263
    const v2, 0x7f0f0486

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v8

    aput-object v1, v2, v7

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2264
    invoke-static {p1, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->aD:Landroid/widget/Toast;

    .line 2267
    :cond_0
    return-void
.end method

.method a(Landroid/text/format/Time;Landroid/text/format/Time;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1683
    iget-object v0, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v4}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    .line 1684
    new-instance v0, Lcom/android/calendar/event/iv;

    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-virtual {p1, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {p2, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/event/iv;-><init>(Landroid/content/Context;JJZ)V

    .line 1685
    iget-object v1, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v1}, Lcom/android/calendar/AllInOneActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 1686
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 1687
    sget-object v3, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 1688
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1690
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1695
    :cond_0
    sget-object v1, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1697
    :try_start_1
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1701
    :goto_0
    return-void

    .line 1691
    :catch_0
    move-exception v0

    goto :goto_0

    .line 1698
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected a(Landroid/text/format/Time;Z)V
    .locals 14

    .prologue
    .line 1087
    invoke-super/range {p0 .. p2}, Lcom/android/calendar/month/by;->a(Landroid/text/format/Time;Z)V

    .line 1088
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-nez v0, :cond_5

    .line 1095
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 1100
    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 1101
    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    cmp-long v1, v4, v2

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/month/k;->az:Z

    if-eqz v1, :cond_0

    .line 1102
    const-wide/32 v2, 0x240c8400

    iget v1, p0, Lcom/android/calendar/month/k;->Q:I

    int-to-long v6, v1

    mul-long/2addr v2, v6

    const-wide/16 v6, 0x3

    div-long/2addr v2, v6

    .line 1103
    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/al;->a(J)V

    .line 1106
    :cond_0
    new-instance v6, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-direct {v6, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1108
    iget v1, p1, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    if-ne v1, v2, :cond_1

    iget v1, p1, Landroid/text/format/Time;->month:I

    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->month:I

    if-eq v1, v2, :cond_4

    .line 1109
    :cond_1
    iget v1, p1, Landroid/text/format/Time;->year:I

    iput v1, v6, Landroid/text/format/Time;->year:I

    .line 1110
    iget v1, p1, Landroid/text/format/Time;->month:I

    iput v1, v6, Landroid/text/format/Time;->month:I

    .line 1112
    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    .line 1113
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v2

    if-le v1, v2, :cond_2

    .line 1114
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    .line 1117
    :cond_2
    iput v1, v6, Landroid/text/format/Time;->monthDay:I

    .line 1118
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1119
    iget-object v1, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, v1, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    if-eqz v1, :cond_4

    .line 1120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1121
    iget-wide v4, p0, Lcom/android/calendar/month/k;->p:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xc8

    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v1}, Lcom/android/calendar/month/ce;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-boolean v1, Lcom/android/calendar/month/h;->r:Z

    if-eqz v1, :cond_4

    .line 1122
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    .line 1123
    iget-object v1, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/android/calendar/AllInOneActivity;->a(Ljava/lang/String;)V

    .line 1128
    :cond_4
    const-wide/16 v2, 0x400

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x34

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1158
    :cond_5
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 8

    .prologue
    .line 1378
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1379
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1380
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1381
    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1, v3}, Lcom/android/calendar/month/MonthListView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1383
    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/android/calendar/month/k;->m:Lcom/android/calendar/month/MonthDayHeader;

    invoke-virtual {v1}, Lcom/android/calendar/month/MonthDayHeader;->getHeight()I

    move-result v1

    add-int v2, v0, v1

    .line 1384
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1}, Lcom/android/calendar/month/MonthListView;->getHeight()I

    move-result v1

    iget v4, p0, Lcom/android/calendar/month/k;->Q:I

    div-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x3

    sub-int v1, v0, v1

    .line 1386
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 1387
    int-to-float v4, v2

    add-float/2addr v0, v4

    .line 1389
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->r()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1390
    iget v2, v3, Landroid/graphics/Rect;->top:I

    .line 1391
    iget v1, v3, Landroid/graphics/Rect;->bottom:I

    .line 1392
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    .line 1395
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, p0, Lcom/android/calendar/month/k;->F:F

    .line 1397
    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_2

    .line 1398
    sget-object v1, Lcom/android/calendar/month/ad;->b:Lcom/android/calendar/month/ad;

    iput-object v1, p0, Lcom/android/calendar/month/k;->D:Lcom/android/calendar/month/ad;

    .line 1407
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/month/k;->D:Lcom/android/calendar/month/ad;

    sget-object v2, Lcom/android/calendar/month/ad;->a:Lcom/android/calendar/month/ad;

    if-eq v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/android/calendar/month/k;->E:Z

    if-nez v1, :cond_1

    .line 1408
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/month/k;->o:Z

    .line 1409
    iget-object v1, p0, Lcom/android/calendar/month/k;->aR:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1412
    :cond_1
    iget-boolean v1, p0, Lcom/android/calendar/month/k;->E:Z

    if-eqz v1, :cond_5

    .line 1446
    :goto_1
    return-void

    .line 1399
    :cond_2
    int-to-float v2, v1

    cmpl-float v2, v0, v2

    if-lez v2, :cond_3

    .line 1400
    sget-object v1, Lcom/android/calendar/month/ad;->c:Lcom/android/calendar/month/ad;

    iput-object v1, p0, Lcom/android/calendar/month/k;->D:Lcom/android/calendar/month/ad;

    goto :goto_0

    .line 1401
    :cond_3
    iget v2, p0, Lcom/android/calendar/month/k;->aE:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_4

    add-int/lit8 v1, v1, -0x28

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_4

    .line 1402
    sget-object v1, Lcom/android/calendar/month/ad;->c:Lcom/android/calendar/month/ad;

    iput-object v1, p0, Lcom/android/calendar/month/k;->D:Lcom/android/calendar/month/ad;

    goto :goto_0

    .line 1404
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->i()V

    goto :goto_0

    .line 1416
    :cond_5
    iget-object v4, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    .line 1418
    const/4 v2, 0x0

    .line 1419
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1420
    invoke-virtual {v4}, Lcom/android/calendar/month/MonthListView;->getChildCount()I

    move-result v6

    .line 1421
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->r()Z

    move-result v1

    if-eqz v1, :cond_7

    iget v1, v3, Landroid/graphics/Rect;->bottom:I

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    iget-object v3, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v3}, Lcom/android/calendar/month/MonthListView;->getTranslationY()F

    move-result v3

    add-float/2addr v1, v3

    const/4 v3, 0x0

    cmpg-float v1, v1, v3

    if-gez v1, :cond_7

    .line 1422
    add-int/lit8 v1, v6, -0x1

    move v7, v1

    move-object v1, v2

    move v2, v7

    :goto_2
    if-ltz v2, :cond_a

    .line 1423
    invoke-virtual {v4, v2}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1424
    invoke-virtual {v1, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1425
    iget v3, v5, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    cmpl-float v3, v0, v3

    if-lez v3, :cond_6

    move-object v0, v1

    .line 1439
    :goto_3
    if-nez v0, :cond_9

    .line 1440
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->d()V

    goto :goto_1

    .line 1422
    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 1430
    :cond_7
    const/4 v1, 0x0

    move v7, v1

    move-object v1, v2

    move v2, v7

    :goto_4
    if-ge v2, v6, :cond_a

    .line 1431
    invoke-virtual {v4, v2}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1432
    invoke-virtual {v1, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1433
    iget v3, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_8

    move-object v0, v1

    .line 1434
    goto :goto_3

    .line 1430
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1444
    :cond_9
    check-cast v0, Lcom/android/calendar/month/cd;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/cd;->c(F)I

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->z:I

    .line 1445
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->notifyDataSetChanged()V

    goto/16 :goto_1

    :cond_a
    move-object v0, v1

    goto :goto_3
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 712
    invoke-super {p0, p1}, Lcom/android/calendar/month/by;->a(Landroid/view/View;)V

    .line 714
    const v0, 0x7f1201db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    .line 716
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-nez v0, :cond_0

    .line 717
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 718
    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/MonthListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 719
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setDividerHeight(I)V

    .line 720
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const v1, 0x7f02015c

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setSelector(I)V

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, p0}, Lcom/android/calendar/month/MonthListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 724
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, p0}, Lcom/android/calendar/month/MonthListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 725
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthListView;->requestFocus()Z

    .line 726
    return-void
.end method

.method a(Lcom/android/calendar/al;Landroid/text/format/Time;Landroid/text/format/Time;)V
    .locals 19

    .prologue
    .line 1676
    const-wide/16 v4, 0x1

    const-wide/16 v6, -0x1

    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x10

    const-wide/16 v16, -0x1

    const/16 v18, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v18}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJZ)V

    .line 1680
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 12

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 977
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v6, 0x20

    cmp-long v0, v0, v6

    if-nez v0, :cond_8

    .line 988
    iget-object v0, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-direct {p0, v0}, Lcom/android/calendar/month/k;->a(Landroid/text/format/Time;)Z

    move-result v4

    .line 990
    iget-object v0, p0, Lcom/android/calendar/month/k;->aw:Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 991
    iget-object v0, p0, Lcom/android/calendar/month/k;->aw:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 992
    iget-wide v0, p1, Lcom/android/calendar/aq;->p:J

    const-wide/16 v6, 0x8

    and-long/2addr v0, v6

    cmp-long v0, v0, v8

    if-eqz v0, :cond_5

    move v0, v5

    .line 993
    :goto_0
    iget-wide v6, p1, Lcom/android/calendar/aq;->p:J

    const-wide/16 v10, 0x10

    and-long/2addr v6, v10

    cmp-long v1, v6, v8

    if-eqz v1, :cond_6

    move v1, v5

    .line 996
    :goto_1
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v4, v3

    .line 1001
    :cond_1
    if-eqz v0, :cond_2

    .line 1002
    sput-boolean v5, Lcom/android/calendar/f/i;->n:Z

    .line 1007
    :cond_2
    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    const/16 v2, 0x770

    if-ne v1, v2, :cond_3

    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_3

    .line 1008
    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    iput-boolean v3, v1, Landroid/text/format/Time;->allDay:Z

    .line 1009
    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    const/16 v2, 0x9

    iput v2, v1, Landroid/text/format/Time;->hour:I

    .line 1010
    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-virtual {v1, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 1013
    :cond_3
    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-virtual {v1, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    move-object v1, p0

    move v6, v5

    invoke-virtual/range {v1 .. v6}, Lcom/android/calendar/month/k;->a(JZZZ)Z

    move-result v1

    .line 1014
    if-eqz v0, :cond_4

    .line 1017
    iget-object v2, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v3, Lcom/android/calendar/month/x;

    invoke-direct {v3, p0}, Lcom/android/calendar/month/x;-><init>(Lcom/android/calendar/month/k;)V

    if-eqz v1, :cond_7

    const-wide/16 v0, 0x12c

    :goto_2
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1041
    :cond_4
    :goto_3
    return-void

    :cond_5
    move v0, v3

    .line 992
    goto :goto_0

    :cond_6
    move v1, v3

    .line 993
    goto :goto_1

    :cond_7
    move-wide v0, v8

    .line 1017
    goto :goto_2

    .line 1025
    :cond_8
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v6, 0x80

    cmp-long v0, v0, v6

    if-nez v0, :cond_9

    .line 1026
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->e()V

    goto :goto_3

    .line 1027
    :cond_9
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/32 v6, 0x10000

    cmp-long v0, v0, v6

    if-nez v0, :cond_a

    .line 1028
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->c()V

    goto :goto_3

    .line 1029
    :cond_a
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/32 v6, 0x8000000

    cmp-long v0, v0, v6

    if-nez v0, :cond_4

    .line 1030
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-nez v0, :cond_4

    .line 1031
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->c()V

    .line 1032
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->b()V

    .line 1033
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->d()V

    .line 1038
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    move-object v0, p0

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/k;->a(JZZZ)Z

    goto :goto_3
.end method

.method public a(Ljava/util/ArrayList;II)V
    .locals 15

    .prologue
    .line 2092
    new-instance v9, Landroid/text/format/Time;

    const-string v2, "UTC"

    invoke-direct {v9, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2093
    new-instance v10, Landroid/text/format/Time;

    const-string v2, "UTC"

    invoke-direct {v10, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2095
    move/from16 v0, p2

    invoke-static {v9, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 2096
    move/from16 v0, p3

    invoke-static {v10, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 2098
    iget-object v2, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2099
    const-string v3, "preferences_islarm_correction"

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2100
    const-string v4, "preferences_ramadan_start_day"

    const-string v5, "0"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 2101
    const-string v4, "preferences_ramadan_end_day"

    const-string v5, "0"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 2103
    new-instance v11, Lcom/tss21/calenarlib/CalendarConverter;

    const/4 v2, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v11, v2, v3}, Lcom/tss21/calenarlib/CalendarConverter;-><init>(II)V

    .line 2104
    const/4 v3, 0x0

    .line 2105
    const/4 v4, 0x0

    .line 2108
    :try_start_0
    iget v2, v9, Landroid/text/format/Time;->year:I

    iget v5, v9, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    iget v7, v9, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v11, v2, v5, v7}, Lcom/tss21/calenarlib/CalendarConverter;->b(III)Lcom/tss21/calenarlib/c;

    move-result-object v3

    .line 2109
    iget v2, v10, Landroid/text/format/Time;->year:I

    iget v5, v10, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    iget v7, v10, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v11, v2, v5, v7}, Lcom/tss21/calenarlib/CalendarConverter;->b(III)Lcom/tss21/calenarlib/c;
    :try_end_0
    .catch Lcom/tss21/calenarlib/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 2119
    :goto_0
    if-gez v6, :cond_5

    .line 2120
    const/4 v2, 0x0

    .line 2122
    if-eqz v3, :cond_0

    .line 2123
    :try_start_1
    iget v5, v3, Lcom/tss21/calenarlib/c;->a:I

    const/16 v7, 0x8

    const/4 v12, 0x1

    invoke-static {v5, v7, v12}, Lcom/tss21/calenarlib/CalendarConverter;->c(III)I
    :try_end_1
    .catch Lcom/tss21/calenarlib/a; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 2128
    :cond_0
    :goto_1
    const/16 v5, 0x8

    .line 2129
    add-int/2addr v2, v6

    add-int/lit8 v6, v2, 0x1

    .line 2135
    :goto_2
    if-gez v8, :cond_6

    .line 2136
    const/4 v2, 0x0

    .line 2138
    if-eqz v3, :cond_1

    .line 2139
    :try_start_2
    iget v7, v3, Lcom/tss21/calenarlib/c;->a:I

    const/16 v12, 0x9

    const/4 v13, 0x1

    invoke-static {v7, v12, v13}, Lcom/tss21/calenarlib/CalendarConverter;->c(III)I
    :try_end_2
    .catch Lcom/tss21/calenarlib/a; {:try_start_2 .. :try_end_2} :catch_2

    move-result v2

    .line 2144
    :cond_1
    :goto_3
    const/16 v7, 0x9

    .line 2145
    add-int/2addr v2, v8

    add-int/lit8 v8, v2, 0x1

    :goto_4
    move-object v2, p0

    .line 2151
    invoke-direct/range {v2 .. v8}, Lcom/android/calendar/month/k;->a(Lcom/tss21/calenarlib/c;Lcom/tss21/calenarlib/c;IIII)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2152
    const/4 v4, 0x0

    .line 2153
    const/4 v2, 0x0

    .line 2159
    :try_start_3
    iget v12, v3, Lcom/tss21/calenarlib/c;->a:I

    invoke-virtual {v11, v12, v5, v6}, Lcom/tss21/calenarlib/CalendarConverter;->a(III)Lcom/tss21/calenarlib/c;
    :try_end_3
    .catch Lcom/tss21/calenarlib/a; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v4

    .line 2160
    :try_start_4
    iget v3, v3, Lcom/tss21/calenarlib/c;->a:I

    invoke-virtual {v11, v3, v7, v8}, Lcom/tss21/calenarlib/CalendarConverter;->a(III)Lcom/tss21/calenarlib/c;
    :try_end_4
    .catch Lcom/tss21/calenarlib/a; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v2

    move-object v3, v4

    .line 2165
    :goto_5
    new-instance v4, Landroid/text/format/Time;

    const-string v5, "UTC"

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2167
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 2168
    if-eqz v3, :cond_2

    .line 2169
    iget v5, v3, Lcom/tss21/calenarlib/c;->a:I

    iput v5, v4, Landroid/text/format/Time;->year:I

    .line 2170
    iget v5, v3, Lcom/tss21/calenarlib/c;->b:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Landroid/text/format/Time;->month:I

    .line 2171
    iget v3, v3, Lcom/tss21/calenarlib/c;->c:I

    iput v3, v4, Landroid/text/format/Time;->monthDay:I

    .line 2172
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 2175
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-wide v12, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v12, v13}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 2177
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 2178
    if-eqz v2, :cond_3

    .line 2179
    iget v5, v2, Lcom/tss21/calenarlib/c;->a:I

    iput v5, v4, Landroid/text/format/Time;->year:I

    .line 2180
    iget v5, v2, Lcom/tss21/calenarlib/c;->b:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Landroid/text/format/Time;->month:I

    .line 2181
    iget v2, v2, Lcom/tss21/calenarlib/c;->c:I

    iput v2, v4, Landroid/text/format/Time;->monthDay:I

    .line 2182
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 2185
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-wide v4, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 2187
    new-instance v4, Lcom/android/calendar/dh;

    invoke-direct {v4}, Lcom/android/calendar/dh;-><init>()V

    .line 2189
    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/android/calendar/dh;->f:Z

    .line 2190
    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/android/calendar/dh;->h:Z

    .line 2191
    const-string v5, "UTC"

    iput-object v5, v4, Lcom/android/calendar/dh;->s:Ljava/lang/String;

    .line 2192
    const-string v5, "LOCAL"

    iput-object v5, v4, Lcom/android/calendar/dh;->x:Ljava/lang/String;

    .line 2194
    const v5, 0x7f0f0366

    invoke-virtual {p0, v5}, Lcom/android/calendar/month/k;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    .line 2196
    const-string v5, "#F27200"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Lcom/android/calendar/dh;->c:I

    .line 2198
    invoke-static {v9, v3}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/android/calendar/dh;->m:J

    .line 2199
    const/4 v5, 0x0

    iput v5, v4, Lcom/android/calendar/dh;->k:I

    .line 2200
    iput v3, v4, Lcom/android/calendar/dh;->i:I

    .line 2202
    invoke-static {v10, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/android/calendar/dh;->n:J

    .line 2203
    const/4 v3, 0x0

    iput v3, v4, Lcom/android/calendar/dh;->l:I

    .line 2204
    iput v2, v4, Lcom/android/calendar/dh;->j:I

    .line 2205
    const/4 v2, 0x0

    iput-boolean v2, v4, Lcom/android/calendar/dh;->o:Z

    .line 2207
    const/4 v2, 0x0

    iput-object v2, v4, Lcom/android/calendar/dh;->t:Ljava/lang/String;

    .line 2208
    const/4 v2, 0x0

    iput-object v2, v4, Lcom/android/calendar/dh;->u:Ljava/lang/String;

    .line 2209
    const/4 v2, 0x0

    iput-boolean v2, v4, Lcom/android/calendar/dh;->p:Z

    .line 2210
    const/4 v2, 0x0

    iput-boolean v2, v4, Lcom/android/calendar/dh;->K:Z

    .line 2211
    const-wide/16 v2, 0x0

    iput-wide v2, v4, Lcom/android/calendar/dh;->y:J

    .line 2213
    const/4 v2, 0x0

    iput-object v2, v4, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    .line 2214
    const/4 v2, 0x0

    iput-boolean v2, v4, Lcom/android/calendar/dh;->M:Z

    .line 2216
    const-wide/16 v2, -0x1

    iput-wide v2, v4, Lcom/android/calendar/dh;->b:J

    .line 2227
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2229
    :cond_4
    return-void

    .line 2110
    :catch_0
    move-exception v2

    .line 2111
    invoke-virtual {v2}, Lcom/tss21/calenarlib/a;->printStackTrace()V

    goto/16 :goto_0

    .line 2125
    :catch_1
    move-exception v5

    .line 2126
    invoke-virtual {v5}, Lcom/tss21/calenarlib/a;->printStackTrace()V

    goto/16 :goto_1

    .line 2131
    :cond_5
    const/16 v5, 0x9

    .line 2132
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    .line 2141
    :catch_2
    move-exception v7

    .line 2142
    invoke-virtual {v7}, Lcom/tss21/calenarlib/a;->printStackTrace()V

    goto/16 :goto_3

    .line 2147
    :cond_6
    const/16 v7, 0xa

    .line 2148
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_4

    .line 2161
    :catch_3
    move-exception v3

    move-object v14, v3

    move-object v3, v4

    move-object v4, v14

    .line 2162
    :goto_6
    invoke-virtual {v4}, Lcom/tss21/calenarlib/a;->printStackTrace()V

    goto/16 :goto_5

    .line 2161
    :catch_4
    move-exception v3

    move-object v14, v3

    move-object v3, v4

    move-object v4, v14

    goto :goto_6
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 586
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 587
    if-eqz v0, :cond_2

    .line 589
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getView()Landroid/view/View;

    move-result-object v1

    .line 591
    if-nez v1, :cond_1

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 594
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 595
    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->a()V

    .line 598
    :cond_2
    if-eqz v0, :cond_0

    .line 599
    if-eqz p1, :cond_3

    .line 600
    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->a(I)V

    .line 601
    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 602
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->b(I)V

    .line 603
    if-eqz v1, :cond_0

    .line 604
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 607
    :cond_3
    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->a(I)V

    .line 608
    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->b(I)V

    .line 609
    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->b()V

    goto :goto_0
.end method

.method public a(JZZZ)Z
    .locals 9

    .prologue
    .line 1707
    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/month/k;->a(JZZZZ)Z

    move-result v0

    return v0
.end method

.method public a(JZZZZ)Z
    .locals 13

    .prologue
    .line 1712
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 1713
    const-string v0, "MonthFragment"

    const-string v1, "time is invalid"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1714
    const/4 v0, 0x0

    .line 1983
    :goto_0
    return v0

    .line 1717
    :cond_0
    const-wide/16 v0, 0x0

    .line 1722
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v5, v2, Landroid/text/format/Time;->month:I

    .line 1723
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v6, v2, Landroid/text/format/Time;->year:I

    .line 1724
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 1727
    if-eqz p4, :cond_1

    .line 1728
    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v4, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 1729
    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->normalize(Z)J

    .line 1732
    :cond_1
    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->year:I

    if-ne v6, v4, :cond_6

    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->month:I

    if-ne v5, v4, :cond_6

    .line 1733
    sget-boolean v4, Lcom/android/calendar/month/k;->X:Z

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/android/calendar/month/k;->a:Z

    if-eqz v4, :cond_2

    .line 1734
    const/16 p5, 0x0

    .line 1740
    :cond_2
    :goto_1
    sget-boolean v4, Lcom/android/calendar/month/k;->q:Z

    if-eqz v4, :cond_3

    .line 1741
    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->year:I

    if-ne v6, v4, :cond_7

    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->month:I

    if-ne v5, v4, :cond_7

    .line 1742
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/android/calendar/month/k;->b(Z)V

    .line 1743
    iget-object v2, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v2}, Lcom/android/calendar/month/ce;->g()Landroid/text/format/Time;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1744
    iget-object v2, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v2}, Lcom/android/calendar/month/ce;->g()Landroid/text/format/Time;

    move-result-object v2

    .line 1745
    iget v3, v2, Landroid/text/format/Time;->year:I

    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->year:I

    if-ne v3, v4, :cond_3

    iget v3, v2, Landroid/text/format/Time;->month:I

    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->month:I

    if-ne v3, v4, :cond_3

    iget v3, v2, Landroid/text/format/Time;->hour:I

    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->hour:I

    if-ne v3, v4, :cond_3

    iget v2, v2, Landroid/text/format/Time;->minute:I

    iget-object v3, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->minute:I

    if-ne v2, v3, :cond_3

    .line 1749
    iget-object v2, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1750
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/android/calendar/month/k;->b(Z)V

    .line 1751
    iget-object v2, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/calendar/AllInOneActivity;->f(Z)V

    .line 1771
    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    if-eqz v2, :cond_4

    .line 1772
    iget-object v2, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 1782
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->isResumed()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1783
    const-string v0, "MonthFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1784
    const-string v0, "MonthFragment"

    const-string v1, "We\'re not visible yet"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1786
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1737
    :cond_6
    const-wide/16 v0, 0x2bc

    goto :goto_1

    .line 1756
    :cond_7
    iget-boolean v4, p0, Lcom/android/calendar/month/k;->aI:Z

    if-eqz v4, :cond_8

    .line 1757
    iget-object v4, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    const-wide/16 v8, 0x2bc

    invoke-static {v4, v8, v9}, Lcom/android/calendar/month/k;->a(Landroid/os/Handler;J)V

    .line 1760
    :cond_8
    cmp-long v2, v2, p1

    if-lez v2, :cond_9

    .line 1761
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/calendar/month/k;->aF:Z

    .line 1765
    :goto_3
    sget-boolean v2, Lcom/android/calendar/month/k;->aF:Z

    invoke-virtual {p0, v2}, Lcom/android/calendar/month/k;->a(Z)V

    .line 1768
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/calendar/month/cd;->bN:Z

    goto :goto_2

    .line 1763
    :cond_9
    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/calendar/month/k;->aF:Z

    goto :goto_3

    .line 1789
    :cond_a
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x76e

    if-ge v2, v3, :cond_13

    .line 1791
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/16 v3, 0x76e

    iput v3, v2, Landroid/text/format/Time;->year:I

    .line 1792
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v3, 0x0

    iput v3, v2, Landroid/text/format/Time;->month:I

    .line 1793
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 1794
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 1796
    iget-object v2, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1804
    :goto_4
    iget-object v2, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 1807
    iget-object v4, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget-wide v8, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v8, v9}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    iget v3, p0, Lcom/android/calendar/month/k;->ad:I

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(II)I

    move-result v7

    .line 1812
    const/4 v3, 0x0

    .line 1813
    const/4 v2, 0x0

    .line 1816
    :goto_5
    iget-object v8, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v8, v3}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1817
    if-nez v3, :cond_15

    move v4, v2

    .line 1828
    :goto_6
    if-eqz v3, :cond_17

    .line 1829
    iget-object v2, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v2, v3}, Lcom/android/calendar/month/MonthListView;->getPositionForView(Landroid/view/View;)I

    move-result v2

    .line 1833
    :goto_7
    iget v3, p0, Lcom/android/calendar/month/k;->Q:I

    add-int/2addr v3, v2

    add-int/lit8 v3, v3, -0x1

    .line 1834
    iget v8, p0, Lcom/android/calendar/month/k;->K:I

    if-le v4, v8, :cond_27

    .line 1835
    add-int/lit8 v3, v3, -0x1

    move v4, v3

    .line 1838
    :goto_8
    if-eqz p4, :cond_b

    .line 1839
    iget-object v3, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    if-eqz v3, :cond_b

    .line 1840
    iget-object v3, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    iget-object v8, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v3, v8}, Lcom/android/calendar/month/ce;->a(Landroid/text/format/Time;)V

    .line 1844
    :cond_b
    const-string v3, "MonthFragment"

    const/4 v8, 0x3

    invoke-static {v3, v8}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1845
    const-string v3, "MonthFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GoTo position "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1849
    :cond_c
    if-ltz v7, :cond_d

    const/4 v3, 0x5

    if-gt v7, v3, :cond_d

    .line 1850
    const/16 p5, 0x1

    .line 1856
    :cond_d
    if-lt v7, v2, :cond_e

    if-gt v7, v4, :cond_e

    if-eqz p5, :cond_20

    .line 1857
    :cond_e
    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget-object v8, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1858
    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    const/4 v8, 0x1

    iput v8, v3, Landroid/text/format/Time;->monthDay:I

    .line 1859
    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    invoke-static {v3}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;)V

    .line 1860
    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    .line 1861
    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget-wide v10, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v10, v11}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    iget v8, p0, Lcom/android/calendar/month/k;->ad:I

    invoke-static {v3, v8}, Lcom/android/calendar/hj;->a(II)I

    move-result v8

    .line 1864
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/calendar/month/k;->al:I

    .line 1866
    const/4 v3, 0x0

    sput v3, Lcom/android/calendar/month/k;->I:I

    .line 1868
    if-nez p6, :cond_1f

    .line 1869
    if-eqz p3, :cond_1e

    .line 1870
    iget-boolean v3, p0, Lcom/android/calendar/month/k;->x:Z

    if-eqz v3, :cond_18

    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->year:I

    if-ne v6, v3, :cond_f

    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->month:I

    if-eq v5, v3, :cond_18

    .line 1872
    :cond_f
    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    const/4 v5, 0x1

    invoke-virtual {p0, v3, v5}, Lcom/android/calendar/month/k;->a(Landroid/text/format/Time;Z)V

    .line 1873
    iget-object v3, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    sget v5, Lcom/android/calendar/month/k;->I:I

    invoke-virtual {v3, v8, v5}, Lcom/android/calendar/month/MonthListView;->setSelectionFromTop(II)V

    .line 1875
    iget-object v3, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, Lcom/android/calendar/month/k;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 1905
    :goto_9
    iget-boolean v3, p0, Lcom/android/calendar/month/k;->x:Z

    if-eqz v3, :cond_10

    .line 1906
    sub-int v3, v7, v8

    .line 1907
    if-ltz v3, :cond_10

    .line 1908
    invoke-virtual {p0, v3}, Lcom/android/calendar/month/k;->b(I)V

    .line 1937
    :cond_10
    :goto_a
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->m()V

    .line 1939
    sget-boolean v3, Lcom/android/calendar/month/k;->X:Z

    if-eqz v3, :cond_11

    iget-boolean v3, p0, Lcom/android/calendar/month/k;->a:Z

    if-eqz v3, :cond_12

    .line 1940
    :cond_11
    sget v3, Lcom/android/calendar/month/k;->n:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_21

    .line 1950
    :cond_12
    iget-object v3, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/dz;->D(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_22

    sget v3, Lcom/android/calendar/month/k;->n:I

    const/4 v5, 0x1

    if-ne v3, v5, :cond_22

    iget v3, p0, Lcom/android/calendar/month/k;->aE:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_22

    .line 1952
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1797
    :cond_13
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7f4

    if-le v2, v3, :cond_14

    .line 1798
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const v3, 0x259d23

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1799
    iget-object v2, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto/16 :goto_4

    .line 1801
    :cond_14
    iget-object v2, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-virtual {v2, p1, p2}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_4

    .line 1820
    :cond_15
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v2

    .line 1821
    const-string v8, "MonthFragment"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 1822
    const-string v8, "MonthFragment"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "child at "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v4, -0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " has top "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1824
    :cond_16
    if-ltz v2, :cond_28

    move v4, v2

    goto/16 :goto_6

    .line 1831
    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 1878
    :cond_18
    const/16 v3, 0x12c

    .line 1879
    if-le v8, v2, :cond_19

    .line 1880
    const/16 v3, 0xc8

    .line 1881
    const/4 v9, -0x1

    sput v9, Lcom/android/calendar/month/k;->I:I

    .line 1883
    :cond_19
    sub-int v9, v2, v8

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    const/4 v10, 0x1

    if-gt v9, v10, :cond_1a

    .line 1884
    const/16 v3, 0x96

    .line 1886
    :cond_1a
    sget-boolean v9, Lcom/android/calendar/month/h;->q:Z

    if-nez v9, :cond_1b

    sget-boolean v9, Lcom/android/calendar/month/h;->w:Z

    if-eqz v9, :cond_1c

    .line 1887
    :cond_1b
    const/16 v3, 0x64

    .line 1890
    :cond_1c
    iget-object v9, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v9, v9, Landroid/text/format/Time;->year:I

    if-ne v6, v9, :cond_1d

    iget-object v6, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v6, v6, Landroid/text/format/Time;->month:I

    if-ne v5, v6, :cond_1d

    .line 1891
    iget-object v5, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    const-wide/16 v10, 0x64

    invoke-static {v5, v10, v11}, Lcom/android/calendar/month/k;->a(Landroid/os/Handler;J)V

    .line 1896
    :goto_b
    iget-object v5, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    sget v6, Lcom/android/calendar/month/k;->I:I

    invoke-virtual {v5, v8, v6, v3}, Lcom/android/calendar/month/MonthListView;->smoothScrollToPositionFromTop(III)V

    goto/16 :goto_9

    .line 1893
    :cond_1d
    iget-object v5, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    const-wide/16 v10, 0x1f4

    invoke-static {v5, v10, v11}, Lcom/android/calendar/month/k;->a(Landroid/os/Handler;J)V

    goto :goto_b

    .line 1899
    :cond_1e
    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    const/4 v5, 0x1

    invoke-virtual {p0, v3, v5}, Lcom/android/calendar/month/k;->a(Landroid/text/format/Time;Z)V

    .line 1900
    iget-object v3, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    sget v5, Lcom/android/calendar/month/k;->I:I

    invoke-virtual {v3, v8, v5}, Lcom/android/calendar/month/MonthListView;->setSelectionFromTop(II)V

    .line 1902
    iget-object v3, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, Lcom/android/calendar/month/k;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto/16 :goto_9

    .line 1912
    :cond_1f
    const-string v3, "MonthFragment"

    const-string v5, "forceNotScroll"

    invoke-static {v3, v5}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1914
    iget-object v3, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    const/4 v5, 0x1

    invoke-virtual {p0, v3, v5}, Lcom/android/calendar/month/k;->a(Landroid/text/format/Time;Z)V

    .line 1915
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/android/calendar/month/o;

    invoke-direct {v5, p0, v8}, Lcom/android/calendar/month/o;-><init>(Lcom/android/calendar/month/k;I)V

    const-wide/16 v10, 0x32

    invoke-virtual {v3, v5, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1922
    iget-object v3, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, Lcom/android/calendar/month/k;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 1924
    iget-boolean v3, p0, Lcom/android/calendar/month/k;->x:Z

    if-eqz v3, :cond_10

    .line 1925
    sub-int v3, v7, v8

    .line 1926
    if-ltz v3, :cond_10

    .line 1927
    invoke-virtual {p0, v3}, Lcom/android/calendar/month/k;->b(I)V

    goto/16 :goto_a

    .line 1932
    :cond_20
    if-eqz p4, :cond_10

    .line 1934
    iget-object v3, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v5, 0x1

    invoke-virtual {p0, v3, v5}, Lcom/android/calendar/month/k;->a(Landroid/text/format/Time;Z)V

    goto/16 :goto_a

    .line 1944
    :cond_21
    iget-object v3, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->l()Z

    move-result v3

    if-nez v3, :cond_12

    .line 1945
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1958
    :cond_22
    add-int/lit8 v2, v2, -0x1

    if-lt v7, v2, :cond_23

    if-gt v7, v4, :cond_23

    .line 1959
    iget-object v2, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v3, Lcom/android/calendar/month/p;

    invoke-direct {v3, p0}, Lcom/android/calendar/month/p;-><init>(Lcom/android/calendar/month/k;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1969
    :cond_23
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 1970
    if-eqz v0, :cond_25

    .line 1971
    iget-boolean v1, p0, Lcom/android/calendar/month/k;->aI:Z

    if-nez v1, :cond_26

    iget-object v1, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->l()Z

    move-result v1

    if-nez v1, :cond_26

    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_26

    iget v1, p0, Lcom/android/calendar/month/k;->aE:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_26

    .line 1974
    iget-object v1, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    if-eqz v1, :cond_25

    iget-object v1, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v1}, Lcom/android/calendar/month/ce;->h()Z

    move-result v1

    if-eqz v1, :cond_24

    sget-boolean v1, Lcom/android/calendar/month/h;->r:Z

    if-eqz v1, :cond_25

    .line 1977
    :cond_24
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->e(Z)V

    .line 1983
    :cond_25
    :goto_c
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1980
    :cond_26
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    goto :goto_c

    :cond_27
    move v4, v3

    goto/16 :goto_8

    :cond_28
    move v3, v4

    goto/16 :goto_5
.end method

.method public a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFZ)Z
    .locals 13

    .prologue
    .line 393
    sget v0, Lcom/android/calendar/month/k;->u:I

    sget v1, Lcom/android/calendar/month/k;->s:I

    sub-int/2addr v0, v1

    .line 394
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 395
    sget v2, Lcom/android/calendar/month/k;->v:I

    sget v3, Lcom/android/calendar/month/k;->t:I

    sub-int v8, v2, v3

    .line 396
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 401
    sget-boolean v3, Lcom/android/calendar/month/k;->r:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/android/calendar/month/k;->y:Z

    if-eqz v3, :cond_1

    .line 402
    :cond_0
    const/4 v0, 0x0

    .line 531
    :goto_0
    return v0

    .line 405
    :cond_1
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/calendar/month/k;->z:Z

    .line 407
    const/16 v3, 0x4b

    if-lt v1, v3, :cond_e

    if-le v1, v2, :cond_e

    .line 408
    iget-boolean v1, p0, Lcom/android/calendar/month/k;->a:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/android/calendar/month/k;->A:Z

    if-eqz v1, :cond_3

    .line 409
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 412
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    sget v2, Lcom/android/calendar/month/k;->ah:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 413
    const/16 v2, 0x8

    if-le v1, v2, :cond_4

    .line 414
    add-int/lit8 v1, v1, -0xb

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 417
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    sget v2, Lcom/android/calendar/month/k;->ah:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_5

    .line 418
    const/4 v0, 0x0

    goto :goto_0

    .line 421
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 423
    iget-object v1, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 425
    iget-object v1, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 427
    if-eqz p5, :cond_7

    .line 428
    if-gez v0, :cond_6

    .line 429
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 460
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 463
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-eqz v0, :cond_c

    .line 464
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    .line 465
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/k;->z:Z

    .line 466
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/k;->A:Z

    .line 467
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/month/k;->am:I

    .line 468
    const/4 v0, 0x1

    goto :goto_0

    .line 431
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    goto :goto_1

    .line 433
    :cond_7
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->r()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 434
    if-gez v0, :cond_8

    .line 435
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x7

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    goto :goto_1

    .line 437
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, -0x7

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    goto :goto_1

    .line 440
    :cond_9
    if-gez v0, :cond_b

    .line 444
    iget-object v1, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v2, v1, Landroid/text/format/Time;->month:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/text/format/Time;->month:I

    .line 451
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/calendar/month/k;->a(ILandroid/text/format/Time;Landroid/text/format/Time;)V

    .line 452
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    .line 453
    iget-object v0, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    iget-object v0, v0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_a

    .line 454
    iget-object v0, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->c(Z)V

    .line 457
    :cond_a
    sget-object v0, Lcom/android/calendar/hj;->v:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-string v2, "LeftRightFlick"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 449
    :cond_b
    iget-object v1, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v2, v1, Landroid/text/format/Time;->month:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Landroid/text/format/Time;->month:I

    goto :goto_2

    .line 472
    :cond_c
    if-eqz p5, :cond_d

    iget-boolean v0, p0, Lcom/android/calendar/month/k;->x:Z

    if-eqz v0, :cond_d

    .line 473
    const-wide/16 v9, 0x1

    .line 482
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v2, 0x20

    iget-object v4, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 485
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 475
    :cond_d
    const-wide/16 v9, 0x10

    .line 477
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/month/k;->b(Z)V

    .line 479
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    const-wide/16 v2, 0x320

    invoke-static {v0, v2, v3}, Lcom/android/calendar/month/k;->a(Landroid/os/Handler;J)V

    goto :goto_3

    .line 487
    :cond_e
    const/16 v0, 0x4b

    if-lt v2, v0, :cond_14

    if-le v2, v1, :cond_14

    .line 489
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->x:Z

    if-nez v0, :cond_f

    if-eqz p5, :cond_10

    .line 490
    :cond_f
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 493
    :cond_10
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 495
    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/MonthListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 496
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 498
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->month:I

    sget v1, Lcom/android/calendar/month/k;->ah:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 499
    const/16 v1, 0x8

    if-le v0, v1, :cond_11

    .line 500
    add-int/lit8 v0, v0, -0xb

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 506
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 507
    iget-object v0, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 508
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 509
    if-gez v8, :cond_12

    .line 510
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 514
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-virtual {p0, v8, v0, v1}, Lcom/android/calendar/month/k;->a(ILandroid/text/format/Time;Landroid/text/format/Time;)V

    .line 515
    sget-object v0, Lcom/android/calendar/hj;->v:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-string v2, "UpDownFlick"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 516
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-eqz v0, :cond_13

    .line 517
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    .line 518
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/k;->z:Z

    .line 519
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/k;->A:Z

    .line 520
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/month/k;->am:I

    .line 521
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 512
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/text/format/Time;->month:I

    goto :goto_4

    .line 524
    :cond_13
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Lcom/android/calendar/month/k;->a(Landroid/os/Handler;J)V

    .line 525
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/k;->w:Z

    .line 526
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    iget-object v1, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v2, 0x20

    iget-object v4, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 528
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 531
    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected b()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 677
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 678
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    .line 681
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->ad:I

    .line 682
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->R:Z

    .line 685
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->aI:Z

    if-eqz v0, :cond_1

    .line 686
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->ad:I

    .line 687
    iput-boolean v2, p0, Lcom/android/calendar/month/k;->R:Z

    .line 690
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 691
    const-string v0, "num_weeks"

    iget v4, p0, Lcom/android/calendar/month/k;->Q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 692
    const-string v4, "week_numbers"

    iget-boolean v0, p0, Lcom/android/calendar/month/k;->R:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 693
    const-string v0, "week_start"

    iget v4, p0, Lcom/android/calendar/month/k;->ad:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 694
    const-string v4, "mini_month"

    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    const-string v0, "selected_day"

    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v4, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-object v6, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-wide v6, v6, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    const-string v0, "days_per_week"

    iget v4, p0, Lcom/android/calendar/month/k;->S:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 699
    const-string v0, "tablet"

    sget-boolean v4, Lcom/android/calendar/month/k;->X:Z

    if-eqz v4, :cond_4

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 701
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    if-nez v0, :cond_5

    .line 702
    new-instance v0, Lcom/android/calendar/month/h;

    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/month/h;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    .line 703
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    iget-object v1, p0, Lcom/android/calendar/month/k;->aq:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 707
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->notifyDataSetChanged()V

    .line 708
    return-void

    :cond_2
    move v0, v2

    .line 692
    goto :goto_0

    :cond_3
    move v0, v2

    .line 694
    goto :goto_1

    :cond_4
    move v1, v2

    .line 699
    goto :goto_2

    .line 705
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0, v3}, Lcom/android/calendar/month/ce;->a(Ljava/util/HashMap;)V

    goto :goto_3
.end method

.method b(I)V
    .locals 4

    .prologue
    .line 2949
    iget v0, p0, Lcom/android/calendar/month/k;->aL:I

    neg-int v1, p1

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x6

    .line 2950
    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1}, Lcom/android/calendar/month/MonthListView;->getTranslationY()F

    move-result v1

    float-to-int v1, v1

    .line 2952
    sub-int v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    .line 2957
    :goto_0
    return-void

    .line 2956
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v1}, Lcom/android/calendar/month/MonthListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2469
    const v0, 0x7f1201dd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/android/calendar/month/k;->i:Landroid/view/ViewStub;

    .line 2470
    const v0, 0x7f12009d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/MonthEventList;

    iput-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    .line 2474
    const v0, 0x7f12009c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->j:Landroid/view/View;

    .line 2484
    sget-boolean v0, Lcom/android/calendar/month/k;->X:Z

    if-eqz v0, :cond_0

    .line 2485
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->o()V

    .line 2487
    :cond_0
    return-void
.end method

.method public c()V
    .locals 12

    .prologue
    const/4 v8, 0x4

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 910
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->ad:I

    .line 911
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->R:Z

    .line 914
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->aI:Z

    if-eqz v0, :cond_0

    .line 915
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->ad:I

    .line 916
    iput-boolean v11, p0, Lcom/android/calendar/month/k;->R:Z

    .line 919
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->b:Z

    .line 920
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->h(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->c:Z

    .line 925
    iget-object v1, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    monitor-enter v1

    .line 926
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 927
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->ay:Z

    .line 928
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 929
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 931
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->k(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->S:I

    .line 932
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->d()V

    .line 933
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->a(Landroid/text/format/Time;)V

    .line 934
    iget-object v0, p0, Lcom/android/calendar/month/k;->aN:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 935
    iget-object v0, p0, Lcom/android/calendar/month/k;->ap:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 938
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->o:Z

    if-eqz v0, :cond_2

    .line 939
    iput-boolean v11, p0, Lcom/android/calendar/month/k;->o:Z

    move v9, v10

    .line 943
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-nez v0, :cond_1

    .line 944
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-ne v0, v8, :cond_1

    .line 949
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    const-wide/16 v2, 0x400

    iget-object v4, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const-wide/16 v6, -0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 953
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    move-object v1, p0

    move v4, v11

    move v5, v10

    move v6, v9

    invoke-virtual/range {v1 .. v6}, Lcom/android/calendar/month/k;->a(JZZZ)Z

    .line 954
    return-void

    .line 929
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v9, v11

    goto :goto_0
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Lcom/android/calendar/month/k;->m:Lcom/android/calendar/month/MonthDayHeader;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthDayHeader;->c()V

    .line 958
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 963
    iget-object v1, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    monitor-enter v1

    .line 964
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/calendar/month/k;->ay:Z

    .line 965
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 966
    monitor-exit v1

    .line 967
    return-void

    .line 966
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 1355
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->j()V

    .line 1356
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->i()V

    .line 1357
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->d()V

    .line 1358
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 971
    const-wide/32 v0, 0x80000a0

    .line 972
    return-wide v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 1361
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->d()V

    .line 1362
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/h;->x:Z

    .line 1363
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->p()V

    .line 1364
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 1449
    sget-object v0, Lcom/android/calendar/month/ad;->a:Lcom/android/calendar/month/ad;

    iput-object v0, p0, Lcom/android/calendar/month/k;->D:Lcom/android/calendar/month/ad;

    .line 1450
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->E:Z

    .line 1451
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/k;->aR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1452
    return-void
.end method

.method j()V
    .locals 26

    .prologue
    .line 1488
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v4

    .line 1489
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v7

    .line 1491
    sget v6, Lcom/android/calendar/month/h;->y:I

    .line 1492
    sget v5, Lcom/android/calendar/month/h;->z:I

    .line 1493
    sget v8, Lcom/android/calendar/month/h;->y:I

    sget v9, Lcom/android/calendar/month/h;->z:I

    if-le v8, v9, :cond_0

    .line 1494
    sget v6, Lcom/android/calendar/month/h;->z:I

    .line 1495
    sget v5, Lcom/android/calendar/month/h;->y:I

    .line 1498
    :cond_0
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8, v7}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1499
    invoke-static {v8, v6}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 1500
    const/16 v9, 0x8

    iput v9, v8, Landroid/text/format/Time;->hour:I

    .line 1502
    new-instance v23, Landroid/text/format/Time;

    move-object/from16 v0, v23

    invoke-direct {v0, v7}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1503
    move-object/from16 v0, v23

    invoke-static {v0, v5}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 1504
    const/16 v9, 0x8

    move-object/from16 v0, v23

    iput v9, v0, Landroid/text/format/Time;->hour:I

    .line 1506
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-wide v12, v9, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v10, v11, v12, v13}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v9

    .line 1507
    const/4 v10, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-wide v12, v12, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v10, v11, v12, v13}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v10

    .line 1509
    const v11, 0x24dc87

    if-lt v9, v11, :cond_1

    const v11, 0x24dc87

    if-ge v10, v11, :cond_2

    .line 1511
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/k;->k()V

    .line 1512
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/k;->i()V

    .line 1513
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v5}, Lcom/android/calendar/month/ce;->d()V

    .line 1514
    const v5, 0x24dc88

    invoke-virtual {v8, v5}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1516
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v6, 0x20

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const-wide/16 v13, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v9, v8

    invoke-virtual/range {v4 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1645
    :goto_0
    return-void

    .line 1519
    :cond_2
    const v11, 0x259d23

    if-gt v9, v11, :cond_3

    const v9, 0x259d23

    if-le v10, v9, :cond_4

    .line 1521
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/k;->k()V

    .line 1522
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/k;->i()V

    .line 1523
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v5}, Lcom/android/calendar/month/ce;->d()V

    .line 1524
    const v5, 0x259d23

    invoke-virtual {v8, v5}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1526
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v6, 0x20

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const-wide/16 v13, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v9, v8

    invoke-virtual/range {v4 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 1544
    :cond_4
    if-ne v6, v5, :cond_7

    .line 1546
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->month:I

    iget v6, v8, Landroid/text/format/Time;->month:I

    if-eq v5, v6, :cond_5

    .line 1547
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v6, Lcom/android/calendar/month/aa;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v6, v0, v4, v8, v1}, Lcom/android/calendar/month/aa;-><init>(Lcom/android/calendar/month/k;Lcom/android/calendar/al;Landroid/text/format/Time;Landroid/text/format/Time;)V

    const-wide/16 v8, 0xa

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1567
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v6, 0x20

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const-wide/16 v13, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v9, v8

    invoke-virtual/range {v4 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1570
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v4}, Lcom/android/calendar/hj;->r(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v5

    .line 1572
    sget-boolean v4, Lcom/android/calendar/month/h;->x:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v4}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1573
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v5, Lcom/android/calendar/month/ac;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v5, v0, v8, v1}, Lcom/android/calendar/month/ac;-><init>(Lcom/android/calendar/month/k;Landroid/text/format/Time;Landroid/text/format/Time;)V

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1580
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    const-wide/16 v6, 0x1

    const-wide/16 v8, -0x1

    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    goto/16 :goto_0

    .line 1584
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget-wide v12, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v10, v11, v12, v13}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v24

    .line 1587
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->year:I

    .line 1588
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/month/k;->ae:Landroid/text/format/Time;

    iget v6, v6, Landroid/text/format/Time;->month:I

    .line 1590
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 1591
    const/4 v10, 0x1

    invoke-virtual {v9, v10, v5}, Ljava/util/Calendar;->set(II)V

    .line 1592
    const/4 v5, 0x2

    invoke-virtual {v9, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 1593
    const/4 v5, 0x5

    invoke-virtual {v9, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v5

    .line 1595
    add-int v5, v5, v24

    add-int/lit8 v25, v5, -0x1

    .line 1597
    sget v5, Lcom/android/calendar/month/h;->y:I

    move/from16 v0, v24

    if-lt v5, v0, :cond_a

    sget v5, Lcom/android/calendar/month/h;->y:I

    move/from16 v0, v25

    if-gt v5, v0, :cond_a

    .line 1599
    sget v5, Lcom/android/calendar/month/h;->y:I

    sget v6, Lcom/android/calendar/month/h;->z:I

    if-ge v5, v6, :cond_9

    .line 1600
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v6, 0x20

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const-wide/16 v13, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v9, v8

    invoke-virtual/range {v4 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1625
    :cond_8
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v6, Lcom/android/calendar/month/m;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v6, v0, v8, v1, v4}, Lcom/android/calendar/month/m;-><init>(Lcom/android/calendar/month/k;Landroid/text/format/Time;Landroid/text/format/Time;Lcom/android/calendar/al;)V

    sget v4, Lcom/android/calendar/month/h;->y:I

    sget v7, Lcom/android/calendar/month/h;->z:I

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v4, v7, v1, v2}, Lcom/android/calendar/month/k;->a(IIII)I

    move-result v4

    int-to-long v8, v4

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1603
    :cond_9
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v12, 0x20

    const-wide/16 v16, -0x1

    const/16 v18, 0x0

    const-wide/16 v19, 0x1

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object v10, v4

    move-object/from16 v14, v23

    move-object/from16 v15, v23

    invoke-virtual/range {v10 .. v22}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_1

    .line 1606
    :cond_a
    sget v5, Lcom/android/calendar/month/h;->z:I

    move/from16 v0, v24

    if-lt v5, v0, :cond_c

    sget v5, Lcom/android/calendar/month/h;->z:I

    move/from16 v0, v25

    if-gt v5, v0, :cond_c

    .line 1608
    sget v5, Lcom/android/calendar/month/h;->y:I

    sget v6, Lcom/android/calendar/month/h;->z:I

    if-ge v5, v6, :cond_b

    .line 1609
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v12, 0x20

    const-wide/16 v16, -0x1

    const/16 v18, 0x0

    const-wide/16 v19, 0x1

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object v10, v4

    move-object/from16 v14, v23

    move-object/from16 v15, v23

    invoke-virtual/range {v10 .. v22}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_1

    .line 1612
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v6, 0x20

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const-wide/16 v13, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v9, v8

    invoke-virtual/range {v4 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_1

    .line 1616
    :cond_c
    new-instance v14, Landroid/text/format/Time;

    invoke-direct {v14, v7}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1617
    move/from16 v0, v24

    invoke-static {v14, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 1618
    const/16 v5, 0x8

    iput v5, v14, Landroid/text/format/Time;->hour:I

    .line 1619
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v12, 0x20

    const-wide/16 v16, -0x1

    const/16 v18, 0x0

    const-wide/16 v19, 0x1

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object v10, v4

    move-object v15, v14

    invoke-virtual/range {v10 .. v22}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1621
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v5

    if-nez v5, :cond_d

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1622
    :cond_d
    const/4 v5, 0x1

    invoke-virtual {v14, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x1

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v14}, Lcom/android/calendar/month/k;->a(JZZZ)Z

    goto/16 :goto_1
.end method

.method public k()V
    .locals 1

    .prologue
    .line 2419
    iget-object v0, p0, Lcom/android/calendar/month/k;->aD:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 2420
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/k;->a(Landroid/content/Context;)V

    .line 2422
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->aD:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 2423
    iget-object v0, p0, Lcom/android/calendar/month/k;->aD:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2425
    :cond_1
    return-void
.end method

.method l()V
    .locals 4

    .prologue
    .line 2455
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/k;->H:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2456
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->aG:Z

    .line 2457
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/k;->H:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2458
    return-void
.end method

.method m()V
    .locals 7

    .prologue
    .line 2490
    iget-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    if-nez v0, :cond_1

    .line 2516
    :cond_0
    :goto_0
    return-void

    .line 2494
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 2497
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2498
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2499
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2501
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    check-cast v0, Lcom/android/calendar/month/h;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/h;->a(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 2502
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    check-cast v0, Lcom/android/calendar/month/h;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/h;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2504
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 2505
    invoke-static {v2, v5, v1, v1}, Lcom/android/calendar/month/k;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    .line 2507
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 2508
    iget-object v5, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v3, v0, v1, v1, v5}, Lcom/android/calendar/month/k;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;IILandroid/content/Context;)V

    .line 2511
    :cond_3
    invoke-static {v4, v2, v3}, Lcom/android/calendar/month/ba;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 2514
    iget-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthEventList;->setSelectedDay(Landroid/text/format/Time;)V

    .line 2515
    iget-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0, v4}, Lcom/android/calendar/month/MonthEventList;->setEventsAndTasks(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public n()J
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2530
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 2531
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2532
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 2533
    iget v0, v1, Landroid/text/format/Time;->minute:I

    if-lez v0, :cond_0

    .line 2534
    iget v0, v1, Landroid/text/format/Time;->hour:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Landroid/text/format/Time;->hour:I

    .line 2535
    iput v2, v1, Landroid/text/format/Time;->minute:I

    .line 2537
    :cond_0
    iget-boolean v0, v1, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_2

    iget v0, v1, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_1

    iget v0, v1, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_1

    iget v0, v1, Landroid/text/format/Time;->second:I

    if-eqz v0, :cond_2

    .line 2538
    :cond_1
    iput v2, v1, Landroid/text/format/Time;->hour:I

    .line 2539
    iput v2, v1, Landroid/text/format/Time;->minute:I

    .line 2540
    iput v2, v1, Landroid/text/format/Time;->second:I

    .line 2546
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public o()V
    .locals 4

    .prologue
    .line 2682
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    .line 2683
    iget-object v1, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->m()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->l()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/android/calendar/month/k;->aH:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/month/k;->aI:Z

    if-nez v0, :cond_1

    .line 2684
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->e()V

    .line 2685
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/k;->a(I)V

    .line 2704
    :goto_0
    return-void

    .line 2687
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->r()Z

    move-result v0

    .line 2689
    if-eqz v0, :cond_2

    .line 2690
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/k;->a(J)V

    .line 2692
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/month/r;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/r;-><init>(Lcom/android/calendar/month/k;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2701
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/k;->a(I)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 752
    invoke-super {p0, p1}, Lcom/android/calendar/month/by;->onActivityCreated(Landroid/os/Bundle;)V

    .line 754
    new-instance v0, Lcom/android/calendar/month/ae;

    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/ae;-><init>(Lcom/android/calendar/month/k;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->as:Landroid/content/AsyncQueryHandler;

    .line 756
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gx;->a(Landroid/app/FragmentManager;)Landroid/util/LruCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->h:Landroid/util/LruCache;

    .line 757
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 647
    invoke-super {p0, p1}, Lcom/android/calendar/month/by;->onAttach(Landroid/app/Activity;)V

    .line 649
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->aI:Z

    .line 651
    iget-object v0, p0, Lcom/android/calendar/month/k;->aN:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 652
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->a(Landroid/text/format/Time;)V

    .line 656
    :cond_0
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/android/calendar/month/ai;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/ai;-><init>(Lcom/android/calendar/month/k;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->av:Landroid/view/GestureDetector;

    .line 658
    sget v0, Lcom/android/calendar/month/k;->aA:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 659
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 660
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/android/calendar/month/k;->aA:F

    .line 662
    :cond_1
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    .line 664
    instance-of v0, p1, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 665
    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    iput-object v0, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    .line 668
    :cond_2
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 669
    const v1, 0x7f0c025c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/k;->aJ:I

    .line 670
    const v1, 0x7f0c025d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->aK:I

    .line 671
    new-instance v0, Landroid/text/format/Time;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/month/k;->ax:Landroid/text/format/Time;

    .line 673
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 634
    invoke-super {p0, p1}, Lcom/android/calendar/month/by;->onCreate(Landroid/os/Bundle;)V

    .line 635
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 637
    const v1, 0x7f0a000d

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->aH:Z

    .line 642
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/month/k;->aE:I

    .line 643
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 730
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/month/k;->aE:I

    .line 733
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-eqz v0, :cond_0

    .line 734
    const v0, 0x7f040073

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 741
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/month/k;->a(Landroid/view/View;)V

    .line 743
    const v0, 0x7f1201da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/MonthDayHeader;

    iput-object v0, p0, Lcom/android/calendar/month/k;->m:Lcom/android/calendar/month/MonthDayHeader;

    .line 745
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/k;->setHasOptionsMenu(Z)V

    .line 747
    return-object v1

    .line 736
    :cond_0
    const v0, 0x7f040058

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 738
    invoke-virtual {p0, v0}, Lcom/android/calendar/month/k;->b(Landroid/view/View;)V

    move-object v1, v0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2720
    iput-object v0, p0, Lcom/android/calendar/month/k;->h:Landroid/util/LruCache;

    .line 2721
    iput-object v0, p0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    .line 2722
    invoke-super {p0}, Lcom/android/calendar/month/by;->onDestroyView()V

    .line 2723
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 14

    .prologue
    .line 2273
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-nez v0, :cond_8

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_8

    .line 2274
    invoke-direct {p0}, Lcom/android/calendar/month/k;->C()Landroid/view/View;

    move-result-object v0

    .line 2275
    if-nez v0, :cond_0

    .line 2276
    const/4 v0, 0x0

    .line 2415
    :goto_0
    return v0

    .line 2279
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2280
    sparse-switch p2, :sswitch_data_0

    .line 2384
    const/4 v0, 0x0

    goto :goto_0

    .line 2282
    :sswitch_0
    const/4 v0, -0x7

    invoke-direct {p0, v0}, Lcom/android/calendar/month/k;->c(I)V

    .line 2283
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 2284
    const/4 v0, 0x1

    goto :goto_0

    .line 2286
    :sswitch_1
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/android/calendar/month/k;->c(I)V

    .line 2287
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 2288
    const/4 v0, 0x1

    goto :goto_0

    .line 2290
    :sswitch_2
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/month/k;->c(I)V

    .line 2291
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 2292
    const/4 v0, 0x1

    goto :goto_0

    .line 2294
    :sswitch_3
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/month/k;->c(I)V

    .line 2295
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 2296
    const/4 v0, 0x1

    goto :goto_0

    .line 2300
    :sswitch_4
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2301
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->y:I

    .line 2303
    const/4 v0, 0x1

    goto :goto_0

    .line 2306
    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2308
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->r(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v1

    .line 2309
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    const-wide/16 v2, 0x1

    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, -0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    .line 2311
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 2315
    :sswitch_5
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->year:I

    .line 2316
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v3, v0, Landroid/text/format/Time;->month:I

    .line 2320
    const/16 v0, 0xb

    if-ne v3, v0, :cond_3

    .line 2321
    const/4 v2, 0x0

    .line 2322
    add-int/lit8 v0, v1, 0x1

    .line 2328
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 2329
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v1}, Ljava/util/Calendar;->set(II)V

    .line 2330
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 2331
    const/4 v1, 0x5

    invoke-virtual {v4, v1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    .line 2333
    const/4 v3, 0x1

    invoke-virtual {v4, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 2334
    const/4 v0, 0x2

    invoke-virtual {v4, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 2335
    const/4 v0, 0x5

    invoke-virtual {v4, v0}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    .line 2338
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    if-le v2, v0, :cond_4

    .line 2339
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2343
    :goto_2
    invoke-direct {p0, v0}, Lcom/android/calendar/month/k;->c(I)V

    .line 2344
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 2345
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2324
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v2, v0

    move v0, v1

    .line 2325
    goto :goto_1

    :cond_4
    move v0, v1

    .line 2341
    goto :goto_2

    .line 2348
    :sswitch_6
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->year:I

    .line 2349
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v3, v0, Landroid/text/format/Time;->month:I

    .line 2353
    if-nez v3, :cond_5

    .line 2354
    const/16 v2, 0xb

    .line 2355
    add-int/lit8 v0, v1, -0x1

    .line 2361
    :goto_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 2362
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v1}, Ljava/util/Calendar;->set(II)V

    .line 2363
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 2364
    const/4 v1, 0x5

    invoke-virtual {v4, v1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    .line 2366
    const/4 v3, 0x1

    invoke-virtual {v4, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 2367
    const/4 v0, 0x2

    invoke-virtual {v4, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 2368
    const/4 v0, 0x5

    invoke-virtual {v4, v0}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    .line 2371
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    if-ge v2, v0, :cond_6

    .line 2372
    iget-object v1, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    neg-int v1, v1

    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    sub-int v0, v2, v0

    add-int/2addr v0, v1

    .line 2379
    :goto_4
    invoke-direct {p0, v0}, Lcom/android/calendar/month/k;->c(I)V

    .line 2380
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 2381
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2357
    :cond_5
    add-int/lit8 v0, v3, -0x1

    move v2, v0

    move v0, v1

    .line 2358
    goto :goto_3

    .line 2373
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    if-le v1, v2, :cond_7

    iget-object v2, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    if-gt v0, v2, :cond_7

    .line 2375
    iget-object v0, p0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    neg-int v0, v0

    goto :goto_4

    .line 2377
    :cond_7
    neg-int v0, v1

    goto :goto_4

    .line 2388
    :cond_8
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-nez v0, :cond_b

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    .line 2389
    sparse-switch p2, :sswitch_data_1

    .line 2412
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2392
    :sswitch_7
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v0

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/android/calendar/month/k;->G:Z

    if-nez v0, :cond_b

    .line 2393
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2394
    iget-wide v2, p0, Lcom/android/calendar/month/k;->p:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1f4

    cmp-long v0, v0, v2

    if-gez v0, :cond_9

    .line 2395
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2398
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->r(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v1

    .line 2399
    iget v0, v1, Landroid/text/format/Time;->hour:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Landroid/text/format/Time;->hour:I

    .line 2401
    invoke-direct {p0}, Lcom/android/calendar/month/k;->C()Landroid/view/View;

    move-result-object v2

    .line 2402
    if-nez v2, :cond_a

    .line 2403
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2405
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    check-cast v0, Lcom/android/calendar/month/h;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/h;->a(Landroid/text/format/Time;Landroid/view/View;)V

    .line 2407
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2415
    :cond_b
    :sswitch_8
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2280
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x3e -> :sswitch_5
        0x42 -> :sswitch_4
        0x52 -> :sswitch_8
        0x5c -> :sswitch_6
        0x5d -> :sswitch_5
    .end sparse-switch

    .line 2389
    :sswitch_data_1
    .sparse-switch
        0x17 -> :sswitch_7
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 762
    invoke-super {p0}, Lcom/android/calendar/month/by;->onPause()V

    .line 763
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->E:Z

    if-eqz v0, :cond_0

    .line 764
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->i()V

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->aT:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 767
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/k;->aT:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 769
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->b(I)V

    .line 770
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 872
    invoke-super {p0, p1}, Lcom/android/calendar/month/by;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 873
    iget-object v0, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 875
    iget-object v1, p0, Lcom/android/calendar/month/k;->aC:Lcom/android/calendar/AllInOneActivity;

    invoke-direct {p0}, Lcom/android/calendar/month/k;->B()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/calendar/AllInOneActivity;->a(Z)V

    .line 878
    :cond_0
    return-void

    .line 875
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 775
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->aI:Z

    .line 777
    const v0, 0x7f05000f

    .line 778
    const v3, 0x7f050010

    .line 780
    iget-object v4, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v4, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->f:Landroid/view/animation/Animation;

    .line 781
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/k;->g:Landroid/view/animation/Animation;

    .line 783
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->aI:Z

    if-eqz v0, :cond_8

    .line 784
    iget-object v0, p0, Lcom/android/calendar/month/k;->f:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 785
    iget-object v0, p0, Lcom/android/calendar/month/k;->g:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 791
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->m:Lcom/android/calendar/month/MonthDayHeader;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthDayHeader;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 793
    iget-boolean v3, p0, Lcom/android/calendar/month/k;->aI:Z

    if-eqz v3, :cond_9

    .line 794
    iget v3, p0, Lcom/android/calendar/month/k;->aK:I

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 799
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/month/k;->m:Lcom/android/calendar/month/MonthDayHeader;

    invoke-virtual {v3, v0}, Lcom/android/calendar/month/MonthDayHeader;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 800
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/month/k;->p:J

    .line 802
    invoke-super {p0}, Lcom/android/calendar/month/by;->onResume()V

    .line 804
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/month/k;->p:J

    .line 806
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->i()V

    .line 807
    iget-object v0, p0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->d()V

    .line 809
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v0

    .line 810
    if-ne v0, v6, :cond_0

    .line 811
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->q()V

    .line 814
    :cond_0
    sget-boolean v3, Lcom/android/calendar/month/k;->X:Z

    if-nez v3, :cond_2

    .line 815
    if-eq v0, v6, :cond_1

    iget-boolean v3, p0, Lcom/android/calendar/month/k;->aI:Z

    if-eqz v3, :cond_a

    .line 816
    :cond_1
    invoke-virtual {p0, v7}, Lcom/android/calendar/month/k;->a(I)V

    .line 817
    iget-object v3, p0, Lcom/android/calendar/month/k;->i:Landroid/view/ViewStub;

    invoke-virtual {v3, v7}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 818
    sput v6, Lcom/android/calendar/month/k;->n:I

    .line 829
    :cond_2
    :goto_2
    sget-boolean v3, Lcom/android/calendar/month/k;->X:Z

    if-nez v3, :cond_e

    .line 830
    iget-object v3, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 831
    if-eq v0, v6, :cond_3

    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->u(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 832
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setHoverScrollMode(Z)V

    .line 837
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const v3, 0x7f0b0094

    invoke-virtual {v0, v3}, Lcom/android/calendar/month/MonthListView;->setBackgroundResource(I)V

    .line 848
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->ad:I

    .line 849
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->R:Z

    .line 852
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->aI:Z

    if-eqz v0, :cond_6

    .line 853
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/k;->ad:I

    .line 854
    iput-boolean v2, p0, Lcom/android/calendar/month/k;->R:Z

    .line 857
    :cond_6
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->aI:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-nez v0, :cond_7

    sget v0, Lcom/android/calendar/month/k;->n:I

    if-ne v0, v6, :cond_7

    iget v0, p0, Lcom/android/calendar/month/k;->aE:I

    if-ne v0, v1, :cond_7

    .line 860
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 861
    if-eqz v0, :cond_7

    .line 862
    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->e(Z)V

    .line 865
    :cond_7
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 866
    const-string v1, "android.location.MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 867
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/k;->aT:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 868
    return-void

    .line 787
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/month/k;->f:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0018

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 788
    iget-object v0, p0, Lcom/android/calendar/month/k;->g:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0019

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    goto/16 :goto_0

    .line 796
    :cond_9
    iget v3, p0, Lcom/android/calendar/month/k;->aJ:I

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_1

    .line 821
    :cond_a
    iget-object v3, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->l()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->m()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 822
    :cond_b
    invoke-virtual {p0, v2}, Lcom/android/calendar/month/k;->a(I)V

    .line 823
    iget-object v3, p0, Lcom/android/calendar/month/k;->i:Landroid/view/ViewStub;

    invoke-virtual {v3, v2}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 825
    :cond_c
    sput v1, Lcom/android/calendar/month/k;->n:I

    goto/16 :goto_2

    .line 834
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthListView;->setHoverScrollMode(Z)V

    goto/16 :goto_3

    .line 840
    :cond_e
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 841
    iget-object v0, p0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 842
    iget v0, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v3, 0xa0

    if-ne v0, v3, :cond_f

    move v0, v1

    .line 843
    :goto_5
    if-eqz v0, :cond_5

    .line 844
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthListView;->setHoverScrollMode(Z)V

    goto/16 :goto_4

    :cond_f
    move v0, v2

    .line 842
    goto :goto_5
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2775
    invoke-super {p0, p1}, Lcom/android/calendar/month/by;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2776
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 1163
    if-eqz p2, :cond_1

    .line 1164
    sput-boolean v3, Lcom/android/calendar/month/k;->A:Z

    .line 1169
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    monitor-enter v1

    .line 1170
    if-eqz p2, :cond_2

    .line 1171
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/android/calendar/month/k;->ay:Z

    .line 1172
    invoke-direct {p0}, Lcom/android/calendar/month/k;->z()V

    .line 1173
    invoke-direct {p0}, Lcom/android/calendar/month/k;->A()V

    .line 1174
    iget-object v0, p0, Lcom/android/calendar/month/k;->aw:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 1180
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1181
    if-ne p2, v3, :cond_0

    .line 1182
    iput-boolean v3, p0, Lcom/android/calendar/month/k;->az:Z

    .line 1185
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->aB:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/android/calendar/month/k;->a:Z

    if-eqz v0, :cond_3

    .line 1191
    :goto_2
    return-void

    .line 1166
    :cond_1
    sput-boolean v0, Lcom/android/calendar/month/k;->A:Z

    goto :goto_0

    .line 1176
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/k;->ay:Z

    .line 1178
    iget-object v0, p0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/calendar/month/k;->aO:Ljava/lang/Runnable;

    const-wide/16 v4, 0x64

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 1180
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1189
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/k;->C:Lcom/android/calendar/month/cc;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/month/cc;->a(Landroid/widget/AbsListView;I)V

    goto :goto_2
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1316
    sget-boolean v1, Lcom/android/calendar/month/k;->r:Z

    if-nez v1, :cond_0

    .line 1346
    :goto_0
    return v0

    .line 1319
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 1320
    sparse-switch v1, :sswitch_data_0

    .line 1345
    :cond_1
    :goto_1
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/month/k;->aw:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 1346
    iget-object v0, p0, Lcom/android/calendar/month/k;->av:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 1326
    :sswitch_1
    sget-boolean v1, Lcom/android/calendar/month/h;->w:Z

    if-eqz v1, :cond_1

    .line 1327
    invoke-virtual {p0, p2}, Lcom/android/calendar/month/k;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 1335
    :sswitch_2
    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-eqz v0, :cond_2

    .line 1336
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->j()V

    .line 1339
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->i()V

    goto :goto_1

    .line 1320
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0xd4 -> :sswitch_2
        0xd5 -> :sswitch_1
    .end sparse-switch
.end method

.method public p()V
    .locals 1

    .prologue
    .line 2746
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 2747
    if-eqz v0, :cond_0

    .line 2748
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getView()Landroid/view/View;

    move-result-object v0

    .line 2749
    if-nez v0, :cond_1

    .line 2754
    :cond_0
    :goto_0
    return-void

    .line 2752
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/month/k;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method

.method q()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2960
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->x:Z

    if-eqz v0, :cond_0

    .line 2961
    iput-boolean v2, p0, Lcom/android/calendar/month/k;->x:Z

    .line 2963
    iget-object v0, p0, Lcom/android/calendar/month/k;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/month/k;->aM:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2965
    iget-object v0, p0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2967
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2968
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2970
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2972
    iget-object v0, p0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setTranslationY(F)V

    .line 2974
    :cond_0
    return-void
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 2977
    iget-boolean v0, p0, Lcom/android/calendar/month/k;->x:Z

    return v0
.end method
