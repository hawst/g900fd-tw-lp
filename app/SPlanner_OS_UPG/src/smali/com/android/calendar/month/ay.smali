.class Lcom/android/calendar/month/ay;
.super Ljava/lang/Object;
.source "MonthEventListFragment.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/av;


# direct methods
.method private constructor <init>(Lcom/android/calendar/month/av;)V
    .locals 0

    .prologue
    .line 588
    iput-object p1, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/month/av;Lcom/android/calendar/month/aw;)V
    .locals 0

    .prologue
    .line 588
    invoke-direct {p0, p1}, Lcom/android/calendar/month/ay;-><init>(Lcom/android/calendar/month/av;)V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 15

    .prologue
    .line 591
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->l(Lcom/android/calendar/month/av;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    const/4 v0, 0x0

    .line 666
    :goto_0
    return v0

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->l(Lcom/android/calendar/month/av;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v1}, Lcom/android/calendar/month/av;->n(Lcom/android/calendar/month/av;)I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 595
    const/4 v0, 0x0

    goto :goto_0

    .line 597
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->l(Lcom/android/calendar/month/av;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v1}, Lcom/android/calendar/month/av;->n(Lcom/android/calendar/month/av;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/calendar/month/ba;

    .line 601
    invoke-static {}, Lcom/android/calendar/month/av;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 602
    const/4 v0, 0x0

    goto :goto_0

    .line 604
    :cond_2
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 666
    const/4 v0, 0x1

    goto :goto_0

    .line 606
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/ba;Z)V

    .line 607
    const/4 v0, 0x1

    goto :goto_0

    .line 610
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/ba;Z)V

    .line 611
    const/4 v0, 0x1

    goto :goto_0

    .line 614
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-virtual {v0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 616
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 617
    iget-boolean v2, v1, Lcom/android/calendar/month/ba;->l:Z

    if-nez v2, :cond_3

    .line 618
    iput-object v0, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 620
    :cond_3
    iget-wide v2, v1, Lcom/android/calendar/month/ba;->h:J

    invoke-virtual {v4, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 621
    iget-boolean v2, v1, Lcom/android/calendar/month/ba;->l:Z

    if-eqz v2, :cond_4

    .line 622
    iput-object v0, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 624
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 626
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 627
    iget-boolean v2, v1, Lcom/android/calendar/month/ba;->l:Z

    if-nez v2, :cond_5

    .line 628
    iput-object v0, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 630
    :cond_5
    iget-wide v2, v1, Lcom/android/calendar/month/ba;->i:J

    invoke-virtual {v5, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 631
    iget-boolean v2, v1, Lcom/android/calendar/month/ba;->l:Z

    if-eqz v2, :cond_6

    .line 632
    iput-object v0, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 634
    :cond_6
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 636
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->o(Lcom/android/calendar/month/av;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x10

    const/4 v6, 0x0

    iget-wide v7, v1, Lcom/android/calendar/month/ba;->f:J

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v14}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    .line 638
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 641
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v0, v1}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/av;Lcom/android/calendar/month/ba;)V

    .line 642
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 645
    :pswitch_4
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/ba;Z)V

    .line 646
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 649
    :pswitch_5
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/ba;Z)V

    .line 650
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 653
    :pswitch_6
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->o(Lcom/android/calendar/month/av;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x10

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-wide v6, v1, Lcom/android/calendar/month/ba;->f:J

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v9}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    .line 657
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 660
    :pswitch_7
    iget-object v0, p0, Lcom/android/calendar/month/ay;->a:Lcom/android/calendar/month/av;

    invoke-static {v0, v1}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/av;Lcom/android/calendar/month/ba;)V

    .line 661
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 604
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
