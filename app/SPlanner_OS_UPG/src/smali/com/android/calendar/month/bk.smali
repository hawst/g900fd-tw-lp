.class Lcom/android/calendar/month/bk;
.super Ljava/lang/Object;
.source "MonthHoverEventFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:J

.field final synthetic b:J

.field final synthetic c:J

.field final synthetic d:Lcom/android/calendar/month/bh;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bh;JJJ)V
    .locals 0

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/android/calendar/month/bk;->d:Lcom/android/calendar/month/bh;

    iput-wide p2, p0, Lcom/android/calendar/month/bk;->a:J

    iput-wide p4, p0, Lcom/android/calendar/month/bk;->b:J

    iput-wide p6, p0, Lcom/android/calendar/month/bk;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 18

    .prologue
    .line 1009
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bk;->d:Lcom/android/calendar/month/bh;

    iget-object v2, v2, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v2}, Lcom/android/calendar/month/bb;->o(Lcom/android/calendar/month/bb;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bk;->d:Lcom/android/calendar/month/bh;

    iget-object v2, v2, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v2}, Lcom/android/calendar/month/bb;->p(Lcom/android/calendar/month/bb;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1010
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bk;->d:Lcom/android/calendar/month/bh;

    iget-object v2, v2, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v2}, Lcom/android/calendar/month/bb;->q(Lcom/android/calendar/month/bb;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1011
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 1012
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/bk;->d:Lcom/android/calendar/month/bh;

    iget-object v3, v3, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v3}, Lcom/android/calendar/month/bb;->q(Lcom/android/calendar/month/bb;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1013
    invoke-static {v2}, Lcom/android/calendar/month/h;->c(Landroid/text/format/Time;)V

    .line 1020
    :cond_0
    :goto_0
    return-void

    .line 1016
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bk;->d:Lcom/android/calendar/month/bh;

    iget-object v2, v2, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-virtual {v2}, Lcom/android/calendar/month/bb;->dismissAllowingStateLoss()V

    .line 1017
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bk;->d:Lcom/android/calendar/month/bh;

    iget-object v2, v2, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v2}, Lcom/android/calendar/month/bb;->h(Lcom/android/calendar/month/bb;)Lcom/android/calendar/al;

    move-result-object v2

    const-wide/16 v4, 0x2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/month/bk;->a:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/month/bk;->b:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/calendar/month/bk;->c:J

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_0
.end method
