.class public abstract Lcom/android/calendar/month/by;
.super Landroid/app/Fragment;
.source "SimpleDayPickerFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# static fields
.field protected static I:I

.field protected static O:I

.field protected static P:I

.field protected static X:Z

.field private static a:Z

.field protected static ah:I

.field protected static ai:I

.field protected static ak:Z

.field public static ao:Z

.field private static b:F


# instance fields
.field protected J:I

.field protected K:I

.field protected L:I

.field protected M:I

.field protected N:I

.field protected Q:I

.field protected R:Z

.field protected S:I

.field protected T:F

.field protected U:F

.field protected V:Landroid/content/Context;

.field protected W:Landroid/os/Handler;

.field protected Y:F

.field protected Z:Landroid/text/format/Time;

.field protected aa:Lcom/android/calendar/month/ce;

.field protected ab:Lcom/android/calendar/month/MonthListView;

.field protected ac:Landroid/text/format/Time;

.field protected ad:I

.field protected ae:Landroid/text/format/Time;

.field protected af:Landroid/text/format/Time;

.field protected ag:Landroid/widget/TextView;

.field protected aj:J

.field protected al:I

.field protected am:I

.field protected an:Lcom/android/calendar/al;

.field protected ap:Ljava/lang/Runnable;

.field protected aq:Landroid/database/DataSetObserver;

.field protected ar:Lcom/android/calendar/month/cc;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x40

    const/4 v1, 0x0

    .line 65
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/by;->a:Z

    .line 89
    sput v1, Lcom/android/calendar/month/by;->I:I

    .line 101
    sput v2, Lcom/android/calendar/month/by;->O:I

    .line 103
    sput v2, Lcom/android/calendar/month/by;->P:I

    .line 135
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/month/by;->b:F

    .line 162
    sput-boolean v1, Lcom/android/calendar/month/by;->ak:Z

    .line 173
    sput-boolean v1, Lcom/android/calendar/month/by;->ao:Z

    return-void
.end method

.method public constructor <init>(J)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 215
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 91
    const/16 v0, 0xc

    iput v0, p0, Lcom/android/calendar/month/by;->J:I

    .line 93
    const/16 v0, 0x14

    iput v0, p0, Lcom/android/calendar/month/by;->K:I

    .line 95
    iput v4, p0, Lcom/android/calendar/month/by;->L:I

    .line 97
    iput v4, p0, Lcom/android/calendar/month/by;->M:I

    .line 99
    iput v4, p0, Lcom/android/calendar/month/by;->N:I

    .line 106
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/calendar/month/by;->Q:I

    .line 108
    iput-boolean v4, p0, Lcom/android/calendar/month/by;->R:Z

    .line 110
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/month/by;->S:I

    .line 113
    const v0, 0x3d4ccccd    # 0.05f

    iput v0, p0, Lcom/android/calendar/month/by;->T:F

    .line 115
    const v0, 0x3eaa7efa    # 0.333f

    iput v0, p0, Lcom/android/calendar/month/by;->U:F

    .line 126
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    .line 133
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    .line 141
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/by;->ae:Landroid/text/format/Time;

    .line 144
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/by;->af:Landroid/text/format/Time;

    .line 166
    iput v4, p0, Lcom/android/calendar/month/by;->al:I

    .line 169
    iput v4, p0, Lcom/android/calendar/month/by;->am:I

    .line 177
    new-instance v0, Lcom/android/calendar/month/bz;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/bz;-><init>(Lcom/android/calendar/month/by;)V

    iput-object v0, p0, Lcom/android/calendar/month/by;->ap:Ljava/lang/Runnable;

    .line 205
    new-instance v0, Lcom/android/calendar/month/ca;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/ca;-><init>(Lcom/android/calendar/month/by;)V

    iput-object v0, p0, Lcom/android/calendar/month/by;->aq:Landroid/database/DataSetObserver;

    .line 708
    new-instance v0, Lcom/android/calendar/month/cc;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/cc;-><init>(Lcom/android/calendar/month/by;)V

    iput-object v0, p0, Lcom/android/calendar/month/by;->ar:Lcom/android/calendar/month/cc;

    move-object v1, p0

    move-wide v2, p1

    move v6, v5

    .line 216
    invoke-virtual/range {v1 .. v6}, Lcom/android/calendar/month/by;->a(JZZZ)Z

    .line 217
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/by;->W:Landroid/os/Handler;

    .line 218
    return-void
.end method

.method private a(Landroid/widget/AbsListView;)V
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 617
    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/cd;

    .line 618
    if-nez v0, :cond_1

    .line 666
    :cond_0
    :goto_0
    return-void

    .line 623
    :cond_1
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getBottom()I

    move-result v0

    iget v3, p0, Lcom/android/calendar/month/by;->J:I

    if-ge v0, v3, :cond_2

    move v0, v1

    .line 627
    :goto_1
    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/cd;

    .line 629
    if-eqz v0, :cond_0

    .line 635
    sget-boolean v3, Lcom/android/calendar/month/by;->ak:Z

    if-eqz v3, :cond_3

    .line 636
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getFirstMonth()I

    move-result v3

    .line 643
    :goto_2
    sget v4, Lcom/android/calendar/month/by;->ah:I

    if-ne v4, v5, :cond_4

    if-nez v3, :cond_4

    .line 653
    :goto_3
    if-eqz v1, :cond_0

    .line 654
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v0

    .line 655
    sget-boolean v1, Lcom/android/calendar/month/by;->ak:Z

    if-eqz v1, :cond_6

    .line 662
    :goto_4
    iget-object v1, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 664
    iget-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    invoke-virtual {p0, v0, v2}, Lcom/android/calendar/month/by;->a(Landroid/text/format/Time;Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 623
    goto :goto_1

    .line 638
    :cond_3
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getLastMonth()I

    move-result v3

    goto :goto_2

    .line 645
    :cond_4
    sget v1, Lcom/android/calendar/month/by;->ah:I

    if-nez v1, :cond_5

    if-ne v3, v5, :cond_5

    .line 646
    const/4 v1, -0x1

    goto :goto_3

    .line 648
    :cond_5
    sget v1, Lcom/android/calendar/month/by;->ah:I

    sub-int v1, v3, v1

    goto :goto_3

    .line 659
    :cond_6
    add-int/lit8 v0, v0, 0x7

    goto :goto_4
.end method

.method public static v()V
    .locals 1

    .prologue
    .line 697
    sget-boolean v0, Lcom/android/calendar/month/by;->ak:Z

    if-eqz v0, :cond_0

    .line 698
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/by;->a:Z

    .line 702
    :goto_0
    return-void

    .line 700
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/by;->a:Z

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/text/format/Time;Z)V
    .locals 3

    .prologue
    .line 675
    iget-object v0, p0, Lcom/android/calendar/month/by;->ag:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 676
    iget-object v1, p0, Lcom/android/calendar/month/by;->ag:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/month/by;->V:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 677
    iget-object v1, p0, Lcom/android/calendar/month/by;->ag:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 678
    iget-object v1, p0, Lcom/android/calendar/month/by;->ag:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/android/calendar/month/by;->ag:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    .line 681
    :cond_0
    iget v0, p1, Landroid/text/format/Time;->month:I

    sput v0, Lcom/android/calendar/month/by;->ah:I

    .line 682
    iget v0, p1, Landroid/text/format/Time;->year:I

    sput v0, Lcom/android/calendar/month/by;->ai:I

    .line 683
    if-eqz p2, :cond_1

    .line 684
    iget-object v0, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    sget v1, Lcom/android/calendar/month/by;->ah:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->c(I)V

    .line 686
    :cond_1
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 319
    const v0, 0x7f1201dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/MonthListView;

    iput-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    .line 321
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    iget-object v1, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 322
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthListView;->setCacheColorHint(I)V

    .line 324
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 326
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setItemsCanFocus(Z)V

    .line 328
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthListView;->setFastScrollEnabled(Z)V

    .line 329
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthListView;->setVerticalScrollBarEnabled(Z)V

    .line 330
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, p0}, Lcom/android/calendar/month/MonthListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 331
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthListView;->setFadingEdgeLength(I)V

    .line 333
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    iget v1, p0, Lcom/android/calendar/month/by;->T:F

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setFriction(F)V

    .line 334
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    iget v1, p0, Lcom/android/calendar/month/by;->U:F

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setVelocityScale(F)V

    .line 335
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->setImportantForAccessibility(I)V

    .line 337
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->t()V

    .line 338
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->u()V

    .line 340
    return-void
.end method

.method public a(JZZZ)Z
    .locals 11

    .prologue
    const/4 v9, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 470
    const-wide/16 v2, -0x1

    cmp-long v0, p1, v2

    if-nez v0, :cond_1

    .line 471
    const-string v0, "MonthFragment"

    const-string v2, "time is invalid"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    :cond_0
    :goto_0
    return v1

    .line 476
    :cond_1
    if-eqz p4, :cond_2

    .line 477
    iget-object v0, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 478
    iget-object v0, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 483
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->isResumed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 484
    const-string v0, "MonthFragment"

    invoke-static {v0, v9}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 485
    const-string v0, "MonthFragment"

    const-string v2, "We\'re not visible yet"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 490
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 491
    iget-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 494
    iget-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iget v2, p0, Lcom/android/calendar/month/by;->ad:I

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(II)I

    move-result v5

    move v0, v1

    move v2, v1

    .line 502
    :goto_1
    iget-object v6, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v6, v2}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 503
    if-nez v2, :cond_8

    move v3, v0

    .line 514
    :goto_2
    if-eqz v2, :cond_a

    .line 515
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 519
    :goto_3
    iget v2, p0, Lcom/android/calendar/month/by;->Q:I

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    .line 520
    iget v6, p0, Lcom/android/calendar/month/by;->K:I

    if-le v3, v6, :cond_4

    .line 521
    add-int/lit8 v2, v2, -0x1

    .line 524
    :cond_4
    if-eqz p4, :cond_5

    .line 525
    iget-object v3, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    iget-object v6, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {v3, v6}, Lcom/android/calendar/month/ce;->a(Landroid/text/format/Time;)V

    .line 528
    :cond_5
    const-string v3, "MonthFragment"

    invoke-static {v3, v9}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 529
    const-string v3, "MonthFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GoTo position "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    :cond_6
    if-lt v5, v0, :cond_7

    if-gt v5, v2, :cond_7

    if-eqz p5, :cond_c

    .line 534
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/month/by;->ae:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 535
    iget-object v0, p0, Lcom/android/calendar/month/by;->ae:Landroid/text/format/Time;

    iput v4, v0, Landroid/text/format/Time;->monthDay:I

    .line 536
    iget-object v0, p0, Lcom/android/calendar/month/by;->ae:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 537
    iget-object v0, p0, Lcom/android/calendar/month/by;->ae:Landroid/text/format/Time;

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iget v2, p0, Lcom/android/calendar/month/by;->ad:I

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(II)I

    move-result v0

    .line 540
    const/4 v2, 0x2

    iput v2, p0, Lcom/android/calendar/month/by;->al:I

    .line 541
    if-eqz p3, :cond_b

    .line 542
    iget-object v1, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    sget v2, Lcom/android/calendar/month/by;->I:I

    const/16 v3, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/calendar/month/MonthListView;->smoothScrollToPositionFromTop(III)V

    move v1, v4

    .line 543
    goto/16 :goto_0

    .line 506
    :cond_8
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    .line 507
    const-string v6, "MonthFragment"

    invoke-static {v6, v9}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 508
    const-string v6, "MonthFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "child at "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v8, v3, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has top "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    :cond_9
    if-ltz v0, :cond_d

    move v3, v0

    goto/16 :goto_2

    :cond_a
    move v0, v1

    .line 517
    goto/16 :goto_3

    .line 545
    :cond_b
    iget-object v2, p0, Lcom/android/calendar/month/by;->ae:Landroid/text/format/Time;

    invoke-virtual {p0, v2, v4}, Lcom/android/calendar/month/by;->a(Landroid/text/format/Time;Z)V

    .line 546
    iget-object v2, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    sget v3, Lcom/android/calendar/month/by;->I:I

    invoke-virtual {v2, v0, v3}, Lcom/android/calendar/month/MonthListView;->setSelectionFromTop(II)V

    .line 548
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/by;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto/16 :goto_0

    .line 550
    :cond_c
    if-eqz p4, :cond_0

    .line 552
    iget-object v0, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {p0, v0, v4}, Lcom/android/calendar/month/by;->a(Landroid/text/format/Time;Z)V

    goto/16 :goto_0

    :cond_d
    move v2, v3

    goto/16 :goto_1
.end method

.method public abstract a(JZZZZ)Z
.end method

.method protected b()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 260
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 261
    const-string v0, "num_weeks"

    iget v3, p0, Lcom/android/calendar/month/by;->Q:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    const-string v3, "week_numbers"

    iget-boolean v0, p0, Lcom/android/calendar/month/by;->R:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    const-string v0, "week_start"

    iget v3, p0, Lcom/android/calendar/month/by;->ad:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    const-string v0, "selected_day"

    iget-object v3, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {v3, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-object v1, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    iget-wide v6, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    iget-object v0, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    if-nez v0, :cond_1

    .line 267
    new-instance v0, Lcom/android/calendar/month/ce;

    invoke-virtual {p0}, Lcom/android/calendar/month/by;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/ce;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    .line 268
    iget-object v0, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    iget-object v1, p0, Lcom/android/calendar/month/by;->aq:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 273
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->notifyDataSetChanged()V

    .line 274
    return-void

    :cond_0
    move v0, v1

    .line 262
    goto :goto_0

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/ce;->a(Ljava/util/HashMap;)V

    goto :goto_1
.end method

.method protected c()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 413
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 414
    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/month/by;->ad:I

    .line 416
    iput-boolean v4, p0, Lcom/android/calendar/month/by;->R:Z

    .line 418
    iget-object v0, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    move-object v1, p0

    move v5, v4

    move v6, v4

    invoke-virtual/range {v1 .. v6}, Lcom/android/calendar/month/by;->a(JZZZ)Z

    .line 419
    iget-object v0, p0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    iget-object v1, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->a(Landroid/text/format/Time;)V

    .line 420
    iget-object v0, p0, Lcom/android/calendar/month/by;->ap:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 421
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 287
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 289
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1201d9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/month/by;->ag:Landroid/widget/TextView;

    .line 290
    iget-object v0, p0, Lcom/android/calendar/month/by;->ag:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/android/calendar/month/by;->ag:Landroid/widget/TextView;

    new-instance v1, Lcom/android/calendar/month/cb;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/cb;-><init>(Lcom/android/calendar/month/by;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/cd;

    .line 304
    if-nez v0, :cond_1

    .line 312
    :goto_0
    return-void

    .line 307
    :cond_1
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v0

    .line 308
    iget-object v1, p0, Lcom/android/calendar/month/by;->af:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 310
    iget-object v1, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    add-int/lit8 v0, v0, 0x7

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 311
    iget-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/by;->a(Landroid/text/format/Time;Z)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 222
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 223
    iput-object p1, p0, Lcom/android/calendar/month/by;->V:Landroid/content/Context;

    .line 224
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/by;->an:Lcom/android/calendar/al;

    .line 226
    iget-object v0, p0, Lcom/android/calendar/month/by;->V:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 227
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 228
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/calendar/month/by;->Y:F

    .line 231
    iget-object v1, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 232
    iget-object v1, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 233
    iget-object v1, p0, Lcom/android/calendar/month/by;->ae:Landroid/text/format/Time;

    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 234
    iget-object v1, p0, Lcom/android/calendar/month/by;->ae:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 235
    iget-object v1, p0, Lcom/android/calendar/month/by;->af:Landroid/text/format/Time;

    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 236
    iget-object v1, p0, Lcom/android/calendar/month/by;->af:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 237
    iget-object v1, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 239
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 240
    const v1, 0x7f0b00ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/by;->L:I

    .line 241
    const v1, 0x7f0b00e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/by;->M:I

    .line 242
    const v1, 0x7f0b0123

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/by;->N:I

    .line 245
    sget v0, Lcom/android/calendar/month/by;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 246
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/android/calendar/month/by;->b:F

    .line 247
    sget v0, Lcom/android/calendar/month/by;->b:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 248
    iget v0, p0, Lcom/android/calendar/month/by;->J:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/by;->b:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/month/by;->J:I

    .line 249
    iget v0, p0, Lcom/android/calendar/month/by;->K:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/by;->b:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/month/by;->K:I

    .line 250
    sget v0, Lcom/android/calendar/month/by;->I:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/by;->b:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/by;->I:I

    .line 253
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->b()V

    .line 254
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 278
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 279
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/month/by;->X:Z

    .line 280
    if-eqz p1, :cond_0

    const-string v0, "current_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    const-string v0, "current_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x0

    move-object v1, p0

    move v6, v5

    invoke-virtual/range {v1 .. v6}, Lcom/android/calendar/month/by;->a(JZZZ)Z

    .line 283
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 443
    const v0, 0x7f040073

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 445
    invoke-virtual {p0, v0}, Lcom/android/calendar/month/by;->a(Landroid/view/View;)V

    .line 446
    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 397
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 398
    iget-object v0, p0, Lcom/android/calendar/month/by;->W:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/by;->ap:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 399
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 390
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 391
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->c()V

    .line 392
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->b()V

    .line 393
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 403
    const-string v0, "current_time"

    iget-object v1, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 404
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 405
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 564
    invoke-virtual {p1, v4}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/cd;

    .line 565
    if-nez v0, :cond_1

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 570
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 576
    iget v1, v1, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_2

    .line 577
    iget-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 578
    iget-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/by;->ad:I

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(II)I

    move-result v0

    .line 580
    iget-object v1, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    sget v2, Lcom/android/calendar/month/by;->I:I

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/month/MonthListView;->setSelectionFromTop(II)V

    .line 581
    iget-object v0, p0, Lcom/android/calendar/month/by;->ac:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    move-object v1, p0

    move v6, v4

    move v7, v5

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/month/by;->a(JZZZZ)Z

    .line 582
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0, v5}, Lcom/android/calendar/month/MonthListView;->setEnabled(Z)V

    goto :goto_0

    .line 587
    :cond_2
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getHeight()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getBottom()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-long v2, v1

    .line 588
    iget-object v1, p0, Lcom/android/calendar/month/by;->af:Landroid/text/format/Time;

    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 591
    iget-wide v0, p0, Lcom/android/calendar/month/by;->aj:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_3

    .line 592
    sput-boolean v5, Lcom/android/calendar/month/by;->ak:Z

    .line 599
    :goto_1
    iput-wide v2, p0, Lcom/android/calendar/month/by;->aj:J

    .line 600
    iget v0, p0, Lcom/android/calendar/month/by;->am:I

    iput v0, p0, Lcom/android/calendar/month/by;->al:I

    .line 602
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-direct {p0, v0}, Lcom/android/calendar/month/by;->a(Landroid/widget/AbsListView;)V

    goto :goto_0

    .line 593
    :cond_3
    iget-wide v0, p0, Lcom/android/calendar/month/by;->aj:J

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 594
    sput-boolean v4, Lcom/android/calendar/month/by;->ak:Z

    goto :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/android/calendar/month/by;->ar:Lcom/android/calendar/month/cc;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/month/cc;->a(Landroid/widget/AbsListView;I)V

    .line 693
    return-void
.end method

.method t()V
    .locals 5

    .prologue
    .line 349
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 353
    :try_start_0
    const-string v1, "setMotionEvent"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 354
    if-eqz v0, :cond_0

    .line 355
    iget-object v1, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 357
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method u()V
    .locals 6

    .prologue
    .line 367
    iget-object v0, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 370
    sget-boolean v0, Lcom/android/calendar/month/by;->X:Z

    if-eqz v0, :cond_1

    .line 371
    sget v0, Lcom/android/calendar/month/by;->O:I

    .line 377
    :goto_0
    :try_start_0
    const-string v2, "setTouchSlop"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 378
    if-eqz v1, :cond_0

    .line 379
    iget-object v2, p0, Lcom/android/calendar/month/by;->ab:Lcom/android/calendar/month/MonthListView;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 385
    :cond_0
    :goto_1
    return-void

    .line 373
    :cond_1
    sget v0, Lcom/android/calendar/month/by;->P:I

    goto :goto_0

    .line 381
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected w()V
    .locals 6

    .prologue
    const v5, 0x7f120076

    .line 771
    const/4 v1, 0x0

    .line 772
    const/4 v0, 0x0

    .line 773
    if-nez v1, :cond_0

    .line 774
    const/4 v0, 0x1

    .line 775
    invoke-virtual {p0}, Lcom/android/calendar/month/by;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 778
    :cond_0
    new-instance v2, Lcom/android/calendar/ez;

    iget-object v3, p0, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    invoke-direct {v2, v3}, Lcom/android/calendar/ez;-><init>(Landroid/text/format/Time;)V

    .line 779
    iget-object v3, p0, Lcom/android/calendar/month/by;->an:Lcom/android/calendar/al;

    invoke-virtual {v3, v5, v2}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 780
    const v3, 0x7f060002

    const v4, 0x7f060003

    invoke-virtual {v1, v3, v4}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 781
    invoke-virtual {v1, v5, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 782
    if-eqz v0, :cond_1

    .line 784
    :try_start_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 789
    :cond_1
    :goto_0
    return-void

    .line 785
    :catch_0
    move-exception v0

    goto :goto_0
.end method
