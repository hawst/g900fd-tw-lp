.class Lcom/android/calendar/month/f;
.super Ljava/lang/Object;
.source "EventInfoListFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/e;

.field private final b:J

.field private final c:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Lcom/android/calendar/month/e;JLandroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 746
    iput-object p1, p0, Lcom/android/calendar/month/f;->a:Lcom/android/calendar/month/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 747
    iput-wide p2, p0, Lcom/android/calendar/month/f;->b:J

    .line 748
    iput-object p4, p0, Lcom/android/calendar/month/f;->c:Landroid/widget/TextView;

    .line 749
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/month/e;JLandroid/widget/TextView;Lcom/android/calendar/month/b;)V
    .locals 0

    .prologue
    .line 742
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/month/f;-><init>(Lcom/android/calendar/month/e;JLandroid/widget/TextView;)V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    .line 753
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 754
    iget-object v0, p0, Lcom/android/calendar/month/f;->a:Lcom/android/calendar/month/e;

    iget-wide v2, p0, Lcom/android/calendar/month/f;->b:J

    invoke-virtual {v0, p2, v2, v3}, Lcom/android/calendar/month/e;->a(ZJ)V

    .line 755
    iget-object v0, p0, Lcom/android/calendar/month/f;->a:Lcom/android/calendar/month/e;

    iget-object v1, p0, Lcom/android/calendar/month/f;->c:Landroid/widget/TextView;

    invoke-static {v0, p2, v1}, Lcom/android/calendar/month/e;->a(Lcom/android/calendar/month/e;ZLandroid/widget/TextView;)V

    .line 756
    return-void
.end method
