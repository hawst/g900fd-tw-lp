.class Lcom/android/calendar/month/bh;
.super Landroid/widget/BaseAdapter;
.source "MonthHoverEventFragment.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/bb;

.field private b:Ljava/util/ArrayList;

.field private c:Ljava/util/ArrayList;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/calendar/month/bb;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 749
    iput-object p1, p0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 743
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bh;->b:Ljava/util/ArrayList;

    .line 745
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bh;->c:Ljava/util/ArrayList;

    .line 750
    iput-object p3, p0, Lcom/android/calendar/month/bh;->b:Ljava/util/ArrayList;

    .line 751
    iput-object p4, p0, Lcom/android/calendar/month/bh;->c:Ljava/util/ArrayList;

    .line 752
    iput-object p2, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    .line 753
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1130
    const-string v0, "%H:%M"

    .line 1132
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%-l:%M %^p"

    goto :goto_0
.end method


# virtual methods
.method public a(ILandroid/view/View;Lcom/android/calendar/month/bn;)Landroid/view/View;
    .locals 11

    .prologue
    .line 854
    iget-object v0, p3, Lcom/android/calendar/month/bn;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 855
    iget-object v0, p3, Lcom/android/calendar/month/bn;->g:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 857
    iget-object v2, p3, Lcom/android/calendar/month/bn;->h:Landroid/view/View;

    .line 858
    iget-object v3, p3, Lcom/android/calendar/month/bn;->i:Landroid/widget/CheckBox;

    .line 859
    iget-object v4, p3, Lcom/android/calendar/month/bn;->j:Landroid/widget/TextView;

    .line 860
    iget-object v5, p3, Lcom/android/calendar/month/bn;->k:Landroid/widget/TextView;

    .line 861
    iget-object v6, p3, Lcom/android/calendar/month/bn;->l:Landroid/widget/ImageView;

    .line 862
    iget-object v7, p3, Lcom/android/calendar/month/bn;->m:Landroid/widget/ImageView;

    .line 866
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/aa;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 871
    :goto_0
    if-eqz v1, :cond_2

    .line 872
    const v0, 0x7f1201f0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 873
    iget-wide v8, v1, Lcom/android/calendar/task/aa;->b:J

    .line 874
    new-instance v10, Lcom/android/calendar/month/bi;

    invoke-direct {v10, p0, v8, v9}, Lcom/android/calendar/month/bi;-><init>(Lcom/android/calendar/month/bh;J)V

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 890
    new-instance v8, Lcom/android/calendar/month/bj;

    invoke-direct {v8, p0}, Lcom/android/calendar/month/bj;-><init>(Lcom/android/calendar/month/bh;)V

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 927
    iget-object v0, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    iget v8, v1, Lcom/android/calendar/task/aa;->i:I

    invoke-static {v0, v8}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 928
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 930
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 932
    iget-boolean v0, v1, Lcom/android/calendar/task/aa;->g:Z

    if-eqz v0, :cond_3

    .line 933
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 934
    iget-object v0, v1, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 935
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x10

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 936
    const/high16 v0, 0x7f000000

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 937
    iget-object v0, v1, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 938
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 939
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 952
    :cond_0
    :goto_1
    const-string v0, ""

    .line 953
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 954
    iget-wide v2, v1, Lcom/android/calendar/task/aa;->d:J

    const-wide/16 v8, -0x1

    cmp-long v0, v2, v8

    if-nez v0, :cond_4

    .line 955
    iget-object v0, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f02d1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 959
    :goto_2
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 961
    if-eqz v6, :cond_1

    .line 962
    iget v0, v1, Lcom/android/calendar/task/aa;->h:I

    if-nez v0, :cond_5

    .line 963
    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 969
    :cond_1
    :goto_3
    if-eqz v7, :cond_2

    .line 970
    iget v0, v1, Lcom/android/calendar/task/aa;->f:I

    if-nez v0, :cond_6

    .line 971
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 972
    const v0, 0x7f02013c

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 981
    :cond_2
    :goto_4
    return-object p2

    .line 867
    :catch_0
    move-exception v0

    .line 868
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_0

    .line 942
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 943
    iget-object v0, v1, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 944
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    and-int/lit8 v0, v0, -0x11

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 945
    const/high16 v0, -0x1000000

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 946
    iget-object v0, v1, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 947
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 948
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 957
    :cond_4
    iget-wide v2, v1, Lcom/android/calendar/task/aa;->d:J

    iget-object v0, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    const/4 v4, 0x2

    invoke-static {v2, v3, v0, v4}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 965
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 973
    :cond_6
    iget v0, v1, Lcom/android/calendar/task/aa;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 974
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 975
    const v0, 0x7f02013b

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4

    .line 977
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4
.end method

.method public a(JJZ)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 1113
    const-string v0, ""

    .line 1114
    iget-object v1, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    .line 1115
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1116
    if-eqz p5, :cond_1

    .line 1117
    iget-object v0, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1125
    :cond_0
    :goto_0
    return-object v0

    .line 1118
    :cond_1
    cmp-long v1, p1, v4

    if-eqz v1, :cond_0

    cmp-long v1, p3, v4

    if-eqz v1, :cond_0

    .line 1119
    invoke-virtual {v2, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 1120
    invoke-direct {p0}, Lcom/android/calendar/month/bh;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1121
    invoke-virtual {v2, p3, p4}, Landroid/text/format/Time;->set(J)V

    .line 1122
    invoke-direct {p0}, Lcom/android/calendar/month/bh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1123
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ~ "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 756
    invoke-virtual {p0}, Lcom/android/calendar/month/bh;->getCount()I

    move-result v2

    .line 757
    if-ge v2, v1, :cond_1

    .line 764
    :cond_0
    :goto_0
    return v0

    .line 759
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/month/bh;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt p1, v3, :cond_0

    .line 761
    if-ge p1, v2, :cond_0

    move v0, v1

    .line 762
    goto :goto_0
.end method

.method public b(ILandroid/view/View;Lcom/android/calendar/month/bn;)Landroid/view/View;
    .locals 17

    .prologue
    .line 985
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/month/bn;->a:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 986
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/month/bn;->g:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 987
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/android/calendar/month/bn;->b:Landroid/view/View;

    .line 988
    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/android/calendar/month/bn;->c:Landroid/widget/TextView;

    .line 989
    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/android/calendar/month/bn;->d:Landroid/widget/ImageView;

    .line 990
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/calendar/month/bn;->e:Landroid/widget/TextView;

    .line 991
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/calendar/month/bn;->f:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    .line 996
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/month/bh;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v11, v2

    .line 1001
    :goto_0
    if-eqz v11, :cond_3

    .line 1002
    const v2, 0x7f1201e9

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Landroid/widget/LinearLayout;

    .line 1003
    iget-wide v4, v11, Lcom/android/calendar/dh;->b:J

    .line 1004
    iget-wide v6, v11, Lcom/android/calendar/dh;->m:J

    .line 1005
    iget-wide v8, v11, Lcom/android/calendar/dh;->n:J

    .line 1006
    new-instance v2, Lcom/android/calendar/month/bk;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/android/calendar/month/bk;-><init>(Lcom/android/calendar/month/bh;JJJ)V

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1023
    new-instance v2, Lcom/android/calendar/month/bl;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/calendar/month/bl;-><init>(Lcom/android/calendar/month/bh;)V

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1059
    iget v2, v11, Lcom/android/calendar/dh;->c:I

    invoke-virtual {v12, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1060
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1062
    iget-object v2, v11, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1063
    iget-object v2, v11, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1064
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 1065
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1068
    :cond_0
    if-eqz v14, :cond_1

    .line 1069
    invoke-virtual {v11}, Lcom/android/calendar/dh;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v2}, Lcom/android/calendar/month/bb;->p(Lcom/android/calendar/month/bb;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1070
    iget-object v2, v11, Lcom/android/calendar/dh;->A:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1071
    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1077
    :cond_1
    :goto_1
    iget-wide v4, v11, Lcom/android/calendar/dh;->m:J

    iget-wide v6, v11, Lcom/android/calendar/dh;->n:J

    iget-boolean v8, v11, Lcom/android/calendar/dh;->f:Z

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/android/calendar/month/bh;->a(JJZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1078
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1080
    iget-boolean v2, v11, Lcom/android/calendar/dh;->o:Z

    if-eqz v2, :cond_2

    invoke-virtual {v11}, Lcom/android/calendar/dh;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1081
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0209

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1082
    invoke-virtual {v15}, Landroid/widget/TextView;->setSingleLine()V

    .line 1083
    int-to-float v2, v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1086
    :cond_2
    if-eqz v16, :cond_3

    .line 1087
    iget-boolean v2, v11, Lcom/android/calendar/dh;->o:Z

    if-eqz v2, :cond_5

    .line 1088
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1103
    :cond_3
    :goto_2
    return-object p2

    .line 997
    :catch_0
    move-exception v2

    .line 998
    const/4 v2, 0x0

    move-object v11, v2

    goto/16 :goto_0

    .line 1073
    :cond_4
    const/16 v2, 0x8

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1090
    :cond_5
    const/16 v2, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 769
    const/4 v0, 0x0

    .line 770
    iget-object v1, p0, Lcom/android/calendar/month/bh;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 771
    iget-object v1, p0, Lcom/android/calendar/month/bh;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 773
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/bh;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 774
    iget-object v1, p0, Lcom/android/calendar/month/bh;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 776
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->m(Lcom/android/calendar/month/bb;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->n(Lcom/android/calendar/month/bb;)I

    move-result v1

    if-le v0, v1, :cond_2

    .line 778
    iget-object v0, p0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->n(Lcom/android/calendar/month/bb;)I

    move-result v0

    .line 780
    :cond_2
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 785
    invoke-virtual {p0}, Lcom/android/calendar/month/bh;->getCount()I

    move-result v1

    .line 786
    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-object v0

    .line 789
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v2}, Lcom/android/calendar/month/bb;->m(Lcom/android/calendar/month/bb;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v2}, Lcom/android/calendar/month/bb;->n(Lcom/android/calendar/month/bb;)I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 793
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/month/bh;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p1, v2, :cond_3

    .line 794
    iget-object v0, p0, Lcom/android/calendar/month/bh;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 796
    :cond_3
    if-ge p1, v1, :cond_0

    .line 797
    iget-object v0, p0, Lcom/android/calendar/month/bh;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/month/bh;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 804
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 809
    .line 813
    iget-object v0, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 814
    iget-object v0, p0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-virtual {v0}, Lcom/android/calendar/month/bb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    .line 817
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bh;->d:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 819
    if-nez p2, :cond_1

    .line 820
    const v1, 0x7f040064

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 821
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 822
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 823
    new-instance v1, Lcom/android/calendar/month/bn;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/android/calendar/month/bn;-><init>(Lcom/android/calendar/month/bc;)V

    .line 825
    const v0, 0x7f1201e9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->a:Landroid/widget/LinearLayout;

    .line 826
    const v0, 0x7f1201ea

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/month/bn;->b:Landroid/view/View;

    .line 827
    const v0, 0x7f1201eb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->c:Landroid/widget/TextView;

    .line 828
    const v0, 0x7f1201ef

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->d:Landroid/widget/ImageView;

    .line 829
    const v0, 0x7f1201ee

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->f:Landroid/widget/ImageView;

    .line 830
    const v0, 0x7f1201ec

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->e:Landroid/widget/TextView;

    .line 833
    const v0, 0x7f1201f0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->g:Landroid/widget/LinearLayout;

    .line 834
    const v0, 0x7f1201f1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/month/bn;->h:Landroid/view/View;

    .line 835
    const v0, 0x7f1201f2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->i:Landroid/widget/CheckBox;

    .line 836
    const v0, 0x7f1201f3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->j:Landroid/widget/TextView;

    .line 837
    const v0, 0x7f1201f6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->l:Landroid/widget/ImageView;

    .line 838
    const v0, 0x7f1201f7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->m:Landroid/widget/ImageView;

    .line 839
    const v0, 0x7f1201f4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/month/bn;->k:Landroid/widget/TextView;

    .line 840
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 846
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bh;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 847
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/calendar/month/bh;->a(ILandroid/view/View;Lcom/android/calendar/month/bn;)Landroid/view/View;

    move-result-object v0

    .line 849
    :goto_1
    return-object v0

    .line 843
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/bn;

    goto :goto_0

    .line 849
    :cond_2
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/calendar/month/bh;->b(ILandroid/view/View;Lcom/android/calendar/month/bn;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 1138
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 1139
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/bn;

    .line 1140
    if-eqz v0, :cond_0

    .line 1141
    iget-object v1, v0, Lcom/android/calendar/month/bn;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 1142
    iget-object v0, v0, Lcom/android/calendar/month/bn;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 1145
    :cond_0
    return-void
.end method
