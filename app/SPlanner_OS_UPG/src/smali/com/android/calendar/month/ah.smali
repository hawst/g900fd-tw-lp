.class Lcom/android/calendar/month/ah;
.super Landroid/view/animation/Animation;
.source "MonthByWeekFragment.java"


# instance fields
.field a:I

.field b:I

.field c:Landroid/view/View;

.field final synthetic d:Lcom/android/calendar/month/k;


# direct methods
.method public constructor <init>(Lcom/android/calendar/month/k;)V
    .locals 0

    .prologue
    .line 2906
    iput-object p1, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 2907
    return-void
.end method


# virtual methods
.method public a(ZZ)V
    .locals 2

    .prologue
    .line 2926
    if-eqz p1, :cond_1

    .line 2927
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    iput-object v0, p0, Lcom/android/calendar/month/ah;->c:Landroid/view/View;

    .line 2928
    if-eqz p2, :cond_0

    .line 2929
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->r(Lcom/android/calendar/month/k;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/android/calendar/month/ah;->a:I

    .line 2930
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->r(Lcom/android/calendar/month/k;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/android/calendar/month/ah;->b:I

    .line 2945
    :goto_0
    return-void

    .line 2932
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->r(Lcom/android/calendar/month/k;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/ah;->a:I

    .line 2933
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->r(Lcom/android/calendar/month/k;)I

    move-result v0

    neg-int v0, v0

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/android/calendar/month/ah;->b:I

    goto :goto_0

    .line 2936
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->j:Landroid/view/View;

    iput-object v0, p0, Lcom/android/calendar/month/ah;->c:Landroid/view/View;

    .line 2937
    if-eqz p2, :cond_2

    .line 2938
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->q(Lcom/android/calendar/month/k;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v1}, Lcom/android/calendar/month/k;->r(Lcom/android/calendar/month/k;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x5

    div-int/lit8 v1, v1, 0x6

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/ah;->a:I

    .line 2939
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->r(Lcom/android/calendar/month/k;)I

    move-result v0

    neg-int v0, v0

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/android/calendar/month/ah;->b:I

    goto :goto_0

    .line 2941
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->q(Lcom/android/calendar/month/k;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/ah;->a:I

    .line 2942
    iget-object v0, p0, Lcom/android/calendar/month/ah;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->r(Lcom/android/calendar/month/k;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/android/calendar/month/ah;->b:I

    goto :goto_0
.end method

.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 2911
    iget-object v0, p0, Lcom/android/calendar/month/ah;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/month/ah;->a:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/calendar/month/ah;->b:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2912
    iget-object v0, p0, Lcom/android/calendar/month/ah;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 2913
    return-void
.end method

.method public initialize(IIII)V
    .locals 0

    .prologue
    .line 2917
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 2918
    return-void
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 2922
    const/4 v0, 0x1

    return v0
.end method
