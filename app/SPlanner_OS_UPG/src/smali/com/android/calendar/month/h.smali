.class public Lcom/android/calendar/month/h;
.super Lcom/android/calendar/month/ce;
.source "MonthByWeekAdapter.java"


# static fields
.field public static A:Z

.field private static final aj:Z

.field public static q:Z

.field public static r:Z

.field public static s:Landroid/view/View;

.field public static t:F

.field public static u:F

.field public static v:F

.field public static w:Z

.field public static x:Z

.field public static y:I

.field public static z:I


# instance fields
.field private W:Lcom/android/calendar/month/bo;

.field private X:Z

.field private Y:Landroid/text/format/Time;

.field private Z:Ljava/util/ArrayList;

.field protected a:Lcom/android/calendar/al;

.field private aa:Ljava/util/ArrayList;

.field private ab:Z

.field private ac:Z

.field private ad:J

.field private ae:Lcom/android/calendar/month/a;

.field private af:I

.field private ag:Landroid/os/Handler;

.field private ah:Landroid/text/format/Time;

.field private ai:I

.field protected b:Ljava/lang/String;

.field protected c:Landroid/text/format/Time;

.field protected d:Landroid/text/format/Time;

.field protected e:I

.field protected f:Z

.field protected g:Z

.field protected h:I

.field protected i:Z

.field protected j:Z

.field protected k:Z

.field l:Lcom/android/calendar/month/bo;

.field protected m:Ljava/util/ArrayList;

.field protected n:Ljava/util/ArrayList;

.field protected o:Ljava/util/ArrayList;

.field p:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 124
    sput-boolean v1, Lcom/android/calendar/month/h;->q:Z

    .line 126
    sput-boolean v1, Lcom/android/calendar/month/h;->r:Z

    .line 128
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/calendar/month/h;->aj:Z

    .line 811
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/month/h;->s:Landroid/view/View;

    .line 813
    sput v2, Lcom/android/calendar/month/h;->t:F

    .line 815
    sput v2, Lcom/android/calendar/month/h;->u:F

    .line 817
    sput v2, Lcom/android/calendar/month/h;->v:F

    .line 819
    sput-boolean v1, Lcom/android/calendar/month/h;->w:Z

    .line 820
    sput-boolean v1, Lcom/android/calendar/month/h;->x:Z

    .line 822
    sput v1, Lcom/android/calendar/month/h;->y:I

    .line 824
    sput v1, Lcom/android/calendar/month/h;->z:I

    .line 827
    sput-boolean v1, Lcom/android/calendar/month/h;->A:Z

    return-void

    :cond_1
    move v0, v1

    .line 128
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 131
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/month/ce;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 77
    iput-boolean v1, p0, Lcom/android/calendar/month/h;->f:Z

    .line 79
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->g:Z

    .line 81
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/month/h;->h:I

    .line 83
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->i:Z

    .line 85
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->j:Z

    .line 87
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->k:Z

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/month/h;->n:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    .line 100
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->X:Z

    .line 108
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->ab:Z

    .line 110
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->ac:Z

    .line 112
    iput-wide v4, p0, Lcom/android/calendar/month/h;->ad:J

    .line 114
    iput-wide v4, p0, Lcom/android/calendar/month/h;->p:J

    .line 118
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/month/h;->af:I

    .line 120
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/h;->ag:Landroid/os/Handler;

    .line 132
    new-instance v0, Landroid/view/GestureDetector;

    sget-object v3, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    new-instance v4, Lcom/android/calendar/month/j;

    invoke-direct {v4, p0}, Lcom/android/calendar/month/j;-><init>(Lcom/android/calendar/month/h;)V

    invoke-direct {v0, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/month/h;->N:Landroid/view/GestureDetector;

    .line 134
    const-string v0, "mini_month"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const-string v0, "mini_month"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/month/h;->f:Z

    .line 138
    :cond_0
    const-string v0, "tablet"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    const-string v0, "tablet"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    iput-boolean v1, p0, Lcom/android/calendar/month/h;->g:Z

    .line 141
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 135
    goto :goto_0

    :cond_3
    move v1, v2

    .line 139
    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/month/h;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/android/calendar/month/h;->af:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/month/h;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/android/calendar/month/h;->Z:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(Landroid/text/format/Time;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 863
    sget-object v1, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    .line 864
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 937
    :cond_0
    :goto_0
    return-void

    .line 868
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/h;->W:Lcom/android/calendar/month/bo;

    if-eqz v0, :cond_0

    .line 873
    invoke-virtual {p2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 874
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 875
    if-eqz v2, :cond_8

    move v3, v7

    .line 876
    :goto_1
    if-ge v3, v4, :cond_3

    .line 877
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 878
    iget-wide v8, v0, Lcom/android/calendar/dh;->b:J

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-gez v0, :cond_2

    .line 879
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 876
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 882
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/2addr v0, v7

    .line 885
    :goto_2
    if-eqz p3, :cond_4

    .line 886
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 889
    :cond_4
    if-lt v0, v6, :cond_0

    .line 893
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 894
    invoke-virtual {v3, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 895
    iput v7, v3, Landroid/text/format/Time;->hour:I

    .line 896
    iput v7, v3, Landroid/text/format/Time;->minute:I

    .line 897
    sget-boolean v0, Lcom/android/calendar/month/h;->aj:Z

    if-eqz v0, :cond_5

    .line 898
    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    const-wide/16 v8, -0x1

    cmp-long v0, v4, v8

    if-nez v0, :cond_5

    iget v0, v3, Landroid/text/format/Time;->year:I

    const/16 v4, 0x7b2

    if-eq v0, v4, :cond_5

    .line 899
    iput-boolean v7, v3, Landroid/text/format/Time;->allDay:Z

    .line 900
    iput v6, v3, Landroid/text/format/Time;->hour:I

    .line 903
    :cond_5
    new-instance v0, Lcom/android/calendar/month/a;

    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/month/a;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;JZZ)V

    iput-object v0, p0, Lcom/android/calendar/month/h;->ae:Lcom/android/calendar/month/a;

    .line 905
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 906
    iget-object v0, p0, Lcom/android/calendar/month/h;->W:Lcom/android/calendar/month/bo;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/bo;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 907
    iget-object v0, p0, Lcom/android/calendar/month/h;->W:Lcom/android/calendar/month/bo;

    iget v3, p1, Landroid/text/format/Time;->weekDay:I

    invoke-virtual {v0, v3}, Lcom/android/calendar/month/bo;->a(I)I

    move-result v0

    .line 909
    iget v3, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    .line 911
    iget v0, p0, Lcom/android/calendar/month/h;->h:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_7

    .line 912
    iget v0, v2, Landroid/graphics/Rect;->top:I

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    sget-object v4, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/calendar/hj;->A(Landroid/content/Context;)I

    move-result v4

    add-int/2addr v0, v4

    .line 917
    :goto_3
    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, v4, v2

    .line 918
    iget-object v4, p0, Lcom/android/calendar/month/h;->ae:Lcom/android/calendar/month/a;

    invoke-virtual {v4, v3, v0, v2}, Lcom/android/calendar/month/a;->a(III)V

    .line 919
    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 920
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 922
    sget-object v2, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 923
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_6

    .line 925
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 931
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/month/h;->ae:Lcom/android/calendar/month/a;

    sget-object v2, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 933
    :try_start_1
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 934
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 914
    :cond_7
    iget v0, v2, Landroid/graphics/Rect;->top:I

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    goto :goto_3

    .line 926
    :catch_1
    move-exception v0

    goto/16 :goto_0

    :cond_8
    move v0, v7

    goto/16 :goto_2
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    .line 760
    iget-object v0, p0, Lcom/android/calendar/month/h;->W:Lcom/android/calendar/month/bo;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/bo;->a(F)Landroid/text/format/Time;

    move-result-object v0

    .line 763
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 764
    sget-wide v4, Lcom/android/calendar/AllInOneActivity;->d:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x9c4

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 765
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/text/format/Time;->year:I

    const/16 v2, 0x76e

    if-ne v1, v2, :cond_1

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/h;->a(Landroid/text/format/Time;Z)V

    goto :goto_0
.end method

.method private a(Lcom/android/calendar/month/bo;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 428
    iget-object v0, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 429
    const-string v0, "MonthByWeek"

    invoke-static {v0, v5}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    const-string v0, "MonthByWeek"

    const-string v1, "No events loaded, did not pass any events to view."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :cond_0
    invoke-virtual {p1, v4, v4}, Lcom/android/calendar/month/bo;->a(Ljava/util/List;Ljava/util/ArrayList;)V

    .line 447
    :goto_0
    return-void

    .line 435
    :cond_1
    invoke-virtual {p1}, Lcom/android/calendar/month/bo;->getFirstJulianDay()I

    move-result v0

    .line 436
    iget v1, p0, Lcom/android/calendar/month/h;->S:I

    sub-int v1, v0, v1

    .line 437
    iget v2, p1, Lcom/android/calendar/month/bo;->ba:I

    add-int/2addr v2, v1

    .line 438
    if-ltz v1, :cond_2

    iget-object v3, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v2, v3, :cond_4

    .line 439
    :cond_2
    const-string v1, "MonthByWeek"

    invoke-static {v1, v5}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 440
    const-string v1, "MonthByWeek"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Week is outside range of loaded events. viewStart: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " eventsStart: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/calendar/month/h;->S:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_3
    invoke-virtual {p1, v4, v4}, Lcom/android/calendar/month/bo;->a(Ljava/util/List;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 446
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/h;->n:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Lcom/android/calendar/month/bo;->a(Ljava/util/List;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/month/h;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/month/h;->Z:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/month/h;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/android/calendar/month/h;->aa:Ljava/util/ArrayList;

    return-object p1
.end method

.method private b(Lcom/android/calendar/month/bo;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 450
    iget-object v0, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 476
    :goto_0
    return-void

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 457
    const-string v0, "MonthByWeek"

    invoke-static {v0, v4}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 458
    const-string v0, "MonthByWeek"

    const-string v1, "No tasks loaded, did not pass any events to view."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :cond_1
    invoke-virtual {p1, v5}, Lcom/android/calendar/month/bo;->setTasks(Ljava/util/List;)V

    goto :goto_0

    .line 463
    :cond_2
    invoke-virtual {p1}, Lcom/android/calendar/month/bo;->getFirstJulianDay()I

    move-result v0

    .line 464
    iget v1, p0, Lcom/android/calendar/month/h;->S:I

    sub-int v1, v0, v1

    .line 465
    iget v2, p1, Lcom/android/calendar/month/bo;->ba:I

    add-int/2addr v2, v1

    .line 466
    if-ltz v1, :cond_3

    iget-object v3, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v2, v3, :cond_5

    .line 467
    :cond_3
    const-string v1, "MonthByWeek"

    invoke-static {v1, v4}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 468
    const-string v1, "MonthByWeek"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Week is outside range of loaded events. viewStart: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " eventsStart: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/calendar/month/h;->S:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    :cond_4
    invoke-virtual {p1, v5}, Lcom/android/calendar/month/bo;->setTasks(Ljava/util/List;)V

    goto :goto_0

    .line 475
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/calendar/month/bo;->setTasks(Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/month/h;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/month/h;->aa:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected static c(Landroid/text/format/Time;)V
    .locals 10

    .prologue
    const-wide/32 v8, 0x5265c00

    .line 640
    invoke-static {p0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 641
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 642
    add-long v0, v2, v8

    .line 643
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 645
    sget-boolean v5, Lcom/android/calendar/month/h;->aj:Z

    if-eqz v5, :cond_0

    .line 646
    const-wide/16 v6, -0x1

    cmp-long v5, v2, v6

    if-nez v5, :cond_1

    .line 647
    invoke-static {p0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v2

    .line 648
    add-long v0, v2, v8

    .line 656
    :cond_0
    :goto_0
    :try_start_0
    const-string v5, "KEY_START_MILLIS"

    invoke-virtual {v4, v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 657
    const-string v2, "KEY_END_MILLIS"

    invoke-virtual {v4, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 659
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    const-class v1, Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 660
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 664
    :goto_1
    return-void

    .line 650
    :cond_1
    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v2

    .line 651
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v0

    goto :goto_0

    .line 661
    :catch_0
    move-exception v0

    .line 662
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic d(Lcom/android/calendar/month/h;)Lcom/android/calendar/month/a;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/month/h;->ae:Lcom/android/calendar/month/a;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 164
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 166
    iget-object v0, p0, Lcom/android/calendar/month/h;->d:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/android/calendar/month/h;->d:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 168
    iget-object v0, p0, Lcom/android/calendar/month/h;->c:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/android/calendar/month/h;->ah:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/android/calendar/month/h;->ah:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 172
    return-void
.end method

.method private j()V
    .locals 14

    .prologue
    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 667
    sget-boolean v0, Lcom/android/calendar/month/h;->x:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 668
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->r(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v4

    .line 669
    new-instance v0, Lcom/android/calendar/event/iv;

    sget-object v1, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v4, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/event/iv;-><init>(Landroid/content/Context;JJZ)V

    .line 670
    sget-object v1, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 671
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 672
    sget-object v3, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 673
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_0

    .line 675
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 680
    :cond_0
    sget-object v1, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 682
    :try_start_1
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 690
    :goto_0
    return-void

    .line 687
    :cond_1
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->r(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v1

    .line 688
    iget-object v0, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object v1, p0

    move v11, v10

    move-wide v12, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    goto :goto_0

    .line 676
    :catch_0
    move-exception v0

    goto :goto_0

    .line 683
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 951
    iget-object v0, p0, Lcom/android/calendar/month/h;->ae:Lcom/android/calendar/month/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/h;->ae:Lcom/android/calendar/month/a;

    invoke-virtual {v0}, Lcom/android/calendar/month/a;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/h;->ae:Lcom/android/calendar/month/a;

    invoke-virtual {v0}, Lcom/android/calendar/month/a;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 953
    iget-object v0, p0, Lcom/android/calendar/month/h;->ag:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/month/i;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/i;-><init>(Lcom/android/calendar/month/h;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 965
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Ljava/util/ArrayList;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 257
    iget-object v1, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-object v0

    .line 261
    :cond_1
    iget v1, p0, Lcom/android/calendar/month/h;->S:I

    sub-int v1, p1, v1

    .line 262
    if-ltz v1, :cond_0

    iget v2, p0, Lcom/android/calendar/month/h;->e:I

    if-ge v1, v2, :cond_0

    .line 263
    iget-object v0, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->ac:Z

    .line 145
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/month/h;->ad:J

    .line 146
    return-void
.end method

.method public a(IILjava/util/ArrayList;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x0

    .line 200
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->f:Z

    if-eqz v0, :cond_1

    .line 201
    const-string v0, "MonthByWeek"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const-string v0, "MonthByWeek"

    const-string v1, "Attempted to set events for mini view. Events only supported in full view."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iput-object p3, p0, Lcom/android/calendar/month/h;->n:Ljava/util/ArrayList;

    .line 207
    iput p1, p0, Lcom/android/calendar/month/h;->S:I

    .line 208
    iput p2, p0, Lcom/android/calendar/month/h;->e:I

    .line 211
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v4

    .line 212
    :goto_1
    if-ge v0, p2, :cond_2

    .line 213
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 216
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 217
    :cond_3
    const-string v0, "MonthByWeek"

    invoke-static {v0, v7}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 218
    const-string v0, "MonthByWeek"

    const-string v1, "No events. Returning early--go schedule something fun."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_4
    iput-object v5, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    goto :goto_0

    .line 226
    :cond_5
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 227
    iget v1, v0, Lcom/android/calendar/dh;->i:I

    iget v2, p0, Lcom/android/calendar/month/h;->S:I

    sub-int v3, v1, v2

    .line 228
    iget v1, v0, Lcom/android/calendar/dh;->j:I

    iget v2, p0, Lcom/android/calendar/month/h;->S:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 229
    if-lt v3, p2, :cond_7

    if-ltz v1, :cond_6

    .line 230
    :cond_7
    if-gez v3, :cond_8

    move v3, v4

    .line 233
    :cond_8
    if-gt v3, p2, :cond_6

    .line 236
    if-ltz v1, :cond_6

    .line 239
    if-le v1, p2, :cond_b

    move v2, p2

    .line 242
    :goto_2
    if-ge v3, v2, :cond_6

    .line 243
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 247
    :cond_9
    const-string v0, "MonthByWeek"

    invoke-static {v0, v7}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 248
    const-string v0, "MonthByWeek"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Processed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " events."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_a
    iput-object v5, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    goto :goto_2
.end method

.method public a(Landroid/text/format/Time;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 184
    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-eqz v0, :cond_0

    .line 185
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/month/h;->ah:Landroid/text/format/Time;

    .line 189
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 190
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 191
    iget-object v2, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/h;->K:I

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(II)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/h;->J:I

    .line 194
    iget-object v0, p0, Lcom/android/calendar/month/h;->ah:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/h;->ah:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/h;->K:I

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(II)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/h;->ai:I

    .line 196
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->notifyDataSetChanged()V

    .line 197
    return-void

    .line 187
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/month/h;->ah:Landroid/text/format/Time;

    goto :goto_0
.end method

.method public a(Landroid/text/format/Time;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 506
    check-cast p2, Lcom/android/calendar/month/bo;

    iput-object p2, p0, Lcom/android/calendar/month/h;->W:Lcom/android/calendar/month/bo;

    .line 507
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/month/h;->a(Landroid/text/format/Time;Z)V

    .line 508
    return-void
.end method

.method protected a(Landroid/text/format/Time;Z)V
    .locals 14

    .prologue
    .line 517
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    if-nez v0, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_2

    .line 525
    :cond_2
    invoke-static {p1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 526
    iget-object v0, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    goto :goto_0

    .line 530
    :cond_3
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/h;->q:Z

    .line 531
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->X:Z

    .line 533
    iget-object v0, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    iput-object v0, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 534
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 535
    iget-object v1, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 536
    iget v1, v0, Landroid/text/format/Time;->hour:I

    iput v1, p1, Landroid/text/format/Time;->hour:I

    .line 537
    iget v0, v0, Landroid/text/format/Time;->minute:I

    iput v0, p1, Landroid/text/format/Time;->minute:I

    .line 538
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/text/format/Time;->allDay:Z

    .line 539
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 540
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-wide v2, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 541
    iget v1, p0, Lcom/android/calendar/month/h;->S:I

    sub-int v3, v0, v1

    .line 542
    sget-boolean v0, Lcom/android/calendar/month/h;->aj:Z

    if-eqz v0, :cond_4

    .line 543
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    const/16 v1, 0x7b2

    if-eq v0, v1, :cond_4

    .line 544
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/text/format/Time;->allDay:Z

    .line 545
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    const/4 v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 548
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget-wide v4, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v6

    .line 549
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-wide v4, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v7

    .line 550
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget v8, v0, Landroid/text/format/Time;->month:I

    .line 551
    iget v4, p1, Landroid/text/format/Time;->month:I

    .line 553
    iget v9, p1, Landroid/text/format/Time;->month:I

    .line 555
    sub-int v0, v6, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x2a

    if-ge v0, v1, :cond_0

    .line 558
    if-eq v8, v9, :cond_5

    .line 559
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/k;->w:Z

    .line 562
    :cond_5
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->f:Z

    if-nez v0, :cond_10

    .line 563
    const/4 v0, 0x0

    .line 564
    const/4 v2, 0x0

    .line 566
    if-ltz v3, :cond_12

    iget-object v1, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v3, v1, :cond_12

    .line 567
    iget-object v0, p0, Lcom/android/calendar/month/h;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move-object v1, v0

    .line 569
    :goto_1
    iput v3, p0, Lcom/android/calendar/month/h;->af:I

    .line 570
    if-ltz v3, :cond_6

    iget-object v0, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 571
    iget-object v0, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move-object v2, v0

    .line 574
    :cond_6
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->ab:Z

    if-nez v0, :cond_c

    .line 575
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_e

    .line 576
    :cond_8
    monitor-enter p0

    .line 577
    :try_start_0
    sget v0, Lcom/android/calendar/month/k;->n:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_9

    iget-object v0, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->m()Z

    move-result v0

    if-nez v0, :cond_a

    .line 578
    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->X:Z

    .line 579
    invoke-static {}, Lcom/android/calendar/dz;->t()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 580
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_a

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    iget-wide v10, v0, Lcom/android/calendar/dh;->b:J

    const-wide/16 v12, 0x0

    cmp-long v0, v10, v12

    if-gez v0, :cond_a

    .line 581
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->X:Z

    .line 585
    :cond_a
    iput-object p1, p0, Lcom/android/calendar/month/h;->Y:Landroid/text/format/Time;

    .line 586
    iput-object v1, p0, Lcom/android/calendar/month/h;->Z:Ljava/util/ArrayList;

    .line 587
    iput-object v2, p0, Lcom/android/calendar/month/h;->aa:Ljava/util/ArrayList;

    .line 589
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->g:Z

    if-nez v0, :cond_b

    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 592
    if-ne v8, v4, :cond_b

    .line 593
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->X:Z

    .line 594
    invoke-static {p1}, Lcom/android/calendar/month/h;->c(Landroid/text/format/Time;)V

    .line 597
    :cond_b
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 624
    :cond_c
    :goto_2
    if-eqz p2, :cond_d

    .line 625
    iget v0, p1, Landroid/text/format/Time;->month:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/h;->c(I)V

    .line 628
    :cond_d
    if-ne v8, v9, :cond_11

    if-eq v6, v7, :cond_11

    .line 629
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/bo;->ah:Z

    .line 634
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    sget-object v1, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v4, p1

    move-object v5, p1

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 636
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->ab:Z

    goto/16 :goto_0

    .line 597
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 602
    :cond_e
    if-ne v6, v7, :cond_c

    .line 603
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 604
    iget-wide v2, p0, Lcom/android/calendar/month/h;->p:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_f

    iget-wide v2, p0, Lcom/android/calendar/month/h;->p:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x258

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 607
    :cond_f
    iput-wide v0, p0, Lcom/android/calendar/month/h;->p:J

    .line 608
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/h;->q:Z

    .line 609
    invoke-direct {p0}, Lcom/android/calendar/month/h;->j()V

    goto :goto_2

    .line 617
    :cond_10
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->R:Z

    if-eqz v0, :cond_c

    .line 618
    if-ne v6, v7, :cond_c

    .line 619
    iget-object v0, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    sget-object v1, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    const-wide/32 v2, 0x4000000

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;I)V

    goto :goto_2

    .line 631
    :cond_11
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/bo;->ah:Z

    goto :goto_3

    :cond_12
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 270
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->f:Z

    if-eqz v0, :cond_0

    .line 315
    :goto_0
    return-void

    .line 274
    :cond_0
    iget v0, p0, Lcom/android/calendar/month/h;->e:I

    if-ge v0, v3, :cond_1

    .line 275
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->c()V

    goto :goto_0

    .line 285
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v2

    .line 286
    :goto_1
    iget v1, p0, Lcom/android/calendar/month/h;->e:I

    if-ge v0, v1, :cond_2

    .line 287
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 290
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 291
    :cond_3
    const-string v0, "MonthByWeek"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 292
    const-string v0, "MonthByWeek"

    const-string v1, "No tasks. Returning early--go schedule something fun."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_4
    iput-object v4, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    .line 295
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->c()V

    goto :goto_0

    .line 299
    :cond_5
    new-instance v5, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    invoke-direct {v5, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 300
    invoke-virtual {v5}, Landroid/text/format/Time;->setToNow()V

    .line 301
    invoke-virtual {v5, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 303
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/aa;

    .line 304
    iget-wide v8, v0, Lcom/android/calendar/task/aa;->e:J

    iget-wide v10, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v10, v11}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    iget v3, p0, Lcom/android/calendar/month/h;->S:I

    sub-int v7, v1, v3

    move v3, v2

    .line 306
    :goto_2
    iget v1, p0, Lcom/android/calendar/month/h;->e:I

    if-ge v3, v1, :cond_6

    .line 307
    if-ne v7, v3, :cond_7

    .line 308
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    :cond_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 313
    :cond_8
    iput-object v4, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    .line 314
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->c()V

    goto :goto_0
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 1

    .prologue
    .line 941
    invoke-super {p0, p1}, Lcom/android/calendar/month/ce;->a(Ljava/util/HashMap;)V

    .line 943
    const-string v0, "tablet"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 944
    const-string v0, "tablet"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/month/h;->g:Z

    .line 947
    :cond_0
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->k:Z

    .line 948
    return-void

    .line 944
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Ljava/util/ArrayList;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 318
    iget-object v1, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-object v0

    .line 322
    :cond_1
    iget v1, p0, Lcom/android/calendar/month/h;->S:I

    sub-int v1, p1, v1

    .line 323
    if-ltz v1, :cond_0

    iget v2, p0, Lcom/android/calendar/month/h;->e:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 324
    iget-object v0, p0, Lcom/android/calendar/month/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Lcom/android/calendar/month/ce;->b()V

    .line 151
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/h;->a:Lcom/android/calendar/al;

    .line 152
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 155
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, Lcom/android/calendar/month/h;->ah:Landroid/text/format/Time;

    .line 156
    iget-object v0, p0, Lcom/android/calendar/month/h;->ah:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 158
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/month/h;->d:Landroid/text/format/Time;

    .line 159
    iget-object v0, p0, Lcom/android/calendar/month/h;->d:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 160
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/month/h;->c:Landroid/text/format/Time;

    .line 161
    return-void
.end method

.method protected b(Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 512
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/month/h;->a(Landroid/text/format/Time;Z)V

    .line 513
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 481
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/h;->K:I

    .line 482
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->L:Z

    .line 484
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->i:Z

    .line 487
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->i:Z

    if-eqz v0, :cond_0

    .line 488
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/h;->K:I

    .line 489
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->L:Z

    .line 492
    :cond_0
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/h;->b:Ljava/lang/String;

    .line 493
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/month/h;->h:I

    .line 495
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->z(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->j:Z

    .line 497
    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->b(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->M:Z

    .line 499
    invoke-direct {p0}, Lcom/android/calendar/month/h;->i()V

    .line 500
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->notifyDataSetChanged()V

    .line 502
    invoke-direct {p0}, Lcom/android/calendar/month/h;->k()V

    .line 503
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 831
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/month/h;->s:Landroid/view/View;

    .line 832
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/month/h;->t:F

    .line 833
    sput-boolean v1, Lcom/android/calendar/month/h;->w:Z

    .line 834
    sput v1, Lcom/android/calendar/month/h;->z:I

    sput v1, Lcom/android/calendar/month/h;->y:I

    .line 835
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->notifyDataSetInvalidated()V

    .line 836
    return-void
.end method

.method e()V
    .locals 3

    .prologue
    .line 841
    monitor-enter p0

    .line 842
    :try_start_0
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->X:Z

    if-eqz v0, :cond_3

    .line 844
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->g:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 846
    iget-object v0, p0, Lcom/android/calendar/month/h;->Y:Landroid/text/format/Time;

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lcom/android/calendar/month/h;->Y:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/month/h;->c(Landroid/text/format/Time;)V

    .line 848
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->X:Z

    .line 849
    monitor-exit p0

    .line 859
    :goto_0
    return-void

    .line 851
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->g:Z

    if-eqz v0, :cond_2

    .line 852
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->X:Z

    .line 853
    monitor-exit p0

    goto :goto_0

    .line 858
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 855
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/month/h;->Y:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/h;->Z:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/month/h;->aa:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/month/h;->a(Landroid/text/format/Time;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 856
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/h;->X:Z

    .line 858
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 968
    monitor-enter p0

    .line 970
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/h;->S:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/h;->af:I

    .line 971
    monitor-exit p0

    .line 972
    return-void

    .line 971
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 333
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->f:Z

    if-eqz v0, :cond_0

    .line 334
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/month/ce;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 424
    :goto_0
    return-object v0

    .line 337
    :cond_0
    new-instance v7, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v7, v4, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 338
    const/4 v0, 0x0

    .line 340
    if-eqz p2, :cond_8

    .line 341
    check-cast p2, Lcom/android/calendar/month/bo;

    .line 343
    invoke-virtual {p2}, Lcom/android/calendar/month/bo;->getWindowVisibility()I

    move-result v0

    .line 344
    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Lcom/android/calendar/month/bo;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_1

    .line 346
    new-instance p2, Lcom/android/calendar/month/bo;

    sget-object v0, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/android/calendar/month/bo;-><init>(Landroid/content/Context;)V

    .line 351
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->ac:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/android/calendar/month/h;->k:Z

    invoke-virtual {p2, v0, v1}, Lcom/android/calendar/month/bo;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 352
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 356
    iget-wide v8, p0, Lcom/android/calendar/month/h;->ad:J

    sub-long/2addr v0, v8

    const-wide/16 v8, 0x3e8

    cmp-long v0, v0, v8

    if-lez v0, :cond_7

    .line 357
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->ac:Z

    .line 358
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/month/h;->ad:J

    move v1, v2

    .line 367
    :goto_1
    invoke-virtual {p2}, Lcom/android/calendar/month/bo;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    move v6, v1

    move-object v5, p2

    .line 372
    :goto_2
    if-nez v0, :cond_2

    .line 373
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 375
    :cond_2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 377
    invoke-virtual {v5, v7}, Lcom/android/calendar/month/bo;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 378
    invoke-virtual {v5, v3}, Lcom/android/calendar/month/bo;->setClickable(Z)V

    .line 379
    invoke-virtual {v5, p0}, Lcom/android/calendar/month/bo;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 382
    iget v1, p0, Lcom/android/calendar/month/h;->J:I

    if-ne v1, p1, :cond_d

    .line 383
    iget-object v1, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->weekDay:I

    .line 386
    :goto_3
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    .line 387
    iget v4, p0, Lcom/android/calendar/month/h;->O:I

    div-int v4, v7, v4

    .line 392
    const/16 v8, 0x1b85

    if-eq p1, v8, :cond_3

    rem-int/lit8 v8, p1, 0x6

    if-nez v8, :cond_4

    .line 393
    :cond_3
    iget v8, p0, Lcom/android/calendar/month/h;->O:I

    mul-int/2addr v8, v4

    sub-int/2addr v7, v8

    .line 394
    add-int/2addr v4, v7

    .line 397
    :cond_4
    const-string v7, "height"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    const-string v4, "selected_day"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    const-string v4, "show_wk_num"

    iget-boolean v1, p0, Lcom/android/calendar/month/h;->L:Z

    if-eqz v1, :cond_9

    move v1, v3

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    const-string v1, "week_start"

    iget v4, p0, Lcom/android/calendar/month/h;->K:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    const-string v1, "num_days"

    iget v4, p0, Lcom/android/calendar/month/h;->P:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    const-string v1, "week"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    const-string v1, "focus_month"

    iget v4, p0, Lcom/android/calendar/month/h;->Q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    const-string v4, "is_weather_enabled"

    iget-boolean v1, p0, Lcom/android/calendar/month/h;->M:Z

    if-eqz v1, :cond_a

    move v1, v3

    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    const-string v1, "orientation"

    iget v4, p0, Lcom/android/calendar/month/h;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    const-string v4, "simple_month_view"

    iget-boolean v1, p0, Lcom/android/calendar/month/h;->j:Z

    if-eqz v1, :cond_b

    move v1, v3

    :goto_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    const-string v4, "system_timezone"

    iget-boolean v1, p0, Lcom/android/calendar/month/h;->k:Z

    if-eqz v1, :cond_c

    move v1, v3

    :goto_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    iget-boolean v1, p0, Lcom/android/calendar/month/h;->i:Z

    if-eqz v1, :cond_5

    .line 410
    const-string v1, "easy_mode"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    :cond_5
    if-eqz v6, :cond_6

    .line 414
    const-string v1, "animate_today"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    iput-boolean v2, p0, Lcom/android/calendar/month/h;->ac:Z

    .line 418
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/month/h;->I:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Lcom/android/calendar/month/bo;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 419
    invoke-direct {p0, v5}, Lcom/android/calendar/month/h;->a(Lcom/android/calendar/month/bo;)V

    .line 420
    invoke-direct {p0, v5}, Lcom/android/calendar/month/h;->b(Lcom/android/calendar/month/bo;)V

    .line 422
    iput-object v5, p0, Lcom/android/calendar/month/h;->l:Lcom/android/calendar/month/bo;

    move-object v0, v5

    .line 424
    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 360
    goto/16 :goto_1

    .line 370
    :cond_8
    new-instance v5, Lcom/android/calendar/month/bo;

    sget-object v1, Lcom/android/calendar/month/h;->H:Landroid/content/Context;

    invoke-direct {v5, v1}, Lcom/android/calendar/month/bo;-><init>(Landroid/content/Context;)V

    move v6, v2

    goto/16 :goto_2

    :cond_9
    move v1, v2

    .line 399
    goto/16 :goto_4

    :cond_a
    move v1, v2

    .line 404
    goto :goto_5

    :cond_b
    move v1, v2

    .line 406
    goto :goto_6

    :cond_c
    move v1, v2

    .line 407
    goto :goto_7

    :cond_d
    move v1, v4

    goto/16 :goto_3

    :cond_e
    move v1, v2

    goto/16 :goto_1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 695
    if-nez p2, :cond_0

    move v0, v1

    .line 752
    :goto_0
    return v0

    .line 699
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->f:Z

    if-eqz v0, :cond_1

    .line 700
    invoke-super {p0, p1, p2}, Lcom/android/calendar/month/ce;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 702
    :cond_1
    sget-boolean v0, Lcom/android/calendar/month/k;->r:Z

    if-nez v0, :cond_2

    move v0, v2

    .line 703
    goto :goto_0

    .line 706
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 708
    sparse-switch v0, :sswitch_data_0

    .line 748
    :cond_3
    :goto_1
    check-cast p1, Lcom/android/calendar/month/bo;

    iput-object p1, p0, Lcom/android/calendar/month/h;->W:Lcom/android/calendar/month/bo;

    .line 749
    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-nez v0, :cond_4

    .line 750
    iget-object v0, p0, Lcom/android/calendar/month/h;->N:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_4
    move v0, v1

    .line 752
    goto :goto_0

    .line 710
    :sswitch_0
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    .line 711
    sput-boolean v2, Lcom/android/calendar/month/h;->x:Z

    goto :goto_1

    .line 713
    :cond_5
    sput-boolean v1, Lcom/android/calendar/month/h;->x:Z

    goto :goto_1

    .line 719
    :sswitch_1
    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-eqz v0, :cond_3

    .line 721
    check-cast p1, Lcom/android/calendar/month/cd;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/android/calendar/month/cd;->c(F)I

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->z:I

    .line 722
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->notifyDataSetInvalidated()V

    move v0, v2

    .line 723
    goto :goto_0

    .line 728
    :sswitch_2
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->d()V

    .line 729
    invoke-direct {p0, p2}, Lcom/android/calendar/month/h;->a(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 734
    :sswitch_3
    iget-boolean v0, p0, Lcom/android/calendar/month/h;->f:Z

    if-eqz v0, :cond_6

    move v0, v1

    .line 735
    goto :goto_0

    :cond_6
    move-object v0, p1

    .line 737
    check-cast v0, Lcom/android/calendar/month/cd;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/calendar/month/cd;->c(F)I

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->y:I

    .line 738
    sput-boolean v2, Lcom/android/calendar/month/h;->w:Z

    .line 739
    sput-boolean v2, Lcom/android/calendar/month/h;->x:Z

    goto :goto_1

    .line 743
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/calendar/month/h;->d()V

    .line 744
    sput-boolean v1, Lcom/android/calendar/month/h;->x:Z

    goto :goto_1

    .line 708
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0xd3 -> :sswitch_3
        0xd4 -> :sswitch_4
        0xd5 -> :sswitch_1
    .end sparse-switch
.end method
