.class Lcom/android/calendar/month/ca;
.super Landroid/database/DataSetObserver;
.source "SimpleDayPickerFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/month/by;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/by;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/android/calendar/month/ca;->a:Lcom/android/calendar/month/by;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 208
    iget-object v0, p0, Lcom/android/calendar/month/ca;->a:Lcom/android/calendar/month/by;

    iget-object v0, v0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->g()Landroid/text/format/Time;

    move-result-object v0

    .line 209
    iget v1, v0, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/android/calendar/month/ca;->a:Lcom/android/calendar/month/by;

    iget-object v2, v2, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    if-ne v1, v2, :cond_0

    iget v1, v0, Landroid/text/format/Time;->yearDay:I

    iget-object v2, p0, Lcom/android/calendar/month/ca;->a:Lcom/android/calendar/month/by;

    iget-object v2, v2, Lcom/android/calendar/month/by;->Z:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->yearDay:I

    if-eq v1, v2, :cond_1

    .line 210
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/ca;->a:Lcom/android/calendar/month/by;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const/4 v6, 0x0

    move v5, v4

    invoke-virtual/range {v1 .. v6}, Lcom/android/calendar/month/by;->a(JZZZ)Z

    .line 212
    :cond_1
    return-void
.end method
