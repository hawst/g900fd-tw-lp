.class public Lcom/android/calendar/month/bo;
.super Lcom/android/calendar/month/cd;
.source "MonthWeekEventsView.java"


# static fields
.field public static R:I

.field public static S:I

.field public static W:Z

.field public static ah:Z

.field private static bP:I

.field private static bQ:I

.field private static bR:I

.field private static bS:I

.field private static bT:I

.field private static bU:I

.field private static bV:I

.field private static bW:I

.field private static bX:I

.field private static bY:I

.field private static bZ:I

.field private static cA:I

.field private static cB:I

.field private static cC:I

.field private static cD:I

.field private static cE:I

.field private static cF:I

.field private static cG:I

.field private static cH:I

.field private static cI:I

.field private static cJ:I

.field private static cK:I

.field private static cL:I

.field private static cM:I

.field private static cN:I

.field private static cO:I

.field private static cP:I

.field private static cQ:I

.field private static cR:I

.field private static cS:I

.field private static cT:I

.field private static cU:I

.field private static cV:I

.field private static cW:I

.field private static cX:I

.field private static cY:I

.field private static cZ:I

.field private static ca:I

.field private static cb:I

.field private static cc:I

.field private static cd:I

.field private static ce:I

.field private static cf:I

.field private static cg:I

.field private static ch:I

.field private static ci:I

.field private static cj:I

.field private static ck:I

.field private static cl:I

.field private static cm:I

.field private static cn:I

.field private static co:I

.field private static cp:I

.field private static cq:I

.field private static cr:I

.field private static cs:I

.field private static ct:I

.field private static cu:I

.field private static cv:I

.field private static cw:I

.field private static cx:I

.field private static cy:I

.field private static cz:I

.field private static dI:Ljava/lang/String;

.field private static dZ:I

.field private static da:I

.field private static db:I

.field private static dc:I

.field private static dd:I

.field private static de:I

.field private static df:I

.field private static dg:I

.field private static dh:I

.field private static di:I

.field private static dj:I

.field private static dk:I

.field private static dl:I

.field private static dm:I

.field private static dn:I

.field private static dq:Z

.field private static ea:I

.field private static eb:I

.field private static ec:I

.field private static ed:I

.field private static ef:Ljava/lang/String;

.field private static eg:I

.field private static ei:F

.field private static volatile ej:Ljava/lang/ref/WeakReference;


# instance fields
.field protected A:I

.field protected B:I

.field protected C:I

.field protected D:I

.field protected E:I

.field protected F:I

.field protected G:I

.field protected H:I

.field protected I:I

.field protected J:I

.field protected K:Landroid/graphics/drawable/Drawable;

.field protected L:Landroid/graphics/drawable/Drawable;

.field protected M:Landroid/graphics/drawable/Drawable;

.field protected N:Landroid/graphics/drawable/Drawable;

.field protected O:Landroid/graphics/drawable/Drawable;

.field protected P:Landroid/graphics/Bitmap;

.field protected Q:Landroid/graphics/drawable/ColorDrawable;

.field protected T:Landroid/os/Handler;

.field U:Z

.field V:Z

.field protected a:Landroid/text/format/Time;

.field aa:Ljava/lang/String;

.field protected ab:I

.field ac:I

.field ad:I

.field ae:[Ljava/lang/String;

.field protected af:Landroid/graphics/drawable/Drawable;

.field protected ag:Landroid/graphics/drawable/Drawable;

.field protected b:I

.field protected c:I

.field protected d:I

.field private dA:I

.field private dB:[I

.field private dC:[[J

.field private dD:[[J

.field private dE:I

.field private dF:Ljava/lang/String;

.field private dG:Ljava/lang/String;

.field private dH:Z

.field private dJ:Z

.field private dK:I

.field private dL:I

.field private dM:I

.field private dN:I

.field private dO:I

.field private dP:I

.field private dQ:I

.field private dR:I

.field private dS:I

.field private dT:I

.field private dU:Z

.field private dV:Z

.field private dW:Lcom/android/calendar/al;

.field private dX:Lcom/android/calendar/AllInOneActivity;

.field private dY:Landroid/graphics/RectF;

.field private do:I

.field private dp:I

.field private dr:I

.field private ds:F

.field private dt:F

.field private du:Z

.field private dv:Lcom/android/calendar/month/bb;

.field private dw:I

.field private dx:Ljava/util/Set;

.field private dy:Ljava/util/Set;

.field private dz:[[Lcom/android/calendar/month/bw;

.field protected e:Z

.field private ee:Z

.field private eh:Landroid/animation/ValueAnimator;

.field private ek:Lcom/android/calendar/month/bx;

.field private el:Ljava/lang/Runnable;

.field private em:Ljava/lang/Runnable;

.field private en:Ljava/lang/Runnable;

.field private eo:Landroid/animation/Animator$AnimatorListener;

.field protected f:Z

.field protected g:Landroid/text/TextPaint;

.field protected h:Landroid/text/TextPaint;

.field protected i:Landroid/text/TextPaint;

.field protected j:Landroid/text/TextPaint;

.field protected k:Landroid/graphics/Paint;

.field protected l:Landroid/graphics/Paint;

.field protected m:Landroid/graphics/Paint;

.field protected n:Landroid/graphics/Paint;

.field protected o:Landroid/graphics/Paint;

.field protected p:I

.field protected q:I

.field protected r:I

.field protected s:I

.field protected t:I

.field protected u:I

.field protected v:I

.field protected w:I

.field protected x:I

.field protected y:I

.field protected z:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 100
    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/month/bo;->bP:I

    .line 102
    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/month/bo;->bQ:I

    .line 104
    const/16 v0, 0xc

    sput v0, Lcom/android/calendar/month/bo;->bR:I

    .line 106
    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/month/bo;->bS:I

    .line 108
    sput v1, Lcom/android/calendar/month/bo;->bT:I

    .line 110
    sput v1, Lcom/android/calendar/month/bo;->bU:I

    .line 112
    sput v2, Lcom/android/calendar/month/bo;->bV:I

    .line 114
    sput v1, Lcom/android/calendar/month/bo;->bW:I

    .line 116
    const/4 v0, 0x6

    sput v0, Lcom/android/calendar/month/bo;->bX:I

    .line 118
    sput v3, Lcom/android/calendar/month/bo;->bY:I

    .line 120
    const/16 v0, 0x18

    sput v0, Lcom/android/calendar/month/bo;->bZ:I

    .line 122
    const/16 v0, 0xb

    sput v0, Lcom/android/calendar/month/bo;->ca:I

    .line 124
    const/16 v0, 0x12

    sput v0, Lcom/android/calendar/month/bo;->cb:I

    .line 126
    sput v5, Lcom/android/calendar/month/bo;->cc:I

    .line 128
    sput v3, Lcom/android/calendar/month/bo;->cd:I

    .line 130
    sput v5, Lcom/android/calendar/month/bo;->ce:I

    .line 132
    sput v5, Lcom/android/calendar/month/bo;->cf:I

    .line 134
    sput v2, Lcom/android/calendar/month/bo;->cg:I

    .line 136
    const/16 v0, 0xc

    sput v0, Lcom/android/calendar/month/bo;->ch:I

    .line 138
    const/4 v0, 0x5

    sput v0, Lcom/android/calendar/month/bo;->ci:I

    .line 140
    sput v3, Lcom/android/calendar/month/bo;->cj:I

    .line 142
    sput v5, Lcom/android/calendar/month/bo;->ck:I

    .line 144
    const/4 v0, 0x5

    sput v0, Lcom/android/calendar/month/bo;->cl:I

    .line 146
    const/4 v0, -0x2

    sput v0, Lcom/android/calendar/month/bo;->cm:I

    .line 148
    const/4 v0, 0x7

    sput v0, Lcom/android/calendar/month/bo;->cn:I

    .line 150
    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/month/bo;->co:I

    .line 152
    sput v1, Lcom/android/calendar/month/bo;->cp:I

    .line 154
    sput v4, Lcom/android/calendar/month/bo;->cq:I

    .line 156
    sput v4, Lcom/android/calendar/month/bo;->cr:I

    .line 158
    sput v4, Lcom/android/calendar/month/bo;->cs:I

    .line 160
    sput v1, Lcom/android/calendar/month/bo;->ct:I

    .line 162
    sput v4, Lcom/android/calendar/month/bo;->cu:I

    .line 177
    sput v1, Lcom/android/calendar/month/bo;->cB:I

    .line 179
    sput v1, Lcom/android/calendar/month/bo;->cC:I

    .line 181
    sput v1, Lcom/android/calendar/month/bo;->cD:I

    .line 183
    sput v1, Lcom/android/calendar/month/bo;->cE:I

    .line 185
    sput v1, Lcom/android/calendar/month/bo;->cF:I

    .line 189
    sput v1, Lcom/android/calendar/month/bo;->cG:I

    .line 200
    sput v1, Lcom/android/calendar/month/bo;->cL:I

    .line 202
    sput v1, Lcom/android/calendar/month/bo;->cM:I

    .line 204
    sput v1, Lcom/android/calendar/month/bo;->cN:I

    .line 206
    const/16 v0, 0x11

    sput v0, Lcom/android/calendar/month/bo;->cO:I

    .line 208
    const/16 v0, 0x11

    sput v0, Lcom/android/calendar/month/bo;->cP:I

    .line 210
    const/16 v0, 0x8

    sput v0, Lcom/android/calendar/month/bo;->cQ:I

    .line 212
    const/16 v0, 0x8

    sput v0, Lcom/android/calendar/month/bo;->cR:I

    .line 214
    const/4 v0, 0x5

    sput v0, Lcom/android/calendar/month/bo;->cS:I

    .line 216
    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/month/bo;->cT:I

    .line 218
    const/16 v0, 0x22

    sput v0, Lcom/android/calendar/month/bo;->cU:I

    .line 220
    sput v2, Lcom/android/calendar/month/bo;->cV:I

    .line 222
    sput v2, Lcom/android/calendar/month/bo;->cW:I

    .line 224
    sput v2, Lcom/android/calendar/month/bo;->cX:I

    .line 227
    const/16 v0, 0x14

    sput v0, Lcom/android/calendar/month/bo;->cY:I

    .line 229
    sput v1, Lcom/android/calendar/month/bo;->cZ:I

    .line 231
    sput v3, Lcom/android/calendar/month/bo;->da:I

    .line 233
    sput v1, Lcom/android/calendar/month/bo;->db:I

    .line 236
    const/16 v0, 0x14

    sput v0, Lcom/android/calendar/month/bo;->dc:I

    .line 238
    const/16 v0, 0x14

    sput v0, Lcom/android/calendar/month/bo;->dd:I

    .line 240
    const/16 v0, 0xd

    sput v0, Lcom/android/calendar/month/bo;->de:I

    .line 242
    const/16 v0, 0xd

    sput v0, Lcom/android/calendar/month/bo;->df:I

    .line 246
    sput v1, Lcom/android/calendar/month/bo;->dg:I

    .line 248
    sput v1, Lcom/android/calendar/month/bo;->dh:I

    .line 250
    const/16 v0, 0x5d

    sput v0, Lcom/android/calendar/month/bo;->di:I

    .line 252
    sput v1, Lcom/android/calendar/month/bo;->dj:I

    .line 254
    sput v1, Lcom/android/calendar/month/bo;->dk:I

    .line 256
    sput v1, Lcom/android/calendar/month/bo;->dl:I

    .line 258
    sput v1, Lcom/android/calendar/month/bo;->dm:I

    .line 260
    sput v1, Lcom/android/calendar/month/bo;->dn:I

    .line 270
    sput-boolean v1, Lcom/android/calendar/month/bo;->dq:Z

    .line 372
    sput v1, Lcom/android/calendar/month/bo;->R:I

    .line 374
    sput v1, Lcom/android/calendar/month/bo;->S:I

    .line 403
    sput-boolean v1, Lcom/android/calendar/month/bo;->W:Z

    .line 417
    const-string v0, "VerificationLog"

    sput-object v0, Lcom/android/calendar/month/bo;->dI:Ljava/lang/String;

    .line 459
    const/16 v0, 0x12

    sput v0, Lcom/android/calendar/month/bo;->dZ:I

    .line 460
    const/16 v0, 0x1b

    sput v0, Lcom/android/calendar/month/bo;->ea:I

    .line 461
    sput v1, Lcom/android/calendar/month/bo;->eb:I

    .line 462
    sput v1, Lcom/android/calendar/month/bo;->ec:I

    .line 463
    sput v1, Lcom/android/calendar/month/bo;->ed:I

    .line 466
    sput v1, Lcom/android/calendar/month/bo;->eg:I

    .line 470
    sput-boolean v1, Lcom/android/calendar/month/bo;->ah:Z

    .line 472
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/android/calendar/month/bo;->ei:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v2, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 478
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cd;-><init>(Landroid/content/Context;)V

    .line 272
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->a:Landroid/text/format/Time;

    .line 274
    iput v4, p0, Lcom/android/calendar/month/bo;->b:I

    .line 276
    iput v4, p0, Lcom/android/calendar/month/bo;->c:I

    .line 278
    iput v3, p0, Lcom/android/calendar/month/bo;->d:I

    .line 280
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->e:Z

    .line 282
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->f:Z

    .line 362
    iput v1, p0, Lcom/android/calendar/month/bo;->dr:I

    .line 364
    iput v2, p0, Lcom/android/calendar/month/bo;->ds:F

    .line 366
    iput v2, p0, Lcom/android/calendar/month/bo;->dt:F

    .line 368
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->du:Z

    .line 376
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    .line 378
    iput v1, p0, Lcom/android/calendar/month/bo;->dw:I

    .line 380
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->U:Z

    .line 382
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->V:Z

    .line 392
    const/4 v0, 0x7

    filled-new-array {v0, v5}, [I

    move-result-object v0

    const-class v2, Lcom/android/calendar/month/bw;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/android/calendar/month/bw;

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    .line 394
    iput v1, p0, Lcom/android/calendar/month/bo;->dA:I

    .line 396
    iget v0, p0, Lcom/android/calendar/month/bo;->ba:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dB:[I

    .line 398
    iget v0, p0, Lcom/android/calendar/month/bo;->ba:I

    filled-new-array {v0, v3}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[J

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dC:[[J

    .line 400
    iget v0, p0, Lcom/android/calendar/month/bo;->ba:I

    filled-new-array {v0, v3}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[J

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    .line 405
    iput v4, p0, Lcom/android/calendar/month/bo;->dE:I

    .line 413
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->dH:Z

    .line 415
    iput v1, p0, Lcom/android/calendar/month/bo;->ab:I

    .line 424
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->dJ:Z

    .line 426
    iput v5, p0, Lcom/android/calendar/month/bo;->dK:I

    .line 428
    const/16 v0, 0xc

    iput v0, p0, Lcom/android/calendar/month/bo;->dL:I

    .line 430
    iput v3, p0, Lcom/android/calendar/month/bo;->dM:I

    .line 443
    iput v1, p0, Lcom/android/calendar/month/bo;->dR:I

    .line 456
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dY:Landroid/graphics/RectF;

    .line 464
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->ee:Z

    .line 3511
    new-instance v0, Lcom/android/calendar/month/br;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/br;-><init>(Lcom/android/calendar/month/bo;)V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->el:Ljava/lang/Runnable;

    .line 3540
    new-instance v0, Lcom/android/calendar/month/bs;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/bs;-><init>(Lcom/android/calendar/month/bo;)V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->em:Ljava/lang/Runnable;

    .line 3561
    new-instance v0, Lcom/android/calendar/month/bt;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/bt;-><init>(Lcom/android/calendar/month/bo;)V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->en:Ljava/lang/Runnable;

    .line 3663
    new-instance v0, Lcom/android/calendar/month/bv;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/bv;-><init>(Lcom/android/calendar/month/bo;)V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->eo:Landroid/animation/Animator$AnimatorListener;

    .line 479
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->k(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/bo;->U:Z

    .line 480
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->l(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/bo;->V:Z

    .line 481
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dW:Lcom/android/calendar/al;

    .line 482
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/month/bo;->dV:Z

    .line 483
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/bo;->dU:Z

    .line 484
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dX:Lcom/android/calendar/AllInOneActivity;

    .line 485
    return-void

    :cond_0
    move v0, v1

    .line 482
    goto :goto_0
.end method

.method private a(IZ)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3499
    if-nez p2, :cond_0

    .line 3508
    :goto_0
    return p1

    .line 3503
    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 3504
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 3505
    aget v1, v0, v3

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v1, v2

    aput v1, v0, v3

    .line 3508
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result p1

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/month/bo;I)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/android/calendar/month/bo;->dK:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/month/bo;->dK:I

    return v0
.end method

.method private a(Z)Landroid/text/format/Time;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 3602
    if-eqz p1, :cond_0

    move v0, v1

    .line 3604
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 3605
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 3609
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/month/bo;->dW:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 3610
    iget v3, v2, Landroid/text/format/Time;->month:I

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v3

    iput v0, v2, Landroid/text/format/Time;->month:I

    .line 3611
    invoke-static {v2}, Lcom/android/calendar/f/m;->a(Landroid/text/format/Time;)V

    .line 3612
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 3613
    invoke-static {v2}, Lcom/android/calendar/hj;->b(Landroid/text/format/Time;)V

    .line 3614
    return-object v2

    .line 3602
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 3607
    :cond_1
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/month/bo;)Lcom/android/calendar/month/bb;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/month/bo;Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bb;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    return-object p1
.end method

.method private a(Landroid/graphics/Canvas;IIIIIZ)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1630
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_4

    move v0, v1

    .line 1633
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aF:Lcom/android/calendar/hl;

    iget-object v3, v3, Lcom/android/calendar/hl;->d:[Z

    aget-boolean v3, v3, p5

    if-eqz v3, :cond_5

    .line 1638
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/android/calendar/month/bo;->ef:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/month/bo;->aF:Lcom/android/calendar/hl;

    iget-object v4, v4, Lcom/android/calendar/hl;->c:[Ljava/lang/String;

    aget-object v4, v4, p5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1647
    :goto_1
    iget-boolean v4, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-eqz v4, :cond_6

    .line 1648
    sget v4, Lcom/android/calendar/month/bo;->ed:I

    .line 1655
    :goto_2
    iget-object v5, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    if-eqz v5, :cond_9

    .line 1656
    iget-boolean v5, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-eqz v5, :cond_0

    .line 1657
    iget-object v5, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    sget v6, Lcom/android/calendar/month/bo;->ea:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1659
    :cond_0
    iget-object v5, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v5, v5

    div-int/lit8 v5, v5, 0x2

    sub-int v5, p2, v5

    .line 1662
    :goto_3
    iget-boolean v6, p0, Lcom/android/calendar/month/bo;->aU:Z

    if-eqz v6, :cond_8

    .line 1663
    sub-int v0, p5, v0

    if-ne p6, v0, :cond_8

    .line 1664
    int-to-float v0, v5

    int-to-float v6, v4

    iget-object v7, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1669
    :goto_4
    if-eqz p7, :cond_3

    if-eqz v2, :cond_3

    .line 1670
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-nez v0, :cond_1

    if-eqz p5, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_3

    if-ne p5, v1, :cond_3

    .line 1671
    :cond_2
    int-to-float v0, v5

    int-to-float v1, v4

    iget-object v2, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1674
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 1630
    goto :goto_0

    .line 1641
    :cond_5
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aF:Lcom/android/calendar/hl;

    iget-object v3, v3, Lcom/android/calendar/hl;->c:[Ljava/lang/String;

    aget-object v3, v3, p5

    goto :goto_1

    .line 1649
    :cond_6
    iget-boolean v4, p0, Lcom/android/calendar/month/bo;->e:Z

    if-eqz v4, :cond_7

    .line 1650
    sget v4, Lcom/android/calendar/month/bo;->eb:I

    goto :goto_2

    .line 1652
    :cond_7
    sget v4, Lcom/android/calendar/month/bo;->ec:I

    add-int/2addr v4, p3

    goto :goto_2

    :cond_8
    move v2, v1

    goto :goto_4

    :cond_9
    move v5, v2

    goto :goto_3
.end method

.method private a(Landroid/graphics/Canvas;JJIIZLandroid/graphics/Paint;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 1887
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 1889
    if-nez v0, :cond_1

    .line 1955
    :cond_0
    :goto_0
    return-void

    .line 1893
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1894
    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 1896
    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/android/calendar/month/k;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1897
    check-cast v0, Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->h:Landroid/util/LruCache;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1899
    if-nez v0, :cond_2

    .line 1900
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0, p4, p5}, Lcom/android/calendar/gx;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 1901
    invoke-static {v0, v3, v3}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1902
    if-eqz v0, :cond_2

    .line 1903
    check-cast v1, Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->h:Landroid/util/LruCache;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1906
    :cond_2
    if-eqz v0, :cond_0

    .line 1911
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1913
    invoke-virtual {p0, p2, p3}, Lcom/android/calendar/month/bo;->a(J)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1914
    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 1915
    iput p6, v1, Landroid/graphics/Rect;->left:I

    .line 1916
    iget v2, p0, Lcom/android/calendar/month/bo;->aS:I

    sget v3, Lcom/android/calendar/month/bo;->bY:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1917
    iput p7, v1, Landroid/graphics/Rect;->right:I

    .line 1930
    :goto_1
    invoke-virtual {p0, p2, p3}, Lcom/android/calendar/month/bo;->a(J)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1931
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 1932
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 1934
    if-nez p8, :cond_3

    .line 1935
    invoke-virtual {v2}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->c(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 1938
    :cond_3
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/ColorDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1939
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 1918
    :cond_4
    int-to-long v2, v3

    cmp-long v2, p2, v2

    if-nez v2, :cond_5

    .line 1919
    iget v2, p0, Lcom/android/calendar/month/bo;->aS:I

    sget v3, Lcom/android/calendar/month/bo;->bY:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1920
    iput p7, v1, Landroid/graphics/Rect;->right:I

    .line 1921
    iget v2, v1, Landroid/graphics/Rect;->right:I

    sget v3, Lcom/android/calendar/month/bo;->cY:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1922
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    sget v3, Lcom/android/calendar/month/bo;->cY:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 1924
    :cond_5
    iget v2, p0, Lcom/android/calendar/month/bo;->aS:I

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1925
    iput p7, v1, Landroid/graphics/Rect;->right:I

    .line 1926
    iget v2, v1, Landroid/graphics/Rect;->right:I

    sget v3, Lcom/android/calendar/month/bo;->cY:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1927
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    sget v3, Lcom/android/calendar/month/bo;->cY:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 1942
    :cond_6
    if-eqz p8, :cond_8

    .line 1948
    :goto_2
    if-eqz v0, :cond_7

    .line 1949
    invoke-virtual {p1, v0, v5, v1, p9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1952
    :cond_7
    invoke-virtual {p9, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_0

    .line 1945
    :cond_8
    new-instance v2, Landroid/graphics/LightingColorFilter;

    iget v3, p0, Lcom/android/calendar/month/bo;->D:I

    iget v4, p0, Lcom/android/calendar/month/bo;->E:I

    invoke-direct {v2, v3, v4}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {p9, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_2
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 1340
    .line 1343
    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-eqz v0, :cond_2

    .line 1344
    const/4 v2, 0x0

    .line 1346
    sget v1, Lcom/android/calendar/month/h;->y:I

    .line 1347
    sget v0, Lcom/android/calendar/month/h;->z:I

    .line 1348
    sget v3, Lcom/android/calendar/month/h;->y:I

    sget v4, Lcom/android/calendar/month/h;->z:I

    if-le v3, v4, :cond_0

    .line 1349
    sget v1, Lcom/android/calendar/month/h;->z:I

    .line 1350
    sget v0, Lcom/android/calendar/month/h;->y:I

    .line 1354
    :cond_0
    iget v3, p0, Lcom/android/calendar/month/bo;->aN:I

    add-int/2addr v3, v2

    .line 1356
    if-lt v3, v1, :cond_1

    if-gt v3, v0, :cond_1

    .line 1357
    invoke-direct {p0, v2}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v3

    iput v3, p2, Landroid/graphics/Rect;->left:I

    .line 1358
    add-int/lit8 v3, v2, 0x1

    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v3

    iput v3, p2, Landroid/graphics/Rect;->right:I

    .line 1360
    iget-object v3, p0, Lcom/android/calendar/month/bo;->ax:Landroid/graphics/Paint;

    iget v4, p0, Lcom/android/calendar/month/bo;->J:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1361
    iget-object v3, p0, Lcom/android/calendar/month/bo;->ax:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1363
    :cond_1
    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/month/bo;->aM:[Z

    array-length v3, v3

    if-lt v2, v3, :cond_0

    .line 1366
    :cond_2
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Z)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1098
    iget v0, p0, Lcom/android/calendar/month/bo;->ba:I

    .line 1101
    iget-boolean v3, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v3, :cond_4

    .line 1102
    add-int/lit8 v0, v0, 0x1

    move v3, v1

    move v4, v0

    move v0, v1

    .line 1107
    :goto_0
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1108
    new-instance v6, Landroid/graphics/LightingColorFilter;

    iget v7, p0, Lcom/android/calendar/month/bo;->D:I

    iget v8, p0, Lcom/android/calendar/month/bo;->E:I

    invoke-direct {v6, v7, v8}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    .line 1110
    :goto_1
    if-ge v3, v4, :cond_3

    .line 1111
    sub-int v7, v3, v0

    .line 1112
    if-eqz p2, :cond_1

    iget-object v8, p0, Lcom/android/calendar/month/bo;->at:[I

    aget v8, v8, v3

    if-ne v8, v1, :cond_1

    .line 1113
    iput v2, v5, Landroid/graphics/Rect;->top:I

    .line 1114
    iget v8, p0, Lcom/android/calendar/month/bo;->aS:I

    sget v9, Lcom/android/calendar/month/bo;->bY:I

    sub-int/2addr v8, v9

    iput v8, v5, Landroid/graphics/Rect;->bottom:I

    .line 1116
    invoke-direct {p0, v7}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v8

    iput v8, v5, Landroid/graphics/Rect;->left:I

    .line 1117
    add-int/lit8 v7, v7, 0x1

    invoke-direct {p0, v7}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v7

    iput v7, v5, Landroid/graphics/Rect;->right:I

    .line 1119
    iget-object v7, p0, Lcom/android/calendar/month/bo;->af:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    .line 1120
    iget-object v7, p0, Lcom/android/calendar/month/bo;->af:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1121
    iget-object v7, p0, Lcom/android/calendar/month/bo;->af:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1110
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1123
    :cond_1
    if-nez p2, :cond_0

    iget-object v8, p0, Lcom/android/calendar/month/bo;->at:[I

    aget v8, v8, v3

    const/4 v9, 0x2

    if-ne v8, v9, :cond_0

    .line 1124
    iget v8, p0, Lcom/android/calendar/month/bo;->aS:I

    iput v8, v5, Landroid/graphics/Rect;->bottom:I

    .line 1125
    add-int/lit8 v7, v7, 0x1

    invoke-direct {p0, v7}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v7

    iput v7, v5, Landroid/graphics/Rect;->right:I

    .line 1126
    iget v7, v5, Landroid/graphics/Rect;->right:I

    sget v8, Lcom/android/calendar/month/bo;->cY:I

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->left:I

    .line 1127
    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    sget v8, Lcom/android/calendar/month/bo;->cY:I

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->top:I

    .line 1129
    iget-object v7, p0, Lcom/android/calendar/month/bo;->ag:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    .line 1130
    iget-object v7, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v7, v7, v3

    if-nez v7, :cond_2

    .line 1131
    iget-object v7, p0, Lcom/android/calendar/month/bo;->ag:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1135
    :goto_3
    iget-object v7, p0, Lcom/android/calendar/month/bo;->ag:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1136
    iget-object v7, p0, Lcom/android/calendar/month/bo;->ag:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 1133
    :cond_2
    iget-object v7, p0, Lcom/android/calendar/month/bo;->ag:Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_3

    .line 1140
    :cond_3
    return-void

    :cond_4
    move v3, v2

    move v4, v0

    move v0, v2

    goto/16 :goto_0
.end method

.method private a(III)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3570
    iget v2, p0, Lcom/android/calendar/month/bo;->aR:I

    .line 3571
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 3572
    iget-boolean v1, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v4, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v1, v4

    .line 3574
    :goto_0
    sub-int v1, v2, v1

    sub-int/2addr v1, p3

    if-le p1, v1, :cond_0

    const/16 v1, 0xa

    if-le p2, v1, :cond_0

    add-int/lit8 v1, v3, -0xa

    if-ge p2, v1, :cond_0

    .line 3576
    const/4 v0, 0x1

    .line 3578
    :cond_0
    return v0

    :cond_1
    move v1, v0

    .line 3572
    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/high16 v11, 0x41200000    # 10.0f

    const-wide/16 v12, 0x0

    const/4 v10, -0x1

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 2886
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 3041
    :cond_0
    :goto_0
    return v3

    .line 2892
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 2893
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v1, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v0, v1

    .line 2894
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    int-to-float v2, v0

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/bo;->ba:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/bo;->aR:I

    sub-int v0, v2, v0

    iget v2, p0, Lcom/android/calendar/month/bo;->av:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-int v4, v0

    .line 2895
    if-ltz v4, :cond_0

    const/4 v0, 0x6

    if-gt v4, v0, :cond_0

    .line 2899
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2900
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 2901
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2902
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 2903
    if-eqz v0, :cond_4

    move v5, v3

    .line 2904
    :goto_2
    if-ge v5, v7, :cond_4

    .line 2905
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 2906
    iget-wide v8, v2, Lcom/android/calendar/dh;->b:J

    cmp-long v2, v8, v12

    if-gez v2, :cond_2

    .line 2907
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2904
    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 2893
    :cond_3
    iget v0, p0, Lcom/android/calendar/month/bo;->av:I

    goto :goto_1

    .line 2912
    :cond_4
    iget v2, p0, Lcom/android/calendar/month/bo;->aN:I

    add-int/2addr v2, v4

    .line 2914
    new-instance v5, Landroid/text/format/Time;

    iget-object v7, p0, Lcom/android/calendar/month/bo;->be:Ljava/lang/String;

    invoke-direct {v5, v7}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2915
    iget v7, p0, Lcom/android/calendar/month/bo;->aQ:I

    const/16 v8, 0xddc

    if-ne v7, v8, :cond_5

    .line 2917
    const v7, 0x253d8c    # 3.419992E-39f

    if-ne v2, v7, :cond_5

    .line 2918
    const/16 v7, 0x7b2

    invoke-virtual {v5, v6, v3, v7}, Landroid/text/format/Time;->set(III)V

    .line 2919
    invoke-virtual {v5, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 2922
    :cond_5
    invoke-static {v5, v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 2924
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_6
    :goto_3
    :pswitch_0
    move v3, v6

    .line 3041
    goto/16 :goto_0

    .line 2926
    :pswitch_1
    iput-boolean v3, p0, Lcom/android/calendar/month/bo;->du:Z

    .line 2927
    iput v3, p0, Lcom/android/calendar/month/bo;->dr:I

    .line 2928
    iput v10, p0, Lcom/android/calendar/month/bo;->dE:I

    goto :goto_3

    .line 2939
    :pswitch_2
    iget v2, p0, Lcom/android/calendar/month/bo;->dE:I

    if-eq v2, v4, :cond_7

    .line 2941
    iget-object v2, p0, Lcom/android/calendar/month/bo;->em:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 2942
    iget-object v2, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->en:Ljava/lang/Runnable;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2943
    iput-boolean v3, p0, Lcom/android/calendar/month/bo;->du:Z

    .line 2945
    iput v4, p0, Lcom/android/calendar/month/bo;->dE:I

    .line 2947
    iput v3, p0, Lcom/android/calendar/month/bo;->dr:I

    .line 2948
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/android/calendar/month/bo;->ds:F

    .line 2949
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/android/calendar/month/bo;->dt:F

    .line 2953
    :cond_7
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->du:Z

    if-nez v2, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_8

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 2954
    :cond_8
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->du:Z

    if-nez v2, :cond_6

    .line 2955
    iget v2, p0, Lcom/android/calendar/month/bo;->dr:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/calendar/month/bo;->dr:I

    .line 2957
    iget v2, p0, Lcom/android/calendar/month/bo;->dr:I

    if-le v2, v6, :cond_a

    .line 2959
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 2960
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 2962
    iget v7, p0, Lcom/android/calendar/month/bo;->ds:F

    sub-float v7, v2, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpl-float v7, v7, v11

    if-gtz v7, :cond_9

    iget v7, p0, Lcom/android/calendar/month/bo;->dt:F

    sub-float v7, v5, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpl-float v7, v7, v11

    if-lez v7, :cond_a

    .line 2964
    :cond_9
    iput v3, p0, Lcom/android/calendar/month/bo;->dr:I

    .line 2965
    iput v2, p0, Lcom/android/calendar/month/bo;->ds:F

    .line 2966
    iput v5, p0, Lcom/android/calendar/month/bo;->dt:F

    .line 2970
    :cond_a
    iget v2, p0, Lcom/android/calendar/month/bo;->dr:I

    if-le v2, v6, :cond_6

    .line 2971
    iget-object v2, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->em:Ljava/lang/Runnable;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2972
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 2973
    invoke-virtual {p0, v7}, Lcom/android/calendar/month/bo;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2974
    iput-boolean v6, p0, Lcom/android/calendar/month/bo;->du:Z

    .line 2975
    iput v4, p0, Lcom/android/calendar/month/bo;->dw:I

    .line 2977
    iput v3, p0, Lcom/android/calendar/month/bo;->b:I

    .line 2978
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->invalidate()V

    .line 2979
    iget-object v2, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->en:Ljava/lang/Runnable;

    const-wide/16 v8, 0x64

    invoke-virtual {v2, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2980
    iput v3, p0, Lcom/android/calendar/month/bo;->b:I

    .line 2981
    iget-object v2, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->en:Ljava/lang/Runnable;

    const-wide/16 v8, 0x12c

    invoke-virtual {v2, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2983
    iget-object v2, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    if-eqz v2, :cond_b

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-ne v2, v6, :cond_b

    iget-object v2, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/dz;->o(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2985
    iget-object v2, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    new-instance v5, Lcom/android/calendar/month/bq;

    invoke-direct {v5, p0}, Lcom/android/calendar/month/bq;-><init>(Lcom/android/calendar/month/bo;)V

    invoke-virtual {v2, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2995
    :cond_b
    new-instance v2, Lcom/android/calendar/month/bb;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-direct {v2, v5, v0, v1}, Lcom/android/calendar/month/bb;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v2, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    .line 2996
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2997
    const-string v1, "finger_hover"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2998
    iget-object v1, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/bb;->setArguments(Landroid/os/Bundle;)V

    .line 3000
    iget-object v0, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->en:Ljava/lang/Runnable;

    const-wide/16 v8, 0x12c

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3002
    iget-object v0, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->el:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3004
    iput v3, p0, Lcom/android/calendar/month/bo;->dr:I

    .line 3006
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/bo;->dp:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x7

    sput v0, Lcom/android/calendar/month/bo;->R:I

    .line 3007
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getHeight()I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->S:I

    .line 3009
    sget v0, Lcom/android/calendar/month/bo;->R:I

    mul-int/2addr v0, v4

    iget v1, p0, Lcom/android/calendar/month/bo;->dp:I

    add-int v2, v0, v1

    .line 3011
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    :goto_4
    iput v0, p0, Lcom/android/calendar/month/bo;->dp:I

    .line 3012
    iget v0, p0, Lcom/android/calendar/month/bo;->aR:I

    iget v1, p0, Lcom/android/calendar/month/bo;->av:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 3013
    iget v1, p0, Lcom/android/calendar/month/bo;->dp:I

    sub-int/2addr v0, v1

    .line 3015
    iget v1, p0, Lcom/android/calendar/month/bo;->ba:I

    div-int v5, v0, v1

    .line 3017
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    if-eqz v0, :cond_6

    .line 3018
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    iget v3, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;IIIIZ)V

    goto/16 :goto_3

    :cond_c
    move v0, v3

    .line 3011
    goto :goto_4

    .line 3027
    :pswitch_3
    iput-boolean v3, p0, Lcom/android/calendar/month/bo;->du:Z

    .line 3028
    iput v10, p0, Lcom/android/calendar/month/bo;->dE:I

    .line 3030
    iput v3, p0, Lcom/android/calendar/month/bo;->dr:I

    .line 3031
    iput v10, p0, Lcom/android/calendar/month/bo;->b:I

    .line 3032
    iget-object v0, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->en:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3033
    iget-object v0, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->el:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3034
    iget-object v0, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->em:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3035
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->invalidate()V

    goto/16 :goto_3

    .line 2924
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/android/calendar/month/bo;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/android/calendar/month/bo;->dJ:Z

    return p1
.end method

.method static synthetic b(F)F
    .locals 0

    .prologue
    .line 84
    sput p0, Lcom/android/calendar/month/bo;->ei:F

    return p0
.end method

.method private b(I)I
    .locals 3

    .prologue
    .line 1230
    iget v1, p0, Lcom/android/calendar/month/bo;->aR:I

    .line 1232
    const/4 v0, 0x0

    .line 1233
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v2, :cond_0

    .line 1234
    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v2, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v0, v2

    .line 1235
    sub-int/2addr v1, v0

    .line 1238
    :cond_0
    mul-int/2addr v1, p1

    iget v2, p0, Lcom/android/calendar/month/bo;->ba:I

    div-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1239
    return v0
.end method

.method static synthetic b(Lcom/android/calendar/month/bo;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/android/calendar/month/bo;->dM:I

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/month/bo;I)I
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    return v0
.end method

.method private b(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    .line 1369
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aU:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-nez v0, :cond_0

    .line 1371
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 1372
    iget v0, p0, Lcom/android/calendar/month/bo;->G:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1373
    iget v0, p0, Lcom/android/calendar/month/bo;->dK:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1374
    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1375
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dJ:Z

    if-eqz v0, :cond_1

    .line 1376
    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1381
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-nez v0, :cond_4

    .line 1382
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    div-int/lit8 v0, v0, 0x2

    .line 1383
    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    invoke-direct {p0, v2}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1385
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->e:Z

    if-eqz v2, :cond_2

    .line 1386
    sget v2, Lcom/android/calendar/month/bo;->cN:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    iput v2, p2, Landroid/graphics/Rect;->top:I

    .line 1387
    sget v2, Lcom/android/calendar/month/bo;->cN:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 1388
    iget v0, p0, Lcom/android/calendar/month/bo;->aX:I

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    div-int/lit8 v2, v1, 0x2

    add-int/2addr v0, v2

    sget v2, Lcom/android/calendar/month/bo;->cN:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 1389
    iget v0, p0, Lcom/android/calendar/month/bo;->aX:I

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/month/bo;->cN:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 1390
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->cN:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1426
    :cond_0
    :goto_1
    return-void

    .line 1378
    :cond_1
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    goto :goto_0

    .line 1392
    :cond_2
    iget v0, p0, Lcom/android/calendar/month/bo;->d:I

    if-ne v0, v3, :cond_3

    .line 1393
    sget v0, Lcom/android/calendar/month/bo;->cz:I

    iput v0, p2, Landroid/graphics/Rect;->top:I

    .line 1394
    iget v0, p2, Landroid/graphics/Rect;->top:I

    sget v1, Lcom/android/calendar/month/bo;->cG:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 1395
    iget v0, p0, Lcom/android/calendar/month/bo;->aX:I

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    sget v1, Lcom/android/calendar/month/bo;->cz:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 1396
    iget v0, p2, Landroid/graphics/Rect;->left:I

    sget v1, Lcom/android/calendar/month/bo;->cG:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 1397
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->cG:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1399
    :cond_3
    iget v0, p0, Lcom/android/calendar/month/bo;->aX:I

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    sget v1, Lcom/android/calendar/month/bo;->cK:I

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/month/bo;->cH:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->ei:F

    sub-float v2, v4, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 1400
    sget v0, Lcom/android/calendar/month/bo;->cz:I

    sget v1, Lcom/android/calendar/month/bo;->cI:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->ei:F

    sub-float v2, v4, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->top:I

    .line 1401
    iget v0, p2, Landroid/graphics/Rect;->left:I

    sget v1, Lcom/android/calendar/month/bo;->cH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 1402
    iget v0, p2, Landroid/graphics/Rect;->top:I

    sget v1, Lcom/android/calendar/month/bo;->cI:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 1403
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawOval(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 1407
    :cond_4
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    div-int/lit8 v0, v0, 0x2

    .line 1408
    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    invoke-direct {p0, v2}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1410
    iget v2, p0, Lcom/android/calendar/month/bo;->d:I

    if-ne v2, v3, :cond_5

    .line 1411
    sget v2, Lcom/android/calendar/month/bo;->cM:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->top:I

    .line 1412
    iget v0, p2, Landroid/graphics/Rect;->top:I

    sget v2, Lcom/android/calendar/month/bo;->cM:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 1413
    iget v0, p0, Lcom/android/calendar/month/bo;->aX:I

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/month/bo;->cM:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 1414
    iget v0, p2, Landroid/graphics/Rect;->left:I

    sget v1, Lcom/android/calendar/month/bo;->cM:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 1415
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->cM:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 1417
    :cond_5
    sget v2, Lcom/android/calendar/month/bo;->cC:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sub-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->top:I

    .line 1418
    iget v0, p2, Landroid/graphics/Rect;->top:I

    sget v2, Lcom/android/calendar/month/bo;->cC:I

    int-to-float v2, v2

    sget v3, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 1419
    iget v0, p0, Lcom/android/calendar/month/bo;->aX:I

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/month/bo;->cB:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 1420
    iget v0, p2, Landroid/graphics/Rect;->left:I

    sget v1, Lcom/android/calendar/month/bo;->cB:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->ei:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 1421
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, p2, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawOval(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method private b(Landroid/graphics/Canvas;Z)V
    .locals 15

    .prologue
    .line 1835
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1883
    :cond_0
    return-void

    .line 1839
    :cond_1
    const/4 v0, 0x0

    .line 1840
    iget-boolean v1, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v1, :cond_7

    .line 1841
    const/4 v0, 0x1

    move v13, v0

    .line 1846
    :goto_0
    if-eqz p2, :cond_3

    .line 1847
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    move-object v10, v0

    .line 1852
    :goto_1
    const/4 v1, -0x1

    .line 1853
    new-instance v9, Landroid/graphics/Paint;

    const/4 v0, 0x2

    invoke-direct {v9, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 1855
    array-length v14, v10

    const/4 v0, 0x0

    move v11, v0

    move v0, v1

    :goto_2
    if-ge v11, v14, :cond_0

    aget-object v1, v10, v11

    .line 1856
    add-int/lit8 v12, v0, 0x1

    .line 1858
    sget v0, Lcom/android/calendar/month/bo;->cZ:I

    aget-wide v2, v1, v0

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 1855
    :cond_2
    :goto_3
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    move v0, v12

    goto :goto_2

    .line 1849
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dC:[[J

    move-object v10, v0

    goto :goto_1

    .line 1862
    :cond_4
    invoke-direct {p0, v12}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v6

    .line 1863
    add-int/lit8 v0, v12, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v7

    .line 1866
    add-int v0, v12, v13

    .line 1867
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    array-length v2, v2

    if-le v0, v2, :cond_6

    .line 1868
    const/4 v8, 0x0

    .line 1873
    :goto_4
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->bJ:Z

    if-eqz v2, :cond_5

    if-nez p2, :cond_5

    .line 1874
    iget-object v2, p0, Lcom/android/calendar/month/bo;->at:[I

    aget v0, v2, v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    .line 1879
    :cond_5
    sget v0, Lcom/android/calendar/month/bo;->cZ:I

    aget-wide v2, v1, v0

    .line 1880
    sget v0, Lcom/android/calendar/month/bo;->da:I

    aget-wide v4, v1, v0

    move-object v0, p0

    move-object/from16 v1, p1

    .line 1881
    invoke-direct/range {v0 .. v9}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;JJIIZLandroid/graphics/Paint;)V

    goto :goto_3

    .line 1870
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    add-int v3, v12, v13

    aget-boolean v8, v2, v3

    goto :goto_4

    :cond_7
    move v13, v0

    goto :goto_0
.end method

.method private b(III)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3583
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 3584
    iget-boolean v1, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v3, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v1, v3

    .line 3586
    :goto_0
    add-int/2addr v1, p3

    if-ge p1, v1, :cond_0

    const/16 v1, 0xa

    if-le p2, v1, :cond_0

    add-int/lit8 v1, v2, -0xa

    if-ge p2, v1, :cond_0

    .line 3587
    const/4 v0, 0x1

    .line 3589
    :cond_0
    return v0

    :cond_1
    move v1, v0

    .line 3584
    goto :goto_0
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 3045
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 3046
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v1, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v0, v1

    .line 3047
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    int-to-float v2, v0

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/bo;->ba:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/bo;->aR:I

    sub-int v0, v2, v0

    iget v2, p0, Lcom/android/calendar/month/bo;->av:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-int v4, v0

    .line 3048
    if-ltz v4, :cond_0

    const/4 v0, 0x6

    if-le v4, v0, :cond_3

    .line 3049
    :cond_0
    sget-boolean v0, Lcom/android/calendar/ek;->a:Z

    if-nez v0, :cond_1

    .line 3050
    invoke-static {v6}, Lcom/android/calendar/ek;->a(I)V

    .line 3179
    :cond_1
    :goto_1
    return v5

    .line 3046
    :cond_2
    iget v0, p0, Lcom/android/calendar/month/bo;->av:I

    goto :goto_0

    .line 3055
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3056
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 3058
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3059
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 3060
    if-eqz v0, :cond_5

    move v3, v5

    .line 3061
    :goto_2
    if-ge v3, v7, :cond_5

    .line 3062
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    .line 3063
    iget-wide v8, v2, Lcom/android/calendar/dh;->b:J

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-gez v2, :cond_4

    .line 3064
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 3061
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 3068
    :cond_5
    iget v3, p0, Lcom/android/calendar/month/bo;->dQ:I

    .line 3069
    iget v2, p0, Lcom/android/calendar/month/bo;->dQ:I

    .line 3071
    iget-boolean v7, p0, Lcom/android/calendar/month/bo;->dV:Z

    if-eqz v7, :cond_6

    iget-boolean v7, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v7, :cond_6

    .line 3072
    iget-object v7, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/calendar/hj;->i(Landroid/content/Context;)I

    move-result v7

    .line 3073
    const/4 v8, 0x2

    if-ne v7, v8, :cond_8

    .line 3074
    mul-int/lit8 v2, v2, 0x2

    .line 3075
    iget-boolean v7, p0, Lcom/android/calendar/month/bo;->dV:Z

    if-eqz v7, :cond_6

    iget-boolean v7, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v7, :cond_6

    .line 3076
    div-int/lit8 v2, v2, 0x4

    .line 3084
    :cond_6
    :goto_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    .line 3085
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v8, v8

    .line 3087
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :cond_7
    :goto_4
    :pswitch_0
    move v5, v6

    .line 3179
    goto :goto_1

    .line 3079
    :cond_8
    if-ne v7, v6, :cond_6

    iget-boolean v7, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v7, :cond_6

    .line 3080
    iget v7, p0, Lcom/android/calendar/month/bo;->ab:I

    sub-int/2addr v2, v7

    goto :goto_3

    .line 3089
    :pswitch_1
    iput-boolean v5, p0, Lcom/android/calendar/month/bo;->du:Z

    .line 3090
    iput v5, p0, Lcom/android/calendar/month/bo;->dr:I

    goto :goto_4

    .line 3095
    :pswitch_2
    iget-boolean v9, p0, Lcom/android/calendar/month/bo;->dU:Z

    if-ne v9, v6, :cond_d

    .line 3096
    invoke-direct {p0, v7, v8, v2}, Lcom/android/calendar/month/bo;->a(III)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3097
    const/16 v0, 0xd

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 3098
    iget v0, p0, Lcom/android/calendar/month/bo;->dR:I

    iget v1, p0, Lcom/android/calendar/month/bo;->dS:I

    if-le v0, v1, :cond_9

    .line 3099
    iget-object v0, p0, Lcom/android/calendar/month/bo;->em:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 3100
    iput v5, p0, Lcom/android/calendar/month/bo;->dR:I

    .line 3101
    invoke-direct {p0}, Lcom/android/calendar/month/bo;->j()V

    :goto_5
    move v5, v6

    .line 3105
    goto/16 :goto_1

    .line 3103
    :cond_9
    iget v0, p0, Lcom/android/calendar/month/bo;->dR:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/bo;->dR:I

    goto :goto_5

    .line 3106
    :cond_a
    invoke-direct {p0, v7, v8, v3}, Lcom/android/calendar/month/bo;->b(III)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 3107
    const/16 v0, 0x11

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 3108
    iget v0, p0, Lcom/android/calendar/month/bo;->dR:I

    iget v1, p0, Lcom/android/calendar/month/bo;->dS:I

    if-le v0, v1, :cond_b

    .line 3109
    iget-object v0, p0, Lcom/android/calendar/month/bo;->em:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 3110
    iput v5, p0, Lcom/android/calendar/month/bo;->dR:I

    .line 3111
    invoke-direct {p0}, Lcom/android/calendar/month/bo;->i()V

    :goto_6
    move v5, v6

    .line 3115
    goto/16 :goto_1

    .line 3113
    :cond_b
    iget v0, p0, Lcom/android/calendar/month/bo;->dR:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/bo;->dR:I

    goto :goto_6

    .line 3116
    :cond_c
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->du:Z

    if-nez v2, :cond_d

    sget-boolean v2, Lcom/android/calendar/ek;->a:Z

    if-nez v2, :cond_d

    .line 3117
    invoke-static {v6}, Lcom/android/calendar/ek;->a(I)V

    .line 3121
    :cond_d
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->du:Z

    if-nez v2, :cond_e

    .line 3122
    iget v2, p0, Lcom/android/calendar/month/bo;->dr:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/calendar/month/bo;->dr:I

    .line 3125
    :cond_e
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->du:Z

    if-nez v2, :cond_7

    iget v2, p0, Lcom/android/calendar/month/bo;->dr:I

    iget v3, p0, Lcom/android/calendar/month/bo;->dT:I

    if-le v2, v3, :cond_7

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_f

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 3127
    :cond_f
    iput-boolean v6, p0, Lcom/android/calendar/month/bo;->du:Z

    .line 3128
    new-instance v2, Lcom/android/calendar/month/bb;

    iget-object v3, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-direct {v2, v3, v0, v1}, Lcom/android/calendar/month/bb;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v2, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    .line 3130
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    if-eqz v0, :cond_11

    .line 3133
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v0, :cond_10

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-eqz v0, :cond_10

    .line 3134
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/bo;->a(F)Landroid/text/format/Time;

    move-result-object v0

    .line 3135
    if-eqz v0, :cond_10

    .line 3136
    invoke-virtual {v0, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 3137
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 3138
    const-string v3, "hover_day"

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 3139
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/bb;->setArguments(Landroid/os/Bundle;)V

    .line 3143
    :cond_10
    sget-object v0, Lcom/android/calendar/hj;->r:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 3144
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;IIIZ)V

    .line 3147
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 3148
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 3149
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 3152
    const-string v2, "MonthHoverEventList"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 3153
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_12

    .line 3155
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3161
    :cond_12
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dv:Lcom/android/calendar/month/bb;

    const-string v2, "MonthHoverEventList"

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 3162
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 3168
    :pswitch_3
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->du:Z

    if-nez v0, :cond_13

    sget-boolean v0, Lcom/android/calendar/ek;->a:Z

    if-nez v0, :cond_13

    .line 3169
    invoke-static {v6}, Lcom/android/calendar/ek;->a(I)V

    .line 3171
    :cond_13
    iput-boolean v5, p0, Lcom/android/calendar/month/bo;->du:Z

    .line 3172
    iput v5, p0, Lcom/android/calendar/month/bo;->dr:I

    goto/16 :goto_4

    .line 3156
    :catch_0
    move-exception v0

    goto/16 :goto_1

    .line 3087
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private c(I)I
    .locals 1

    .prologue
    .line 3365
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/month/bo;->a(IZ)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/month/bo;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/android/calendar/month/bo;->dL:I

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/month/bo;I)I
    .locals 0

    .prologue
    .line 84
    iput p1, p0, Lcom/android/calendar/month/bo;->dK:I

    return p1
.end method

.method private c(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1430
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1431
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    :goto_0
    iput v0, p0, Lcom/android/calendar/month/bo;->dp:I

    .line 1432
    iget v0, p0, Lcom/android/calendar/month/bo;->aR:I

    iget v3, p0, Lcom/android/calendar/month/bo;->av:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 1433
    iget v3, p0, Lcom/android/calendar/month/bo;->dp:I

    sub-int/2addr v0, v3

    .line 1435
    iget v3, p0, Lcom/android/calendar/month/bo;->dw:I

    mul-int/2addr v3, v0

    iget v4, p0, Lcom/android/calendar/month/bo;->ba:I

    div-int/2addr v3, v4

    iget v4, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v3, v4

    .line 1436
    iget v4, p0, Lcom/android/calendar/month/bo;->dw:I

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v0, v4

    iget v4, p0, Lcom/android/calendar/month/bo;->ba:I

    div-int/2addr v0, v4

    iget v4, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v0, v4

    .line 1437
    iget v4, p0, Lcom/android/calendar/month/bo;->dp:I

    add-int/2addr v3, v4

    .line 1438
    iget v4, p0, Lcom/android/calendar/month/bo;->dp:I

    add-int/2addr v0, v4

    .line 1439
    iget v4, p0, Lcom/android/calendar/month/bo;->b:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    sget-boolean v4, Lcom/android/calendar/month/h;->w:Z

    if-nez v4, :cond_0

    .line 1440
    iput v1, p2, Landroid/graphics/Rect;->top:I

    .line 1441
    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/android/calendar/month/bo;->aS:I

    add-int/2addr v1, v4

    iput v1, p2, Landroid/graphics/Rect;->bottom:I

    .line 1442
    iput v3, p2, Landroid/graphics/Rect;->left:I

    .line 1443
    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 1448
    const v0, 0x7f020255

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->M:Landroid/graphics/drawable/Drawable;

    .line 1449
    iget-object v0, p0, Lcom/android/calendar/month/bo;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1450
    iget-object v0, p0, Lcom/android/calendar/month/bo;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1452
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1431
    goto :goto_0
.end method

.method private d(Landroid/graphics/Paint;I)V
    .locals 2

    .prologue
    .line 1612
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bJ:Z

    if-eqz v0, :cond_5

    .line 1613
    iget-object v0, p0, Lcom/android/calendar/month/bo;->at:[I

    aget v0, v0, p2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1614
    iget-object v1, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v0, v0, p2

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/calendar/month/bo;->bo:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1626
    :cond_0
    :goto_1
    return-void

    .line 1614
    :cond_1
    iget v0, p0, Lcom/android/calendar/month/bo;->bs:I

    goto :goto_0

    .line 1615
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/bo;->at:[I

    aget v0, v0, p2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aF:Lcom/android/calendar/hl;

    iget-object v0, v0, Lcom/android/calendar/hl;->a:[Z

    aget-boolean v0, v0, p2

    if-eqz v0, :cond_0

    .line 1617
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v0, v0, p2

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/calendar/month/bo;->br:I

    :goto_2
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/android/calendar/month/bo;->bu:I

    goto :goto_2

    .line 1620
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aF:Lcom/android/calendar/hl;

    iget-object v0, v0, Lcom/android/calendar/hl;->a:[Z

    aget-boolean v0, v0, p2

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->ee:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aF:Lcom/android/calendar/hl;

    iget-object v0, v0, Lcom/android/calendar/hl;->b:[Z

    aget-boolean v0, v0, p2

    if-nez v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/android/calendar/month/bo;->as:[Z

    aget-boolean v0, v0, p2

    if-eqz v0, :cond_0

    .line 1623
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v0, v0, p2

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/android/calendar/month/bo;->br:I

    :goto_3
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :cond_8
    iget v0, p0, Lcom/android/calendar/month/bo;->bu:I

    goto :goto_3
.end method

.method private f()I
    .locals 8

    .prologue
    const/16 v2, 0x64

    const/4 v1, 0x0

    .line 1781
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    sget v3, Lcom/android/calendar/month/bo;->dk:I

    sub-int v3, v0, v3

    .line 1783
    sget v0, Lcom/android/calendar/month/bo;->cb:I

    sget v4, Lcom/android/calendar/month/bo;->ci:I

    add-int/2addr v4, v0

    .line 1784
    sget v0, Lcom/android/calendar/month/bo;->bZ:I

    move v0, v1

    .line 1790
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 1791
    sget v5, Lcom/android/calendar/month/bo;->bZ:I

    mul-int v6, v4, v0

    add-int/2addr v5, v6

    .line 1793
    sget v6, Lcom/android/calendar/month/bo;->cb:I

    add-int/2addr v5, v6

    if-lt v5, v3, :cond_2

    .line 1804
    iget-boolean v3, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v3, :cond_1

    :cond_1
    move v3, v1

    .line 1808
    :goto_0
    const/4 v1, 0x7

    if-ge v3, v1, :cond_5

    .line 1809
    iget-object v1, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v1, v1, v3

    sget v4, Lcom/android/calendar/month/bo;->da:I

    aget-wide v4, v1, v4

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_4

    move v1, v0

    .line 1810
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v4, v4, v3

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 1811
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v4, v4, v3

    sget-object v5, Lcom/android/calendar/month/bw;->c:Lcom/android/calendar/month/bw;

    aput-object v5, v4, v1

    .line 1810
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1797
    :cond_2
    const/16 v5, 0x14

    if-lt v0, v5, :cond_0

    move v0, v2

    .line 1816
    :cond_3
    :goto_2
    return v0

    .line 1808
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1816
    :cond_5
    if-nez v0, :cond_3

    move v0, v2

    goto :goto_2
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    .line 1820
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dC:[[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->dC:[[J

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 1821
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dC:[[J

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1822
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v2, v2, v0

    sget v3, Lcom/android/calendar/month/bo;->cZ:I

    aput-wide v4, v2, v3

    .line 1823
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v2, v2, v0

    sget v3, Lcom/android/calendar/month/bo;->da:I

    aput-wide v4, v2, v3

    .line 1821
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1826
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1827
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 1828
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    aget-object v0, v0, v1

    sget v2, Lcom/android/calendar/month/bo;->cZ:I

    aput-wide v4, v0, v2

    .line 1829
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    aget-object v0, v0, v1

    sget v2, Lcom/android/calendar/month/bo;->da:I

    aput-wide v4, v0, v2

    .line 1827
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1832
    :cond_1
    return-void
.end method

.method private declared-synchronized getImageCache()Lcom/android/calendar/month/bx;
    .locals 2

    .prologue
    .line 610
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/android/calendar/month/bo;->ej:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/month/bo;->ej:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 611
    :cond_0
    new-instance v0, Lcom/android/calendar/month/bx;

    invoke-direct {v0}, Lcom/android/calendar/month/bx;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    .line 612
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/calendar/month/bo;->ej:Ljava/lang/ref/WeakReference;

    .line 614
    :cond_1
    sget-object v0, Lcom/android/calendar/month/bo;->ej:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/bx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 610
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2058
    iget-object v3, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    move v0, v1

    .line 2059
    :goto_1
    array-length v6, v5

    if-ge v0, v6, :cond_0

    .line 2060
    sget-object v6, Lcom/android/calendar/month/bw;->a:Lcom/android/calendar/month/bw;

    aput-object v6, v5, v0

    .line 2059
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2058
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2063
    :cond_1
    return-void
.end method

.method private h(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 1456
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 1457
    iget v0, p0, Lcom/android/calendar/month/bo;->G:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1458
    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1459
    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1460
    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1462
    iget v0, p0, Lcom/android/calendar/month/bo;->c:I

    .line 1463
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v2, :cond_0

    .line 1464
    add-int/lit8 v0, v0, 0x1

    .line 1466
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v0, v2, v0

    if-nez v0, :cond_6

    move v0, v6

    .line 1469
    :goto_0
    if-eqz v0, :cond_1

    .line 1470
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1474
    :goto_1
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-nez v0, :cond_4

    .line 1475
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    div-int/lit8 v0, v0, 0x2

    .line 1476
    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v2

    invoke-direct {p0, v6}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 1479
    iget-boolean v3, p0, Lcom/android/calendar/month/bo;->e:Z

    if-eqz v3, :cond_2

    .line 1480
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v3, Lcom/android/calendar/month/bo;->cE:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 1481
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v3, Lcom/android/calendar/month/bo;->cE:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1482
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/bo;->c:I

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    div-int/lit8 v3, v2, 0x2

    add-int/2addr v1, v3

    sget v3, Lcom/android/calendar/month/bo;->cE:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1483
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/bo;->c:I

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sget v2, Lcom/android/calendar/month/bo;->cE:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1484
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->cE:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1519
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v6, v0, Landroid/graphics/Rect;->top:I

    .line 1520
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/bo;->aS:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1521
    return-void

    .line 1472
    :cond_1
    new-instance v0, Landroid/graphics/LightingColorFilter;

    iget v2, p0, Lcom/android/calendar/month/bo;->D:I

    iget v3, p0, Lcom/android/calendar/month/bo;->E:I

    invoke-direct {v0, v2, v3}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_1

    .line 1486
    :cond_2
    iget v0, p0, Lcom/android/calendar/month/bo;->d:I

    if-ne v0, v1, :cond_3

    .line 1487
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v1, Lcom/android/calendar/month/bo;->cz:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1488
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget v2, Lcom/android/calendar/month/bo;->cD:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1489
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/bo;->c:I

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    sget v2, Lcom/android/calendar/month/bo;->cz:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1490
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/android/calendar/month/bo;->cD:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1491
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->cD:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 1493
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/bo;->c:I

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    sget v2, Lcom/android/calendar/month/bo;->cA:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1494
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v1, Lcom/android/calendar/month/bo;->cz:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1495
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/android/calendar/month/bo;->cx:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1496
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget v2, Lcom/android/calendar/month/bo;->cy:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1497
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawOval(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1501
    :cond_4
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    div-int/lit8 v0, v0, 0x2

    .line 1502
    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v2

    invoke-direct {p0, v6}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 1504
    iget v3, p0, Lcom/android/calendar/month/bo;->d:I

    if-ne v3, v1, :cond_5

    .line 1505
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v3, Lcom/android/calendar/month/bo;->cM:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1506
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget v3, Lcom/android/calendar/month/bo;->cM:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1507
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/bo;->c:I

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sget v2, Lcom/android/calendar/month/bo;->cM:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1508
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/android/calendar/month/bo;->cM:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1509
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/bo;->cM:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1511
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v3, Lcom/android/calendar/month/bo;->cC:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1512
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget v3, Lcom/android/calendar/month/bo;->cC:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1513
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/bo;->c:I

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sget v2, Lcom/android/calendar/month/bo;->cB:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1514
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/android/calendar/month/bo;->cB:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1515
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawOval(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method

.method private i()V
    .locals 13

    .prologue
    const/4 v11, 0x0

    const/4 v8, 0x0

    .line 3594
    invoke-direct {p0, v8}, Lcom/android/calendar/month/bo;->a(Z)Landroid/text/format/Time;

    move-result-object v4

    .line 3595
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/month/k;->b(Z)V

    .line 3596
    iget-object v0, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    const-wide/16 v2, 0x320

    invoke-static {v0, v2, v3}, Lcom/android/calendar/month/k;->a(Landroid/os/Handler;J)V

    .line 3597
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dW:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const-wide/16 v9, 0x10

    move-object v1, p0

    move-object v5, v4

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 3599
    return-void
.end method

.method private i(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 1959
    const/4 v1, 0x0

    .line 1960
    iget v0, p0, Lcom/android/calendar/month/bo;->ba:I

    .line 1961
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v2, :cond_0

    .line 1962
    add-int/lit8 v0, v0, 0x1

    .line 1963
    const/4 v1, 0x1

    .line 1965
    :cond_0
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 1966
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 1970
    new-instance v8, Landroid/graphics/LightingColorFilter;

    iget v2, p0, Lcom/android/calendar/month/bo;->D:I

    iget v3, p0, Lcom/android/calendar/month/bo;->E:I

    invoke-direct {v8, v2, v3}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    move v3, v1

    .line 1971
    :goto_0
    if-ge v3, v0, :cond_2

    .line 1972
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aK:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v3

    if-eqz v1, :cond_b

    .line 1973
    iget-boolean v1, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v1, :cond_3

    .line 1974
    add-int/lit8 v1, v3, -0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    .line 1975
    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    .line 1981
    :goto_1
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v2, :cond_4

    add-int/lit8 v2, v3, -0x1

    .line 1986
    :goto_2
    iget-boolean v4, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-eqz v4, :cond_5

    .line 1987
    sget v5, Lcom/android/calendar/month/bo;->cY:I

    .line 1988
    sget v4, Lcom/android/calendar/month/bo;->cY:I

    .line 1990
    iput v1, v7, Landroid/graphics/Rect;->right:I

    .line 1991
    sub-int v9, v1, v5

    iput v9, v7, Landroid/graphics/Rect;->left:I

    .line 1992
    iget v9, p0, Lcom/android/calendar/month/bo;->aS:I

    iput v9, v7, Landroid/graphics/Rect;->bottom:I

    .line 1993
    iget v9, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v4

    iput v9, v7, Landroid/graphics/Rect;->top:I

    .line 1995
    iget-object v9, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v9, v9, v2

    sget v10, Lcom/android/calendar/month/bo;->da:I

    aget-wide v10, v9, v10

    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-eqz v9, :cond_1

    .line 1996
    iget v9, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v5

    iput v9, v7, Landroid/graphics/Rect;->left:I

    .line 1997
    iget v9, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v5

    iput v9, v7, Landroid/graphics/Rect;->right:I

    .line 2033
    :cond_1
    :goto_3
    iget-object v9, p0, Lcom/android/calendar/month/bo;->aK:[Landroid/graphics/Bitmap;

    aget-object v9, v9, v3

    const/4 v10, 0x1

    invoke-static {v9, v5, v4, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2035
    if-nez v4, :cond_a

    .line 2055
    :cond_2
    return-void

    .line 1977
    :cond_3
    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    .line 1978
    add-int/lit8 v1, v3, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    goto :goto_1

    :cond_4
    move v2, v3

    .line 1981
    goto :goto_2

    .line 2000
    :cond_5
    sget v5, Lcom/android/calendar/month/bo;->dc:I

    .line 2001
    sget v4, Lcom/android/calendar/month/bo;->dd:I

    .line 2002
    iget v9, p0, Lcom/android/calendar/month/bo;->d:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_6

    .line 2003
    iput v1, v7, Landroid/graphics/Rect;->right:I

    .line 2004
    sub-int v9, v1, v5

    iput v9, v7, Landroid/graphics/Rect;->left:I

    .line 2005
    sget v9, Lcom/android/calendar/month/bo;->de:I

    iput v9, v7, Landroid/graphics/Rect;->top:I

    .line 2006
    sget v9, Lcom/android/calendar/month/bo;->de:I

    add-int/2addr v9, v4

    iput v9, v7, Landroid/graphics/Rect;->bottom:I

    goto :goto_3

    .line 2008
    :cond_6
    iget-object v9, p0, Lcom/android/calendar/month/bo;->dW:Lcom/android/calendar/al;

    invoke-virtual {v9}, Lcom/android/calendar/al;->l()Z

    move-result v9

    if-nez v9, :cond_8

    .line 2009
    const/4 v9, 0x0

    iput v9, v7, Landroid/graphics/Rect;->top:I

    .line 2010
    iget v9, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v4

    iput v9, v7, Landroid/graphics/Rect;->bottom:I

    .line 2011
    iget-object v9, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v9, v9, v2

    if-lez v9, :cond_7

    .line 2012
    sget v9, Lcom/android/calendar/month/bo;->df:I

    sub-int v9, v1, v9

    iput v9, v7, Landroid/graphics/Rect;->right:I

    .line 2013
    iget v9, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v5

    iput v9, v7, Landroid/graphics/Rect;->left:I

    goto :goto_3

    .line 2015
    :cond_7
    iput v1, v7, Landroid/graphics/Rect;->right:I

    .line 2016
    iget v9, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v5

    iput v9, v7, Landroid/graphics/Rect;->left:I

    goto :goto_3

    .line 2019
    :cond_8
    iput v1, v7, Landroid/graphics/Rect;->right:I

    .line 2020
    sub-int v9, v1, v5

    iput v9, v7, Landroid/graphics/Rect;->left:I

    .line 2021
    iget-object v9, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v9, v9, v2

    if-lez v9, :cond_9

    .line 2022
    sget v9, Lcom/android/calendar/month/bo;->de:I

    iput v9, v7, Landroid/graphics/Rect;->top:I

    .line 2023
    sget v9, Lcom/android/calendar/month/bo;->de:I

    add-int/2addr v9, v4

    iput v9, v7, Landroid/graphics/Rect;->bottom:I

    goto :goto_3

    .line 2025
    :cond_9
    const/4 v9, 0x0

    iput v9, v7, Landroid/graphics/Rect;->top:I

    .line 2026
    iget v9, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v4

    iput v9, v7, Landroid/graphics/Rect;->bottom:I

    goto :goto_3

    .line 2039
    :cond_a
    iget-object v9, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v2, v9, v2

    sget v9, Lcom/android/calendar/month/bo;->da:I

    aget-wide v10, v2, v9

    const-wide/16 v12, 0x0

    cmp-long v2, v10, v12

    if-nez v2, :cond_c

    .line 1971
    :cond_b
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 2042
    :cond_c
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v2, v2, v3

    if-eqz v2, :cond_e

    .line 2047
    :goto_5
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dW:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->l()Z

    move-result v2

    if-eqz v2, :cond_d

    iget v2, p0, Lcom/android/calendar/month/bo;->d:I

    const/4 v9, 0x2

    if-ne v2, v9, :cond_d

    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v2

    sget v9, Lcom/android/calendar/month/bo;->cA:I

    add-int/2addr v2, v9

    sget v9, Lcom/android/calendar/month/bo;->cx:I

    add-int/2addr v2, v9

    sub-int/2addr v1, v5

    if-ge v2, v1, :cond_2

    .line 2051
    :cond_d
    const/4 v1, 0x0

    invoke-virtual {p1, v4, v1, v7, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2052
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_4

    .line 2045
    :cond_e
    invoke-virtual {v6, v8}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_5
.end method

.method private j()V
    .locals 13

    .prologue
    const/4 v11, 0x0

    const/4 v0, 0x1

    .line 3618
    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->a(Z)Landroid/text/format/Time;

    move-result-object v4

    .line 3619
    invoke-static {v0}, Lcom/android/calendar/month/k;->b(Z)V

    .line 3620
    iget-object v0, p0, Lcom/android/calendar/month/bo;->T:Landroid/os/Handler;

    const-wide/16 v2, 0x320

    invoke-static {v0, v2, v3}, Lcom/android/calendar/month/k;->a(Landroid/os/Handler;J)V

    .line 3621
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dW:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x10

    move-object v1, p0

    move-object v5, v4

    move-object v12, v11

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 3623
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 3625
    iget-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 3626
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    .line 3627
    iget-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/calendar/month/bu;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/bu;-><init>(Lcom/android/calendar/month/bo;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 3654
    iget-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/calendar/month/bo;->eo:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 3655
    iget-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 3656
    iget-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 3657
    iget-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 3659
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3660
    iget-object v0, p0, Lcom/android/calendar/month/bo;->eh:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 3662
    :cond_1
    return-void

    .line 3626
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method a(I)I
    .locals 2

    .prologue
    .line 3225
    iget v0, p0, Lcom/android/calendar/month/bo;->aR:I

    iget v1, p0, Lcom/android/calendar/month/bo;->av:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 3226
    iget v1, p0, Lcom/android/calendar/month/bo;->dp:I

    sub-int/2addr v0, v1

    .line 3227
    iget v1, p0, Lcom/android/calendar/month/bo;->ba:I

    div-int v1, v0, v1

    .line 3228
    iget v0, p0, Lcom/android/calendar/month/bo;->aZ:I

    sub-int v0, p1, v0

    rem-int/lit8 v0, v0, 0x7

    .line 3229
    if-gez v0, :cond_0

    .line 3230
    add-int/lit8 v0, v0, 0x7

    .line 3232
    :cond_0
    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bo;->dp:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;IIIIII)I
    .locals 23

    .prologue
    .line 2133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bo;->dx:Ljava/util/Set;

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/dh;->q:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2134
    const/16 p4, -0x1

    .line 2417
    :goto_0
    return p4

    .line 2136
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bo;->dx:Ljava/util/Set;

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/dh;->q:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2138
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/dh;->j:I

    move-object/from16 v0, p2

    iget v3, v0, Lcom/android/calendar/dh;->i:I

    if-eq v2, v3, :cond_5

    const/4 v2, 0x1

    move v3, v2

    .line 2139
    :goto_1
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/calendar/dh;->j:I

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v2, v4

    add-int/lit8 v2, v2, 0x1

    .line 2141
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/bo;->aN:I

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/dh;->i:I

    if-le v4, v5, :cond_1

    .line 2142
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/bo;->aN:I

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v4, v5

    .line 2143
    sub-int/2addr v2, v4

    .line 2148
    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/bo;->aN:I

    add-int v4, v4, p6

    .line 2149
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/bo;->aN:I

    move-object/from16 v0, p2

    iget v6, v0, Lcom/android/calendar/dh;->i:I

    if-gt v5, v6, :cond_6

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/dh;->i:I

    if-ge v5, v4, :cond_6

    .line 2150
    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/dh;->i:I

    sub-int/2addr v4, v5

    sub-int/2addr v2, v4

    .line 2155
    :cond_2
    :goto_2
    add-int v4, p6, v2

    const/4 v5, 0x7

    if-le v4, v5, :cond_3

    .line 2156
    rsub-int/lit8 v2, p6, 0x7

    .line 2161
    :cond_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/bo;->aN:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/bo;->ad:I

    if-gt v4, v5, :cond_7

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/dh;->i:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/bo;->ad:I

    if-gt v4, v5, :cond_7

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/dh;->j:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/bo;->ac:I

    if-lt v4, v5, :cond_7

    .line 2163
    const/4 v4, 0x1

    move v11, v4

    .line 2170
    :goto_3
    const/4 v4, 0x0

    .line 2171
    if-nez v11, :cond_4

    .line 2172
    const/4 v4, 0x1

    .line 2175
    :cond_4
    sget v8, Lcom/android/calendar/month/bo;->cb:I

    .line 2177
    const/4 v5, 0x0

    move/from16 v6, p4

    .line 2178
    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v7, v7, p6

    array-length v7, v7

    if-ge v5, v7, :cond_8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v7, v7, p6

    aget-object v7, v7, v5

    sget-object v9, Lcom/android/calendar/month/bw;->a:Lcom/android/calendar/month/bw;

    if-eq v7, v9, :cond_8

    .line 2179
    sget v7, Lcom/android/calendar/month/bo;->cb:I

    sget v9, Lcom/android/calendar/month/bo;->ci:I

    add-int/2addr v7, v9

    add-int/2addr v6, v7

    .line 2180
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 2138
    :cond_5
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_1

    .line 2151
    :cond_6
    if-ltz p6, :cond_2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/bo;->aN:I

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/dh;->i:I

    if-le v4, v5, :cond_2

    .line 2152
    sub-int v2, v2, p6

    goto :goto_2

    .line 2167
    :cond_7
    const/4 v4, 0x0

    move v11, v4

    goto :goto_3

    .line 2183
    :cond_8
    const/4 v7, 0x0

    .line 2185
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/month/bo;->d:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_9

    .line 2186
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v9, v9, p6

    sget v10, Lcom/android/calendar/month/bo;->da:I

    aget-wide v12, v9, v10

    const-wide/16 v14, -0x1

    cmp-long v9, v12, v14

    if-eqz v9, :cond_9

    .line 2187
    sget v7, Lcom/android/calendar/month/bo;->dk:I

    .line 2190
    :cond_9
    add-int/2addr v8, v6

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/month/bo;->aS:I

    sub-int v7, v9, v7

    if-lt v8, v7, :cond_a

    .line 2191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bo;->dx:Ljava/util/Set;

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/dh;->q:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2195
    :cond_a
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v7, v7, p6

    array-length v7, v7

    if-ge v5, v7, :cond_c

    .line 2196
    const/4 v7, 0x0

    :goto_5
    if-ge v7, v2, :cond_c

    .line 2197
    add-int v8, p6, v7

    const/4 v9, 0x7

    if-ge v8, v9, :cond_b

    .line 2198
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    add-int v9, p6, v7

    aget-object v8, v8, v9

    aget-object v8, v8, v5

    sget-object v9, Lcom/android/calendar/month/bw;->c:Lcom/android/calendar/month/bw;

    if-eq v8, v9, :cond_b

    .line 2199
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    add-int v9, p6, v7

    aget-object v8, v8, v9

    sget-object v9, Lcom/android/calendar/month/bw;->b:Lcom/android/calendar/month/bw;

    aput-object v9, v8, v5

    .line 2196
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 2205
    :cond_c
    move-object/from16 v0, p2

    iget v7, v0, Lcom/android/calendar/dh;->c:I

    .line 2206
    move-object/from16 v0, p2

    iget v8, v0, Lcom/android/calendar/dh;->B:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_10

    const/4 v8, 0x1

    move v10, v8

    .line 2208
    :goto_6
    if-eqz v10, :cond_d

    .line 2209
    invoke-static {v7}, Lcom/android/calendar/hj;->c(I)I

    move-result v7

    .line 2212
    :cond_d
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    invoke-virtual {v8, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 2213
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    sget-object v9, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2215
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v9, Lcom/android/calendar/month/bo;->bY:I

    add-int v9, v9, p3

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 2216
    invoke-virtual/range {p2 .. p2}, Lcom/android/calendar/dh;->e()Z

    move-result v8

    if-nez v8, :cond_e

    if-eqz v3, :cond_11

    .line 2217
    :cond_e
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    add-int v9, p6, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v9

    iput v9, v8, Landroid/graphics/Rect;->right:I

    .line 2222
    :goto_7
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v6, v8, Landroid/graphics/Rect;->top:I

    .line 2223
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v9, Lcom/android/calendar/month/bo;->cb:I

    add-int/2addr v9, v6

    iput v9, v8, Landroid/graphics/Rect;->bottom:I

    .line 2226
    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/calendar/month/bo;->q:I

    add-int/2addr v6, v8

    sget v8, Lcom/android/calendar/month/bo;->cm:I

    add-int v12, v6, v8

    .line 2229
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 2230
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 2232
    if-eqz v3, :cond_14

    move/from16 v0, p8

    if-lt v5, v0, :cond_14

    .line 2234
    const/4 v9, 0x1

    .line 2235
    const/4 v6, 0x0

    .line 2237
    const/4 v8, 0x1

    :goto_8
    if-ge v8, v2, :cond_14

    .line 2238
    add-int v15, p6, v8

    const/16 v16, 0x7

    move/from16 v0, v16

    if-ge v15, v0, :cond_f

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    add-int v16, p6, v8

    aget-object v15, v15, v16

    array-length v15, v15

    if-ge v5, v15, :cond_f

    .line 2239
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    add-int v16, p6, v8

    aget-object v15, v15, v16

    aget-object v15, v15, v5

    sget-object v16, Lcom/android/calendar/month/bw;->c:Lcom/android/calendar/month/bw;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_12

    .line 2240
    if-eqz v9, :cond_f

    .line 2241
    const/4 v9, 0x0

    .line 2242
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2244
    sub-int v15, v8, v6

    .line 2245
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2237
    :cond_f
    :goto_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 2206
    :cond_10
    const/4 v8, 0x0

    move v10, v8

    goto/16 :goto_6

    .line 2219
    :cond_11
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v9, Lcom/android/calendar/month/bo;->bY:I

    add-int v9, v9, p3

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/month/bo;->dO:I

    add-int/2addr v9, v12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/month/bo;->dP:I

    add-int/2addr v9, v12

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 2220
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/month/bo;->dN:I

    add-int/2addr v9, v12

    iput v9, v8, Landroid/graphics/Rect;->right:I

    goto/16 :goto_7

    .line 2249
    :cond_12
    if-nez v9, :cond_13

    .line 2250
    const/4 v6, 0x1

    .line 2253
    const/4 v9, 0x6

    if-ne v8, v9, :cond_27

    .line 2254
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2256
    sub-int v9, v8, v8

    add-int/lit8 v9, v9, 0x1

    .line 2257
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v14, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v9, v6

    move v6, v8

    .line 2258
    goto :goto_9

    .line 2260
    :cond_13
    add-int/lit8 v15, v2, -0x1

    if-ne v8, v15, :cond_f

    .line 2261
    const/4 v9, 0x0

    .line 2262
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2264
    sub-int v15, v8, v6

    add-int/lit8 v15, v15, 0x1

    .line 2265
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 2274
    :cond_14
    const/4 v5, 0x0

    .line 2276
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_15

    .line 2277
    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2279
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    add-int v8, p6, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v8

    iput v8, v6, Landroid/graphics/Rect;->right:I

    :cond_15
    move v6, v2

    move/from16 v2, p6

    move/from16 v22, v4

    move v4, v5

    move/from16 v5, v22

    .line 2288
    :cond_16
    invoke-virtual/range {p2 .. p2}, Lcom/android/calendar/dh;->e()Z

    move-result v8

    if-nez v8, :cond_17

    if-eqz v3, :cond_20

    .line 2289
    :cond_17
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v8, :cond_1e

    .line 2290
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sget v15, Lcom/android/calendar/month/bo;->ce:I

    sub-int/2addr v9, v15

    sget v15, Lcom/android/calendar/month/bo;->cf:I

    add-int/2addr v9, v15

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 2294
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/calendar/month/bo;->by:Z

    if-eqz v8, :cond_18

    .line 2295
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v8, Landroid/graphics/Rect;->left:I

    sget v15, Lcom/android/calendar/month/bo;->bY:I

    add-int/2addr v9, v15

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 2298
    :cond_18
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v8, :cond_1f

    .line 2299
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v8, Landroid/graphics/Rect;->right:I

    sget v15, Lcom/android/calendar/month/bo;->cf:I

    sub-int/2addr v9, v15

    iput v9, v8, Landroid/graphics/Rect;->right:I

    .line 2304
    :goto_b
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    sget v9, Lcom/android/calendar/month/bo;->cn:I

    add-int/2addr v8, v9

    .line 2318
    :goto_c
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    move/from16 v0, p5

    if-le v0, v9, :cond_22

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    const-string v15, ".."

    invoke-virtual {v9, v15}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    float-to-int v9, v9

    add-int/2addr v9, v8

    sub-int v9, p5, v9

    int-to-float v9, v9

    .line 2322
    :goto_d
    const/4 v15, 0x3

    if-eq v2, v15, :cond_19

    const/4 v15, 0x6

    if-ne v2, v15, :cond_1a

    .line 2323
    :cond_19
    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v9, v15

    .line 2326
    :cond_1a
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v15}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/android/calendar/hj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 2327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    move-object/from16 v16, v0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END_SMALL:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v15, v0, v9, v1}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 2330
    invoke-virtual/range {p2 .. p2}, Lcom/android/calendar/dh;->e()Z

    move-result v15

    if-nez v15, :cond_1b

    if-eqz v3, :cond_23

    .line 2331
    :cond_1b
    move-object/from16 v0, p2

    iget v15, v0, Lcom/android/calendar/dh;->c:I

    invoke-static {v15}, Lcom/android/calendar/hj;->d(I)I

    move-result v15

    .line 2332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    move-object/from16 v16, v0

    sget-object v17, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual/range {v16 .. v17}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/android/calendar/month/bo;->a(IZ)I

    move-result v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 2334
    new-instance v5, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-direct {v5, v15}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 2335
    if-eqz v10, :cond_1c

    .line 2336
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    const/16 v16, 0x66

    invoke-virtual/range {v15 .. v16}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2338
    :cond_1c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2382
    :goto_e
    const/4 v5, 0x0

    .line 2384
    if-nez v11, :cond_25

    .line 2385
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/month/bo;->w:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/text/TextPaint;->setColor(I)V

    .line 2389
    :goto_f
    if-eqz v10, :cond_1d

    .line 2390
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    const/16 v16, 0x66

    invoke-virtual/range {v15 .. v16}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 2393
    :cond_1d
    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    int-to-float v8, v8

    int-to-float v15, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v9, v8, v15, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2395
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/month/bo;->t:I

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->setColor(I)V

    .line 2398
    add-int/lit8 v4, v4, 0x1

    .line 2400
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_26

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v4, v8, :cond_26

    .line 2402
    const/4 v8, 0x1

    .line 2404
    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int v6, p6, v2

    .line 2406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v9

    sget v15, Lcom/android/calendar/month/bo;->ce:I

    add-int/2addr v9, v15

    iput v9, v2, Landroid/graphics/Rect;->left:I

    .line 2408
    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2409
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    add-int v15, v6, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v15

    iput v15, v9, Landroid/graphics/Rect;->right:I

    move/from16 v22, v6

    move v6, v2

    move/from16 v2, v22

    .line 2415
    :goto_10
    if-nez v8, :cond_16

    .line 2417
    sget v2, Lcom/android/calendar/month/bo;->cb:I

    add-int v2, v2, p4

    sget v3, Lcom/android/calendar/month/bo;->ci:I

    add-int p4, v2, v3

    goto/16 :goto_0

    .line 2292
    :cond_1e
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v8, Landroid/graphics/Rect;->left:I

    sget v15, Lcom/android/calendar/month/bo;->bY:I

    add-int/2addr v9, v15

    iput v9, v8, Landroid/graphics/Rect;->left:I

    goto/16 :goto_a

    .line 2301
    :cond_1f
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v8, Landroid/graphics/Rect;->right:I

    sget v15, Lcom/android/calendar/month/bo;->bY:I

    sub-int/2addr v9, v15

    iput v9, v8, Landroid/graphics/Rect;->right:I

    goto/16 :goto_b

    .line 2307
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v8, :cond_21

    .line 2308
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sget v15, Lcom/android/calendar/month/bo;->ce:I

    sub-int/2addr v9, v15

    sget v15, Lcom/android/calendar/month/bo;->bY:I

    add-int/2addr v9, v15

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 2314
    :goto_11
    sget v8, Lcom/android/calendar/month/bo;->cc:I

    add-int v8, v8, p3

    sget v9, Lcom/android/calendar/month/bo;->cl:I

    add-int/2addr v8, v9

    goto/16 :goto_c

    .line 2310
    :cond_21
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v8, Landroid/graphics/Rect;->left:I

    sget v15, Lcom/android/calendar/month/bo;->bY:I

    add-int/2addr v9, v15

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 2311
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v8, Landroid/graphics/Rect;->top:I

    int-to-double v0, v9

    move-wide/from16 v16, v0

    sget v9, Lcom/android/calendar/month/bo;->cb:I

    int-to-double v0, v9

    move-wide/from16 v18, v0

    const-wide v20, 0x3fb999999999999aL    # 0.1

    mul-double v18, v18, v20

    add-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-int v9, v0

    iput v9, v8, Landroid/graphics/Rect;->top:I

    goto :goto_11

    .line 2318
    :cond_22
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v8

    int-to-float v9, v9

    goto/16 :goto_d

    .line 2369
    :cond_23
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v5}, Lcom/android/calendar/month/bo;->a(IZ)I

    move-result v5

    invoke-virtual {v15, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 2370
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2371
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    sget-object v15, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v15}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2372
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    const/16 v15, 0x33

    invoke-virtual {v5, v15}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2373
    new-instance v5, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-direct {v5, v15}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 2374
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->right:I

    iput v15, v5, Landroid/graphics/Rect;->left:I

    .line 2375
    add-int v15, p6, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v15

    iput v15, v5, Landroid/graphics/Rect;->right:I

    .line 2376
    if-eqz v10, :cond_24

    .line 2377
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    const/16 v16, 0x66

    invoke-virtual/range {v15 .. v16}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2379
    :cond_24
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_e

    .line 2387
    :cond_25
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v2}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Paint;I)V

    goto/16 :goto_f

    .line 2412
    :cond_26
    const/4 v8, 0x0

    goto/16 :goto_10

    :cond_27
    move v9, v6

    move v6, v8

    goto/16 :goto_9
.end method

.method protected a(Landroid/graphics/Canvas;Lcom/android/calendar/task/aa;IIIII)I
    .locals 14

    .prologue
    .line 2482
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dy:Ljava/util/Set;

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/task/aa;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2483
    const/16 p4, -0x1

    .line 2661
    :goto_0
    return p4

    .line 2485
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dy:Ljava/util/Set;

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/task/aa;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2487
    sget v5, Lcom/android/calendar/month/bo;->cb:I

    .line 2489
    const/4 v2, 0x0

    .line 2491
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 2492
    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/android/calendar/task/aa;->d:J

    invoke-virtual {v6, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 2493
    const/4 v3, 0x1

    invoke-virtual {v6, v3}, Landroid/text/format/Time;->normalize(Z)J

    move/from16 v3, p4

    .line 2495
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v4, v4, p6

    array-length v4, v4

    if-ge v2, v4, :cond_1

    iget-object v4, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v4, v4, p6

    aget-object v4, v4, v2

    sget-object v7, Lcom/android/calendar/month/bw;->a:Lcom/android/calendar/month/bw;

    if-eq v4, v7, :cond_1

    .line 2496
    sget v4, Lcom/android/calendar/month/bo;->cb:I

    sget v7, Lcom/android/calendar/month/bo;->ci:I

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    .line 2497
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2499
    :cond_1
    const/4 v4, 0x0

    .line 2501
    iget v7, p0, Lcom/android/calendar/month/bo;->d:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 2502
    iget-object v7, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v7, v7, p6

    sget v8, Lcom/android/calendar/month/bo;->da:I

    aget-wide v8, v7, v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_2

    .line 2503
    sget v4, Lcom/android/calendar/month/bo;->dk:I

    .line 2507
    :cond_2
    add-int/2addr v5, v3

    iget v7, p0, Lcom/android/calendar/month/bo;->aS:I

    sub-int v4, v7, v4

    if-lt v5, v4, :cond_3

    .line 2509
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dy:Ljava/util/Set;

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/task/aa;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2514
    :cond_3
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v4, v4, p6

    array-length v4, v4

    if-ge v2, v4, :cond_4

    .line 2515
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v4, v4, p6

    sget-object v5, Lcom/android/calendar/month/bw;->b:Lcom/android/calendar/month/bw;

    aput-object v5, v4, v2

    .line 2519
    :cond_4
    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/calendar/task/aa;->i:I

    if-gez v4, :cond_11

    .line 2520
    iget-object v4, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v4

    .line 2525
    :goto_2
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v7, Lcom/android/calendar/month/bo;->bY:I

    add-int v7, v7, p3

    sget v8, Lcom/android/calendar/month/bo;->cc:I

    add-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->left:I

    .line 2527
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v7, Lcom/android/calendar/month/bo;->bY:I

    sub-int v7, p5, v7

    iput v7, v5, Landroid/graphics/Rect;->right:I

    .line 2528
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v3, v5, Landroid/graphics/Rect;->top:I

    .line 2529
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v7, Lcom/android/calendar/month/bo;->cb:I

    add-int/2addr v7, v3

    iput v7, v5, Landroid/graphics/Rect;->bottom:I

    .line 2530
    iget-object v5, p0, Lcom/android/calendar/month/bo;->m:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2531
    sget v5, Lcom/android/calendar/month/k;->ah:I

    iget v7, v6, Landroid/text/format/Time;->month:I

    if-eq v5, v7, :cond_5

    .line 2532
    iget-object v5, p0, Lcom/android/calendar/month/bo;->m:Landroid/graphics/Paint;

    invoke-direct {p0, v4}, Lcom/android/calendar/month/bo;->c(I)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 2534
    :cond_5
    iget-object v5, p0, Lcom/android/calendar/month/bo;->m:Landroid/graphics/Paint;

    const/16 v7, 0x33

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2535
    iget-object v5, p0, Lcom/android/calendar/month/bo;->m:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2536
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/calendar/month/bo;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2538
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v7, Lcom/android/calendar/month/bo;->bY:I

    add-int v7, v7, p3

    iput v7, v5, Landroid/graphics/Rect;->left:I

    .line 2539
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    sget v8, Lcom/android/calendar/month/bo;->cc:I

    add-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->right:I

    .line 2540
    iget-object v5, p0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2541
    iget-object v5, p0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2543
    sget v5, Lcom/android/calendar/month/k;->ah:I

    iget v7, v6, Landroid/text/format/Time;->month:I

    if-eq v5, v7, :cond_6

    .line 2544
    iget-object v5, p0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    invoke-direct {p0, v4}, Lcom/android/calendar/month/bo;->c(I)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2547
    :cond_6
    iget-boolean v4, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-eqz v4, :cond_7

    .line 2548
    iget-object v4, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->top:I

    int-to-double v8, v5

    sget v5, Lcom/android/calendar/month/bo;->cb:I

    int-to-double v10, v5

    const-wide v12, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-int v5, v8

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 2551
    :cond_7
    iget-object v4, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2553
    iget-boolean v4, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-eqz v4, :cond_8

    .line 2554
    iget-object v4, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v3, v4, Landroid/graphics/Rect;->top:I

    .line 2555
    iget-object v4, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v5, Lcom/android/calendar/month/bo;->cb:I

    add-int/2addr v5, v3

    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    .line 2558
    :cond_8
    add-int/lit8 v4, v3, 0x1

    .line 2560
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v5, Lcom/android/calendar/month/bo;->cc:I

    add-int v5, v5, p3

    sget v7, Lcom/android/calendar/month/bo;->cq:I

    add-int/2addr v5, v7

    iput v5, v3, Landroid/graphics/Rect;->left:I

    .line 2561
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sget v7, Lcom/android/calendar/month/bo;->co:I

    add-int/2addr v5, v7

    iput v5, v3, Landroid/graphics/Rect;->right:I

    .line 2562
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v5, Lcom/android/calendar/month/bo;->cp:I

    add-int/2addr v5, v4

    iput v5, v3, Landroid/graphics/Rect;->top:I

    .line 2563
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sget v7, Lcom/android/calendar/month/bo;->co:I

    add-int/2addr v5, v7

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    .line 2565
    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/android/calendar/task/aa;->g:Z

    if-eqz v3, :cond_12

    .line 2566
    iget-object v3, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    const/16 v5, 0xff

    invoke-virtual {v3, v5}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 2567
    iget-object v3, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    iget v5, p0, Lcom/android/calendar/month/bo;->w:I

    invoke-virtual {v3, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 2569
    sget v3, Lcom/android/calendar/month/k;->ah:I

    iget v5, v6, Landroid/text/format/Time;->month:I

    if-eq v3, v5, :cond_9

    .line 2570
    iget-object v3, p0, Lcom/android/calendar/month/bo;->N:Landroid/graphics/drawable/Drawable;

    new-instance v5, Landroid/graphics/LightingColorFilter;

    iget v7, p0, Lcom/android/calendar/month/bo;->D:I

    iget v8, p0, Lcom/android/calendar/month/bo;->E:I

    invoke-direct {v5, v7, v8}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2573
    :cond_9
    iget-object v3, p0, Lcom/android/calendar/month/bo;->N:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2574
    iget-object v3, p0, Lcom/android/calendar/month/bo;->N:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2575
    iget v3, p0, Lcom/android/calendar/month/bo;->dA:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/calendar/month/bo;->dA:I

    .line 2578
    iget-object v3, p0, Lcom/android/calendar/month/bo;->N:Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2601
    :goto_3
    sget v3, Lcom/android/calendar/month/bo;->co:I

    add-int v3, v3, p3

    sget v5, Lcom/android/calendar/month/bo;->cq:I

    add-int/2addr v3, v5

    sget v5, Lcom/android/calendar/month/bo;->cr:I

    add-int/2addr v3, v5

    sget v5, Lcom/android/calendar/month/bo;->cc:I

    add-int/2addr v5, v3

    .line 2605
    iget v3, p0, Lcom/android/calendar/month/bo;->q:I

    add-int/2addr v3, v4

    sget v7, Lcom/android/calendar/month/bo;->cm:I

    add-int/2addr v3, v7

    sget v7, Lcom/android/calendar/month/bo;->ct:I

    add-int/2addr v7, v3

    .line 2607
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    move/from16 v0, p5

    if-le v0, v3, :cond_15

    sub-int v3, p5, v5

    int-to-float v3, v3

    .line 2610
    :goto_4
    const/4 v8, 0x3

    move/from16 v0, p6

    if-eq v0, v8, :cond_a

    const/4 v8, 0x6

    move/from16 v0, p6

    if-ne v0, v8, :cond_b

    .line 2611
    :cond_a
    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v3, v8

    .line 2613
    :cond_b
    iget-boolean v8, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-eqz v8, :cond_16

    sget v8, Lcom/android/calendar/month/bo;->dl:I

    if-eq v2, v8, :cond_c

    sget v8, Lcom/android/calendar/month/bo;->dl:I

    add-int/lit8 v8, v8, -0x1

    if-ne v2, v8, :cond_16

    .line 2615
    :cond_c
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v2, v2, p6

    sget v8, Lcom/android/calendar/month/bo;->da:I

    aget-wide v8, v2, v8

    const-wide/16 v10, -0x1

    cmp-long v2, v8, v10

    if-eqz v2, :cond_d

    .line 2616
    sget v2, Lcom/android/calendar/month/bo;->dn:I

    int-to-float v2, v2

    sub-float/2addr v3, v2

    .line 2626
    :cond_d
    :goto_5
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/calendar/task/aa;->c:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/calendar/hj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2627
    iget-object v8, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v8, v3, v9}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2630
    sget v3, Lcom/android/calendar/month/bo;->cc:I

    add-int v3, v3, p3

    sget v8, Lcom/android/calendar/month/bo;->cq:I

    add-int/2addr v3, v8

    sget v8, Lcom/android/calendar/month/bo;->cs:I

    add-int/2addr v3, v8

    .line 2631
    iget-object v8, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    move/from16 v0, p5

    if-le v0, v8, :cond_17

    sub-int v3, p5, v3

    .line 2633
    :goto_6
    iget-object v3, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    move/from16 v0, p6

    invoke-virtual {p0, v3, v0}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Paint;I)V

    .line 2635
    sget v3, Lcom/android/calendar/month/k;->ah:I

    iget v8, v6, Landroid/text/format/Time;->month:I

    if-eq v3, v8, :cond_e

    .line 2636
    iget-object v3, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    iget v8, p0, Lcom/android/calendar/month/bo;->w:I

    invoke-virtual {v3, v8}, Landroid/text/TextPaint;->setColor(I)V

    .line 2641
    :cond_e
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    int-to-float v8, v5

    int-to-float v7, v7

    iget-object v9, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    invoke-virtual {p1, v3, v8, v7, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2643
    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/android/calendar/task/aa;->g:Z

    if-eqz v3, :cond_10

    .line 2644
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 2645
    iget-object v7, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v10

    invoke-virtual {v7, v8, v9, v10, v3}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2647
    int-to-float v3, v5

    .line 2649
    sget v5, Lcom/android/calendar/month/bo;->cb:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    .line 2651
    sget v5, Lcom/android/calendar/month/k;->ah:I

    iget v6, v6, Landroid/text/format/Time;->month:I

    if-eq v5, v6, :cond_f

    .line 2652
    iget-object v5, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    iget v6, p0, Lcom/android/calendar/month/bo;->w:I

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->setColor(I)V

    .line 2654
    :cond_f
    iget-object v5, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    add-float v5, v3, v2

    .line 2655
    iget-object v7, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    move-object v2, p1

    move v6, v4

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2658
    :cond_10
    iget-object v2, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    iget v3, p0, Lcom/android/calendar/month/bo;->t:I

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 2659
    iget-object v2, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 2661
    sget v2, Lcom/android/calendar/month/bo;->cb:I

    add-int v2, v2, p4

    sget v3, Lcom/android/calendar/month/bo;->ci:I

    add-int p4, v2, v3

    goto/16 :goto_0

    .line 2522
    :cond_11
    iget-object v4, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/calendar/task/aa;->i:I

    invoke-static {v4, v5}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v4

    goto/16 :goto_2

    .line 2581
    :cond_12
    iget-object v3, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    const/16 v5, 0xff

    invoke-virtual {v3, v5}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 2582
    iget-boolean v3, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-eqz v3, :cond_13

    .line 2583
    iget-object v3, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    iget v5, p0, Lcom/android/calendar/month/bo;->t:I

    invoke-virtual {v3, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 2589
    :cond_13
    sget v3, Lcom/android/calendar/month/k;->ah:I

    iget v5, v6, Landroid/text/format/Time;->month:I

    if-eq v3, v5, :cond_14

    .line 2590
    iget-object v3, p0, Lcom/android/calendar/month/bo;->O:Landroid/graphics/drawable/Drawable;

    new-instance v5, Landroid/graphics/LightingColorFilter;

    iget v7, p0, Lcom/android/calendar/month/bo;->D:I

    iget v8, p0, Lcom/android/calendar/month/bo;->E:I

    invoke-direct {v5, v7, v8}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2593
    :cond_14
    iget-object v3, p0, Lcom/android/calendar/month/bo;->O:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2594
    iget-object v3, p0, Lcom/android/calendar/month/bo;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2595
    iget v3, p0, Lcom/android/calendar/month/bo;->dA:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/calendar/month/bo;->dA:I

    .line 2598
    iget-object v3, p0, Lcom/android/calendar/month/bo;->O:Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto/16 :goto_3

    .line 2607
    :cond_15
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v5

    int-to-float v3, v3

    goto/16 :goto_4

    .line 2618
    :cond_16
    iget-boolean v8, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v8, :cond_d

    iget v8, p0, Lcom/android/calendar/month/bo;->d:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_d

    sget v8, Lcom/android/calendar/month/bo;->dl:I

    if-ne v2, v8, :cond_d

    .line 2621
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v2, v2, p6

    sget v8, Lcom/android/calendar/month/bo;->da:I

    aget-wide v8, v2, v8

    const-wide/16 v10, -0x1

    cmp-long v2, v8, v10

    if-eqz v2, :cond_d

    .line 2622
    sget v2, Lcom/android/calendar/month/bo;->dn:I

    int-to-float v2, v2

    sub-float/2addr v3, v2

    goto/16 :goto_5

    .line 2631
    :cond_17
    iget-object v8, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    sub-int v3, v8, v3

    goto/16 :goto_6
.end method

.method public a(F)Landroid/text/format/Time;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2731
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v1, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v0, v1

    .line 2732
    :goto_0
    int-to-float v1, v0

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/calendar/month/bo;->aR:I

    iget v2, p0, Lcom/android/calendar/month/bo;->av:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_2

    .line 2733
    :cond_0
    const/4 v0, 0x0

    .line 2750
    :goto_1
    return-object v0

    .line 2731
    :cond_1
    iget v0, p0, Lcom/android/calendar/month/bo;->av:I

    goto :goto_0

    .line 2736
    :cond_2
    int-to-float v1, v0

    sub-float v1, p1, v1

    iget v2, p0, Lcom/android/calendar/month/bo;->ba:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/bo;->aR:I

    sub-int v0, v2, v0

    iget v2, p0, Lcom/android/calendar/month/bo;->av:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-int v0, v0

    .line 2737
    iget v1, p0, Lcom/android/calendar/month/bo;->aN:I

    add-int/2addr v1, v0

    .line 2739
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/bo;->be:Ljava/lang/String;

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2740
    iget v2, p0, Lcom/android/calendar/month/bo;->aQ:I

    const/16 v3, 0xddc

    if-ne v2, v3, :cond_3

    .line 2742
    const v2, 0x253d8c    # 3.419992E-39f

    if-ne v1, v2, :cond_3

    .line 2743
    const/4 v1, 0x0

    const/16 v2, 0x7b2

    invoke-virtual {v0, v4, v1, v2}, Landroid/text/format/Time;->set(III)V

    .line 2744
    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    goto :goto_1

    .line 2749
    :cond_3
    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    goto :goto_1
.end method

.method protected a()V
    .locals 7

    .prologue
    const/4 v0, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 692
    new-array v1, v0, [I

    .line 693
    new-array v2, v0, [I

    .line 695
    const-string v0, "XXXXXXR"

    .line 697
    iget v3, p0, Lcom/android/calendar/month/bo;->bo:I

    aput v3, v1, v4

    .line 698
    iget v3, p0, Lcom/android/calendar/month/bo;->bq:I

    aput v3, v1, v5

    .line 699
    iget v3, p0, Lcom/android/calendar/month/bo;->br:I

    aput v3, v1, v6

    .line 701
    iget v3, p0, Lcom/android/calendar/month/bo;->bs:I

    aput v3, v2, v4

    .line 702
    iget v3, p0, Lcom/android/calendar/month/bo;->bt:I

    aput v3, v2, v5

    .line 703
    iget v3, p0, Lcom/android/calendar/month/bo;->bu:I

    aput v3, v2, v6

    .line 706
    :try_start_0
    invoke-static {}, Lcom/android/calendar/dz;->e()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 711
    :goto_0
    iget v3, p0, Lcom/android/calendar/month/bo;->aZ:I

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/bo;->bf:[I

    .line 712
    iget v1, p0, Lcom/android/calendar/month/bo;->aZ:I

    invoke-static {v0, v2, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->bg:[I

    .line 713
    return-void

    .line 707
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const v2, 0x7f0b00c5

    const v3, 0x7f0b00bc

    .line 618
    invoke-direct {p0}, Lcom/android/calendar/month/bo;->getImageCache()Lcom/android/calendar/month/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    .line 620
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 621
    const v0, 0x7f0b00af

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->bp:I

    .line 622
    const v0, 0x7f0b00b2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->bo:I

    .line 623
    const v0, 0x7f0b0120

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->bs:I

    .line 624
    const v0, 0x7f0b00ac

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->bq:I

    .line 625
    const v0, 0x7f0b00cb

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->bt:I

    .line 626
    const v0, 0x7f0b00b0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->br:I

    .line 627
    const v0, 0x7f0b00e3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->bu:I

    .line 628
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->a()V

    .line 629
    const v0, 0x7f0b0099

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->t:I

    .line 630
    const v0, 0x7f0b0011

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->u:I

    .line 631
    const v0, 0x7f0b0013

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->v:I

    .line 632
    const v0, 0x7f0b009d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->w:I

    .line 634
    const v0, 0x7f0b00a0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->A:I

    .line 635
    const v0, 0x7f0b00a1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->B:I

    .line 636
    const v0, 0x7f0b00a7

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->x:I

    .line 637
    const v0, 0x7f0b0105

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->C:I

    .line 639
    const v0, 0x7f0b009f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->y:I

    .line 640
    const v0, 0x7f0b009e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->z:I

    .line 642
    const v0, 0x7f0b00ab

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->D:I

    .line 643
    const v0, 0x7f0b00aa

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->E:I

    .line 645
    const v0, 0x7f0b00a3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->bw:I

    .line 646
    const v0, 0x7f0b00a6

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->F:I

    .line 647
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->G:I

    .line 648
    const v0, 0x7f0b0098

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->J:I

    .line 649
    const v0, 0x7f0b0094

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->H:I

    .line 650
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->I:I

    .line 654
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f020089

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->b(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->P:Landroid/graphics/Bitmap;

    .line 655
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->L:Landroid/graphics/drawable/Drawable;

    .line 656
    iget-object v0, p0, Lcom/android/calendar/month/bo;->L:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 658
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->U:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->V:Z

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f020255

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->M:Landroid/graphics/drawable/Drawable;

    .line 662
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v0, :cond_2

    .line 663
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->K:Landroid/graphics/drawable/Drawable;

    .line 664
    iget-object v0, p0, Lcom/android/calendar/month/bo;->K:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/android/calendar/month/bo;->bp:I

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 670
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-eqz v0, :cond_3

    .line 671
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f0201c8

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->N:Landroid/graphics/drawable/Drawable;

    .line 672
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f0201c7

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->O:Landroid/graphics/drawable/Drawable;

    .line 674
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->aC:Landroid/graphics/drawable/Drawable;

    .line 675
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aC:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0b012d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 683
    :goto_1
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->Q:Landroid/graphics/drawable/ColorDrawable;

    .line 684
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bJ:Z

    if-eqz v0, :cond_1

    .line 685
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f020117

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->af:Landroid/graphics/drawable/Drawable;

    .line 686
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f020118

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->ag:Landroid/graphics/drawable/Drawable;

    .line 688
    :cond_1
    return-void

    .line 666
    :cond_2
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->K:Landroid/graphics/drawable/Drawable;

    .line 667
    iget-object v0, p0, Lcom/android/calendar/month/bo;->K:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    goto :goto_0

    .line 677
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f0200c4

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->N:Landroid/graphics/drawable/Drawable;

    .line 678
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f0200c3

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->O:Landroid/graphics/drawable/Drawable;

    .line 680
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ek:Lcom/android/calendar/month/bx;

    const v2, 0x7f020150

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/month/bx;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->aC:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1244
    const/16 v0, 0x20

    new-array v1, v0, [F

    .line 1249
    const/16 v2, 0x1c

    .line 1250
    const/4 v0, 0x1

    const/high16 v3, -0x3f600000    # -5.0f

    aput v3, v1, v7

    .line 1251
    const/4 v3, 0x2

    iget v4, p0, Lcom/android/calendar/month/bo;->aS:I

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    aput v4, v1, v0

    .line 1252
    const/4 v4, 0x3

    iget v0, p0, Lcom/android/calendar/month/bo;->aR:I

    add-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    aput v0, v1, v3

    .line 1253
    const/4 v0, 0x4

    iget v3, p0, Lcom/android/calendar/month/bo;->aS:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    aput v3, v1, v4

    .line 1255
    iget v3, p0, Lcom/android/calendar/month/bo;->aS:I

    .line 1257
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1258
    div-int/lit8 v4, v0, 0x4

    sub-int/2addr v4, v7

    invoke-direct {p0, v4}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v4

    .line 1259
    add-int/lit8 v5, v0, 0x1

    int-to-float v6, v4

    aput v6, v1, v0

    .line 1260
    add-int/lit8 v0, v5, 0x1

    int-to-float v6, v7

    aput v6, v1, v5

    .line 1261
    add-int/lit8 v5, v0, 0x1

    int-to-float v4, v4

    aput v4, v1, v0

    .line 1262
    add-int/lit8 v0, v5, 0x1

    int-to-float v4, v3

    aput v4, v1, v5

    goto :goto_0

    .line 1264
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ax:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/month/bo;->A:I

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1265
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ax:Landroid/graphics/Paint;

    sget v3, Lcom/android/calendar/month/bo;->bY:I

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1266
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ax:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7, v2, v0}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    .line 1268
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_1

    .line 1269
    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v1, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v0, v1

    .line 1271
    int-to-float v1, v0

    const/4 v2, 0x0

    int-to-float v3, v0

    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/calendar/month/bo;->ax:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1273
    :cond_1
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;III)V
    .locals 8

    .prologue
    const/16 v7, 0xff

    const/16 v6, 0x99

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2422
    .line 2423
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_5

    move v0, v1

    .line 2427
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    invoke-virtual {p0, v3, p4}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Paint;I)V

    .line 2428
    iget-object v3, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    invoke-virtual {p0, v3, p4}, Lcom/android/calendar/month/bo;->c(Landroid/graphics/Paint;I)V

    .line 2429
    iget-object v3, p0, Lcom/android/calendar/month/bo;->n:Landroid/graphics/Paint;

    iget v4, p0, Lcom/android/calendar/month/bo;->F:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2431
    if-ne p2, v1, :cond_3

    iget-object v3, p0, Lcom/android/calendar/month/bo;->ae:[Ljava/lang/String;

    aget-object v3, v3, v2

    .line 2435
    :goto_1
    iget-boolean v4, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-eqz v4, :cond_4

    .line 2436
    sget v4, Lcom/android/calendar/month/bo;->cg:I

    sub-int/2addr p3, v4

    .line 2437
    sget v4, Lcom/android/calendar/month/bo;->ch:I

    .line 2439
    :goto_2
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2440
    const-string v3, "1.0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2442
    const-string v1, "+1"

    .line 2444
    :cond_0
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v5, Lcom/android/calendar/month/bo;->cS:I

    iput v5, v3, Landroid/graphics/Rect;->top:I

    .line 2446
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v5, Lcom/android/calendar/month/bo;->cT:I

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    .line 2450
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v5, 0x2

    if-le v3, v5, :cond_1

    .line 2451
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    .line 2454
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v5, Lcom/android/calendar/month/bo;->cU:I

    sub-int v5, p3, v5

    sub-int v2, v5, v2

    iput v2, v3, Landroid/graphics/Rect;->left:I

    .line 2455
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sget v3, Lcom/android/calendar/month/bo;->cV:I

    add-int/2addr v3, p3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 2457
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    add-int/2addr v0, p4

    aget-boolean v0, v2, v0

    if-nez v0, :cond_2

    .line 2458
    iget-object v0, p0, Lcom/android/calendar/month/bo;->n:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2459
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 2461
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dY:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 2462
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dY:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 2463
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dY:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 2464
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dY:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 2467
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dY:Landroid/graphics/RectF;

    iget v2, p0, Lcom/android/calendar/month/bo;->do:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/calendar/month/bo;->do:I

    int-to-float v3, v3

    iget-object v5, p0, Lcom/android/calendar/month/bo;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 2468
    sget v0, Lcom/android/calendar/month/bo;->cW:I

    sub-int v0, p3, v0

    int-to-float v0, v0

    sget v2, Lcom/android/calendar/month/bo;->cX:I

    add-int/2addr v2, v4

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2471
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    sget v1, Lcom/android/calendar/month/bo;->bR:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2473
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    iget v1, p0, Lcom/android/calendar/month/bo;->x:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 2474
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 2476
    iget-object v0, p0, Lcom/android/calendar/month/bo;->n:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2478
    return-void

    .line 2431
    :cond_3
    iget-object v3, p0, Lcom/android/calendar/month/bo;->ae:[Ljava/lang/String;

    aget-object v3, v3, v1

    goto/16 :goto_1

    :cond_4
    move v4, v2

    goto/16 :goto_2

    :cond_5
    move v0, v2

    goto/16 :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;IIII)V
    .locals 9

    .prologue
    const/16 v3, 0xc

    const/4 v5, 0x6

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/16 v0, 0xb

    .line 3334
    .line 3337
    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v8}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 3338
    iget v4, p0, Lcom/android/calendar/month/bo;->d:I

    if-ne v4, v1, :cond_1

    move v7, v2

    .line 3339
    :goto_0
    if-eqz v7, :cond_4

    move v4, v0

    move v6, v0

    .line 3344
    :goto_1
    if-le p5, v6, :cond_0

    .line 3345
    if-eqz v7, :cond_2

    move p5, v0

    .line 3351
    :cond_0
    :goto_2
    sub-int v0, p5, p3

    .line 3352
    if-le p5, v5, :cond_3

    sub-int v3, p5, v4

    if-ge p3, v3, :cond_3

    if-nez v7, :cond_3

    .line 3354
    sub-int/2addr v0, v4

    .line 3356
    :goto_3
    sget v2, Lcom/android/calendar/month/bo;->db:I

    sget v3, Lcom/android/calendar/month/bo;->dh:I

    add-int/2addr v2, v3

    mul-int/2addr v0, v2

    sub-int v0, p2, v0

    .line 3357
    iget v2, p0, Lcom/android/calendar/month/bo;->aS:I

    sget v3, Lcom/android/calendar/month/bo;->db:I

    sget v4, Lcom/android/calendar/month/bo;->dg:I

    add-int/2addr v3, v4

    mul-int/2addr v1, v3

    sub-int v1, v2, v1

    .line 3359
    sget v2, Lcom/android/calendar/month/bo;->db:I

    add-int/2addr v2, v0

    sget v3, Lcom/android/calendar/month/bo;->db:I

    add-int/2addr v3, v1

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 3360
    invoke-virtual {v8, p4}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 3361
    invoke-virtual {v8, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 3362
    return-void

    .line 3338
    :cond_1
    const/4 v4, 0x0

    move v7, v4

    goto :goto_0

    :cond_2
    move p5, v3

    .line 3348
    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3

    :cond_4
    move v4, v5

    move v6, v3

    goto :goto_1
.end method

.method a(Landroid/graphics/Paint;I)V
    .locals 0

    .prologue
    .line 2666
    return-void
.end method

.method a(Landroid/graphics/Paint;IZ)V
    .locals 1

    .prologue
    .line 1608
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->bf:[I

    aget v0, v0, p2

    :goto_0
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1609
    return-void

    .line 1608
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bg:[I

    aget v0, v0, p2

    goto :goto_0
.end method

.method public a(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 983
    invoke-super {p0, p1, p2}, Lcom/android/calendar/month/cd;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 985
    const-string v0, "orientation"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 986
    const-string v0, "orientation"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->d:I

    .line 989
    :cond_0
    const-string v0, "system_timezone"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 990
    const-string v0, "system_timezone"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_4

    .line 991
    iput-boolean v2, p0, Lcom/android/calendar/month/bo;->f:Z

    .line 997
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->f:Z

    invoke-virtual {p0, p2, v0}, Lcom/android/calendar/month/bo;->a(Ljava/lang/String;Z)Z

    .line 998
    iget v0, p0, Lcom/android/calendar/month/bo;->ba:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/bo;->bb:I

    .line 1000
    const-string v0, "easy_mode"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1001
    iput-boolean v2, p0, Lcom/android/calendar/month/bo;->dH:Z

    .line 1002
    sget v0, Lcom/android/calendar/month/bo;->cP:I

    iput v0, p0, Lcom/android/calendar/month/bo;->ab:I

    .line 1008
    :goto_1
    const-string v0, "simple_month_view"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1009
    const-string v0, "simple_month_view"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_6

    .line 1010
    iput-boolean v2, p0, Lcom/android/calendar/month/bo;->e:Z

    .line 1015
    :cond_2
    :goto_2
    sget-boolean v0, Lcom/android/calendar/month/bo;->ah:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aU:Z

    if-eqz v0, :cond_3

    .line 1016
    invoke-direct {p0}, Lcom/android/calendar/month/bo;->k()V

    .line 1018
    :cond_3
    return-void

    .line 993
    :cond_4
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->f:Z

    goto :goto_0

    .line 1004
    :cond_5
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->dH:Z

    .line 1005
    sget v0, Lcom/android/calendar/month/bo;->cO:I

    iput v0, p0, Lcom/android/calendar/month/bo;->ab:I

    goto :goto_1

    .line 1012
    :cond_6
    iput-boolean v1, p0, Lcom/android/calendar/month/bo;->e:Z

    goto :goto_2
.end method

.method public a(Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 491
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bo;->setEvents(Ljava/util/List;)V

    .line 492
    return-void
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 1220
    const-wide/16 v0, 0xa

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1024
    if-eqz p2, :cond_0

    .line 1025
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object p1

    .line 1028
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->a:Landroid/text/format/Time;

    iput-object p1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1029
    iget-object v0, p0, Lcom/android/calendar/month/bo;->a:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 1030
    iget-object v0, p0, Lcom/android/calendar/month/bo;->a:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 1031
    iget-object v0, p0, Lcom/android/calendar/month/bo;->a:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/bo;->a:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 1032
    iget v1, p0, Lcom/android/calendar/month/bo;->aN:I

    if-lt v0, v1, :cond_1

    iget v1, p0, Lcom/android/calendar/month/bo;->aN:I

    iget v2, p0, Lcom/android/calendar/month/bo;->ba:I

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_1

    .line 1033
    iput-boolean v5, p0, Lcom/android/calendar/month/bo;->aV:Z

    .line 1034
    iget v1, p0, Lcom/android/calendar/month/bo;->aN:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bo;->c:I

    .line 1039
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aV:Z

    return v0

    .line 1036
    :cond_1
    iput-boolean v4, p0, Lcom/android/calendar/month/bo;->aV:Z

    .line 1037
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/month/bo;->c:I

    goto :goto_0
.end method

.method protected b()V
    .locals 10

    .prologue
    const v9, 0x7f0e0004

    const/4 v8, 0x2

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 720
    invoke-super {p0}, Lcom/android/calendar/month/cd;->b()V

    .line 722
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->aI:Lcom/android/calendar/d/g;

    .line 723
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 724
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->aG:Lcom/android/calendar/d/a/a;

    .line 727
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->z(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/bo;->e:Z

    .line 728
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->aa:Ljava/lang/String;

    .line 729
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dx:Ljava/util/Set;

    .line 730
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dy:Ljava/util/Set;

    .line 732
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 733
    const v0, 0x7f0c033d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->bP:I

    .line 734
    const v0, 0x7f0c033f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->bS:I

    .line 736
    const v0, 0x7f0c0248

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->bU:I

    .line 737
    const v0, 0x7f0c0247

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cQ:I

    .line 738
    const v0, 0x7f0c0249

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cR:I

    .line 741
    const v0, 0x7f0d0015

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dj:I

    .line 743
    const v0, 0x7f0d001e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->bW:I

    .line 746
    const v0, 0x7f0c0290

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cO:I

    .line 747
    const v0, 0x7f0c0109

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cP:I

    .line 749
    sget v0, Lcom/android/calendar/month/bo;->cO:I

    iput v0, p0, Lcom/android/calendar/month/bo;->ab:I

    .line 751
    const v0, 0x7f0c0251

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dP:I

    .line 753
    const v0, 0x7f0c025a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->ce:I

    .line 754
    const v0, 0x7f0c0244

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cf:I

    .line 755
    const v0, 0x7f0c025b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->bZ:I

    .line 756
    const v0, 0x7f0c0246

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->bY:I

    .line 758
    const v0, 0x7f0c024b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dN:I

    .line 759
    const v0, 0x7f0c024a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dO:I

    .line 760
    const v0, 0x7f0c0254

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cd:I

    .line 761
    const v0, 0x7f0c0256

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cb:I

    .line 762
    const v0, 0x7f0c0257

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cc:I

    .line 763
    const v0, 0x7f0c0252

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->ci:I

    .line 764
    const v0, 0x7f0c0255

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cj:I

    .line 765
    const v0, 0x7f0c0258

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->bQ:I

    .line 766
    const v0, 0x7f0c0273

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->bR:I

    .line 767
    const v0, 0x7f0c0270

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cg:I

    .line 768
    const v0, 0x7f0c0275

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->ch:I

    .line 769
    const v0, 0x7f0c027f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->ck:I

    .line 771
    const v0, 0x7f0c0274

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cS:I

    .line 772
    const v0, 0x7f0c026d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cT:I

    .line 773
    const v0, 0x7f0c026e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cU:I

    .line 774
    const v0, 0x7f0c026f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cV:I

    .line 775
    const v0, 0x7f0c0271

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cW:I

    .line 776
    const v0, 0x7f0c0272

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cX:I

    .line 778
    const v0, 0x7f0d0016

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cl:I

    .line 779
    const v0, 0x7f0c0259

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cm:I

    .line 780
    const v0, 0x7f0d0014

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cn:I

    .line 781
    const v0, 0x7f0d001b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->co:I

    .line 782
    const v0, 0x7f0d001a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cp:I

    .line 784
    const v0, 0x7f0c0281

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cq:I

    .line 785
    const v0, 0x7f0c0282

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cr:I

    .line 786
    const v0, 0x7f0c0280

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cs:I

    .line 787
    const v0, 0x7f0d001c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->ct:I

    .line 789
    const v0, 0x7f0c0287

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cx:I

    .line 790
    const v0, 0x7f0c0285

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cy:I

    .line 791
    const v0, 0x7f0c028a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cz:I

    .line 792
    const v0, 0x7f0c0289

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cA:I

    .line 794
    const v0, 0x7f0c0288

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cv:I

    .line 795
    const v0, 0x7f0c0286

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cw:I

    .line 797
    const v0, 0x7f0c0283

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cD:I

    .line 799
    const v0, 0x7f0c0284

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cE:I

    .line 802
    const v0, 0x7f0c0276

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cG:I

    .line 804
    const v0, 0x7f0c027b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cH:I

    .line 806
    const v0, 0x7f0c0278

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cI:I

    .line 808
    const v0, 0x7f0c027a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cJ:I

    .line 810
    const v0, 0x7f0c0279

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cK:I

    .line 812
    const v0, 0x7f0c0277

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cN:I

    .line 813
    const v0, 0x7f0c0250

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cL:I

    .line 815
    const v0, 0x7f0c024f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cM:I

    .line 817
    const/high16 v0, 0x7f0c0000

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cF:I

    .line 819
    const v0, 0x7f0c0253

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->s:I

    .line 821
    const v0, 0x7f0d0017

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dK:I

    .line 822
    const v0, 0x7f0c027e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dL:I

    .line 823
    const v0, 0x7f0c027c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dM:I

    .line 824
    const v0, 0x7f0c0419

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cB:I

    .line 825
    const v0, 0x7f0c0418

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cC:I

    .line 826
    const v0, 0x7f0c0405

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->di:I

    .line 828
    const v0, 0x7f0c0291

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->cY:I

    .line 830
    const v0, 0x7f0c028f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dc:I

    .line 831
    const v0, 0x7f0c028c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dd:I

    .line 832
    const v0, 0x7f0c028e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->de:I

    .line 833
    const v0, 0x7f0c028d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->df:I

    .line 835
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c024e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->db:I

    .line 838
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c024c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dg:I

    .line 839
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c024d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dh:I

    .line 841
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c028b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dk:I

    .line 843
    const v0, 0x7f0d001d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dl:I

    .line 846
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c026a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dm:I

    .line 847
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c026b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dn:I

    .line 849
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0415

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->do:I

    .line 851
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0009

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dQ:I

    .line 852
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d000f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dS:I

    .line 853
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0010

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->dT:I

    .line 855
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/bo;->a(Landroid/content/Context;)V

    .line 857
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bL:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bM:Z

    if-eqz v0, :cond_2

    .line 858
    :cond_1
    const v0, 0x7f0c033b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->dZ:I

    .line 859
    sget v0, Lcom/android/calendar/month/bo;->bZ:I

    sget v2, Lcom/android/calendar/month/bo;->dZ:I

    add-int/2addr v0, v2

    sput v0, Lcom/android/calendar/month/bo;->bZ:I

    .line 861
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    .line 862
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 863
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 864
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/bo;->dZ:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 865
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/bo;->bw:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 866
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 867
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 868
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    invoke-static {v4}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 870
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0f0126

    .line 871
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/month/bo;->ef:Ljava/lang/String;

    .line 872
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ay:Landroid/graphics/Paint;

    sget-object v2, Lcom/android/calendar/month/bo;->ef:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/calendar/month/bo;->aH:I

    .line 874
    const v0, 0x7f0c0212

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->eb:I

    .line 875
    const v0, 0x7f0c0214

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->ec:I

    .line 876
    const v0, 0x7f0c0213

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->ed:I

    .line 877
    const v0, 0x7f0c033c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->ea:I

    .line 878
    const v0, 0x7f0d0011

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/bo;->eg:I

    .line 881
    :cond_2
    sget-boolean v0, Lcom/android/calendar/month/bo;->dq:Z

    if-nez v0, :cond_4

    .line 882
    sget v0, Lcom/android/calendar/month/bo;->au:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_3

    .line 883
    sget v0, Lcom/android/calendar/month/bo;->bV:I

    int-to-float v0, v0

    sget v2, Lcom/android/calendar/month/bo;->au:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/bo;->bV:I

    .line 884
    sget v0, Lcom/android/calendar/month/bo;->bX:I

    int-to-float v0, v0

    sget v2, Lcom/android/calendar/month/bo;->au:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/bo;->bX:I

    .line 887
    sget v0, Lcom/android/calendar/month/bo;->ce:I

    int-to-float v0, v0

    sget v2, Lcom/android/calendar/month/bo;->au:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/bo;->ce:I

    .line 888
    sget v0, Lcom/android/calendar/month/bo;->bT:I

    int-to-float v0, v0

    sget v2, Lcom/android/calendar/month/bo;->au:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/bo;->bT:I

    .line 889
    sget v0, Lcom/android/calendar/month/bo;->cu:I

    int-to-float v0, v0

    sget v2, Lcom/android/calendar/month/bo;->au:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/bo;->cu:I

    .line 891
    sget v0, Lcom/android/calendar/month/bo;->cl:I

    int-to-float v0, v0

    sget v2, Lcom/android/calendar/month/bo;->au:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/bo;->cl:I

    .line 892
    sget v0, Lcom/android/calendar/month/bo;->cn:I

    int-to-float v0, v0

    sget v2, Lcom/android/calendar/month/bo;->au:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/bo;->cn:I

    .line 895
    :cond_3
    sput-boolean v5, Lcom/android/calendar/month/bo;->dq:Z

    .line 897
    :cond_4
    sget v0, Lcom/android/calendar/month/bo;->bT:I

    iput v0, p0, Lcom/android/calendar/month/bo;->av:I

    .line 899
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    .line 900
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 901
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 902
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/bo;->bP:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 903
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/bo;->bo:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 904
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    const-string v2, "sec-roboto-light"

    invoke-static {v2, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 905
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 906
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 908
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    iget-object v2, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    move-result v2

    add-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/month/bo;->p:I

    .line 912
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    .line 913
    iget-object v0, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 914
    iget-object v0, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 915
    iget-object v0, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/bo;->bQ:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 916
    iget-object v0, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    iget v2, p0, Lcom/android/calendar/month/bo;->t:I

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 917
    iget-object v0, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/bo;->ck:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 918
    iget-object v0, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    invoke-static {v4}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 920
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->h:Landroid/text/TextPaint;

    .line 921
    iget-object v0, p0, Lcom/android/calendar/month/bo;->h:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 922
    iget-object v0, p0, Lcom/android/calendar/month/bo;->h:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 923
    iget-object v0, p0, Lcom/android/calendar/month/bo;->h:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/bo;->bQ:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 924
    iget-object v0, p0, Lcom/android/calendar/month/bo;->h:Landroid/text/TextPaint;

    iget v2, p0, Lcom/android/calendar/month/bo;->u:I

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 926
    iget-object v0, p0, Lcom/android/calendar/month/bo;->g:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->ascent()F

    move-result v0

    neg-float v0, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/month/bo;->q:I

    .line 928
    iget v0, p0, Lcom/android/calendar/month/bo;->q:I

    sget v2, Lcom/android/calendar/month/bo;->bQ:I

    int-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->q:I

    .line 930
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    .line 931
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 932
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 933
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/bo;->cd:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 934
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/bo;->bR:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 935
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    iget v2, p0, Lcom/android/calendar/month/bo;->x:I

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 936
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 937
    iget-object v0, p0, Lcom/android/calendar/month/bo;->i:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 939
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->j:Landroid/text/TextPaint;

    .line 940
    iget-object v0, p0, Lcom/android/calendar/month/bo;->j:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 941
    iget-object v0, p0, Lcom/android/calendar/month/bo;->j:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 942
    iget-object v0, p0, Lcom/android/calendar/month/bo;->j:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/bo;->cd:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 943
    iget-object v0, p0, Lcom/android/calendar/month/bo;->j:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/bo;->bQ:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 944
    iget-object v0, p0, Lcom/android/calendar/month/bo;->j:Landroid/text/TextPaint;

    iget v2, p0, Lcom/android/calendar/month/bo;->v:I

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 945
    iget-object v0, p0, Lcom/android/calendar/month/bo;->j:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 946
    iget-object v0, p0, Lcom/android/calendar/month/bo;->j:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 948
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    .line 949
    iget-object v0, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 950
    iget-object v0, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 951
    iget-object v0, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/bo;->bS:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 952
    iget-object v0, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/bo;->bm:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 953
    iget-object v0, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 954
    iget-object v0, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 956
    iget-object v0, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/month/bo;->r:I

    .line 958
    iget v0, p0, Lcom/android/calendar/month/bo;->r:I

    sget v2, Lcom/android/calendar/month/bo;->bS:I

    int-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->r:I

    .line 960
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    .line 961
    iget-object v0, p0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/bo;->cd:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 962
    iget-object v0, p0, Lcom/android/calendar/month/bo;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 964
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->m:Landroid/graphics/Paint;

    .line 965
    iget-object v0, p0, Lcom/android/calendar/month/bo;->m:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/bo;->cd:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 966
    iget-object v0, p0, Lcom/android/calendar/month/bo;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 968
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->n:Landroid/graphics/Paint;

    .line 971
    new-array v0, v8, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/month/bo;->ae:[Ljava/lang/String;

    .line 972
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ae:[Ljava/lang/String;

    invoke-virtual {v1, v9, v5}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 973
    iget-object v0, p0, Lcom/android/calendar/month/bo;->ae:[Ljava/lang/String;

    invoke-virtual {v1, v9, v8}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    .line 975
    const v0, 0x7f0f02d3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dF:Ljava/lang/String;

    .line 977
    const v0, 0x7f0f016d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->dG:Ljava/lang/String;

    .line 979
    return-void

    .line 870
    :cond_5
    const v0, 0x7f0f0125

    goto/16 :goto_0
.end method

.method protected b(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1277
    .line 1280
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v2, :cond_0

    .line 1281
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->top:I

    .line 1282
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/bo;->aS:I

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 1283
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->left:I

    .line 1284
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v4, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 1286
    iget-object v2, p0, Lcom/android/calendar/month/bo;->K:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1287
    iget-object v2, p0, Lcom/android/calendar/month/bo;->K:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1290
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->top:I

    .line 1291
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/bo;->aS:I

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 1293
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->left:I

    .line 1294
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/bo;->aR:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 1296
    iget-boolean v2, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v2, :cond_7

    move v2, v0

    .line 1300
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v3, v3, v2

    if-nez v3, :cond_2

    .line 1302
    iget-boolean v3, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-nez v3, :cond_1

    .line 1303
    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v1, v3, Landroid/graphics/Rect;->left:I

    .line 1309
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aM:[Z

    array-length v1, v1

    if-ge v2, v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v1, v1, v2

    if-nez v1, :cond_6

    .line 1310
    add-int/lit8 v2, v2, 0x1

    .line 1311
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sub-int v3, v2, v0

    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v3

    iput v3, v1, Landroid/graphics/Rect;->right:I

    .line 1313
    iget-object v1, p0, Lcom/android/calendar/month/bo;->L:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1314
    iget-object v1, p0, Lcom/android/calendar/month/bo;->L:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1315
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iput v3, v1, Landroid/graphics/Rect;->left:I

    goto :goto_1

    .line 1306
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v4, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Rect;->left:I

    goto :goto_1

    .line 1318
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    iget-object v1, p0, Lcom/android/calendar/month/bo;->aM:[Z

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-boolean v2, v2, v1

    if-nez v2, :cond_6

    .line 1319
    :cond_3
    add-int/lit8 v1, v1, -0x1

    if-lt v1, v0, :cond_4

    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_3

    .line 1322
    :cond_4
    :goto_2
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 1323
    add-int/lit8 v1, v1, 0x1

    .line 1324
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    sub-int v3, v1, v0

    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 1326
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 1327
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    add-int/lit8 v3, v1, 0x1

    sub-int/2addr v3, v0

    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 1332
    :goto_3
    iget-object v2, p0, Lcom/android/calendar/month/bo;->L:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1333
    iget-object v2, p0, Lcom/android/calendar/month/bo;->L:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 1329
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/bo;->aR:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto :goto_3

    .line 1337
    :cond_6
    return-void

    :cond_7
    move v0, v1

    move v2, v1

    goto/16 :goto_0
.end method

.method b(Landroid/graphics/Paint;I)V
    .locals 1

    .prologue
    .line 2669
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aV:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/month/bo;->c:I

    if-ne p2, v0, :cond_0

    .line 2670
    iget v0, p0, Lcom/android/calendar/month/bo;->y:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2672
    :cond_0
    return-void
.end method

.method protected c()V
    .locals 3

    .prologue
    .line 1143
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1170
    :cond_0
    return-void

    .line 1147
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1148
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_2

    .line 1155
    new-instance v2, Lcom/android/calendar/month/bp;

    invoke-direct {v2, p0}, Lcom/android/calendar/month/bp;-><init>(Lcom/android/calendar/month/bo;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method protected c(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1526
    .line 1529
    iget v1, p0, Lcom/android/calendar/month/bo;->ba:I

    .line 1530
    invoke-direct {p0, v2}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    invoke-direct {p0, v3}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v4

    sub-int v4, v0, v4

    .line 1531
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v10

    .line 1532
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->d()Z

    move-result v7

    .line 1533
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->e()Z

    move-result v11

    .line 1535
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sput v0, Lcom/android/calendar/month/bo;->bV:I

    .line 1537
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_a

    .line 1538
    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v5, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v0, v5

    div-int/lit8 v5, v0, 0x2

    .line 1539
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-eqz v0, :cond_4

    .line 1540
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    sget v6, Lcom/android/calendar/month/bo;->bX:I

    sub-int/2addr v0, v6

    .line 1544
    :goto_0
    const-string v6, "%d"

    new-array v8, v2, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/android/calendar/month/bo;->aL:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    int-to-float v5, v5

    int-to-float v0, v0

    iget-object v6, p0, Lcom/android/calendar/month/bo;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v5, v0, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1545
    add-int/lit8 v0, v1, 0x1

    move v8, v0

    move v9, v2

    .line 1551
    :goto_1
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-eqz v0, :cond_5

    .line 1552
    sget v0, Lcom/android/calendar/month/bo;->bU:I

    sget v1, Lcom/android/calendar/month/bo;->dj:I

    add-int v3, v0, v1

    .line 1554
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/bo;->di:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1555
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1570
    :goto_2
    iget v0, p0, Lcom/android/calendar/month/bo;->aW:I

    iget v1, p0, Lcom/android/calendar/month/bo;->aZ:I

    sub-int v6, v0, v1

    .line 1571
    if-gez v6, :cond_9

    .line 1572
    add-int/lit8 v6, v6, 0x7

    move v5, v2

    .line 1575
    :goto_3
    if-ge v5, v8, :cond_8

    .line 1576
    sub-int v0, v5, v9

    .line 1578
    iget-object v1, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v2, v2, v5

    invoke-virtual {p0, v1, v0, v2}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Paint;IZ)V

    .line 1581
    if-eqz v11, :cond_0

    .line 1582
    iget-object v1, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v5}, Lcom/android/calendar/month/bo;->d(Landroid/graphics/Paint;I)V

    .line 1586
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Paint;I)V

    .line 1588
    iget-object v1, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/month/bo;->b(Landroid/graphics/Paint;I)V

    .line 1590
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->e:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-eqz v0, :cond_7

    .line 1591
    :cond_1
    sub-int v0, v5, v9

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    div-int/lit8 v1, v4, 0x2

    add-int v2, v0, v1

    .line 1597
    :goto_4
    if-nez v10, :cond_2

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bL:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bM:Z

    if-eqz v0, :cond_3

    :cond_2
    move-object v0, p0

    move-object v1, p1

    .line 1598
    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;IIIIIZ)V

    .line 1602
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aL:[Ljava/lang/String;

    aget-object v0, v0, v5

    int-to-float v1, v2

    int-to-float v2, v3

    iget-object v12, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1575
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    .line 1542
    :cond_4
    sget v0, Lcom/android/calendar/month/bo;->bV:I

    sget v6, Lcom/android/calendar/month/bo;->bW:I

    add-int/2addr v0, v6

    goto/16 :goto_0

    .line 1556
    :cond_5
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->e:Z

    if-eqz v0, :cond_6

    .line 1557
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/android/calendar/month/bo;->p:I

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    .line 1559
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/bo;->bP:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1560
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    const/4 v1, 0x0

    const/4 v5, -0x1

    invoke-virtual {v0, v12, v1, v12, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1561
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_2

    .line 1563
    :cond_6
    sget v3, Lcom/android/calendar/month/bo;->bU:I

    .line 1565
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/bo;->bP:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1566
    iget-object v0, p0, Lcom/android/calendar/month/bo;->az:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto/16 :goto_2

    .line 1593
    :cond_7
    sub-int v0, v5, v9

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    sget v1, Lcom/android/calendar/month/bo;->cH:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/month/bo;->cQ:I

    add-int v2, v0, v1

    goto :goto_4

    .line 1604
    :cond_8
    return-void

    :cond_9
    move v5, v2

    goto/16 :goto_3

    :cond_a
    move v8, v1

    move v9, v3

    move v2, v3

    goto/16 :goto_1
.end method

.method c(Landroid/graphics/Paint;I)V
    .locals 1

    .prologue
    .line 2675
    .line 2676
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_0

    .line 2677
    add-int/lit8 p2, p2, 0x1

    .line 2680
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v0, v0, p2

    if-nez v0, :cond_1

    .line 2681
    invoke-virtual {p1}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->c(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2683
    :cond_1
    return-void
.end method

.method protected d()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    .line 1173
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1217
    :cond_0
    return-void

    .line 1177
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/month/bo;->g()V

    .line 1179
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->dC:[[J

    if-eqz v0, :cond_0

    .line 1183
    const/4 v0, -0x1

    .line 1184
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1185
    add-int/lit8 v1, v1, 0x1

    .line 1186
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_2

    .line 1190
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1191
    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1192
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dD:[[J

    aget-object v0, v0, v1

    sget v4, Lcom/android/calendar/month/bo;->cZ:I

    aget-wide v4, v0, v4

    cmp-long v0, v4, v8

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v0, v0, v1

    sget v4, Lcom/android/calendar/month/bo;->cZ:I

    aget-wide v4, v0, v4

    cmp-long v0, v4, v8

    if-nez v0, :cond_2

    .line 1195
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 1196
    invoke-virtual {v0}, Lcom/android/calendar/dh;->h()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1197
    invoke-virtual {v0}, Lcom/android/calendar/dh;->i()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1198
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dD:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->cZ:I

    aget-wide v4, v4, v5

    cmp-long v4, v4, v8

    if-nez v4, :cond_3

    .line 1199
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dD:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->cZ:I

    iget v6, v0, Lcom/android/calendar/dh;->z:I

    int-to-long v6, v6

    aput-wide v6, v4, v5

    .line 1200
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dD:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->da:I

    iget-wide v6, v0, Lcom/android/calendar/dh;->y:J

    aput-wide v6, v4, v5

    goto :goto_0

    .line 1203
    :cond_5
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->cZ:I

    aget-wide v4, v4, v5

    cmp-long v4, v4, v8

    if-nez v4, :cond_6

    iget v4, v0, Lcom/android/calendar/dh;->i:I

    iget v5, v0, Lcom/android/calendar/dh;->j:I

    if-ne v4, v5, :cond_6

    .line 1204
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->cZ:I

    iget v6, v0, Lcom/android/calendar/dh;->z:I

    int-to-long v6, v6

    aput-wide v6, v4, v5

    .line 1205
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->da:I

    iget-wide v6, v0, Lcom/android/calendar/dh;->y:J

    aput-wide v6, v4, v5

    goto :goto_0

    .line 1206
    :cond_6
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->cZ:I

    aget-wide v4, v4, v5

    cmp-long v4, v4, v8

    if-nez v4, :cond_3

    iget v4, v0, Lcom/android/calendar/dh;->i:I

    iget v5, v0, Lcom/android/calendar/dh;->j:I

    if-eq v4, v5, :cond_3

    .line 1207
    iget v4, p0, Lcom/android/calendar/month/bo;->aN:I

    add-int/2addr v4, v1

    .line 1208
    iget v5, v0, Lcom/android/calendar/dh;->i:I

    if-ne v4, v5, :cond_3

    .line 1209
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->cZ:I

    iget v6, v0, Lcom/android/calendar/dh;->z:I

    int-to-long v6, v6

    aput-wide v6, v4, v5

    .line 1210
    iget-object v4, p0, Lcom/android/calendar/month/bo;->dC:[[J

    aget-object v4, v4, v1

    sget v5, Lcom/android/calendar/month/bo;->da:I

    iget-wide v6, v0, Lcom/android/calendar/dh;->y:J

    aput-wide v6, v4, v5

    goto/16 :goto_0
.end method

.method protected d(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/16 v3, 0x3b

    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 1677
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1776
    :cond_0
    return-void

    .line 1683
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 1684
    iget-object v1, p0, Lcom/android/calendar/month/bo;->aa:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1685
    sget v1, Lcom/android/calendar/month/k;->ai:I

    iput v1, v0, Landroid/text/format/Time;->year:I

    .line 1686
    sget v1, Lcom/android/calendar/month/k;->ah:I

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 1687
    iput v6, v0, Landroid/text/format/Time;->monthDay:I

    .line 1688
    iput v10, v0, Landroid/text/format/Time;->second:I

    iput v10, v0, Landroid/text/format/Time;->minute:I

    iput v10, v0, Landroid/text/format/Time;->hour:I

    .line 1689
    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 1691
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 1692
    iget-object v2, p0, Lcom/android/calendar/month/bo;->aa:Ljava/lang/String;

    iput-object v2, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1693
    sget v2, Lcom/android/calendar/month/k;->ai:I

    iput v2, v1, Landroid/text/format/Time;->year:I

    .line 1694
    sget v2, Lcom/android/calendar/month/k;->ah:I

    iput v2, v1, Landroid/text/format/Time;->month:I

    .line 1695
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->monthDay:I

    .line 1696
    const/16 v2, 0x17

    iput v2, v1, Landroid/text/format/Time;->hour:I

    .line 1697
    iput v3, v1, Landroid/text/format/Time;->minute:I

    .line 1698
    iput v3, v1, Landroid/text/format/Time;->second:I

    .line 1699
    invoke-virtual {v1, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 1701
    invoke-virtual {v0, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->ac:I

    .line 1702
    invoke-virtual {v1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->ad:I

    .line 1705
    iget v0, p0, Lcom/android/calendar/month/bo;->ac:I

    iget v2, p0, Lcom/android/calendar/month/bo;->ad:I

    if-lt v0, v2, :cond_2

    .line 1706
    iget v0, p0, Lcom/android/calendar/month/bo;->ad:I

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bo;->ac:I

    .line 1711
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dx:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1712
    invoke-direct {p0}, Lcom/android/calendar/month/bo;->h()V

    move v0, v10

    .line 1713
    :goto_0
    iget v1, p0, Lcom/android/calendar/month/bo;->ba:I

    if-ge v0, v1, :cond_3

    .line 1714
    iget-object v1, p0, Lcom/android/calendar/month/bo;->dB:[I

    aput v10, v1, v0

    .line 1713
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1717
    :cond_3
    const/4 v6, -0x1

    .line 1722
    invoke-direct {p0}, Lcom/android/calendar/month/bo;->f()I

    move-result v8

    .line 1724
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_4
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/util/ArrayList;

    .line 1725
    add-int/lit8 v6, v6, 0x1

    .line 1726
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_4

    .line 1729
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1730
    sget v4, Lcom/android/calendar/month/bo;->bZ:I

    .line 1731
    invoke-direct {p0, v6}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v3

    .line 1732
    add-int/lit8 v0, v6, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v5

    .line 1736
    const/4 v0, 0x0

    .line 1737
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v6, v1, :cond_5

    .line 1738
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1742
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_b

    .line 1749
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dW:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1750
    sget v0, Lcom/android/calendar/month/bo;->ca:I

    add-int/2addr v4, v0

    .line 1753
    :cond_7
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    move-object v0, p0

    move-object v1, p1

    .line 1756
    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;Lcom/android/calendar/dh;IIIIII)I

    move-result v0

    .line 1761
    if-ne v0, v4, :cond_8

    .line 1768
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dz:[[Lcom/android/calendar/month/bw;

    aget-object v2, v0, v6

    array-length v3, v2

    move v1, v10

    move v0, v10

    :goto_3
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 1769
    sget-object v5, Lcom/android/calendar/month/bw;->b:Lcom/android/calendar/month/bw;

    if-ne v4, v5, :cond_a

    .line 1770
    add-int/lit8 v0, v0, 0x1

    .line 1768
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1745
    :cond_b
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    goto :goto_2

    .line 1774
    :cond_c
    iget-object v1, p0, Lcom/android/calendar/month/bo;->dB:[I

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v0, v2, v0

    aput v0, v1, v6

    goto/16 :goto_1
.end method

.method protected e(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 2066
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dy:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2068
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2070
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v0, v0, v11

    if-lez v0, :cond_1

    iget v0, p0, Lcom/android/calendar/month/bo;->ba:I

    if-ge v11, v0, :cond_1

    .line 2071
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v0, v0, v11

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;III)V

    .line 2114
    :cond_1
    return-void

    .line 2077
    :cond_2
    const/4 v6, -0x1

    .line 2079
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/ArrayList;

    .line 2080
    iput v11, p0, Lcom/android/calendar/month/bo;->dA:I

    .line 2081
    add-int/lit8 v6, v6, 0x1

    .line 2082
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 2083
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v0, v0, v6

    if-lez v0, :cond_3

    .line 2084
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v0, v0, v6

    add-int/lit8 v1, v6, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, v6}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;III)V

    goto :goto_0

    .line 2089
    :cond_5
    sget v4, Lcom/android/calendar/month/bo;->bZ:I

    .line 2090
    invoke-direct {p0, v6}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v0

    sget v1, Lcom/android/calendar/month/bo;->ce:I

    add-int v3, v0, v1

    .line 2091
    add-int/lit8 v0, v6, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v5

    .line 2092
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 2094
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dW:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2095
    sget v0, Lcom/android/calendar/month/bo;->ca:I

    add-int/2addr v4, v0

    .line 2098
    :cond_6
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/task/aa;

    move-object v0, p0

    move-object v1, p1

    .line 2100
    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;Lcom/android/calendar/task/aa;IIIII)I

    move-result v0

    .line 2104
    if-ne v0, v4, :cond_7

    .line 2109
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v1, v0, v6

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/android/calendar/month/bo;->dA:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    aput v1, v0, v6

    .line 2110
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v0, v0, v6

    if-lez v0, :cond_3

    .line 2111
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dB:[I

    aget v0, v0, v6

    invoke-virtual {p0, p1, v0, v5, v6}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;III)V

    goto :goto_0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 3236
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aU:Z

    return v0
.end method

.method protected f(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    .line 3261
    iget v0, p0, Lcom/android/calendar/month/bo;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 3262
    :goto_0
    const/4 v1, -0x1

    .line 3263
    const/16 v0, 0xc

    .line 3264
    if-eqz v2, :cond_b

    .line 3265
    const/16 v0, 0xb

    move v8, v0

    .line 3267
    :goto_1
    add-int/lit8 v0, v8, 0x1

    new-array v10, v0, [I

    .line 3269
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    if-nez v0, :cond_2

    .line 3329
    :cond_0
    return-void

    .line 3261
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 3273
    :cond_2
    const/4 v0, 0x0

    :goto_2
    array-length v2, v10

    if-ge v0, v2, :cond_3

    .line 3274
    const/4 v2, 0x0

    aput v2, v10, v0

    .line 3273
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3277
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3278
    add-int/lit8 v1, v1, 0x1

    .line 3279
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_4

    .line 3282
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    aput v0, v10, v1

    goto :goto_3

    .line 3285
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 3289
    const/4 v0, -0x1

    .line 3290
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v1, v0

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/ArrayList;

    .line 3291
    add-int/lit8 v9, v1, 0x1

    .line 3292
    if-nez v6, :cond_6

    move v1, v9

    .line 3293
    goto :goto_4

    .line 3296
    :cond_6
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 3297
    add-int/lit8 v0, v9, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v2

    .line 3307
    aget v0, v10, v9

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    aput v0, v10, v9

    .line 3308
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/ArrayList;

    .line 3310
    const/4 v3, 0x0

    :goto_5
    aget v0, v10, v9

    if-ge v3, v0, :cond_9

    if-ge v3, v8, :cond_9

    .line 3312
    if-ge v3, v12, :cond_7

    .line 3313
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 3314
    iget v1, v0, Lcom/android/calendar/dh;->c:I

    .line 3315
    iget v0, v0, Lcom/android/calendar/dh;->B:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_a

    .line 3316
    invoke-static {v1}, Lcom/android/calendar/hj;->c(I)I

    move-result v0

    :goto_6
    move v4, v0

    .line 3326
    :goto_7
    aget v5, v10, v9

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;IIII)V

    .line 3310
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 3319
    :cond_7
    sub-int v0, v3, v12

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/aa;

    .line 3320
    iget v1, v0, Lcom/android/calendar/task/aa;->i:I

    if-gez v1, :cond_8

    .line 3321
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v4

    goto :goto_7

    .line 3323
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    iget v0, v0, Lcom/android/calendar/task/aa;->i:I

    invoke-static {v1, v0}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v4

    goto :goto_7

    :cond_9
    move v1, v9

    .line 3328
    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_6

    :cond_b
    move v8, v0

    goto/16 :goto_1
.end method

.method protected g(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v7, 0x3b

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v10, 0x1

    .line 3369
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    sget v3, Lcom/android/calendar/month/bo;->cF:I

    sub-int v3, v0, v3

    .line 3371
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    div-int/lit8 v0, v0, 0x2

    iget v4, p0, Lcom/android/calendar/month/bo;->p:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 3373
    if-gt v3, v0, :cond_1

    .line 3496
    :cond_0
    return-void

    .line 3380
    :cond_1
    const/4 v0, 0x7

    new-array v4, v0, [I

    .line 3381
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 3384
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    .line 3385
    iget-object v0, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3386
    iget-object v0, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3391
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 3392
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aa:Ljava/lang/String;

    iput-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3393
    sget v5, Lcom/android/calendar/month/k;->ai:I

    iput v5, v0, Landroid/text/format/Time;->year:I

    .line 3394
    sget v5, Lcom/android/calendar/month/k;->ah:I

    iput v5, v0, Landroid/text/format/Time;->month:I

    .line 3395
    iput v10, v0, Landroid/text/format/Time;->monthDay:I

    .line 3396
    iput v1, v0, Landroid/text/format/Time;->second:I

    iput v1, v0, Landroid/text/format/Time;->minute:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 3397
    invoke-virtual {v0, v10}, Landroid/text/format/Time;->normalize(Z)J

    .line 3399
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 3400
    iget-object v6, p0, Lcom/android/calendar/month/bo;->aa:Ljava/lang/String;

    iput-object v6, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3401
    sget v6, Lcom/android/calendar/month/k;->ai:I

    iput v6, v5, Landroid/text/format/Time;->year:I

    .line 3402
    sget v6, Lcom/android/calendar/month/k;->ah:I

    iput v6, v5, Landroid/text/format/Time;->month:I

    .line 3403
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->monthDay:I

    .line 3404
    const/16 v6, 0x17

    iput v6, v5, Landroid/text/format/Time;->hour:I

    .line 3405
    iput v7, v5, Landroid/text/format/Time;->minute:I

    .line 3406
    iput v7, v5, Landroid/text/format/Time;->second:I

    .line 3407
    invoke-virtual {v5, v10}, Landroid/text/format/Time;->normalize(Z)J

    .line 3409
    invoke-virtual {v0, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-wide v8, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->ac:I

    .line 3410
    invoke-virtual {v5, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-wide v8, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bo;->ad:I

    move v0, v1

    .line 3413
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_2

    .line 3414
    aput v1, v4, v0

    .line 3413
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3417
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3418
    add-int/lit8 v1, v1, 0x1

    .line 3419
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eqz v6, :cond_3

    .line 3422
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    aput v0, v4, v1

    goto :goto_1

    .line 3425
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 3430
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3431
    add-int/lit8 v2, v2, 0x1

    .line 3432
    if-eqz v0, :cond_5

    .line 3435
    aget v5, v4, v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/2addr v0, v5

    aput v0, v4, v2

    .line 3436
    aget v0, v4, v2

    if-eqz v0, :cond_5

    .line 3442
    iget v0, p0, Lcom/android/calendar/month/bo;->aS:I

    div-int/lit8 v5, v0, 0x2

    .line 3443
    sget v0, Lcom/android/calendar/month/bo;->cw:I

    .line 3445
    iget v6, p0, Lcom/android/calendar/month/bo;->d:I

    if-ne v6, v10, :cond_6

    .line 3446
    iget v6, p0, Lcom/android/calendar/month/bo;->aS:I

    sget v7, Lcom/android/calendar/month/bo;->cw:I

    if-gt v6, v7, :cond_6

    .line 3447
    sget v0, Lcom/android/calendar/month/bo;->cy:I

    .line 3451
    :cond_6
    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v5

    .line 3454
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iput v3, v5, Landroid/graphics/Rect;->top:I

    .line 3456
    iget-boolean v5, p0, Lcom/android/calendar/month/bo;->aV:Z

    if-eqz v5, :cond_8

    iget v5, p0, Lcom/android/calendar/month/bo;->c:I

    if-ne v2, v5, :cond_8

    .line 3457
    iget-object v5, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    iget v6, p0, Lcom/android/calendar/month/bo;->H:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 3458
    iget-object v5, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    invoke-virtual {v5, v11}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 3459
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/android/calendar/month/bo;->s:I

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    .line 3461
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    if-le v5, v0, :cond_7

    .line 3462
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    add-int/lit8 v0, v0, -0x2

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    .line 3463
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget v6, p0, Lcom/android/calendar/month/bo;->s:I

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 3466
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-direct {p0, v2}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    invoke-direct {p0, v6}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v6

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iget v6, p0, Lcom/android/calendar/month/bo;->s:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 3468
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget v6, p0, Lcom/android/calendar/month/bo;->s:I

    add-int/2addr v5, v6

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 3469
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v6, p0, Lcom/android/calendar/month/bo;->s:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    iget-object v7, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 3471
    :cond_8
    iget-object v5, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    iget v6, p0, Lcom/android/calendar/month/bo;->I:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 3472
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/android/calendar/month/bo;->s:I

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    .line 3474
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    if-le v5, v0, :cond_9

    .line 3475
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    add-int/lit8 v0, v0, -0x2

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    .line 3476
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget v6, p0, Lcom/android/calendar/month/bo;->s:I

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 3479
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-direct {p0, v2}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    invoke-direct {p0, v6}, Lcom/android/calendar/month/bo;->b(I)I

    move-result v6

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iget v6, p0, Lcom/android/calendar/month/bo;->s:I

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 3481
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget v6, p0, Lcom/android/calendar/month/bo;->s:I

    add-int/2addr v5, v6

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 3483
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_b

    .line 3484
    add-int/lit8 v0, v2, 0x1

    .line 3486
    :goto_3
    iget-object v5, p0, Lcom/android/calendar/month/bo;->aM:[Z

    aget-boolean v0, v5, v0

    if-nez v0, :cond_a

    .line 3487
    iget-object v0, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/LightingColorFilter;

    iget v6, p0, Lcom/android/calendar/month/bo;->D:I

    iget v7, p0, Lcom/android/calendar/month/bo;->E:I

    invoke-direct {v5, v6, v7}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 3491
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v5, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v6, p0, Lcom/android/calendar/month/bo;->s:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    iget-object v7, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 3489
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/month/bo;->o:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_4

    :cond_b
    move v0, v2

    goto :goto_3
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 3241
    invoke-super {p0}, Lcom/android/calendar/month/cd;->onDetachedFromWindow()V

    .line 3242
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1044
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bo;->b(Landroid/graphics/Canvas;)V

    .line 1046
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->e:Z

    if-nez v0, :cond_0

    .line 1047
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/month/bo;->b(Landroid/graphics/Canvas;Z)V

    .line 1050
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bJ:Z

    if-eqz v0, :cond_1

    .line 1051
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;Z)V

    .line 1054
    :cond_1
    iget v0, p0, Lcom/android/calendar/month/bo;->aX:I

    iget v1, p0, Lcom/android/calendar/month/bo;->c:I

    if-eq v0, v1, :cond_2

    .line 1055
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/month/bo;->b(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1059
    :cond_2
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aV:Z

    if-eqz v0, :cond_3

    .line 1060
    invoke-direct {p0, p1}, Lcom/android/calendar/month/bo;->h(Landroid/graphics/Canvas;)V

    .line 1063
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bx:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->U:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->V:Z

    if-eqz v0, :cond_4

    .line 1065
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/month/bo;->c(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1068
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aw:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1070
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;)V

    .line 1072
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-eqz v0, :cond_8

    .line 1073
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bo;->f(Landroid/graphics/Canvas;)V

    .line 1080
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aJ:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->e:Z

    if-nez v0, :cond_5

    .line 1081
    invoke-direct {p0, p1}, Lcom/android/calendar/month/bo;->i(Landroid/graphics/Canvas;)V

    .line 1084
    :cond_5
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->bJ:Z

    if-eqz v0, :cond_6

    .line 1085
    invoke-direct {p0, p1, v2}, Lcom/android/calendar/month/bo;->a(Landroid/graphics/Canvas;Z)V

    .line 1088
    :cond_6
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->e:Z

    if-nez v0, :cond_7

    .line 1089
    invoke-direct {p0, p1, v2}, Lcom/android/calendar/month/bo;->b(Landroid/graphics/Canvas;Z)V

    .line 1092
    :cond_7
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bo;->c(Landroid/graphics/Canvas;)V

    .line 1094
    sget-object v0, Lcom/android/calendar/month/bo;->dI:Ljava/lang/String;

    const-string v1, "Executed"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    return-void

    .line 1074
    :cond_8
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->e:Z

    if-eqz v0, :cond_9

    .line 1075
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bo;->g(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1077
    :cond_9
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bo;->d(Landroid/graphics/Canvas;)V

    .line 1078
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/bo;->e(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2755
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 2756
    iget-object v0, p0, Lcom/android/calendar/month/bo;->em:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    move v0, v7

    .line 2881
    :goto_0
    return v0

    .line 2759
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dX:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v7

    .line 2760
    goto :goto_0

    .line 2762
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->d(Landroid/content/Context;)Z

    move-result v1

    .line 2763
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->l(Landroid/content/Context;)Z

    move-result v2

    .line 2764
    if-nez v1, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v8

    .line 2766
    :goto_1
    if-eqz v0, :cond_6

    .line 2767
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 2768
    if-nez v1, :cond_5

    move v0, v7

    .line 2769
    goto :goto_0

    :cond_3
    move v0, v7

    .line 2764
    goto :goto_1

    .line 2771
    :cond_4
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v8, :cond_5

    .line 2772
    if-nez v2, :cond_5

    move v0, v7

    .line 2773
    goto :goto_0

    .line 2777
    :cond_5
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v3, :cond_8

    if-eqz v1, :cond_8

    .line 2778
    invoke-direct {p0, p1}, Lcom/android/calendar/month/bo;->b(Landroid/view/MotionEvent;)Z

    .line 2786
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 2787
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2788
    :cond_7
    invoke-super {p0, p1}, Lcom/android/calendar/month/cd;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 2779
    :cond_8
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v8, :cond_6

    .line 2780
    invoke-direct {p0, p1}, Lcom/android/calendar/month/bo;->a(Landroid/view/MotionEvent;)Z

    goto :goto_2

    .line 2800
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_f

    .line 2801
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 2803
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/bo;->a(F)Landroid/text/format/Time;

    move-result-object v10

    .line 2804
    if-eqz v10, :cond_f

    iget-object v0, p0, Lcom/android/calendar/month/bo;->bO:Landroid/text/format/Time;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/calendar/month/bo;->bO:Landroid/text/format/Time;

    invoke-static {v10, v0}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v0

    if-eqz v0, :cond_f

    .line 2805
    :cond_a
    invoke-virtual {v10, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2806
    iget-object v1, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v6, 0x10

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    .line 2807
    const v1, 0x8000

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 2812
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2814
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2815
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 2816
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    .line 2818
    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_10

    iget v0, p0, Lcom/android/calendar/month/bo;->ab:I

    iget v1, p0, Lcom/android/calendar/month/bo;->av:I

    add-int/2addr v0, v1

    .line 2819
    :goto_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    int-to-float v4, v0

    sub-float/2addr v1, v4

    iget v4, p0, Lcom/android/calendar/month/bo;->ba:I

    int-to-float v4, v4

    mul-float/2addr v1, v4

    iget v4, p0, Lcom/android/calendar/month/bo;->aR:I

    sub-int v0, v4, v0

    iget v4, p0, Lcom/android/calendar/month/bo;->av:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    div-float v0, v1, v0

    float-to-int v1, v0

    .line 2821
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    if-eqz v0, :cond_e

    .line 2822
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2823
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2824
    iget-object v5, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 2829
    if-eqz v0, :cond_12

    .line 2830
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2832
    :goto_4
    if-eqz v1, :cond_b

    .line 2833
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 2836
    :cond_b
    if-lez v0, :cond_c

    .line 2837
    const-string v1, ""

    .line 2839
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0e0001

    invoke-virtual {v1, v5, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2841
    const-string v1, ", "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2842
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2847
    :cond_c
    :goto_5
    if-lez v7, :cond_d

    .line 2848
    const-string v0, ""

    .line 2850
    :try_start_1
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0003

    invoke-virtual {v0, v1, v7}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2852
    const-string v1, ", "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2853
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2859
    :cond_d
    :goto_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-gtz v0, :cond_11

    .line 2860
    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2861
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dF:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2867
    :goto_7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2869
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2875
    :cond_e
    invoke-virtual {p0, v9}, Lcom/android/calendar/month/bo;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2876
    invoke-virtual {p0, v2}, Lcom/android/calendar/month/bo;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2878
    iput-object v10, p0, Lcom/android/calendar/month/bo;->bO:Landroid/text/format/Time;

    :cond_f
    move v0, v8

    .line 2881
    goto/16 :goto_0

    .line 2818
    :cond_10
    iget v0, p0, Lcom/android/calendar/month/bo;->av:I

    goto/16 :goto_3

    .line 2843
    :catch_0
    move-exception v0

    .line 2844
    const-string v0, "MonthView"

    const-string v1, "Resource Not Found For eventCountString"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 2854
    :catch_1
    move-exception v0

    .line 2855
    const-string v0, "MonthView"

    const-string v1, "Resource Not Found For taskCountString"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 2863
    :cond_11
    const-string v0, ". "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2864
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dG:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_12
    move v0, v7

    goto/16 :goto_4
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 2726
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/calendar/month/cd;->onSizeChanged(IIII)V

    .line 2727
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3184
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 3185
    packed-switch v0, :pswitch_data_0

    .line 3220
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Lcom/android/calendar/month/cd;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 3190
    :pswitch_1
    sput-object p0, Lcom/android/calendar/month/h;->s:Landroid/view/View;

    .line 3191
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->t:F

    .line 3193
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->u:F

    .line 3194
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->v:F

    goto :goto_0

    .line 3199
    :pswitch_2
    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/calendar/month/bo;->W:Z

    if-eqz v0, :cond_1

    .line 3200
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 3201
    if-eqz v0, :cond_0

    .line 3202
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 3203
    const v1, 0x7f12006d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 3205
    if-eqz v0, :cond_0

    .line 3206
    instance-of v1, v0, Lcom/android/calendar/month/k;

    if-eqz v1, :cond_0

    .line 3207
    check-cast v0, Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->f()V

    .line 3212
    :cond_0
    sput-boolean v2, Lcom/android/calendar/month/h;->w:Z

    .line 3214
    :cond_1
    sput-boolean v2, Lcom/android/calendar/month/bo;->W:Z

    goto :goto_0

    .line 3185
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setEvents(Ljava/util/List;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 495
    iget-object v2, p0, Lcom/android/calendar/month/bo;->dx:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 497
    if-nez p1, :cond_1

    .line 498
    iput-object v4, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    .line 538
    :cond_0
    :goto_0
    return-void

    .line 502
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lcom/android/calendar/month/bo;->ba:I

    if-eq v2, v3, :cond_3

    .line 503
    const-string v0, "MonthView"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 504
    const-string v0, "MonthView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Events size must be same as days displayed: size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " days="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/month/bo;->ba:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :cond_2
    iput-object v4, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    goto :goto_0

    .line 514
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    if-eqz v2, :cond_5

    .line 515
    iget-object v2, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    .line 516
    :goto_1
    if-ge v2, v3, :cond_6

    .line 517
    iget-object v4, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eq v4, v5, :cond_4

    move v0, v1

    .line 516
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    .line 525
    :cond_6
    iput-object p1, p0, Lcom/android/calendar/month/bo;->bD:Ljava/util/List;

    .line 527
    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/android/calendar/month/bo;->aI:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 529
    iget-object v0, p0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->s(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/bo;->ee:Z

    .line 532
    :cond_7
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->c()V

    .line 534
    sget-boolean v0, Lcom/android/calendar/dz;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->dH:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/bo;->e:Z

    if-nez v0, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/android/calendar/month/bo;->d()V

    goto/16 :goto_0
.end method

.method public setTasks(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 541
    iput-object p1, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    .line 542
    iget-object v0, p0, Lcom/android/calendar/month/bo;->dy:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 543
    if-nez p1, :cond_1

    .line 554
    :cond_0
    :goto_0
    return-void

    .line 546
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/bo;->ba:I

    if-eq v0, v1, :cond_0

    .line 547
    const-string v0, "MonthView"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548
    const-string v0, "MonthView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tasks size must be same as days displayed: size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " days="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/month/bo;->ba:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/month/bo;->bE:Ljava/util/List;

    goto :goto_0
.end method
