.class Lcom/android/calendar/month/au;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "MonthEventList.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/month/MonthEventList;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/MonthEventList;)V
    .locals 0

    .prologue
    .line 831
    iput-object p1, p0, Lcom/android/calendar/month/au;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .prologue
    .line 834
    iget-object v0, p0, Lcom/android/calendar/month/au;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    if-nez v0, :cond_0

    .line 835
    const/4 v0, 0x0

    .line 861
    :goto_0
    return v0

    .line 838
    :cond_0
    sget v0, Lcom/android/calendar/month/k;->u:I

    sget v1, Lcom/android/calendar/month/k;->s:I

    sub-int/2addr v0, v1

    .line 839
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 840
    sget v1, Lcom/android/calendar/month/k;->v:I

    sget v2, Lcom/android/calendar/month/k;->t:I

    sub-int/2addr v1, v2

    .line 841
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 843
    const/16 v2, 0x4b

    if-lt v0, v2, :cond_1

    if-le v0, v1, :cond_1

    .line 845
    iget-object v0, p0, Lcom/android/calendar/month/au;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    const/4 v5, 0x1

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/month/k;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFZ)Z

    .line 847
    iget-object v0, p0, Lcom/android/calendar/month/au;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 850
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 853
    iget-object v1, p0, Lcom/android/calendar/month/au;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v1, v1, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 855
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 858
    const/4 v0, 0x1

    goto :goto_0

    .line 861
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
