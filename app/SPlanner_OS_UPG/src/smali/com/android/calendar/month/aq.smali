.class Lcom/android/calendar/month/aq;
.super Ljava/lang/Object;
.source "MonthEventList.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/MonthEventList;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/MonthEventList;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v8, 0x1

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 277
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    if-nez v0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return v7

    .line 280
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0}, Lcom/android/calendar/month/MonthEventList;->b(Lcom/android/calendar/month/MonthEventList;)I

    move-result v0

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->m()Z

    move-result v0

    if-nez v0, :cond_3

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0}, Lcom/android/calendar/month/MonthEventList;->c(Lcom/android/calendar/month/MonthEventList;)Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    goto :goto_0

    .line 283
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->r()Z

    move-result v0

    .line 284
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 285
    packed-switch v1, :pswitch_data_0

    .line 339
    :cond_4
    :goto_1
    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0}, Lcom/android/calendar/month/MonthEventList;->c(Lcom/android/calendar/month/MonthEventList;)Landroid/view/GestureDetector;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    goto :goto_0

    .line 287
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-static {v1, v2}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;F)F

    .line 289
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v1, v1, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_4

    .line 294
    iget-object v2, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v2, v2, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v2

    .line 296
    if-nez v2, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-nez v1, :cond_4

    .line 297
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v1, v8}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;Z)Z

    goto :goto_1

    .line 303
    :pswitch_1
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-static {v1, v2}, Lcom/android/calendar/month/MonthEventList;->b(Lcom/android/calendar/month/MonthEventList;F)F

    .line 305
    if-eqz v0, :cond_4

    .line 306
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v1}, Lcom/android/calendar/month/MonthEventList;->f(Lcom/android/calendar/month/MonthEventList;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 307
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v1}, Lcom/android/calendar/month/MonthEventList;->d(Lcom/android/calendar/month/MonthEventList;)F

    move-result v1

    cmpl-float v1, v1, v5

    if-ltz v1, :cond_4

    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v1}, Lcom/android/calendar/month/MonthEventList;->e(Lcom/android/calendar/month/MonthEventList;)F

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v2}, Lcom/android/calendar/month/MonthEventList;->d(Lcom/android/calendar/month/MonthEventList;)F

    move-result v2

    sub-float/2addr v1, v2

    sget v2, Lcom/android/calendar/month/MonthEventList;->p:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 309
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 311
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0, v7}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;Z)Z

    .line 312
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0, v3}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;F)F

    .line 313
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/month/k;->a(J)V

    .line 314
    iget-object v0, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 317
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 320
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v1, v1, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 322
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    move v7, v8

    .line 324
    goto/16 :goto_0

    .line 332
    :pswitch_2
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v1, v7}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;Z)Z

    .line 333
    iget-object v1, p0, Lcom/android/calendar/month/aq;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v1, v3}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;F)F

    goto/16 :goto_1

    .line 285
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
