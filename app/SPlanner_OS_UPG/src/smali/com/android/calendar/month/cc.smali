.class public Lcom/android/calendar/month/cc;
.super Ljava/lang/Object;
.source "SimpleDayPickerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected b:Landroid/widget/AbsListView;

.field protected c:I

.field final synthetic d:Lcom/android/calendar/month/by;


# direct methods
.method protected constructor <init>(Lcom/android/calendar/month/by;)V
    .locals 0

    .prologue
    .line 710
    iput-object p1, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/AbsListView;I)V
    .locals 4

    .prologue
    .line 722
    iput-object p1, p0, Lcom/android/calendar/month/cc;->b:Landroid/widget/AbsListView;

    .line 723
    iget-object v0, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    iget-object v0, v0, Lcom/android/calendar/month/by;->W:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 724
    iput p2, p0, Lcom/android/calendar/month/cc;->c:I

    .line 725
    iget-object v0, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    iget-object v0, v0, Lcom/android/calendar/month/by;->W:Landroid/os/Handler;

    const-wide/16 v2, 0x28

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 726
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 730
    iget-object v0, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    iget v1, p0, Lcom/android/calendar/month/cc;->c:I

    iput v1, v0, Lcom/android/calendar/month/by;->am:I

    .line 731
    const-string v0, "MonthFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 732
    const-string v0, "MonthFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new scroll state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/month/cc;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " old state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    iget v2, v2, Lcom/android/calendar/month/by;->al:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    :cond_0
    iget v0, p0, Lcom/android/calendar/month/cc;->c:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    iget v0, v0, Lcom/android/calendar/month/by;->al:I

    if-eqz v0, :cond_1

    .line 737
    iget-object v0, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    iget v1, p0, Lcom/android/calendar/month/cc;->c:I

    iput v1, v0, Lcom/android/calendar/month/by;->al:I

    .line 760
    iget-object v0, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    iget-object v0, v0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    sget v1, Lcom/android/calendar/month/by;->ah:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->c(I)V

    .line 764
    :goto_0
    return-void

    .line 762
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/cc;->d:Lcom/android/calendar/month/by;

    iget v1, p0, Lcom/android/calendar/month/cc;->c:I

    iput v1, v0, Lcom/android/calendar/month/by;->al:I

    goto :goto_0
.end method
