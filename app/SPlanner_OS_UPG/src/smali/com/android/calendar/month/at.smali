.class Lcom/android/calendar/month/at;
.super Ljava/lang/Object;
.source "MonthEventList.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/MonthEventList;


# direct methods
.method private constructor <init>(Lcom/android/calendar/month/MonthEventList;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/month/MonthEventList;Lcom/android/calendar/month/ao;)V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0, p1}, Lcom/android/calendar/month/at;-><init>(Lcom/android/calendar/month/MonthEventList;)V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 488
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0}, Lcom/android/calendar/month/MonthEventList;->g(Lcom/android/calendar/month/MonthEventList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 490
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v9, v8

    .line 542
    :goto_0
    return v9

    .line 493
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v2}, Lcom/android/calendar/month/MonthEventList;->h(Lcom/android/calendar/month/MonthEventList;)I

    move-result v2

    if-gt v1, v2, :cond_2

    move v9, v8

    .line 494
    goto :goto_0

    .line 497
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v1}, Lcom/android/calendar/month/MonthEventList;->h(Lcom/android/calendar/month/MonthEventList;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/calendar/month/ba;

    .line 499
    invoke-static {}, Lcom/android/calendar/month/MonthEventList;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v9, v8

    .line 500
    goto :goto_0

    .line 503
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 505
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0, v1, v8}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/ba;Z)V

    goto :goto_0

    .line 509
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0, v1, v9}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/ba;Z)V

    goto :goto_0

    .line 513
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/ba;)V

    goto :goto_0

    .line 517
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0, v1}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;Lcom/android/calendar/month/ba;)V

    goto :goto_0

    .line 521
    :pswitch_4
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0, v1, v8}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/ba;Z)V

    goto :goto_0

    .line 525
    :pswitch_5
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0, v1, v9}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/ba;Z)V

    goto :goto_0

    .line 529
    :pswitch_6
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    const-wide/16 v2, 0x10

    iget-wide v6, v1, Lcom/android/calendar/month/ba;->f:J

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v9}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    goto :goto_0

    .line 536
    :pswitch_7
    iget-object v0, p0, Lcom/android/calendar/month/at;->a:Lcom/android/calendar/month/MonthEventList;

    invoke-static {v0, v1}, Lcom/android/calendar/month/MonthEventList;->a(Lcom/android/calendar/month/MonthEventList;Lcom/android/calendar/month/ba;)V

    goto :goto_0

    .line 503
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
