.class Lcom/android/calendar/month/s;
.super Ljava/lang/Object;
.source "MonthByWeekFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/k;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/k;)V
    .locals 0

    .prologue
    .line 2868
    iput-object p1, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2880
    sput-boolean v3, Lcom/android/calendar/month/k;->y:Z

    .line 2882
    iget-object v0, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    iget-boolean v0, v0, Lcom/android/calendar/month/k;->x:Z

    if-eqz v0, :cond_0

    .line 2895
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    iget-object v0, v0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 2896
    return-void

    .line 2885
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    invoke-virtual {v1}, Lcom/android/calendar/month/k;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b009a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2886
    iget-object v0, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    invoke-static {v1}, Lcom/android/calendar/month/k;->q(Lcom/android/calendar/month/k;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2888
    iget-object v0, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2890
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2891
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2893
    iget-object v0, p0, Lcom/android/calendar/month/s;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    invoke-virtual {v0}, Lcom/android/calendar/month/MonthListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2876
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 2871
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/k;->y:Z

    .line 2872
    return-void
.end method
