.class Lcom/android/calendar/month/m;
.super Ljava/lang/Object;
.source "MonthByWeekFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/text/format/Time;

.field final synthetic b:Landroid/text/format/Time;

.field final synthetic c:Lcom/android/calendar/al;

.field final synthetic d:Lcom/android/calendar/month/k;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/k;Landroid/text/format/Time;Landroid/text/format/Time;Lcom/android/calendar/al;)V
    .locals 0

    .prologue
    .line 1626
    iput-object p1, p0, Lcom/android/calendar/month/m;->d:Lcom/android/calendar/month/k;

    iput-object p2, p0, Lcom/android/calendar/month/m;->a:Landroid/text/format/Time;

    iput-object p3, p0, Lcom/android/calendar/month/m;->b:Landroid/text/format/Time;

    iput-object p4, p0, Lcom/android/calendar/month/m;->c:Lcom/android/calendar/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1629
    sget-boolean v0, Lcom/android/calendar/month/h;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/m;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->j(Lcom/android/calendar/month/k;)Lcom/android/calendar/AllInOneActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1630
    iget-object v0, p0, Lcom/android/calendar/month/m;->d:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/month/n;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/n;-><init>(Lcom/android/calendar/month/m;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1639
    :goto_0
    return-void

    .line 1637
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/m;->d:Lcom/android/calendar/month/k;

    iget-object v1, p0, Lcom/android/calendar/month/m;->c:Lcom/android/calendar/al;

    iget-object v2, p0, Lcom/android/calendar/month/m;->a:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/month/m;->b:Landroid/text/format/Time;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/calendar/month/k;->a(Lcom/android/calendar/al;Landroid/text/format/Time;Landroid/text/format/Time;)V

    goto :goto_0
.end method
