.class final enum Lcom/android/calendar/month/bm;
.super Ljava/lang/Enum;
.source "MonthHoverEventFragment.java"


# static fields
.field public static final enum a:Lcom/android/calendar/month/bm;

.field public static final enum b:Lcom/android/calendar/month/bm;

.field public static final enum c:Lcom/android/calendar/month/bm;

.field public static final enum d:Lcom/android/calendar/month/bm;

.field public static final enum e:Lcom/android/calendar/month/bm;

.field private static final synthetic f:[Lcom/android/calendar/month/bm;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 165
    new-instance v0, Lcom/android/calendar/month/bm;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/bm;->a:Lcom/android/calendar/month/bm;

    new-instance v0, Lcom/android/calendar/month/bm;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/month/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/bm;->b:Lcom/android/calendar/month/bm;

    new-instance v0, Lcom/android/calendar/month/bm;

    const-string v1, "UP"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/month/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/bm;->c:Lcom/android/calendar/month/bm;

    new-instance v0, Lcom/android/calendar/month/bm;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v5}, Lcom/android/calendar/month/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/bm;->d:Lcom/android/calendar/month/bm;

    new-instance v0, Lcom/android/calendar/month/bm;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v6}, Lcom/android/calendar/month/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/bm;->e:Lcom/android/calendar/month/bm;

    .line 164
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/calendar/month/bm;

    sget-object v1, Lcom/android/calendar/month/bm;->a:Lcom/android/calendar/month/bm;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/month/bm;->b:Lcom/android/calendar/month/bm;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/month/bm;->c:Lcom/android/calendar/month/bm;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/calendar/month/bm;->d:Lcom/android/calendar/month/bm;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/calendar/month/bm;->e:Lcom/android/calendar/month/bm;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/calendar/month/bm;->f:[Lcom/android/calendar/month/bm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/month/bm;
    .locals 1

    .prologue
    .line 164
    const-class v0, Lcom/android/calendar/month/bm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/bm;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/month/bm;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/android/calendar/month/bm;->f:[Lcom/android/calendar/month/bm;

    invoke-virtual {v0}, [Lcom/android/calendar/month/bm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/month/bm;

    return-object v0
.end method
