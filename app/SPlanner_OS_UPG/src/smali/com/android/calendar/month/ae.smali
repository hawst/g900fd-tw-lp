.class Lcom/android/calendar/month/ae;
.super Landroid/content/AsyncQueryHandler;
.source "MonthByWeekFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/month/k;


# direct methods
.method public constructor <init>(Lcom/android/calendar/month/k;Landroid/content/ContentResolver;)V
    .locals 4

    .prologue
    .line 1987
    iput-object p1, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    .line 1988
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 1990
    iget-object v0, p1, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p1, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iget v1, p1, Lcom/android/calendar/month/k;->Q:I

    mul-int/lit8 v1, v1, 0x7

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p1, Lcom/android/calendar/month/k;->d:I

    .line 1992
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    .line 1996
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2087
    :cond_0
    :goto_0
    return-void

    .line 2000
    :cond_1
    if-nez p1, :cond_6

    .line 2001
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->n(Lcom/android/calendar/month/k;)Ljava/lang/Runnable;

    move-result-object v8

    monitor-enter v8

    .line 2002
    :try_start_0
    const-string v0, "MonthFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2003
    const-string v0, "MonthFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cursor entries for uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v2}, Lcom/android/calendar/month/k;->e(Lcom/android/calendar/month/k;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 2006
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2007
    iget-object v1, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v2, v1, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v3, v1, Lcom/android/calendar/month/k;->d:I

    iget-object v1, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v4, v1, Lcom/android/calendar/month/k;->e:I

    const/4 v5, 0x1

    move-object v1, p3

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;IIZ)V

    .line 2009
    invoke-static {}, Lcom/android/calendar/dz;->t()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2010
    iget-object v1, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v2, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v2, v2, Lcom/android/calendar/month/k;->d:I

    iget-object v3, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v3, v3, Lcom/android/calendar/month/k;->e:I

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/calendar/month/k;->a(Ljava/util/ArrayList;II)V

    .line 2013
    :cond_3
    if-eqz p3, :cond_4

    .line 2014
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 2017
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-boolean v1, v1, Lcom/android/calendar/month/k;->a:Z

    if-eqz v1, :cond_5

    .line 2018
    iget-object v1, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    iget-object v2, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v2, v2, Lcom/android/calendar/month/k;->d:I

    iget-object v3, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v3, v3, Lcom/android/calendar/month/k;->e:I

    iget-object v4, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v4, v4, Lcom/android/calendar/month/k;->d:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/calendar/month/ce;->b(IILjava/util/ArrayList;)V

    .line 2033
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    sget-object v1, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/android/calendar/month/k;->b(Lcom/android/calendar/month/k;Landroid/net/Uri;)Landroid/net/Uri;

    .line 2034
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->f(Lcom/android/calendar/month/k;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v3}, Lcom/android/calendar/month/k;->h(Lcom/android/calendar/month/k;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/calendar/task/aa;->a:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v5}, Lcom/android/calendar/month/k;->i(Lcom/android/calendar/month/k;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "due_date ASC, importance DESC"

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2037
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2084
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-boolean v0, v0, Lcom/android/calendar/month/k;->a:Z

    if-nez v0, :cond_0

    .line 2085
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    check-cast v0, Lcom/android/calendar/month/h;

    invoke-virtual {v0}, Lcom/android/calendar/month/h;->f()V

    goto/16 :goto_0

    .line 2021
    :cond_5
    :try_start_1
    iget-object v1, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    check-cast v1, Lcom/android/calendar/month/h;

    iget-object v2, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v2, v2, Lcom/android/calendar/month/k;->d:I

    iget-object v3, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v3, v3, Lcom/android/calendar/month/k;->e:I

    iget-object v4, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v4, v4, Lcom/android/calendar/month/k;->d:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/calendar/month/h;->a(IILjava/util/ArrayList;)V

    goto :goto_1

    .line 2037
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2039
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->o(Lcom/android/calendar/month/k;)Ljava/lang/Runnable;

    move-result-object v1

    monitor-enter v1

    .line 2040
    :try_start_2
    const-string v0, "MonthFragment"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2041
    const-string v0, "MonthFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " task cursor entries for uri "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v3}, Lcom/android/calendar/month/k;->h(Lcom/android/calendar/month/k;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 2044
    :cond_7
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2046
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    invoke-static {v2, v0, p3}, Lcom/android/calendar/task/aa;->a(Ljava/util/ArrayList;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 2048
    if-eqz p3, :cond_8

    .line 2049
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 2052
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-boolean v0, v0, Lcom/android/calendar/month/k;->a:Z

    if-eqz v0, :cond_b

    .line 2053
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    iget-object v3, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v3, v3, Lcom/android/calendar/month/k;->d:I

    iget-object v4, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v4, v4, Lcom/android/calendar/month/k;->e:I

    iget-object v5, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget v5, v5, Lcom/android/calendar/month/k;->d:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v5, v5, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget-object v5, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/android/calendar/month/ce;->a(IILjava/util/ArrayList;Ljava/lang/String;)V

    .line 2059
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v2, Lcom/android/calendar/month/af;

    invoke-direct {v2, p0}, Lcom/android/calendar/month/af;-><init>(Lcom/android/calendar/month/ae;)V

    const-wide/16 v4, 0xa

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2066
    sget-boolean v0, Lcom/android/calendar/month/k;->q:Z

    if-eqz v0, :cond_a

    .line 2067
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    .line 2069
    if-eqz v0, :cond_9

    .line 2070
    invoke-static {}, Lcom/android/calendar/month/k;->s()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2071
    iget-object v2, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v2, v2, Lcom/android/calendar/month/k;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->b(Landroid/view/animation/Animation;)V

    .line 2072
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->g:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v2}, Lcom/android/calendar/month/k;->p(Lcom/android/calendar/month/k;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2078
    :cond_9
    :goto_4
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/calendar/month/k;->b(Z)V

    .line 2080
    :cond_a
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/k;->w:Z

    .line 2081
    monitor-exit v1

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 2056
    :cond_b
    :try_start_3
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    check-cast v0, Lcom/android/calendar/month/h;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/h;->a(Ljava/util/ArrayList;)V

    goto :goto_3

    .line 2074
    :cond_c
    iget-object v2, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v2, v2, Lcom/android/calendar/month/k;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/view/animation/Animation;)V

    .line 2075
    iget-object v0, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->f:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/android/calendar/month/ae;->a:Lcom/android/calendar/month/k;

    invoke-static {v2}, Lcom/android/calendar/month/k;->p(Lcom/android/calendar/month/k;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4
.end method
