.class Lcom/android/calendar/month/aa;
.super Ljava/lang/Object;
.source "MonthByWeekFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/al;

.field final synthetic b:Landroid/text/format/Time;

.field final synthetic c:Landroid/text/format/Time;

.field final synthetic d:Lcom/android/calendar/month/k;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/k;Lcom/android/calendar/al;Landroid/text/format/Time;Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 1547
    iput-object p1, p0, Lcom/android/calendar/month/aa;->d:Lcom/android/calendar/month/k;

    iput-object p2, p0, Lcom/android/calendar/month/aa;->a:Lcom/android/calendar/al;

    iput-object p3, p0, Lcom/android/calendar/month/aa;->b:Landroid/text/format/Time;

    iput-object p4, p0, Lcom/android/calendar/month/aa;->c:Landroid/text/format/Time;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/android/calendar/month/aa;->a:Lcom/android/calendar/al;

    iget-object v1, p0, Lcom/android/calendar/month/aa;->d:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v2, 0x20

    iget-object v4, p0, Lcom/android/calendar/month/aa;->b:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/month/aa;->b:Landroid/text/format/Time;

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1553
    iget-object v0, p0, Lcom/android/calendar/month/aa;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->j(Lcom/android/calendar/month/k;)Lcom/android/calendar/AllInOneActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->r(Landroid/content/Context;)Landroid/text/format/Time;

    move-result-object v1

    .line 1554
    sget-boolean v0, Lcom/android/calendar/month/h;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/aa;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->j(Lcom/android/calendar/month/k;)Lcom/android/calendar/AllInOneActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->y(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1555
    iget-object v0, p0, Lcom/android/calendar/month/aa;->d:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/month/ab;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/ab;-><init>(Lcom/android/calendar/month/aa;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1564
    :goto_0
    return-void

    .line 1562
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/aa;->d:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->l(Lcom/android/calendar/month/k;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x1

    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, -0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    goto :goto_0
.end method
