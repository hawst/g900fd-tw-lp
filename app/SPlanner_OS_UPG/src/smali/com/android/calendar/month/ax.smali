.class Lcom/android/calendar/month/ax;
.super Ljava/lang/Object;
.source "MonthEventListFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/av;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/av;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 162
    monitor-enter p0

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->c(Lcom/android/calendar/month/av;)V

    .line 166
    iget-object v0, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    invoke-virtual {v0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 167
    monitor-exit p0

    .line 176
    :goto_0
    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    iget-object v1, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    invoke-static {v1}, Lcom/android/calendar/month/av;->d(Lcom/android/calendar/month/av;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/av;Landroid/net/Uri;)Landroid/net/Uri;

    .line 170
    iget-object v0, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->g(Lcom/android/calendar/month/av;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    invoke-static {v3}, Lcom/android/calendar/month/av;->e(Lcom/android/calendar/month/av;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/calendar/month/ba;->a:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    invoke-static {v5}, Lcom/android/calendar/month/av;->f(Lcom/android/calendar/month/av;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "allDay DESC,startDay,startMinute,title"

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    sget-object v0, Lcom/android/calendar/month/av;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    sget-object v0, Lcom/android/calendar/month/av;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Started loader with uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/ax;->a:Lcom/android/calendar/month/av;

    invoke-static {v2}, Lcom/android/calendar/month/av;->e(Lcom/android/calendar/month/av;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
