.class public Lcom/android/calendar/month/MonthDayHeader;
.super Landroid/view/View;
.source "MonthDayHeader.java"


# instance fields
.field a:Landroid/graphics/drawable/Drawable;

.field b:Landroid/graphics/Paint;

.field c:Ljava/lang/String;

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:I

.field i:I

.field j:I

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:I

.field t:I

.field u:I

.field v:Z

.field w:[I

.field x:[Ljava/lang/String;

.field y:[Ljava/lang/String;

.field private z:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 91
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthDayHeader;->a(Landroid/content/Context;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthDayHeader;->a(Landroid/content/Context;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthDayHeader;->a(Landroid/content/Context;)V

    .line 102
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthDayHeader;->getWidth()I

    move-result v0

    .line 281
    iget-boolean v1, p0, Lcom/android/calendar/month/MonthDayHeader;->v:Z

    if-eqz v1, :cond_0

    .line 282
    iget v1, p0, Lcom/android/calendar/month/MonthDayHeader;->o:I

    sub-int/2addr p1, v1

    .line 283
    iget v1, p0, Lcom/android/calendar/month/MonthDayHeader;->o:I

    sub-int/2addr v0, v1

    .line 286
    :cond_0
    div-int/lit8 v0, v0, 0x7

    .line 288
    div-int v0, p1, v0

    .line 291
    const/4 v1, 0x7

    if-lt v0, v1, :cond_1

    .line 292
    const/4 v0, 0x6

    .line 295
    :cond_1
    iget-boolean v1, p0, Lcom/android/calendar/month/MonthDayHeader;->v:Z

    if-eqz v1, :cond_3

    .line 296
    if-gez p1, :cond_2

    .line 297
    const/4 v0, -0x1

    .line 305
    :cond_2
    :goto_0
    iput v0, p0, Lcom/android/calendar/month/MonthDayHeader;->i:I

    .line 306
    return-void

    .line 300
    :cond_3
    if-gez v0, :cond_2

    .line 301
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 107
    iput-object p1, p0, Lcom/android/calendar/month/MonthDayHeader;->z:Landroid/content/Context;

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/android/calendar/month/MonthDayHeader;->j:I

    .line 113
    const v1, 0x7f0a000a

    invoke-static {p1, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/MonthDayHeader;->e:Z

    .line 115
    iget-object v1, p0, Lcom/android/calendar/month/MonthDayHeader;->z:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/MonthDayHeader;->d:Z

    .line 119
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0b0096

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/month/MonthDayHeader;->a:Landroid/graphics/drawable/Drawable;

    .line 123
    const v1, 0x7f0f0474

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/MonthDayHeader;->c:Ljava/lang/String;

    .line 127
    const v1, 0x7f0b0123

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthDayHeader;->k:I

    .line 129
    const v1, 0x7f0b00ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthDayHeader;->l:I

    .line 131
    const v1, 0x7f0b00e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthDayHeader;->m:I

    .line 133
    const v1, 0x7f0b0097

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthDayHeader;->n:I

    .line 137
    const v1, 0x7f0c0399

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthDayHeader;->p:I

    .line 139
    const v1, 0x7f0c00be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthDayHeader;->q:I

    .line 141
    const v1, 0x7f0c010a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthDayHeader;->r:I

    .line 143
    const v1, 0x7f0c0290

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/MonthDayHeader;->o:I

    .line 147
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    .line 148
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 149
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 150
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 151
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 152
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    const-string v1, "sec-roboto-light"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 154
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 158
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/MonthDayHeader;->p:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 160
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    const-string v2, "A"

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 162
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/calendar/month/MonthDayHeader;->s:I

    .line 166
    iget-object v2, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthDayHeader;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/month/MonthDayHeader;->r:I

    int-to-float v0, v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 168
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    const-string v2, "A"

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 170
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/calendar/month/MonthDayHeader;->t:I

    .line 174
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthDayHeader;->d()V

    .line 176
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthDayHeader;->b()V

    .line 178
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthDayHeader;->c()V

    .line 179
    return-void

    .line 166
    :cond_0
    iget v0, p0, Lcom/android/calendar/month/MonthDayHeader;->q:I

    int-to-float v0, v0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthDayHeader;->f:Z

    if-nez v0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget v0, p0, Lcom/android/calendar/month/MonthDayHeader;->h:I

    iget v1, p0, Lcom/android/calendar/month/MonthDayHeader;->i:I

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    .line 315
    :goto_1
    if-eqz v0, :cond_0

    .line 316
    iget v0, p0, Lcom/android/calendar/month/MonthDayHeader;->i:I

    iput v0, p0, Lcom/android/calendar/month/MonthDayHeader;->h:I

    .line 320
    iget v0, p0, Lcom/android/calendar/month/MonthDayHeader;->i:I

    if-gez v0, :cond_3

    .line 321
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->c:Ljava/lang/String;

    .line 332
    :goto_2
    const v1, 0x8000

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 333
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 336
    invoke-virtual {p0, v0}, Lcom/android/calendar/month/MonthDayHeader;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 338
    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthDayHeader;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 341
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/MonthDayHeader;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 313
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 323
    :cond_3
    iget v0, p0, Lcom/android/calendar/month/MonthDayHeader;->i:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthDayHeader;->y:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 325
    iget v0, p0, Lcom/android/calendar/month/MonthDayHeader;->u:I

    iget v1, p0, Lcom/android/calendar/month/MonthDayHeader;->i:I

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x7

    .line 327
    iget-object v1, p0, Lcom/android/calendar/month/MonthDayHeader;->y:[Ljava/lang/String;

    aget-object v0, v1, v0

    goto :goto_2
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 262
    packed-switch v0, :pswitch_data_0

    .line 274
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 266
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 267
    invoke-direct {p0, v0}, Lcom/android/calendar/month/MonthDayHeader;->a(I)V

    .line 269
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/month/MonthDayHeader;->a(Z)V

    goto :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected a()[I
    .locals 4

    .prologue
    .line 182
    const/4 v0, 0x3

    new-array v1, v0, [I

    .line 184
    const-string v0, "XXXXXXR"

    .line 186
    const/4 v2, 0x0

    iget v3, p0, Lcom/android/calendar/month/MonthDayHeader;->k:I

    aput v3, v1, v2

    .line 187
    const/4 v2, 0x1

    iget v3, p0, Lcom/android/calendar/month/MonthDayHeader;->l:I

    aput v3, v1, v2

    .line 188
    const/4 v2, 0x2

    iget v3, p0, Lcom/android/calendar/month/MonthDayHeader;->m:I

    aput v3, v1, v2

    .line 191
    :try_start_0
    invoke-static {}, Lcom/android/calendar/dz;->e()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 196
    :goto_0
    iget v2, p0, Lcom/android/calendar/month/MonthDayHeader;->u:I

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v0

    return-object v0

    .line 192
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected b()V
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v1, 0x1

    .line 201
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 203
    const/4 v0, 0x2

    .line 204
    sget-object v3, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/util/Locale;->GERMANY:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 208
    :cond_1
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 209
    iget-boolean v2, p0, Lcom/android/calendar/month/MonthDayHeader;->e:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/android/calendar/month/MonthDayHeader;->j:I

    if-ne v2, v1, :cond_2

    .line 210
    const/4 v0, 0x0

    .line 214
    :cond_2
    new-array v2, v5, [Ljava/lang/String;

    iput-object v2, p0, Lcom/android/calendar/month/MonthDayHeader;->x:[Ljava/lang/String;

    .line 215
    new-array v2, v5, [Ljava/lang/String;

    iput-object v2, p0, Lcom/android/calendar/month/MonthDayHeader;->y:[Ljava/lang/String;

    .line 217
    :goto_0
    if-gt v1, v5, :cond_3

    .line 218
    iget-object v2, p0, Lcom/android/calendar/month/MonthDayHeader;->x:[Ljava/lang/String;

    add-int/lit8 v3, v1, -0x1

    invoke-static {v1, v0}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 220
    iget-object v2, p0, Lcom/android/calendar/month/MonthDayHeader;->y:[Ljava/lang/String;

    add-int/lit8 v3, v1, -0x1

    const/4 v4, 0x3

    invoke-static {v1, v4}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 223
    :cond_3
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthDayHeader;->d:Z

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->z:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/MonthDayHeader;->u:I

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthDayHeader;->v:Z

    .line 235
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthDayHeader;->a()[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->w:[I

    .line 237
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthDayHeader;->invalidate()V

    .line 238
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->z:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/MonthDayHeader;->u:I

    .line 232
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->z:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthDayHeader;->v:Z

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 241
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->z:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 242
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/calendar/month/MonthDayHeader;->f:Z

    .line 243
    iget-boolean v1, p0, Lcom/android/calendar/month/MonthDayHeader;->f:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthDayHeader;->g:Z

    .line 244
    return-void

    :cond_0
    move v1, v3

    .line 242
    goto :goto_0

    :cond_1
    move v2, v3

    .line 243
    goto :goto_1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 356
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthDayHeader;->getWidth()I

    move-result v1

    .line 357
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthDayHeader;->getHeight()I

    move-result v2

    .line 362
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 363
    iget-object v0, p0, Lcom/android/calendar/month/MonthDayHeader;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 366
    div-int/lit8 v0, v1, 0xe

    .line 370
    iget-boolean v4, p0, Lcom/android/calendar/month/MonthDayHeader;->v:Z

    if-eqz v4, :cond_0

    .line 372
    iget v4, p0, Lcom/android/calendar/month/MonthDayHeader;->o:I

    sub-int/2addr v1, v4

    .line 374
    iget v4, p0, Lcom/android/calendar/month/MonthDayHeader;->o:I

    add-int/2addr v0, v4

    .line 375
    div-int/lit8 v4, v2, 0x2

    iget v5, p0, Lcom/android/calendar/month/MonthDayHeader;->s:I

    add-int/2addr v4, v5

    .line 377
    iget-object v5, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    iget v6, p0, Lcom/android/calendar/month/MonthDayHeader;->p:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 378
    iget-object v5, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    iget v6, p0, Lcom/android/calendar/month/MonthDayHeader;->n:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 380
    iget-object v5, p0, Lcom/android/calendar/month/MonthDayHeader;->c:Ljava/lang/String;

    iget v6, p0, Lcom/android/calendar/month/MonthDayHeader;->o:I

    int-to-float v6, v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    int-to-float v4, v4

    iget-object v7, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v4, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 384
    :cond_0
    div-int/lit8 v2, v2, 0x2

    iget v4, p0, Lcom/android/calendar/month/MonthDayHeader;->t:I

    add-int/2addr v4, v2

    .line 386
    iget v5, p0, Lcom/android/calendar/month/MonthDayHeader;->u:I

    .line 388
    iget-object v6, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    iget-boolean v2, p0, Lcom/android/calendar/month/MonthDayHeader;->d:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/calendar/month/MonthDayHeader;->r:I

    int-to-float v2, v2

    :goto_0
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    move v2, v0

    move v0, v3

    .line 390
    :goto_1
    const/4 v3, 0x7

    if-ge v0, v3, :cond_2

    .line 391
    iget-object v3, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/android/calendar/month/MonthDayHeader;->w:[I

    aget v6, v6, v0

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 393
    add-int v3, v5, v0

    rem-int/lit8 v3, v3, 0x7

    .line 395
    iget-object v6, p0, Lcom/android/calendar/month/MonthDayHeader;->x:[Ljava/lang/String;

    aget-object v3, v6, v3

    int-to-float v6, v2

    int-to-float v7, v4

    iget-object v8, p0, Lcom/android/calendar/month/MonthDayHeader;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 397
    div-int/lit8 v3, v1, 0x7

    add-int/2addr v2, v3

    .line 390
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 388
    :cond_1
    iget v2, p0, Lcom/android/calendar/month/MonthDayHeader;->q:I

    int-to-float v2, v2

    goto :goto_0

    .line 400
    :cond_2
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthDayHeader;->g:Z

    if-eqz v0, :cond_0

    .line 251
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthDayHeader;->a(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 257
    :goto_0
    return v0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    const-string v0, "MonthDayHeader"

    const-string v1, "Failed to perform AccessibilityHoverEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 347
    const/16 v0, 0x40

    if-ne p1, v0, :cond_0

    .line 348
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/MonthDayHeader;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 351
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
