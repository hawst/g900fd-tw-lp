.class public Lcom/android/calendar/month/ag;
.super Lcom/android/calendar/month/cc;
.source "MonthByWeekFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/month/k;


# direct methods
.method protected constructor <init>(Lcom/android/calendar/month/k;)V
    .locals 0

    .prologue
    .line 1195
    iput-object p1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    invoke-direct {p0, p1}, Lcom/android/calendar/month/cc;-><init>(Lcom/android/calendar/month/by;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget v1, p0, Lcom/android/calendar/month/ag;->c:I

    iput v1, v0, Lcom/android/calendar/month/k;->am:I

    .line 1199
    const-string v0, "MonthFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1200
    const-string v0, "MonthFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new scroll state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/month/ag;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " old state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget v2, v2, Lcom/android/calendar/month/k;->al:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " first pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/ag;->b:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    :cond_0
    iget v0, p0, Lcom/android/calendar/month/ag;->c:I

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget v0, v0, Lcom/android/calendar/month/k;->al:I

    if-eqz v0, :cond_b

    .line 1206
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget v1, p0, Lcom/android/calendar/month/ag;->c:I

    iput v1, v0, Lcom/android/calendar/month/k;->al:I

    .line 1207
    iget-object v0, p0, Lcom/android/calendar/month/ag;->b:Landroid/widget/AbsListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1208
    if-nez v0, :cond_2

    .line 1311
    :cond_1
    :goto_0
    return-void

    .line 1213
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    sget v1, Lcom/android/calendar/month/by;->ah:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->c(I)V

    .line 1215
    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-nez v0, :cond_1

    .line 1220
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->k(Lcom/android/calendar/month/k;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1231
    sget-boolean v0, Lcom/android/calendar/month/by;->ak:Z

    if-eqz v0, :cond_4

    .line 1232
    const/4 v0, 0x2

    move v1, v0

    .line 1241
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    if-eqz v0, :cond_3

    .line 1242
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-string v2, "accessibility"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1243
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1244
    const/4 v1, 0x3

    .line 1248
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/ag;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/cd;

    .line 1249
    if-nez v0, :cond_6

    .line 1250
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto :goto_0

    .line 1234
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->month:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 1235
    const/4 v0, 0x3

    move v1, v0

    goto :goto_1

    .line 1237
    :cond_5
    const/4 v0, 0x4

    move v1, v0

    goto :goto_1

    .line 1254
    :cond_6
    invoke-virtual {v0}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v0

    .line 1255
    iget-object v1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 1257
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->month:I

    iget-object v1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    if-ne v0, v1, :cond_7

    sget-boolean v0, Lcom/android/calendar/month/by;->X:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-boolean v0, v0, Lcom/android/calendar/month/k;->a:Z

    if-nez v0, :cond_1

    .line 1263
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget-object v1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_8

    .line 1267
    iget-object v1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/android/calendar/month/k;->a(JZZZ)Z

    goto/16 :goto_0

    .line 1271
    :cond_8
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 1272
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1273
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    iget-object v1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    if-le v0, v1, :cond_9

    .line 1274
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    .line 1278
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iput v0, v4, Landroid/text/format/Time;->year:I

    .line 1279
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->ac:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->month:I

    iput v0, v4, Landroid/text/format/Time;->month:I

    .line 1280
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 1283
    sget-boolean v0, Lcom/android/calendar/month/by;->X:Z

    if-nez v0, :cond_a

    .line 1284
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->l(Lcom/android/calendar/month/k;)Lcom/android/calendar/al;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v5, v4

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_0

    .line 1276
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->Z:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    iput v0, v4, Landroid/text/format/Time;->monthDay:I

    goto :goto_2

    .line 1287
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->l(Lcom/android/calendar/month/k;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 1299
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    invoke-static {v0}, Lcom/android/calendar/month/k;->l(Lcom/android/calendar/month/k;)Lcom/android/calendar/al;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->V:Landroid/content/Context;

    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v5, v4

    invoke-virtual/range {v0 .. v12}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    goto/16 :goto_0

    .line 1305
    :cond_b
    iget v0, p0, Lcom/android/calendar/month/ag;->c:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget v0, v0, Lcom/android/calendar/month/k;->al:I

    if-nez v0, :cond_c

    .line 1307
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    sget v1, Lcom/android/calendar/month/by;->ah:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/ce;->c(I)V

    goto/16 :goto_0

    .line 1309
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/month/ag;->a:Lcom/android/calendar/month/k;

    iget v1, p0, Lcom/android/calendar/month/ag;->c:I

    iput v1, v0, Lcom/android/calendar/month/k;->al:I

    goto/16 :goto_0
.end method
