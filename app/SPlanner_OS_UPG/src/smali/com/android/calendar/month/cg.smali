.class public Lcom/android/calendar/month/cg;
.super Ljava/lang/Object;
.source "TwDayOfMonthCursor.java"


# static fields
.field private static h:I


# instance fields
.field private final a:I

.field private b:Ljava/util/Calendar;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private i:Lcom/android/calendar/month/ch;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/month/cg;->h:I

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/month/cg;->i:Lcom/android/calendar/month/ch;

    .line 64
    if-lt p4, v3, :cond_0

    const/4 v0, 0x7

    if-le p4, v0, :cond_1

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 67
    :cond_1
    iput p4, p0, Lcom/android/calendar/month/cg;->a:I

    .line 69
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    .line 70
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    invoke-virtual {v0, v3, p1}, Ljava/util/Calendar;->set(II)V

    .line 71
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 72
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 73
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 74
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 75
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 76
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    .line 78
    invoke-direct {p0}, Lcom/android/calendar/month/cg;->g()V

    .line 80
    invoke-virtual {p0, p3}, Lcom/android/calendar/month/cg;->a(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cg;->f:I

    .line 81
    invoke-virtual {p0, p3}, Lcom/android/calendar/month/cg;->b(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cg;->g:I

    .line 82
    return-void
.end method

.method private final g()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x2

    .line 214
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cg;->c:I

    .line 216
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 217
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cg;->d:I

    .line 218
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 220
    invoke-virtual {p0}, Lcom/android/calendar/month/cg;->c()I

    move-result v0

    .line 221
    iget v1, p0, Lcom/android/calendar/month/cg;->a:I

    sub-int/2addr v0, v1

    .line 222
    if-gez v0, :cond_0

    .line 223
    add-int/lit8 v0, v0, 0x7

    .line 225
    :cond_0
    iput v0, p0, Lcom/android/calendar/month/cg;->e:I

    .line 226
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public a(I)I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/android/calendar/month/cg;->e:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    return v0
.end method

.method public a(II)I
    .locals 2

    .prologue
    .line 142
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 143
    :goto_0
    if-nez p1, :cond_2

    iget v1, p0, Lcom/android/calendar/month/cg;->e:I

    if-ge v0, v1, :cond_2

    .line 144
    iget v1, p0, Lcom/android/calendar/month/cg;->d:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/cg;->e:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 149
    :cond_0
    :goto_1
    return v0

    .line 142
    :cond_1
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 147
    :cond_2
    mul-int/lit8 v1, p1, 0x7

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/cg;->e:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 149
    iget v1, p0, Lcom/android/calendar/month/cg;->c:I

    if-le v0, v1, :cond_0

    iget v1, p0, Lcom/android/calendar/month/cg;->c:I

    sub-int/2addr v0, v1

    goto :goto_1
.end method

.method public a(IILandroid/text/format/Time;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 422
    invoke-virtual {p0}, Lcom/android/calendar/month/cg;->a()I

    move-result v1

    .line 423
    iget v2, p3, Landroid/text/format/Time;->year:I

    if-eq v2, v1, :cond_1

    .line 437
    :cond_0
    :goto_0
    return v0

    .line 427
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/month/cg;->b()I

    move-result v1

    .line 428
    iget v2, p3, Landroid/text/format/Time;->month:I

    if-ne v2, v1, :cond_0

    .line 432
    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v1

    .line 433
    iget v2, p3, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v1, :cond_0

    .line 437
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/android/calendar/month/cg;->e:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public b(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 193
    if-nez p2, :cond_1

    move v1, v0

    .line 195
    :goto_0
    if-ltz p1, :cond_0

    if-ltz v1, :cond_0

    const/4 v2, 0x5

    if-gt p1, v2, :cond_0

    const/4 v2, 0x6

    if-le v1, v2, :cond_2

    .line 207
    :cond_0
    :goto_1
    return v0

    .line 193
    :cond_1
    add-int/lit8 v1, p2, -0x1

    goto :goto_0

    .line 199
    :cond_2
    if-nez p1, :cond_3

    iget v2, p0, Lcom/android/calendar/month/cg;->e:I

    if-lt v1, v2, :cond_0

    .line 203
    :cond_3
    mul-int/lit8 v2, p1, 0x7

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/cg;->e:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 204
    iget v2, p0, Lcom/android/calendar/month/cg;->c:I

    if-gt v1, v2, :cond_0

    .line 207
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public c()I
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/android/calendar/month/cg;->c:I

    return v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 179
    invoke-direct {p0}, Lcom/android/calendar/month/cg;->g()V

    .line 180
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 184
    iget-object v0, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 185
    iget-object v1, p0, Lcom/android/calendar/month/cg;->b:Ljava/util/Calendar;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 186
    invoke-direct {p0}, Lcom/android/calendar/month/cg;->g()V

    .line 187
    return-void
.end method
