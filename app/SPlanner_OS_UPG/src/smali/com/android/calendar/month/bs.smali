.class Lcom/android/calendar/month/bs;
.super Ljava/lang/Object;
.source "MonthWeekEventsView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/bo;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bo;)V
    .locals 0

    .prologue
    .line 3540
    iput-object p1, p0, Lcom/android/calendar/month/bs;->a:Lcom/android/calendar/month/bo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 3543
    iget-object v0, p0, Lcom/android/calendar/month/bs;->a:Lcom/android/calendar/month/bo;

    iget-object v0, v0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 3544
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 3545
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 3546
    const-string v2, "MonthHoverEventList"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 3549
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_0

    .line 3550
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3552
    :cond_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3557
    iget-object v0, p0, Lcom/android/calendar/month/bs;->a:Lcom/android/calendar/month/bo;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/month/bo;->a(Lcom/android/calendar/month/bo;Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bb;

    .line 3558
    :goto_0
    return-void

    .line 3553
    :catch_0
    move-exception v0

    goto :goto_0
.end method
