.class public Lcom/android/calendar/month/cl;
.super Landroid/view/View;
.source "YearView.java"


# static fields
.field private static aX:Z

.field private static aY:Z

.field public static k:Z


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:I

.field private P:I

.field private Q:I

.field private R:I

.field private S:I

.field private T:I

.field private U:I

.field private V:I

.field private W:I

.field a:Z

.field private aA:I

.field private aB:I

.field private aC:I

.field private aD:I

.field private aE:I

.field private aF:I

.field private aG:I

.field private aH:I

.field private aI:I

.field private aJ:I

.field private aK:Landroid/graphics/drawable/Drawable;

.field private aL:Landroid/graphics/drawable/Drawable;

.field private aM:Landroid/graphics/drawable/Drawable;

.field private aN:Landroid/graphics/drawable/Drawable;

.field private aO:Landroid/graphics/drawable/Drawable;

.field private aP:Landroid/graphics/drawable/Drawable;

.field private aQ:Landroid/graphics/drawable/Drawable;

.field private aR:I

.field private aS:I

.field private aT:I

.field private aU:[Ljava/lang/String;

.field private aV:Z

.field private aW:Landroid/graphics/Rect;

.field private aZ:Landroid/view/accessibility/AccessibilityManager;

.field private aa:I

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:I

.field private ag:I

.field private ah:I

.field private ai:I

.field private aj:I

.field private ak:I

.field private al:I

.field private am:I

.field private an:I

.field private ao:I

.field private ap:I

.field private aq:I

.field private ar:I

.field private as:I

.field private at:I

.field private au:I

.field private av:I

.field private aw:I

.field private ax:I

.field private ay:Z

.field private az:I

.field protected b:[I

.field private ba:I

.field private bb:I

.field private bc:Z

.field private bd:I

.field private be:I

.field private final bf:Landroid/content/Context;

.field private bg:F

.field private bh:F

.field private bi:Z

.field private bj:Z

.field private bk:Landroid/view/animation/Animation$AnimationListener;

.field private bl:Ljava/lang/Runnable;

.field protected c:[I

.field protected d:Lcom/android/calendar/d/g;

.field protected e:Lcom/android/calendar/d/a/a;

.field public f:Z

.field protected g:Landroid/os/Handler;

.field h:J

.field i:Ljava/lang/String;

.field j:I

.field l:Ljava/lang/Runnable;

.field private m:Landroid/app/Activity;

.field private n:Lcom/android/calendar/AllInOneActivity;

.field private o:Landroid/graphics/Paint;

.field private p:Landroid/view/GestureDetector;

.field private q:Lcom/android/calendar/al;

.field private r:Landroid/view/animation/AnimationSet;

.field private s:Landroid/view/animation/AnimationSet;

.field private t:Lcom/android/calendar/ej;

.field private u:Landroid/util/DisplayMetrics;

.field private v:Landroid/text/format/Time;

.field private w:Landroid/text/format/Time;

.field private x:Landroid/text/format/Time;

.field private y:Landroid/text/format/Time;

.field private z:Lcom/android/calendar/month/cg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 328
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/cl;->k:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/16 v5, 0x500

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 333
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 82
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->o:Landroid/graphics/Paint;

    .line 94
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->u:Landroid/util/DisplayMetrics;

    .line 192
    iput v2, p0, Lcom/android/calendar/month/cl;->au:I

    .line 194
    iput v2, p0, Lcom/android/calendar/month/cl;->av:I

    .line 196
    iput v2, p0, Lcom/android/calendar/month/cl;->aw:I

    .line 198
    iput v2, p0, Lcom/android/calendar/month/cl;->ax:I

    .line 200
    iput-boolean v2, p0, Lcom/android/calendar/month/cl;->ay:Z

    .line 258
    iput v2, p0, Lcom/android/calendar/month/cl;->aT:I

    .line 276
    iput-boolean v1, p0, Lcom/android/calendar/month/cl;->aV:Z

    .line 278
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    .line 284
    iput-object v6, p0, Lcom/android/calendar/month/cl;->aZ:Landroid/view/accessibility/AccessibilityManager;

    .line 289
    iput v2, p0, Lcom/android/calendar/month/cl;->ba:I

    .line 291
    iput v2, p0, Lcom/android/calendar/month/cl;->bb:I

    .line 293
    iput-boolean v2, p0, Lcom/android/calendar/month/cl;->bc:Z

    .line 295
    iput v2, p0, Lcom/android/calendar/month/cl;->bd:I

    .line 297
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/month/cl;->be:I

    .line 301
    iput-boolean v2, p0, Lcom/android/calendar/month/cl;->a:Z

    .line 311
    iput-boolean v1, p0, Lcom/android/calendar/month/cl;->f:Z

    .line 320
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/month/cl;->j:I

    .line 323
    iput v3, p0, Lcom/android/calendar/month/cl;->bg:F

    .line 324
    iput v3, p0, Lcom/android/calendar/month/cl;->bh:F

    .line 326
    iput-boolean v2, p0, Lcom/android/calendar/month/cl;->bi:Z

    .line 330
    iput-boolean v2, p0, Lcom/android/calendar/month/cl;->bj:Z

    .line 1625
    new-instance v0, Lcom/android/calendar/month/co;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/co;-><init>(Lcom/android/calendar/month/cl;)V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->bk:Landroid/view/animation/Animation$AnimationListener;

    .line 1770
    new-instance v0, Lcom/android/calendar/month/cp;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/cp;-><init>(Lcom/android/calendar/month/cl;)V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->bl:Ljava/lang/Runnable;

    .line 1789
    new-instance v0, Lcom/android/calendar/month/cq;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/cq;-><init>(Lcom/android/calendar/month/cl;)V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->l:Ljava/lang/Runnable;

    .line 334
    iput-object p1, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    .line 335
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    .line 336
    invoke-virtual {p0, v1}, Lcom/android/calendar/month/cl;->setFocusable(Z)V

    .line 337
    invoke-virtual {p0, v1}, Lcom/android/calendar/month/cl;->setFocusableInTouchMode(Z)V

    .line 338
    invoke-virtual {p0, v1}, Lcom/android/calendar/month/cl;->setClickable(Z)V

    .line 339
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/cl;->bi:Z

    .line 341
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/month/cl;->u:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 343
    iput v5, p0, Lcom/android/calendar/month/cl;->aR:I

    .line 344
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->bi:Z

    if-eqz v0, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0441

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aR:I

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aS:I

    .line 350
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 351
    const v0, 0x7f0c0440

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->O:I

    .line 352
    const v0, 0x7f0c03b8

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->D:I

    .line 353
    const v0, 0x7f0c03b2

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->C:I

    .line 354
    const v0, 0x7f0c03ae

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->E:I

    .line 355
    const v0, 0x7f0c03b0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->F:I

    .line 356
    const v0, 0x7f0c03b1

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->G:I

    .line 357
    const v0, 0x7f0c03af

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->H:I

    .line 358
    const v0, 0x7f0c03b9

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->J:I

    .line 359
    const v0, 0x7f0c03aa

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->K:I

    .line 360
    const v0, 0x7f0c03ab

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->L:I

    .line 361
    const v0, 0x7f0c03c0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->M:I

    .line 362
    const v0, 0x7f0c03ac

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->P:I

    .line 363
    const v0, 0x7f0c03ba

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->N:I

    .line 364
    const v0, 0x7f0c03b4

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->az:I

    .line 365
    const v0, 0x7f0c03b5

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->I:I

    .line 366
    const v0, 0x7f0d0029

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aA:I

    .line 370
    const v0, 0x7f0d0028

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aB:I

    .line 374
    const v0, 0x7f0c03a6

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->Q:I

    .line 375
    const v0, 0x7f0c03a3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->U:I

    .line 376
    const v0, 0x7f0c03a7

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->V:I

    .line 377
    const v0, 0x7f0c03a4

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->W:I

    .line 378
    const v0, 0x7f0c03a9

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ao:I

    .line 379
    const v0, 0x7f0c03a2

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ap:I

    .line 380
    const v0, 0x7f0c03a5

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->R:I

    .line 381
    const v0, 0x7f0c03a8

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->S:I

    .line 382
    const v0, 0x7f0c03a0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aa:I

    .line 383
    const v0, 0x7f0c03a1

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ab:I

    .line 384
    const v0, 0x7f0c039f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ac:I

    .line 385
    const v0, 0x7f0c03b3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ad:I

    .line 386
    const v0, 0x7f0c03b7

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ae:I

    .line 387
    const v0, 0x7f0c03bd

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->af:I

    .line 388
    const v0, 0x7f0c03be

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ag:I

    .line 389
    const v0, 0x7f0c03bf

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ah:I

    .line 390
    const v0, 0x7f0c03bb

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ai:I

    .line 391
    const v0, 0x7f0c00a3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aj:I

    .line 392
    const v0, 0x7f0d000f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ak:I

    .line 393
    const v0, 0x7f0d0009

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->al:I

    .line 394
    const v0, 0x7f0c0215

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->am:I

    .line 395
    const v0, 0x7f0c03b6

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->an:I

    .line 396
    const v0, 0x7f0c03ad

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->T:I

    .line 397
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_7

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/month/cl;->a:Z

    .line 398
    const v0, 0x7f0c042f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aI:I

    .line 399
    const v0, 0x7f0c0430

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aJ:I

    .line 401
    sget-boolean v0, Lcom/android/calendar/month/cl;->aX:Z

    if-nez v0, :cond_1

    .line 402
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->a:Z

    if-nez v0, :cond_8

    .line 403
    const v0, 0x7f0c0438

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->E:I

    .line 404
    const v0, 0x7f0c0432

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ap:I

    .line 405
    const v0, 0x7f0c0435

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->W:I

    .line 406
    const v0, 0x7f0c043f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->M:I

    .line 407
    const v0, 0x7f0c043e

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->N:I

    .line 408
    const v0, 0x7f0c0437

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->P:I

    .line 409
    const v0, 0x7f0c043a

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->H:I

    .line 419
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->bi:Z

    if-eqz v0, :cond_2

    .line 421
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->a:Z

    if-eqz v0, :cond_9

    .line 422
    const v0, 0x7f0c0444

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->M:I

    :cond_2
    :goto_2
    move-object v0, p1

    .line 429
    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    iput-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    .line 430
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/month/cl;->aX:Z

    .line 431
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/month/cl;->aY:Z

    .line 432
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->g:Landroid/os/Handler;

    .line 437
    sget-boolean v0, Lcom/android/calendar/month/cl;->aY:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v1, 0x320

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-eq v0, v5, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v0, v5, :cond_5

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v1, 0x320

    if-ne v0, v1, :cond_5

    :cond_4
    sget-boolean v0, Lcom/android/calendar/month/cl;->aX:Z

    if-nez v0, :cond_5

    .line 440
    const v0, 0x7f0c043d

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->D:I

    .line 441
    const v0, 0x7f0c043c

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->C:I

    .line 442
    const v0, 0x7f0c0439

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->E:I

    .line 443
    const v0, 0x7f0c043b

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->F:I

    .line 444
    const v0, 0x7f0c0436

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ao:I

    .line 445
    const v0, 0x7f0c0433

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ap:I

    .line 446
    const v0, 0x7f0c0431

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ac:I

    .line 447
    const v0, 0x7f0d002e

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aA:I

    .line 451
    const v0, 0x7f0d002d

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aB:I

    .line 457
    :cond_5
    const v0, 0x7f0d002a

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aq:I

    .line 458
    const v0, 0x7f0d0027

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ar:I

    .line 460
    const v0, 0x7f0d002a

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->as:I

    .line 462
    const v0, 0x7f0d0027

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->at:I

    .line 466
    const v0, 0x7f0b012a

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aC:I

    .line 467
    const v0, 0x7f0b0123

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aD:I

    .line 468
    const v0, 0x7f0b00ce

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aE:I

    .line 471
    const v0, 0x7f0b00e6

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aF:I

    .line 473
    const v0, 0x7f0b0129

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aH:I

    .line 475
    const v0, 0x7f0b012c

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aG:I

    .line 477
    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->a()V

    .line 481
    const v0, 0x7f0200bd

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aK:Landroid/graphics/drawable/Drawable;

    .line 482
    const v0, 0x7f0201f0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aL:Landroid/graphics/drawable/Drawable;

    .line 483
    const v0, 0x7f0201f1

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aM:Landroid/graphics/drawable/Drawable;

    .line 484
    const v0, 0x7f0200ae

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aN:Landroid/graphics/drawable/Drawable;

    .line 485
    const v0, 0x7f0200ad

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aO:Landroid/graphics/drawable/Drawable;

    .line 486
    iput-object p1, p0, Lcom/android/calendar/month/cl;->m:Landroid/app/Activity;

    .line 487
    invoke-static {p1, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 489
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    .line 490
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/cl;->w:Landroid/text/format/Time;

    .line 491
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/cl;->x:Landroid/text/format/Time;

    .line 492
    iget-object v1, p0, Lcom/android/calendar/month/cl;->x:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 493
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/cl;->y:Landroid/text/format/Time;

    .line 494
    iget-object v0, p0, Lcom/android/calendar/month/cl;->y:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 496
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/android/calendar/month/cr;

    invoke-direct {v1, p0, v6}, Lcom/android/calendar/month/cr;-><init>(Lcom/android/calendar/month/cl;Lcom/android/calendar/month/cm;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->p:Landroid/view/GestureDetector;

    .line 498
    sget-boolean v0, Lcom/android/calendar/month/cl;->aX:Z

    if-eqz v0, :cond_a

    .line 499
    const v0, 0x7f02003d

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aP:Landroid/graphics/drawable/Drawable;

    .line 507
    :goto_3
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->d:Lcom/android/calendar/d/g;

    .line 509
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 510
    iget-object v0, p0, Lcom/android/calendar/month/cl;->d:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 511
    invoke-static {p1}, Lcom/android/calendar/ej;->a(Landroid/content/Context;)Lcom/android/calendar/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->t:Lcom/android/calendar/ej;

    .line 515
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/month/cl;->d:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->e:Lcom/android/calendar/d/a/a;

    .line 516
    return-void

    :cond_7
    move v0, v2

    .line 397
    goto/16 :goto_0

    .line 411
    :cond_8
    const v0, 0x7f0c0432

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->ap:I

    .line 412
    const v0, 0x7f0c0443

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->V:I

    .line 413
    const v0, 0x7f0c043f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->M:I

    .line 414
    const v0, 0x7f0c043e

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->N:I

    .line 415
    const v0, 0x7f0c0438

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->E:I

    goto/16 :goto_1

    .line 423
    :cond_9
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->a:Z

    if-nez v0, :cond_2

    .line 424
    const v0, 0x7f0c042e

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->K:I

    .line 425
    const v0, 0x7f0c0434

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->U:I

    goto/16 :goto_2

    .line 503
    :cond_a
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aQ:Landroid/graphics/drawable/Drawable;

    .line 504
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aQ:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x7f0b0128

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    goto/16 :goto_3
.end method

.method static synthetic a(Lcom/android/calendar/month/cl;I)I
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/android/calendar/month/cl;->aT:I

    return p1
.end method

.method private a(Z)Landroid/text/format/Time;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1663
    if-eqz p1, :cond_0

    move v0, v1

    .line 1664
    :goto_0
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1665
    iget-object v3, p0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1666
    iget-object v3, p0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->g()I

    move-result v3

    .line 1667
    packed-switch v3, :pswitch_data_0

    .line 1675
    :goto_1
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1676
    return-object v2

    .line 1663
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 1669
    :pswitch_0
    iget v3, v2, Landroid/text/format/Time;->year:I

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v3

    iput v0, v2, Landroid/text/format/Time;->year:I

    goto :goto_1

    .line 1667
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/android/calendar/month/cl;Landroid/view/accessibility/AccessibilityManager;)Landroid/view/accessibility/AccessibilityManager;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/android/calendar/month/cl;->aZ:Landroid/view/accessibility/AccessibilityManager;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/month/cl;)Lcom/android/calendar/AllInOneActivity;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    return-object v0
.end method

.method private a(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;II)V
    .locals 9

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v0, p2, p3}, Lcom/android/calendar/month/cg;->b(II)Z

    move-result v1

    .line 1176
    const/4 v0, 0x0

    .line 1177
    iget-object v2, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    iget-object v3, p0, Lcom/android/calendar/month/cl;->x:Landroid/text/format/Time;

    invoke-virtual {v2, p2, p3, v3}, Lcom/android/calendar/month/cg;->a(IILandroid/text/format/Time;)Z

    move-result v2

    .line 1178
    iget-wide v4, p0, Lcom/android/calendar/month/cl;->h:J

    iget-object v3, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    iget-wide v6, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 1180
    add-int/lit8 v4, p1, 0x1

    if-ne v3, v4, :cond_0

    .line 1181
    const/4 v0, 0x1

    .line 1185
    :cond_0
    iget v3, p0, Lcom/android/calendar/month/cl;->aB:I

    int-to-float v3, v3

    invoke-virtual {p5, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1186
    iget v3, p0, Lcom/android/calendar/month/cl;->aD:I

    invoke-virtual {p5, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1188
    const/4 v3, 0x0

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {p5, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1190
    iget-object v3, p0, Lcom/android/calendar/month/cl;->b:[I

    add-int/lit8 v4, p3, -0x1

    aget v3, v3, v4

    invoke-virtual {p5, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1192
    iget-object v3, p0, Lcom/android/calendar/month/cl;->d:Lcom/android/calendar/d/g;

    invoke-virtual {v3}, Lcom/android/calendar/d/g;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/calendar/month/cl;->d:Lcom/android/calendar/d/g;

    invoke-virtual {v3}, Lcom/android/calendar/d/g;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1193
    iget-object v3, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v3}, Lcom/android/calendar/month/cg;->a()I

    move-result v3

    .line 1194
    iget-object v4, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v4}, Lcom/android/calendar/month/cg;->b()I

    move-result v4

    .line 1195
    iget-object v5, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v5, p2, p3}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v5

    .line 1196
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 1197
    invoke-virtual {v6, v5, v4, v3}, Landroid/text/format/Time;->set(III)V

    .line 1198
    iget-object v3, p0, Lcom/android/calendar/month/cl;->e:Lcom/android/calendar/d/a/a;

    invoke-virtual {v3, v6}, Lcom/android/calendar/d/a/a;->a(Landroid/text/format/Time;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/android/calendar/month/cl;->bj:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/calendar/month/cl;->e:Lcom/android/calendar/d/a/a;

    invoke-virtual {v3, v6}, Lcom/android/calendar/d/a/a;->c(Landroid/text/format/Time;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1200
    :cond_1
    iget v3, p0, Lcom/android/calendar/month/cl;->aF:I

    invoke-virtual {p5, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1204
    :cond_2
    iget-object v3, p0, Lcom/android/calendar/month/cl;->i:Ljava/lang/String;

    const-string v4, "JAPAN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1205
    iget-object v3, p0, Lcom/android/calendar/month/cl;->d:Lcom/android/calendar/d/g;

    invoke-virtual {v3}, Lcom/android/calendar/d/g;->e()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1206
    iget-object v3, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v3}, Lcom/android/calendar/month/cg;->a()I

    move-result v3

    .line 1207
    iget-object v4, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v4}, Lcom/android/calendar/month/cg;->b()I

    move-result v4

    .line 1208
    iget-object v5, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v5, p2, p3}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v5

    .line 1209
    invoke-static {v3, v4, v5}, Lcom/android/calendar/hj;->a(III)I

    move-result v6

    .line 1210
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    .line 1211
    invoke-virtual {v7, v5, v4, v3}, Landroid/text/format/Time;->set(III)V

    .line 1212
    iput v6, v7, Landroid/text/format/Time;->weekDay:I

    .line 1213
    iget-object v3, p0, Lcom/android/calendar/month/cl;->t:Lcom/android/calendar/ej;

    invoke-virtual {v3, v7}, Lcom/android/calendar/ej;->a(Landroid/text/format/Time;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/calendar/month/cl;->t:Lcom/android/calendar/ej;

    invoke-virtual {v3, v7}, Lcom/android/calendar/ej;->b(Landroid/text/format/Time;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1215
    :cond_3
    iget v3, p0, Lcom/android/calendar/month/cl;->aF:I

    invoke-virtual {p5, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1220
    :cond_4
    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p5, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1222
    add-int/lit8 v3, p3, -0x1

    iget v4, p0, Lcom/android/calendar/month/cl;->F:I

    iget v5, p0, Lcom/android/calendar/month/cl;->G:I

    add-int/2addr v4, v5

    mul-int/2addr v3, v4

    add-int/2addr v3, p6

    iget v4, p0, Lcom/android/calendar/month/cl;->F:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 1223
    iget v4, p0, Lcom/android/calendar/month/cl;->E:I

    iget v5, p0, Lcom/android/calendar/month/cl;->H:I

    add-int/2addr v4, v5

    mul-int/2addr v4, p2

    add-int v4, v4, p7

    iget v5, p0, Lcom/android/calendar/month/cl;->E:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    .line 1225
    if-eqz v1, :cond_7

    .line 1226
    if-eqz v2, :cond_5

    .line 1227
    iget-object v1, p0, Lcom/android/calendar/month/cl;->aN:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/android/calendar/month/cl;->af:I

    sub-int v5, v3, v5

    iget v6, p0, Lcom/android/calendar/month/cl;->ah:I

    sub-int v6, v4, v6

    iget v7, p0, Lcom/android/calendar/month/cl;->ag:I

    add-int/2addr v7, v3

    iget v8, p0, Lcom/android/calendar/month/cl;->ai:I

    add-int/2addr v8, v4

    invoke-virtual {v1, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1229
    iget-object v1, p0, Lcom/android/calendar/month/cl;->aN:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1230
    iget v1, p0, Lcom/android/calendar/month/cl;->aH:I

    invoke-virtual {p5, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1232
    :cond_5
    if-eqz v0, :cond_6

    if-nez v2, :cond_6

    .line 1233
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aO:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/android/calendar/month/cl;->af:I

    sub-int v1, v3, v1

    iget v2, p0, Lcom/android/calendar/month/cl;->ah:I

    sub-int v2, v4, v2

    iget v5, p0, Lcom/android/calendar/month/cl;->ag:I

    add-int/2addr v5, v3

    iget v6, p0, Lcom/android/calendar/month/cl;->ai:I

    add-int/2addr v6, v4

    invoke-virtual {v0, v1, v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1235
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1237
    :cond_6
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->bc:Z

    if-eqz v0, :cond_8

    .line 1238
    const-string v0, "%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v5, p2, p3}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v3

    int-to-float v2, v4

    invoke-virtual {p4, v0, v1, v2, p5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1244
    :cond_7
    :goto_0
    return-void

    .line 1240
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v0, p2, p3}, Lcom/android/calendar/month/cg;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v3

    int-to-float v2, v4

    invoke-virtual {p4, v0, v1, v2, p5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 21

    .prologue
    .line 878
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->d:Lcom/android/calendar/d/g;

    invoke-virtual {v2}, Lcom/android/calendar/d/g;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 879
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/hj;->s(Landroid/content/Context;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cl;->bj:Z

    .line 882
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->bd:I

    .line 885
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->l()Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    invoke-virtual {v2}, Lcom/android/calendar/al;->m()Z

    move-result v2

    if-nez v2, :cond_8

    .line 886
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->bb:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->ba:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->bb:I

    .line 887
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->ba:I

    .line 888
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/cl;->getWidth()I

    move-result v3

    .line 889
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/cl;->getHeight()I

    move-result v4

    .line 890
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->D:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->K:I

    add-int/2addr v2, v5

    .line 892
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->K:I

    add-int/2addr v5, v3

    div-int v2, v5, v2

    .line 893
    if-nez v2, :cond_1

    .line 894
    const/4 v2, 0x1

    .line 896
    :cond_1
    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->ar:I

    .line 897
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->ar:I

    div-int/2addr v2, v5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->aq:I

    .line 900
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->C:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->P:I

    add-int/2addr v2, v5

    .line 901
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->bb:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->N:I

    mul-int/lit8 v6, v6, 0x2

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/month/cl;->aq:I

    mul-int/2addr v7, v2

    add-int/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/month/cl;->P:I

    sub-int/2addr v6, v7

    sub-int/2addr v6, v4

    if-le v5, v6, :cond_7

    .line 903
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->N:I

    mul-int/lit8 v5, v5, 0x2

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->aq:I

    mul-int/2addr v2, v6

    add-int/2addr v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->P:I

    sub-int/2addr v2, v5

    sub-int/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->bb:I

    .line 909
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->D:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->K:I

    add-int/2addr v2, v4

    .line 910
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 911
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->ar:I

    mul-int/2addr v4, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->K:I

    sub-int/2addr v4, v5

    if-le v3, v4, :cond_3

    .line 912
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->ar:I

    mul-int/2addr v2, v4

    sub-int v2, v3, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->K:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->J:I

    .line 913
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->J:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->Q:I

    .line 921
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/month/cl;->o:Landroid/graphics/Paint;

    .line 923
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->f()V

    .line 924
    new-instance v18, Landroid/text/format/Time;

    const-string v2, "UTC"

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 925
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v3}, Lcom/android/calendar/month/cg;->b()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v4}, Lcom/android/calendar/month/cg;->a()I

    move-result v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3, v4}, Landroid/text/format/Time;->set(III)V

    .line 933
    const/4 v5, 0x0

    .line 934
    const/4 v4, 0x0

    .line 941
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->aS:I

    add-int/lit8 v2, v2, 0x6

    rem-int/lit8 v6, v2, 0x7

    .line 943
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->aT:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 944
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->Q:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->B:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/calendar/month/cl;->ao:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/month/cl;->U:I

    add-int/2addr v8, v9

    mul-int/2addr v3, v8

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->R:I

    sub-int v8, v2, v3

    .line 945
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->V:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->A:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/month/cl;->ap:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->W:I

    add-int/2addr v9, v10

    mul-int/2addr v3, v9

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->bb:I

    sub-int v3, v2, v3

    .line 947
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->ao:I

    add-int/2addr v2, v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/calendar/month/cl;->S:I

    add-int/2addr v9, v2

    .line 948
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->ap:I

    add-int/2addr v2, v3

    .line 949
    new-instance v10, Landroid/util/DisplayMetrics;

    invoke-direct {v10}, Landroid/util/DisplayMetrics;-><init>()V

    .line 950
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/month/cl;->m:Landroid/app/Activity;

    invoke-virtual {v11}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v11

    invoke-virtual {v11, v10}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 951
    iget v10, v10, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v11, 0xf0

    if-ne v10, v11, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v11, 0x168

    if-ne v10, v11, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_9

    .line 954
    add-int/lit8 v3, v3, -0x3

    .line 963
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/android/calendar/AllInOneActivity;->b(Z)V

    .line 964
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/month/cl;->g:Landroid/os/Handler;

    new-instance v11, Lcom/android/calendar/month/cn;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/android/calendar/month/cn;-><init>(Lcom/android/calendar/month/cl;)V

    const-wide/16 v12, 0x3e8

    invoke-virtual {v10, v11, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 970
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/month/cl;->aK:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10, v8, v3, v9, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 971
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->aK:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 974
    :cond_5
    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    .line 976
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/calendar/month/cl;->setYearWeekday(I)V

    .line 979
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/month/cl;->h:J

    .line 980
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/calendar/month/cl;->i:Ljava/lang/String;

    .line 982
    new-instance v19, Landroid/text/format/Time;

    invoke-direct/range {v19 .. v19}, Landroid/text/format/Time;-><init>()V

    .line 983
    invoke-virtual/range {v19 .. v19}, Landroid/text/format/Time;->setToNow()V

    .line 984
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/month/cl;->bc:Z

    move/from16 v16, v4

    move/from16 v17, v5

    .line 986
    :goto_3
    const/4 v2, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 987
    move-object/from16 v0, v18

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v8

    .line 991
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/calendar/month/cg;->b(I)I

    move-result v9

    .line 993
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->d()I

    move-result v2

    .line 994
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v3, v2}, Lcom/android/calendar/month/cg;->a(I)I

    move-result v2

    add-int/lit8 v20, v2, 0x1

    .line 995
    const-string v2, ""

    .line 996
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 997
    const-string v3, "ml"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 998
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->b()I

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v2

    .line 1004
    :goto_4
    sget-boolean v3, Lcom/android/calendar/month/cl;->aX:Z

    if-nez v3, :cond_e

    .line 1005
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->D:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->K:I

    add-int/2addr v3, v4

    mul-int v3, v3, v16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->J:I

    add-int v14, v3, v4

    .line 1006
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->C:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->P:I

    add-int/2addr v3, v4

    mul-int v3, v3, v17

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->N:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->bb:I

    sub-int/2addr v3, v4

    .line 1009
    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1010
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->aA:I

    int-to-float v4, v4

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1012
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v4}, Lcom/android/calendar/month/cg;->a()I

    move-result v4

    move-object/from16 v0, v19

    iget v5, v0, Landroid/text/format/Time;->year:I

    if-ne v4, v5, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v4}, Lcom/android/calendar/month/cg;->b()I

    move-result v4

    move-object/from16 v0, v19

    iget v5, v0, Landroid/text/format/Time;->month:I

    if-ne v4, v5, :cond_b

    .line 1013
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1017
    :goto_5
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->aC:I

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1018
    const/4 v4, 0x1

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1020
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->D:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v14

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->I:I

    mul-int/lit8 v5, v5, 0x2

    div-int/lit8 v5, v5, 0x3

    add-int/2addr v5, v3

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1022
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->I:I

    add-int v10, v3, v2

    .line 1025
    const v2, -0xb4b4b5

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1026
    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1027
    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1028
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1029
    int-to-float v3, v14

    add-int/lit8 v2, v10, -0x1

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->D:I

    add-int/2addr v2, v14

    int-to-float v5, v2

    add-int/lit8 v2, v10, 0x1

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1031
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->aJ:I

    add-int v4, v10, v2

    .line 1034
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1035
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1036
    const/4 v2, 0x1

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1039
    sub-int v9, v8, v9

    .line 1043
    const/4 v2, 0x0

    move v3, v14

    :goto_6
    const/4 v5, 0x7

    if-ge v2, v5, :cond_c

    .line 1044
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->aB:I

    add-int/lit8 v5, v5, -0x6

    int-to-float v5, v5

    invoke-virtual {v7, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1045
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/cl;->c:[I

    aget v5, v5, v2

    invoke-virtual {v7, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1046
    const-string v5, "sans-serif"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1050
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    aget-object v5, v5, v2

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->F:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v3

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/calendar/month/cl;->aI:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v4

    int-to-float v8, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v8, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1051
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->F:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->G:I

    add-int/2addr v5, v6

    add-int/2addr v3, v5

    .line 1043
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 904
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->bb:I

    if-gez v2, :cond_2

    .line 905
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/month/cl;->bb:I

    goto/16 :goto_0

    .line 918
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/cl;->b()V

    goto/16 :goto_1

    .line 956
    :cond_9
    sget-boolean v10, Lcom/android/calendar/dz;->b:Z

    if-eqz v10, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_4

    .line 958
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/android/calendar/month/cl;->bi:Z

    if-eqz v10, :cond_4

    .line 959
    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->O:I

    add-int/2addr v2, v10

    .line 960
    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->O:I

    add-int/2addr v3, v10

    goto/16 :goto_2

    .line 1000
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->b()I

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 1015
    :cond_b
    const-string v4, "sans-serif"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto/16 :goto_5

    .line 1054
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->aI:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->aJ:I

    add-int/2addr v2, v3

    add-int v15, v4, v2

    .line 1056
    const/4 v10, 0x0

    :goto_7
    move/from16 v0, v20

    if-ge v10, v0, :cond_17

    .line 1057
    const/4 v11, 0x1

    :goto_8
    const/16 v2, 0x8

    if-ge v11, v2, :cond_d

    .line 1058
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cl;->o:Landroid/graphics/Paint;

    move-object/from16 v8, p0

    move-object/from16 v12, p1

    invoke-direct/range {v8 .. v15}, Lcom/android/calendar/month/cl;->a(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;II)V

    .line 1059
    add-int/lit8 v9, v9, 0x1

    .line 1057
    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    .line 1056
    :cond_d
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 1067
    :cond_e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->D:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->K:I

    add-int/2addr v3, v4

    mul-int v3, v3, v16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/cl;->bd:I

    add-int/2addr v4, v3

    .line 1068
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->M:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->I:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->C:I

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->N:I

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->az:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    mul-int v5, v5, v17

    add-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/cl;->bb:I

    sub-int/2addr v3, v5

    .line 1073
    if-nez v16, :cond_f

    .line 1074
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/cl;->aL:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->D:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->ar:I

    mul-int/2addr v6, v10

    add-int/2addr v6, v4

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->K:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/calendar/month/cl;->ar:I

    add-int/lit8 v11, v11, -0x1

    mul-int/2addr v10, v11

    add-int/2addr v6, v10

    add-int/lit8 v10, v3, 0x1

    invoke-virtual {v5, v4, v3, v6, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1076
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/month/cl;->aL:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1078
    :cond_f
    add-int/lit8 v5, v3, 0x1

    .line 1081
    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1082
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->aA:I

    int-to-float v3, v3

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1083
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->aC:I

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1084
    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1085
    const/4 v3, 0x0

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1089
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v6, 0x7

    if-gt v3, v6, :cond_11

    .line 1090
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->an:I

    rsub-int/lit8 v3, v3, -0x7

    .line 1095
    :goto_9
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->I:I

    int-to-float v6, v6

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v6, v10

    invoke-virtual {v7}, Landroid/graphics/Paint;->ascent()F

    move-result v10

    neg-float v10, v10

    invoke-virtual {v7}, Landroid/graphics/Paint;->descent()F

    move-result v11

    add-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v7}, Landroid/graphics/Paint;->descent()F

    move-result v11

    sub-float/2addr v10, v11

    add-float/2addr v6, v10

    .line 1097
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/android/calendar/month/cl;->a:Z

    if-eqz v10, :cond_12

    .line 1098
    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->D:I

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v3, v10

    int-to-float v3, v3

    int-to-float v10, v5

    add-float/2addr v6, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1102
    :goto_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->I:I

    add-int/2addr v5, v2

    .line 1105
    if-nez v16, :cond_10

    .line 1106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->aM:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->D:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->ar:I

    mul-int/2addr v3, v6

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/month/cl;->K:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->ar:I

    add-int/lit8 v10, v10, -0x1

    mul-int/2addr v6, v10

    add-int/2addr v3, v6

    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v2, v4, v5, v3, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->aM:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1112
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->aa:I

    add-int v3, v4, v2

    .line 1116
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->P:I

    add-int v6, v5, v2

    .line 1118
    const/4 v2, 0x0

    :goto_b
    const/4 v10, 0x7

    if-ge v2, v10, :cond_13

    .line 1119
    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->aB:I

    int-to-float v10, v10

    invoke-virtual {v7, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1120
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/month/cl;->b:[I

    aget v10, v10, v2

    invoke-virtual {v7, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 1123
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    aget-object v10, v10, v2

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/calendar/month/cl;->ac:I

    add-int/2addr v3, v11

    int-to-float v11, v3

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/calendar/month/cl;->ab:I

    add-int/2addr v12, v5

    int-to-float v12, v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11, v12, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1118
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 1092
    :cond_11
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->an:I

    neg-int v3, v3

    goto/16 :goto_9

    .line 1100
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->an:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/month/cl;->D:I

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v3, v10

    int-to-float v3, v3

    int-to-float v10, v5

    add-float/2addr v6, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_a

    .line 1128
    :cond_13
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1129
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1130
    const/4 v2, 0x1

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1133
    sub-int v9, v8, v9

    .line 1135
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/cl;->a:Z

    if-eqz v2, :cond_15

    .line 1136
    const/4 v10, 0x0

    :goto_c
    move/from16 v0, v20

    if-ge v10, v0, :cond_17

    .line 1137
    const/4 v11, 0x1

    :goto_d
    const/16 v2, 0x8

    if-ge v11, v2, :cond_14

    .line 1138
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cl;->o:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->L:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->J:I

    add-int v14, v2, v3

    add-int/lit8 v15, v6, 0x2

    move-object/from16 v8, p0

    move-object/from16 v12, p1

    invoke-direct/range {v8 .. v15}, Lcom/android/calendar/month/cl;->a(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;II)V

    .line 1140
    add-int/lit8 v9, v9, 0x1

    .line 1137
    add-int/lit8 v11, v11, 0x1

    goto :goto_d

    .line 1136
    :cond_14
    add-int/lit8 v10, v10, 0x1

    goto :goto_c

    .line 1144
    :cond_15
    const/4 v10, 0x0

    :goto_e
    move/from16 v0, v20

    if-ge v10, v0, :cond_17

    .line 1145
    const/4 v11, 0x1

    :goto_f
    const/16 v2, 0x8

    if-ge v11, v2, :cond_16

    .line 1146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/month/cl;->o:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->L:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/cl;->J:I

    add-int/2addr v2, v3

    add-int/lit8 v14, v2, -0x3

    add-int/lit8 v15, v6, 0x2

    move-object/from16 v8, p0

    move-object/from16 v12, p1

    invoke-direct/range {v8 .. v15}, Lcom/android/calendar/month/cl;->a(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;II)V

    .line 1148
    add-int/lit8 v9, v9, 0x1

    .line 1145
    add-int/lit8 v11, v11, 0x1

    goto :goto_f

    .line 1144
    :cond_16
    add-int/lit8 v10, v10, 0x1

    goto :goto_e

    .line 1154
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->b()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_18

    .line 1171
    return-void

    .line 1158
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v2}, Lcom/android/calendar/month/cg;->e()V

    .line 1159
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v3}, Lcom/android/calendar/month/cg;->b()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    invoke-virtual {v4}, Lcom/android/calendar/month/cg;->a()I

    move-result v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3, v4}, Landroid/text/format/Time;->set(III)V

    .line 1161
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/cl;->ar:I

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, v16

    if-ge v0, v2, :cond_19

    .line 1162
    add-int/lit8 v2, v16, 0x1

    .line 1167
    :goto_10
    if-nez v2, :cond_1a

    .line 1168
    add-int/lit8 v3, v17, 0x1

    :goto_11
    move/from16 v16, v2

    move/from16 v17, v3

    .line 1170
    goto/16 :goto_3

    .line 1164
    :cond_19
    const/4 v2, 0x0

    goto :goto_10

    :cond_1a
    move/from16 v3, v17

    goto :goto_11
.end method

.method private a(Landroid/text/format/Time;)V
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 1680
    invoke-static {p1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1681
    iget-object v0, p0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    const-wide/16 v2, 0x20

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v6, p1

    move-object v12, v5

    move-object v13, v5

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJLjava/lang/String;Landroid/content/ComponentName;)V

    .line 1686
    :goto_0
    return-void

    .line 1684
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    goto :goto_0
.end method

.method private a(III)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1735
    sget-boolean v2, Lcom/android/calendar/month/cl;->aY:Z

    if-eqz v2, :cond_2

    .line 1736
    iget-object v2, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, p3

    if-ge p1, v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, p3

    iget v3, p0, Lcom/android/calendar/month/cl;->am:I

    sub-int/2addr v2, v3

    if-le p1, v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-le p2, v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    if-ge p2, v2, :cond_1

    .line 1748
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1741
    goto :goto_0

    .line 1744
    :cond_2
    iget v2, p0, Lcom/android/calendar/month/cl;->aR:I

    sub-int/2addr v2, p3

    if-le p1, v2, :cond_3

    const/16 v2, 0xa

    if-le p2, v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v2, v2, -0xa

    if-lt p2, v2, :cond_0

    :cond_3
    move v0, v1

    .line 1748
    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v3, 0x3

    const/4 v13, -0x1

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 562
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->d(Landroid/content/Context;)Z

    move-result v0

    .line 563
    iget-object v5, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/calendar/dz;->l(Landroid/content/Context;)Z

    move-result v5

    .line 564
    iget-object v6, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v6}, Lcom/android/calendar/dz;->e(Landroid/content/Context;)Z

    move-result v6

    .line 565
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v7

    .line 567
    if-ne v7, v2, :cond_1

    .line 568
    if-nez v0, :cond_2

    .line 829
    :cond_0
    :goto_0
    return v4

    .line 571
    :cond_1
    if-ne v7, v1, :cond_2

    .line 572
    if-eqz v5, :cond_0

    .line 577
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    .line 578
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v9, v0

    .line 579
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v10, v0

    .line 581
    if-gez v10, :cond_3

    .line 583
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 584
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 587
    const-string v11, "YearViewZoomIn"

    invoke-virtual {v0, v11}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 589
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v11

    if-nez v11, :cond_3

    .line 591
    :try_start_0
    invoke-virtual {v5, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 592
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    .line 598
    :cond_3
    :goto_1
    iget v5, p0, Lcom/android/calendar/month/cl;->al:I

    .line 599
    iget v0, p0, Lcom/android/calendar/month/cl;->al:I

    .line 601
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->b(Landroid/view/MotionEvent;)I

    move-result v11

    .line 602
    if-ne v11, v13, :cond_4

    const/16 v12, 0xa

    if-ne v8, v12, :cond_0

    .line 606
    :cond_4
    iget-boolean v12, p0, Lcom/android/calendar/month/cl;->a:Z

    if-nez v12, :cond_5

    sget-boolean v12, Lcom/android/calendar/month/cl;->aX:Z

    if-nez v12, :cond_5

    .line 607
    iget v12, p0, Lcom/android/calendar/month/cl;->aj:I

    add-int/2addr v0, v12

    .line 610
    :cond_5
    iget-boolean v12, p0, Lcom/android/calendar/month/cl;->a:Z

    if-eqz v12, :cond_6

    sget-boolean v12, Lcom/android/calendar/month/cl;->aX:Z

    if-nez v12, :cond_6

    .line 611
    mul-int/lit8 v0, v0, 0x2

    iget v12, p0, Lcom/android/calendar/month/cl;->aj:I

    add-int/2addr v0, v12

    .line 614
    :cond_6
    sget-boolean v12, Lcom/android/calendar/month/cl;->aY:Z

    if-eqz v12, :cond_7

    .line 616
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->a:Z

    if-nez v0, :cond_9

    .line 617
    iget v0, p0, Lcom/android/calendar/month/cl;->al:I

    iget v5, p0, Lcom/android/calendar/month/cl;->am:I

    add-int/2addr v0, v5

    .line 618
    iget v5, p0, Lcom/android/calendar/month/cl;->al:I

    add-int/lit8 v5, v5, -0xa

    .line 625
    :cond_7
    :goto_2
    packed-switch v8, :pswitch_data_0

    .line 827
    :cond_8
    :goto_3
    :pswitch_0
    iput v9, p0, Lcom/android/calendar/month/cl;->au:I

    .line 828
    iput v10, p0, Lcom/android/calendar/month/cl;->av:I

    move v4, v1

    .line 829
    goto :goto_0

    .line 620
    :cond_9
    iget v0, p0, Lcom/android/calendar/month/cl;->al:I

    iget v5, p0, Lcom/android/calendar/month/cl;->am:I

    add-int/2addr v0, v5

    .line 621
    iget v5, p0, Lcom/android/calendar/month/cl;->al:I

    goto :goto_2

    .line 630
    :pswitch_1
    if-eqz v6, :cond_e

    if-ne v7, v2, :cond_e

    .line 631
    invoke-direct {p0, v9, v10, v0}, Lcom/android/calendar/month/cl;->a(III)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 632
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/android/calendar/month/cl;->setHoverPointerIcon(I)V

    .line 633
    iget v0, p0, Lcom/android/calendar/month/cl;->ax:I

    iget v2, p0, Lcom/android/calendar/month/cl;->ak:I

    if-le v0, v2, :cond_a

    .line 634
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bl:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 635
    iput v4, p0, Lcom/android/calendar/month/cl;->ax:I

    .line 636
    invoke-direct {p0}, Lcom/android/calendar/month/cl;->g()V

    :goto_4
    move v4, v1

    .line 640
    goto/16 :goto_0

    .line 638
    :cond_a
    iget v0, p0, Lcom/android/calendar/month/cl;->ax:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/cl;->ax:I

    goto :goto_4

    .line 641
    :cond_b
    invoke-direct {p0, v9, v10, v5}, Lcom/android/calendar/month/cl;->b(III)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 642
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/android/calendar/month/cl;->setHoverPointerIcon(I)V

    .line 643
    iget v0, p0, Lcom/android/calendar/month/cl;->ax:I

    iget v2, p0, Lcom/android/calendar/month/cl;->ak:I

    if-le v0, v2, :cond_c

    .line 644
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bl:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 645
    iput v4, p0, Lcom/android/calendar/month/cl;->ax:I

    .line 646
    invoke-direct {p0}, Lcom/android/calendar/month/cl;->f()V

    :goto_5
    move v4, v1

    .line 650
    goto/16 :goto_0

    .line 648
    :cond_c
    iget v0, p0, Lcom/android/calendar/month/cl;->ax:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/cl;->ax:I

    goto :goto_5

    .line 651
    :cond_d
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->aV:Z

    if-nez v0, :cond_e

    .line 652
    invoke-direct {p0, v1}, Lcom/android/calendar/month/cl;->setHoverPointerIcon(I)V

    .line 655
    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x14

    int-to-float v5, v5

    cmpl-float v0, v0, v5

    if-ltz v0, :cond_10

    .line 659
    iput v4, p0, Lcom/android/calendar/month/cl;->aw:I

    .line 660
    iput-boolean v4, p0, Lcom/android/calendar/month/cl;->ay:Z

    .line 662
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 663
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 665
    const-string v6, "YearViewZoomIn"

    invoke-virtual {v0, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 667
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v6

    if-nez v6, :cond_10

    .line 669
    :try_start_1
    iget v6, p0, Lcom/android/calendar/month/cl;->be:I

    if-eq v6, v13, :cond_f

    iget v6, p0, Lcom/android/calendar/month/cl;->be:I

    if-eq v6, v11, :cond_f

    .line 670
    invoke-virtual {v5, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 671
    :cond_f
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    .line 679
    :cond_10
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 681
    iget-boolean v5, p0, Lcom/android/calendar/month/cl;->ay:Z

    if-eqz v5, :cond_28

    if-eq v0, v13, :cond_28

    .line 683
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->b(Landroid/view/MotionEvent;)I

    move-result v5

    .line 684
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 685
    invoke-virtual {p0, v6}, Lcom/android/calendar/month/cl;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 687
    iput v4, p0, Lcom/android/calendar/month/cl;->aw:I

    .line 688
    iput-boolean v4, p0, Lcom/android/calendar/month/cl;->ay:Z

    .line 692
    iget v0, v6, Landroid/graphics/Rect;->right:I

    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v8

    iget v8, p0, Lcom/android/calendar/month/cl;->ar:I

    div-int v8, v0, v8

    .line 695
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->a:Z

    if-nez v0, :cond_19

    .line 696
    add-int/lit8 v0, v5, 0x1

    if-gt v0, v3, :cond_14

    move v0, v1

    .line 705
    :goto_6
    add-int/lit8 v3, v5, 0x1

    iget v11, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v3, v11

    if-nez v3, :cond_17

    .line 706
    iget v2, p0, Lcom/android/calendar/month/cl;->ar:I

    mul-int/2addr v2, v8

    move v3, v0

    .line 751
    :goto_7
    new-instance v8, Lcom/android/calendar/month/cs;

    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    if-ne v7, v1, :cond_11

    move v4, v1

    :cond_11
    invoke-direct {v8, v0, v4}, Lcom/android/calendar/month/cs;-><init>(Landroid/content/Context;Z)V

    .line 753
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 754
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 756
    invoke-virtual {v8, v5}, Lcom/android/calendar/month/cs;->a(I)V

    .line 757
    iget-object v11, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    iget v12, v6, Landroid/graphics/Rect;->bottom:I

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v12, v6

    iget v12, p0, Lcom/android/calendar/month/cl;->aq:I

    div-int/2addr v6, v12

    mul-int/2addr v3, v6

    invoke-virtual {v8, v11, v2, v3}, Lcom/android/calendar/month/cs;->a(Landroid/content/Context;II)V

    .line 758
    iget-object v2, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    invoke-virtual {v8, v2}, Lcom/android/calendar/month/cs;->a(Landroid/text/format/Time;)V

    .line 761
    const-string v2, "YearViewZoomIn"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 763
    if-eqz v0, :cond_26

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_26

    .line 765
    :try_start_2
    iget v2, p0, Lcom/android/calendar/month/cl;->be:I

    if-eq v2, v13, :cond_12

    iget v2, p0, Lcom/android/calendar/month/cl;->be:I

    if-eq v2, v5, :cond_12

    .line 766
    invoke-virtual {v4, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 767
    :cond_12
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1

    .line 795
    :cond_13
    :goto_8
    iget v0, p0, Lcom/android/calendar/month/cl;->av:I

    add-int/lit8 v0, v0, 0xa

    if-ge v10, v0, :cond_8

    iget v0, p0, Lcom/android/calendar/month/cl;->av:I

    add-int/lit8 v0, v0, -0xa

    if-le v10, v0, :cond_8

    iget v0, p0, Lcom/android/calendar/month/cl;->au:I

    add-int/lit8 v0, v0, 0xa

    if-ge v9, v0, :cond_8

    iget v0, p0, Lcom/android/calendar/month/cl;->au:I

    add-int/lit8 v0, v0, -0xa

    if-le v9, v0, :cond_8

    .line 797
    iget v0, p0, Lcom/android/calendar/month/cl;->aw:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/cl;->aw:I

    .line 799
    iget v0, p0, Lcom/android/calendar/month/cl;->aw:I

    const/16 v2, 0xa

    if-le v0, v2, :cond_8

    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->ay:Z

    if-nez v0, :cond_8

    .line 800
    iput-boolean v1, p0, Lcom/android/calendar/month/cl;->ay:Z

    goto/16 :goto_3

    .line 698
    :cond_14
    add-int/lit8 v0, v5, 0x1

    const/4 v11, 0x6

    if-gt v0, v11, :cond_15

    move v0, v2

    .line 699
    goto/16 :goto_6

    .line 700
    :cond_15
    add-int/lit8 v0, v5, 0x1

    const/16 v11, 0x9

    if-gt v0, v11, :cond_16

    move v0, v3

    .line 701
    goto/16 :goto_6

    .line 702
    :cond_16
    add-int/lit8 v0, v5, 0x1

    const/16 v3, 0xc

    if-gt v0, v3, :cond_2c

    .line 703
    const/4 v0, 0x4

    goto/16 :goto_6

    .line 707
    :cond_17
    add-int/lit8 v3, v5, 0x1

    iget v11, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v3, v11

    if-ne v3, v1, :cond_18

    .line 708
    mul-int/lit8 v2, v8, 0x1

    move v3, v0

    goto/16 :goto_7

    .line 709
    :cond_18
    add-int/lit8 v3, v5, 0x1

    iget v11, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v3, v11

    if-ne v3, v2, :cond_29

    .line 710
    mul-int/lit8 v2, v8, 0x2

    move v3, v0

    goto/16 :goto_7

    .line 713
    :cond_19
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->D(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 714
    add-int/lit8 v0, v5, 0x1

    const/4 v11, 0x6

    if-gt v0, v11, :cond_1a

    move v0, v1

    .line 719
    :goto_9
    add-int/lit8 v11, v5, 0x1

    iget v12, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v11, v12

    if-nez v11, :cond_1b

    .line 720
    iget v2, p0, Lcom/android/calendar/month/cl;->ar:I

    mul-int/2addr v2, v8

    move v3, v0

    goto/16 :goto_7

    .line 716
    :cond_1a
    add-int/lit8 v0, v5, 0x1

    const/16 v11, 0xc

    if-gt v0, v11, :cond_2b

    move v0, v2

    .line 717
    goto :goto_9

    .line 721
    :cond_1b
    add-int/lit8 v11, v5, 0x1

    iget v12, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v11, v12

    if-ne v11, v1, :cond_1c

    .line 722
    mul-int/lit8 v2, v8, 0x1

    move v3, v0

    goto/16 :goto_7

    .line 723
    :cond_1c
    add-int/lit8 v11, v5, 0x1

    iget v12, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v11, v12

    if-ne v11, v2, :cond_1d

    .line 724
    mul-int/lit8 v2, v8, 0x2

    move v3, v0

    goto/16 :goto_7

    .line 725
    :cond_1d
    add-int/lit8 v2, v5, 0x1

    iget v11, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v2, v11

    if-ne v2, v3, :cond_1e

    .line 726
    mul-int/lit8 v2, v8, 0x3

    move v3, v0

    goto/16 :goto_7

    .line 727
    :cond_1e
    add-int/lit8 v2, v5, 0x1

    iget v3, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v2, v3

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1f

    .line 728
    mul-int/lit8 v2, v8, 0x4

    move v3, v0

    goto/16 :goto_7

    .line 729
    :cond_1f
    add-int/lit8 v2, v5, 0x1

    iget v3, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v2, v3

    const/4 v3, 0x5

    if-ne v2, v3, :cond_29

    .line 730
    mul-int/lit8 v2, v8, 0x5

    move v3, v0

    goto/16 :goto_7

    .line 733
    :cond_20
    add-int/lit8 v0, v5, 0x1

    const/4 v11, 0x4

    if-gt v0, v11, :cond_21

    move v0, v1

    .line 740
    :goto_a
    add-int/lit8 v11, v5, 0x1

    iget v12, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v11, v12

    if-nez v11, :cond_23

    .line 741
    iget v2, p0, Lcom/android/calendar/month/cl;->ar:I

    mul-int/2addr v2, v8

    move v3, v0

    goto/16 :goto_7

    .line 735
    :cond_21
    add-int/lit8 v0, v5, 0x1

    const/16 v11, 0x8

    if-gt v0, v11, :cond_22

    move v0, v2

    .line 736
    goto :goto_a

    .line 737
    :cond_22
    add-int/lit8 v0, v5, 0x1

    const/16 v11, 0xc

    if-gt v0, v11, :cond_2a

    move v0, v3

    .line 738
    goto :goto_a

    .line 742
    :cond_23
    add-int/lit8 v11, v5, 0x1

    iget v12, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v11, v12

    if-ne v11, v1, :cond_24

    .line 743
    mul-int/lit8 v2, v8, 0x1

    move v3, v0

    goto/16 :goto_7

    .line 744
    :cond_24
    add-int/lit8 v11, v5, 0x1

    iget v12, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v11, v12

    if-ne v11, v2, :cond_25

    .line 745
    mul-int/lit8 v2, v8, 0x2

    move v3, v0

    goto/16 :goto_7

    .line 746
    :cond_25
    add-int/lit8 v2, v5, 0x1

    iget v11, p0, Lcom/android/calendar/month/cl;->ar:I

    rem-int/2addr v2, v11

    if-ne v2, v3, :cond_29

    .line 747
    mul-int/lit8 v2, v8, 0x3

    move v3, v0

    goto/16 :goto_7

    .line 773
    :cond_26
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_27

    if-ne v7, v1, :cond_27

    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v0}, Lcom/android/calendar/dz;->o(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 775
    iget-object v0, p0, Lcom/android/calendar/month/cl;->g:Landroid/os/Handler;

    new-instance v2, Lcom/android/calendar/month/cm;

    invoke-direct {v2, p0}, Lcom/android/calendar/month/cm;-><init>(Lcom/android/calendar/month/cl;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 784
    :cond_27
    sget-object v2, Lcom/android/calendar/hj;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v2, v0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 785
    const-string v0, "YearViewZoomIn"

    invoke-virtual {v4, v8, v0}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 786
    iput v5, p0, Lcom/android/calendar/month/cl;->be:I

    .line 787
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_8

    .line 789
    :cond_28
    if-ne v0, v13, :cond_13

    .line 790
    iput v4, p0, Lcom/android/calendar/month/cl;->aw:I

    .line 791
    iput-boolean v4, p0, Lcom/android/calendar/month/cl;->ay:Z

    goto/16 :goto_8

    .line 806
    :pswitch_2
    iput v4, p0, Lcom/android/calendar/month/cl;->aw:I

    .line 807
    iput-boolean v4, p0, Lcom/android/calendar/month/cl;->ay:Z

    .line 808
    invoke-direct {p0, v1}, Lcom/android/calendar/month/cl;->setHoverPointerIcon(I)V

    .line 809
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 810
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 812
    const-string v3, "YearViewZoomIn"

    invoke-virtual {v0, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 813
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_8

    .line 815
    :try_start_3
    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 816
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_3

    .line 817
    :catch_0
    move-exception v0

    goto/16 :goto_3

    .line 768
    :catch_1
    move-exception v0

    goto/16 :goto_3

    .line 672
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 593
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_29
    move v2, v4

    move v3, v0

    goto/16 :goto_7

    :cond_2a
    move v0, v4

    goto/16 :goto_a

    :cond_2b
    move v0, v4

    goto/16 :goto_9

    :cond_2c
    move v0, v4

    goto/16 :goto_6

    .line 625
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private b(I)I
    .locals 3

    .prologue
    .line 1266
    iget v0, p0, Lcom/android/calendar/month/cl;->V:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/android/calendar/month/cl;->V:I

    iget v1, p0, Lcom/android/calendar/month/cl;->W:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/cl;->ap:I

    iget v2, p0, Lcom/android/calendar/month/cl;->W:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/cl;->aq:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    if-le p1, v0, :cond_1

    .line 1268
    :cond_0
    const/4 v0, -0x1

    .line 1269
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/android/calendar/month/cl;->bb:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/android/calendar/month/cl;->V:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/cl;->ap:I

    iget v2, p0, Lcom/android/calendar/month/cl;->W:I

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    goto :goto_0
.end method

.method private b(Landroid/view/MotionEvent;)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 1279
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lcom/android/calendar/month/cl;->b(I)I

    move-result v1

    .line 1280
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, v2}, Lcom/android/calendar/month/cl;->c(I)I

    move-result v2

    .line 1281
    if-eq v1, v0, :cond_0

    if-ne v2, v0, :cond_1

    .line 1304
    :cond_0
    :goto_0
    return v0

    .line 1284
    :cond_1
    iget v0, p0, Lcom/android/calendar/month/cl;->ar:I

    mul-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 1285
    iget-object v1, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    iget v2, v1, Landroid/text/format/Time;->monthDay:I

    .line 1287
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 1288
    iget-object v1, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    invoke-virtual {v3, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1289
    iget-object v1, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    iget v4, v3, Landroid/text/format/Time;->year:I

    invoke-virtual {v3, v1, v0, v4}, Landroid/text/format/Time;->set(III)V

    .line 1292
    const/4 v1, 0x4

    :try_start_0
    invoke-virtual {v3, v1}, Landroid/text/format/Time;->getActualMaximum(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1297
    :goto_1
    if-le v2, v1, :cond_2

    .line 1298
    iput v1, v3, Landroid/text/format/Time;->monthDay:I

    .line 1302
    :goto_2
    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Landroid/text/format/Time;->normalize(Z)J

    goto :goto_0

    .line 1293
    :catch_0
    move-exception v1

    .line 1294
    const/16 v1, 0xf

    goto :goto_1

    .line 1300
    :cond_2
    iput v2, v3, Landroid/text/format/Time;->monthDay:I

    goto :goto_2
.end method

.method static synthetic b(Lcom/android/calendar/month/cl;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/month/cl;->m:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/month/cl;I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->e(I)V

    return-void
.end method

.method private b(III)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1754
    sget-boolean v2, Lcom/android/calendar/month/cl;->aY:Z

    if-eqz v2, :cond_2

    .line 1755
    if-ge p1, p3, :cond_1

    iget v2, p0, Lcom/android/calendar/month/cl;->am:I

    sub-int v2, p3, v2

    if-le p1, v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-le p2, v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    if-ge p2, v2, :cond_1

    .line 1765
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1759
    goto :goto_0

    .line 1762
    :cond_2
    if-ge p1, p3, :cond_3

    const/16 v2, 0xa

    if-le p2, v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v2, v2, -0xa

    if-lt p2, v2, :cond_0

    :cond_3
    move v0, v1

    .line 1765
    goto :goto_0
.end method

.method private c(I)I
    .locals 3

    .prologue
    .line 1273
    iget v0, p0, Lcom/android/calendar/month/cl;->Q:I

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/cl;->Q:I

    sub-int/2addr v0, v1

    if-le p1, v0, :cond_1

    .line 1274
    :cond_0
    const/4 v0, -0x1

    .line 1275
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/android/calendar/month/cl;->Q:I

    sub-int v0, p1, v0

    iget v1, p0, Lcom/android/calendar/month/cl;->ao:I

    iget v2, p0, Lcom/android/calendar/month/cl;->U:I

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/month/cl;I)I
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->b(I)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/month/cl;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/month/cl;I)I
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->c(I)I

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/month/cl;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    return-object v0
.end method

.method private d()V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 1478
    iget-object v0, p0, Lcom/android/calendar/month/cl;->q:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1479
    iput v1, p0, Lcom/android/calendar/month/cl;->bg:F

    .line 1480
    iput v1, p0, Lcom/android/calendar/month/cl;->bh:F

    .line 1494
    :goto_0
    return-void

    .line 1482
    :cond_0
    iget v0, p0, Lcom/android/calendar/month/cl;->B:I

    iget v1, p0, Lcom/android/calendar/month/cl;->ao:I

    iget v2, p0, Lcom/android/calendar/month/cl;->U:I

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/cl;->Q:I

    add-int/2addr v0, v1

    .line 1483
    iget v1, p0, Lcom/android/calendar/month/cl;->V:I

    iget v2, p0, Lcom/android/calendar/month/cl;->A:I

    iget v3, p0, Lcom/android/calendar/month/cl;->ap:I

    iget v4, p0, Lcom/android/calendar/month/cl;->W:I

    add-int/2addr v3, v4

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/cl;->bb:I

    sub-int/2addr v1, v2

    .line 1485
    iget v2, p0, Lcom/android/calendar/month/cl;->ao:I

    add-int/2addr v2, v0

    .line 1486
    iget v3, p0, Lcom/android/calendar/month/cl;->ap:I

    add-int/2addr v3, v1

    .line 1487
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1488
    iget-object v5, p0, Lcom/android/calendar/month/cl;->m:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1489
    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1490
    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v6, p0, Lcom/android/calendar/month/cl;->m:Landroid/app/Activity;

    invoke-static {v6}, Lcom/android/calendar/hj;->v(Landroid/content/Context;)I

    move-result v6

    sub-int/2addr v4, v6

    .line 1491
    add-int/2addr v0, v2

    int-to-float v0, v0

    div-float/2addr v0, v7

    int-to-float v2, v5

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/month/cl;->bg:F

    .line 1492
    add-int v0, v1, v3

    int-to-float v0, v0

    div-float/2addr v0, v7

    int-to-float v1, v4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/cl;->bh:F

    goto :goto_0
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 1694
    if-ltz p1, :cond_0

    const/16 v0, 0xc

    if-ge p1, v0, :cond_0

    .line 1695
    const v0, 0x8000

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 1696
    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-static {p1, v1, v2}, Lcom/android/calendar/gm;->a(IIZ)Ljava/lang/String;

    move-result-object v1

    .line 1699
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1700
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 1701
    invoke-virtual {p0, v0}, Lcom/android/calendar/month/cl;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1703
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/month/cl;I)I
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/android/calendar/month/cl;->A:I

    return p1
.end method

.method static synthetic e(Lcom/android/calendar/month/cl;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aZ:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1607
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1608
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    const v1, 0x7f020066

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->c(I)V

    .line 1610
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    iget-object v0, v0, Lcom/android/calendar/AllInOneActivity;->g:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1611
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/cl;->k:Z

    .line 1612
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->a(I)V

    .line 1613
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->a()V

    .line 1614
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v3}, Lcom/android/calendar/AllInOneActivity;->b(I)V

    .line 1615
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v3}, Lcom/android/calendar/AllInOneActivity;->d(I)V

    .line 1616
    invoke-direct {p0}, Lcom/android/calendar/month/cl;->d()V

    .line 1617
    invoke-direct {p0}, Lcom/android/calendar/month/cl;->getBeforeAnimation()Landroid/view/animation/AnimationSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->s:Landroid/view/animation/AnimationSet;

    .line 1618
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, p0, Lcom/android/calendar/month/cl;->s:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/view/animation/Animation;)V

    .line 1619
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v2}, Lcom/android/calendar/AllInOneActivity;->b(I)V

    .line 1620
    invoke-direct {p0}, Lcom/android/calendar/month/cl;->getAfterAnimation()Landroid/view/animation/AnimationSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->r:Landroid/view/animation/AnimationSet;

    .line 1621
    iget-object v0, p0, Lcom/android/calendar/month/cl;->r:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/android/calendar/month/cl;->bk:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1622
    iget-object v0, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, p0, Lcom/android/calendar/month/cl;->r:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->b(Landroid/view/animation/Animation;)V

    .line 1623
    return-void
.end method

.method private e(I)V
    .locals 3

    .prologue
    .line 1706
    if-ltz p1, :cond_0

    .line 1707
    const v0, 0x8000

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 1708
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1711
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1712
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 1713
    invoke-virtual {p0, v0}, Lcom/android/calendar/month/cl;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1715
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/month/cl;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/android/calendar/month/cl;->aq:I

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/month/cl;I)I
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/android/calendar/month/cl;->B:I

    return p1
.end method

.method private f()V
    .locals 1

    .prologue
    .line 1658
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/month/cl;->a(Z)Landroid/text/format/Time;

    move-result-object v0

    .line 1659
    invoke-direct {p0, v0}, Lcom/android/calendar/month/cl;->a(Landroid/text/format/Time;)V

    .line 1660
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/month/cl;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/android/calendar/month/cl;->A:I

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/month/cl;I)I
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/android/calendar/month/cl;->ba:I

    return p1
.end method

.method private g()V
    .locals 1

    .prologue
    .line 1689
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/month/cl;->a(Z)Landroid/text/format/Time;

    move-result-object v0

    .line 1690
    invoke-direct {p0, v0}, Lcom/android/calendar/month/cl;->a(Landroid/text/format/Time;)V

    .line 1691
    return-void
.end method

.method private getAfterAnimation()Landroid/view/animation/AnimationSet;
    .locals 14

    .prologue
    const-wide/16 v12, 0x12c

    const v10, 0x10a0006

    const v1, 0x3e4ccccd    # 0.2f

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1515
    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1517
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v3, 0x0

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1518
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1519
    iget-object v3, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v3, v10}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/content/Context;I)V

    .line 1520
    const-wide/16 v6, 0x258

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    .line 1521
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1523
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget v6, p0, Lcom/android/calendar/month/cl;->bg:F

    iget v8, p0, Lcom/android/calendar/month/cl;->bh:F

    move v3, v1

    move v4, v2

    move v7, v5

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1524
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 1525
    iget-object v1, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v1, v10}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/content/Context;I)V

    .line 1526
    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 1527
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1529
    return-object v9
.end method

.method private getBeforeAnimation()Landroid/view/animation/AnimationSet;
    .locals 13

    .prologue
    const v12, 0x10a0006

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v5, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    const-wide/16 v10, 0x12c

    .line 1497
    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1499
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1500
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1501
    iget-object v3, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v3, v12}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/content/Context;I)V

    .line 1502
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    .line 1503
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1505
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget v6, p0, Lcom/android/calendar/month/cl;->bg:F

    iget v8, p0, Lcom/android/calendar/month/cl;->bh:F

    move v3, v1

    move v4, v2

    move v7, v5

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1506
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 1507
    iget-object v1, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0, v1, v12}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/content/Context;I)V

    .line 1508
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 1509
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1511
    return-object v9
.end method

.method static synthetic h(Lcom/android/calendar/month/cl;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/android/calendar/month/cl;->ar:I

    return v0
.end method

.method static synthetic i(Lcom/android/calendar/month/cl;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/android/calendar/month/cl;->B:I

    return v0
.end method

.method private setHoverPointerIcon(I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1726
    if-ne p1, v0, :cond_0

    .line 1727
    iput-boolean v0, p0, Lcom/android/calendar/month/cl;->aV:Z

    .line 1731
    :goto_0
    invoke-static {p1}, Lcom/android/calendar/ek;->a(I)V

    .line 1732
    return-void

    .line 1729
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/cl;->aV:Z

    goto :goto_0
.end method

.method private setYearWeekday(I)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v5, 0x0

    .line 1535
    const-string v0, "persist.sys.language"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1536
    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1537
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0x6

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f012e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1538
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0x7

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f012f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1539
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0x8

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0130

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1540
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0x9

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0131

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1541
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0xa

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0132

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1542
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0xb

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0133

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1543
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    rsub-int/lit8 v1, p1, 0xc

    rem-int/lit8 v1, v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0134

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1583
    :goto_0
    return-void

    .line 1546
    :cond_0
    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "lo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1547
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 1549
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x2

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1551
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x3

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1553
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x4

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 1555
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x5

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 1557
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    const/4 v1, 0x5

    add-int/lit8 v2, p1, 0x6

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v3}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1559
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    const/4 v1, 0x6

    add-int/lit8 v2, p1, 0x7

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v3}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0

    .line 1563
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 1566
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x2

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1569
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x3

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1572
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x4

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 1575
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    add-int/lit8 v1, p1, 0x5

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 1578
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    const/4 v1, 0x5

    add-int/lit8 v2, p1, 0x6

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1581
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aU:[Ljava/lang/String;

    const/4 v1, 0x6

    add-int/lit8 v2, p1, 0x7

    rem-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2, v5}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public a(I)Landroid/text/format/Time;
    .locals 6

    .prologue
    .line 1247
    iget-object v0, p0, Lcom/android/calendar/month/cl;->w:Landroid/text/format/Time;

    .line 1248
    iget-object v1, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1250
    iget v1, v0, Landroid/text/format/Time;->year:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/text/format/Time;->year:I

    .line 1251
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 1253
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1254
    const/16 v1, 0x1f

    const/16 v2, 0xb

    const/16 v3, 0x7f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/format/Time;->set(III)V

    .line 1255
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1258
    :cond_0
    return-object v0
.end method

.method protected final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1587
    const/4 v0, 0x3

    new-array v1, v0, [I

    .line 1589
    const-string v0, "XXXXXXR"

    .line 1591
    iget v2, p0, Lcom/android/calendar/month/cl;->aD:I

    aput v2, v1, v4

    .line 1592
    const/4 v2, 0x1

    iget v3, p0, Lcom/android/calendar/month/cl;->aE:I

    aput v3, v1, v2

    .line 1593
    const/4 v2, 0x2

    iget v3, p0, Lcom/android/calendar/month/cl;->aF:I

    aput v3, v1, v2

    .line 1596
    :try_start_0
    invoke-static {}, Lcom/android/calendar/dz;->e()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1600
    :goto_0
    iget v2, p0, Lcom/android/calendar/month/cl;->aS:I

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/month/cl;->b:[I

    .line 1602
    iget v2, p0, Lcom/android/calendar/month/cl;->aG:I

    aput v2, v1, v4

    .line 1603
    iget v2, p0, Lcom/android/calendar/month/cl;->aS:I

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;[II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/cl;->c:[I

    .line 1604
    return-void

    .line 1597
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method a(J)V
    .locals 3

    .prologue
    .line 1802
    iget-object v0, p0, Lcom/android/calendar/month/cl;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/cl;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1803
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/cl;->f:Z

    .line 1804
    iget-object v0, p0, Lcom/android/calendar/month/cl;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/cl;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1805
    return-void
.end method

.method public a(Landroid/text/format/Time;IZ)V
    .locals 9

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/android/calendar/month/cl;->m:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 1348
    if-eqz p3, :cond_0

    .line 1349
    invoke-direct {p0}, Lcom/android/calendar/month/cl;->e()V

    .line 1351
    :cond_0
    sget-boolean v1, Lcom/android/calendar/month/cl;->aX:Z

    if-nez v1, :cond_1

    .line 1352
    iget-object v1, p0, Lcom/android/calendar/month/cl;->y:Landroid/text/format/Time;

    invoke-virtual {v1, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1354
    :cond_1
    const-wide/16 v2, 0x20

    const-wide/16 v6, -0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p1

    move v8, p2

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 1355
    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->c()V

    .line 1357
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1719
    iput v0, p0, Lcom/android/calendar/month/cl;->bb:I

    .line 1720
    iput v0, p0, Lcom/android/calendar/month/cl;->bd:I

    .line 1721
    iget v0, p0, Lcom/android/calendar/month/cl;->as:I

    iput v0, p0, Lcom/android/calendar/month/cl;->aq:I

    .line 1722
    iget v0, p0, Lcom/android/calendar/month/cl;->at:I

    iput v0, p0, Lcom/android/calendar/month/cl;->ar:I

    .line 1723
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 1798
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/cl;->a(J)V

    .line 1799
    return-void
.end method

.method public getSelectedDate()Landroid/text/format/Time;
    .locals 2

    .prologue
    .line 1314
    iget-object v0, p0, Lcom/android/calendar/month/cl;->w:Landroid/text/format/Time;

    .line 1315
    iget-object v1, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1317
    const/4 v1, 0x0

    iput v1, v0, Landroid/text/format/Time;->second:I

    iput v1, v0, Landroid/text/format/Time;->minute:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 1320
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1321
    return-object v0
.end method

.method public getSelectedMillis()J
    .locals 2

    .prologue
    .line 1325
    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getSelectedDate()Landroid/text/format/Time;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTime()Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1342
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1343
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/month/cl;->setHoverPointerIcon(I)V

    .line 1344
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 856
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 857
    iget-object v1, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/cl;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 859
    sget-boolean v1, Lcom/android/calendar/month/cl;->aX:Z

    if-nez v1, :cond_1

    .line 860
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/android/calendar/month/cl;->aR:I

    .line 861
    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->bi:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/cl;->a:Z

    if-nez v0, :cond_0

    .line 862
    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0441

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/cl;->aR:I

    .line 864
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 865
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 866
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 867
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 868
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aQ:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/calendar/month/cl;->aW:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 869
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aQ:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 873
    :goto_0
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->a(Landroid/graphics/Canvas;)V

    .line 874
    return-void

    .line 871
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aP:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 520
    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 521
    iget-object v2, p0, Lcom/android/calendar/month/cl;->n:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 558
    :goto_0
    return v0

    .line 524
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 525
    iget-object v1, p0, Lcom/android/calendar/month/cl;->bl:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 528
    :cond_1
    const-string v0, "accessibility"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/month/cl;->aZ:Landroid/view/accessibility/AccessibilityManager;

    .line 530
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 531
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->a(Landroid/view/MotionEvent;)Z

    .line 545
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/month/cl;->aZ:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 546
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    .line 547
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cl;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 549
    iget v1, p0, Lcom/android/calendar/month/cl;->j:I

    if-eq v0, v1, :cond_2

    .line 550
    invoke-direct {p0, v0}, Lcom/android/calendar/month/cl;->d(I)V

    .line 551
    iput v0, p0, Lcom/android/calendar/month/cl;->j:I

    .line 558
    :cond_2
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 533
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_1
    goto :goto_1

    .line 554
    :cond_4
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/month/cl;->j:I

    goto :goto_2

    .line 533
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1647
    const/16 v1, 0x16

    if-ne p1, v1, :cond_0

    .line 1648
    invoke-direct {p0}, Lcom/android/calendar/month/cl;->g()V

    .line 1654
    :goto_0
    return v0

    .line 1650
    :cond_0
    const/16 v1, 0x15

    if-ne p1, v1, :cond_1

    .line 1651
    invoke-direct {p0}, Lcom/android/calendar/month/cl;->f()V

    goto :goto_0

    .line 1654
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/android/calendar/month/cl;->p:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1310
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setSelectedTime(Landroid/text/format/Time;)V
    .locals 5

    .prologue
    .line 833
    iget-object v0, p0, Lcom/android/calendar/month/cl;->v:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 834
    new-instance v0, Lcom/android/calendar/month/cg;

    iget v1, p1, Landroid/text/format/Time;->year:I

    iget v2, p1, Landroid/text/format/Time;->month:I

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    iget v4, p0, Lcom/android/calendar/month/cl;->aS:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendar/month/cg;-><init>(IIII)V

    iput-object v0, p0, Lcom/android/calendar/month/cl;->z:Lcom/android/calendar/month/cg;

    .line 838
    iget-object v0, p0, Lcom/android/calendar/month/cl;->bf:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 839
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 844
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/month/cl;->x:Landroid/text/format/Time;

    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 845
    iget-object v0, p0, Lcom/android/calendar/month/cl;->x:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 847
    sget-boolean v0, Lcom/android/calendar/month/cl;->aX:Z

    if-nez v0, :cond_0

    .line 848
    iget-object v0, p0, Lcom/android/calendar/month/cl;->y:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 851
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/month/cl;->invalidate()V

    .line 852
    return-void

    .line 841
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/cl;->m:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
