.class Lcom/android/calendar/month/bu;
.super Ljava/lang/Object;
.source "MonthWeekEventsView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/bo;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bo;)V
    .locals 0

    .prologue
    .line 3627
    iput-object p1, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3630
    iget-object v0, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget v0, v0, Lcom/android/calendar/month/bo;->aW:I

    iget-object v1, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget v1, v1, Lcom/android/calendar/month/bo;->aZ:I

    sub-int/2addr v0, v1

    .line 3632
    if-gez v0, :cond_3

    .line 3633
    add-int/lit8 v0, v0, 0x7

    move v1, v0

    .line 3635
    :goto_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 3636
    invoke-static {v0}, Lcom/android/calendar/month/bo;->b(F)F

    .line 3637
    float-to-double v2, v0

    const-wide v4, 0x3fe3333333333333L    # 0.6

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 3638
    iget-object v0, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/calendar/month/bo;->a(Lcom/android/calendar/month/bo;Z)Z

    .line 3646
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget-boolean v0, v0, Lcom/android/calendar/month/bo;->aT:Z

    if-eqz v0, :cond_2

    .line 3647
    iget-object v0, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget-object v2, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    add-int/lit8 v1, v1, -0x1

    invoke-static {v2, v1}, Lcom/android/calendar/month/bo;->b(Lcom/android/calendar/month/bo;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget v2, v2, Lcom/android/calendar/month/bo;->aR:I

    iget-object v3, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget v3, v3, Lcom/android/calendar/month/bo;->aS:I

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/android/calendar/month/bo;->invalidate(IIII)V

    .line 3652
    :goto_2
    return-void

    .line 3641
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    invoke-static {v2, v6}, Lcom/android/calendar/month/bo;->a(Lcom/android/calendar/month/bo;Z)Z

    .line 3642
    float-to-double v2, v0

    const-wide v4, 0x3fb999999999999aL    # 0.1

    rem-double/2addr v2, v4

    const-wide/16 v4, 0x0

    cmpl-double v0, v2, v4

    if-nez v0, :cond_0

    .line 3643
    iget-object v0, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget-object v2, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    invoke-static {v2}, Lcom/android/calendar/month/bo;->b(Lcom/android/calendar/month/bo;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/android/calendar/month/bo;->a(Lcom/android/calendar/month/bo;I)I

    goto :goto_1

    .line 3650
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget-object v2, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    invoke-static {v2, v1}, Lcom/android/calendar/month/bo;->b(Lcom/android/calendar/month/bo;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget v2, v2, Lcom/android/calendar/month/bo;->aR:I

    iget-object v3, p0, Lcom/android/calendar/month/bu;->a:Lcom/android/calendar/month/bo;

    iget v3, v3, Lcom/android/calendar/month/bo;->aS:I

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/android/calendar/month/bo;->invalidate(IIII)V

    goto :goto_2

    :cond_3
    move v1, v0

    goto :goto_0
.end method
