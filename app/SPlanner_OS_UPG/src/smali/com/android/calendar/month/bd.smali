.class Lcom/android/calendar/month/bd;
.super Ljava/lang/Object;
.source "MonthHoverEventFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/bb;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bb;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lcom/android/calendar/month/bd;->a:Lcom/android/calendar/month/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 606
    iget-object v0, p0, Lcom/android/calendar/month/bd;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->k(Lcom/android/calendar/month/bb;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/month/bb;->a(Z)Z

    .line 608
    iget-object v0, p0, Lcom/android/calendar/month/bd;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->b(Lcom/android/calendar/month/bb;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/bd;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->j(Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 610
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 4

    .prologue
    .line 596
    if-eqz p2, :cond_0

    .line 597
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/month/bb;->a(Z)Z

    .line 602
    :goto_0
    return-void

    .line 599
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/calendar/month/bb;->a(Z)Z

    .line 600
    iget-object v0, p0, Lcom/android/calendar/month/bd;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->b(Lcom/android/calendar/month/bb;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/bd;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->j(Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bg;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
