.class Lcom/android/calendar/month/al;
.super Ljava/lang/Object;
.source "MonthEventAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field a:Lcom/android/calendar/month/ba;

.field final synthetic b:Lcom/android/calendar/month/aj;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/android/calendar/month/aj;)V
    .locals 1

    .prologue
    .line 626
    iput-object p1, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/month/al;->c:Z

    .line 624
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/month/al;->a:Lcom/android/calendar/month/ba;

    .line 628
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 700
    const-string v6, ""

    .line 701
    iget-object v0, p0, Lcom/android/calendar/month/al;->a:Lcom/android/calendar/month/ba;

    iget v0, v0, Lcom/android/calendar/month/ba;->c:I

    if-ne v0, v10, :cond_2

    .line 702
    iget-object v0, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->j(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v11

    const-string v3, "accountKey"

    aput-object v3, v2, v7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/calendar/month/al;->a:Lcom/android/calendar/month/ba;

    iget-wide v8, v5, Lcom/android/calendar/month/ba;->f:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 705
    const-wide/16 v0, 0x0

    .line 706
    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 707
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    move-wide v8, v0

    .line 709
    :goto_0
    if-eqz v2, :cond_0

    .line 710
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 712
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->k(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v11

    const-string v3, "displayName"

    aput-object v3, v2, v7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_sync_account_key="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 716
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 717
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 720
    :goto_1
    if-eqz v1, :cond_1

    .line 721
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 728
    :cond_1
    :goto_2
    return-object v0

    .line 725
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/al;->a:Lcom/android/calendar/month/ba;

    iget-object v0, v0, Lcom/android/calendar/month/ba;->C:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v0, v6

    goto :goto_1

    :cond_4
    move-wide v8, v0

    goto :goto_0
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x32

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 632
    iget-object v0, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->e(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->c(Landroid/content/Context;)Z

    move-result v0

    .line 633
    iget-object v1, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    invoke-static {v1}, Lcom/android/calendar/month/aj;->f(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/dz;->d(Landroid/content/Context;)Z

    move-result v1

    .line 635
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 696
    :cond_0
    :goto_0
    return v5

    .line 638
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 639
    if-ltz v0, :cond_0

    .line 643
    iget-object v1, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    iget-object v1, v1, Lcom/android/calendar/month/aj;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/ba;

    iput-object v0, p0, Lcom/android/calendar/month/al;->a:Lcom/android/calendar/month/ba;

    .line 650
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 651
    iget-object v0, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->g(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c01f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 652
    iget-object v0, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->h(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c01f2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 657
    :goto_1
    const-string v2, "translationX"

    new-array v3, v4, [F

    aput v1, v3, v5

    aput v0, v3, v6

    invoke-static {p1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 659
    const-string v3, "translationX"

    new-array v4, v4, [F

    aput v0, v4, v5

    aput v1, v4, v6

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 661
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 675
    :pswitch_1
    sget v1, Lcom/android/calendar/month/aj;->e:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/android/calendar/month/aj;->e:I

    .line 676
    sget v1, Lcom/android/calendar/month/aj;->e:I

    const/4 v3, 0x7

    if-ne v1, v3, :cond_0

    .line 677
    iget-boolean v1, p0, Lcom/android/calendar/month/al;->c:Z

    if-nez v1, :cond_0

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 678
    sput v5, Lcom/android/calendar/month/aj;->e:I

    .line 679
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 680
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 681
    iput-boolean v6, p0, Lcom/android/calendar/month/al;->c:Z

    goto/16 :goto_0

    .line 654
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/al;->b:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->i(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 655
    const/4 v0, 0x0

    goto :goto_1

    .line 664
    :pswitch_2
    sget v0, Lcom/android/calendar/month/aj;->e:I

    const/16 v1, 0x64

    if-le v0, v1, :cond_3

    .line 665
    sput v5, Lcom/android/calendar/month/aj;->e:I

    .line 668
    :cond_3
    const v0, 0x7f1200b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 669
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 670
    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/calendar/month/al;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 687
    :pswitch_3
    sput v5, Lcom/android/calendar/month/aj;->e:I

    .line 688
    iget-boolean v1, p0, Lcom/android/calendar/month/al;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 690
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 691
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 692
    iput-boolean v5, p0, Lcom/android/calendar/month/al;->c:Z

    goto/16 :goto_0

    .line 661
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
