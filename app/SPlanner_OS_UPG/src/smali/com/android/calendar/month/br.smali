.class Lcom/android/calendar/month/br;
.super Ljava/lang/Object;
.source "MonthWeekEventsView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/bo;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bo;)V
    .locals 0

    .prologue
    .line 3511
    iput-object p1, p0, Lcom/android/calendar/month/br;->a:Lcom/android/calendar/month/bo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 3514
    iget-object v0, p0, Lcom/android/calendar/month/br;->a:Lcom/android/calendar/month/bo;

    invoke-static {v0}, Lcom/android/calendar/month/bo;->a(Lcom/android/calendar/month/bo;)Lcom/android/calendar/month/bb;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3537
    :goto_0
    return-void

    .line 3516
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/br;->a:Lcom/android/calendar/month/bo;

    iget-object v0, v0, Lcom/android/calendar/month/bo;->bI:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 3517
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 3518
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 3521
    const-string v3, "MonthHoverEventList"

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 3523
    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v1}, Landroid/app/Fragment;->isRemoving()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3524
    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3528
    :cond_1
    sget-object v1, Lcom/android/calendar/hj;->r:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 3530
    iget-object v0, p0, Lcom/android/calendar/month/br;->a:Lcom/android/calendar/month/bo;

    invoke-static {v0}, Lcom/android/calendar/month/bo;->a(Lcom/android/calendar/month/bo;)Lcom/android/calendar/month/bb;

    move-result-object v0

    const-string v1, "MonthHoverEventList"

    invoke-virtual {v2, v0, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 3531
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3533
    :catch_0
    move-exception v0

    goto :goto_0
.end method
