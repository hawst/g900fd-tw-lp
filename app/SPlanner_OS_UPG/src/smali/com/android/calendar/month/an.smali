.class Lcom/android/calendar/month/an;
.super Ljava/lang/Object;
.source "MonthEventAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/aj;

.field private b:Landroid/widget/HoverPopupWindow;


# direct methods
.method private constructor <init>(Lcom/android/calendar/month/aj;)V
    .locals 0

    .prologue
    .line 550
    iput-object p1, p0, Lcom/android/calendar/month/an;->a:Lcom/android/calendar/month/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/month/aj;Lcom/android/calendar/month/ak;)V
    .locals 0

    .prologue
    .line 550
    invoke-direct {p0, p1}, Lcom/android/calendar/month/an;-><init>(Lcom/android/calendar/month/aj;)V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 557
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 617
    :cond_0
    :goto_0
    return v5

    .line 560
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 577
    :pswitch_1
    const v0, 0x7f1200b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 578
    const v1, 0x7f1200b4

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 580
    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    .line 583
    :cond_2
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/widget/TextView;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v1}, Lcom/android/calendar/hj;->a(Landroid/widget/TextView;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    iget-object v2, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    if-eqz v2, :cond_6

    .line 584
    iget-object v2, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v2}, Landroid/widget/HoverPopupWindow;->getContent()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 585
    if-eqz v2, :cond_5

    .line 586
    const v3, 0x7f120034

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 587
    const v4, 0x7f120035

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 589
    if-eqz v3, :cond_4

    .line 590
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 591
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 592
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593
    iget-object v3, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 596
    :cond_4
    if-eqz v2, :cond_5

    .line 597
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 598
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 599
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 600
    iget-object v0, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 604
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/month/an;->a:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->c(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 605
    sget-object v1, Lcom/android/calendar/hj;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/month/an;->a:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->d(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 563
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/month/an;->a:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->a(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 564
    const v1, 0x7f0c0269

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 565
    iget-object v0, p0, Lcom/android/calendar/month/an;->a:Lcom/android/calendar/month/aj;

    invoke-static {v0}, Lcom/android/calendar/month/aj;->b(Lcom/android/calendar/month/aj;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040075

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 566
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Landroid/view/View;->setHoverPopupType(I)V

    .line 568
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    .line 569
    iget-object v2, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v1, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0, v3}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 570
    iget-object v0, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    const/16 v1, 0x3031

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 571
    iget-object v0, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 572
    iget-object v0, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 573
    iget-object v0, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    goto/16 :goto_0

    .line 608
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/android/calendar/month/an;->b:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->dismiss()V

    goto/16 :goto_0

    .line 560
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
