.class public Lcom/android/calendar/month/aj;
.super Lcom/android/calendar/by;
.source "MonthEventAdapter.java"


# static fields
.field public static e:I

.field public static f:Z

.field private static k:Z


# instance fields
.field public d:I

.field protected g:Ljava/util/ArrayList;

.field protected h:Z

.field protected i:I

.field private j:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/month/aj;->e:I

    .line 86
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/aj;->f:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 105
    invoke-direct {p0, p1}, Lcom/android/calendar/by;-><init>(Landroid/content/Context;)V

    .line 80
    iput-boolean v0, p0, Lcom/android/calendar/month/aj;->l:Z

    .line 90
    iput-boolean v1, p0, Lcom/android/calendar/month/aj;->h:Z

    .line 93
    iput-boolean v0, p0, Lcom/android/calendar/month/aj;->m:Z

    .line 94
    iput-boolean v0, p0, Lcom/android/calendar/month/aj;->n:Z

    .line 97
    iput-boolean v0, p0, Lcom/android/calendar/month/aj;->o:Z

    .line 98
    iput-boolean v0, p0, Lcom/android/calendar/month/aj;->p:Z

    .line 111
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/month/aj;->k:Z

    .line 112
    invoke-static {p1}, Lcom/android/calendar/hj;->t(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/aj;->l:Z

    .line 113
    sput-boolean v1, Lcom/android/calendar/month/aj;->f:Z

    .line 115
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/android/calendar/d/g;->i()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/aj;->h:Z

    .line 120
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 122
    const v1, 0x7f0c03f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/aj;->i:I

    .line 124
    iget-object v1, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/aj;->m:Z

    .line 125
    iget-object v1, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->v(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/aj;->n:Z

    .line 126
    invoke-static {}, Lcom/android/calendar/dz;->p()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/aj;->o:Z

    .line 127
    invoke-static {}, Lcom/android/calendar/dz;->x()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/aj;->p:Z

    .line 128
    const v1, 0x7f0c00bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/aj;->q:I

    .line 129
    const v1, 0x7f0c03fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/aj;->r:I

    .line 131
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 744
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 745
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 733
    if-nez p1, :cond_1

    .line 742
    :cond_0
    :goto_0
    return-void

    .line 736
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 737
    iget-boolean v0, p0, Lcom/android/calendar/month/aj;->m:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 738
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 739
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 740
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/month/aj;ZJ)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/month/aj;->a(ZJ)V

    return-void
.end method

.method private a(ZJ)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 536
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 537
    const-string v3, "complete"

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 538
    const-string v0, "date_completed"

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 539
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 540
    iget-object v3, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 541
    sput-boolean v1, Lcom/android/calendar/month/aj;->f:Z

    .line 542
    return-void

    :cond_0
    move v0, v1

    .line 537
    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/month/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 547
    iput p1, p0, Lcom/android/calendar/month/aj;->d:I

    .line 548
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 147
    invoke-static {p1}, Lcom/android/calendar/hj;->t(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/aj;->l:Z

    .line 148
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/android/calendar/month/aj;->g:Ljava/util/ArrayList;

    .line 135
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/calendar/month/aj;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 142
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/aj;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 22

    .prologue
    .line 152
    const/4 v5, 0x0

    .line 153
    const/4 v4, 0x0

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 157
    if-eqz p2, :cond_26

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_26

    .line 160
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 161
    instance-of v6, v3, Lcom/android/calendar/bz;

    if-eqz v6, :cond_26

    .line 163
    check-cast v3, Lcom/android/calendar/bz;

    move-object/from16 v4, p2

    .line 167
    :goto_0
    if-nez v3, :cond_0

    .line 170
    new-instance v3, Lcom/android/calendar/bz;

    invoke-direct {v3}, Lcom/android/calendar/bz;-><init>()V

    .line 172
    const v4, 0x7f040032

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 174
    const v2, 0x7f1200b7

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lcom/android/calendar/bz;->a:Landroid/view/View;

    .line 175
    const v2, 0x7f120039

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/bz;->b:Landroid/widget/ImageView;

    .line 176
    const v2, 0x7f1200b0

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lcom/android/calendar/bz;->c:Landroid/view/View;

    .line 177
    const v2, 0x7f1200b1

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/bz;->d:Landroid/widget/TextView;

    .line 178
    const v2, 0x7f1200b3

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/bz;->e:Landroid/widget/TextView;

    .line 179
    const v2, 0x7f1200b4

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/android/calendar/bz;->f:Landroid/widget/TextView;

    .line 180
    const v2, 0x7f1200b5

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lcom/android/calendar/bz;->g:Landroid/view/View;

    .line 181
    const v2, 0x7f1200b6

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/android/calendar/bz;->i:Landroid/widget/ImageView;

    .line 182
    const v2, 0x7f1200af

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v3, Lcom/android/calendar/bz;->h:Landroid/widget/CheckBox;

    .line 185
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    move-object v9, v4

    .line 188
    iget-object v4, v3, Lcom/android/calendar/bz;->a:Landroid/view/View;

    .line 189
    iget-object v5, v3, Lcom/android/calendar/bz;->b:Landroid/widget/ImageView;

    .line 190
    iget-object v6, v3, Lcom/android/calendar/bz;->c:Landroid/view/View;

    .line 191
    iget-object v13, v3, Lcom/android/calendar/bz;->d:Landroid/widget/TextView;

    .line 192
    iget-object v14, v3, Lcom/android/calendar/bz;->e:Landroid/widget/TextView;

    .line 193
    iget-object v15, v3, Lcom/android/calendar/bz;->f:Landroid/widget/TextView;

    .line 194
    iget-object v0, v3, Lcom/android/calendar/bz;->g:Landroid/view/View;

    move-object/from16 v16, v0

    .line 195
    iget-object v0, v3, Lcom/android/calendar/bz;->i:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    .line 196
    iget-object v7, v3, Lcom/android/calendar/bz;->h:Landroid/widget/CheckBox;

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/aj;->getCount()I

    move-result v2

    move/from16 v0, p1

    if-le v2, v0, :cond_1

    if-gez p1, :cond_2

    .line 518
    :cond_1
    :goto_1
    return-object v9

    .line 202
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/aj;->g:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/month/ba;

    .line 203
    iget v8, v2, Lcom/android/calendar/month/ba;->w:I

    const/4 v10, 0x2

    if-ne v8, v10, :cond_f

    .line 204
    const v8, 0x3ecccccd    # 0.4f

    invoke-virtual {v9, v8}, Landroid/view/View;->setAlpha(F)V

    .line 209
    :goto_2
    iget v8, v2, Lcom/android/calendar/month/ba;->e:I

    invoke-virtual {v4, v8}, Landroid/view/View;->setBackgroundColor(I)V

    .line 210
    iget-object v4, v2, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 212
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v8, 0x7f0c01f3

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    .line 213
    iget-object v8, v3, Lcom/android/calendar/bz;->a:Landroid/view/View;

    invoke-virtual {v8, v4}, Landroid/view/View;->setTranslationX(F)V

    .line 215
    :cond_3
    sget-boolean v4, Lcom/android/calendar/month/aj;->k:Z

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/month/aj;->n:Z

    if-eqz v4, :cond_4

    .line 216
    iget-object v4, v3, Lcom/android/calendar/bz;->a:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/view/View;->setHoverPopupType(I)V

    .line 217
    iget-object v4, v3, Lcom/android/calendar/bz;->a:Landroid/view/View;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 218
    iget-object v3, v3, Lcom/android/calendar/bz;->a:Landroid/view/View;

    new-instance v4, Lcom/android/calendar/month/al;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/android/calendar/month/al;-><init>(Lcom/android/calendar/month/aj;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 221
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 223
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/month/aj;->l:Z

    if-eqz v3, :cond_10

    .line 224
    const/4 v3, 0x0

    const v4, 0x7f0c01de

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v14, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 231
    :goto_3
    iget v3, v2, Lcom/android/calendar/month/ba;->c:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1b

    .line 232
    if-eqz v6, :cond_5

    .line 233
    const/4 v3, 0x0

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 236
    :cond_5
    if-eqz v5, :cond_7

    .line 237
    const/16 v3, 0x8

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 238
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/month/aj;->p:Z

    if-eqz v3, :cond_7

    .line 239
    const/4 v3, 0x0

    .line 240
    iget-wide v10, v2, Lcom/android/calendar/month/ba;->u:J

    const-wide/16 v20, 0x2

    cmp-long v4, v10, v20

    if-nez v4, :cond_11

    .line 241
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    iget-object v8, v2, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v8}, Lcom/android/calendar/eb;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 249
    :cond_6
    :goto_4
    if-eqz v3, :cond_7

    .line 250
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 252
    if-eqz v6, :cond_7

    .line 253
    const/16 v3, 0x8

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 258
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0026

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 259
    const/16 v3, 0x8

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 260
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 261
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 262
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 263
    invoke-static {v7}, Lcom/android/calendar/dz;->a(Landroid/widget/CompoundButton;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 264
    invoke-virtual {v14}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    and-int/lit8 v3, v3, -0x11

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 268
    :cond_8
    const-string v12, ""

    .line 269
    const-string v3, " - "

    .line 270
    const-string v11, ""

    .line 274
    iget-boolean v4, v2, Lcom/android/calendar/month/ba;->m:Z

    if-eqz v4, :cond_9

    iget-boolean v4, v2, Lcom/android/calendar/month/ba;->l:Z

    if-nez v4, :cond_9

    .line 275
    iget v4, v2, Lcom/android/calendar/month/ba;->j:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/aj;->d:I

    if-ne v4, v5, :cond_12

    .line 276
    const/4 v4, 0x0

    iput-boolean v4, v2, Lcom/android/calendar/month/ba;->l:Z

    .line 277
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/calendar/month/aj;->j:Z

    .line 286
    :cond_9
    :goto_5
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v4

    if-eqz v4, :cond_25

    .line 287
    const-string v3, " -   "

    move-object v10, v3

    .line 290
    :goto_6
    const-string v3, ""

    .line 292
    iget-boolean v3, v2, Lcom/android/calendar/month/ba;->l:Z

    if-eqz v3, :cond_14

    .line 293
    const v3, 0x7f0f0045

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v4, v11

    move-object v5, v3

    .line 332
    :goto_7
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 334
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 336
    if-eqz v5, :cond_a

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_a

    .line 337
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 340
    :cond_a
    iget-boolean v5, v2, Lcom/android/calendar/month/ba;->l:Z

    if-nez v5, :cond_c

    .line 342
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-lez v5, :cond_b

    .line 343
    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 346
    :cond_b
    invoke-virtual {v3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 348
    if-eqz v4, :cond_c

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_c

    .line 349
    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 352
    :cond_c
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    .line 358
    iget-object v3, v2, Lcom/android/calendar/month/ba;->g:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 359
    const/16 v3, 0x8

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 360
    const-string v3, ""

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    :goto_8
    const/4 v3, 0x0

    .line 371
    iget-boolean v4, v2, Lcom/android/calendar/month/ba;->s:Z

    if-eqz v4, :cond_d

    .line 372
    iget-wide v4, v2, Lcom/android/calendar/month/ba;->t:J

    .line 373
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-static {v3, v4, v5}, Lcom/android/calendar/gx;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 374
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 376
    :cond_d
    if-eqz v17, :cond_e

    .line 378
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/aj;->q:I

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 379
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/aj;->q:I

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 380
    if-eqz v3, :cond_19

    .line 381
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 382
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 383
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 390
    :cond_e
    :goto_9
    const v3, 0x7f0200b6

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 392
    iget-boolean v2, v2, Lcom/android/calendar/month/ba;->n:Z

    if-eqz v2, :cond_1a

    .line 393
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/aj;->a(Landroid/view/View;I)V

    .line 512
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/aj;->n:Z

    if-eqz v2, :cond_1

    .line 513
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/calendar/month/aj;->a(Landroid/view/View;)V

    .line 514
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/calendar/month/aj;->a(Landroid/view/View;)V

    .line 515
    new-instance v2, Lcom/android/calendar/month/an;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/calendar/month/an;-><init>(Lcom/android/calendar/month/aj;Lcom/android/calendar/month/ak;)V

    invoke-virtual {v9, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_1

    .line 206
    :cond_f
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v9, v8}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_2

    .line 227
    :cond_10
    const/4 v3, 0x0

    const v4, 0x7f0c03fe

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v14, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_3

    .line 244
    :cond_11
    iget-boolean v4, v2, Lcom/android/calendar/month/ba;->v:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/month/aj;->o:Z

    if-nez v4, :cond_6

    .line 245
    invoke-static {}, Lcom/android/calendar/eb;->a()Lcom/android/calendar/eb;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    iget-wide v10, v2, Lcom/android/calendar/month/ba;->f:J

    invoke-virtual {v3, v4, v10, v11}, Lcom/android/calendar/eb;->a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto/16 :goto_4

    .line 278
    :cond_12
    iget v4, v2, Lcom/android/calendar/month/ba;->k:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/month/aj;->d:I

    if-ne v4, v5, :cond_13

    .line 279
    const/4 v4, 0x0

    iput-boolean v4, v2, Lcom/android/calendar/month/ba;->l:Z

    .line 280
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/calendar/month/aj;->j:Z

    goto/16 :goto_5

    .line 282
    :cond_13
    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/android/calendar/month/ba;->l:Z

    goto/16 :goto_5

    .line 297
    :cond_14
    const/16 v8, 0xb01

    .line 298
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 299
    const/16 v8, 0xb81

    .line 302
    :cond_15
    iget-wide v4, v2, Lcom/android/calendar/month/ba;->h:J

    .line 303
    iget-wide v0, v2, Lcom/android/calendar/month/ba;->i:J

    move-wide/from16 v20, v0

    .line 310
    iget-boolean v3, v2, Lcom/android/calendar/month/ba;->m:Z

    if-nez v3, :cond_16

    .line 311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    move-wide v6, v4

    invoke-static/range {v3 .. v8}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v11

    .line 312
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    move-wide/from16 v4, v20

    move-wide/from16 v6, v20

    invoke-static/range {v3 .. v8}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v4

    .line 314
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    const v5, 0x7f0f0036

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v11, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v5, v11

    goto/16 :goto_7

    .line 319
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/month/aj;->j:Z

    if-eqz v3, :cond_17

    .line 320
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    move-wide v6, v4

    invoke-static/range {v3 .. v8}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v3

    move-object v4, v11

    move-object v5, v3

    .line 322
    goto/16 :goto_7

    .line 325
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    move-wide/from16 v4, v20

    move-wide/from16 v6, v20

    invoke-static/range {v3 .. v8}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move-object v5, v12

    .line 327
    goto/16 :goto_7

    .line 362
    :cond_18
    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 363
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 364
    iget-object v3, v2, Lcom/android/calendar/month/ba;->g:Ljava/lang/String;

    .line 365
    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 385
    :cond_19
    const/16 v3, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9

    .line 395
    :cond_1a
    const/16 v2, 0x8

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/aj;->a(Landroid/view/View;I)V

    goto/16 :goto_a

    .line 400
    :cond_1b
    if-eqz v5, :cond_1c

    .line 401
    const/16 v3, 0x8

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 404
    :cond_1c
    const/16 v3, 0x8

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 406
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/aj;->i:I

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 408
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 410
    const/16 v3, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 414
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/month/aj;->h:Z

    if-nez v3, :cond_20

    .line 415
    const/16 v3, 0x8

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 427
    :goto_b
    new-instance v3, Landroid/content/Intent;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Checked"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v10, v2, Lcom/android/calendar/month/ba;->f:J

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 428
    const-string v4, "Checked"

    iget-boolean v5, v2, Lcom/android/calendar/month/ba;->A:Z

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 429
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 430
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 431
    iget-boolean v3, v2, Lcom/android/calendar/month/ba;->A:Z

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 432
    new-instance v3, Lcom/android/calendar/month/am;

    iget-wide v4, v2, Lcom/android/calendar/month/ba;->f:J

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/android/calendar/month/am;-><init>(Lcom/android/calendar/month/aj;JLcom/android/calendar/month/ak;)V

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 434
    iget-boolean v3, v2, Lcom/android/calendar/month/ba;->E:Z

    if-nez v3, :cond_22

    .line 435
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 440
    :goto_c
    iget-boolean v3, v2, Lcom/android/calendar/month/ba;->A:Z

    if-eqz v3, :cond_23

    .line 441
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 442
    const v3, 0x7f0b001b

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 443
    const v3, 0x7f0b000c

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 444
    invoke-static {v7}, Lcom/android/calendar/dz;->a(Landroid/widget/CompoundButton;)Z

    move-result v3

    if-nez v3, :cond_1d

    .line 445
    invoke-virtual {v14}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    or-int/lit8 v3, v3, 0x10

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 456
    :cond_1d
    :goto_d
    if-eqz v17, :cond_1e

    .line 458
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/aj;->r:I

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 459
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/aj;->r:I

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 463
    :cond_1e
    iget v3, v2, Lcom/android/calendar/month/ba;->z:I

    packed-switch v3, :pswitch_data_0

    .line 491
    :cond_1f
    :goto_e
    const v3, 0x7f0200b6

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 493
    iget v2, v2, Lcom/android/calendar/month/ba;->B:I

    packed-switch v2, :pswitch_data_1

    .line 503
    if-eqz v13, :cond_24

    .line 504
    const/16 v2, 0x8

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/aj;->a(Landroid/view/View;I)V

    goto/16 :goto_a

    .line 417
    :cond_20
    iget-object v3, v2, Lcom/android/calendar/month/ba;->D:Ljava/lang/String;

    .line 418
    if-eqz v3, :cond_21

    const-string v4, "Default"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 419
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f009c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 421
    :cond_21
    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_b

    .line 437
    :cond_22
    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    goto/16 :goto_c

    .line 448
    :cond_23
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 449
    const v3, 0x7f0b0026

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 450
    const v3, 0x7f0b0025

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 451
    invoke-static {v7}, Lcom/android/calendar/dz;->a(Landroid/widget/CompoundButton;)Z

    move-result v3

    if-nez v3, :cond_1d

    .line 452
    invoke-virtual {v14}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    and-int/lit8 v3, v3, -0x11

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto/16 :goto_d

    .line 465
    :pswitch_0
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 466
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 467
    const v3, 0x7f02013c

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 468
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/month/aj;->m:Z

    if-eqz v3, :cond_1f

    .line 469
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0347

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 470
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 471
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/16 v4, 0x1f4

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    goto/16 :goto_e

    .line 476
    :pswitch_1
    const/16 v3, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_e

    .line 479
    :pswitch_2
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 480
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 481
    const v3, 0x7f02013b

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 482
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/month/aj;->m:Z

    if-eqz v3, :cond_1f

    .line 483
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/aj;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0346

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 484
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 485
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/16 v4, 0x1f4

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    goto/16 :goto_e

    .line 495
    :pswitch_3
    const/16 v2, 0x8

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/aj;->a(Landroid/view/View;I)V

    goto/16 :goto_a

    .line 500
    :pswitch_4
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/aj;->a(Landroid/view/View;I)V

    goto/16 :goto_a

    .line 506
    :cond_24
    const/4 v2, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/aj;->a(Landroid/view/View;I)V

    goto/16 :goto_a

    :cond_25
    move-object v10, v3

    goto/16 :goto_6

    :cond_26
    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_0

    .line 463
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 493
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
