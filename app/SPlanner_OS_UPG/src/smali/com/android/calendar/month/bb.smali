.class public Lcom/android/calendar/month/bb;
.super Landroid/app/DialogFragment;
.source "MonthHoverEventFragment.java"


# static fields
.field private static C:Z

.field public static final a:Ljava/lang/String;

.field private static l:I

.field private static m:I


# instance fields
.field private A:I

.field private B:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:F

.field private O:F

.field private P:I

.field private Q:J

.field private R:Lcom/android/calendar/month/bm;

.field private S:Landroid/os/Handler;

.field private T:Ljava/lang/Runnable;

.field protected b:I

.field protected c:Z

.field private d:Lcom/android/calendar/month/bh;

.field private final e:Lcom/android/calendar/month/bg;

.field private f:Landroid/view/View;

.field private g:Landroid/content/Context;

.field private h:Lcom/android/calendar/al;

.field private i:Landroid/widget/ListView;

.field private j:Ljava/util/ArrayList;

.field private k:Ljava/util/ArrayList;

.field private n:I

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/ImageView;

.field private q:Landroid/widget/ImageView;

.field private r:Landroid/widget/ImageView;

.field private s:Z

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/android/calendar/month/bb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/month/bb;->a:Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/month/bb;->C:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 176
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 72
    new-instance v0, Lcom/android/calendar/month/bg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/bg;-><init>(Lcom/android/calendar/month/bb;Lcom/android/calendar/month/bc;)V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->e:Lcom/android/calendar/month/bg;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->j:Ljava/util/ArrayList;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->k:Ljava/util/ArrayList;

    .line 108
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/bb;->b:I

    .line 110
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->s:Z

    .line 112
    iput v2, p0, Lcom/android/calendar/month/bb;->t:I

    .line 114
    iput v3, p0, Lcom/android/calendar/month/bb;->u:I

    .line 116
    iput v3, p0, Lcom/android/calendar/month/bb;->v:I

    .line 118
    iput v3, p0, Lcom/android/calendar/month/bb;->w:I

    .line 120
    iput v3, p0, Lcom/android/calendar/month/bb;->x:I

    .line 122
    iput v2, p0, Lcom/android/calendar/month/bb;->y:I

    .line 124
    iput v2, p0, Lcom/android/calendar/month/bb;->z:I

    .line 126
    iput v2, p0, Lcom/android/calendar/month/bb;->A:I

    .line 132
    iput v2, p0, Lcom/android/calendar/month/bb;->D:I

    .line 136
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/calendar/month/bb;->F:I

    .line 139
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->c:Z

    .line 145
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->I:Z

    .line 147
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->J:Z

    .line 149
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->K:Z

    .line 151
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->L:Z

    .line 153
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->M:Z

    .line 155
    iput v4, p0, Lcom/android/calendar/month/bb;->N:F

    .line 156
    iput v4, p0, Lcom/android/calendar/month/bb;->O:F

    .line 158
    iput v2, p0, Lcom/android/calendar/month/bb;->P:I

    .line 160
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/month/bb;->Q:J

    .line 168
    sget-object v0, Lcom/android/calendar/month/bm;->a:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    .line 174
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    .line 315
    new-instance v0, Lcom/android/calendar/month/bc;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/bc;-><init>(Lcom/android/calendar/month/bb;)V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->T:Ljava/lang/Runnable;

    .line 177
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 179
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 72
    new-instance v0, Lcom/android/calendar/month/bg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/bg;-><init>(Lcom/android/calendar/month/bb;Lcom/android/calendar/month/bc;)V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->e:Lcom/android/calendar/month/bg;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->j:Ljava/util/ArrayList;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->k:Ljava/util/ArrayList;

    .line 108
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/bb;->b:I

    .line 110
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->s:Z

    .line 112
    iput v2, p0, Lcom/android/calendar/month/bb;->t:I

    .line 114
    iput v3, p0, Lcom/android/calendar/month/bb;->u:I

    .line 116
    iput v3, p0, Lcom/android/calendar/month/bb;->v:I

    .line 118
    iput v3, p0, Lcom/android/calendar/month/bb;->w:I

    .line 120
    iput v3, p0, Lcom/android/calendar/month/bb;->x:I

    .line 122
    iput v2, p0, Lcom/android/calendar/month/bb;->y:I

    .line 124
    iput v2, p0, Lcom/android/calendar/month/bb;->z:I

    .line 126
    iput v2, p0, Lcom/android/calendar/month/bb;->A:I

    .line 132
    iput v2, p0, Lcom/android/calendar/month/bb;->D:I

    .line 136
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/calendar/month/bb;->F:I

    .line 139
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->c:Z

    .line 145
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->I:Z

    .line 147
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->J:Z

    .line 149
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->K:Z

    .line 151
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->L:Z

    .line 153
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->M:Z

    .line 155
    iput v4, p0, Lcom/android/calendar/month/bb;->N:F

    .line 156
    iput v4, p0, Lcom/android/calendar/month/bb;->O:F

    .line 158
    iput v2, p0, Lcom/android/calendar/month/bb;->P:I

    .line 160
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/month/bb;->Q:J

    .line 168
    sget-object v0, Lcom/android/calendar/month/bm;->a:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    .line 174
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    .line 315
    new-instance v0, Lcom/android/calendar/month/bc;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/bc;-><init>(Lcom/android/calendar/month/bb;)V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->T:Ljava/lang/Runnable;

    .line 180
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 184
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 72
    new-instance v0, Lcom/android/calendar/month/bg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/bg;-><init>(Lcom/android/calendar/month/bb;Lcom/android/calendar/month/bc;)V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->e:Lcom/android/calendar/month/bg;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->j:Ljava/util/ArrayList;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->k:Ljava/util/ArrayList;

    .line 108
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/bb;->b:I

    .line 110
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->s:Z

    .line 112
    iput v2, p0, Lcom/android/calendar/month/bb;->t:I

    .line 114
    iput v3, p0, Lcom/android/calendar/month/bb;->u:I

    .line 116
    iput v3, p0, Lcom/android/calendar/month/bb;->v:I

    .line 118
    iput v3, p0, Lcom/android/calendar/month/bb;->w:I

    .line 120
    iput v3, p0, Lcom/android/calendar/month/bb;->x:I

    .line 122
    iput v2, p0, Lcom/android/calendar/month/bb;->y:I

    .line 124
    iput v2, p0, Lcom/android/calendar/month/bb;->z:I

    .line 126
    iput v2, p0, Lcom/android/calendar/month/bb;->A:I

    .line 132
    iput v2, p0, Lcom/android/calendar/month/bb;->D:I

    .line 136
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/calendar/month/bb;->F:I

    .line 139
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->c:Z

    .line 145
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->I:Z

    .line 147
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->J:Z

    .line 149
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->K:Z

    .line 151
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->L:Z

    .line 153
    iput-boolean v2, p0, Lcom/android/calendar/month/bb;->M:Z

    .line 155
    iput v4, p0, Lcom/android/calendar/month/bb;->N:F

    .line 156
    iput v4, p0, Lcom/android/calendar/month/bb;->O:F

    .line 158
    iput v2, p0, Lcom/android/calendar/month/bb;->P:I

    .line 160
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/month/bb;->Q:J

    .line 168
    sget-object v0, Lcom/android/calendar/month/bm;->a:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    .line 174
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    .line 315
    new-instance v0, Lcom/android/calendar/month/bc;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/bc;-><init>(Lcom/android/calendar/month/bb;)V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->T:Ljava/lang/Runnable;

    .line 185
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 187
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/hc;

    .line 188
    invoke-static {v0}, Lcom/android/calendar/task/aa;->a(Lcom/android/calendar/hc;)Lcom/android/calendar/task/aa;

    move-result-object v0

    .line 189
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    :cond_0
    invoke-direct {p0, p1, p2, v1}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 193
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lcom/android/calendar/month/bb;->m:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/month/bb;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/month/bb;->T:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 196
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 197
    :cond_0
    iput-object p2, p0, Lcom/android/calendar/month/bb;->j:Ljava/util/ArrayList;

    .line 198
    iput-object p3, p0, Lcom/android/calendar/month/bb;->k:Ljava/util/ArrayList;

    .line 200
    :cond_1
    iput-object p1, p0, Lcom/android/calendar/month/bb;->g:Landroid/content/Context;

    .line 201
    iget-object v0, p0, Lcom/android/calendar/month/bb;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bb;->h:Lcom/android/calendar/al;

    .line 202
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 204
    invoke-static {p1}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/bb;->M:Z

    .line 206
    const v1, 0x7f0b00a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/bb;->n:I

    .line 207
    invoke-static {p1}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/bb;->L:Z

    .line 208
    const v1, 0x7f0c0261

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/calendar/month/bb;->l:I

    .line 209
    const v1, 0x7f0c0260

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/calendar/month/bb;->m:I

    .line 211
    const v1, 0x7f0c025f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/bb;->G:I

    .line 213
    const-string v1, "status_bar_height"

    invoke-static {v1}, Lcom/android/calendar/common/b/a;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/bb;->B:I

    .line 215
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/android/calendar/month/bb;->b:I

    .line 217
    const v1, 0x7f0c0417

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/bb;->D:I

    .line 221
    const v1, 0x7f0c01d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/bb;->z:I

    .line 222
    const v1, 0x7f0c01d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/bb;->A:I

    .line 223
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/month/bb;->E:I

    .line 224
    invoke-static {p1}, Lcom/android/calendar/hj;->e(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/bb;->c:Z

    .line 225
    const v1, 0x7f0c041b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/bb;->P:I

    .line 226
    iget v0, p0, Lcom/android/calendar/month/bb;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/bb;->s:Z

    .line 228
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/month/bb;->F:I

    .line 230
    :cond_2
    return-void
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 67
    sput-boolean p0, Lcom/android/calendar/month/bb;->C:Z

    return p0
.end method

.method static synthetic b(Lcom/android/calendar/month/bb;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 67
    sget-boolean v0, Lcom/android/calendar/month/bb;->C:Z

    return v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/android/calendar/month/bb;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_5

    .line 261
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 262
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 263
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/view/Window;->clearFlags(I)V

    .line 265
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->K:Z

    if-eqz v0, :cond_0

    .line 267
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    .line 268
    const/16 v0, 0x10

    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    .line 271
    :cond_0
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 273
    const v0, 0x7f100045

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 274
    const v0, 0x800033

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 275
    const v0, 0x3ecccccd    # 0.4f

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 277
    sget v0, Lcom/android/calendar/month/bb;->l:I

    iget v3, p0, Lcom/android/calendar/month/bb;->z:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 279
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->J:Z

    if-nez v0, :cond_6

    .line 280
    sget v0, Lcom/android/calendar/month/bb;->m:I

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 285
    :goto_0
    iget v0, p0, Lcom/android/calendar/month/bb;->u:I

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 287
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->I:Z

    if-eqz v0, :cond_7

    .line 288
    iget-object v0, p0, Lcom/android/calendar/month/bb;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 289
    iget v3, p0, Lcom/android/calendar/month/bb;->v:I

    int-to-float v3, v3

    const/high16 v4, 0x41200000    # 10.0f

    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 298
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->H:Z

    if-eqz v0, :cond_2

    .line 299
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/lit8 v0, v0, 0x1e

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 302
    :cond_2
    invoke-virtual {v1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 304
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->J:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/month/bb;->h:Lcom/android/calendar/al;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/month/bb;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 305
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bb;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 308
    :cond_4
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->K:Z

    if-nez v0, :cond_5

    .line 309
    iget-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bb;->e:Lcom/android/calendar/month/bg;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 313
    :cond_5
    return-void

    .line 282
    :cond_6
    const/4 v0, -0x2

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_0

    .line 291
    :cond_7
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    sget v3, Lcom/android/calendar/month/bb;->m:I

    sub-int/2addr v0, v3

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 292
    iget v0, p0, Lcom/android/calendar/month/bb;->x:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->s:Z

    if-eqz v0, :cond_1

    .line 293
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    if-gez v0, :cond_8

    const/4 v0, 0x0

    :goto_2
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 294
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    sget v3, Lcom/android/calendar/month/bb;->m:I

    div-int/lit8 v3, v3, 0x4

    add-int/2addr v0, v3

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_1

    .line 293
    :cond_8
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_2
.end method

.method static synthetic c(Lcom/android/calendar/month/bb;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->I:Z

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/month/bb;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/month/bb;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/month/bb;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/android/calendar/month/bb;->w:I

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/month/bb;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/android/calendar/month/bb;->u:I

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/month/bb;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    return v0
.end method

.method static synthetic h(Lcom/android/calendar/month/bb;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/month/bb;->h:Lcom/android/calendar/al;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/month/bb;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/android/calendar/month/bb;->x:I

    return v0
.end method

.method static synthetic j(Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bg;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/month/bb;->e:Lcom/android/calendar/month/bg;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/month/bb;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->L:Z

    return v0
.end method

.method static synthetic l(Lcom/android/calendar/month/bb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/month/bb;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->K:Z

    return v0
.end method

.method static synthetic n(Lcom/android/calendar/month/bb;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/android/calendar/month/bb;->F:I

    return v0
.end method

.method static synthetic o(Lcom/android/calendar/month/bb;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->H:Z

    return v0
.end method

.method static synthetic p(Lcom/android/calendar/month/bb;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->M:Z

    return v0
.end method

.method static synthetic q(Lcom/android/calendar/month/bb;)J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/android/calendar/month/bb;->Q:J

    return-wide v0
.end method

.method static synthetic r(Lcom/android/calendar/month/bb;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/android/calendar/month/bb;->E:I

    return v0
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 380
    iput p1, p0, Lcom/android/calendar/month/bb;->u:I

    .line 381
    iget v0, p0, Lcom/android/calendar/month/bb;->B:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/android/calendar/month/bb;->v:I

    .line 382
    return-void
.end method

.method public a(Landroid/content/Context;IIIIZ)V
    .locals 2

    .prologue
    .line 390
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/calendar/month/bb;->N:F

    .line 391
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/calendar/month/bb;->O:F

    .line 393
    iget v0, p0, Lcom/android/calendar/month/bb;->E:I

    iget v1, p0, Lcom/android/calendar/month/bb;->F:I

    if-le v0, v1, :cond_2

    .line 394
    iget v0, p0, Lcom/android/calendar/month/bb;->D:I

    iget v1, p0, Lcom/android/calendar/month/bb;->F:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->F:I

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->z:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sput v0, Lcom/android/calendar/month/bb;->m:I

    .line 395
    if-eqz p6, :cond_0

    .line 396
    sget v0, Lcom/android/calendar/month/bb;->m:I

    iget v1, p0, Lcom/android/calendar/month/bb;->G:I

    add-int/2addr v0, v1

    sput v0, Lcom/android/calendar/month/bb;->m:I

    .line 402
    :cond_0
    :goto_0
    iput p5, p0, Lcom/android/calendar/month/bb;->y:I

    .line 404
    iput p4, p0, Lcom/android/calendar/month/bb;->x:I

    .line 405
    if-eqz p6, :cond_9

    .line 406
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->s:Z

    if-nez v0, :cond_6

    .line 407
    iput p2, p0, Lcom/android/calendar/month/bb;->u:I

    .line 408
    iget v0, p0, Lcom/android/calendar/month/bb;->B:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/android/calendar/month/bb;->v:I

    .line 410
    iget v0, p0, Lcom/android/calendar/month/bb;->x:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_3

    .line 411
    sget-object v0, Lcom/android/calendar/month/bm;->d:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    .line 421
    :goto_1
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    sget v1, Lcom/android/calendar/month/bb;->m:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->A:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/month/bb;->N:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 422
    sget-object v0, Lcom/android/calendar/month/bm;->b:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    .line 447
    :cond_1
    :goto_2
    return-void

    .line 399
    :cond_2
    iget v0, p0, Lcom/android/calendar/month/bb;->D:I

    iget v1, p0, Lcom/android/calendar/month/bb;->E:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->E:I

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->z:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sput v0, Lcom/android/calendar/month/bb;->m:I

    goto :goto_0

    .line 412
    :cond_3
    iget v0, p0, Lcom/android/calendar/month/bb;->x:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_4

    .line 413
    sget-object v0, Lcom/android/calendar/month/bm;->e:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    goto :goto_1

    .line 414
    :cond_4
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    sget v1, Lcom/android/calendar/month/bb;->m:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->A:I

    sub-int/2addr v0, v1

    if-gtz v0, :cond_5

    .line 415
    sget-object v0, Lcom/android/calendar/month/bm;->a:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    goto :goto_1

    .line 417
    :cond_5
    sget-object v0, Lcom/android/calendar/month/bm;->b:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    goto :goto_1

    .line 426
    :cond_6
    iput p2, p0, Lcom/android/calendar/month/bb;->u:I

    .line 427
    iget v0, p0, Lcom/android/calendar/month/bb;->B:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/android/calendar/month/bb;->v:I

    .line 429
    iget v0, p0, Lcom/android/calendar/month/bb;->x:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_8

    .line 430
    sget-object v0, Lcom/android/calendar/month/bm;->d:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    .line 435
    :cond_7
    :goto_3
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    sget v1, Lcom/android/calendar/month/bb;->m:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->A:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/month/bb;->N:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 436
    sget-object v0, Lcom/android/calendar/month/bm;->b:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    goto :goto_2

    .line 431
    :cond_8
    iget v0, p0, Lcom/android/calendar/month/bb;->x:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_7

    .line 432
    sget-object v0, Lcom/android/calendar/month/bm;->e:Lcom/android/calendar/month/bm;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    goto :goto_3

    .line 442
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/month/bb;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-nez v0, :cond_a

    .line 443
    iget v0, p0, Lcom/android/calendar/month/bb;->P:I

    sub-int/2addr p2, v0

    .line 445
    :cond_a
    invoke-virtual {p0, p2, p3}, Lcom/android/calendar/month/bb;->a(II)V

    goto :goto_2
.end method

.method public a(Landroid/content/Context;IIIZ)V
    .locals 7

    .prologue
    .line 385
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/month/bb;->a(Landroid/content/Context;IIIIZ)V

    .line 386
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 255
    invoke-direct {p0}, Lcom/android/calendar/month/bb;->c()V

    .line 256
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 235
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/bb;->setStyle(II)V

    .line 236
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/bb;->setStyle(II)V

    .line 238
    invoke-virtual {p0}, Lcom/android/calendar/month/bb;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_0

    const-string v1, "from_week"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    const-string v1, "from_week"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/bb;->J:Z

    .line 243
    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "finger_hover"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 244
    const-string v1, "finger_hover"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/bb;->K:Z

    .line 247
    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "hover_day"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 248
    const-string v1, "hover_day"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/month/bb;->Q:J

    .line 250
    :cond_2
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 457
    const v0, 0x7f040065

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    .line 459
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/bb;->g:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 460
    :cond_0
    const/4 v0, 0x0

    .line 621
    :goto_0
    return-object v0

    .line 463
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/bb;->g:Landroid/content/Context;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/bb;->H:Z

    .line 465
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    const v1, 0x7f1201ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->o:Landroid/widget/ImageView;

    .line 466
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    const v1, 0x7f1201ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->p:Landroid/widget/ImageView;

    .line 467
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    const v1, 0x7f1201f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->q:Landroid/widget/ImageView;

    .line 468
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    const v1, 0x7f1201fc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    .line 470
    iget v0, p0, Lcom/android/calendar/month/bb;->E:I

    iget v1, p0, Lcom/android/calendar/month/bb;->F:I

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_5

    .line 471
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    const v1, 0x7f020110

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 475
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->K:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/calendar/month/bb;->E:I

    iget v1, p0, Lcom/android/calendar/month/bb;->F:I

    add-int/lit8 v1, v1, 0x1

    if-lt v0, v1, :cond_3

    .line 476
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    const v1, 0x7f1201fa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 477
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 479
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    const v1, 0x7f1201fb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 480
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 482
    iget-object v1, p0, Lcom/android/calendar/month/bb;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0058

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/calendar/month/bb;->E:I

    iget v4, p0, Lcom/android/calendar/month/bb;->F:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 484
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 487
    :cond_3
    sget-object v0, Lcom/android/calendar/month/bf;->a:[I

    iget-object v1, p0, Lcom/android/calendar/month/bb;->R:Lcom/android/calendar/month/bm;

    invoke-virtual {v1}, Lcom/android/calendar/month/bm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 570
    :goto_2
    new-instance v0, Lcom/android/calendar/month/bh;

    iget-object v1, p0, Lcom/android/calendar/month/bb;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/calendar/month/bb;->j:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/month/bb;->k:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/calendar/month/bh;-><init>(Lcom/android/calendar/month/bb;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/calendar/month/bb;->d:Lcom/android/calendar/month/bh;

    .line 572
    iget-object v0, p0, Lcom/android/calendar/month/bb;->d:Lcom/android/calendar/month/bh;

    if-eqz v0, :cond_4

    .line 573
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    const v1, 0x7f1201f9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    .line 574
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    if-eqz v0, :cond_4

    .line 575
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/android/calendar/month/bb;->n:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 576
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->H:Z

    if-eqz v0, :cond_c

    .line 577
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 581
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/month/bb;->d:Lcom/android/calendar/month/bh;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 582
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 583
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 584
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 585
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->K:Z

    if-eqz v0, :cond_d

    .line 587
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setHoverScrollMode(Z)V

    .line 617
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/month/bb;->d:Lcom/android/calendar/month/bh;

    invoke-virtual {v0}, Lcom/android/calendar/month/bh;->notifyDataSetChanged()V

    .line 621
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/bb;->f:Landroid/view/View;

    goto/16 :goto_0

    .line 472
    :cond_5
    iget v0, p0, Lcom/android/calendar/month/bb;->E:I

    iget v1, p0, Lcom/android/calendar/month/bb;->F:I

    add-int/lit8 v1, v1, 0x1

    if-lt v0, v1, :cond_2

    .line 473
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    const v1, 0x7f020111

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 490
    :pswitch_0
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->K:Z

    if-eqz v0, :cond_6

    .line 491
    iget v0, p0, Lcom/android/calendar/month/bb;->u:I

    sget v1, Lcom/android/calendar/month/bb;->l:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/calendar/month/bb;->z:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/bb;->y:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->u:I

    .line 493
    :cond_6
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    sget v1, Lcom/android/calendar/month/bb;->m:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/calendar/month/bb;->z:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->v:I

    .line 495
    iget-object v0, p0, Lcom/android/calendar/month/bb;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 496
    iget-object v0, p0, Lcom/android/calendar/month/bb;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 497
    iget-object v0, p0, Lcom/android/calendar/month/bb;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 498
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 503
    :pswitch_1
    iget v0, p0, Lcom/android/calendar/month/bb;->u:I

    iget v1, p0, Lcom/android/calendar/month/bb;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->u:I

    .line 504
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    sget v1, Lcom/android/calendar/month/bb;->m:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/calendar/month/bb;->z:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->v:I

    .line 505
    iget-object v0, p0, Lcom/android/calendar/month/bb;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 506
    iget-object v0, p0, Lcom/android/calendar/month/bb;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-eq v0, v9, :cond_7

    iget-object v0, p0, Lcom/android/calendar/month/bb;->h:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-eq v0, v8, :cond_7

    .line 507
    iget-object v0, p0, Lcom/android/calendar/month/bb;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 511
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/month/bb;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 509
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/month/bb;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 516
    :pswitch_2
    iget v0, p0, Lcom/android/calendar/month/bb;->u:I

    sget v1, Lcom/android/calendar/month/bb;->l:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->z:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->u:I

    .line 517
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    sget v1, Lcom/android/calendar/month/bb;->m:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/calendar/month/bb;->z:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->v:I

    .line 519
    iget-object v0, p0, Lcom/android/calendar/month/bb;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lcom/android/calendar/month/bb;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lcom/android/calendar/month/bb;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 522
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 526
    :pswitch_3
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    sget v1, Lcom/android/calendar/month/bb;->m:I

    iget v2, p0, Lcom/android/calendar/month/bb;->z:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->v:I

    .line 527
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->s:Z

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/android/calendar/month/bb;->x:I

    if-nez v0, :cond_9

    .line 528
    iget-object v0, p0, Lcom/android/calendar/month/bb;->q:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 529
    iput v9, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 530
    iget v1, p0, Lcom/android/calendar/month/bb;->z:I

    iput v1, p0, Lcom/android/calendar/month/bb;->t:I

    .line 531
    iget v1, p0, Lcom/android/calendar/month/bb;->t:I

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 539
    :cond_8
    :goto_6
    iget-object v0, p0, Lcom/android/calendar/month/bb;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 540
    iget-object v0, p0, Lcom/android/calendar/month/bb;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 541
    iget-object v0, p0, Lcom/android/calendar/month/bb;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 542
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 532
    :cond_9
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->s:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/android/calendar/month/bb;->x:I

    if-ne v0, v7, :cond_8

    .line 533
    iget-object v0, p0, Lcom/android/calendar/month/bb;->q:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 534
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 535
    iget v1, p0, Lcom/android/calendar/month/bb;->z:I

    neg-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/bb;->t:I

    .line 536
    iget v1, p0, Lcom/android/calendar/month/bb;->t:I

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_6

    .line 546
    :pswitch_4
    iget v0, p0, Lcom/android/calendar/month/bb;->u:I

    sget v1, Lcom/android/calendar/month/bb;->l:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->z:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->y:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->u:I

    .line 547
    iget v0, p0, Lcom/android/calendar/month/bb;->v:I

    iget v1, p0, Lcom/android/calendar/month/bb;->A:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/bb;->v:I

    .line 550
    iget v0, p0, Lcom/android/calendar/month/bb;->u:I

    if-gez v0, :cond_b

    .line 551
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 552
    iget v1, p0, Lcom/android/calendar/month/bb;->u:I

    iput v1, p0, Lcom/android/calendar/month/bb;->t:I

    .line 553
    iget v1, p0, Lcom/android/calendar/month/bb;->t:I

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 561
    :cond_a
    :goto_7
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 562
    iget-object v0, p0, Lcom/android/calendar/month/bb;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 563
    iget-object v0, p0, Lcom/android/calendar/month/bb;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 564
    iget-object v0, p0, Lcom/android/calendar/month/bb;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 555
    :cond_b
    iget v0, p0, Lcom/android/calendar/month/bb;->u:I

    sget v1, Lcom/android/calendar/month/bb;->l:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/bb;->z:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/month/bb;->O:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_a

    .line 556
    iget-object v0, p0, Lcom/android/calendar/month/bb;->r:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 557
    iget v1, p0, Lcom/android/calendar/month/bb;->O:F

    float-to-int v1, v1

    iget v2, p0, Lcom/android/calendar/month/bb;->u:I

    sget v3, Lcom/android/calendar/month/bb;->l:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/month/bb;->z:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/month/bb;->t:I

    .line 558
    iget v1, p0, Lcom/android/calendar/month/bb;->t:I

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_7

    .line 579
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setDividerHeight(I)V

    goto/16 :goto_3

    .line 591
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setHoverScrollMode(Z)V

    .line 592
    iget-object v0, p0, Lcom/android/calendar/month/bb;->i:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/month/bd;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/bd;-><init>(Lcom/android/calendar/month/bb;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto/16 :goto_4

    .line 487
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 665
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 666
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->K:Z

    if-nez v0, :cond_0

    .line 667
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 669
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 641
    iget-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bb;->T:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 642
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 643
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 626
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 627
    iget-boolean v0, p0, Lcom/android/calendar/month/bb;->K:Z

    if-nez v0, :cond_0

    .line 628
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    .line 631
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/month/be;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/be;-><init>(Lcom/android/calendar/month/bb;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 637
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/android/calendar/month/bb;->S:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/bb;->e:Lcom/android/calendar/month/bg;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 660
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 661
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 647
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 648
    iget-object v0, p0, Lcom/android/calendar/month/bb;->d:Lcom/android/calendar/month/bh;

    if-eqz v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/android/calendar/month/bb;->d:Lcom/android/calendar/month/bh;

    invoke-virtual {v0}, Lcom/android/calendar/month/bh;->notifyDataSetChanged()V

    .line 653
    :goto_0
    return-void

    .line 651
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/month/bb;->dismissAllowingStateLoss()V

    goto :goto_0
.end method
