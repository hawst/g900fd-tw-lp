.class public Lcom/android/calendar/month/a;
.super Landroid/app/DialogFragment;
.source "EventInfoListFragment.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/View;

.field private c:F

.field private d:J

.field private e:Landroid/app/Activity;

.field private f:Lcom/android/calendar/al;

.field private g:Landroid/widget/ImageButton;

.field private h:Landroid/widget/ListView;

.field private i:Ljava/util/ArrayList;

.field private j:Ljava/util/ArrayList;

.field private k:Lcom/android/calendar/month/e;

.field private l:Landroid/util/LruCache;

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Z

.field private s:Lcom/android/calendar/d/g;

.field private t:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const-class v0, Lcom/android/calendar/month/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x258

    const/4 v1, 0x0

    .line 130
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/month/a;->c:F

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/a;->i:Ljava/util/ArrayList;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/a;->j:Ljava/util/ArrayList;

    .line 119
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/month/a;->m:I

    .line 120
    iput v2, p0, Lcom/android/calendar/month/a;->n:I

    .line 121
    iput v2, p0, Lcom/android/calendar/month/a;->o:I

    .line 122
    iput v1, p0, Lcom/android/calendar/month/a;->p:I

    .line 123
    iput v1, p0, Lcom/android/calendar/month/a;->q:I

    .line 125
    iput-boolean v1, p0, Lcom/android/calendar/month/a;->r:Z

    .line 848
    new-instance v0, Lcom/android/calendar/month/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/d;-><init>(Lcom/android/calendar/month/a;)V

    iput-object v0, p0, Lcom/android/calendar/month/a;->t:Landroid/content/BroadcastReceiver;

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;JZZ)V
    .locals 4

    .prologue
    const/16 v2, 0x258

    const/4 v1, 0x0

    .line 134
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/month/a;->c:F

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/a;->i:Ljava/util/ArrayList;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/a;->j:Ljava/util/ArrayList;

    .line 119
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/month/a;->m:I

    .line 120
    iput v2, p0, Lcom/android/calendar/month/a;->n:I

    .line 121
    iput v2, p0, Lcom/android/calendar/month/a;->o:I

    .line 122
    iput v1, p0, Lcom/android/calendar/month/a;->p:I

    .line 123
    iput v1, p0, Lcom/android/calendar/month/a;->q:I

    .line 125
    iput-boolean v1, p0, Lcom/android/calendar/month/a;->r:Z

    .line 848
    new-instance v0, Lcom/android/calendar/month/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/d;-><init>(Lcom/android/calendar/month/a;)V

    iput-object v0, p0, Lcom/android/calendar/month/a;->t:Landroid/content/BroadcastReceiver;

    .line 135
    iput-object p2, p0, Lcom/android/calendar/month/a;->i:Ljava/util/ArrayList;

    .line 136
    iput-object p3, p0, Lcom/android/calendar/month/a;->j:Ljava/util/ArrayList;

    .line 137
    iput-wide p4, p0, Lcom/android/calendar/month/a;->d:J

    .line 138
    iput-boolean p7, p0, Lcom/android/calendar/month/a;->r:Z

    .line 139
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/month/a;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/month/a;Landroid/app/Activity;)Landroid/app/Activity;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    return-object p1
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 259
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 260
    if-nez v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 263
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 265
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 266
    invoke-static {v0}, Lcom/android/calendar/g/h;->d(Landroid/view/Window;)V

    .line 267
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 268
    const/high16 v2, 0x3f000000    # 0.5f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 269
    iget v2, p0, Lcom/android/calendar/month/a;->m:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 270
    iget v2, p0, Lcom/android/calendar/month/a;->n:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 271
    const/16 v2, 0x11

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 272
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 273
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 274
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 317
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 318
    new-instance v5, Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/hj;->e()[C

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([C)V

    .line 319
    const v0, 0x7f0f012b

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 320
    iget-object v0, p0, Lcom/android/calendar/month/a;->b:Landroid/view/View;

    const v2, 0x7f1201c8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 321
    const/4 v7, 0x5

    .line 323
    iget-wide v2, p0, Lcom/android/calendar/month/a;->d:J

    .line 324
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 325
    :cond_0
    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(J)J

    move-result-wide v2

    .line 327
    :cond_1
    invoke-static {v2, v3, v1, v7}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    .line 328
    const/16 v8, 0x8

    invoke-static {v2, v3, v1, v8}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 330
    if-eqz v0, :cond_2

    .line 332
    const/16 v3, 0x21

    .line 333
    const-string v1, "YMD"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 334
    new-instance v1, Landroid/text/SpannableStringBuilder;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 335
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    :goto_0
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 342
    const v2, 0x7f0c01c2

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-double v4, v2

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v4, v6

    double-to-int v2, v4

    .line 343
    new-instance v4, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v4, v2}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v1, v4, v2, v5, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 344
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    :cond_2
    return-void

    .line 337
    :cond_3
    new-instance v1, Landroid/text/SpannableStringBuilder;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 338
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;JLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 806
    if-nez p1, :cond_0

    .line 828
    :goto_0
    return-void

    .line 809
    :cond_0
    const/4 v0, 0x0

    .line 810
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-eqz v1, :cond_2

    .line 811
    iget-object v0, p0, Lcom/android/calendar/month/a;->l:Landroid/util/LruCache;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 812
    if-nez v0, :cond_2

    .line 813
    if-nez p4, :cond_1

    .line 814
    iget-object v0, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    invoke-static {v0, p2, p3}, Lcom/android/calendar/gx;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p4

    .line 816
    :cond_1
    invoke-static {p4, v4, v4}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 817
    if-eqz v0, :cond_2

    .line 818
    iget-object v1, p0, Lcom/android/calendar/month/a;->l:Landroid/util/LruCache;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    :cond_2
    if-eqz v0, :cond_3

    .line 823
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 824
    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 826
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 90
    invoke-static {p0, p1}, Lcom/android/calendar/month/a;->b(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/month/a;Landroid/widget/ImageView;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/month/a;->a(Landroid/widget/ImageView;JLjava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/month/a;)Lcom/android/calendar/month/e;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 280
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    if-nez v0, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    const/4 v0, 0x0

    .line 286
    iget v1, p0, Lcom/android/calendar/month/a;->p:I

    iget-object v2, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 287
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/a;->p:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/month/a;->q:I

    mul-int/2addr v0, v1

    .line 289
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/a;->p:I

    .line 291
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 292
    const v2, 0x7f0c01e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 293
    const v3, 0x7f0d0024

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 294
    const v4, 0x7f0d0023

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 295
    add-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/android/calendar/month/a;->c:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 296
    const v4, 0x7f0c01e6

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    .line 297
    add-int/2addr v2, v3

    add-int/2addr v1, v2

    invoke-direct {p0}, Lcom/android/calendar/month/a;->d()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/a;->q:I

    iget-object v3, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v3

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 300
    if-eqz v0, :cond_0

    .line 301
    iget v2, p0, Lcom/android/calendar/month/a;->n:I

    if-ge v1, v2, :cond_3

    .line 302
    iget v1, p0, Lcom/android/calendar/month/a;->n:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/a;->n:I

    .line 304
    :cond_3
    iget v0, p0, Lcom/android/calendar/month/a;->n:I

    iget v1, p0, Lcom/android/calendar/month/a;->o:I

    if-le v0, v1, :cond_4

    .line 305
    iget v0, p0, Lcom/android/calendar/month/a;->o:I

    iput v0, p0, Lcom/android/calendar/month/a;->n:I

    .line 307
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/month/a;->a()V

    goto :goto_0
.end method

.method private static b(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 799
    if-nez p0, :cond_0

    .line 803
    :goto_0
    return-void

    .line 802
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private c()I
    .locals 2

    .prologue
    .line 351
    const/4 v0, 0x0

    .line 352
    iget-object v1, p0, Lcom/android/calendar/month/a;->i:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 353
    iget-object v1, p0, Lcom/android/calendar/month/a;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/a;->j:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 356
    iget-object v1, p0, Lcom/android/calendar/month/a;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    :cond_1
    return v0
.end method

.method static synthetic c(Lcom/android/calendar/month/a;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/month/a;->f:Lcom/android/calendar/al;

    return-object v0
.end method

.method private d()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 879
    .line 880
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    .line 881
    if-nez v1, :cond_1

    .line 890
    :cond_0
    :goto_0
    return v0

    .line 884
    :cond_1
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 885
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 886
    if-eqz v1, :cond_0

    .line 887
    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    .line 888
    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/month/a;)Lcom/android/calendar/d/g;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/month/a;->s:Lcom/android/calendar/d/g;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/month/a;)J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/android/calendar/month/a;->d:J

    return-wide v0
.end method


# virtual methods
.method public a(III)V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method public a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 859
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/month/a;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 876
    :cond_0
    :goto_0
    return-void

    .line 863
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    if-eqz v0, :cond_0

    .line 866
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 867
    :cond_3
    iput-object p1, p0, Lcom/android/calendar/month/a;->i:Ljava/util/ArrayList;

    .line 868
    iput-object p2, p0, Lcom/android/calendar/month/a;->j:Ljava/util/ArrayList;

    .line 869
    iget-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    invoke-virtual {v0, p1}, Lcom/android/calendar/month/e;->a(Ljava/util/ArrayList;)V

    .line 870
    iget-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    invoke-virtual {v0, p2}, Lcom/android/calendar/month/e;->b(Ljava/util/ArrayList;)V

    .line 871
    iget-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    invoke-virtual {v0}, Lcom/android/calendar/month/e;->notifyDataSetChanged()V

    .line 872
    invoke-direct {p0}, Lcom/android/calendar/month/a;->b()V

    goto :goto_0

    .line 874
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->dismissAllowingStateLoss()V

    goto :goto_0
.end method

.method public a(JLandroid/app/Activity;)Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 779
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 795
    :cond_0
    :goto_0
    return v7

    .line 782
    :cond_1
    invoke-virtual {p3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 784
    sget-object v1, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 785
    new-array v2, v6, [Ljava/lang/String;

    const-string v4, "complete"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 787
    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 788
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, v6, :cond_3

    move v0, v6

    :goto_1
    move v7, v0

    .line 791
    :cond_2
    if-eqz v1, :cond_0

    .line 792
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    move v0, v7

    .line 788
    goto :goto_1

    .line 791
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 792
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public dismiss()V
    .locals 2

    .prologue
    .line 833
    :try_start_0
    invoke-super {p0}, Landroid/app/DialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 837
    :goto_0
    return-void

    .line 834
    :catch_0
    move-exception v0

    .line 835
    sget-object v0, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    const-string v1, "Fragment is already removed"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 252
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 253
    iget v0, p0, Lcom/android/calendar/month/a;->n:I

    invoke-direct {p0}, Lcom/android/calendar/month/a;->d()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/month/a;->n:I

    .line 254
    iget v0, p0, Lcom/android/calendar/month/a;->n:I

    iput v0, p0, Lcom/android/calendar/month/a;->o:I

    .line 255
    invoke-direct {p0}, Lcom/android/calendar/month/a;->a()V

    .line 256
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 6

    .prologue
    const/4 v0, 0x5

    .line 143
    iget-object v1, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 144
    iput-object p1, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 147
    iget-object v1, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/android/calendar/month/a;->c:F

    .line 148
    const v1, 0x7f0d000d

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iget v3, p0, Lcom/android/calendar/month/a;->c:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/a;->m:I

    .line 151
    const v1, 0x7f0c01e7

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/lit8 v3, v1, 0x1

    .line 152
    const v1, 0x7f0c0301

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/calendar/month/a;->q:I

    .line 153
    invoke-direct {p0}, Lcom/android/calendar/month/a;->c()I

    move-result v1

    .line 155
    const v4, 0x7f0d0024

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 156
    const v5, 0x7f0d0023

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 157
    add-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, p0, Lcom/android/calendar/month/a;->c:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 158
    const v5, 0x7f0c01e6

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    .line 159
    add-int/2addr v3, v4

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/month/a;->q:I

    if-le v1, v0, :cond_1

    :goto_0
    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/month/a;->n:I

    .line 161
    iget v0, p0, Lcom/android/calendar/month/a;->n:I

    iput v0, p0, Lcom/android/calendar/month/a;->o:I

    .line 162
    const/4 v0, 0x1

    const v1, 0x7f10000c

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/a;->setStyle(II)V

    .line 163
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/a;->s:Lcom/android/calendar/d/g;

    .line 164
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 165
    return-void

    :cond_1
    move v0, v1

    .line 159
    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 169
    const v0, 0x7f040053

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/a;->b:Landroid/view/View;

    .line 170
    iget-object v0, p0, Lcom/android/calendar/month/a;->b:Landroid/view/View;

    const v1, 0x7f1201c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    .line 171
    iget-object v0, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/a;->f:Lcom/android/calendar/al;

    .line 172
    iget-object v0, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gx;->a(Landroid/app/FragmentManager;)Landroid/util/LruCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/a;->l:Landroid/util/LruCache;

    .line 174
    new-instance v0, Lcom/android/calendar/month/e;

    iget-object v1, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/month/a;->i:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/month/a;->j:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/calendar/month/e;-><init>(Lcom/android/calendar/month/a;Landroid/app/Activity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    .line 175
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 177
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 178
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 179
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 181
    iget-object v0, p0, Lcom/android/calendar/month/a;->b:Landroid/view/View;

    const v1, 0x7f1201c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/month/a;->g:Landroid/widget/ImageButton;

    .line 183
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02010a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 184
    iget-object v1, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 185
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 187
    iget-object v0, p0, Lcom/android/calendar/month/a;->g:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/month/b;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/b;-><init>(Lcom/android/calendar/month/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-boolean v0, p0, Lcom/android/calendar/month/a;->r:Z

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/android/calendar/month/a;->g:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/a;->b:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/calendar/month/a;->a(Landroid/view/View;)V

    .line 209
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/a;->p:I

    .line 211
    iget-object v0, p0, Lcom/android/calendar/month/a;->h:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/month/c;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/c;-><init>(Lcom/android/calendar/month/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 247
    iget-object v0, p0, Lcom/android/calendar/month/a;->b:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 410
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 411
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 405
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 406
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 842
    :try_start_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 846
    :goto_0
    return-void

    .line 843
    :catch_0
    move-exception v0

    .line 844
    sget-object v0, Lcom/android/calendar/month/a;->a:Ljava/lang/String;

    const-string v1, "Fragment is already removed"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/calendar/month/a;->e:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/month/a;->t:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 390
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 391
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 363
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 365
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 366
    const-string v1, "Checked"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 367
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/a;->t:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 369
    invoke-direct {p0}, Lcom/android/calendar/month/a;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 370
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/month/a;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->dismissAllowingStateLoss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/month/a;->b:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/calendar/month/a;->a(Landroid/view/View;)V

    .line 385
    return-void

    .line 373
    :catch_0
    move-exception v0

    .line 374
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 395
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 396
    iget-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/android/calendar/month/a;->k:Lcom/android/calendar/month/e;

    invoke-virtual {v0}, Lcom/android/calendar/month/e;->notifyDataSetChanged()V

    .line 401
    :goto_0
    return-void

    .line 399
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/month/a;->dismissAllowingStateLoss()V

    goto :goto_0
.end method
