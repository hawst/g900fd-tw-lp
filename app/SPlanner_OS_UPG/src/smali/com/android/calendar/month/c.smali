.class Lcom/android/calendar/month/c;
.super Ljava/lang/Object;
.source "EventInfoListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/a;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/a;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/android/calendar/month/c;->a:Lcom/android/calendar/month/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 18

    .prologue
    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/c;->a:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->b(Lcom/android/calendar/month/a;)Lcom/android/calendar/month/e;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/android/calendar/month/e;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 217
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/c;->a:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->b(Lcom/android/calendar/month/a;)Lcom/android/calendar/month/e;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/android/calendar/month/e;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/task/aa;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :goto_0
    if-eqz v2, :cond_0

    .line 222
    iget-wide v8, v2, Lcom/android/calendar/task/aa;->b:J

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/c;->a:Lcom/android/calendar/month/a;

    invoke-virtual {v2}, Lcom/android/calendar/month/a;->dismissAllowingStateLoss()V

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/c;->a:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->c(Lcom/android/calendar/month/a;)Lcom/android/calendar/al;

    move-result-object v2

    const-wide/16 v4, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v11}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    .line 244
    :cond_0
    :goto_1
    return-void

    .line 218
    :catch_0
    move-exception v2

    .line 219
    const/4 v2, 0x0

    goto :goto_0

    .line 230
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/c;->a:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->b(Lcom/android/calendar/month/a;)Lcom/android/calendar/month/e;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/android/calendar/month/e;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 234
    :goto_2
    if-eqz v2, :cond_0

    .line 235
    iget-wide v6, v2, Lcom/android/calendar/dh;->b:J

    .line 236
    iget-wide v8, v2, Lcom/android/calendar/dh;->m:J

    .line 237
    iget-wide v10, v2, Lcom/android/calendar/dh;->n:J

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/c;->a:Lcom/android/calendar/month/a;

    invoke-virtual {v2}, Lcom/android/calendar/month/a;->dismissAllowingStateLoss()V

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/c;->a:Lcom/android/calendar/month/a;

    invoke-static {v2}, Lcom/android/calendar/month/a;->c(Lcom/android/calendar/month/a;)Lcom/android/calendar/al;

    move-result-object v2

    const-wide/16 v4, 0x2

    const/4 v12, -0x1

    const/4 v13, -0x1

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_1

    .line 231
    :catch_1
    move-exception v2

    .line 232
    const/4 v2, 0x0

    goto :goto_2
.end method
