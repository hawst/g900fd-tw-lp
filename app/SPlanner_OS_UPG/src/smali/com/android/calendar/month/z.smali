.class Lcom/android/calendar/month/z;
.super Ljava/lang/Object;
.source "MonthByWeekFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/k;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/k;)V
    .locals 0

    .prologue
    .line 1454
    iput-object p1, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/calendar/month/k;->E:Z

    .line 1458
    iget-object v0, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    iget-object v2, v0, Lcom/android/calendar/month/k;->ab:Lcom/android/calendar/month/MonthListView;

    .line 1464
    iget-object v0, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->D:Lcom/android/calendar/month/ad;

    sget-object v1, Lcom/android/calendar/month/ad;->b:Lcom/android/calendar/month/ad;

    if-ne v0, v1, :cond_0

    .line 1465
    const/4 v0, 0x0

    .line 1466
    invoke-virtual {v2, v0}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1467
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    .line 1477
    :goto_0
    const/16 v3, 0x190

    invoke-virtual {v2, v1, v3}, Lcom/android/calendar/month/MonthListView;->smoothScrollBy(II)V

    .line 1479
    check-cast v0, Lcom/android/calendar/month/cd;

    iget-object v1, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    iget v1, v1, Lcom/android/calendar/month/k;->F:F

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/cd;->c(F)I

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->z:I

    .line 1481
    iget-object v0, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->notifyDataSetChanged()V

    .line 1483
    iget-object v0, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->W:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    invoke-static {v1}, Lcom/android/calendar/month/k;->m(Lcom/android/calendar/month/k;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1484
    :goto_1
    return-void

    .line 1468
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    iget-object v0, v0, Lcom/android/calendar/month/k;->D:Lcom/android/calendar/month/ad;

    sget-object v1, Lcom/android/calendar/month/ad;->c:Lcom/android/calendar/month/ad;

    if-ne v0, v1, :cond_1

    .line 1469
    invoke-virtual {v2}, Lcom/android/calendar/month/MonthListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1470
    invoke-virtual {v2, v0}, Lcom/android/calendar/month/MonthListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1471
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_0

    .line 1473
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/z;->a:Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->i()V

    goto :goto_1
.end method
