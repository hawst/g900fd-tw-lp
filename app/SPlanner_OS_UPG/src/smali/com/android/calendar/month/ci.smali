.class public Lcom/android/calendar/month/ci;
.super Landroid/app/Fragment;
.source "YearFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# instance fields
.field private a:Lcom/android/calendar/al;

.field private b:Lcom/android/calendar/AllInOneActivity;

.field private c:Landroid/text/format/Time;

.field private d:Landroid/text/format/Time;

.field private e:Lcom/android/calendar/month/cl;

.field private f:Landroid/view/animation/Animation;

.field private g:Landroid/view/animation/Animation;

.field private h:Ljava/lang/Runnable;

.field private i:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 83
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/month/ci;-><init>(J)V

    .line 84
    return-void
.end method

.method public constructor <init>(J)V
    .locals 5

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 55
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    .line 65
    new-instance v0, Lcom/android/calendar/month/cj;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/cj;-><init>(Lcom/android/calendar/month/ci;)V

    iput-object v0, p0, Lcom/android/calendar/month/ci;->h:Ljava/lang/Runnable;

    .line 278
    new-instance v0, Lcom/android/calendar/month/ck;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/ck;-><init>(Lcom/android/calendar/month/ci;)V

    iput-object v0, p0, Lcom/android/calendar/month/ci;->i:Landroid/view/animation/Animation$AnimationListener;

    .line 87
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 88
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 89
    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    .line 90
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_2

    .line 91
    if-nez v1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 104
    :goto_0
    return-void

    .line 94
    :cond_0
    if-gez v1, :cond_1

    .line 95
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x76e

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/format/Time;->set(III)V

    .line 99
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto :goto_0

    .line 97
    :cond_1
    const/16 v1, 0x1f

    const/16 v2, 0xb

    const/16 v3, 0x7f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/format/Time;->set(III)V

    goto :goto_1

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/month/ci;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/calendar/month/ci;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/calendar/month/ci;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 275
    iget-object v0, p0, Lcom/android/calendar/month/ci;->e:Lcom/android/calendar/month/cl;

    invoke-virtual {v0, p1}, Lcom/android/calendar/month/cl;->setSelectedTime(Landroid/text/format/Time;)V

    .line 276
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/month/ci;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    return-object v0
.end method

.method private b(Lcom/android/calendar/aq;)V
    .locals 11

    .prologue
    const-wide/16 v2, 0x400

    const-wide/16 v6, -0x1

    const/4 v10, 0x6

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 217
    iget-object v0, p0, Lcom/android/calendar/month/ci;->e:Lcom/android/calendar/month/cl;

    if-nez v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/aq;->d:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/ci;->e:Lcom/android/calendar/month/cl;

    invoke-virtual {v0}, Lcom/android/calendar/month/cl;->invalidate()V

    .line 225
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_4

    .line 226
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    .line 227
    if-eqz v0, :cond_2

    .line 228
    iget-object v0, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    iget-object v4, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-ne v0, v10, :cond_3

    .line 231
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->a()V

    .line 233
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 236
    :cond_4
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    .line 237
    if-eqz v0, :cond_6

    .line 238
    iget-object v1, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->i()V

    .line 239
    if-lez v0, :cond_8

    .line 240
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/16 v1, 0x1f

    const/16 v4, 0xb

    const/16 v5, 0x7f4

    invoke-virtual {v0, v1, v4, v5}, Landroid/text/format/Time;->set(III)V

    .line 244
    :cond_5
    :goto_1
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v9}, Landroid/text/format/Time;->normalize(Z)J

    .line 247
    :cond_6
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    .line 248
    iget-object v1, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    sub-int/2addr v0, v1

    .line 249
    iget-object v1, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    iget-object v4, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 251
    iget-object v1, p0, Lcom/android/calendar/month/ci;->e:Lcom/android/calendar/month/cl;

    iget-object v4, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    invoke-virtual {v1, v4}, Lcom/android/calendar/month/cl;->setSelectedTime(Landroid/text/format/Time;)V

    .line 253
    if-eqz v0, :cond_7

    .line 254
    iget-object v1, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    if-eqz v1, :cond_7

    .line 255
    iget-object v1, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v1, v8}, Lcom/android/calendar/AllInOneActivity;->a(I)V

    .line 256
    if-lez v0, :cond_9

    .line 257
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->b()V

    .line 258
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, p0, Lcom/android/calendar/month/ci;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->a(Landroid/view/animation/Animation;)V

    .line 259
    iget-object v0, p0, Lcom/android/calendar/month/ci;->f:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/calendar/month/ci;->i:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 268
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    if-ne v0, v10, :cond_0

    .line 269
    iget-object v0, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    iget-object v4, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    iget-object v5, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    goto/16 :goto_0

    .line 241
    :cond_8
    if-gez v0, :cond_5

    .line 242
    iget-object v0, p1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/16 v1, 0x76e

    invoke-virtual {v0, v9, v8, v1}, Landroid/text/format/Time;->set(III)V

    goto :goto_1

    .line 261
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    iget-object v1, p0, Lcom/android/calendar/month/ci;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/android/calendar/AllInOneActivity;->b(Landroid/view/animation/Animation;)V

    .line 262
    iget-object v0, p0, Lcom/android/calendar/month/ci;->g:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/calendar/month/ci;->i:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_2
.end method

.method private b()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 181
    iget-object v1, p0, Lcom/android/calendar/month/ci;->d:Landroid/text/format/Time;

    if-nez v1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v0

    .line 184
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-static {v1}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v1

    .line 185
    if-eqz v1, :cond_2

    .line 186
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/ci;->d:Landroid/text/format/Time;

    .line 190
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/month/ci;->d:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 191
    iget-object v1, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    invoke-virtual {v1}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 192
    iget-object v2, p0, Lcom/android/calendar/month/ci;->d:Landroid/text/format/Time;

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/month/ci;->d:Landroid/text/format/Time;

    iget-wide v4, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 193
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 188
    :cond_2
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/month/ci;->d:Landroid/text/format/Time;

    goto :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/month/ci;)Lcom/android/calendar/AllInOneActivity;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/android/calendar/month/ci;->e:Lcom/android/calendar/month/cl;

    invoke-virtual {v0}, Lcom/android/calendar/month/cl;->b()V

    .line 299
    iget-object v0, p0, Lcom/android/calendar/month/ci;->e:Lcom/android/calendar/month/cl;

    invoke-virtual {v0}, Lcom/android/calendar/month/cl;->invalidate()V

    .line 300
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 208
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x20

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 209
    invoke-direct {p0, p1}, Lcom/android/calendar/month/ci;->b(Lcom/android/calendar/aq;)V

    .line 214
    :cond_0
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 202
    const-wide/32 v0, 0x10020

    return-wide v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 109
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    .line 110
    const v0, 0x7f050013

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/ci;->f:Landroid/view/animation/Animation;

    .line 112
    const v0, 0x7f050010

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/ci;->g:Landroid/view/animation/Animation;

    .line 114
    instance-of v0, p1, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 115
    check-cast v0, Lcom/android/calendar/AllInOneActivity;

    iput-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    .line 117
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/month/ci;->d:Landroid/text/format/Time;

    .line 118
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    .line 122
    if-eqz p3, :cond_0

    const-string v0, "key_restore_time"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    const-string v1, "key_restore_time"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/ci;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 127
    new-instance v9, Lcom/android/calendar/month/cl;

    invoke-virtual {p0}, Lcom/android/calendar/month/ci;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v9, v0}, Lcom/android/calendar/month/cl;-><init>(Landroid/app/Activity;)V

    .line 128
    iget-object v0, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    invoke-virtual {v9, v0}, Lcom/android/calendar/month/cl;->setSelectedTime(Landroid/text/format/Time;)V

    .line 129
    iput-object v9, p0, Lcom/android/calendar/month/ci;->e:Lcom/android/calendar/month/cl;

    .line 131
    iget-object v0, p0, Lcom/android/calendar/month/ci;->a:Lcom/android/calendar/al;

    const-wide/16 v2, 0x400

    iget-object v4, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 132
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/ci;->setHasOptionsMenu(Z)V

    .line 133
    return-object v9
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 153
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 155
    invoke-virtual {p0}, Lcom/android/calendar/month/ci;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 158
    const-string v2, "YearViewZoomIn"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_0

    .line 162
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 163
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 173
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    instance-of v0, v0, Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 175
    iget-object v1, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-direct {p0}, Lcom/android/calendar/month/ci;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/calendar/AllInOneActivity;->a(Z)V

    .line 178
    :cond_0
    return-void

    .line 175
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 145
    iget-object v0, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    invoke-direct {p0, v0}, Lcom/android/calendar/month/ci;->a(Landroid/text/format/Time;)V

    .line 146
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/android/calendar/month/ci;->b:Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v0}, Lcom/android/calendar/AllInOneActivity;->invalidateOptionsMenu()V

    .line 149
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 138
    const-string v0, "key_restore_time"

    iget-object v1, p0, Lcom/android/calendar/month/ci;->c:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 139
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 140
    return-void
.end method
