.class public Lcom/android/calendar/month/ce;
.super Landroid/widget/BaseAdapter;
.source "SimpleWeeksAdapter.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field protected static B:I

.field protected static C:I

.field protected static D:I

.field protected static E:I

.field protected static F:I

.field protected static G:F

.field protected static H:Landroid/content/Context;


# instance fields
.field protected I:Landroid/text/format/Time;

.field protected J:I

.field protected K:I

.field protected L:Z

.field protected M:Z

.field protected N:Landroid/view/GestureDetector;

.field protected O:I

.field protected P:I

.field protected Q:I

.field protected R:Z

.field protected S:I

.field protected T:Z

.field protected U:Ljava/util/ArrayList;

.field protected V:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 91
    const/4 v0, 0x6

    sput v0, Lcom/android/calendar/month/ce;->B:I

    .line 93
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/month/ce;->C:I

    .line 95
    sput v1, Lcom/android/calendar/month/ce;->D:I

    .line 97
    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/month/ce;->E:I

    .line 99
    sput v1, Lcom/android/calendar/month/ce;->F:I

    .line 101
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/month/ce;->G:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 114
    iput-boolean v1, p0, Lcom/android/calendar/month/ce;->L:Z

    .line 116
    iput-boolean v1, p0, Lcom/android/calendar/month/ce;->M:Z

    .line 120
    sget v0, Lcom/android/calendar/month/ce;->B:I

    iput v0, p0, Lcom/android/calendar/month/ce;->O:I

    .line 122
    sget v0, Lcom/android/calendar/month/ce;->D:I

    iput v0, p0, Lcom/android/calendar/month/ce;->P:I

    .line 124
    sget v0, Lcom/android/calendar/month/ce;->C:I

    iput v0, p0, Lcom/android/calendar/month/ce;->Q:I

    .line 126
    iput-boolean v1, p0, Lcom/android/calendar/month/ce;->R:Z

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/ce;->U:Ljava/util/ArrayList;

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/ce;->V:Ljava/util/ArrayList;

    .line 137
    sput-object p1, Lcom/android/calendar/month/ce;->H:Landroid/content/Context;

    .line 141
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/month/ce;->K:I

    .line 144
    sget v0, Lcom/android/calendar/month/ce;->G:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 145
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/android/calendar/month/ce;->G:F

    .line 146
    sget v0, Lcom/android/calendar/month/ce;->G:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 147
    sget v0, Lcom/android/calendar/month/ce;->F:I

    int-to-float v0, v0

    sget v1, Lcom/android/calendar/month/ce;->G:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/month/ce;->F:I

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->b()V

    .line 151
    invoke-virtual {p0, p2}, Lcom/android/calendar/month/ce;->a(Ljava/util/HashMap;)V

    .line 152
    return-void
.end method

.method private a(Lcom/android/calendar/month/cd;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 359
    iget-object v0, p0, Lcom/android/calendar/month/ce;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 360
    const-string v0, "MonthByWeek"

    invoke-static {v0, v4}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    const-string v0, "MonthByWeek"

    const-string v1, "No events loaded, did not pass any events to view."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_0
    invoke-virtual {p1, v5}, Lcom/android/calendar/month/cd;->setDayEvents(Ljava/util/List;)V

    .line 379
    :goto_0
    return-void

    .line 366
    :cond_1
    invoke-virtual {p1}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v0

    .line 367
    iget v1, p0, Lcom/android/calendar/month/ce;->S:I

    sub-int v1, v0, v1

    .line 368
    iget v2, p1, Lcom/android/calendar/month/cd;->ba:I

    add-int/2addr v2, v1

    .line 370
    if-ltz v1, :cond_2

    iget-object v3, p0, Lcom/android/calendar/month/ce;->U:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v2, v3, :cond_4

    .line 371
    :cond_2
    const-string v1, "MonthByWeek"

    invoke-static {v1, v4}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 372
    const-string v1, "MonthByWeek"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Week is outside range of loaded events. viewStart: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " eventsStart: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/calendar/month/ce;->S:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_3
    invoke-virtual {p1, v5}, Lcom/android/calendar/month/cd;->setDayEvents(Ljava/util/List;)V

    goto :goto_0

    .line 378
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/ce;->U:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/calendar/month/cd;->setDayEvents(Ljava/util/List;)V

    goto :goto_0
.end method

.method private b(Lcom/android/calendar/month/cd;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 382
    iget-object v0, p0, Lcom/android/calendar/month/ce;->V:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 383
    const-string v0, "MonthByWeek"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    const-string v0, "MonthByWeek"

    const-string v1, "No events loaded, did not pass any events to view."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :cond_0
    invoke-virtual {p1, v3}, Lcom/android/calendar/month/cd;->setDayTasks(Ljava/util/List;)V

    .line 398
    :goto_0
    return-void

    .line 389
    :cond_1
    invoke-virtual {p1}, Lcom/android/calendar/month/cd;->getFirstJulianDay()I

    move-result v0

    .line 390
    iget v1, p0, Lcom/android/calendar/month/ce;->S:I

    sub-int/2addr v0, v1

    .line 391
    iget v1, p1, Lcom/android/calendar/month/cd;->ba:I

    add-int/2addr v1, v0

    .line 393
    if-ltz v0, :cond_2

    iget-object v2, p0, Lcom/android/calendar/month/ce;->V:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v1, v2, :cond_3

    .line 394
    :cond_2
    invoke-virtual {p1, v3}, Lcom/android/calendar/month/cd;->setDayTasks(Ljava/util/List;)V

    goto :goto_0

    .line 397
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/month/ce;->V:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/calendar/month/cd;->setDayTasks(Ljava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method public a(IILjava/util/ArrayList;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 455
    iput p1, p0, Lcom/android/calendar/month/ce;->S:I

    .line 459
    if-nez p4, :cond_0

    .line 460
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    move-object v2, v0

    .line 465
    :goto_0
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 466
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 468
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 469
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p2, :cond_1

    .line 470
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 462
    :cond_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object v2, v0

    goto :goto_0

    .line 473
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 474
    :cond_2
    const-string v0, "MonthByWeek"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 475
    const-string v0, "MonthByWeek"

    const-string v1, "No events. Returning early--go schedule something fun."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_3
    iput-object v4, p0, Lcom/android/calendar/month/ce;->V:Ljava/util/ArrayList;

    .line 478
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->c()V

    .line 497
    :goto_2
    return-void

    .line 483
    :cond_4
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/aa;

    .line 484
    iget-wide v6, v0, Lcom/android/calendar/task/aa;->e:J

    iget-wide v8, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 485
    sub-int v6, v1, p1

    .line 486
    const/4 v1, 0x0

    move v3, v1

    :goto_3
    if-ge v3, p2, :cond_5

    .line 487
    if-ne v6, v3, :cond_6

    .line 488
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 486
    :cond_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 492
    :cond_7
    const-string v0, "MonthByWeek"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 493
    const-string v0, "MonthByWeek"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Processed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " events."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    :cond_8
    iput-object v4, p0, Lcom/android/calendar/month/ce;->V:Ljava/util/ArrayList;

    .line 496
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->c()V

    goto :goto_2
.end method

.method public a(Landroid/text/format/Time;)V
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 206
    iget-object v0, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 207
    iget-object v2, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/ce;->K:I

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(II)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/ce;->J:I

    .line 209
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->notifyDataSetChanged()V

    .line 210
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 169
    if-nez p1, :cond_0

    .line 170
    const-string v0, "MonthByWeek"

    const-string v1, "WeekParameters are null! Cannot update adapter."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :goto_0
    return-void

    .line 173
    :cond_0
    const-string v0, "focus_month"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    const-string v0, "focus_month"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/ce;->Q:I

    .line 176
    :cond_1
    const-string v0, "num_weeks"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    const-string v0, "num_weeks"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/ce;->O:I

    .line 179
    :cond_2
    const-string v0, "week_numbers"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    const-string v0, "week_numbers"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/month/ce;->L:Z

    .line 182
    :cond_3
    const-string v0, "week_start"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 183
    const-string v0, "week_start"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/ce;->K:I

    .line 185
    :cond_4
    const-string v0, "selected_day"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 186
    const-string v0, "selected_day"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 187
    iget-object v3, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    invoke-static {v3, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 188
    iget v3, p0, Lcom/android/calendar/month/ce;->K:I

    invoke-static {v0, v3}, Lcom/android/calendar/hj;->a(II)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/ce;->J:I

    .line 190
    :cond_5
    const-string v0, "days_per_week"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 191
    const-string v0, "days_per_week"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/ce;->P:I

    .line 193
    :cond_6
    const-string v0, "mini_mode_service"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 194
    const-string v0, "mini_mode_service"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_9

    :goto_2
    iput-boolean v1, p0, Lcom/android/calendar/month/ce;->R:Z

    .line 196
    :cond_7
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->c()V

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 180
    goto :goto_1

    :cond_9
    move v1, v2

    .line 194
    goto :goto_2
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 158
    new-instance v0, Landroid/view/GestureDetector;

    sget-object v1, Lcom/android/calendar/month/ce;->H:Landroid/content/Context;

    new-instance v2, Lcom/android/calendar/month/cf;

    invoke-direct {v2, p0}, Lcom/android/calendar/month/cf;-><init>(Lcom/android/calendar/month/ce;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/month/ce;->N:Landroid/view/GestureDetector;

    .line 159
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    .line 160
    iget-object v0, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 161
    return-void
.end method

.method public b(IILjava/util/ArrayList;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x0

    .line 403
    iput p1, p0, Lcom/android/calendar/month/ce;->S:I

    .line 405
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v4

    .line 406
    :goto_0
    if-ge v0, p2, :cond_0

    .line 407
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 406
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 410
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 411
    :cond_1
    const-string v0, "MonthByWeek"

    invoke-static {v0, v7}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 412
    const-string v0, "MonthByWeek"

    const-string v1, "No events. Returning early--go schedule something fun."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_2
    iput-object v5, p0, Lcom/android/calendar/month/ce;->U:Ljava/util/ArrayList;

    .line 415
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->c()V

    .line 446
    :goto_1
    return-void

    .line 420
    :cond_3
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 421
    iget v1, v0, Lcom/android/calendar/dh;->i:I

    sub-int v3, v1, p1

    .line 422
    iget v1, v0, Lcom/android/calendar/dh;->j:I

    sub-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x1

    .line 423
    if-lt v3, p2, :cond_5

    if-ltz v1, :cond_4

    .line 424
    :cond_5
    if-gez v3, :cond_6

    move v3, v4

    .line 427
    :cond_6
    if-gt v3, p2, :cond_4

    .line 430
    if-ltz v1, :cond_4

    .line 433
    if-le v1, p2, :cond_9

    move v2, p2

    .line 436
    :goto_2
    if-ge v3, v2, :cond_4

    .line 437
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 441
    :cond_7
    const-string v0, "MonthByWeek"

    invoke-static {v0, v7}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 442
    const-string v0, "MonthByWeek"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Processed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " events."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_8
    iput-object v5, p0, Lcom/android/calendar/month/ce;->U:Ljava/util/ArrayList;

    .line 445
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->c()V

    goto :goto_1

    :cond_9
    move v2, v1

    goto :goto_2
.end method

.method protected b(Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    iput v0, p1, Landroid/text/format/Time;->hour:I

    .line 339
    iget-object v0, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    iput v0, p1, Landroid/text/format/Time;->minute:I

    .line 340
    iget-object v0, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->second:I

    iput v0, p1, Landroid/text/format/Time;->second:I

    .line 341
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/ce;->a(Landroid/text/format/Time;)V

    .line 342
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->notifyDataSetChanged()V

    .line 226
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 308
    iput p1, p0, Lcom/android/calendar/month/ce;->Q:I

    .line 309
    invoke-virtual {p0}, Lcom/android/calendar/month/ce;->notifyDataSetChanged()V

    .line 310
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 356
    return-void
.end method

.method public g()Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 230
    const/16 v0, 0x1b86

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 240
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 254
    const/4 v0, 0x0

    .line 255
    if-eqz p2, :cond_2

    .line 256
    check-cast p2, Lcom/android/calendar/month/cd;

    .line 258
    invoke-virtual {p2}, Lcom/android/calendar/month/cd;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 267
    :goto_0
    if-nez v0, :cond_0

    .line 268
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 270
    :cond_0
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 273
    iget v4, p0, Lcom/android/calendar/month/ce;->J:I

    if-ne v4, p1, :cond_1

    .line 274
    iget-object v1, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->weekDay:I

    .line 280
    :cond_1
    const-string v4, "height"

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/android/calendar/month/ce;->O:I

    div-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    const-string v4, "selected_day"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    const-string v4, "show_wk_num"

    iget-boolean v1, p0, Lcom/android/calendar/month/ce;->L:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    const-string v1, "week_start"

    iget v4, p0, Lcom/android/calendar/month/ce;->K:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    const-string v1, "num_days"

    iget v4, p0, Lcom/android/calendar/month/ce;->P:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    const-string v1, "week"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    const-string v1, "focus_month"

    iget v4, p0, Lcom/android/calendar/month/ce;->Q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    iget-boolean v1, p0, Lcom/android/calendar/month/ce;->R:Z

    if-eqz v1, :cond_4

    .line 292
    :goto_2
    const-string v1, "mini_mode_service"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    iget-object v1, p0, Lcom/android/calendar/month/ce;->I:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/android/calendar/month/cd;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p2}, Lcom/android/calendar/month/cd;->invalidate()V

    .line 297
    invoke-direct {p0, p2}, Lcom/android/calendar/month/ce;->a(Lcom/android/calendar/month/cd;)V

    .line 298
    invoke-direct {p0, p2}, Lcom/android/calendar/month/ce;->b(Lcom/android/calendar/month/cd;)V

    .line 299
    return-object p2

    .line 260
    :cond_2
    new-instance p2, Lcom/android/calendar/month/cd;

    sget-object v4, Lcom/android/calendar/month/ce;->H:Landroid/content/Context;

    invoke-direct {p2, v4}, Lcom/android/calendar/month/cd;-><init>(Landroid/content/Context;)V

    .line 262
    new-instance v4, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v4, v1, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 263
    invoke-virtual {p2, v4}, Lcom/android/calendar/month/cd;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 264
    invoke-virtual {p2, v2}, Lcom/android/calendar/month/cd;->setClickable(Z)V

    .line 265
    invoke-virtual {p2, p0}, Lcom/android/calendar/month/cd;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_0

    :cond_3
    move v1, v3

    .line 282
    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 500
    iget-boolean v0, p0, Lcom/android/calendar/month/ce;->T:Z

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 314
    iget-object v0, p0, Lcom/android/calendar/month/ce;->N:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 315
    check-cast v0, Lcom/android/calendar/month/cd;

    .line 316
    check-cast p1, Lcom/android/calendar/month/cd;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1, v1}, Lcom/android/calendar/month/cd;->a(F)Landroid/text/format/Time;

    move-result-object v1

    .line 318
    if-eqz v1, :cond_0

    .line 319
    invoke-virtual {p0, v1}, Lcom/android/calendar/month/ce;->b(Landroid/text/format/Time;)V

    .line 321
    const-string v2, "MonthByWeek"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    const-string v2, "MonthByWeek"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Touched day at Row="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Lcom/android/calendar/month/cd;->aQ:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " day="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :cond_0
    const/4 v0, 0x1

    .line 329
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 245
    if-eqz p1, :cond_0

    .line 246
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 248
    :cond_0
    return-void
.end method
