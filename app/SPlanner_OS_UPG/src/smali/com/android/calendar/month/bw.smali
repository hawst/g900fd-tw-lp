.class final enum Lcom/android/calendar/month/bw;
.super Ljava/lang/Enum;
.source "MonthWeekEventsView.java"


# static fields
.field public static final enum a:Lcom/android/calendar/month/bw;

.field public static final enum b:Lcom/android/calendar/month/bw;

.field public static final enum c:Lcom/android/calendar/month/bw;

.field private static final synthetic d:[Lcom/android/calendar/month/bw;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 389
    new-instance v0, Lcom/android/calendar/month/bw;

    const-string v1, "NO_DRAWED_ITEM_POINTER"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/bw;->a:Lcom/android/calendar/month/bw;

    new-instance v0, Lcom/android/calendar/month/bw;

    const-string v1, "EVENT_DRAWED_POINTER"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/month/bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/bw;->b:Lcom/android/calendar/month/bw;

    .line 390
    new-instance v0, Lcom/android/calendar/month/bw;

    const-string v1, "DRAWED_STICKER_WEATHER_POINTER"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/month/bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/bw;->c:Lcom/android/calendar/month/bw;

    .line 389
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/calendar/month/bw;

    sget-object v1, Lcom/android/calendar/month/bw;->a:Lcom/android/calendar/month/bw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/month/bw;->b:Lcom/android/calendar/month/bw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/month/bw;->c:Lcom/android/calendar/month/bw;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/month/bw;->d:[Lcom/android/calendar/month/bw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 389
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/month/bw;
    .locals 1

    .prologue
    .line 389
    const-class v0, Lcom/android/calendar/month/bw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/bw;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/month/bw;
    .locals 1

    .prologue
    .line 389
    sget-object v0, Lcom/android/calendar/month/bw;->d:[Lcom/android/calendar/month/bw;

    invoke-virtual {v0}, [Lcom/android/calendar/month/bw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/month/bw;

    return-object v0
.end method
