.class public Lcom/android/calendar/month/j;
.super Lcom/android/calendar/month/cf;
.source "MonthByWeekAdapter.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/month/h;


# direct methods
.method protected constructor <init>(Lcom/android/calendar/month/h;)V
    .locals 0

    .prologue
    .line 781
    iput-object p1, p0, Lcom/android/calendar/month/j;->a:Lcom/android/calendar/month/h;

    invoke-direct {p0, p1}, Lcom/android/calendar/month/cf;-><init>(Lcom/android/calendar/month/ce;)V

    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 784
    sget-boolean v0, Lcom/android/calendar/month/h;->w:Z

    if-eqz v0, :cond_1

    .line 807
    :cond_0
    :goto_0
    return-void

    .line 787
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/j;->a:Lcom/android/calendar/month/h;

    iget-boolean v0, v0, Lcom/android/calendar/month/h;->f:Z

    if-nez v0, :cond_0

    .line 790
    sget-object v0, Lcom/android/calendar/month/h;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 795
    sget-object v0, Lcom/android/calendar/month/h;->s:Landroid/view/View;

    check-cast v0, Lcom/android/calendar/month/cd;

    sget v1, Lcom/android/calendar/month/h;->t:F

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/cd;->c(F)I

    move-result v0

    sput v0, Lcom/android/calendar/month/h;->z:I

    sput v0, Lcom/android/calendar/month/h;->y:I

    .line 797
    iget-object v1, p0, Lcom/android/calendar/month/j;->a:Lcom/android/calendar/month/h;

    sget-object v0, Lcom/android/calendar/month/h;->s:Landroid/view/View;

    check-cast v0, Lcom/android/calendar/month/cd;

    sget v2, Lcom/android/calendar/month/h;->t:F

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/cd;->a(F)Landroid/text/format/Time;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/h;->a(Landroid/text/format/Time;)V

    .line 803
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/month/h;->w:Z

    .line 804
    iget-object v0, p0, Lcom/android/calendar/month/j;->a:Lcom/android/calendar/month/h;

    invoke-virtual {v0}, Lcom/android/calendar/month/h;->notifyDataSetInvalidated()V

    .line 806
    invoke-super {p0, p1}, Lcom/android/calendar/month/cf;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method
