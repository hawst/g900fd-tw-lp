.class Lcom/android/calendar/month/t;
.super Landroid/content/BroadcastReceiver;
.source "MonthByWeekFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/month/k;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/k;)V
    .locals 0

    .prologue
    .line 3007
    iput-object p1, p0, Lcom/android/calendar/month/t;->a:Lcom/android/calendar/month/k;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3010
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 3011
    const-string v2, "android.location.MODE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3012
    iget-object v1, p0, Lcom/android/calendar/month/t;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    if-eqz v1, :cond_1

    .line 3013
    iget-object v1, p0, Lcom/android/calendar/month/t;->a:Lcom/android/calendar/month/k;

    iget-object v1, v1, Lcom/android/calendar/month/k;->k:Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_mode"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lcom/android/calendar/month/MonthEventList;->setLocationOn(Z)V

    .line 3017
    :cond_1
    return-void
.end method
