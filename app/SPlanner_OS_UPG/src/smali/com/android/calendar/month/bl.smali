.class Lcom/android/calendar/month/bl;
.super Ljava/lang/Object;
.source "MonthHoverEventFragment.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/bh;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/bh;)V
    .locals 0

    .prologue
    .line 1023
    iput-object p1, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 1026
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 1027
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-ne v2, v3, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 1028
    iget-object v1, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v1, v1, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-virtual {v1}, Lcom/android/calendar/month/bb;->dismiss()V

    .line 1055
    :goto_0
    return v0

    .line 1031
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 1055
    :cond_1
    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1033
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->m(Lcom/android/calendar/month/bb;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1034
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/android/calendar/ek;->a(I)V

    goto :goto_1

    .line 1038
    :pswitch_2
    invoke-static {v0}, Lcom/android/calendar/month/bb;->a(Z)Z

    .line 1039
    iget-object v0, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->b(Lcom/android/calendar/month/bb;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v1, v1, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->j(Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 1042
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->r(Lcom/android/calendar/month/bb;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v1, v1, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->n(Lcom/android/calendar/month/bb;)I

    move-result v1

    if-le v0, v1, :cond_2

    invoke-static {}, Lcom/android/calendar/month/bb;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1043
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->b(Lcom/android/calendar/month/bb;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v1, v1, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->j(Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bg;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1044
    iget-object v0, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->k(Lcom/android/calendar/month/bb;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1045
    iget-object v0, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->b(Lcom/android/calendar/month/bb;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v1, v1, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->j(Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bg;

    move-result-object v1

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 1047
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v0, v0, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v0}, Lcom/android/calendar/month/bb;->b(Lcom/android/calendar/month/bb;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/bl;->a:Lcom/android/calendar/month/bh;

    iget-object v1, v1, Lcom/android/calendar/month/bh;->a:Lcom/android/calendar/month/bb;

    invoke-static {v1}, Lcom/android/calendar/month/bb;->j(Lcom/android/calendar/month/bb;)Lcom/android/calendar/month/bg;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 1031
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
