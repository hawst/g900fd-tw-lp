.class Lcom/android/calendar/month/cr;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "YearView.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/month/cl;


# direct methods
.method private constructor <init>(Lcom/android/calendar/month/cl;)V
    .locals 0

    .prologue
    .line 1359
    iput-object p1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/month/cl;Lcom/android/calendar/month/cm;)V
    .locals 0

    .prologue
    .line 1359
    invoke-direct {p0, p1}, Lcom/android/calendar/month/cr;-><init>(Lcom/android/calendar/month/cl;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1401
    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, v1}, Lcom/android/calendar/month/cl;->c(Lcom/android/calendar/month/cl;I)I

    move-result v0

    .line 1402
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v1, v2}, Lcom/android/calendar/month/cl;->d(Lcom/android/calendar/month/cl;I)I

    move-result v1

    .line 1403
    if-eq v0, v3, :cond_0

    if-eq v1, v3, :cond_0

    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v2}, Lcom/android/calendar/month/cl;->f(Lcom/android/calendar/month/cl;)I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 1404
    :cond_0
    const/4 v0, 0x0

    .line 1409
    :goto_0
    return v0

    .line 1407
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v2, v0}, Lcom/android/calendar/month/cl;->e(Lcom/android/calendar/month/cl;I)I

    .line 1408
    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v0, v1}, Lcom/android/calendar/month/cl;->f(Lcom/android/calendar/month/cl;I)I

    .line 1409
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1362
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 1363
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1364
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    .line 1365
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 1366
    sget-boolean v5, Lcom/android/calendar/f/o;->n:Z

    if-nez v5, :cond_1

    .line 1396
    :cond_0
    :goto_0
    return v0

    .line 1368
    :cond_1
    iget-object v5, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    iget-boolean v5, v5, Lcom/android/calendar/month/cl;->f:Z

    if-eqz v5, :cond_0

    .line 1371
    const/16 v5, 0x32

    if-lt v3, v5, :cond_0

    if-le v3, v4, :cond_0

    .line 1372
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v4}, Lcom/android/calendar/month/cl;->b(Lcom/android/calendar/month/cl;)Landroid/app/Activity;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1373
    if-lez v2, :cond_4

    .line 1374
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v2}, Lcom/android/calendar/month/cl;->c(Lcom/android/calendar/month/cl;)Landroid/text/format/Time;

    move-result-object v2

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v4, 0x76e

    if-ne v2, v4, :cond_2

    .line 1375
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v1}, Lcom/android/calendar/month/cl;->d(Lcom/android/calendar/month/cl;)Lcom/android/calendar/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/al;->i()V

    goto :goto_0

    .line 1378
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Lcom/android/calendar/month/cl;->a(I)Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1386
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v2, v0}, Lcom/android/calendar/month/cl;->a(Lcom/android/calendar/month/cl;I)I

    .line 1387
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v2}, Lcom/android/calendar/month/cl;->invalidate()V

    .line 1388
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    const/4 v4, 0x6

    invoke-virtual {v2, v3, v4, v0}, Lcom/android/calendar/month/cl;->a(Landroid/text/format/Time;IZ)V

    .line 1389
    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v0}, Lcom/android/calendar/month/cl;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1390
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    const-string v4, "accessibility"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v2, v0}, Lcom/android/calendar/month/cl;->a(Lcom/android/calendar/month/cl;Landroid/view/accessibility/AccessibilityManager;)Landroid/view/accessibility/AccessibilityManager;

    .line 1391
    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v0}, Lcom/android/calendar/month/cl;->e(Lcom/android/calendar/month/cl;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v0}, Lcom/android/calendar/month/cl;->e(Lcom/android/calendar/month/cl;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1392
    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    iget v2, v3, Landroid/text/format/Time;->year:I

    invoke-static {v0, v2}, Lcom/android/calendar/month/cl;->b(Lcom/android/calendar/month/cl;I)V

    :cond_3
    move v0, v1

    .line 1394
    goto :goto_0

    .line 1380
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v2}, Lcom/android/calendar/month/cl;->c(Lcom/android/calendar/month/cl;)Landroid/text/format/Time;

    move-result-object v2

    iget v2, v2, Landroid/text/format/Time;->year:I

    const/16 v4, 0x7f4

    if-ne v2, v4, :cond_5

    .line 1381
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v1}, Lcom/android/calendar/month/cl;->d(Lcom/android/calendar/month/cl;)Lcom/android/calendar/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/al;->i()V

    goto/16 :goto_0

    .line 1384
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v2, v1}, Lcom/android/calendar/month/cl;->a(I)Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto :goto_1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 1414
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/cr;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    .line 1415
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 1416
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    .line 1467
    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v0}, Lcom/android/calendar/month/cl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1468
    if-eqz v0, :cond_0

    .line 1469
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v1}, Lcom/android/calendar/month/cl;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1471
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    float-to-int v1, p4

    invoke-static {v0, v1}, Lcom/android/calendar/month/cl;->g(Lcom/android/calendar/month/cl;I)I

    .line 1472
    iget-object v0, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v0}, Lcom/android/calendar/month/cl;->invalidate()V

    .line 1473
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, -0x1

    const/4 v0, 0x1

    .line 1420
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v1, v2}, Lcom/android/calendar/month/cl;->c(Lcom/android/calendar/month/cl;I)I

    move-result v1

    .line 1421
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-static {v2, v3}, Lcom/android/calendar/month/cl;->d(Lcom/android/calendar/month/cl;I)I

    move-result v2

    .line 1422
    if-eq v1, v4, :cond_0

    if-eq v2, v4, :cond_0

    iget-object v3, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v3}, Lcom/android/calendar/month/cl;->f(Lcom/android/calendar/month/cl;)I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1423
    :cond_0
    const/4 v0, 0x0

    .line 1461
    :goto_0
    return v0

    .line 1426
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v3, v1}, Lcom/android/calendar/month/cl;->e(Lcom/android/calendar/month/cl;I)I

    .line 1427
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v1, v2}, Lcom/android/calendar/month/cl;->f(Lcom/android/calendar/month/cl;I)I

    .line 1430
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v1, v0}, Lcom/android/calendar/month/cl;->a(Lcom/android/calendar/month/cl;I)I

    .line 1434
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v1}, Lcom/android/calendar/month/cl;->invalidate()V

    .line 1436
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v1}, Lcom/android/calendar/month/cl;->g(Lcom/android/calendar/month/cl;)I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v2}, Lcom/android/calendar/month/cl;->h(Lcom/android/calendar/month/cl;)I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v2}, Lcom/android/calendar/month/cl;->i(Lcom/android/calendar/month/cl;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1437
    iget-object v2, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v2}, Lcom/android/calendar/month/cl;->c(Lcom/android/calendar/month/cl;)Landroid/text/format/Time;

    move-result-object v2

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    .line 1440
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 1441
    iget-object v4, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v4}, Lcom/android/calendar/month/cl;->c(Lcom/android/calendar/month/cl;)Landroid/text/format/Time;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1442
    iget-object v4, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-static {v4}, Lcom/android/calendar/month/cl;->c(Lcom/android/calendar/month/cl;)Landroid/text/format/Time;

    move-result-object v4

    iget v4, v4, Landroid/text/format/Time;->monthDay:I

    iget v5, v3, Landroid/text/format/Time;->year:I

    invoke-virtual {v3, v4, v1, v5}, Landroid/text/format/Time;->set(III)V

    .line 1446
    const/4 v1, 0x4

    :try_start_0
    invoke-virtual {v3, v1}, Landroid/text/format/Time;->getActualMaximum(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1453
    :goto_1
    if-le v2, v1, :cond_2

    .line 1454
    iput v1, v3, Landroid/text/format/Time;->monthDay:I

    .line 1458
    :goto_2
    invoke-virtual {v3, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 1459
    iget-object v1, p0, Lcom/android/calendar/month/cr;->a:Lcom/android/calendar/month/cl;

    invoke-virtual {v1, v3, v6, v0}, Lcom/android/calendar/month/cl;->a(Landroid/text/format/Time;IZ)V

    goto :goto_0

    .line 1449
    :catch_0
    move-exception v1

    .line 1450
    const/16 v1, 0xf

    goto :goto_1

    .line 1456
    :cond_2
    iput v2, v3, Landroid/text/format/Time;->monthDay:I

    goto :goto_2
.end method
