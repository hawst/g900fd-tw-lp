.class Lcom/android/calendar/month/az;
.super Landroid/content/AsyncQueryHandler;
.source "MonthEventListFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/month/av;


# direct methods
.method public constructor <init>(Lcom/android/calendar/month/av;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    .line 398
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 399
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 403
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-virtual {v0}, Lcom/android/calendar/month/av;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 406
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-virtual {v0}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 407
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 408
    :cond_0
    if-eqz p3, :cond_1

    .line 409
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 449
    :cond_1
    :goto_0
    return-void

    .line 414
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 446
    :cond_3
    :goto_1
    if-eqz p3, :cond_1

    .line 447
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 416
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v1}, Lcom/android/calendar/month/av;->b(Lcom/android/calendar/month/av;)Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-object v1, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v1}, Lcom/android/calendar/month/av;->b(Lcom/android/calendar/month/av;)Landroid/text/format/Time;

    move-result-object v1

    iget-wide v6, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 417
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 418
    invoke-static {v3, v0, p3, v1, v1}, Lcom/android/calendar/month/ba;->a(Ljava/util/ArrayList;Landroid/content/Context;Landroid/database/Cursor;II)V

    .line 420
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v0, v3}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/av;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 424
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    iget-object v1, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v1}, Lcom/android/calendar/month/av;->h(Lcom/android/calendar/month/av;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/month/av;->b(Lcom/android/calendar/month/av;Landroid/net/Uri;)Landroid/net/Uri;

    .line 425
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->g(Lcom/android/calendar/month/av;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v3}, Lcom/android/calendar/month/av;->i(Lcom/android/calendar/month/av;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/calendar/month/ba;->b:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v5}, Lcom/android/calendar/month/av;->j(Lcom/android/calendar/month/av;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "due_date ASC, importance DESC"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 430
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->b(Lcom/android/calendar/month/av;)Landroid/text/format/Time;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v2}, Lcom/android/calendar/month/av;->b(Lcom/android/calendar/month/av;)Landroid/text/format/Time;

    move-result-object v2

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 431
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 432
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 433
    iget-object v3, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-virtual {v3}, Lcom/android/calendar/month/av;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v4}, Lcom/android/calendar/month/av;->b(Lcom/android/calendar/month/av;)Landroid/text/format/Time;

    move-result-object v4

    iget-object v4, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v1, v3, p3, v0, v4}, Lcom/android/calendar/month/ba;->a(Ljava/util/ArrayList;Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->k(Lcom/android/calendar/month/av;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2, v0, v1}, Lcom/android/calendar/month/ba;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 438
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v0, v2}, Lcom/android/calendar/month/av;->b(Lcom/android/calendar/month/av;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 440
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    iget-object v1, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v1}, Lcom/android/calendar/month/av;->b(Lcom/android/calendar/month/av;)Landroid/text/format/Time;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/month/av;->a(Lcom/android/calendar/month/av;Landroid/text/format/Time;)V

    .line 441
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->m(Lcom/android/calendar/month/av;)Lcom/android/calendar/month/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v1}, Lcom/android/calendar/month/av;->l(Lcom/android/calendar/month/av;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/month/aj;->a(Ljava/util/ArrayList;)V

    .line 442
    iget-object v0, p0, Lcom/android/calendar/month/az;->a:Lcom/android/calendar/month/av;

    invoke-static {v0}, Lcom/android/calendar/month/av;->m(Lcom/android/calendar/month/av;)Lcom/android/calendar/month/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/month/aj;->notifyDataSetChanged()V

    goto/16 :goto_1

    .line 414
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
