.class public Lcom/android/calendar/month/ba;
.super Ljava/lang/Object;
.source "MonthEventListItem.java"


# static fields
.field private static F:Ljava/lang/String;

.field private static G:I

.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;


# instance fields
.field public A:Z

.field public B:I

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:Z

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:J

.field public g:Ljava/lang/String;

.field public h:J

.field public i:J

.field public j:I

.field public k:I

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:J

.field public u:J

.field public v:Z

.field public w:I

.field public x:J

.field public y:J

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 44
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "eventLocation"

    aput-object v1, v0, v4

    const-string v1, "allDay"

    aput-object v1, v0, v5

    const-string v1, "calendar_color"

    aput-object v1, v0, v6

    const-string v1, "begin"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "end"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "rdate"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "startDay"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "endDay"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "event_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "guestsCanModify"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "hasAttendeeData"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "eventColor_index"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/month/ba;->a:[Ljava/lang/String;

    .line 118
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "subject"

    aput-object v1, v0, v4

    const-string v1, "due_date"

    aput-object v1, v0, v5

    const-string v1, "utc_due_date"

    aput-object v1, v0, v6

    const-string v1, "importance"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "groupName"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/month/ba;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/calendar/month/ba;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 367
    new-instance v2, Lcom/android/calendar/month/ba;

    invoke-direct {v2}, Lcom/android/calendar/month/ba;-><init>()V

    .line 369
    iput v6, v2, Lcom/android/calendar/month/ba;->c:I

    .line 371
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/month/ba;->f:J

    .line 373
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    .line 374
    iget-object v3, v2, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 375
    :cond_0
    sget-object v3, Lcom/android/calendar/month/ba;->F:Ljava/lang/String;

    iput-object v3, v2, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    .line 378
    :cond_1
    const/4 v3, 0x7

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 379
    if-gez v3, :cond_2

    .line 380
    invoke-static {p0, v1}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v3

    iput v3, v2, Lcom/android/calendar/month/ba;->e:I

    .line 385
    :goto_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/month/ba;->x:J

    .line 386
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/android/calendar/month/ba;->y:J

    .line 387
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/android/calendar/month/ba;->z:I

    .line 388
    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_3

    :goto_1
    iput-boolean v0, v2, Lcom/android/calendar/month/ba;->A:Z

    .line 389
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/android/calendar/month/ba;->B:I

    .line 390
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/calendar/month/ba;->D:Ljava/lang/String;

    .line 392
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 393
    invoke-static {v0}, Lcom/android/calendar/month/ba;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/android/calendar/month/ba;->E:Z

    .line 395
    return-object v2

    .line 382
    :cond_2
    invoke-static {p0, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v3

    iput v3, v2, Lcom/android/calendar/month/ba;->e:I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 388
    goto :goto_1
.end method

.method private static a(Landroid/database/Cursor;)Lcom/android/calendar/month/ba;
    .locals 9

    .prologue
    const/4 v3, 0x3

    const/16 v8, 0x13

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 291
    new-instance v5, Lcom/android/calendar/month/ba;

    invoke-direct {v5}, Lcom/android/calendar/month/ba;-><init>()V

    .line 293
    iput v1, v5, Lcom/android/calendar/month/ba;->c:I

    .line 295
    const/16 v0, 0xb

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/android/calendar/month/ba;->f:J

    .line 297
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    .line 298
    iget-object v0, v5, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, v5, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 299
    :cond_0
    sget-object v0, Lcom/android/calendar/month/ba;->F:Ljava/lang/String;

    iput-object v0, v5, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    .line 301
    :cond_1
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/android/calendar/month/ba;->g:Ljava/lang/String;

    .line 302
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, v5, Lcom/android/calendar/month/ba;->l:Z

    .line 304
    const/16 v0, 0x16

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v5, Lcom/android/calendar/month/ba;->w:I

    .line 305
    invoke-interface {p0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 306
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/hj;->b(I)I

    move-result v0

    iput v0, v5, Lcom/android/calendar/month/ba;->e:I

    .line 310
    :goto_1
    const/16 v0, 0x14

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_2

    .line 312
    const/16 v0, 0x15

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/hj;->b(I)I

    move-result v0

    iput v0, v5, Lcom/android/calendar/month/ba;->e:I

    .line 314
    :cond_2
    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/android/calendar/month/ba;->h:J

    .line 315
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/android/calendar/month/ba;->i:J

    .line 317
    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v5, Lcom/android/calendar/month/ba;->j:I

    .line 318
    const/16 v0, 0xa

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v5, Lcom/android/calendar/month/ba;->k:I

    .line 320
    iget v0, v5, Lcom/android/calendar/month/ba;->j:I

    iget v3, v5, Lcom/android/calendar/month/ba;->k:I

    if-eq v0, v3, :cond_7

    .line 322
    iput-boolean v1, v5, Lcom/android/calendar/month/ba;->m:Z

    .line 326
    :goto_2
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    iput-boolean v0, v5, Lcom/android/calendar/month/ba;->n:Z

    .line 329
    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 330
    const/16 v3, 0x8

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 331
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 332
    :cond_3
    iput-boolean v1, v5, Lcom/android/calendar/month/ba;->o:Z

    .line 337
    :goto_4
    const/16 v0, 0xf

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_a

    .line 344
    :goto_5
    const/16 v3, 0xc

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 345
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    .line 346
    const/16 v0, 0xd

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    .line 349
    :goto_6
    const/16 v3, 0xe

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v4, 0x1f4

    if-lt v3, v4, :cond_c

    move v3, v1

    .line 350
    :goto_7
    const/16 v4, 0x11

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_d

    move v4, v1

    .line 351
    :goto_8
    if-eqz v3, :cond_e

    if-nez v6, :cond_4

    if-eqz v0, :cond_e

    :cond_4
    if-nez v4, :cond_e

    move v0, v1

    :goto_9
    iput-boolean v0, v5, Lcom/android/calendar/month/ba;->q:Z

    .line 352
    if-eqz v3, :cond_f

    if-nez v4, :cond_f

    move v0, v1

    :goto_a
    iput-boolean v0, v5, Lcom/android/calendar/month/ba;->p:Z

    .line 354
    const/16 v0, 0x12

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/month/ba;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v5, Lcom/android/calendar/month/ba;->r:Z

    .line 356
    invoke-interface {p0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_10

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 357
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v5, Lcom/android/calendar/month/ba;->t:J

    .line 358
    iput-boolean v1, v5, Lcom/android/calendar/month/ba;->s:Z

    .line 363
    :goto_b
    return-object v5

    :cond_5
    move v0, v2

    .line 302
    goto/16 :goto_0

    .line 308
    :cond_6
    sget v0, Lcom/android/calendar/month/ba;->G:I

    iput v0, v5, Lcom/android/calendar/month/ba;->e:I

    goto/16 :goto_1

    .line 324
    :cond_7
    iput-boolean v2, v5, Lcom/android/calendar/month/ba;->m:Z

    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 326
    goto/16 :goto_3

    .line 334
    :cond_9
    iput-boolean v2, v5, Lcom/android/calendar/month/ba;->o:Z

    goto :goto_4

    .line 342
    :cond_a
    const-string v0, ""

    goto :goto_5

    :cond_b
    move v0, v2

    .line 346
    goto :goto_6

    :cond_c
    move v3, v2

    .line 349
    goto :goto_7

    :cond_d
    move v4, v2

    .line 350
    goto :goto_8

    :cond_e
    move v0, v2

    .line 351
    goto :goto_9

    :cond_f
    move v0, v2

    .line 352
    goto :goto_a

    .line 360
    :cond_10
    iput-boolean v2, v5, Lcom/android/calendar/month/ba;->s:Z

    goto :goto_b
.end method

.method public static a(Ljava/util/ArrayList;Landroid/content/Context;Landroid/database/Cursor;II)V
    .locals 2

    .prologue
    .line 228
    if-nez p0, :cond_1

    .line 229
    const-string v0, "MonthEventListItem"

    const-string v1, "buildMonthListItemsFromCursor: null cursor or null month events list!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :cond_0
    return-void

    .line 233
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 235
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 236
    const v1, 0x7f0f02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/calendar/month/ba;->F:Ljava/lang/String;

    .line 237
    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/ba;->G:I

    .line 239
    if-eqz p2, :cond_0

    .line 240
    :cond_2
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    invoke-static {p2}, Lcom/android/calendar/month/ba;->a(Landroid/database/Cursor;)Lcom/android/calendar/month/ba;

    move-result-object v0

    .line 242
    iget v1, v0, Lcom/android/calendar/month/ba;->j:I

    if-gt v1, p4, :cond_2

    iget v1, v0, Lcom/android/calendar/month/ba;->k:I

    if-lt v1, p3, :cond_2

    .line 245
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 257
    if-nez p0, :cond_1

    .line 258
    const-string v0, "MonthEventListItem"

    const-string v1, "buildMonthListItemsFromCursor: null cursor or null month events list!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :cond_0
    return-void

    .line 262
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 264
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 265
    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/calendar/month/ba;->F:Ljava/lang/String;

    .line 266
    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/calendar/month/ba;->G:I

    .line 269
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 270
    :cond_2
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 275
    :goto_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 276
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 278
    if-eqz p2, :cond_0

    .line 279
    :cond_3
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    invoke-static {p1, p2}, Lcom/android/calendar/month/ba;->a(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/calendar/month/ba;

    move-result-object v1

    .line 281
    iget-wide v2, v1, Lcom/android/calendar/month/ba;->y:J

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v2

    .line 282
    if-ne v2, p3, :cond_3

    .line 285
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 272
    :cond_4
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 401
    if-nez p0, :cond_0

    .line 402
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 405
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 407
    if-eqz p1, :cond_1

    .line 408
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 411
    :cond_1
    if-eqz p2, :cond_2

    .line 412
    invoke-virtual {p0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 414
    :cond_2
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 417
    if-nez p0, :cond_1

    .line 420
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "com.sec.android.app.sns3.facebook"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.sec.android.app.snsaccountfacebook.account_type"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 424
    const-string v0, "My task (personal)"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    const/4 v0, 0x0

    .line 427
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
