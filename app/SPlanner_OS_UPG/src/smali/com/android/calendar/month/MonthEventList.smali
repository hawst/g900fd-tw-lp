.class public Lcom/android/calendar/month/MonthEventList;
.super Landroid/widget/RelativeLayout;
.source "MonthEventList.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field protected static final a:Ljava/lang/String;

.field static p:I

.field private static u:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:F

.field private C:F

.field private D:Z

.field private E:Z

.field private F:I

.field private G:Lcom/android/calendar/gr;

.field b:Landroid/content/Context;

.field c:Lcom/android/calendar/al;

.field d:Lcom/android/calendar/month/at;

.field e:Landroid/os/Handler;

.field f:Landroid/widget/ListView;

.field g:Lcom/android/calendar/month/aj;

.field h:Landroid/view/View;

.field i:Landroid/widget/TextView;

.field j:Landroid/view/View;

.field k:Landroid/widget/ImageView;

.field l:Landroid/widget/ImageView;

.field m:Landroid/widget/TextView;

.field n:Landroid/widget/TextView;

.field protected o:Landroid/text/format/Time;

.field q:Lcom/android/calendar/month/k;

.field private r:Z

.field private s:Ljava/util/ArrayList;

.field private t:Z

.field private v:I

.field private w:J

.field private x:Landroid/view/GestureDetector;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/android/calendar/month/MonthEventList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/month/MonthEventList;->a:Ljava/lang/String;

    .line 108
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/month/MonthEventList;->u:Ljava/lang/String;

    .line 122
    const/16 v0, 0x12c

    sput v0, Lcom/android/calendar/month/MonthEventList;->p:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 139
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 76
    new-instance v0, Lcom/android/calendar/month/at;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/at;-><init>(Lcom/android/calendar/month/MonthEventList;Lcom/android/calendar/month/ao;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    .line 95
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->r:Z

    .line 103
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthEventList;->o:Landroid/text/format/Time;

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/month/MonthEventList;->w:J

    .line 124
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->A:Z

    .line 128
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->D:Z

    .line 130
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->E:Z

    .line 132
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/MonthEventList;->F:I

    .line 140
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthEventList;->a(Landroid/content/Context;)V

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 144
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    new-instance v0, Lcom/android/calendar/month/at;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/at;-><init>(Lcom/android/calendar/month/MonthEventList;Lcom/android/calendar/month/ao;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    .line 95
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->r:Z

    .line 103
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthEventList;->o:Landroid/text/format/Time;

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/month/MonthEventList;->w:J

    .line 124
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->A:Z

    .line 128
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->D:Z

    .line 130
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->E:Z

    .line 132
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/MonthEventList;->F:I

    .line 145
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthEventList;->a(Landroid/content/Context;)V

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    new-instance v0, Lcom/android/calendar/month/at;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/at;-><init>(Lcom/android/calendar/month/MonthEventList;Lcom/android/calendar/month/ao;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    .line 95
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->r:Z

    .line 103
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthEventList;->o:Landroid/text/format/Time;

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/month/MonthEventList;->w:J

    .line 124
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->A:Z

    .line 128
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->D:Z

    .line 130
    iput-boolean v2, p0, Lcom/android/calendar/month/MonthEventList;->E:Z

    .line 132
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/MonthEventList;->F:I

    .line 150
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthEventList;->a(Landroid/content/Context;)V

    .line 151
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/month/MonthEventList;F)F
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    return p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/android/calendar/month/MonthEventList;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/month/MonthEventList;Lcom/android/calendar/month/ba;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthEventList;->c(Lcom/android/calendar/month/ba;)V

    return-void
.end method

.method private a(Landroid/text/format/Time;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 667
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 668
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 670
    invoke-virtual {v1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    .line 671
    invoke-virtual {p1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-wide v4, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 673
    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/month/MonthEventList;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->A:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/month/MonthEventList;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/android/calendar/month/MonthEventList;->D:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/month/MonthEventList;F)F
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/android/calendar/month/MonthEventList;->C:F

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/month/MonthEventList;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/android/calendar/month/MonthEventList;->F:I

    return v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 677
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->b(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->y:Z

    .line 679
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->y:Z

    if-eqz v0, :cond_8

    .line 680
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v1

    .line 681
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/month/MonthEventList;->o:Landroid/text/format/Time;

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/h/c;->a(Landroid/content/res/Resources;Landroid/text/format/Time;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 682
    if-eqz v2, :cond_7

    .line 683
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 684
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->r:Z

    if-eqz v0, :cond_5

    .line 685
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->l:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02015b

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 689
    :goto_0
    invoke-virtual {v1}, Lcom/android/calendar/h/c;->f()Ljava/lang/String;

    move-result-object v0

    .line 690
    if-nez v0, :cond_0

    .line 691
    const-string v0, ""

    .line 694
    :cond_0
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->m:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 696
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->o:Landroid/text/format/Time;

    invoke-direct {p0, v0}, Lcom/android/calendar/month/MonthEventList;->a(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 697
    invoke-virtual {v1}, Lcom/android/calendar/h/c;->d()Ljava/lang/String;

    move-result-object v0

    .line 698
    if-eqz v0, :cond_1

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 699
    :cond_1
    const-string v0, ""

    .line 702
    :cond_2
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->n:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 703
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 708
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->j:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 716
    :goto_2
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->E:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->m:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    if-nez v2, :cond_4

    .line 719
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/android/calendar/h/c;->b(Landroid/content/Context;)V

    .line 721
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->e:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/month/as;

    invoke-direct {v1, p0}, Lcom/android/calendar/month/as;-><init>(Lcom/android/calendar/month/MonthEventList;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 737
    :cond_4
    :goto_3
    iput-boolean v5, p0, Lcom/android/calendar/month/MonthEventList;->E:Z

    .line 738
    return-void

    .line 687
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->l:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02015a

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 705
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 711
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->j:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 732
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->j:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 733
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->j:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method static synthetic c(Lcom/android/calendar/month/MonthEventList;)Landroid/view/GestureDetector;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->x:Landroid/view/GestureDetector;

    return-object v0
.end method

.method private c()V
    .locals 7

    .prologue
    const v6, 0x7f0f02d3

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 741
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->g:Lcom/android/calendar/month/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    if-nez v0, :cond_1

    .line 781
    :cond_0
    :goto_0
    return-void

    .line 745
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->o:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/calendar/month/MonthEventList;->o:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 746
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->g:Lcom/android/calendar/month/aj;

    invoke-virtual {v1, v0}, Lcom/android/calendar/month/aj;->a(I)V

    .line 748
    invoke-direct {p0}, Lcom/android/calendar/month/MonthEventList;->getProperListArray()Ljava/util/ArrayList;

    move-result-object v1

    .line 750
    if-nez v1, :cond_2

    .line 751
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 752
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 753
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthEventList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 757
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 758
    if-lez v2, :cond_3

    .line 759
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 760
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 767
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthEventList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 768
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->g:Lcom/android/calendar/month/aj;

    invoke-virtual {v3, v1}, Lcom/android/calendar/month/aj;->a(Ljava/util/ArrayList;)V

    .line 769
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->g:Lcom/android/calendar/month/aj;

    invoke-virtual {v3}, Lcom/android/calendar/month/aj;->notifyDataSetChanged()V

    .line 771
    if-lez v2, :cond_0

    .line 775
    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/month/MonthEventList;->a(ILjava/util/ArrayList;)V

    goto :goto_0

    .line 763
    :cond_3
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 764
    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private c(Lcom/android/calendar/month/ba;)V
    .locals 6

    .prologue
    .line 613
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 614
    iget-wide v2, p0, Lcom/android/calendar/month/MonthEventList;->w:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x258

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    iput-wide v0, p0, Lcom/android/calendar/month/MonthEventList;->w:J

    .line 622
    if-eqz p1, :cond_0

    .line 626
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    .line 627
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    const-string v1, "enterprise_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 628
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/RestrictionPolicy;->isShareListAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthEventList;->b(Lcom/android/calendar/month/ba;)V

    .line 632
    iget v0, p1, Lcom/android/calendar/month/ba;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 633
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->G:Lcom/android/calendar/gr;

    iget-wide v2, p1, Lcom/android/calendar/month/ba;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/gr;->a(J)V

    goto :goto_0

    .line 635
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->G:Lcom/android/calendar/gr;

    iget-wide v2, p1, Lcom/android/calendar/month/ba;->f:J

    const-wide/16 v4, -0x1

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/gr;->a(J)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/month/MonthEventList;)F
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/month/MonthEventList;)F
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/android/calendar/month/MonthEventList;->C:F

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/month/MonthEventList;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->D:Z

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/month/MonthEventList;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/android/calendar/month/MonthEventList;->getProperListArray()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private getProperListArray()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 820
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/month/MonthEventList;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/android/calendar/month/MonthEventList;->v:I

    return v0
.end method

.method static synthetic i(Lcom/android/calendar/month/MonthEventList;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/android/calendar/month/MonthEventList;->b()V

    return-void
.end method


# virtual methods
.method a(ILjava/util/ArrayList;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 785
    sget-boolean v0, Lcom/android/calendar/month/aj;->f:Z

    if-nez v0, :cond_1

    .line 786
    sput-boolean v8, Lcom/android/calendar/month/aj;->f:Z

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 790
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 791
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 793
    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 795
    if-eq v1, p1, :cond_2

    .line 796
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 797
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    .line 802
    :cond_2
    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 805
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 806
    :goto_1
    if-ge v1, v3, :cond_4

    .line 807
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/ba;

    .line 808
    iget-wide v6, v0, Lcom/android/calendar/month/ba;->h:J

    cmp-long v0, v6, v4

    if-lez v0, :cond_3

    move v0, v1

    .line 814
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 815
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    .line 806
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method final a(Landroid/content/Context;)V
    .locals 9

    .prologue
    const v8, 0x7f120242

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 154
    iput-object p1, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    .line 155
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    .line 157
    iput-boolean v6, p0, Lcom/android/calendar/month/MonthEventList;->E:Z

    .line 159
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthEventList;->e:Landroid/os/Handler;

    .line 161
    invoke-virtual {p0}, Lcom/android/calendar/month/MonthEventList;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 163
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->t:Z

    .line 166
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->A:Z

    .line 168
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/month/MonthEventList;->F:I

    .line 170
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 172
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 173
    const v3, 0x7f12006d

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 175
    if-eqz v1, :cond_0

    instance-of v3, v1, Lcom/android/calendar/month/k;

    if-eqz v3, :cond_0

    .line 176
    check-cast v1, Lcom/android/calendar/month/k;

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    .line 180
    :cond_0
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    new-instance v4, Lcom/android/calendar/month/au;

    invoke-direct {v4, p0}, Lcom/android/calendar/month/au;-><init>(Lcom/android/calendar/month/MonthEventList;)V

    invoke-direct {v1, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->x:Landroid/view/GestureDetector;

    .line 182
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 184
    const v3, 0x7f040074

    invoke-virtual {v1, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 187
    new-instance v1, Lcom/android/calendar/month/aj;

    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/android/calendar/month/aj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->g:Lcom/android/calendar/month/aj;

    .line 188
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->g:Lcom/android/calendar/month/aj;

    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v1, v3}, Lcom/android/calendar/month/aj;->a(Landroid/content/Context;)V

    .line 190
    const v1, 0x7f120247

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    .line 191
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/android/calendar/month/MonthEventList;->g:Lcom/android/calendar/month/aj;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 192
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 194
    const v1, 0x7f1200ab

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->i:Landroid/widget/TextView;

    .line 196
    const v1, 0x7f120171

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    .line 198
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    new-instance v3, Lcom/android/calendar/month/ao;

    invoke-direct {v3, p0}, Lcom/android/calendar/month/ao;-><init>(Lcom/android/calendar/month/MonthEventList;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 207
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    new-instance v3, Lcom/android/calendar/month/ap;

    invoke-direct {v3, p0}, Lcom/android/calendar/month/ap;-><init>(Lcom/android/calendar/month/MonthEventList;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 267
    const v1, 0x7f0b0053

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 268
    iget-object v2, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 269
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 271
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 274
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    new-instance v2, Lcom/android/calendar/month/aq;

    invoke-direct {v2, p0}, Lcom/android/calendar/month/aq;-><init>(Lcom/android/calendar/month/MonthEventList;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 348
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    new-instance v2, Lcom/android/calendar/month/ar;

    invoke-direct {v2, p0}, Lcom/android/calendar/month/ar;-><init>(Lcom/android/calendar/month/MonthEventList;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 356
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->s(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/MonthEventList;->r:Z

    .line 357
    invoke-static {}, Lcom/android/calendar/dz;->b()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/MonthEventList;->y:Z

    .line 358
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/month/MonthEventList;->z:Z

    .line 359
    iget-boolean v1, p0, Lcom/android/calendar/month/MonthEventList;->y:Z

    if-eqz v1, :cond_3

    .line 360
    invoke-virtual {p0, v8}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->j:Landroid/view/View;

    .line 361
    const v1, 0x7f120243

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->k:Landroid/widget/ImageView;

    .line 362
    const v1, 0x7f120244

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->l:Landroid/widget/ImageView;

    .line 363
    const v1, 0x7f120245

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->m:Landroid/widget/TextView;

    .line 364
    const v1, 0x7f120246

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->n:Landroid/widget/TextView;

    .line 365
    iget-boolean v1, p0, Lcom/android/calendar/month/MonthEventList;->z:Z

    if-eqz v1, :cond_1

    .line 366
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->m:Landroid/widget/TextView;

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 368
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/month/MonthEventList;->b()V

    .line 377
    :cond_2
    :goto_0
    new-instance v1, Lcom/android/calendar/gr;

    invoke-direct {v1, v0, v5}, Lcom/android/calendar/gr;-><init>(Landroid/app/Activity;Z)V

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->G:Lcom/android/calendar/gr;

    .line 380
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->h:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 382
    return-void

    .line 371
    :cond_3
    invoke-virtual {p0, v8}, Lcom/android/calendar/month/MonthEventList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/MonthEventList;->j:Landroid/view/View;

    .line 372
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->j:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 373
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->j:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/month/ba;)V
    .locals 17

    .prologue
    .line 581
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    .line 583
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 584
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/month/ba;->l:Z

    if-nez v3, :cond_0

    .line 585
    iput-object v2, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 587
    :cond_0
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/month/ba;->h:J

    invoke-virtual {v6, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 588
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/month/ba;->l:Z

    if-eqz v3, :cond_1

    .line 589
    iput-object v2, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 591
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v6, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 593
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    .line 594
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/month/ba;->l:Z

    if-nez v3, :cond_2

    .line 595
    iput-object v2, v7, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 597
    :cond_2
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/month/ba;->i:J

    invoke-virtual {v7, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 598
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/android/calendar/month/ba;->l:Z

    if-eqz v3, :cond_3

    .line 599
    iput-object v2, v7, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 601
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 603
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    const-wide/16 v4, 0x10

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/month/ba;->f:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    .line 605
    return-void
.end method

.method public a(Lcom/android/calendar/month/ba;Z)V
    .locals 17

    .prologue
    .line 547
    if-nez p1, :cond_0

    .line 578
    :goto_0
    return-void

    .line 551
    :cond_0
    const/4 v14, 0x0

    .line 556
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/month/ba;->c:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 557
    const/4 v14, 0x1

    .line 559
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    .line 561
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 562
    iput-object v2, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 563
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/month/ba;->h:J

    invoke-virtual {v6, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 564
    const/4 v3, 0x1

    invoke-virtual {v6, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 566
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    .line 567
    iput-object v2, v7, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 568
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/calendar/month/ba;->i:J

    invoke-virtual {v7, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 569
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 571
    if-nez p2, :cond_2

    .line 572
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    const-wide/16 v4, 0x8

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/month/ba;->f:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0

    .line 575
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    const-wide/32 v4, 0x200000

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/android/calendar/month/ba;->f:J

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    goto :goto_0
.end method

.method public b(Lcom/android/calendar/month/ba;)V
    .locals 4

    .prologue
    .line 640
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 641
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 643
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 644
    iget-boolean v2, p1, Lcom/android/calendar/month/ba;->l:Z

    if-nez v2, :cond_0

    .line 645
    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 647
    :cond_0
    iget-wide v2, p1, Lcom/android/calendar/month/ba;->h:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 648
    iget-boolean v2, p1, Lcom/android/calendar/month/ba;->l:Z

    if-eqz v2, :cond_1

    .line 649
    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 652
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->G:Lcom/android/calendar/gr;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/gr;->c(J)V

    .line 653
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/high16 v9, -0x40800000    # -1.0f

    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 868
    if-nez p1, :cond_1

    move v8, v7

    .line 937
    :cond_0
    :goto_0
    return v8

    .line 871
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    if-nez v0, :cond_2

    .line 872
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto :goto_0

    .line 874
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    if-nez v0, :cond_3

    .line 875
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto :goto_0

    .line 877
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->A:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/calendar/month/MonthEventList;->F:I

    if-eq v0, v4, :cond_4

    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->l()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->m()Z

    move-result v0

    if-nez v0, :cond_5

    .line 878
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto :goto_0

    .line 881
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    invoke-virtual {v0}, Lcom/android/calendar/month/k;->r()Z

    move-result v1

    .line 882
    if-eqz v1, :cond_6

    .line 883
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto :goto_0

    .line 888
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 890
    packed-switch v2, :pswitch_data_0

    move v0, v8

    .line 932
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->x:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 934
    if-ne v2, v4, :cond_8

    if-nez v0, :cond_0

    .line 937
    :cond_8
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto :goto_0

    .line 892
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    move v0, v8

    .line 894
    goto :goto_1

    .line 897
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/MonthEventList;->C:F

    .line 899
    iget v0, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    cmpg-float v0, v0, v5

    if-ltz v0, :cond_9

    iget v0, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    iget v3, p0, Lcom/android/calendar/month/MonthEventList;->C:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_a

    :cond_9
    move v0, v7

    .line 903
    :goto_2
    if-nez v1, :cond_7

    .line 904
    iget v1, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    cmpl-float v1, v1, v5

    if-ltz v1, :cond_7

    iget v1, p0, Lcom/android/calendar/month/MonthEventList;->C:F

    iget v3, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    sub-float/2addr v1, v3

    sget v3, Lcom/android/calendar/month/MonthEventList;->p:I

    neg-int v3, v3

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gez v1, :cond_7

    .line 905
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->q:Lcom/android/calendar/month/k;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/month/k;->a(J)V

    .line 908
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 911
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 913
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 916
    iput-boolean v7, p0, Lcom/android/calendar/month/MonthEventList;->D:Z

    .line 917
    iput v9, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    goto/16 :goto_0

    .line 925
    :pswitch_2
    iput-boolean v7, p0, Lcom/android/calendar/month/MonthEventList;->D:Z

    .line 926
    iput v9, p0, Lcom/android/calendar/month/MonthEventList;->B:F

    move v0, v8

    .line 927
    goto :goto_1

    :cond_a
    move v0, v8

    goto :goto_2

    .line 890
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCounts()I
    .locals 1

    .prologue
    .line 824
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 825
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    .line 827
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 8

    .prologue
    const v7, 0x7f0f0187

    const v6, 0x7f0f0143

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 426
    if-nez p3, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 430
    :cond_1
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    iput v0, p0, Lcom/android/calendar/month/MonthEventList;->v:I

    .line 432
    invoke-direct {p0}, Lcom/android/calendar/month/MonthEventList;->getProperListArray()Ljava/util/ArrayList;

    move-result-object v0

    .line 434
    if-eqz v0, :cond_0

    iget v1, p0, Lcom/android/calendar/month/MonthEventList;->v:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/calendar/month/MonthEventList;->v:I

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 438
    iget v1, p0, Lcom/android/calendar/month/MonthEventList;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/ba;

    .line 440
    iget-object v1, v0, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 441
    iget-object v1, v0, Lcom/android/calendar/month/ba;->d:Ljava/lang/String;

    sput-object v1, Lcom/android/calendar/month/MonthEventList;->u:Ljava/lang/String;

    .line 444
    iget v1, v0, Lcom/android/calendar/month/ba;->c:I

    if-ne v1, v5, :cond_5

    .line 445
    iget-boolean v1, v0, Lcom/android/calendar/month/ba;->E:Z

    if-eqz v1, :cond_2

    .line 446
    const/4 v1, 0x5

    invoke-interface {p1, v3, v1, v3, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 447
    iget-object v2, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 450
    :cond_2
    invoke-static {}, Lcom/android/calendar/dz;->F()Z

    move-result v1

    if-nez v1, :cond_3

    .line 451
    const/4 v1, 0x6

    const v2, 0x7f0f01d6

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 452
    iget-object v2, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 455
    :cond_3
    iget-boolean v0, v0, Lcom/android/calendar/month/ba;->E:Z

    if-eqz v0, :cond_4

    .line 456
    const/4 v0, 0x7

    invoke-interface {p1, v3, v0, v3, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 457
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 460
    :cond_4
    const/16 v0, 0x8

    const v1, 0x7f0f03d8

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 461
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 463
    :cond_5
    iget v1, v0, Lcom/android/calendar/month/ba;->c:I

    if-ne v1, v4, :cond_0

    .line 464
    iget-boolean v1, v0, Lcom/android/calendar/month/ba;->q:Z

    if-eqz v1, :cond_6

    .line 465
    invoke-interface {p1, v3, v4, v3, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 466
    iget-object v2, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 469
    :cond_6
    invoke-static {}, Lcom/android/calendar/dz;->F()Z

    move-result v1

    if-nez v1, :cond_7

    .line 470
    const v1, 0x7f0f01d6

    invoke-interface {p1, v3, v5, v3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 471
    iget-object v2, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 474
    :cond_7
    iget-boolean v0, v0, Lcom/android/calendar/month/ba;->p:Z

    if-eqz v0, :cond_8

    .line 475
    const/4 v0, 0x3

    invoke-interface {p1, v3, v0, v3, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 476
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 479
    :cond_8
    const/4 v0, 0x4

    const v1, 0x7f0f03d8

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 480
    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->d:Lcom/android/calendar/month/at;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 18

    .prologue
    .line 387
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 388
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 390
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/month/MonthEventList;->getProperListArray()Ljava/util/ArrayList;

    move-result-object v2

    .line 392
    if-nez v2, :cond_0

    .line 413
    :goto_0
    return-void

    .line 396
    :cond_0
    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/android/calendar/month/ba;

    .line 398
    iget v2, v4, Landroid/graphics/Rect;->left:I

    add-int/lit8 v12, v2, 0xa

    .line 399
    iget v2, v4, Landroid/graphics/Rect;->top:I

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v4

    div-int/lit8 v13, v2, 0x2

    .line 401
    iget v2, v3, Lcom/android/calendar/month/ba;->c:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    const-wide/16 v4, 0x2

    iget-wide v6, v3, Lcom/android/calendar/month/ba;->f:J

    iget-wide v8, v3, Lcom/android/calendar/month/ba;->h:J

    iget-wide v10, v3, Lcom/android/calendar/month/ba;->i:J

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_0

    .line 405
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/month/MonthEventList;->t:Z

    if-eqz v2, :cond_2

    .line 406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    const-wide/16 v4, 0x2

    iget-wide v6, v3, Lcom/android/calendar/month/ba;->f:J

    const-wide/16 v8, -0x1

    iget-wide v10, v3, Lcom/android/calendar/month/ba;->x:J

    const-wide/16 v14, -0x1

    const/16 v16, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v16}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJZ)V

    goto :goto_0

    .line 409
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthEventList;->c:Lcom/android/calendar/al;

    const-wide/16 v4, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-wide v8, v3, Lcom/android/calendar/month/ba;->f:J

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v11}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    goto :goto_0
.end method

.method public setEventsAndTasks(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 660
    iput-object p1, p0, Lcom/android/calendar/month/MonthEventList;->s:Ljava/util/ArrayList;

    .line 662
    invoke-direct {p0}, Lcom/android/calendar/month/MonthEventList;->c()V

    .line 663
    invoke-direct {p0}, Lcom/android/calendar/month/MonthEventList;->b()V

    .line 664
    return-void
.end method

.method public setLocationOn(Z)V
    .locals 3

    .prologue
    .line 941
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->l:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 951
    :goto_0
    return-void

    .line 944
    :cond_0
    iput-boolean p1, p0, Lcom/android/calendar/month/MonthEventList;->r:Z

    .line 945
    iget-boolean v0, p0, Lcom/android/calendar/month/MonthEventList;->r:Z

    if-eqz v0, :cond_1

    .line 946
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->l:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02015b

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 949
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->l:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/month/MonthEventList;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02015a

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setSelectedDay(Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/android/calendar/month/MonthEventList;->o:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 657
    return-void
.end method
