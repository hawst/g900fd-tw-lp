.class final enum Lcom/android/calendar/month/ad;
.super Ljava/lang/Enum;
.source "MonthByWeekFragment.java"


# static fields
.field public static final enum a:Lcom/android/calendar/month/ad;

.field public static final enum b:Lcom/android/calendar/month/ad;

.field public static final enum c:Lcom/android/calendar/month/ad;

.field private static final synthetic d:[Lcom/android/calendar/month/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1367
    new-instance v0, Lcom/android/calendar/month/ad;

    const-string v1, "NOT_SCROLLED"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/month/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/ad;->a:Lcom/android/calendar/month/ad;

    new-instance v0, Lcom/android/calendar/month/ad;

    const-string v1, "SCROLL_UP"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/month/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/ad;->b:Lcom/android/calendar/month/ad;

    new-instance v0, Lcom/android/calendar/month/ad;

    const-string v1, "SCROLL_DOWN"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/month/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/month/ad;->c:Lcom/android/calendar/month/ad;

    .line 1366
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/calendar/month/ad;

    sget-object v1, Lcom/android/calendar/month/ad;->a:Lcom/android/calendar/month/ad;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/month/ad;->b:Lcom/android/calendar/month/ad;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/month/ad;->c:Lcom/android/calendar/month/ad;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/month/ad;->d:[Lcom/android/calendar/month/ad;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1366
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/month/ad;
    .locals 1

    .prologue
    .line 1366
    const-class v0, Lcom/android/calendar/month/ad;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/month/ad;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/month/ad;
    .locals 1

    .prologue
    .line 1366
    sget-object v0, Lcom/android/calendar/month/ad;->d:[Lcom/android/calendar/month/ad;

    invoke-virtual {v0}, [Lcom/android/calendar/month/ad;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/month/ad;

    return-object v0
.end method
