.class Lcom/android/calendar/month/cj;
.super Ljava/lang/Object;
.source "YearFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/ci;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/ci;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 68
    iget-object v0, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-virtual {v0}, Lcom/android/calendar/month/ci;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-virtual {v0}, Lcom/android/calendar/month/ci;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-static {v1}, Lcom/android/calendar/month/ci;->a(Lcom/android/calendar/month/ci;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-static {v1}, Lcom/android/calendar/month/ci;->b(Lcom/android/calendar/month/ci;)Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-static {v0}, Lcom/android/calendar/month/ci;->b(Lcom/android/calendar/month/ci;)Landroid/text/format/Time;

    move-result-object v0

    iget-boolean v0, v0, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-static {v0}, Lcom/android/calendar/month/ci;->b(Lcom/android/calendar/month/ci;)Landroid/text/format/Time;

    move-result-object v0

    iput v2, v0, Landroid/text/format/Time;->hour:I

    .line 75
    iget-object v0, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-static {v0}, Lcom/android/calendar/month/ci;->b(Lcom/android/calendar/month/ci;)Landroid/text/format/Time;

    move-result-object v0

    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 76
    iget-object v0, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-static {v0}, Lcom/android/calendar/month/ci;->b(Lcom/android/calendar/month/ci;)Landroid/text/format/Time;

    move-result-object v0

    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 77
    iget-object v0, p0, Lcom/android/calendar/month/cj;->a:Lcom/android/calendar/month/ci;

    invoke-static {v0}, Lcom/android/calendar/month/ci;->b(Lcom/android/calendar/month/ci;)Landroid/text/format/Time;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    goto :goto_0
.end method
