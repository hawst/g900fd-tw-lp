.class Lcom/android/calendar/month/bz;
.super Ljava/lang/Object;
.source "SimpleDayPickerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/month/by;


# direct methods
.method constructor <init>(Lcom/android/calendar/month/by;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/android/calendar/month/bz;->a:Lcom/android/calendar/month/by;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 181
    iget-object v0, p0, Lcom/android/calendar/month/bz;->a:Lcom/android/calendar/month/by;

    iget-object v0, v0, Lcom/android/calendar/month/by;->V:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/bz;->a:Lcom/android/calendar/month/by;

    iget-object v0, v0, Lcom/android/calendar/month/by;->V:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 187
    :goto_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 189
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 191
    iput v4, v0, Landroid/text/format/Time;->hour:I

    .line 192
    iput v4, v0, Landroid/text/format/Time;->minute:I

    .line 193
    iput v4, v0, Landroid/text/format/Time;->second:I

    .line 194
    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 195
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    sub-long/2addr v0, v2

    .line 196
    iget-object v2, p0, Lcom/android/calendar/month/bz;->a:Lcom/android/calendar/month/by;

    iget-object v2, v2, Lcom/android/calendar/month/by;->W:Landroid/os/Handler;

    invoke-virtual {v2, p0, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 198
    iget-object v0, p0, Lcom/android/calendar/month/bz;->a:Lcom/android/calendar/month/by;

    iget-object v0, v0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/android/calendar/month/bz;->a:Lcom/android/calendar/month/by;

    iget-object v0, v0, Lcom/android/calendar/month/by;->aa:Lcom/android/calendar/month/ce;

    invoke-virtual {v0}, Lcom/android/calendar/month/ce;->notifyDataSetChanged()V

    .line 201
    :cond_0
    return-void

    .line 184
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/month/bz;->a:Lcom/android/calendar/month/by;

    iget-object v1, v1, Lcom/android/calendar/month/by;->af:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
