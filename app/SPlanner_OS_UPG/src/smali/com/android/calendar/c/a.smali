.class public Lcom/android/calendar/c/a;
.super Ljava/lang/Object;
.source "BezelManager.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/ArrayList;

.field private c:Lcom/samsung/android/quickconnect/QuickConnectManager;

.field private d:Lcom/android/calendar/gr;

.field private e:Lcom/android/calendar/c/d;

.field private f:Landroid/app/Activity;

.field private g:Z

.field private final h:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/c/a;-><init>(Landroid/app/Activity;Lcom/android/calendar/c/d;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/android/calendar/c/d;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-class v0, Lcom/android/calendar/c/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c/a;->a:Ljava/lang/String;

    .line 31
    iput-boolean v2, p0, Lcom/android/calendar/c/a;->g:Z

    .line 33
    new-instance v0, Lcom/android/calendar/c/b;

    invoke-direct {v0, p0}, Lcom/android/calendar/c/b;-><init>(Lcom/android/calendar/c/a;)V

    iput-object v0, p0, Lcom/android/calendar/c/a;->h:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 51
    iput-object p1, p0, Lcom/android/calendar/c/a;->f:Landroid/app/Activity;

    .line 52
    iput-object p2, p0, Lcom/android/calendar/c/a;->e:Lcom/android/calendar/c/d;

    .line 53
    const-string v0, "quickconnect"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v0, p0, Lcom/android/calendar/c/a;->c:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 54
    new-instance v0, Lcom/android/calendar/gr;

    iget-object v1, p0, Lcom/android/calendar/c/a;->f:Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/gr;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/android/calendar/c/a;->d:Lcom/android/calendar/gr;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/c/a;)Lcom/android/calendar/c/d;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/android/calendar/c/a;->e:Lcom/android/calendar/c/d;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/c/a;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/android/calendar/c/a;->d()V

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/c/a;->d:Lcom/android/calendar/gr;

    invoke-direct {p0}, Lcom/android/calendar/c/a;->e()[Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/android/calendar/gu;->c:Lcom/android/calendar/gu;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/calendar/gr;->a([Ljava/lang/Long;Ljava/lang/Runnable;Lcom/android/calendar/gu;)V

    .line 59
    return-void
.end method

.method private e()[Ljava/lang/Long;
    .locals 6

    .prologue
    .line 62
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/calendar/c/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 65
    iget-object v0, p0, Lcom/android/calendar/c/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/c/c;

    .line 66
    iget-object v1, v0, Lcom/android/calendar/c/c;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 67
    iget-boolean v0, v0, Lcom/android/calendar/c/c;->b:Z

    if-eqz v0, :cond_1

    .line 68
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v4, -0x1

    mul-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 69
    :goto_1
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Long;

    .line 73
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Long;

    .line 74
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/c/a;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/c/a;->b:Ljava/util/ArrayList;

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/c/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/c/c;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/calendar/c/a;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/c/a;->b:Ljava/util/ArrayList;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/c/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/android/calendar/c/a;->g:Z

    if-eqz v0, :cond_0

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/c/a;->c:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/android/calendar/c/a;->h:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/c/a;->g:Z

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/c/a;->g:Z

    if-nez v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/c/a;->c:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/c/a;->h:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/android/calendar/c/a;->c:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/android/calendar/c/a;->h:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 110
    iget-object v0, p0, Lcom/android/calendar/c/a;->c:Lcom/samsung/android/quickconnect/QuickConnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/quickconnect/QuickConnectManager;->terminate()V

    .line 111
    iput-object v2, p0, Lcom/android/calendar/c/a;->c:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 114
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/c/a;->g:Z

    .line 115
    iput-object v2, p0, Lcom/android/calendar/c/a;->e:Lcom/android/calendar/c/d;

    goto :goto_0
.end method
