.class final Lcom/android/calendar/fq;
.super Landroid/widget/Filter;
.source "RecipientAdapter.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/fk;

.field private final b:Lcom/android/calendar/fs;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/fk;Lcom/android/calendar/fs;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 340
    iput-object p2, p0, Lcom/android/calendar/fq;->b:Lcom/android/calendar/fs;

    .line 341
    return-void
.end method


# virtual methods
.method public declared-synchronized a()I
    .locals 1

    .prologue
    .line 348
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/calendar/fq;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 344
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/android/calendar/fq;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    monitor-exit p0

    return-void

    .line 344
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 357
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 358
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 359
    const/4 v2, 0x0

    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 361
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 362
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 369
    :try_start_0
    iget-object v3, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v3}, Lcom/android/calendar/fk;->c(Lcom/android/calendar/fk;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/calendar/hj;->f(Landroid/content/Context;)Z

    move-result v3

    .line 370
    if-eqz v3, :cond_1

    .line 371
    iget-object v3, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-virtual {p0}, Lcom/android/calendar/fq;->a()I

    move-result v4

    iget-object v5, p0, Lcom/android/calendar/fq;->b:Lcom/android/calendar/fs;

    iget-wide v6, v5, Lcom/android/calendar/fs;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v3, p1, v4, v5}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;

    move-result-object v1

    .line 376
    :goto_0
    if-eqz v1, :cond_2

    .line 377
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 378
    new-instance v3, Lcom/android/calendar/fu;

    invoke-direct {v3, v1}, Lcom/android/calendar/fu;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 382
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 383
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 373
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-virtual {p0}, Lcom/android/calendar/fq;->a()I

    move-result v4

    const/4 v5, 0x0

    invoke-static {v3, p1, v4, v5}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 382
    :cond_2
    if-eqz v1, :cond_3

    .line 383
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 386
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 387
    iput-object v2, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 388
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 397
    :cond_4
    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 406
    iget-object v0, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v0}, Lcom/android/calendar/fk;->d(Lcom/android/calendar/fk;)Lcom/android/calendar/fp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/fp;->b()V

    .line 411
    iget-object v0, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v0}, Lcom/android/calendar/fk;->e(Lcom/android/calendar/fk;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 412
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_1

    .line 414
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 417
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/fu;

    .line 418
    iget-object v0, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    iget-object v2, p0, Lcom/android/calendar/fq;->b:Lcom/android/calendar/fs;

    iget-wide v2, v2, Lcom/android/calendar/fs;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    iget-object v3, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v3}, Lcom/android/calendar/fk;->f(Lcom/android/calendar/fk;)Ljava/util/LinkedHashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v4}, Lcom/android/calendar/fk;->g(Lcom/android/calendar/fk;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v5}, Lcom/android/calendar/fk;->h(Lcom/android/calendar/fk;)Ljava/util/Set;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;Lcom/android/calendar/fu;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V

    goto :goto_0

    :cond_0
    move v2, v6

    goto :goto_1

    .line 424
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v0}, Lcom/android/calendar/fk;->i(Lcom/android/calendar/fk;)I

    .line 425
    iget-object v0, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v0}, Lcom/android/calendar/fk;->j(Lcom/android/calendar/fk;)I

    move-result v0

    if-lez v0, :cond_2

    .line 430
    iget-object v0, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v0}, Lcom/android/calendar/fk;->d(Lcom/android/calendar/fk;)Lcom/android/calendar/fp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/fp;->a()V

    .line 435
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    iget-object v1, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    iget-object v2, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v2}, Lcom/android/calendar/fk;->f(Lcom/android/calendar/fk;)Ljava/util/LinkedHashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v3}, Lcom/android/calendar/fk;->g(Lcom/android/calendar/fk;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/fq;->a:Lcom/android/calendar/fk;

    invoke-static {v4}, Lcom/android/calendar/fk;->h(Lcom/android/calendar/fk;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v1, v6, v2, v3, v4}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/fk;->b(Lcom/android/calendar/fk;Ljava/util/List;)V

    .line 437
    return-void
.end method
