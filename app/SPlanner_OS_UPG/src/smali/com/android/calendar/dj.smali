.class public Lcom/android/calendar/dj;
.super Ljava/lang/Object;
.source "EventLoader.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/os/Handler;

.field private c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private d:Ljava/util/concurrent/LinkedBlockingQueue;

.field private e:Lcom/android/calendar/dn;

.field private f:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/dj;->b:Landroid/os/Handler;

    .line 36
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/dj;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 209
    iput-object p1, p0, Lcom/android/calendar/dj;->a:Landroid/content/Context;

    .line 210
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/dj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 211
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/dj;->f:Landroid/content/ContentResolver;

    .line 212
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/dj;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/calendar/dj;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/dj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/calendar/dj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/dj;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/calendar/dj;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 218
    new-instance v0, Lcom/android/calendar/dn;

    iget-object v1, p0, Lcom/android/calendar/dj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0, v1, p0}, Lcom/android/calendar/dn;-><init>(Ljava/util/concurrent/LinkedBlockingQueue;Lcom/android/calendar/dj;)V

    iput-object v0, p0, Lcom/android/calendar/dj;->e:Lcom/android/calendar/dn;

    .line 219
    iget-object v0, p0, Lcom/android/calendar/dj;->e:Lcom/android/calendar/dn;

    invoke-virtual {v0}, Lcom/android/calendar/dn;->start()V

    .line 220
    return-void
.end method

.method public a(ILjava/util/ArrayList;ILjava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7

    .prologue
    .line 244
    iget-object v0, p0, Lcom/android/calendar/dj;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    .line 247
    new-instance v0, Lcom/android/calendar/dl;

    move v2, p3

    move v3, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/dl;-><init>(IIILjava/util/ArrayList;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 251
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/dj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v0

    .line 256
    const-string v0, "Cal"

    const-string v1, "loadEventsInBackground() interrupted!"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/calendar/dj;->e:Lcom/android/calendar/dn;

    invoke-virtual {v0}, Lcom/android/calendar/dn;->a()V

    .line 227
    return-void
.end method
