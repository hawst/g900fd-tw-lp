.class Lcom/android/calendar/bp;
.super Landroid/app/DialogFragment;
.source "CopyEventToSolarHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/bm;


# direct methods
.method private constructor <init>(Lcom/android/calendar/bm;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/android/calendar/bp;->a:Lcom/android/calendar/bm;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/bm;Lcom/android/calendar/bn;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/calendar/bp;-><init>(Lcom/android/calendar/bm;)V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/android/calendar/bp;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 115
    const v1, 0x7f04003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 116
    const v1, 0x7f12007b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 117
    const v2, 0x7f120143

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 119
    const v3, 0x7f12007e

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 122
    new-instance v4, Lcom/android/calendar/bq;

    invoke-direct {v4, p0, v3}, Lcom/android/calendar/bq;-><init>(Lcom/android/calendar/bp;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/bp;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 132
    invoke-virtual {p0}, Lcom/android/calendar/bp;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0f0399

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    const v1, 0x7f0f02e1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02e3

    new-instance v4, Lcom/android/calendar/br;

    invoke-direct {v4, p0, v3}, Lcom/android/calendar/br;-><init>(Lcom/android/calendar/bp;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 146
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/android/calendar/bp;->dismiss()V

    .line 156
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 157
    return-void
.end method
