.class public Lcom/android/calendar/c;
.super Landroid/app/Dialog;
.source "AirViewHelpDialog.java"


# static fields
.field private static h:Lcom/android/calendar/c;


# instance fields
.field private a:Landroid/view/animation/Animation;

.field private b:Landroid/view/animation/Animation;

.field private c:Landroid/view/animation/Animation;

.field private d:Landroid/view/animation/Animation;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/app/Activity;

.field private i:Landroid/widget/Toast;

.field private j:Z

.field private k:Landroid/view/animation/Animation$AnimationListener;

.field private l:Landroid/view/animation/Animation$AnimationListener;

.field private m:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/c;->h:Lcom/android/calendar/c;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/16 v1, 0x400

    const/4 v2, 0x0

    .line 34
    const v0, 0x7f10006d

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 31
    iput-boolean v2, p0, Lcom/android/calendar/c;->j:Z

    .line 122
    new-instance v0, Lcom/android/calendar/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/d;-><init>(Lcom/android/calendar/c;)V

    iput-object v0, p0, Lcom/android/calendar/c;->k:Landroid/view/animation/Animation$AnimationListener;

    .line 137
    new-instance v0, Lcom/android/calendar/e;

    invoke-direct {v0, p0}, Lcom/android/calendar/e;-><init>(Lcom/android/calendar/c;)V

    iput-object v0, p0, Lcom/android/calendar/c;->l:Landroid/view/animation/Animation$AnimationListener;

    .line 152
    new-instance v0, Lcom/android/calendar/f;

    invoke-direct {v0, p0}, Lcom/android/calendar/f;-><init>(Lcom/android/calendar/c;)V

    iput-object v0, p0, Lcom/android/calendar/c;->m:Landroid/view/animation/Animation$AnimationListener;

    .line 35
    invoke-virtual {p0}, Lcom/android/calendar/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 36
    invoke-direct {p0, p1}, Lcom/android/calendar/c;->a(Landroid/content/Context;)V

    .line 37
    invoke-virtual {p0}, Lcom/android/calendar/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 39
    const v0, 0x7f0f0009

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->i:Landroid/widget/Toast;

    .line 40
    invoke-virtual {p0}, Lcom/android/calendar/c;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->g:Landroid/app/Activity;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/c;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/android/calendar/c;->c:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public static a()Lcom/android/calendar/c;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/android/calendar/c;->h:Lcom/android/calendar/c;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 111
    const v0, 0x7f050009

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->a:Landroid/view/animation/Animation;

    .line 112
    iget-object v0, p0, Lcom/android/calendar/c;->a:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/calendar/c;->m:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 113
    const v0, 0x7f050007

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->b:Landroid/view/animation/Animation;

    .line 114
    iget-object v0, p0, Lcom/android/calendar/c;->b:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/calendar/c;->k:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 115
    const v0, 0x7f050008

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->c:Landroid/view/animation/Animation;

    .line 116
    iget-object v0, p0, Lcom/android/calendar/c;->c:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/android/calendar/c;->l:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 118
    const/high16 v0, 0x7f050000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->d:Landroid/view/animation/Animation;

    .line 119
    iget-object v0, p0, Lcom/android/calendar/c;->d:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 120
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/android/calendar/c;->e:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/c;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/android/calendar/c;->b:Landroid/view/animation/Animation;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/android/calendar/c;->j:Z

    .line 44
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const v3, 0x7f0f0234

    .line 85
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 86
    const v0, 0x7f12004a

    invoke-virtual {p0, v0}, Lcom/android/calendar/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->e:Landroid/view/View;

    .line 87
    const v0, 0x7f120049

    invoke-virtual {p0, v0}, Lcom/android/calendar/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->f:Landroid/view/View;

    .line 88
    iget-object v0, p0, Lcom/android/calendar/c;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/c;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 89
    iget-object v0, p0, Lcom/android/calendar/c;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/c;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 90
    iget-object v0, p0, Lcom/android/calendar/c;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    .line 91
    iget-object v0, p0, Lcom/android/calendar/c;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    .line 92
    const v0, 0x7f12004b

    invoke-virtual {p0, v0}, Lcom/android/calendar/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 94
    invoke-virtual {p0}, Lcom/android/calendar/c;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 95
    invoke-static {}, Lcom/android/calendar/dz;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 107
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void

    .line 98
    :cond_0
    invoke-static {}, Lcom/android/calendar/dz;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/android/calendar/dz;->d()Z

    move-result v2

    if-nez v2, :cond_1

    .line 100
    const v2, 0x7f0f0233

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 101
    :cond_1
    invoke-static {}, Lcom/android/calendar/dz;->c()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/android/calendar/dz;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 103
    const v2, 0x7f0f0235

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 73
    sput-object p0, Lcom/android/calendar/c;->h:Lcom/android/calendar/c;

    .line 74
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/c;->g:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/android/calendar/c;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/c;->g:Landroid/app/Activity;

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/c;->g:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/android/calendar/c;->g:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 58
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Dialog;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/c;->h:Lcom/android/calendar/c;

    .line 79
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 80
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/android/calendar/c;->j:Z

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/android/calendar/c;->i:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 67
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
