.class public Lcom/android/calendar/eo;
.super Landroid/app/DialogFragment;
.source "LinkActionChooserFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/net/Uri;

.field private c:Ljava/lang/String;

.field private d:Landroid/widget/ArrayAdapter;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/android/calendar/eo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/eo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 59
    return-void
.end method

.method static synthetic a(Landroid/app/Activity;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/android/calendar/eo;->c(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static final a(Ljava/lang/String;)Landroid/app/DialogFragment;
    .locals 3

    .prologue
    .line 77
    new-instance v0, Lcom/android/calendar/eo;

    invoke-direct {v0}, Lcom/android/calendar/eo;-><init>()V

    .line 78
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 79
    const-string v2, "url"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0, v1}, Lcom/android/calendar/eo;->setArguments(Landroid/os/Bundle;)V

    .line 81
    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/android/calendar/eo;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/eo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/android/calendar/eo;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/eo;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/android/calendar/eo;->e:Z

    return v0
.end method

.method static synthetic b(Landroid/app/Activity;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/android/calendar/eo;->d(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/eo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/eo;->c:Ljava/lang/String;

    return-object v0
.end method

.method private b()Z
    .locals 3

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 295
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 296
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 297
    const-string v2, "com.sec.android.app.sbrowser"

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    const/4 v0, 0x1

    .line 300
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/app/Activity;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 259
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v6

    .line 272
    :cond_1
    :goto_0
    return v0

    .line 262
    :cond_2
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 263
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 265
    if-eqz v1, :cond_3

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 266
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 268
    if-eqz v1, :cond_1

    .line 269
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 268
    :cond_3
    if-eqz v1, :cond_4

    .line 269
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v6

    .line 272
    goto :goto_0

    .line 268
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 269
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method static synthetic c(Lcom/android/calendar/eo;)Z
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/calendar/eo;->b()Z

    move-result v0

    return v0
.end method

.method private static d(Landroid/app/Activity;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, -0x1

    .line 276
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v6

    .line 290
    :cond_1
    :goto_0
    return v0

    .line 279
    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "contact_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "data1=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 284
    if-eqz v1, :cond_3

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 285
    const-string v0, "contact_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 287
    if-eqz v1, :cond_1

    .line 288
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 287
    :cond_3
    if-eqz v1, :cond_4

    .line 288
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v6

    .line 290
    goto :goto_0

    .line 287
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 288
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/calendar/eo;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/ew;

    .line 255
    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/eo;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/ew;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 256
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/android/calendar/eo;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 87
    if-nez v0, :cond_0

    .line 88
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    const-string v1, "url"

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0, v0}, Lcom/android/calendar/eo;->setArguments(Landroid/os/Bundle;)V

    .line 93
    :cond_0
    const-string v1, "url"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/eo;->b:Landroid/net/Uri;

    .line 94
    iget-object v0, p0, Lcom/android/calendar/eo;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    const-string v2, ";"

    invoke-static {}, Lcom/android/calendar/dz;->E()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, " ext. "

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/eo;->c:Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/android/calendar/eo;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "url-action"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 97
    iget-object v0, p0, Lcom/android/calendar/eo;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mailto-action"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 98
    iget-object v0, p0, Lcom/android/calendar/eo;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "tel-action"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 99
    if-eqz v4, :cond_4

    .line 100
    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/eo;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/android/calendar/eo;->c(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/eo;->e:Z

    .line 104
    :cond_1
    :goto_2
    new-instance v7, Lcom/android/calendar/ep;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f0023

    invoke-direct {v7, p0, v0, v1}, Lcom/android/calendar/ep;-><init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V

    .line 119
    new-instance v8, Lcom/android/calendar/eq;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f0025

    invoke-direct {v8, p0, v0, v1}, Lcom/android/calendar/eq;-><init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V

    .line 132
    new-instance v0, Lcom/android/calendar/er;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-boolean v1, p0, Lcom/android/calendar/eo;->e:Z

    if-eqz v1, :cond_6

    const v3, 0x7f0f002b

    :goto_3
    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/er;-><init>(Lcom/android/calendar/eo;Landroid/app/Activity;IZZ)V

    .line 169
    new-instance v4, Lcom/android/calendar/es;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0f01d6

    invoke-direct {v4, p0, v1, v2}, Lcom/android/calendar/es;-><init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V

    .line 179
    new-instance v2, Lcom/android/calendar/et;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v3, 0x7f0f011c

    invoke-direct {v2, p0, v1, v3}, Lcom/android/calendar/et;-><init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V

    .line 195
    new-instance v9, Lcom/android/calendar/eu;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v3, 0x7f0f0033

    invoke-direct {v9, p0, v1, v3}, Lcom/android/calendar/eu;-><init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V

    .line 217
    new-instance v10, Lcom/android/calendar/ev;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v3, 0x7f0f0024

    invoke-direct {v10, p0, v1, v3}, Lcom/android/calendar/ev;-><init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V

    .line 230
    const/4 v1, 0x4

    new-array v3, v1, [Lcom/android/calendar/ew;

    const/4 v1, 0x0

    aput-object v7, v3, v1

    const/4 v1, 0x1

    aput-object v8, v3, v1

    const/4 v1, 0x2

    aput-object v0, v3, v1

    const/4 v1, 0x3

    aput-object v4, v3, v1

    .line 231
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/android/calendar/ew;

    const/4 v7, 0x0

    aput-object v2, v1, v7

    const/4 v2, 0x1

    aput-object v9, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    .line 232
    const/4 v2, 0x4

    new-array v2, v2, [Lcom/android/calendar/ew;

    const/4 v7, 0x0

    aput-object v10, v2, v7

    const/4 v7, 0x1

    aput-object v8, v2, v7

    const/4 v7, 0x2

    aput-object v0, v2, v7

    const/4 v0, 0x3

    aput-object v4, v2, v0

    .line 234
    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const v8, 0x7f040067

    if-eqz v6, :cond_7

    move-object v0, v1

    :goto_4
    invoke-direct {v4, v7, v8, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/android/calendar/eo;->d:Landroid/widget/ArrayAdapter;

    .line 236
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 237
    iget-object v1, p0, Lcom/android/calendar/eo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/eo;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 239
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 240
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 242
    return-object v0

    .line 94
    :cond_2
    const-string v0, " #"

    goto/16 :goto_0

    .line 100
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 101
    :cond_4
    if-eqz v5, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/eo;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/android/calendar/eo;->d(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/android/calendar/eo;->e:Z

    goto/16 :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    .line 132
    :cond_6
    const v3, 0x7f0f0022

    goto/16 :goto_3

    .line 234
    :cond_7
    if-eqz v5, :cond_8

    move-object v0, v2

    goto :goto_4

    :cond_8
    move-object v0, v3

    goto :goto_4
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 249
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 250
    return-void
.end method
