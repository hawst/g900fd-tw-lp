.class public abstract Lcom/android/b/b/a;
.super Landroid/widget/BaseAdapter;
.source "CompositeCursorAdapter.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/util/ArrayList;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z


# virtual methods
.method protected a(II)I
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x1

    return v0
.end method

.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/b/b/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method protected a(ILandroid/database/Cursor;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 392
    if-eqz p4, :cond_0

    .line 397
    :goto_0
    invoke-virtual {p0, p4, p1, p2, p3}, Lcom/android/b/b/a;->a(Landroid/view/View;ILandroid/database/Cursor;I)V

    .line 398
    return-object p4

    .line 395
    :cond_0
    iget-object v1, p0, Lcom/android/b/b/a;->a:Landroid/content/Context;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/b/b/a;->a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    goto :goto_0
.end method

.method protected a(ILandroid/database/Cursor;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 365
    if-eqz p3, :cond_0

    .line 368
    :goto_0
    invoke-virtual {p0, p3, p1, p2}, Lcom/android/b/b/a;->a(Landroid/view/View;ILandroid/database/Cursor;)V

    .line 369
    return-object p3

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/android/b/b/a;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, p2, p4}, Lcom/android/b/b/a;->a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    goto :goto_0
.end method

.method protected abstract a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget-object v0, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    .line 213
    if-eq v0, p2, :cond_2

    .line 214
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 215
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iput-object p2, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    .line 218
    if-eqz p2, :cond_1

    .line 219
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    const-string v1, "_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/android/b/b/b;->l:I

    .line 221
    :cond_1
    invoke-virtual {p0}, Lcom/android/b/b/a;->c()V

    .line 222
    invoke-virtual {p0}, Lcom/android/b/b/a;->notifyDataSetChanged()V

    .line 224
    :cond_2
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 384
    return-void
.end method

.method protected abstract a(Landroid/view/View;ILandroid/database/Cursor;I)V
.end method

.method public a(Lcom/android/b/b/b;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-virtual {p0}, Lcom/android/b/b/a;->c()V

    .line 95
    invoke-virtual {p0}, Lcom/android/b/b/a;->notifyDataSetChanged()V

    .line 96
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 521
    iput-boolean p1, p0, Lcom/android/b/b/a;->e:Z

    .line 522
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/b/b/a;->f:Z

    if-eqz v0, :cond_0

    .line 523
    invoke-virtual {p0}, Lcom/android/b/b/a;->notifyDataSetChanged()V

    .line 525
    :cond_0
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    .line 478
    iget-boolean v0, v0, Lcom/android/b/b/b;->j:Z

    if-eqz v0, :cond_0

    .line 479
    const/4 v0, 0x0

    .line 482
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(I)Lcom/android/b/b/b;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    .line 133
    iget-object v0, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    .line 134
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 135
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 139
    invoke-virtual {p0}, Lcom/android/b/b/a;->c()V

    .line 140
    invoke-virtual {p0}, Lcom/android/b/b/a;->notifyDataSetChanged()V

    .line 141
    return-void
.end method

.method protected b(II)Z
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x1

    return v0
.end method

.method public c(I)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget-object v0, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    return-object v0
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/b/b/a;->d:Z

    .line 159
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected e()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 166
    iget-boolean v0, p0, Lcom/android/b/b/a;->d:Z

    if-eqz v0, :cond_0

    .line 184
    :goto_0
    return-void

    .line 170
    :cond_0
    iput v2, p0, Lcom/android/b/b/a;->c:I

    .line 171
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    .line 172
    iget-object v1, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    .line 173
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 174
    :goto_2
    iget-boolean v4, v0, Lcom/android/b/b/b;->j:Z

    if-eqz v4, :cond_2

    .line 175
    if-nez v1, :cond_1

    iget-boolean v4, v0, Lcom/android/b/b/b;->i:Z

    if-eqz v4, :cond_2

    .line 176
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 179
    :cond_2
    iput v1, v0, Lcom/android/b/b/b;->m:I

    .line 180
    iget v0, p0, Lcom/android/b/b/a;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/b/b/a;->c:I

    goto :goto_1

    :cond_3
    move v1, v2

    .line 173
    goto :goto_2

    .line 183
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/b/b/a;->d:Z

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/android/b/b/a;->e()V

    .line 198
    iget v0, p0, Lcom/android/b/b/a;->c:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 418
    invoke-virtual {p0}, Lcom/android/b/b/a;->e()V

    .line 419
    const/4 v0, 0x0

    .line 420
    iget-object v1, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    .line 421
    iget v2, v0, Lcom/android/b/b/b;->m:I

    add-int/2addr v2, v1

    .line 422
    if-lt p1, v1, :cond_2

    if-ge p1, v2, :cond_2

    .line 423
    sub-int v1, p1, v1

    .line 424
    iget-boolean v2, v0, Lcom/android/b/b/b;->j:Z

    if-eqz v2, :cond_0

    .line 425
    add-int/lit8 v1, v1, -0x1

    .line 427
    :cond_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    move-object v0, v3

    .line 437
    :goto_1
    return-object v0

    .line 430
    :cond_1
    iget-object v0, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    .line 431
    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1

    :cond_2
    move v1, v2

    .line 435
    goto :goto_0

    :cond_3
    move-object v0, v3

    .line 437
    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 7

    .prologue
    const/4 v6, -0x1

    const-wide/16 v4, 0x0

    .line 444
    invoke-virtual {p0}, Lcom/android/b/b/a;->e()V

    .line 445
    const/4 v0, 0x0

    .line 446
    iget-object v1, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    .line 447
    iget v2, v0, Lcom/android/b/b/b;->m:I

    add-int/2addr v2, v1

    .line 448
    if-lt p1, v1, :cond_5

    if-ge p1, v2, :cond_5

    .line 449
    sub-int v1, p1, v1

    .line 450
    iget-boolean v2, v0, Lcom/android/b/b/b;->j:Z

    if-eqz v2, :cond_0

    .line 451
    add-int/lit8 v1, v1, -0x1

    .line 453
    :cond_0
    if-ne v1, v6, :cond_1

    move-wide v0, v4

    .line 469
    :goto_1
    return-wide v0

    .line 456
    :cond_1
    iget v2, v0, Lcom/android/b/b/b;->l:I

    if-ne v2, v6, :cond_2

    move-wide v0, v4

    .line 457
    goto :goto_1

    .line 460
    :cond_2
    iget-object v2, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    .line 461
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    move-wide v0, v4

    .line 462
    goto :goto_1

    .line 464
    :cond_4
    iget v0, v0, Lcom/android/b/b/b;->l:I

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_1

    :cond_5
    move v1, v2

    .line 467
    goto :goto_0

    :cond_6
    move-wide v0, v4

    .line 469
    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 307
    invoke-virtual {p0}, Lcom/android/b/b/a;->e()V

    .line 309
    iget-object v2, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v5, :cond_2

    .line 310
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget v0, v0, Lcom/android/b/b/b;->m:I

    add-int v4, v2, v0

    .line 311
    if-lt p1, v2, :cond_1

    if-ge p1, v4, :cond_1

    .line 312
    sub-int v2, p1, v2

    .line 313
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget-boolean v0, v0, Lcom/android/b/b/b;->j:Z

    if-eqz v0, :cond_3

    .line 314
    add-int/lit8 v0, v2, -0x1

    .line 316
    :goto_1
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 319
    :goto_2
    return v0

    :cond_0
    invoke-virtual {p0, v3, v0}, Lcom/android/b/b/a;->a(II)I

    move-result v0

    goto :goto_2

    .line 309
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v4

    goto :goto_0

    .line 325
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 329
    invoke-virtual {p0}, Lcom/android/b/b/a;->e()V

    .line 331
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v1, v3, :cond_4

    .line 332
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget v0, v0, Lcom/android/b/b/b;->m:I

    add-int/2addr v0, v2

    .line 333
    if-lt p1, v2, :cond_3

    if-ge p1, v0, :cond_3

    .line 334
    sub-int v3, p1, v2

    .line 335
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget-boolean v0, v0, Lcom/android/b/b/b;->j:Z

    if-eqz v0, :cond_0

    .line 336
    add-int/lit8 v3, v3, -0x1

    .line 339
    :cond_0
    const/4 v0, -0x1

    if-ne v3, v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget-object v0, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    invoke-virtual {p0, v1, v0, p2, p3}, Lcom/android/b/b/a;->a(ILandroid/database/Cursor;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 348
    :goto_1
    if-nez v0, :cond_5

    .line 349
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "View should not be null, partition: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget-object v0, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 343
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t move cursor to position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_2
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget-object v2, v0, Lcom/android/b/b/b;->k:Landroid/database/Cursor;

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/b/b/a;->a(ILandroid/database/Cursor;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 331
    :cond_3
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 357
    :cond_4
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 352
    :cond_5
    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/android/b/b/a;->f()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 490
    invoke-virtual {p0}, Lcom/android/b/b/a;->e()V

    .line 492
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_2

    .line 493
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget v0, v0, Lcom/android/b/b/b;->m:I

    add-int v4, v3, v0

    .line 494
    if-lt p1, v3, :cond_1

    if-ge p1, v4, :cond_1

    .line 495
    sub-int v3, p1, v3

    .line 496
    iget-object v0, p0, Lcom/android/b/b/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/b/b/b;

    iget-boolean v0, v0, Lcom/android/b/b/b;->j:Z

    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    move v0, v1

    .line 505
    :goto_1
    return v0

    .line 499
    :cond_0
    invoke-virtual {p0, v2, v3}, Lcom/android/b/b/a;->b(II)Z

    move-result v0

    goto :goto_1

    .line 492
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v4

    goto :goto_0

    :cond_2
    move v0, v1

    .line 505
    goto :goto_1
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 529
    iget-boolean v0, p0, Lcom/android/b/b/a;->e:Z

    if-eqz v0, :cond_0

    .line 530
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/b/b/a;->f:Z

    .line 531
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 535
    :goto_0
    return-void

    .line 533
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/b/b/a;->f:Z

    goto :goto_0
.end method
