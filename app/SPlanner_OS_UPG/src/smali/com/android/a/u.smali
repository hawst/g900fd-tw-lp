.class public Lcom/android/a/u;
.super Ljava/lang/Object;
.source "ICalendar.java"


# direct methods
.method public static a(Lcom/android/a/w;Ljava/lang/String;)Lcom/android/a/w;
    .locals 1

    .prologue
    .line 657
    invoke-static {p1}, Lcom/android/a/u;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 658
    invoke-static {p0, v0}, Lcom/android/a/u;->b(Lcom/android/a/w;Ljava/lang/String;)Lcom/android/a/w;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/android/a/w;
    .locals 3

    .prologue
    .line 612
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/android/a/u;->a(Lcom/android/a/w;Ljava/lang/String;)Lcom/android/a/w;

    move-result-object v0

    .line 613
    if-eqz v0, :cond_0

    const-string v1, "VCALENDAR"

    invoke-virtual {v0}, Lcom/android/a/w;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 614
    :cond_0
    new-instance v0, Lcom/android/a/x;

    const-string v1, "Expected VCALENDAR"

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 616
    :cond_1
    return-object v0
.end method

.method private static a(Ljava/lang/String;Lcom/android/a/z;Lcom/android/a/w;)Lcom/android/a/w;
    .locals 5

    .prologue
    const/16 v4, 0x3b

    const/4 v1, 0x0

    .line 469
    iput-object p0, p1, Lcom/android/a/z;->a:Ljava/lang/String;

    .line 470
    iget-object v0, p1, Lcom/android/a/z;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 474
    iput v1, p1, Lcom/android/a/z;->b:I

    move v0, v1

    :goto_0
    iget v3, p1, Lcom/android/a/z;->b:I

    if-ge v3, v2, :cond_0

    .line 475
    iget v0, p1, Lcom/android/a/z;->b:I

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 476
    if-eq v0, v4, :cond_0

    const/16 v3, 0x3a

    if-ne v0, v3, :cond_1

    .line 480
    :cond_0
    iget v2, p1, Lcom/android/a/z;->b:I

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 482
    if-nez p2, :cond_2

    .line 483
    const-string v2, "BEGIN"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 484
    new-instance v0, Lcom/android/a/x;

    const-string v1, "Expected BEGIN"

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 474
    :cond_1
    iget v3, p1, Lcom/android/a/z;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p1, Lcom/android/a/z;->b:I

    goto :goto_0

    .line 489
    :cond_2
    const-string v2, "BEGIN"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 491
    invoke-static {p1}, Lcom/android/a/u;->a(Lcom/android/a/z;)Ljava/lang/String;

    move-result-object v1

    .line 492
    new-instance v0, Lcom/android/a/w;

    invoke-direct {v0, v1, p2}, Lcom/android/a/w;-><init>(Ljava/lang/String;Lcom/android/a/w;)V

    .line 493
    if-eqz p2, :cond_3

    .line 494
    invoke-virtual {p2, v0}, Lcom/android/a/w;->a(Lcom/android/a/w;)V

    :cond_3
    move-object p2, v0

    .line 518
    :goto_1
    return-object p2

    .line 497
    :cond_4
    const-string v2, "END"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 499
    invoke-static {p1}, Lcom/android/a/u;->a(Lcom/android/a/z;)Ljava/lang/String;

    move-result-object v0

    .line 500
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/android/a/w;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 502
    :cond_5
    new-instance v1, Lcom/android/a/x;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected END "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v1

    .line 504
    :cond_6
    invoke-virtual {p2}, Lcom/android/a/w;->b()Lcom/android/a/w;

    move-result-object p2

    goto :goto_1

    .line 506
    :cond_7
    new-instance v2, Lcom/android/a/aa;

    invoke-direct {v2, v1}, Lcom/android/a/aa;-><init>(Ljava/lang/String;)V

    .line 509
    if-ne v0, v4, :cond_8

    .line 511
    :goto_2
    invoke-static {p1}, Lcom/android/a/u;->b(Lcom/android/a/z;)Lcom/android/a/y;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 512
    invoke-virtual {v2, v0}, Lcom/android/a/aa;->a(Lcom/android/a/y;)V

    goto :goto_2

    .line 515
    :cond_8
    invoke-static {p1}, Lcom/android/a/u;->a(Lcom/android/a/z;)Ljava/lang/String;

    move-result-object v0

    .line 516
    invoke-virtual {v2, v0}, Lcom/android/a/aa;->a(Ljava/lang/String;)V

    .line 517
    invoke-virtual {p2, v2}, Lcom/android/a/w;->a(Lcom/android/a/aa;)V

    goto :goto_1
.end method

.method private static a(Lcom/android/a/z;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 527
    iget-object v0, p0, Lcom/android/a/z;->a:Ljava/lang/String;

    .line 528
    iget v1, p0, Lcom/android/a/z;->b:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/android/a/z;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3a

    if-eq v1, v2, :cond_1

    .line 529
    :cond_0
    new-instance v1, Lcom/android/a/x;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected \':\' before end of line in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v1

    .line 532
    :cond_1
    iget v1, p0, Lcom/android/a/z;->b:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 533
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/a/z;->b:I

    .line 534
    return-object v1
.end method

.method private static b(Lcom/android/a/w;Ljava/lang/String;)Lcom/android/a/w;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 430
    .line 431
    new-instance v3, Lcom/android/a/z;

    const/4 v1, 0x0

    invoke-direct {v3, v1}, Lcom/android/a/z;-><init>(Lcom/android/a/v;)V

    .line 432
    iput v0, v3, Lcom/android/a/z;->b:I

    .line 435
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 439
    array-length v5, v4

    move v2, v0

    move-object v1, p0

    move-object v0, p0

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 441
    :try_start_0
    invoke-static {v6, v3, v1}, Lcom/android/a/u;->a(Ljava/lang/String;Lcom/android/a/z;Lcom/android/a/w;)Lcom/android/a/w;
    :try_end_0
    .catch Lcom/android/a/x; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 445
    if-nez v0, :cond_0

    move-object v0, v1

    .line 439
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 448
    :catch_0
    move-exception v6

    goto :goto_1

    .line 457
    :cond_1
    return-object v0
.end method

.method private static b(Lcom/android/a/z;)Lcom/android/a/y;
    .locals 9

    .prologue
    const/16 v8, 0x22

    const/4 v3, -0x1

    .line 543
    iget-object v4, p0, Lcom/android/a/z;->a:Ljava/lang/String;

    .line 544
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 545
    const/4 v2, 0x0

    move v0, v3

    move v1, v3

    .line 548
    :goto_0
    iget v6, p0, Lcom/android/a/z;->b:I

    if-ge v6, v5, :cond_e

    .line 549
    iget v6, p0, Lcom/android/a/z;->b:I

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 550
    const/16 v7, 0x3a

    if-ne v6, v7, :cond_2

    .line 551
    if-eqz v2, :cond_1

    .line 552
    if-ne v0, v3, :cond_0

    .line 553
    new-instance v0, Lcom/android/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected \'=\' within parameter in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 556
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/a/z;->b:I

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/a/y;->b:Ljava/lang/String;

    .line 596
    :cond_1
    :goto_1
    return-object v2

    .line 560
    :cond_2
    const/16 v7, 0x3b

    if-ne v6, v7, :cond_6

    .line 561
    if-eqz v2, :cond_4

    .line 562
    if-ne v0, v3, :cond_3

    .line 563
    new-instance v0, Lcom/android/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected \'=\' within parameter in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/a/z;->b:I

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/a/y;->b:Ljava/lang/String;

    goto :goto_1

    .line 570
    :cond_4
    new-instance v2, Lcom/android/a/y;

    invoke-direct {v2}, Lcom/android/a/y;-><init>()V

    .line 571
    iget v1, p0, Lcom/android/a/z;->b:I

    .line 598
    :cond_5
    :goto_2
    iget v6, p0, Lcom/android/a/z;->b:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/android/a/z;->b:I

    goto :goto_0

    .line 573
    :cond_6
    const/16 v7, 0x3d

    if-ne v6, v7, :cond_9

    .line 574
    iget v0, p0, Lcom/android/a/z;->b:I

    .line 575
    if-eqz v2, :cond_7

    if-ne v1, v3, :cond_8

    .line 576
    :cond_7
    new-instance v0, Lcom/android/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected \';\' before \'=\' in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 579
    :cond_8
    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v4, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/android/a/y;->a:Ljava/lang/String;

    goto :goto_2

    .line 580
    :cond_9
    if-ne v6, v8, :cond_5

    .line 581
    if-nez v2, :cond_a

    .line 582
    new-instance v0, Lcom/android/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected parameter before \'\"\' in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 584
    :cond_a
    if-ne v0, v3, :cond_b

    .line 585
    new-instance v0, Lcom/android/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected \'=\' within parameter in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 587
    :cond_b
    iget v1, p0, Lcom/android/a/z;->b:I

    add-int/lit8 v0, v0, 0x1

    if-le v1, v0, :cond_c

    .line 588
    new-instance v0, Lcom/android/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Parameter value cannot contain a \'\"\' in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 590
    :cond_c
    iget v0, p0, Lcom/android/a/z;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v4, v8, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 591
    if-gez v0, :cond_d

    .line 592
    new-instance v0, Lcom/android/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected closing \'\"\' in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0

    .line 594
    :cond_d
    iget v1, p0, Lcom/android/a/z;->b:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/android/a/y;->b:Ljava/lang/String;

    .line 595
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/a/z;->b:I

    goto/16 :goto_1

    .line 600
    :cond_e
    new-instance v0, Lcom/android/a/x;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected \':\' before end of line in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/x;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 408
    const-string v0, "\r\n"

    const-string v1, "\n"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 409
    const-string v1, "\r"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 414
    const-string v1, "\n "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 416
    return-object v0
.end method
