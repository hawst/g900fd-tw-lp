.class public Lcom/android/ex/a/b;
.super Ljava/lang/Object;
.source "RecipientEntry.java"


# instance fields
.field private final a:I

.field private b:Z

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:J

.field private final h:J

.field private final i:Z

.field private final j:Landroid/net/Uri;

.field private k:Z

.field private l:[B

.field private final m:Z


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;JJLandroid/net/Uri;ZZZ)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput p1, p0, Lcom/android/ex/a/b;->a:I

    .line 83
    iput-boolean p11, p0, Lcom/android/ex/a/b;->b:Z

    .line 84
    iput-object p2, p0, Lcom/android/ex/a/b;->c:Ljava/lang/String;

    .line 85
    iput-object p3, p0, Lcom/android/ex/a/b;->d:Ljava/lang/String;

    .line 86
    iput p4, p0, Lcom/android/ex/a/b;->e:I

    .line 87
    iput-object p5, p0, Lcom/android/ex/a/b;->f:Ljava/lang/String;

    .line 88
    iput-wide p6, p0, Lcom/android/ex/a/b;->g:J

    .line 89
    iput-wide p8, p0, Lcom/android/ex/a/b;->h:J

    .line 90
    iput-object p10, p0, Lcom/android/ex/a/b;->j:Landroid/net/Uri;

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/a/b;->l:[B

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/ex/a/b;->i:Z

    .line 93
    iput-boolean p12, p0, Lcom/android/ex/a/b;->k:Z

    .line 94
    iput-boolean p13, p0, Lcom/android/ex/a/b;->m:Z

    .line 95
    return-void
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;JJLjava/lang/String;ZZ)Lcom/android/ex/a/b;
    .locals 19

    .prologue
    .line 167
    new-instance v4, Lcom/android/ex/a/b;

    const/4 v5, 0x0

    move/from16 v0, p1

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/android/ex/a/b;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz p9, :cond_0

    invoke-static/range {p9 .. p9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    :goto_0
    const/4 v15, 0x1

    move-object/from16 v7, p2

    move/from16 v8, p3

    move-object/from16 v9, p4

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    move/from16 v16, p10

    move/from16 v17, p11

    invoke-direct/range {v4 .. v17}, Lcom/android/ex/a/b;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;JJLandroid/net/Uri;ZZZ)V

    return-object v4

    :cond_0
    const/4 v14, 0x0

    goto :goto_0
.end method

.method private static a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const/16 v0, 0x14

    if-le p0, v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;JJLjava/lang/String;ZZ)Lcom/android/ex/a/b;
    .locals 19

    .prologue
    .line 177
    new-instance v4, Lcom/android/ex/a/b;

    const/4 v5, 0x0

    move/from16 v0, p1

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/android/ex/a/b;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz p9, :cond_0

    invoke-static/range {p9 .. p9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    :goto_0
    const/4 v15, 0x0

    move-object/from16 v7, p2

    move/from16 v8, p3

    move-object/from16 v9, p4

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    move/from16 v16, p10

    move/from16 v17, p11

    invoke-direct/range {v4 .. v17}, Lcom/android/ex/a/b;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;JJLandroid/net/Uri;ZZZ)V

    return-object v4

    :cond_0
    const/4 v14, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/android/ex/a/b;->a:I

    return v0
.end method

.method public declared-synchronized a([B)V
    .locals 1

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/android/ex/a/b;->l:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    monitor-exit p0

    return-void

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/ex/a/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/android/ex/a/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/android/ex/a/b;->e:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/android/ex/a/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/android/ex/a/b;->b:Z

    return v0
.end method

.method public g()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/ex/a/b;->j:Landroid/net/Uri;

    return-object v0
.end method

.method public declared-synchronized h()[B
    .locals 1

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/ex/a/b;->l:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/android/ex/a/b;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/ex/a/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ex/a/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">, isValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/ex/a/b;->k:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
