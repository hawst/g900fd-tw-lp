.class public Lcom/android/ex/editstyledtext/EditStyledText;
.super Landroid/widget/EditText;
.source "EditStyledText.java"


# static fields
.field private static a:Ljava/lang/CharSequence;

.field private static b:Ljava/lang/CharSequence;

.field private static c:Ljava/lang/CharSequence;

.field private static final k:Landroid/text/NoCopySpan$Concrete;


# instance fields
.field private d:F

.field private e:Ljava/util/ArrayList;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Lcom/android/ex/editstyledtext/j;

.field private h:Landroid/view/inputmethod/InputConnection;

.field private i:Lcom/android/ex/editstyledtext/l;

.field private j:Lcom/android/ex/editstyledtext/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 196
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    sput-object v0, Lcom/android/ex/editstyledtext/EditStyledText;->k:Landroid/text/NoCopySpan$Concrete;

    return-void
.end method

.method static synthetic a(Lcom/android/ex/editstyledtext/EditStyledText;)I
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getMaxImageWidthPx()I

    move-result v0

    return v0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/editstyledtext/d;

    .line 535
    invoke-interface {v0, p1, p2}, Lcom/android/ex/editstyledtext/d;->a(II)V

    goto :goto_0

    .line 538
    :cond_0
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/editstyledtext/d;

    .line 460
    invoke-interface {v0, p1}, Lcom/android/ex/editstyledtext/d;->a(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 463
    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/view/View;Landroid/text/Spannable;)V
    .locals 0

    .prologue
    .line 93
    invoke-static {p0, p1}, Lcom/android/ex/editstyledtext/EditStyledText;->d(Landroid/view/View;Landroid/text/Spannable;)V

    return-void
.end method

.method static synthetic a(Lcom/android/ex/editstyledtext/EditStyledText;II)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Lcom/android/ex/editstyledtext/EditStyledText;->a(II)V

    return-void
.end method

.method private b(I)I
    .locals 4

    .prologue
    .line 904
    iget v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->d:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 905
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->d:F

    .line 907
    :cond_0
    int-to-float v0, p1

    invoke-direct {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getPaddingScale()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method static synthetic b(Lcom/android/ex/editstyledtext/EditStyledText;)I
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getMaxImageWidthDip()I

    move-result v0

    return v0
.end method

.method static synthetic b(Landroid/view/View;Landroid/text/Spannable;)V
    .locals 0

    .prologue
    .line 93
    invoke-static {p0, p1}, Lcom/android/ex/editstyledtext/EditStyledText;->c(Landroid/view/View;Landroid/text/Spannable;)V

    return-void
.end method

.method private static c(Landroid/view/View;Landroid/text/Spannable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 930
    sget-object v0, Lcom/android/ex/editstyledtext/EditStyledText;->k:Landroid/text/NoCopySpan$Concrete;

    const v1, 0x1000011

    invoke-interface {p1, v0, v2, v2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 931
    return-void
.end method

.method static synthetic c(Lcom/android/ex/editstyledtext/EditStyledText;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->p()V

    return-void
.end method

.method static synthetic d(Lcom/android/ex/editstyledtext/EditStyledText;)F
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getPaddingScale()F

    move-result v0

    return v0
.end method

.method private static d(Landroid/view/View;Landroid/text/Spannable;)V
    .locals 1

    .prologue
    .line 934
    sget-object v0, Lcom/android/ex/editstyledtext/EditStyledText;->k:Landroid/text/NoCopySpan$Concrete;

    invoke-interface {p1, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 935
    return-void
.end method

.method static synthetic e(Lcom/android/ex/editstyledtext/EditStyledText;)Lcom/android/ex/editstyledtext/j;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    return-object v0
.end method

.method private getMaxImageWidthDip()I
    .locals 1

    .prologue
    .line 911
    const/16 v0, 0x12c

    return v0
.end method

.method private getMaxImageWidthPx()I
    .locals 1

    .prologue
    .line 915
    const/16 v0, 0x12c

    invoke-direct {p0, v0}, Lcom/android/ex/editstyledtext/EditStyledText;->b(I)I

    move-result v0

    return v0
.end method

.method private getPaddingScale()F
    .locals 2

    .prologue
    .line 896
    iget v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->d:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 897
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->d:F

    .line 899
    :cond_0
    iget v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->d:F

    return v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->e()V

    .line 672
    return-void
.end method

.method private p()V
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->d()V

    .line 676
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 3

    .prologue
    const/high16 v1, -0x1000000

    .line 875
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-le p1, v0, :cond_1

    :cond_0
    move v0, v1

    .line 883
    :goto_0
    return v0

    .line 878
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v0

    const-class v2, Landroid/text/style/ForegroundColorSpan;

    invoke-interface {v0, p1, p1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ForegroundColorSpan;

    .line 880
    array-length v2, v0

    if-lez v2, :cond_2

    .line 881
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/text/style/ForegroundColorSpan;->getForegroundColor()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 883
    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 466
    const/4 v0, 0x0

    .line 467
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->e:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 468
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/editstyledtext/d;

    .line 469
    invoke-interface {v0}, Lcom/android/ex/editstyledtext/d;->a()Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 470
    goto :goto_0

    :cond_0
    move v1, v0

    .line 472
    :cond_1
    return v1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 542
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->a(I)V

    .line 543
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->a(I)V

    .line 548
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->a(I)V

    .line 557
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 272
    invoke-super {p0}, Landroid/widget/EditText;->drawableStateChanged()V

    .line 273
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->e()V

    .line 276
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 561
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->a(I)V

    .line 562
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 566
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->a(I)V

    .line 567
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->a(Z)V

    .line 604
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 862
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->k()I

    move-result v0

    return v0
.end method

.method public getEditMode()I
    .locals 1

    .prologue
    .line 816
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->l()I

    move-result v0

    return v0
.end method

.method public getEditStyledTextManager()Lcom/android/ex/editstyledtext/j;
    .locals 1

    .prologue
    .line 866
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    return-object v0
.end method

.method public getHtml()Ljava/lang/String;
    .locals 2

    .prologue
    .line 834
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->i:Lcom/android/ex/editstyledtext/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/l;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 853
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->i:Lcom/android/ex/editstyledtext/l;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/l;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectState()I
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->m()I

    move-result v0

    return v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 608
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->b(Z)V

    .line 609
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->b()V

    .line 626
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->a(I)V

    .line 652
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->c()V

    .line 656
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->g()Z

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->h()Z

    move-result v0

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->i()Z

    move-result v0

    return v0
.end method

.method protected onCreateContextMenu(Landroid/view/ContextMenu;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 329
    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateContextMenu(Landroid/view/ContextMenu;)V

    .line 330
    new-instance v0, Lcom/android/ex/editstyledtext/k;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/ex/editstyledtext/k;-><init>(Lcom/android/ex/editstyledtext/EditStyledText;Lcom/android/ex/editstyledtext/a;)V

    .line 331
    sget-object v1, Lcom/android/ex/editstyledtext/EditStyledText;->a:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 332
    const v1, 0xffff01

    sget-object v2, Lcom/android/ex/editstyledtext/EditStyledText;->a:Ljava/lang/CharSequence;

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 335
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/ex/editstyledtext/EditStyledText;->b:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    .line 336
    const v1, 0xffff02

    sget-object v2, Lcom/android/ex/editstyledtext/EditStyledText;->b:Ljava/lang/CharSequence;

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 339
    :cond_1
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/j;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 340
    const v1, 0x1020022

    sget-object v2, Lcom/android/ex/editstyledtext/EditStyledText;->c:Ljava/lang/CharSequence;

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object v0

    const/16 v1, 0x76

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 343
    :cond_2
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .prologue
    .line 370
    new-instance v0, Lcom/android/ex/editstyledtext/p;

    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/ex/editstyledtext/p;-><init>(Landroid/view/inputmethod/InputConnection;Lcom/android/ex/editstyledtext/EditStyledText;)V

    iput-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->h:Landroid/view/inputmethod/InputConnection;

    .line 372
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->h:Landroid/view/inputmethod/InputConnection;

    return-object v0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 377
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 378
    if-eqz p1, :cond_1

    .line 379
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->b()V

    .line 383
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->c()V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 261
    instance-of v0, p1, Lcom/android/ex/editstyledtext/EditStyledText$SavedStyledTextState;

    if-nez v0, :cond_0

    .line 262
    invoke-super {p0, p1}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 268
    :goto_0
    return-void

    .line 265
    :cond_0
    check-cast p1, Lcom/android/ex/editstyledtext/EditStyledText$SavedStyledTextState;

    .line 266
    invoke-virtual {p1}, Lcom/android/ex/editstyledtext/EditStyledText$SavedStyledTextState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 267
    iget v0, p1, Lcom/android/ex/editstyledtext/EditStyledText$SavedStyledTextState;->a:I

    invoke-virtual {p0, v0}, Lcom/android/ex/editstyledtext/EditStyledText;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 253
    invoke-super {p0}, Landroid/widget/EditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 254
    new-instance v1, Lcom/android/ex/editstyledtext/EditStyledText$SavedStyledTextState;

    invoke-direct {v1, v0}, Lcom/android/ex/editstyledtext/EditStyledText$SavedStyledTextState;-><init>(Landroid/os/Parcelable;)V

    .line 255
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->k()I

    move-result v0

    iput v0, v1, Lcom/android/ex/editstyledtext/EditStyledText$SavedStyledTextState;->a:I

    .line 256
    return-object v1
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/android/ex/editstyledtext/j;->b(Landroid/text/Editable;III)V

    .line 350
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/android/ex/editstyledtext/j;->a(Landroid/text/Editable;III)V

    .line 351
    if-le p4, p3, :cond_2

    .line 352
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    add-int v1, p2, p4

    invoke-virtual {v0, p2, v1}, Lcom/android/ex/editstyledtext/j;->a(II)V

    .line 356
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    if-le p4, p3, :cond_3

    .line 358
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->a()V

    .line 359
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->i()V

    .line 365
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 366
    return-void

    .line 353
    :cond_2
    if-ge p3, p4, :cond_0

    .line 354
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->f()V

    goto :goto_0

    .line 360
    :cond_3
    if-ge p4, p3, :cond_1

    .line 361
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/android/ex/editstyledtext/j;->a(I)V

    goto :goto_1
.end method

.method public onTextContextMenuItem(I)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 280
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getSelectionStart()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getSelectionEnd()I

    move-result v3

    if-eq v1, v3, :cond_0

    move v1, v0

    .line 281
    :goto_0
    sparse-switch p1, :sswitch_data_0

    .line 324
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTextContextMenuItem(I)Z

    move-result v0

    :goto_2
    return v0

    :cond_0
    move v1, v2

    .line 280
    goto :goto_0

    .line 283
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->h()V

    goto :goto_2

    .line 286
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->g()V

    .line 287
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->p()V

    goto :goto_1

    .line 290
    :sswitch_2
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->i()V

    goto :goto_1

    .line 293
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->f()V

    goto :goto_2

    .line 296
    :sswitch_4
    if-eqz v1, :cond_1

    .line 297
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->d()V

    goto :goto_2

    .line 299
    :cond_1
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v1, v2}, Lcom/android/ex/editstyledtext/j;->b(Z)V

    .line 300
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->d()V

    goto :goto_2

    .line 304
    :sswitch_5
    if-eqz v1, :cond_2

    .line 305
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->e()V

    goto :goto_2

    .line 307
    :cond_2
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v1, v2}, Lcom/android/ex/editstyledtext/j;->b(Z)V

    .line 308
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->e()V

    goto :goto_2

    .line 312
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->j()V

    goto :goto_2

    .line 315
    :sswitch_7
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->k()V

    goto :goto_2

    .line 318
    :sswitch_8
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->b()V

    goto :goto_2

    .line 321
    :sswitch_9
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->c()V

    goto :goto_2

    .line 281
    :sswitch_data_0
    .sparse-switch
        0xffff01 -> :sswitch_6
        0xffff02 -> :sswitch_7
        0xffff03 -> :sswitch_8
        0xffff04 -> :sswitch_9
        0x102001f -> :sswitch_0
        0x1020020 -> :sswitch_5
        0x1020021 -> :sswitch_4
        0x1020022 -> :sswitch_3
        0x1020028 -> :sswitch_1
        0x1020029 -> :sswitch_2
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 220
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 221
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->cancelLongPress()V

    .line 222
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->l()Z

    move-result v1

    .line 224
    if-nez v1, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->b()V

    .line 227
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 228
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 229
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 230
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 233
    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getSelectState()I

    move-result v4

    if-nez v4, :cond_1

    .line 234
    if-eqz v1, :cond_2

    .line 235
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/ex/editstyledtext/j;->b(II)V

    .line 242
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/j;->a()V

    .line 243
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/j;->f()V

    .line 247
    :goto_1
    invoke-direct {p0, p1}, Lcom/android/ex/editstyledtext/EditStyledText;->a(Landroid/view/MotionEvent;)V

    .line 248
    return v0

    .line 238
    :cond_2
    iget-object v1, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v1, v2, v3}, Lcom/android/ex/editstyledtext/j;->b(II)V

    goto :goto_0

    .line 245
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public setAlignment(Landroid/text/Layout$Alignment;)V
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0, p1}, Lcom/android/ex/editstyledtext/j;->a(Landroid/text/Layout$Alignment;)V

    .line 703
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 712
    const v0, 0xffffff

    if-eq p1, v0, :cond_0

    .line 713
    invoke-super {p0, p1}, Landroid/widget/EditText;->setBackgroundColor(I)V

    .line 717
    :goto_0
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0, p1}, Lcom/android/ex/editstyledtext/j;->b(I)V

    .line 718
    invoke-direct {p0}, Lcom/android/ex/editstyledtext/EditStyledText;->o()V

    .line 719
    return-void

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/android/ex/editstyledtext/EditStyledText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->j:Lcom/android/ex/editstyledtext/n;

    invoke-virtual {v0, p1}, Lcom/android/ex/editstyledtext/n;->a(Landroid/app/AlertDialog$Builder;)V

    .line 741
    return-void
.end method

.method public setHtml(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->i:Lcom/android/ex/editstyledtext/l;

    invoke-virtual {v0, p1}, Lcom/android/ex/editstyledtext/l;->a(Ljava/lang/String;)V

    .line 732
    return-void
.end method

.method public setItemColor(I)V
    .locals 2

    .prologue
    .line 693
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/ex/editstyledtext/j;->c(IZ)V

    .line 694
    return-void
.end method

.method public setItemSize(I)V
    .locals 2

    .prologue
    .line 684
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/ex/editstyledtext/j;->b(IZ)V

    .line 685
    return-void
.end method

.method public setMarquee(I)V
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->g:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0, p1}, Lcom/android/ex/editstyledtext/j;->c(I)V

    .line 723
    return-void
.end method

.method public setStyledTextHtmlConverter(Lcom/android/ex/editstyledtext/o;)V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/android/ex/editstyledtext/EditStyledText;->i:Lcom/android/ex/editstyledtext/l;

    invoke-virtual {v0, p1}, Lcom/android/ex/editstyledtext/l;->a(Lcom/android/ex/editstyledtext/o;)V

    .line 411
    return-void
.end method
