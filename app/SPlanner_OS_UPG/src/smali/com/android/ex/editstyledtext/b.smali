.class public Lcom/android/ex/editstyledtext/b;
.super Ljava/lang/Object;
.source "EditStyledText.java"


# instance fields
.field private a:Lcom/android/ex/editstyledtext/j;

.field private b:I

.field private c:Ljava/util/HashMap;


# direct methods
.method private c(I)Lcom/android/ex/editstyledtext/c;
    .locals 2

    .prologue
    .line 2876
    iget-object v0, p0, Lcom/android/ex/editstyledtext/b;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2877
    iget-object v0, p0, Lcom/android/ex/editstyledtext/b;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/editstyledtext/c;

    .line 2879
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 2872
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/ex/editstyledtext/b;->b(I)Z

    .line 2873
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 2868
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/ex/editstyledtext/b;->a(I[Ljava/lang/Object;)V

    .line 2869
    return-void
.end method

.method public a(I[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2858
    invoke-direct {p0, p1}, Lcom/android/ex/editstyledtext/b;->c(I)Lcom/android/ex/editstyledtext/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/ex/editstyledtext/c;->a([Ljava/lang/Object;)V

    .line 2859
    iput p1, p0, Lcom/android/ex/editstyledtext/b;->b:I

    .line 2860
    invoke-virtual {p0, p1}, Lcom/android/ex/editstyledtext/b;->b(I)Z

    .line 2861
    return-void
.end method

.method public b(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2888
    const-string v1, "EditModeActions"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--- do the next action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ex/editstyledtext/b;->a:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v3}, Lcom/android/ex/editstyledtext/j;->m()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2890
    invoke-direct {p0, p1}, Lcom/android/ex/editstyledtext/b;->c(I)Lcom/android/ex/editstyledtext/c;

    move-result-object v1

    .line 2891
    if-nez v1, :cond_0

    .line 2892
    const-string v1, "EditModeActions"

    const-string v2, "--- invalid action error."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2909
    :goto_0
    return v0

    .line 2895
    :cond_0
    iget-object v2, p0, Lcom/android/ex/editstyledtext/b;->a:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v2}, Lcom/android/ex/editstyledtext/j;->m()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2897
    :pswitch_0
    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/c;->a()Z

    move-result v0

    goto :goto_0

    .line 2899
    :pswitch_1
    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/c;->b()Z

    move-result v0

    goto :goto_0

    .line 2901
    :pswitch_2
    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/c;->c()Z

    move-result v0

    goto :goto_0

    .line 2903
    :pswitch_3
    iget-object v0, p0, Lcom/android/ex/editstyledtext/b;->a:Lcom/android/ex/editstyledtext/j;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/j;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2904
    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/c;->e()Z

    move-result v0

    goto :goto_0

    .line 2906
    :cond_1
    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/c;->d()Z

    move-result v0

    goto :goto_0

    .line 2895
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
