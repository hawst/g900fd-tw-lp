.class Lcom/android/ex/editstyledtext/l;
.super Ljava/lang/Object;
.source "EditStyledText.java"


# instance fields
.field final synthetic a:Lcom/android/ex/editstyledtext/EditStyledText;

.field private b:Lcom/android/ex/editstyledtext/EditStyledText;

.field private c:Lcom/android/ex/editstyledtext/o;


# direct methods
.method static synthetic a(Lcom/android/ex/editstyledtext/l;)Lcom/android/ex/editstyledtext/EditStyledText;
    .locals 1

    .prologue
    .line 1866
    iget-object v0, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1890
    iget-object v0, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/EditStyledText;->clearComposingText()V

    .line 1891
    iget-object v0, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-static {v0}, Lcom/android/ex/editstyledtext/EditStyledText;->c(Lcom/android/ex/editstyledtext/EditStyledText;)V

    .line 1892
    iget-object v0, p0, Lcom/android/ex/editstyledtext/l;->c:Lcom/android/ex/editstyledtext/o;

    iget-object v1, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lcom/android/ex/editstyledtext/l;->a:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-static {v2}, Lcom/android/ex/editstyledtext/EditStyledText;->b(Lcom/android/ex/editstyledtext/EditStyledText;)I

    move-result v2

    iget-object v3, p0, Lcom/android/ex/editstyledtext/l;->a:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-static {v3}, Lcom/android/ex/editstyledtext/EditStyledText;->d(Lcom/android/ex/editstyledtext/EditStyledText;)F

    move-result v3

    invoke-interface {v0, v1, v6, v2, v3}, Lcom/android/ex/editstyledtext/o;->a(Landroid/text/Spanned;ZIF)Ljava/lang/String;

    move-result-object v0

    .line 1895
    iget-object v1, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/EditStyledText;->getBackgroundColor()I

    move-result v1

    .line 1896
    const-string v2, "<body bgcolor=\"#%02X%02X%02X\">%s</body>"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x3

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1901
    const-string v1, "EditStyledText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--- getPreviewHtml:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-virtual {v3}, Lcom/android/ex/editstyledtext/EditStyledText;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1903
    return-object v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1880
    iget-object v0, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-virtual {v0}, Lcom/android/ex/editstyledtext/EditStyledText;->clearComposingText()V

    .line 1881
    iget-object v0, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-static {v0}, Lcom/android/ex/editstyledtext/EditStyledText;->c(Lcom/android/ex/editstyledtext/EditStyledText;)V

    .line 1882
    iget-object v0, p0, Lcom/android/ex/editstyledtext/l;->c:Lcom/android/ex/editstyledtext/o;

    iget-object v1, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-virtual {v1}, Lcom/android/ex/editstyledtext/EditStyledText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/android/ex/editstyledtext/o;->a(Landroid/text/Spanned;Z)Ljava/lang/String;

    move-result-object v0

    .line 1884
    const-string v1, "EditStyledText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--- getHtml:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1886
    return-object v0
.end method

.method public a(Lcom/android/ex/editstyledtext/o;)V
    .locals 0

    .prologue
    .line 1876
    iput-object p1, p0, Lcom/android/ex/editstyledtext/l;->c:Lcom/android/ex/editstyledtext/o;

    .line 1877
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1926
    iget-object v0, p0, Lcom/android/ex/editstyledtext/l;->c:Lcom/android/ex/editstyledtext/o;

    new-instance v1, Lcom/android/ex/editstyledtext/m;

    invoke-direct {v1, p0}, Lcom/android/ex/editstyledtext/m;-><init>(Lcom/android/ex/editstyledtext/l;)V

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/android/ex/editstyledtext/o;->a(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    .line 1971
    iget-object v1, p0, Lcom/android/ex/editstyledtext/l;->b:Lcom/android/ex/editstyledtext/EditStyledText;

    invoke-virtual {v1, v0}, Lcom/android/ex/editstyledtext/EditStyledText;->setText(Ljava/lang/CharSequence;)V

    .line 1972
    return-void
.end method
