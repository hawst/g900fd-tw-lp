.class public Lcom/amap/api/location/c;
.super Ljava/lang/Object;
.source "IGPSManager.java"


# static fields
.field public static a:Landroid/location/LocationManager;

.field public static b:Z

.field public static c:J

.field public static d:F

.field public static e:Landroid/location/LocationListener;

.field private static f:Lcom/amap/api/location/c;


# instance fields
.field private g:Lcom/amap/api/location/a$a;

.field private h:Lcom/amap/api/location/core/c;

.field private i:Ljava/lang/String;

.field private j:Landroid/location/LocationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    sput-object v2, Lcom/amap/api/location/c;->a:Landroid/location/LocationManager;

    .line 16
    sput-object v2, Lcom/amap/api/location/c;->f:Lcom/amap/api/location/c;

    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/amap/api/location/c;->b:Z

    .line 44
    const-wide/16 v0, 0x7d0

    sput-wide v0, Lcom/amap/api/location/c;->c:J

    .line 45
    const/high16 v0, 0x41200000    # 10.0f

    sput v0, Lcom/amap/api/location/c;->d:F

    .line 46
    sput-object v2, Lcom/amap/api/location/c;->e:Landroid/location/LocationListener;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/location/LocationManager;Lcom/amap/api/location/a$a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Lcom/amap/api/location/d;

    invoke-direct {v0, p0}, Lcom/amap/api/location/d;-><init>(Lcom/amap/api/location/c;)V

    iput-object v0, p0, Lcom/amap/api/location/c;->j:Landroid/location/LocationListener;

    .line 27
    invoke-static {p2}, Lcom/amap/api/location/c;->a(Landroid/location/LocationManager;)V

    .line 28
    iput-object p3, p0, Lcom/amap/api/location/c;->g:Lcom/amap/api/location/a$a;

    .line 29
    invoke-static {p1}, Lcom/amap/api/location/core/c;->a(Landroid/content/Context;)Lcom/amap/api/location/core/c;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/location/c;->h:Lcom/amap/api/location/core/c;

    .line 30
    iget-object v0, p0, Lcom/amap/api/location/c;->h:Lcom/amap/api/location/core/c;

    invoke-virtual {v0, p1}, Lcom/amap/api/location/core/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/location/c;->i:Ljava/lang/String;

    .line 31
    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;Landroid/location/LocationManager;Lcom/amap/api/location/a$a;)Lcom/amap/api/location/c;
    .locals 2

    .prologue
    .line 36
    const-class v1, Lcom/amap/api/location/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/amap/api/location/c;->f:Lcom/amap/api/location/c;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/amap/api/location/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/amap/api/location/c;-><init>(Landroid/content/Context;Landroid/location/LocationManager;Lcom/amap/api/location/a$a;)V

    sput-object v0, Lcom/amap/api/location/c;->f:Lcom/amap/api/location/c;

    .line 40
    :cond_0
    sget-object v0, Lcom/amap/api/location/c;->f:Lcom/amap/api/location/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/amap/api/location/c;)Lcom/amap/api/location/core/c;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/amap/api/location/c;->h:Lcom/amap/api/location/core/c;

    return-object v0
.end method

.method private static a(Landroid/location/LocationListener;)V
    .locals 0

    .prologue
    .line 52
    sput-object p0, Lcom/amap/api/location/c;->e:Landroid/location/LocationListener;

    .line 53
    return-void
.end method

.method private static a(Landroid/location/LocationManager;)V
    .locals 0

    .prologue
    .line 23
    sput-object p0, Lcom/amap/api/location/c;->a:Landroid/location/LocationManager;

    .line 24
    return-void
.end method

.method private static a(Z)V
    .locals 0

    .prologue
    .line 48
    sput-boolean p0, Lcom/amap/api/location/c;->b:Z

    .line 49
    return-void
.end method

.method static synthetic b(Lcom/amap/api/location/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/amap/api/location/c;->i:Ljava/lang/String;

    return-object v0
.end method

.method private static b(JF)V
    .locals 0

    .prologue
    .line 56
    sput-wide p0, Lcom/amap/api/location/c;->c:J

    .line 57
    sput p2, Lcom/amap/api/location/c;->d:F

    .line 58
    return-void
.end method

.method static synthetic c(Lcom/amap/api/location/c;)Lcom/amap/api/location/a$a;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/amap/api/location/c;->g:Lcom/amap/api/location/a$a;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/amap/api/location/c;->a(Z)V

    .line 78
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/amap/api/location/c;->a(Landroid/location/LocationListener;)V

    .line 79
    iget-object v0, p0, Lcom/amap/api/location/c;->j:Landroid/location/LocationListener;

    if-eqz v0, :cond_0

    .line 80
    sget-object v0, Lcom/amap/api/location/c;->a:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/amap/api/location/c;->j:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 87
    :cond_0
    return-void
.end method

.method a(JF)V
    .locals 7

    .prologue
    .line 62
    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Lcom/amap/api/location/c;->a(Z)V

    .line 63
    iget-object v0, p0, Lcom/amap/api/location/c;->j:Landroid/location/LocationListener;

    invoke-static {v0}, Lcom/amap/api/location/c;->a(Landroid/location/LocationListener;)V

    .line 64
    invoke-static {p1, p2, p3}, Lcom/amap/api/location/c;->b(JF)V

    .line 65
    sget-object v0, Lcom/amap/api/location/c;->a:Landroid/location/LocationManager;

    const-string v1, "gps"

    iget-object v5, p0, Lcom/amap/api/location/c;->j:Landroid/location/LocationListener;

    move-wide v2, p1

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 72
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
