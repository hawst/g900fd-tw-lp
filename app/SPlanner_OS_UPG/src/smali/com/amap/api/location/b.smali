.class public Lcom/amap/api/location/b;
.super Ljava/lang/Object;
.source "IAPSManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static e:Lcom/amap/api/location/b;


# instance fields
.field private a:Lcom/a/am;

.field private b:Z

.field private c:Ljava/lang/Thread;

.field private d:Landroid/content/Context;

.field private f:J

.field private g:Lcom/amap/api/location/a$a;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/amap/api/location/a$a;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    .line 27
    iput-boolean v2, p0, Lcom/amap/api/location/b;->b:Z

    .line 28
    iput-object v0, p0, Lcom/amap/api/location/b;->c:Ljava/lang/Thread;

    .line 31
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/amap/api/location/b;->f:J

    .line 37
    iput-boolean v2, p0, Lcom/amap/api/location/b;->b:Z

    .line 38
    iput-object p1, p0, Lcom/amap/api/location/b;->d:Landroid/content/Context;

    .line 39
    invoke-static {}, Lcom/a/ak;->a()Lcom/a/am;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    .line 40
    invoke-static {p1}, Lcom/amap/api/location/core/c;->a(Landroid/content/Context;)Lcom/amap/api/location/core/c;

    .line 41
    iget-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    invoke-interface {v0, p1}, Lcom/a/am;->a(Landroid/content/Context;)V

    .line 42
    iget-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "api_serverSDK_130905##S128DF1572465B890OE3F7A13167KLEI##"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/amap/api/location/core/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/amap/api/location/core/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/am;->a(Ljava/lang/String;)V

    .line 46
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 48
    :try_start_0
    const-string v0, "key"

    invoke-static {p1}, Lcom/amap/api/location/core/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 49
    const-string v0, "X-INFO"

    invoke-static {p1}, Lcom/amap/api/location/core/c;->a(Landroid/content/Context;)Lcom/amap/api/location/core/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/location/core/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 51
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 52
    invoke-static {p1}, Lcom/amap/api/location/core/c;->a(Landroid/content/Context;)Lcom/amap/api/location/core/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/location/core/c;->e()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 55
    :try_start_1
    const-string v3, "ex"

    const-string v4, "UTF-8"

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/ae;->b([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 63
    :goto_0
    :try_start_2
    const-string v0, "X-BIZ"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 64
    const-string v0, "User-Agent"

    const-string v2, "AMAP Location SDK Android 1.1.2"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 71
    :goto_1
    iget-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    invoke-interface {v0, v1}, Lcom/a/am;->a(Lorg/json/JSONObject;)V

    .line 72
    iput-object p2, p0, Lcom/amap/api/location/b;->g:Lcom/amap/api/location/a$a;

    .line 73
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 61
    :try_start_3
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 69
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method private a(Lcom/a/af;)Lcom/amap/api/location/AMapLocation;
    .locals 4

    .prologue
    .line 200
    new-instance v0, Lcom/amap/api/location/AMapLocation;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/amap/api/location/AMapLocation;-><init>(Ljava/lang/String;)V

    .line 201
    const-string v1, "lbs"

    invoke-virtual {v0, v1}, Lcom/amap/api/location/AMapLocation;->setProvider(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p1}, Lcom/a/af;->d()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/amap/api/location/AMapLocation;->setLatitude(D)V

    .line 203
    invoke-virtual {p1}, Lcom/a/af;->c()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/amap/api/location/AMapLocation;->setLongitude(D)V

    .line 204
    invoke-virtual {p1}, Lcom/a/af;->e()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/location/AMapLocation;->setAccuracy(F)V

    .line 205
    invoke-virtual {p1}, Lcom/a/af;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/amap/api/location/AMapLocation;->setTime(J)V

    .line 206
    invoke-virtual {p1}, Lcom/a/af;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/location/AMapLocation;->setPoiId(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p1}, Lcom/a/af;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/location/AMapLocation;->setFloor(Ljava/lang/String;)V

    .line 208
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 209
    const-string v2, "citycode"

    invoke-virtual {p1}, Lcom/a/af;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v2, "desc"

    invoke-virtual {p1}, Lcom/a/af;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v2, "adcode"

    invoke-virtual {p1}, Lcom/a/af;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {v0, v1}, Lcom/amap/api/location/AMapLocation;->setExtras(Landroid/os/Bundle;)V

    .line 213
    invoke-virtual {p1}, Lcom/a/af;->i()Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-virtual {p1}, Lcom/a/af;->j()Ljava/lang/String;

    move-result-object v2

    .line 215
    invoke-virtual {p1}, Lcom/a/af;->k()Ljava/lang/String;

    move-result-object v3

    .line 217
    :try_start_0
    invoke-direct {p0, v0, v1, v3, v2}, Lcom/amap/api/location/b;->a(Lcom/amap/api/location/AMapLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    return-object v0

    .line 218
    :catch_0
    move-exception v1

    .line 222
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static declared-synchronized a(Landroid/content/Context;Lcom/amap/api/location/a$a;)Lcom/amap/api/location/b;
    .locals 2

    .prologue
    .line 77
    const-class v1, Lcom/amap/api/location/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/amap/api/location/b;->e:Lcom/amap/api/location/b;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/amap/api/location/b;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/location/b;-><init>(Landroid/content/Context;Lcom/amap/api/location/a$a;)V

    sput-object v0, Lcom/amap/api/location/b;->e:Lcom/amap/api/location/b;

    .line 80
    :cond_0
    sget-object v0, Lcom/amap/api/location/b;->e:Lcom/amap/api/location/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/amap/api/location/AMapLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 229
    const-string v0, " "

    invoke-virtual {p4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 230
    invoke-virtual {p1, p2}, Lcom/amap/api/location/AMapLocation;->setCityCode(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p1, p3}, Lcom/amap/api/location/AMapLocation;->setAdCode(Ljava/lang/String;)V

    .line 232
    const-string v1, ""

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, p2}, Lcom/amap/api/location/b;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    array-length v1, v0

    if-le v1, v5, :cond_0

    .line 234
    aget-object v1, v0, v3

    invoke-virtual {p1, v1}, Lcom/amap/api/location/AMapLocation;->setCity(Ljava/lang/String;)V

    .line 235
    aget-object v0, v0, v4

    invoke-virtual {p1, v0}, Lcom/amap/api/location/AMapLocation;->setDistrict(Ljava/lang/String;)V

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    array-length v1, v0

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    .line 240
    aget-object v1, v0, v3

    invoke-virtual {p1, v1}, Lcom/amap/api/location/AMapLocation;->setProvince(Ljava/lang/String;)V

    .line 241
    aget-object v1, v0, v4

    invoke-virtual {p1, v1}, Lcom/amap/api/location/AMapLocation;->setCity(Ljava/lang/String;)V

    .line 242
    aget-object v0, v0, v5

    invoke-virtual {p1, v0}, Lcom/amap/api/location/AMapLocation;->setDistrict(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 248
    const-string v0, "010"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "021"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "022"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "023"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    :cond_0
    const/4 v0, 0x1

    .line 252
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    sput-object v0, Lcom/amap/api/location/b;->e:Lcom/amap/api/location/b;

    .line 95
    return-void
.end method

.method private d()Lcom/a/af;
    .locals 1

    .prologue
    .line 168
    .line 169
    invoke-direct {p0}, Lcom/amap/api/location/b;->e()Lcom/a/af;

    move-result-object v0

    .line 170
    return-object v0
.end method

.method private e()Lcom/a/af;
    .locals 2

    .prologue
    .line 174
    const/4 v0, 0x0

    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    invoke-interface {v1}, Lcom/a/am;->b()Lcom/a/af;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 186
    :cond_0
    :goto_0
    return-object v0

    .line 180
    :catch_0
    move-exception v1

    .line 184
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private f()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 190
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 191
    sget-wide v4, Lcom/amap/api/location/a;->b:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x5

    iget-wide v6, p0, Lcom/amap/api/location/b;->f:J

    mul-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 192
    sput-boolean v0, Lcom/amap/api/location/a;->a:Z

    .line 193
    const/4 v0, 0x1

    .line 195
    :cond_0
    return v0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/location/b;->b:Z

    .line 85
    iget-object v0, p0, Lcom/amap/api/location/b;->c:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/amap/api/location/b;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    invoke-interface {v0}, Lcom/a/am;->c()V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    .line 90
    invoke-static {}, Lcom/amap/api/location/b;->c()V

    .line 91
    return-void
.end method

.method a(J)V
    .locals 3

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/amap/api/location/b;->f:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 163
    iput-wide p1, p0, Lcom/amap/api/location/b;->f:J

    .line 165
    :cond_0
    return-void
.end method

.method a(Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    invoke-interface {v0, p1}, Lcom/a/am;->a(Landroid/app/PendingIntent;)V

    .line 273
    return-void
.end method

.method a(Lcom/a/al;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    invoke-interface {v0, p1, p2}, Lcom/a/am;->a(Lcom/a/al;Landroid/app/PendingIntent;)V

    .line 264
    return-void
.end method

.method b()I
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/amap/api/location/b;->a:Lcom/a/am;

    invoke-interface {v0}, Lcom/a/am;->e()I

    move-result v0

    .line 286
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 100
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 101
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/location/b;->c:Ljava/lang/Thread;

    .line 102
    :goto_0
    iget-boolean v0, p0, Lcom/amap/api/location/b;->b:Z

    if-eqz v0, :cond_5

    .line 103
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/location/b;->c:Ljava/lang/Thread;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_6

    .line 105
    const/4 v1, 0x0

    .line 107
    :try_start_1
    sget-boolean v0, Lcom/amap/api/location/a;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/amap/api/location/b;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-boolean v0, Lcom/amap/api/location/a;->c:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_9

    .line 110
    :cond_1
    :try_start_2
    iget-wide v2, p0, Lcom/amap/api/location/b;->f:J

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 131
    if-eqz v1, :cond_3

    :try_start_3
    sget-boolean v0, Lcom/amap/api/location/a;->c:Z

    if-eqz v0, :cond_3

    .line 132
    sget-boolean v0, Lcom/amap/api/location/a;->a:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/amap/api/location/b;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 133
    :cond_2
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 134
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 135
    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->what:I

    .line 136
    iget-object v1, p0, Lcom/amap/api/location/b;->g:Lcom/amap/api/location/a$a;

    invoke-virtual {v1, v0}, Lcom/amap/api/location/a$a;->sendMessage(Landroid/os/Message;)Z

    .line 139
    :cond_3
    invoke-static {}, Lcom/amap/api/location/core/a;->a()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 141
    iget-object v0, p0, Lcom/amap/api/location/b;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/location/core/a;->a(Landroid/content/Context;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_6

    .line 147
    :cond_4
    :try_start_4
    iget-wide v0, p0, Lcom/amap/api/location/b;->f:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 159
    :cond_5
    :goto_1
    return-void

    .line 111
    :catch_1
    move-exception v0

    .line 131
    if-eqz v1, :cond_7

    :try_start_5
    sget-boolean v0, Lcom/amap/api/location/a;->c:Z

    if-eqz v0, :cond_7

    .line 132
    sget-boolean v0, Lcom/amap/api/location/a;->a:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/amap/api/location/b;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 133
    :cond_6
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 134
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 135
    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->what:I

    .line 136
    iget-object v1, p0, Lcom/amap/api/location/b;->g:Lcom/amap/api/location/a$a;

    invoke-virtual {v1, v0}, Lcom/amap/api/location/a$a;->sendMessage(Landroid/os/Message;)Z

    .line 139
    :cond_7
    invoke-static {}, Lcom/amap/api/location/core/a;->a()I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 141
    iget-object v0, p0, Lcom/amap/api/location/b;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/location/core/a;->a(Landroid/content/Context;)Z
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_6

    .line 147
    :cond_8
    :try_start_6
    iget-wide v0, p0, Lcom/amap/api/location/b;->f:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    .line 148
    :catch_2
    move-exception v0

    goto :goto_1

    .line 119
    :cond_9
    :try_start_7
    invoke-direct {p0}, Lcom/amap/api/location/b;->d()Lcom/a/af;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_13

    .line 121
    invoke-direct {p0, v0}, Lcom/amap/api/location/b;->a(Lcom/a/af;)Lcom/amap/api/location/AMapLocation;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    .line 131
    :goto_2
    if-eqz v0, :cond_b

    :try_start_8
    sget-boolean v1, Lcom/amap/api/location/a;->c:Z

    if-eqz v1, :cond_b

    .line 132
    sget-boolean v1, Lcom/amap/api/location/a;->a:Z

    if-eqz v1, :cond_a

    invoke-direct {p0}, Lcom/amap/api/location/b;->f()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 133
    :cond_a
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 134
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 135
    const/16 v0, 0x64

    iput v0, v1, Landroid/os/Message;->what:I

    .line 136
    iget-object v0, p0, Lcom/amap/api/location/b;->g:Lcom/amap/api/location/a$a;

    invoke-virtual {v0, v1}, Lcom/amap/api/location/a$a;->sendMessage(Landroid/os/Message;)Z

    .line 139
    :cond_b
    invoke-static {}, Lcom/amap/api/location/core/a;->a()I

    move-result v0

    if-ne v0, v4, :cond_c

    .line 141
    iget-object v0, p0, Lcom/amap/api/location/b;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/location/core/a;->a(Landroid/content/Context;)Z
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_6

    .line 147
    :cond_c
    :try_start_9
    iget-wide v0, p0, Lcom/amap/api/location/b;->f:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_0

    .line 148
    :catch_3
    move-exception v0

    goto :goto_1

    .line 125
    :catch_4
    move-exception v0

    .line 129
    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 131
    if-eqz v1, :cond_e

    :try_start_b
    sget-boolean v0, Lcom/amap/api/location/a;->c:Z

    if-eqz v0, :cond_e

    .line 132
    sget-boolean v0, Lcom/amap/api/location/a;->a:Z

    if-eqz v0, :cond_d

    invoke-direct {p0}, Lcom/amap/api/location/b;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 133
    :cond_d
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 134
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 135
    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->what:I

    .line 136
    iget-object v1, p0, Lcom/amap/api/location/b;->g:Lcom/amap/api/location/a$a;

    invoke-virtual {v1, v0}, Lcom/amap/api/location/a$a;->sendMessage(Landroid/os/Message;)Z

    .line 139
    :cond_e
    invoke-static {}, Lcom/amap/api/location/core/a;->a()I

    move-result v0

    if-ne v0, v4, :cond_f

    .line 141
    iget-object v0, p0, Lcom/amap/api/location/b;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/location/core/a;->a(Landroid/content/Context;)Z
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_6

    .line 147
    :cond_f
    :try_start_c
    iget-wide v0, p0, Lcom/amap/api/location/b;->f:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_0

    .line 148
    :catch_5
    move-exception v0

    goto/16 :goto_1

    .line 131
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_11

    :try_start_d
    sget-boolean v2, Lcom/amap/api/location/a;->c:Z

    if-eqz v2, :cond_11

    .line 132
    sget-boolean v2, Lcom/amap/api/location/a;->a:Z

    if-eqz v2, :cond_10

    invoke-direct {p0}, Lcom/amap/api/location/b;->f()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 133
    :cond_10
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 134
    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 135
    const/16 v1, 0x64

    iput v1, v2, Landroid/os/Message;->what:I

    .line 136
    iget-object v1, p0, Lcom/amap/api/location/b;->g:Lcom/amap/api/location/a$a;

    invoke-virtual {v1, v2}, Lcom/amap/api/location/a$a;->sendMessage(Landroid/os/Message;)Z

    .line 139
    :cond_11
    invoke-static {}, Lcom/amap/api/location/core/a;->a()I

    move-result v1

    if-ne v1, v4, :cond_12

    .line 141
    iget-object v1, p0, Lcom/amap/api/location/b;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/amap/api/location/core/a;->a(Landroid/content/Context;)Z
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_6

    .line 147
    :cond_12
    :try_start_e
    iget-wide v2, p0, Lcom/amap/api/location/b;->f:J

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_7

    .line 149
    :try_start_f
    throw v0
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_6

    .line 153
    :catch_6
    move-exception v0

    .line 157
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_1

    .line 148
    :catch_7
    move-exception v0

    goto/16 :goto_1

    :cond_13
    move-object v0, v1

    goto/16 :goto_2
.end method
