.class public Lcom/amap/api/location/a;
.super Ljava/lang/Object;
.source "AMapLocationManager.java"


# static fields
.field static a:Z

.field static b:J

.field static c:Z

.field static d:Z

.field private static f:Ljava/util/Vector;

.field private static h:Lcom/amap/api/location/c;

.field private static i:Lcom/amap/api/location/b;

.field private static j:Lcom/amap/api/location/a;


# instance fields
.field private e:Landroid/content/Context;

.field private g:Lcom/amap/api/location/a$a;

.field private k:Lcom/amap/api/location/AMapLocation;

.field private l:Ljava/lang/Thread;

.field private m:J

.field private n:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 17
    sput-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    .line 20
    sput-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    .line 21
    sput-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    .line 22
    sput-object v0, Lcom/amap/api/location/a;->j:Lcom/amap/api/location/a;

    .line 25
    const/4 v0, 0x0

    sput-boolean v0, Lcom/amap/api/location/a;->a:Z

    .line 27
    sput-boolean v1, Lcom/amap/api/location/a;->c:Z

    .line 28
    sput-boolean v1, Lcom/amap/api/location/a;->d:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/location/LocationManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/location/a;->g:Lcom/amap/api/location/a$a;

    .line 76
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/amap/api/location/a;->m:J

    .line 77
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/amap/api/location/a;->n:F

    .line 32
    iput-object p1, p0, Lcom/amap/api/location/a;->e:Landroid/content/Context;

    .line 33
    invoke-static {}, Lcom/amap/api/location/a;->e()V

    .line 34
    new-instance v0, Lcom/amap/api/location/a$a;

    invoke-direct {v0, p0}, Lcom/amap/api/location/a$a;-><init>(Lcom/amap/api/location/a;)V

    iput-object v0, p0, Lcom/amap/api/location/a;->g:Lcom/amap/api/location/a$a;

    .line 35
    iget-object v0, p0, Lcom/amap/api/location/a;->g:Lcom/amap/api/location/a$a;

    invoke-static {p1, v0}, Lcom/amap/api/location/b;->a(Landroid/content/Context;Lcom/amap/api/location/a$a;)Lcom/amap/api/location/b;

    move-result-object v0

    sput-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    .line 36
    iget-object v0, p0, Lcom/amap/api/location/a;->g:Lcom/amap/api/location/a$a;

    invoke-static {p1, p2, v0}, Lcom/amap/api/location/c;->a(Landroid/content/Context;Landroid/location/LocationManager;Lcom/amap/api/location/a$a;)Lcom/amap/api/location/c;

    move-result-object v0

    sput-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    .line 38
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/amap/api/location/a;->b(Z)V

    .line 39
    sput-boolean v2, Lcom/amap/api/location/a;->c:Z

    .line 40
    sput-boolean v2, Lcom/amap/api/location/a;->d:Z

    .line 42
    return-void
.end method

.method static synthetic a(Lcom/amap/api/location/a;)Lcom/amap/api/location/AMapLocation;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/amap/api/location/a;->k:Lcom/amap/api/location/AMapLocation;

    return-object v0
.end method

.method static synthetic a(Lcom/amap/api/location/a;Lcom/amap/api/location/AMapLocation;)Lcom/amap/api/location/AMapLocation;
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Lcom/amap/api/location/a;->k:Lcom/amap/api/location/AMapLocation;

    return-object p1
.end method

.method static declared-synchronized a(Landroid/content/Context;Landroid/location/LocationManager;)Lcom/amap/api/location/a;
    .locals 2

    .prologue
    .line 64
    const-class v1, Lcom/amap/api/location/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/amap/api/location/a;->j:Lcom/amap/api/location/a;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/amap/api/location/a;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/location/a;-><init>(Landroid/content/Context;Landroid/location/LocationManager;)V

    sput-object v0, Lcom/amap/api/location/a;->j:Lcom/amap/api/location/a;

    .line 67
    :cond_0
    sget-object v0, Lcom/amap/api/location/a;->j:Lcom/amap/api/location/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/amap/api/location/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/amap/api/location/a;->e:Landroid/content/Context;

    return-object v0
.end method

.method static b(Z)V
    .locals 0

    .prologue
    .line 165
    sput-boolean p0, Lcom/amap/api/location/a;->a:Z

    .line 166
    return-void
.end method

.method private static c(Z)V
    .locals 0

    .prologue
    .line 105
    sput-boolean p0, Lcom/amap/api/location/a;->c:Z

    .line 106
    return-void
.end method

.method static synthetic d()Ljava/util/Vector;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    return-object v0
.end method

.method private static d(Z)V
    .locals 0

    .prologue
    .line 144
    sput-boolean p0, Lcom/amap/api/location/a;->d:Z

    .line 145
    return-void
.end method

.method private static e()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    .line 45
    return-void
.end method


# virtual methods
.method a()Lcom/amap/api/location/AMapLocation;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/amap/api/location/a;->k:Lcom/amap/api/location/AMapLocation;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/amap/api/location/a;->k:Lcom/amap/api/location/AMapLocation;

    .line 74
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/amap/api/location/a;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/location/core/d;->b(Landroid/content/Context;)Lcom/amap/api/location/AMapLocation;

    move-result-object v0

    goto :goto_0
.end method

.method a(DDFJLandroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Lcom/a/al;

    invoke-direct {v0}, Lcom/a/al;-><init>()V

    .line 51
    iput-wide p1, v0, Lcom/a/al;->b:D

    .line 52
    iput-wide p3, v0, Lcom/a/al;->a:D

    .line 53
    iput p5, v0, Lcom/a/al;->c:F

    .line 54
    invoke-virtual {v0, p6, p7}, Lcom/a/al;->a(J)V

    .line 55
    sget-object v1, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    invoke-virtual {v1, v0, p8}, Lcom/amap/api/location/b;->a(Lcom/a/al;Landroid/app/PendingIntent;)V

    .line 56
    return-void
.end method

.method a(JFLcom/amap/api/location/AMapLocationListener;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 80
    iput-wide p1, p0, Lcom/amap/api/location/a;->m:J

    .line 81
    iput p3, p0, Lcom/amap/api/location/a;->n:F

    .line 82
    if-eqz p4, :cond_0

    .line 83
    new-instance v1, Lcom/amap/api/location/f;

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/amap/api/location/f;-><init>(JFLcom/amap/api/location/AMapLocationListener;Ljava/lang/String;)V

    .line 85
    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_0
    const-string v0, "gps"

    invoke-virtual {v0, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/amap/api/location/c;->a(JF)V

    .line 102
    :cond_1
    :goto_0
    return-void

    .line 90
    :cond_2
    const-string v0, "lbs"

    invoke-virtual {v0, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    sget-boolean v0, Lcom/amap/api/location/a;->d:Z

    if-eqz v0, :cond_3

    .line 92
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/amap/api/location/c;->a(JF)V

    .line 95
    :cond_3
    sget-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    invoke-virtual {v0, p1, p2}, Lcom/amap/api/location/b;->a(J)V

    .line 96
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/amap/api/location/a;->c(Z)V

    .line 97
    iget-object v0, p0, Lcom/amap/api/location/a;->l:Ljava/lang/Thread;

    if-nez v0, :cond_1

    .line 98
    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/amap/api/location/a;->l:Ljava/lang/Thread;

    .line 99
    iget-object v0, p0, Lcom/amap/api/location/a;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method a(Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    invoke-virtual {v0, p1}, Lcom/amap/api/location/b;->a(Landroid/app/PendingIntent;)V

    .line 60
    return-void
.end method

.method a(Lcom/amap/api/location/AMapLocationListener;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 108
    .line 109
    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v3

    .line 111
    :goto_0
    if-ge v1, v2, :cond_0

    .line 112
    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/location/f;

    .line 113
    iget-object v4, v0, Lcom/amap/api/location/f;->a:Lcom/amap/api/location/AMapLocationListener;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 114
    sget-object v4, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    invoke-virtual {v4, v0}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 115
    add-int/lit8 v2, v2, -0x1

    .line 116
    add-int/lit8 v0, v1, -0x1

    move v1, v2

    .line 111
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 120
    :cond_0
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 121
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    invoke-virtual {v0}, Lcom/amap/api/location/c;->a()V

    .line 122
    invoke-static {v3}, Lcom/amap/api/location/a;->b(Z)V

    .line 123
    invoke-static {v3}, Lcom/amap/api/location/a;->c(Z)V

    .line 124
    iget-object v0, p0, Lcom/amap/api/location/a;->l:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/amap/api/location/a;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/location/a;->l:Ljava/lang/Thread;

    .line 129
    :cond_1
    return-void

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method a(Z)V
    .locals 4

    .prologue
    .line 132
    invoke-static {p1}, Lcom/amap/api/location/a;->d(Z)V

    .line 134
    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 135
    if-eqz p1, :cond_1

    .line 136
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    invoke-virtual {v0}, Lcom/amap/api/location/c;->a()V

    .line 137
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    iget-wide v2, p0, Lcom/amap/api/location/a;->m:J

    iget v1, p0, Lcom/amap/api/location/a;->n:F

    invoke-virtual {v0, v2, v3, v1}, Lcom/amap/api/location/c;->a(JF)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    invoke-virtual {v0}, Lcom/amap/api/location/c;->a()V

    goto :goto_0
.end method

.method declared-synchronized b()V
    .locals 1

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    if-eqz v0, :cond_0

    .line 149
    sget-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    invoke-virtual {v0}, Lcom/amap/api/location/c;->a()V

    .line 150
    const/4 v0, 0x0

    sput-object v0, Lcom/amap/api/location/a;->h:Lcom/amap/api/location/c;

    .line 152
    :cond_0
    sget-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    if-eqz v0, :cond_1

    .line 153
    sget-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    invoke-virtual {v0}, Lcom/amap/api/location/b;->a()V

    .line 154
    const/4 v0, 0x0

    sput-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    .line 156
    :cond_1
    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    if-eqz v0, :cond_2

    .line 157
    sget-object v0, Lcom/amap/api/location/a;->f:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 159
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/amap/api/location/a;->b(Z)V

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/location/a;->l:Ljava/lang/Thread;

    .line 161
    const/4 v0, 0x0

    sput-object v0, Lcom/amap/api/location/a;->j:Lcom/amap/api/location/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c()I
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    if-eqz v0, :cond_0

    .line 206
    sget-object v0, Lcom/amap/api/location/a;->i:Lcom/amap/api/location/b;

    invoke-virtual {v0}, Lcom/amap/api/location/b;->b()I

    move-result v0

    .line 208
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
