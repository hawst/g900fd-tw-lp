.class public Lcom/amap/api/services/poisearch/PoiItemDetail;
.super Lcom/amap/api/services/core/PoiItem;
.source "PoiItemDetail.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:Lcom/amap/api/services/poisearch/Dining;

.field b:Lcom/amap/api/services/poisearch/Hotel;

.field c:Lcom/amap/api/services/poisearch/Cinema;

.field d:Lcom/amap/api/services/poisearch/Scenic;

.field e:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

.field private f:Ljava/util/List;

.field private g:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/amap/api/services/poisearch/h;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/h;-><init>()V

    sput-object v0, Lcom/amap/api/services/poisearch/PoiItemDetail;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/amap/api/services/core/PoiItem;-><init>(Landroid/os/Parcel;)V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->f:Ljava/util/List;

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->g:Ljava/util/List;

    .line 92
    const-class v0, Lcom/amap/api/services/poisearch/Groupbuy;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->f:Ljava/util/List;

    .line 93
    const-class v0, Lcom/amap/api/services/poisearch/Discount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->g:Ljava/util/List;

    .line 94
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/amap/api/services/poisearch/h;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/amap/api/services/poisearch/PoiItemDetail;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/amap/api/services/core/LatLonPoint;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/amap/api/services/core/PoiItem;-><init>(Ljava/lang/String;Lcom/amap/api/services/core/LatLonPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->f:Ljava/util/List;

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->g:Ljava/util/List;

    .line 27
    return-void
.end method


# virtual methods
.method protected addDiscount(Lcom/amap/api/services/poisearch/Discount;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method protected addGroupbuy(Lcom/amap/api/services/poisearch/Groupbuy;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public getCinema()Lcom/amap/api/services/poisearch/Cinema;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->c:Lcom/amap/api/services/poisearch/Cinema;

    return-object v0
.end method

.method public getDeepType()Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->e:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    return-object v0
.end method

.method public getDining()Lcom/amap/api/services/poisearch/Dining;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->a:Lcom/amap/api/services/poisearch/Dining;

    return-object v0
.end method

.method public getDiscounts()Ljava/util/List;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->g:Ljava/util/List;

    return-object v0
.end method

.method public getGroupbuys()Ljava/util/List;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->f:Ljava/util/List;

    return-object v0
.end method

.method public getHotel()Lcom/amap/api/services/poisearch/Hotel;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->b:Lcom/amap/api/services/poisearch/Hotel;

    return-object v0
.end method

.method public getScenic()Lcom/amap/api/services/poisearch/Scenic;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->d:Lcom/amap/api/services/poisearch/Scenic;

    return-object v0
.end method

.method protected initDiscounts(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 51
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 58
    :cond_0
    return-void

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 55
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/poisearch/Discount;

    .line 56
    iget-object v2, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected initGroupbuys(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 34
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 40
    :cond_0
    return-void

    .line 37
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/poisearch/Groupbuy;

    .line 38
    iget-object v2, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0, p1, p2}, Lcom/amap/api/services/core/PoiItem;->writeToParcel(Landroid/os/Parcel;I)V

    .line 112
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 113
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiItemDetail;->g:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 114
    return-void
.end method
