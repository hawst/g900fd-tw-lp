.class public Lcom/amap/api/services/poisearch/j;
.super Lcom/amap/api/services/poisearch/g;
.source "PoiSearchIdHandler.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/net/Proxy;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/amap/api/services/poisearch/g;-><init>(Ljava/lang/Object;Ljava/net/Proxy;)V

    .line 22
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 347
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 348
    const-string v0, "id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/amap/api/services/poisearch/j;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    const-string v0, "&output=json"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    const-string v0, "&extensions=all"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "&key="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/amap/api/services/core/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/amap/api/services/poisearch/Discount;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 309
    invoke-direct {p0, p2}, Lcom/amap/api/services/poisearch/j;->d(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v0

    .line 310
    invoke-virtual {p1, v0}, Lcom/amap/api/services/poisearch/Discount;->initPhotos(Ljava/util/List;)V

    .line 311
    return-void
.end method

.method private a(Lcom/amap/api/services/poisearch/Groupbuy;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 278
    invoke-direct {p0, p2}, Lcom/amap/api/services/poisearch/j;->d(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v0

    .line 279
    invoke-virtual {p1, v0}, Lcom/amap/api/services/poisearch/Groupbuy;->initPhotos(Ljava/util/List;)V

    .line 280
    return-void
.end method

.method private a(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 226
    if-nez p2, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiItemDetail;->isGroupbuyInfo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 230
    invoke-direct {p0, p1, p2}, Lcom/amap/api/services/poisearch/j;->b(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V

    .line 232
    :cond_2
    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiItemDetail;->isDiscountInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-direct {p0, p1, p2}, Lcom/amap/api/services/poisearch/j;->c(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V

    goto :goto_0
.end method

.method private a(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 75
    if-nez p2, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    const-string v0, "type"

    invoke-virtual {p0, p2, v0}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    const-string v1, "hotel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 80
    invoke-direct {p0, p1, p2, p3}, Lcom/amap/api/services/poisearch/j;->d(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    .line 82
    :cond_2
    const-string v1, "dining"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/amap/api/services/poisearch/j;->e(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    .line 85
    :cond_3
    const-string v1, "cinema"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 86
    invoke-direct {p0, p1, p2, p3}, Lcom/amap/api/services/poisearch/j;->c(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    .line 89
    :cond_4
    const-string v1, "scenic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-direct {p0, p1, p2, p3}, Lcom/amap/api/services/poisearch/j;->b(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    goto :goto_0
.end method

.method private b(Lorg/json/JSONObject;)Lcom/amap/api/services/poisearch/PoiItemDetail;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 49
    if-nez p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-object v0

    .line 52
    :cond_1
    const-string v1, "pois"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    const-string v1, "pois"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 59
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 60
    invoke-virtual {p0, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;)Lcom/amap/api/services/poisearch/PoiItemDetail;

    move-result-object v0

    .line 61
    const-string v2, "rich_content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 62
    const-string v2, "rich_content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 63
    invoke-direct {p0, v0, v2}, Lcom/amap/api/services/poisearch/j;->a(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V

    .line 66
    :cond_2
    const-string v2, "deep_info"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    const-string v2, "deep_info"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 68
    invoke-direct {p0, v0, v2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    goto :goto_0
.end method

.method private b(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    .line 239
    if-nez p2, :cond_1

    .line 274
    :cond_0
    return-void

    .line 242
    :cond_1
    const-string v0, "groupbuys"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    const-string v0, "groupbuys"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 246
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 247
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 248
    new-instance v3, Lcom/amap/api/services/poisearch/Groupbuy;

    invoke-direct {v3}, Lcom/amap/api/services/poisearch/Groupbuy;-><init>()V

    .line 249
    const-string v4, "typecode"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setTypeCode(Ljava/lang/String;)V

    .line 250
    const-string v4, "type"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setTypeDes(Ljava/lang/String;)V

    .line 251
    const-string v4, "detail"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setDetail(Ljava/lang/String;)V

    .line 252
    const-string v4, "start_time"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/services/core/c;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 254
    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setStartTime(Ljava/util/Date;)V

    .line 255
    const-string v4, "end_time"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/services/core/c;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 257
    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setEndTime(Ljava/util/Date;)V

    .line 259
    const-string v4, "num"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/j;->f(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setCount(I)V

    .line 260
    const-string v4, "sold_num"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/j;->f(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setSoldCount(I)V

    .line 261
    const-string v4, "original_price"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/j;->g(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setOriginalPrice(F)V

    .line 263
    const-string v4, "groupbuy_price"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/j;->g(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setGroupbuyPrice(F)V

    .line 265
    const-string v4, "discount"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/j;->g(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setDiscount(F)V

    .line 266
    const-string v4, "ticket_address"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setTicketAddress(Ljava/lang/String;)V

    .line 267
    const-string v4, "ticket_tel"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setTicketTel(Ljava/lang/String;)V

    .line 268
    const-string v4, "url"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setUrl(Ljava/lang/String;)V

    .line 269
    const-string v4, "provider"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setProvider(Ljava/lang/String;)V

    .line 270
    invoke-direct {p0, v3, v2}, Lcom/amap/api/services/poisearch/j;->a(Lcom/amap/api/services/poisearch/Groupbuy;Lorg/json/JSONObject;)V

    .line 271
    invoke-virtual {p1, v3}, Lcom/amap/api/services/poisearch/PoiItemDetail;->addGroupbuy(Lcom/amap/api/services/poisearch/Groupbuy;)V

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method private b(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lcom/amap/api/services/poisearch/Scenic;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/Scenic;-><init>()V

    .line 98
    const-string v1, "intro"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->a(Ljava/lang/String;)V

    .line 99
    const-string v1, "rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->b(Ljava/lang/String;)V

    .line 100
    const-string v1, "deepsrc"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->c(Ljava/lang/String;)V

    .line 101
    const-string v1, "level"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->d(Ljava/lang/String;)V

    .line 102
    const-string v1, "price"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->e(Ljava/lang/String;)V

    .line 103
    const-string v1, "season"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->f(Ljava/lang/String;)V

    .line 104
    const-string v1, "recommend"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->g(Ljava/lang/String;)V

    .line 105
    const-string v1, "theme"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->h(Ljava/lang/String;)V

    .line 106
    const-string v1, "ordering_wap_url"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->i(Ljava/lang/String;)V

    .line 107
    const-string v1, "ordering_web_url"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->j(Ljava/lang/String;)V

    .line 108
    const-string v1, "opentime_GDF"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->k(Ljava/lang/String;)V

    .line 109
    const-string v1, "opentime"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->l(Ljava/lang/String;)V

    .line 110
    invoke-direct {p0, p2}, Lcom/amap/api/services/poisearch/j;->d(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Scenic;->a(Ljava/util/List;)V

    .line 115
    sget-object v1, Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;->SCENIC:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    iput-object v1, p1, Lcom/amap/api/services/poisearch/PoiItemDetail;->e:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    .line 116
    iput-object v0, p1, Lcom/amap/api/services/poisearch/PoiItemDetail;->d:Lcom/amap/api/services/poisearch/Scenic;

    .line 117
    return-void
.end method

.method private c(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    .line 284
    const-string v0, "discounts"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 306
    :cond_0
    return-void

    .line 287
    :cond_1
    const-string v0, "discounts"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 288
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 289
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 290
    new-instance v3, Lcom/amap/api/services/poisearch/Discount;

    invoke-direct {v3}, Lcom/amap/api/services/poisearch/Discount;-><init>()V

    .line 291
    const-string v4, "title"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setTitle(Ljava/lang/String;)V

    .line 292
    const-string v4, "detail"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setDetail(Ljava/lang/String;)V

    .line 293
    const-string v4, "start_time"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/services/core/c;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 295
    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setStartTime(Ljava/util/Date;)V

    .line 296
    const-string v4, "end_time"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/services/core/c;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 298
    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setEndTime(Ljava/util/Date;)V

    .line 299
    const-string v4, "sold_num"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/j;->f(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setSoldCount(I)V

    .line 300
    const-string v4, "url"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setUrl(Ljava/lang/String;)V

    .line 301
    const-string v4, "provider"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setProvider(Ljava/lang/String;)V

    .line 302
    invoke-direct {p0, v3, v2}, Lcom/amap/api/services/poisearch/j;->a(Lcom/amap/api/services/poisearch/Discount;Lorg/json/JSONObject;)V

    .line 303
    invoke-virtual {p1, v3}, Lcom/amap/api/services/poisearch/PoiItemDetail;->addDiscount(Lcom/amap/api/services/poisearch/Discount;)V

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private c(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lcom/amap/api/services/poisearch/Cinema;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/Cinema;-><init>()V

    .line 122
    const-string v1, "intro"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Cinema;->a(Ljava/lang/String;)V

    .line 123
    const-string v1, "rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Cinema;->b(Ljava/lang/String;)V

    .line 124
    const-string v1, "deepsrc"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Cinema;->c(Ljava/lang/String;)V

    .line 125
    const-string v1, "parking"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Cinema;->d(Ljava/lang/String;)V

    .line 126
    const-string v1, "opentime_GDF"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Cinema;->e(Ljava/lang/String;)V

    .line 127
    const-string v1, "opentime"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Cinema;->f(Ljava/lang/String;)V

    .line 128
    invoke-direct {p0, p2}, Lcom/amap/api/services/poisearch/j;->d(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Cinema;->a(Ljava/util/List;)V

    .line 129
    invoke-direct {p0, p3}, Lcom/amap/api/services/poisearch/j;->c(Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    const-string v1, "seat_ordering"

    invoke-direct {p0, p3, v1}, Lcom/amap/api/services/poisearch/j;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    .line 131
    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Cinema;->a(Z)V

    .line 133
    :cond_0
    sget-object v1, Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;->CINEMA:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    iput-object v1, p1, Lcom/amap/api/services/poisearch/PoiItemDetail;->e:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    .line 134
    iput-object v0, p1, Lcom/amap/api/services/poisearch/PoiItemDetail;->c:Lcom/amap/api/services/poisearch/Cinema;

    .line 135
    return-void
.end method

.method private c(Lorg/json/JSONObject;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 214
    if-nez p1, :cond_1

    .line 220
    :cond_0
    :goto_0
    return v0

    .line 217
    :cond_1
    const-string v1, "biz_ext"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 6

    .prologue
    .line 314
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 315
    if-nez p1, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-object v0

    .line 318
    :cond_1
    const-string v1, "photos"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    :try_start_0
    const-string v1, "photos"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 323
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 324
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 325
    new-instance v4, Lcom/amap/api/services/poisearch/Photo;

    invoke-direct {v4}, Lcom/amap/api/services/poisearch/Photo;-><init>()V

    .line 326
    const-string v5, "title"

    invoke-virtual {p0, v3, v5}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/amap/api/services/poisearch/Photo;->setTitle(Ljava/lang/String;)V

    .line 327
    const-string v5, "url"

    invoke-virtual {p0, v3, v5}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/amap/api/services/poisearch/Photo;->setUrl(Ljava/lang/String;)V

    .line 328
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 330
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private d(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lcom/amap/api/services/poisearch/Hotel;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/Hotel;-><init>()V

    .line 140
    const-string v1, "star"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->b(Ljava/lang/String;)V

    .line 141
    const-string v1, "intro"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->c(Ljava/lang/String;)V

    .line 142
    const-string v1, "rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->a(Ljava/lang/String;)V

    .line 143
    const-string v1, "lowest_price"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->d(Ljava/lang/String;)V

    .line 144
    const-string v1, "deepsrc"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->k(Ljava/lang/String;)V

    .line 145
    const-string v1, "faci_rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->e(Ljava/lang/String;)V

    .line 146
    const-string v1, "health_rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->f(Ljava/lang/String;)V

    .line 147
    const-string v1, "environment_rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->g(Ljava/lang/String;)V

    .line 148
    const-string v1, "service_rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->h(Ljava/lang/String;)V

    .line 149
    const-string v1, "traffic"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->i(Ljava/lang/String;)V

    .line 150
    const-string v1, "addition"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->j(Ljava/lang/String;)V

    .line 151
    invoke-direct {p0, p2}, Lcom/amap/api/services/poisearch/j;->d(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Hotel;->a(Ljava/util/List;)V

    .line 152
    sget-object v1, Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;->HOTEL:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    iput-object v1, p1, Lcom/amap/api/services/poisearch/PoiItemDetail;->e:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    .line 153
    iput-object v0, p1, Lcom/amap/api/services/poisearch/PoiItemDetail;->b:Lcom/amap/api/services/poisearch/Hotel;

    .line 154
    return-void
.end method

.method private d(Lorg/json/JSONObject;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 189
    const-string v0, "biz_ext"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 190
    invoke-virtual {p0, v0, p2}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-direct {p0, v0}, Lcom/amap/api/services/poisearch/j;->j(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private e(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 158
    new-instance v0, Lcom/amap/api/services/poisearch/Dining;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/Dining;-><init>()V

    .line 159
    const-string v1, "cuisines"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->a(Ljava/lang/String;)V

    .line 160
    const-string v1, "tag"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->b(Ljava/lang/String;)V

    .line 161
    const-string v1, "intro"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->c(Ljava/lang/String;)V

    .line 162
    const-string v1, "rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->d(Ljava/lang/String;)V

    .line 163
    const-string v1, "cp_rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->e(Ljava/lang/String;)V

    .line 164
    const-string v1, "deepsrc"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->f(Ljava/lang/String;)V

    .line 165
    const-string v1, "taste_rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->g(Ljava/lang/String;)V

    .line 166
    const-string v1, "environment_rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->h(Ljava/lang/String;)V

    .line 167
    const-string v1, "service_rating"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->i(Ljava/lang/String;)V

    .line 168
    const-string v1, "cost"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->j(Ljava/lang/String;)V

    .line 169
    const-string v1, "recommend"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->k(Ljava/lang/String;)V

    .line 170
    const-string v1, "atmosphere"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->l(Ljava/lang/String;)V

    .line 171
    const-string v1, "ordering_wap_url"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->m(Ljava/lang/String;)V

    .line 172
    const-string v1, "ordering_web_url"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->n(Ljava/lang/String;)V

    .line 173
    const-string v1, "ordering_app_url"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->o(Ljava/lang/String;)V

    .line 174
    const-string v1, "opentime_GDF"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->p(Ljava/lang/String;)V

    .line 175
    const-string v1, "opentime"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->q(Ljava/lang/String;)V

    .line 176
    const-string v1, "addition"

    invoke-virtual {p0, p2, v1}, Lcom/amap/api/services/poisearch/j;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->r(Ljava/lang/String;)V

    .line 177
    invoke-direct {p0, p2}, Lcom/amap/api/services/poisearch/j;->d(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->a(Ljava/util/List;)V

    .line 178
    invoke-direct {p0, p3}, Lcom/amap/api/services/poisearch/j;->c(Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    const-string v1, "meal_ordering"

    invoke-direct {p0, p3, v1}, Lcom/amap/api/services/poisearch/j;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    .line 180
    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/Dining;->a(Z)V

    .line 182
    :cond_0
    sget-object v1, Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;->DINING:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    iput-object v1, p1, Lcom/amap/api/services/poisearch/PoiItemDetail;->e:Lcom/amap/api/services/poisearch/PoiItemDetail$DeepType;

    .line 183
    iput-object v0, p1, Lcom/amap/api/services/poisearch/PoiItemDetail;->a:Lcom/amap/api/services/poisearch/Dining;

    .line 185
    return-void
.end method

.method private j(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 195
    .line 197
    :try_start_0
    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 198
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 199
    if-nez v2, :cond_1

    .line 210
    :cond_0
    :goto_0
    return v0

    .line 201
    :cond_1
    if-ne v2, v1, :cond_0

    move v0, v1

    .line 202
    goto :goto_0

    .line 205
    :catch_0
    move-exception v1

    .line 206
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0

    .line 207
    :catch_1
    move-exception v1

    .line 208
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public synthetic b(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/amap/api/services/poisearch/j;->c(Ljava/io/InputStream;)Lcom/amap/api/services/poisearch/PoiItemDetail;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/io/InputStream;)Lcom/amap/api/services/poisearch/PoiItemDetail;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 31
    .line 32
    invoke-virtual {p0, p1}, Lcom/amap/api/services/poisearch/j;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    .line 33
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 45
    :cond_0
    :goto_0
    return-object v0

    .line 36
    :cond_1
    invoke-static {v1}, Lcom/amap/api/services/core/c;->b(Ljava/lang/String;)V

    .line 38
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 39
    invoke-direct {p0, v2}, Lcom/amap/api/services/poisearch/j;->b(Lorg/json/JSONObject;)Lcom/amap/api/services/poisearch/PoiItemDetail;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 40
    :catch_0
    move-exception v1

    .line 41
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 42
    :catch_1
    move-exception v1

    .line 43
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected c()[B
    .locals 1

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/amap/api/services/poisearch/j;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "http://restapi.amap.com/v3/place/detail?"

    return-object v0
.end method
