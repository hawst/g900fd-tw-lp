.class public Lcom/amap/api/services/poisearch/PoiSearch;
.super Ljava/lang/Object;
.source "PoiSearch.java"


# static fields
.field private static i:Ljava/util/ArrayList;


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

.field private c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

.field private d:Landroid/content/Context;

.field private e:Lcom/amap/api/services/poisearch/PoiSearch$OnPoiSearchListener;

.field private f:Lcom/amap/api/services/poisearch/PoiSearch$Query;

.field private g:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/services/poisearch/PoiSearch$Query;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 460
    new-instance v0, Lcom/amap/api/services/poisearch/i;

    invoke-direct {v0, p0}, Lcom/amap/api/services/poisearch/i;-><init>(Lcom/amap/api/services/poisearch/PoiSearch;)V

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->a:Landroid/os/Handler;

    .line 28
    invoke-static {p1}, Lcom/amap/api/services/core/b;->a(Landroid/content/Context;)Lcom/amap/api/services/core/b;

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->d:Landroid/content/Context;

    .line 30
    invoke-virtual {p0, p2}, Lcom/amap/api/services/poisearch/PoiSearch;->setQuery(Lcom/amap/api/services/poisearch/PoiSearch$Query;)V

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/amap/api/services/poisearch/PoiSearch;)Lcom/amap/api/services/poisearch/PoiSearch$OnPoiSearchListener;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->e:Lcom/amap/api/services/poisearch/PoiSearch$OnPoiSearchListener;

    return-object v0
.end method

.method private a(Lcom/amap/api/services/poisearch/PoiResult;)V
    .locals 3

    .prologue
    .line 441
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/amap/api/services/poisearch/PoiSearch;->i:Ljava/util/ArrayList;

    .line 442
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->h:I

    if-ge v0, v1, :cond_0

    .line 443
    sget-object v1, Lcom/amap/api/services/poisearch/PoiSearch;->i:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 445
    :cond_0
    iget v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->h:I

    if-lez v0, :cond_1

    .line 446
    sget-object v0, Lcom/amap/api/services/poisearch/PoiSearch;->i:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-virtual {v1}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->getPageNum()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 447
    :cond_1
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-static {v0}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->a(Lcom/amap/api/services/poisearch/PoiSearch$Query;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/services/core/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-static {v0}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->b(Lcom/amap/api/services/poisearch/PoiSearch$Query;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/services/core/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 457
    iget v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->h:I

    if-gt p1, v0, :cond_0

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 14
    invoke-static {p0, p1}, Lcom/amap/api/services/poisearch/PoiSearch;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 431
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 432
    const/4 v0, 0x1

    .line 437
    :goto_0
    return v0

    .line 434
    :cond_0
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 435
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 437
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getBound()Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    return-object v0
.end method

.method protected getPageLocal(I)Lcom/amap/api/services/poisearch/PoiResult;
    .locals 2

    .prologue
    .line 450
    invoke-direct {p0, p1}, Lcom/amap/api/services/poisearch/PoiSearch;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 451
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "page out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 453
    :cond_0
    sget-object v0, Lcom/amap/api/services/poisearch/PoiSearch;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/poisearch/PoiResult;

    return-object v0
.end method

.method public getQuery()Lcom/amap/api/services/poisearch/PoiSearch$Query;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    return-object v0
.end method

.method public searchPOI()Lcom/amap/api/services/poisearch/PoiResult;
    .locals 4

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/amap/api/services/poisearch/PoiSearch;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/amap/api/services/core/AMapException;

    const-string v1, "\u65e0\u6548\u7684\u53c2\u6570 - IllegalArgumentException"

    invoke-direct {v0, v1}, Lcom/amap/api/services/core/AMapException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    iget-object v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->f:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->queryEquals(Lcom/amap/api/services/poisearch/PoiSearch$Query;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    iget-object v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->f:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->queryEquals(Lcom/amap/api/services/poisearch/PoiSearch$Query;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    iget-object v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->g:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 50
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->h:I

    .line 51
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-virtual {v0}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->clone()Lcom/amap/api/services/poisearch/PoiSearch$Query;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->f:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    .line 52
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    if-eqz v0, :cond_3

    .line 53
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    invoke-virtual {v0}, Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;->clone()Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->g:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    .line 55
    :cond_3
    sget-object v0, Lcom/amap/api/services/poisearch/PoiSearch;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 56
    sget-object v0, Lcom/amap/api/services/poisearch/PoiSearch;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 59
    :cond_4
    const/4 v0, 0x0

    .line 60
    iget-object v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    if-eqz v1, :cond_5

    .line 61
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    invoke-virtual {v0}, Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;->clone()Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    move-result-object v0

    .line 64
    :cond_5
    iget v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->h:I

    if-nez v1, :cond_6

    .line 65
    new-instance v1, Lcom/amap/api/services/poisearch/k;

    new-instance v2, Lcom/amap/api/services/poisearch/l;

    iget-object v3, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-virtual {v3}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->clone()Lcom/amap/api/services/poisearch/PoiSearch$Query;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/amap/api/services/poisearch/l;-><init>(Lcom/amap/api/services/poisearch/PoiSearch$Query;Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;)V

    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/services/core/c;->a(Landroid/content/Context;)Ljava/net/Proxy;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/amap/api/services/poisearch/k;-><init>(Lcom/amap/api/services/poisearch/l;Ljava/net/Proxy;)V

    .line 67
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-static {v0}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->c(Lcom/amap/api/services/poisearch/PoiSearch$Query;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/amap/api/services/poisearch/k;->a(I)V

    .line 68
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-static {v0}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->d(Lcom/amap/api/services/poisearch/PoiSearch$Query;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/amap/api/services/poisearch/k;->b(I)V

    .line 69
    invoke-virtual {v1}, Lcom/amap/api/services/poisearch/k;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/amap/api/services/poisearch/PoiResult;->a(Lcom/amap/api/services/poisearch/k;Ljava/util/ArrayList;)Lcom/amap/api/services/poisearch/PoiResult;

    move-result-object v0

    .line 70
    invoke-direct {p0, v0}, Lcom/amap/api/services/poisearch/PoiSearch;->a(Lcom/amap/api/services/poisearch/PoiResult;)V

    .line 83
    :goto_0
    return-object v0

    .line 72
    :cond_6
    iget-object v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-virtual {v1}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->getPageNum()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/amap/api/services/poisearch/PoiSearch;->getPageLocal(I)Lcom/amap/api/services/poisearch/PoiResult;

    move-result-object v1

    .line 73
    if-nez v1, :cond_7

    .line 74
    new-instance v1, Lcom/amap/api/services/poisearch/k;

    new-instance v2, Lcom/amap/api/services/poisearch/l;

    iget-object v3, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-virtual {v3}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->clone()Lcom/amap/api/services/poisearch/PoiSearch$Query;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/amap/api/services/poisearch/l;-><init>(Lcom/amap/api/services/poisearch/PoiSearch$Query;Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;)V

    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/services/core/c;->a(Landroid/content/Context;)Ljava/net/Proxy;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/amap/api/services/poisearch/k;-><init>(Lcom/amap/api/services/poisearch/l;Ljava/net/Proxy;)V

    .line 76
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-static {v0}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->c(Lcom/amap/api/services/poisearch/PoiSearch$Query;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/amap/api/services/poisearch/k;->a(I)V

    .line 77
    iget-object v0, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-static {v0}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->d(Lcom/amap/api/services/poisearch/PoiSearch$Query;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/amap/api/services/poisearch/k;->b(I)V

    .line 78
    invoke-virtual {v1}, Lcom/amap/api/services/poisearch/k;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/amap/api/services/poisearch/PoiResult;->a(Lcom/amap/api/services/poisearch/k;Ljava/util/ArrayList;)Lcom/amap/api/services/poisearch/PoiResult;

    move-result-object v0

    .line 80
    sget-object v1, Lcom/amap/api/services/poisearch/PoiSearch;->i:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-static {v2}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->c(Lcom/amap/api/services/poisearch/PoiSearch$Query;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_0
.end method

.method public searchPOIAsyn()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/amap/api/services/poisearch/PoiSearch$1;

    invoke-direct {v0, p0}, Lcom/amap/api/services/poisearch/PoiSearch$1;-><init>(Lcom/amap/api/services/poisearch/PoiSearch;)V

    invoke-virtual {v0}, Lcom/amap/api/services/poisearch/PoiSearch$1;->start()V

    .line 106
    return-void
.end method

.method public searchPOIDetail(Ljava/lang/String;)Lcom/amap/api/services/poisearch/PoiItemDetail;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lcom/amap/api/services/poisearch/j;

    iget-object v1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/amap/api/services/core/c;->a(Landroid/content/Context;)Ljava/net/Proxy;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/amap/api/services/poisearch/j;-><init>(Ljava/lang/String;Ljava/net/Proxy;)V

    .line 141
    invoke-virtual {v0}, Lcom/amap/api/services/poisearch/j;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/poisearch/PoiItemDetail;

    return-object v0
.end method

.method public searchPOIDetailAsyn(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lcom/amap/api/services/poisearch/PoiSearch$2;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/services/poisearch/PoiSearch$2;-><init>(Lcom/amap/api/services/poisearch/PoiSearch;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/amap/api/services/poisearch/PoiSearch$2;->start()V

    .line 165
    return-void
.end method

.method public setBound(Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->b:Lcom/amap/api/services/poisearch/PoiSearch$SearchBound;

    .line 173
    return-void
.end method

.method public setOnPoiSearchListener(Lcom/amap/api/services/poisearch/PoiSearch$OnPoiSearchListener;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->e:Lcom/amap/api/services/poisearch/PoiSearch$OnPoiSearchListener;

    .line 35
    return-void
.end method

.method public setQuery(Lcom/amap/api/services/poisearch/PoiSearch$Query;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/amap/api/services/poisearch/PoiSearch;->c:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    .line 169
    return-void
.end method
