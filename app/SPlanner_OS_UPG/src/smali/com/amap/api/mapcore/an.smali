.class Lcom/amap/api/mapcore/an;
.super Ljava/lang/Object;
.source "PolylineDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/y;


# instance fields
.field a:F

.field b:F

.field c:F

.field d:F

.field e:[F

.field private f:Lcom/amap/api/mapcore/r;

.field private g:F

.field private h:I

.field private i:F

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/util/List;

.field private m:Ljava/nio/FloatBuffer;

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Ljava/util/List;

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Lcom/amap/api/maps/model/LatLngBounds;

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/amap/api/mapcore/an;->g:F

    .line 36
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/amap/api/mapcore/an;->h:I

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/an;->i:F

    .line 38
    iput-boolean v2, p0, Lcom/amap/api/mapcore/an;->j:Z

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    .line 42
    iput v1, p0, Lcom/amap/api/mapcore/an;->n:I

    .line 43
    iput-boolean v1, p0, Lcom/amap/api/mapcore/an;->o:Z

    .line 44
    iput-boolean v1, p0, Lcom/amap/api/mapcore/an;->p:Z

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->q:Ljava/util/List;

    .line 46
    iput-boolean v1, p0, Lcom/amap/api/mapcore/an;->r:Z

    .line 56
    iput-boolean v2, p0, Lcom/amap/api/mapcore/an;->t:Z

    .line 57
    iput-object v3, p0, Lcom/amap/api/mapcore/an;->u:Lcom/amap/api/maps/model/LatLngBounds;

    .line 435
    iput v1, p0, Lcom/amap/api/mapcore/an;->v:I

    .line 437
    iput-boolean v2, p0, Lcom/amap/api/mapcore/an;->w:Z

    .line 438
    iput-boolean v1, p0, Lcom/amap/api/mapcore/an;->x:Z

    .line 439
    iput-object v3, p0, Lcom/amap/api/mapcore/an;->y:Landroid/graphics/Bitmap;

    .line 93
    iput-object p1, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    .line 95
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/an;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->k:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private m()Ljava/util/List;
    .locals 8

    .prologue
    .line 277
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 278
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 279
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 280
    if-eqz v0, :cond_0

    .line 281
    new-instance v3, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 282
    iget-object v4, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-interface {v4, v5, v0, v3}, Lcom/amap/api/mapcore/r;->b(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 283
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v3, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v3, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 288
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private p()Z
    .locals 27

    .prologue
    .line 664
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 666
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v3}, Lcom/amap/api/mapcore/r;->n()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v3

    iget v3, v3, Lcom/amap/api/maps/model/CameraPosition;->zoom:F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/high16 v4, 0x41200000    # 10.0f

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 667
    const/4 v2, 0x0

    .line 698
    :goto_0
    return v2

    .line 668
    :catch_0
    move-exception v3

    .line 669
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    .line 672
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    if-eqz v3, :cond_1

    .line 673
    new-instance v2, Landroid/graphics/Rect;

    const/16 v3, -0x64

    const/16 v4, -0x64

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v5}, Lcom/amap/api/mapcore/r;->j()I

    move-result v5

    add-int/lit8 v5, v5, 0x64

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v6}, Lcom/amap/api/mapcore/r;->k()I

    move-result v6

    add-int/lit8 v6, v6, 0x64

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 675
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/an;->u:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v0, v3, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    move-object/from16 v18, v0

    .line 676
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/an;->u:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v0, v3, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    move-object/from16 v24, v0

    .line 677
    new-instance v8, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 678
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, v24

    iget-wide v6, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v3 .. v8}, Lcom/amap/api/mapcore/r;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 680
    new-instance v14, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v14}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 681
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    move-object/from16 v0, v18

    iget-wide v10, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, v18

    iget-wide v12, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v9 .. v14}, Lcom/amap/api/mapcore/r;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 683
    new-instance v20, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v20 .. v20}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 684
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-wide/from16 v16, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-wide/from16 v18, v0

    invoke-interface/range {v15 .. v20}, Lcom/amap/api/mapcore/r;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 686
    new-instance v26, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v26 .. v26}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 687
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    move-object/from16 v21, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-wide/from16 v24, v0

    invoke-interface/range {v21 .. v26}, Lcom/amap/api/mapcore/r;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 690
    iget v3, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, v14, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v14, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, v20

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v20

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, v26

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v26

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 692
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 698
    :cond_1
    :goto_1
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/16 :goto_0

    .line 694
    :cond_2
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method a(Lcom/autonavi/amap/mapcore/IPoint;Lcom/autonavi/amap/mapcore/IPoint;Lcom/autonavi/amap/mapcore/IPoint;DI)Lcom/autonavi/amap/mapcore/IPoint;
    .locals 12

    .prologue
    .line 148
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 149
    iget v3, p2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, p1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v3, v4

    int-to-double v4, v3

    .line 150
    iget v3, p2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v6, p1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v3, v6

    int-to-double v6, v3

    .line 151
    mul-double v8, v6, v6

    mul-double v10, v4, v4

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    add-double/2addr v8, v10

    .line 152
    move/from16 v0, p6

    int-to-double v10, v0

    mul-double v10, v10, p4

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    div-double v8, v10, v8

    iget v3, p3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-double v10, v3

    add-double/2addr v8, v10

    double-to-int v3, v8

    iput v3, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 153
    iget v3, p3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v8, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v3, v8

    int-to-double v8, v3

    mul-double/2addr v6, v8

    div-double v4, v6, v4

    iget v3, p3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-double v6, v3

    add-double/2addr v4, v6

    double-to-int v3, v4

    iput v3, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 155
    return-object v2
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 319
    iput p1, p0, Lcom/amap/api/mapcore/an;->i:F

    .line 320
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->J()V

    .line 321
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 322
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 304
    iput p1, p0, Lcom/amap/api/mapcore/an;->h:I

    .line 305
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/an;->a:F

    .line 306
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/an;->b:F

    .line 307
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/an;->c:F

    .line 308
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/an;->d:F

    .line 309
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 310
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 444
    iput-boolean v1, p0, Lcom/amap/api/mapcore/an;->w:Z

    .line 445
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/an;->x:Z

    .line 446
    iput-object p1, p0, Lcom/amap/api/mapcore/an;->y:Landroid/graphics/Bitmap;

    .line 447
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 448
    return-void
.end method

.method a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;Ljava/util/List;Lcom/amap/api/maps/model/LatLngBounds$Builder;)V
    .locals 26

    .prologue
    .line 206
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L    # 180.0

    div-double v24, v2, v4

    .line 208
    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    add-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 212
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    .line 215
    iget-wide v4, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide/16 v6, 0x0

    cmpl-double v3, v4, v6

    if-lez v3, :cond_0

    const/16 v23, -0x1

    .line 217
    :goto_0
    new-instance v8, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 218
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v3 .. v8}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 219
    new-instance v14, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v14}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 220
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v9 .. v14}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 221
    new-instance v20, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v20 .. v20}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 222
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    iget-wide v0, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-wide/from16 v16, v0

    iget-wide v0, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-wide/from16 v18, v0

    invoke-interface/range {v15 .. v20}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 226
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double v2, v2, v24

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 228
    iget v4, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v14, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v4, v5

    int-to-double v4, v4

    iget v6, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v7, v14, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v6, v7

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double v6, v6, v24

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    mul-double v21, v4, v6

    move-object/from16 v17, p0

    move-object/from16 v18, v8

    move-object/from16 v19, v14

    .line 233
    invoke-virtual/range {v17 .. v23}, Lcom/amap/api/mapcore/an;->a(Lcom/autonavi/amap/mapcore/IPoint;Lcom/autonavi/amap/mapcore/IPoint;Lcom/autonavi/amap/mapcore/IPoint;DI)Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v4

    .line 236
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 237
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    invoke-interface {v5, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/amap/api/mapcore/an;->a(Ljava/util/List;Ljava/util/List;D)V

    .line 243
    return-void

    .line 215
    :cond_0
    const/16 v23, 0x1

    goto/16 :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 261
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->p:Z

    if-eqz v0, :cond_1

    .line 262
    :cond_0
    iput-object p1, p0, Lcom/amap/api/mapcore/an;->q:Ljava/util/List;

    .line 264
    :cond_1
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/an;->b(Ljava/util/List;)V

    .line 265
    return-void
.end method

.method a(Ljava/util/List;Ljava/util/List;D)V
    .locals 23

    .prologue
    .line 172
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 198
    :cond_0
    return-void

    .line 177
    :cond_1
    const/high16 v4, 0x3f800000    # 1.0f

    .line 179
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/16 v2, 0xa

    if-gt v3, v2, :cond_0

    .line 180
    int-to-float v2, v3

    const/high16 v5, 0x41200000    # 10.0f

    div-float v5, v2, v5

    .line 181
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 182
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    float-to-double v10, v5

    sub-double/2addr v8, v10

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    float-to-double v12, v5

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-double v10, v2

    mul-double/2addr v8, v10

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v10, v2

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    float-to-double v14, v5

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-double v12, v2

    mul-double/2addr v10, v12

    mul-double v10, v10, p3

    add-double/2addr v8, v10

    mul-float v7, v5, v5

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v7

    float-to-double v10, v2

    add-double/2addr v8, v10

    .line 185
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    float-to-double v12, v5

    sub-double/2addr v10, v12

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    float-to-double v14, v5

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-double v12, v2

    mul-double/2addr v10, v12

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v12, v2

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    mul-double/2addr v12, v14

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-double v14, v2

    mul-double/2addr v12, v14

    mul-double v12, v12, p3

    add-double/2addr v10, v12

    mul-float v7, v5, v5

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-float v2, v2

    mul-float/2addr v2, v7

    float-to-double v12, v2

    add-double/2addr v10, v12

    .line 189
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    float-to-double v14, v5

    sub-double/2addr v12, v14

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    mul-double/2addr v12, v14

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v14, v2

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    mul-double v14, v14, v16

    mul-double v14, v14, p3

    add-double/2addr v12, v14

    mul-float v2, v5, v5

    float-to-double v14, v2

    add-double/2addr v12, v14

    .line 190
    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    mul-double v14, v14, v16

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    mul-double v16, v16, v18

    mul-double v16, v16, p3

    add-double v14, v14, v16

    mul-float v2, v5, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    .line 192
    div-double/2addr v8, v12

    double-to-int v2, v8

    iput v2, v6, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 193
    div-double v8, v10, v14

    double-to-int v2, v8

    iput v2, v6, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 195
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    int-to-float v2, v3

    add-float/2addr v2, v4

    float-to-int v2, v2

    move v3, v2

    goto/16 :goto_0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 456
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/an;->g:F

    const/4 v2, 0x0

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->p:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->w:Z

    if-nez v0, :cond_b

    .line 467
    :cond_2
    invoke-direct {p0}, Lcom/amap/api/mapcore/an;->p()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 468
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    iget-object v2, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-static {v0, v2}, Lcom/amap/api/mapcore/util/v;->a(Lcom/amap/api/mapcore/r;Ljava/util/List;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    .line 469
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    :goto_1
    iput v0, p0, Lcom/amap/api/mapcore/an;->n:I

    .line 470
    iput-boolean v11, p0, Lcom/amap/api/mapcore/an;->s:Z

    .line 483
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/amap/api/mapcore/an;->n:I

    if-lez v0, :cond_8

    .line 484
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->t:Z

    if-eqz v0, :cond_e

    .line 521
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/an;->h()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapLenWithWin(I)F

    move-result v2

    .line 523
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapLenWithWin(I)F

    move-result v8

    .line 527
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->w:Z

    if-nez v0, :cond_c

    .line 528
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->x:Z

    if-eqz v0, :cond_7

    .line 529
    iget v0, p0, Lcom/amap/api/mapcore/an;->v:I

    if-eqz v0, :cond_4

    .line 530
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    iget v3, p0, Lcom/amap/api/mapcore/an;->v:I

    invoke-interface {v0, v3}, Lcom/amap/api/mapcore/r;->d(I)V

    .line 531
    new-array v0, v11, [I

    iget v3, p0, Lcom/amap/api/mapcore/an;->v:I

    aput v3, v0, v1

    invoke-interface {p1, v11, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    .line 533
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->H()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/an;->v:I

    .line 534
    iget v0, p0, Lcom/amap/api/mapcore/an;->v:I

    if-nez v0, :cond_5

    .line 535
    new-array v0, v11, [I

    aput v1, v0, v1

    .line 536
    invoke-interface {p1, v11, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 537
    aget v0, v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/an;->v:I

    .line 539
    :cond_5
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->y:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/amap/api/mapcore/an;->y:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_6

    .line 540
    iget v0, p0, Lcom/amap/api/mapcore/an;->v:I

    iget-object v3, p0, Lcom/amap/api/mapcore/an;->y:Landroid/graphics/Bitmap;

    invoke-static {p1, v0, v3, v11}, Lcom/amap/api/mapcore/util/v;->a(Ljavax/microedition/khronos/opengles/GL10;ILandroid/graphics/Bitmap;Z)I

    .line 542
    :cond_6
    iput-boolean v1, p0, Lcom/amap/api/mapcore/an;->x:Z

    .line 543
    const-string v0, "ss"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ssssssssssssssssss"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/amap/api/mapcore/an;->v:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    iget-object v1, p0, Lcom/amap/api/mapcore/an;->e:[F

    array-length v1, v1

    iget v3, p0, Lcom/amap/api/mapcore/an;->v:I

    iget v4, p0, Lcom/amap/api/mapcore/an;->b:F

    iget v5, p0, Lcom/amap/api/mapcore/an;->c:F

    iget v6, p0, Lcom/amap/api/mapcore/an;->d:F

    iget v7, p0, Lcom/amap/api/mapcore/an;->a:F

    iget-boolean v9, p0, Lcom/amap/api/mapcore/an;->p:Z

    iget-boolean v10, p0, Lcom/amap/api/mapcore/an;->w:Z

    invoke-static/range {v0 .. v10}, Lcom/amap/api/maps/AMapNativeRenderer;->nativeDrawLineByTextureID([FIFIFFFFFZZ)V

    .line 660
    :cond_8
    :goto_4
    iput-boolean v11, p0, Lcom/amap/api/mapcore/an;->r:Z

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 469
    goto/16 :goto_1

    .line 472
    :cond_a
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->s:Z

    if-eqz v0, :cond_3

    .line 473
    invoke-virtual {p0}, Lcom/amap/api/mapcore/an;->g()V

    .line 474
    iput-boolean v1, p0, Lcom/amap/api/mapcore/an;->s:Z

    goto/16 :goto_2

    .line 478
    :cond_b
    iget v0, p0, Lcom/amap/api/mapcore/an;->n:I

    if-nez v0, :cond_3

    .line 479
    invoke-virtual {p0}, Lcom/amap/api/mapcore/an;->g()V

    goto/16 :goto_2

    .line 546
    :cond_c
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->p:Z

    if-eqz v0, :cond_d

    .line 547
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->l()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/an;->v:I

    goto :goto_3

    .line 549
    :cond_d
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->c()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/an;->v:I

    goto :goto_3

    .line 651
    :cond_e
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->m:Ljava/nio/FloatBuffer;

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    if-eqz v0, :cond_f

    .line 652
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->m:Ljava/nio/FloatBuffer;

    .line 654
    :cond_f
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/amap/api/mapcore/an;->i()I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/an;->m:Ljava/nio/FloatBuffer;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/an;->h()F

    move-result v4

    iget v5, p0, Lcom/amap/api/mapcore/an;->n:I

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/amap/api/mapcore/n;->a(Ljavax/microedition/khronos/opengles/GL10;IILjava/nio/FloatBuffer;FI)V

    goto :goto_4
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 331
    iput-boolean p1, p0, Lcom/amap/api/mapcore/an;->j:Z

    .line 332
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 333
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 387
    iget-object v2, p0, Lcom/amap/api/mapcore/an;->u:Lcom/amap/api/maps/model/LatLngBounds;

    if-nez v2, :cond_1

    .line 394
    :cond_0
    :goto_0
    return v0

    .line 390
    :cond_1
    iget-object v2, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->D()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v2

    .line 391
    if-nez v2, :cond_2

    move v0, v1

    .line 392
    goto :goto_0

    .line 394
    :cond_2
    iget-object v3, p0, Lcom/amap/api/mapcore/an;->u:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps/model/LatLngBounds;->contains(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/mapcore/an;->u:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps/model/LatLngBounds;->intersects(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/w;)Z
    .locals 2

    .prologue
    .line 343
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/w;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/an;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    :cond_0
    const/4 v0, 0x1

    .line 347
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/an;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Ljava/lang/String;)Z

    .line 248
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 249
    return-void
.end method

.method public b(F)V
    .locals 2

    .prologue
    .line 293
    iput p1, p0, Lcom/amap/api/mapcore/an;->g:F

    .line 294
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 295
    return-void
.end method

.method b(Ljava/util/List;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 102
    .line 103
    invoke-static {}, Lcom/amap/api/maps/model/LatLngBounds;->builder()Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v8

    .line 104
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 105
    if-eqz p1, :cond_4

    .line 106
    const/4 v0, 0x0

    .line 107
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v7, v0

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/model/LatLng;

    .line 108
    if-eqz v0, :cond_0

    invoke-virtual {v0, v7}, Lcom/amap/api/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    iget-boolean v1, p0, Lcom/amap/api/mapcore/an;->o:Z

    if-nez v1, :cond_2

    .line 112
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 113
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v1 .. v6}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 114
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    invoke-virtual {v8, v0}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    :cond_1
    :goto_1
    move-object v7, v0

    .line 135
    goto :goto_0

    .line 117
    :cond_2
    if-eqz v7, :cond_1

    .line 118
    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-wide v4, v7, Lcom/amap/api/maps/model/LatLng;->longitude:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v1, v2, v4

    if-gez v1, :cond_3

    .line 119
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    iget-wide v2, v7, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v7, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v1 .. v6}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 122
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    invoke-virtual {v8, v7}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    .line 124
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 125
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v1 .. v6}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 126
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    invoke-virtual {v8, v0}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    goto :goto_1

    .line 129
    :cond_3
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-virtual {p0, v7, v0, v1, v8}, Lcom/amap/api/mapcore/an;->a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;Ljava/util/List;Lcom/amap/api/maps/model/LatLngBounds$Builder;)V

    goto :goto_1

    .line 137
    :cond_4
    invoke-virtual {v8}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->u:Lcom/amap/api/maps/model/LatLngBounds;

    .line 138
    iput v10, p0, Lcom/amap/api/mapcore/an;->n:I

    .line 140
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, v10}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 141
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/amap/api/mapcore/an;->o:Z

    .line 66
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 67
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 254
    const-string v0, "Polyline"

    invoke-static {v0}, Lcom/amap/api/mapcore/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->k:Ljava/lang/String;

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->k:Ljava/lang/String;

    return-object v0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/amap/api/mapcore/an;->p:Z

    .line 80
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 81
    return-void
.end method

.method public d()F
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Lcom/amap/api/mapcore/an;->i:F

    return v0
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/amap/api/mapcore/an;->t:Z

    .line 61
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 62
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->j:Z

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 352
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 411
    iput-boolean v0, p0, Lcom/amap/api/mapcore/an;->r:Z

    .line 413
    new-instance v2, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 415
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/amap/api/mapcore/an;->e:[F

    .line 416
    iget-object v1, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 418
    iget-object v4, p0, Lcom/amap/api/mapcore/an;->f:Lcom/amap/api/mapcore/r;

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-interface {v4, v5, v0, v2}, Lcom/amap/api/mapcore/r;->b(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 419
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    mul-int/lit8 v4, v1, 0x3

    iget v5, v2, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v5, v0, v4

    .line 420
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    mul-int/lit8 v4, v1, 0x3

    add-int/lit8 v4, v4, 0x1

    iget v5, v2, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v5, v0, v4

    .line 421
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    mul-int/lit8 v4, v1, 0x3

    add-int/lit8 v4, v4, 0x2

    const/4 v5, 0x0

    aput v5, v0, v4

    .line 422
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 423
    goto :goto_0

    .line 424
    :cond_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->t:Z

    if-nez v0, :cond_1

    .line 425
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->m:Ljava/nio/FloatBuffer;

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/an;->n:I

    .line 428
    return-void
.end method

.method public h()F
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lcom/amap/api/mapcore/an;->g:F

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/amap/api/mapcore/an;->h:I

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->p:Z

    if-eqz v0, :cond_1

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->q:Ljava/util/List;

    .line 272
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/amap/api/mapcore/an;->m()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->o:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->p:Z

    return v0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 738
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    if-eqz v0, :cond_0

    .line 739
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->e:[F

    .line 741
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->m:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 742
    iget-object v0, p0, Lcom/amap/api/mapcore/an;->m:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 743
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/an;->m:Ljava/nio/FloatBuffer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 749
    :cond_1
    :goto_0
    return-void

    .line 745
    :catch_0
    move-exception v0

    .line 746
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 747
    const-string v0, "destroy erro"

    const-string v1, "PolylineDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 753
    iget-boolean v0, p0, Lcom/amap/api/mapcore/an;->r:Z

    return v0
.end method
