.class Lcom/amap/api/mapcore/util/j$a$a;
.super Ljava/io/FilterOutputStream;
.source "DiskLruCache.java"


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/util/j$a;


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/util/j$a;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 877
    iput-object p1, p0, Lcom/amap/api/mapcore/util/j$a$a;->a:Lcom/amap/api/mapcore/util/j$a;

    .line 878
    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 879
    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/util/j$a;Ljava/io/OutputStream;Lcom/amap/api/mapcore/util/k;)V
    .locals 0

    .prologue
    .line 876
    invoke-direct {p0, p1, p2}, Lcom/amap/api/mapcore/util/j$a$a;-><init>(Lcom/amap/api/mapcore/util/j$a;Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 902
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/util/j$a$a;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 906
    :goto_0
    return-void

    .line 903
    :catch_0
    move-exception v0

    .line 904
    iget-object v0, p0, Lcom/amap/api/mapcore/util/j$a$a;->a:Lcom/amap/api/mapcore/util/j$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/j$a;->a(Lcom/amap/api/mapcore/util/j$a;Z)Z

    goto :goto_0
.end method

.method public flush()V
    .locals 2

    .prologue
    .line 911
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/util/j$a$a;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 915
    :goto_0
    return-void

    .line 912
    :catch_0
    move-exception v0

    .line 913
    iget-object v0, p0, Lcom/amap/api/mapcore/util/j$a$a;->a:Lcom/amap/api/mapcore/util/j$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/j$a;->a(Lcom/amap/api/mapcore/util/j$a;Z)Z

    goto :goto_0
.end method

.method public write(I)V
    .locals 2

    .prologue
    .line 884
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/util/j$a$a;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 888
    :goto_0
    return-void

    .line 885
    :catch_0
    move-exception v0

    .line 886
    iget-object v0, p0, Lcom/amap/api/mapcore/util/j$a$a;->a:Lcom/amap/api/mapcore/util/j$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/j$a;->a(Lcom/amap/api/mapcore/util/j$a;Z)Z

    goto :goto_0
.end method

.method public write([BII)V
    .locals 2

    .prologue
    .line 893
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/util/j$a$a;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 897
    :goto_0
    return-void

    .line 894
    :catch_0
    move-exception v0

    .line 895
    iget-object v0, p0, Lcom/amap/api/mapcore/util/j$a$a;->a:Lcom/amap/api/mapcore/util/j$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/j$a;->a(Lcom/amap/api/mapcore/util/j$a;Z)Z

    goto :goto_0
.end method
