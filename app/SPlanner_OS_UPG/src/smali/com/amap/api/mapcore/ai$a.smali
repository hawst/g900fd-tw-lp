.class public Lcom/amap/api/mapcore/ai$a;
.super Landroid/view/ViewGroup$LayoutParams;
.source "MapOverlayViewGroup.java"


# instance fields
.field public a:I

.field public b:Lcom/amap/api/maps/model/LatLng;

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(IILcom/amap/api/maps/model/LatLng;III)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/ai$a;->a:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 42
    iput v1, p0, Lcom/amap/api/mapcore/ai$a;->c:I

    .line 43
    iput v1, p0, Lcom/amap/api/mapcore/ai$a;->d:I

    .line 44
    const/16 v0, 0x33

    iput v0, p0, Lcom/amap/api/mapcore/ai$a;->e:I

    .line 53
    iput v1, p0, Lcom/amap/api/mapcore/ai$a;->a:I

    .line 54
    iput-object p3, p0, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 55
    iput p4, p0, Lcom/amap/api/mapcore/ai$a;->c:I

    .line 56
    iput p5, p0, Lcom/amap/api/mapcore/ai$a;->d:I

    .line 57
    iput p6, p0, Lcom/amap/api/mapcore/ai$a;->e:I

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/ai$a;->a:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 42
    iput v1, p0, Lcom/amap/api/mapcore/ai$a;->c:I

    .line 43
    iput v1, p0, Lcom/amap/api/mapcore/ai$a;->d:I

    .line 44
    const/16 v0, 0x33

    iput v0, p0, Lcom/amap/api/mapcore/ai$a;->e:I

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/ai$a;->a:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 42
    iput v1, p0, Lcom/amap/api/mapcore/ai$a;->c:I

    .line 43
    iput v1, p0, Lcom/amap/api/mapcore/ai$a;->d:I

    .line 44
    const/16 v0, 0x33

    iput v0, p0, Lcom/amap/api/mapcore/ai$a;->e:I

    .line 73
    return-void
.end method
