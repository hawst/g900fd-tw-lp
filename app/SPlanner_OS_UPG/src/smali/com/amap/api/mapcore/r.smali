.class public interface abstract Lcom/amap/api/mapcore/r;
.super Ljava/lang/Object;
.source "IAMapDelegate.java"


# virtual methods
.method public abstract A()V
.end method

.method public abstract B()F
.end method

.method public abstract D()Lcom/amap/api/maps/model/LatLngBounds;
.end method

.method public abstract G()F
.end method

.method public abstract H()I
.end method

.method public abstract I()Ljava/util/List;
.end method

.method public abstract J()V
.end method

.method public abstract K()V
.end method

.method public abstract N()I
.end method

.method public abstract O()Z
.end method

.method public abstract a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/s;
.end method

.method public abstract a(Lcom/amap/api/maps/model/GroundOverlayOptions;)Lcom/amap/api/mapcore/t;
.end method

.method public abstract a(Lcom/amap/api/maps/model/PolygonOptions;)Lcom/amap/api/mapcore/x;
.end method

.method public abstract a(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/mapcore/y;
.end method

.method public abstract a(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;
.end method

.method public abstract a(Lcom/amap/api/maps/model/TileOverlayOptions;)Lcom/amap/api/maps/model/TileOverlay;
.end method

.method public abstract a(DDLcom/autonavi/amap/mapcore/FPoint;)V
.end method

.method public abstract a(DDLcom/autonavi/amap/mapcore/IPoint;)V
.end method

.method public abstract a(F)V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public abstract a(IIII)V
.end method

.method public abstract a(IILcom/autonavi/amap/mapcore/DPoint;)V
.end method

.method public abstract a(IILcom/autonavi/amap/mapcore/FPoint;)V
.end method

.method public abstract a(Landroid/location/Location;)V
.end method

.method public abstract a(Lcom/amap/api/mapcore/i;)V
.end method

.method public abstract a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V
.end method

.method public abstract a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V
.end method

.method public abstract a(Lcom/amap/api/mapcore/o;)V
.end method

.method public abstract a(Lcom/amap/api/mapcore/v;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$InfoWindowAdapter;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnCameraChangeListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnMapClickListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnMapLoadedListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)V
.end method

.method public abstract a(Lcom/amap/api/maps/CustomRenderer;)V
.end method

.method public abstract a(Lcom/amap/api/maps/LocationSource;)V
.end method

.method public abstract a(Lcom/amap/api/maps/model/MyLocationStyle;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a(Ljava/lang/String;)Z
.end method

.method public abstract b(I)F
.end method

.method public abstract b(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/mapcore/aj;
.end method

.method public abstract b(DDLcom/autonavi/amap/mapcore/IPoint;)V
.end method

.method public abstract b(IILcom/autonavi/amap/mapcore/DPoint;)V
.end method

.method public abstract b(IILcom/autonavi/amap/mapcore/FPoint;)V
.end method

.method public abstract b(Lcom/amap/api/mapcore/i;)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract b(Lcom/amap/api/mapcore/v;)Z
.end method

.method public abstract b(Ljava/lang/String;)Z
.end method

.method public abstract c()I
.end method

.method public abstract c(I)V
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()Lcom/autonavi/amap/mapcore/MapProjection;
.end method

.method public abstract d(I)V
.end method

.method public abstract d(Z)V
.end method

.method public abstract e(I)V
.end method

.method public abstract e(Z)V
.end method

.method public abstract f()V
.end method

.method public abstract f(Z)V
.end method

.method public abstract g(Z)V
.end method

.method public abstract h(Z)Lcom/amap/api/maps/model/CameraPosition;
.end method

.method public abstract j()I
.end method

.method public abstract k()I
.end method

.method public abstract l()I
.end method

.method public abstract m()V
.end method

.method public abstract n()Lcom/amap/api/maps/model/CameraPosition;
.end method

.method public abstract o()F
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract p()F
.end method

.method public abstract q()V
.end method

.method public abstract r()V
.end method

.method public abstract s()I
.end method

.method public abstract setZOrderOnTop(Z)V
.end method

.method public abstract t()Z
.end method

.method public abstract u()Z
.end method

.method public abstract v()Landroid/location/Location;
.end method

.method public abstract w()Lcom/amap/api/mapcore/ac;
.end method

.method public abstract x()Lcom/amap/api/mapcore/z;
.end method

.method public abstract z()Landroid/view/View;
.end method
