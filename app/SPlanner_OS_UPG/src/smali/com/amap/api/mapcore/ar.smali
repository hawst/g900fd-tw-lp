.class public Lcom/amap/api/mapcore/ar;
.super Landroid/view/View;
.source "TileOverlayView.java"


# instance fields
.field a:Ljava/util/concurrent/CopyOnWriteArrayList;

.field b:Lcom/amap/api/mapcore/ar$a;

.field c:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private d:Lcom/amap/api/mapcore/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 16
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 17
    new-instance v0, Lcom/amap/api/mapcore/ar$a;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ar$a;-><init>(Lcom/amap/api/mapcore/ar;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/ar;->b:Lcom/amap/api/mapcore/ar$a;

    .line 18
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ar;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 16
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 17
    new-instance v0, Lcom/amap/api/mapcore/ar$a;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ar$a;-><init>(Lcom/amap/api/mapcore/ar;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/ar;->b:Lcom/amap/api/mapcore/ar$a;

    .line 18
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ar;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 46
    iput-object p2, p0, Lcom/amap/api/mapcore/ar;->d:Lcom/amap/api/mapcore/r;

    .line 47
    return-void
.end method


# virtual methods
.method a()Lcom/amap/api/mapcore/r;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->d:Lcom/amap/api/mapcore/r;

    return-object v0
.end method

.method public a(Lcom/amap/api/mapcore/ab;)V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/ar;->b(Lcom/amap/api/mapcore/ab;)Z

    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ar;->c()V

    .line 96
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 55
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lcom/amap/api/mapcore/util/v;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    goto :goto_0

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 58
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ab;

    .line 59
    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/ab;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_1

    .line 63
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ab;

    .line 104
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/ab;->b(Z)V

    goto :goto_0

    .line 108
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ab;

    .line 76
    if-eqz v0, :cond_0

    .line 77
    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->a()V

    goto :goto_0

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 81
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ab;

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/ab;->c(Z)V

    goto :goto_0

    .line 132
    :cond_1
    return-void
.end method

.method public b(Lcom/amap/api/mapcore/ab;)Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method c()V
    .locals 5

    .prologue
    .line 84
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 85
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->b:Lcom/amap/api/mapcore/ar$a;

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 86
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 87
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 88
    iget-object v4, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    check-cast v0, Lcom/amap/api/mapcore/ab;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 90
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ab;

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->g()V

    goto :goto_0

    .line 116
    :cond_1
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/amap/api/mapcore/ar;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ab;

    .line 120
    if-eqz v0, :cond_0

    .line 121
    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->h()V

    goto :goto_0

    .line 124
    :cond_1
    return-void
.end method
