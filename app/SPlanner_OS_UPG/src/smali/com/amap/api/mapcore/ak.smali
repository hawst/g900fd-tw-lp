.class Lcom/amap/api/mapcore/ak;
.super Ljava/lang/Object;
.source "MyLocationOverlay.java"


# instance fields
.field private a:Lcom/amap/api/mapcore/r;

.field private b:Lcom/amap/api/mapcore/v;

.field private c:Lcom/amap/api/mapcore/s;

.field private d:Lcom/amap/api/maps/model/MyLocationStyle;

.field private e:Lcom/amap/api/maps/model/LatLng;

.field private f:D

.field private g:F


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/r;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/ak;->g:F

    .line 29
    iput-object p1, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/r;

    .line 30
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    if-nez v0, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/amap/api/mapcore/ak;->c()V

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/ak;->d()V

    goto :goto_0
.end method

.method private c()V
    .locals 8

    .prologue
    .line 105
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/r;

    new-instance v1, Lcom/amap/api/maps/model/CircleOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/CircleOptions;-><init>()V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->strokeWidth(F)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    const/16 v2, 0x64

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xb4

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->fillColor(I)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    const/16 v2, 0xff

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xdc

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->strokeColor(I)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->center(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/s;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    .line 109
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    const-wide/high16 v2, 0x4069000000000000L    # 200.0

    invoke-interface {v0, v2, v3}, Lcom/amap/api/mapcore/s;->a(D)V

    .line 110
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/r;

    new-instance v1, Lcom/amap/api/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/MarkerOptions;-><init>()V

    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/maps/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/amap/api/mapcore/aa$a;->c:Lcom/amap/api/mapcore/aa$a;

    invoke-virtual {v3}, Lcom/amap/api/mapcore/aa$a;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->fromAsset(Ljava/lang/String;)Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->position(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->b(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/mapcore/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private d()V
    .locals 8

    .prologue
    .line 123
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    if-nez v0, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/r;

    new-instance v1, Lcom/amap/api/maps/model/CircleOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/CircleOptions;-><init>()V

    iget-object v2, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getStrokeWidth()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->strokeWidth(F)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getRadiusFillColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->fillColor(I)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getStrokeColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->strokeColor(I)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->center(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/s;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    .line 132
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->e:Lcom/amap/api/maps/model/LatLng;

    if-eqz v0, :cond_2

    .line 133
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    iget-object v1, p0, Lcom/amap/api/mapcore/ak;->e:Lcom/amap/api/maps/model/LatLng;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/s;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    iget-wide v2, p0, Lcom/amap/api/mapcore/ak;->f:D

    invoke-interface {v0, v2, v3}, Lcom/amap/api/mapcore/s;->a(D)V

    .line 136
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/r;

    new-instance v1, Lcom/amap/api/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/MarkerOptions;-><init>()V

    iget-object v2, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getAnchorU()F

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v3}, Lcom/amap/api/maps/model/MyLocationStyle;->getAnchorV()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/maps/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getMyLocationIcon()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->position(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->b(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/mapcore/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    .line 140
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->e:Lcom/amap/api/maps/model/LatLng;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    iget-object v1, p0, Lcom/amap/api/mapcore/ak;->e:Lcom/amap/api/maps/model/LatLng;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/v;->a(Lcom/amap/api/maps/model/LatLng;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 143
    :catch_0
    move-exception v0

    .line 144
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/r;

    iget-object v1, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    invoke-interface {v1}, Lcom/amap/api/mapcore/s;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Ljava/lang/String;)Z

    .line 75
    iput-object v2, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/r;

    iget-object v1, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    invoke-interface {v1}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->b(Ljava/lang/String;)Z

    .line 79
    iput-object v2, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    .line 81
    :cond_1
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 149
    iput p1, p0, Lcom/amap/api/mapcore/ak;->g:F

    .line 150
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(F)V

    .line 153
    :cond_0
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/LatLng;D)V
    .locals 2

    .prologue
    .line 47
    iput-object p1, p0, Lcom/amap/api/mapcore/ak;->e:Lcom/amap/api/maps/model/LatLng;

    .line 48
    iput-wide p2, p0, Lcom/amap/api/mapcore/ak;->f:D

    .line 49
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    if-nez v0, :cond_0

    .line 50
    invoke-direct {p0}, Lcom/amap/api/mapcore/ak;->b()V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 53
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    iget v1, p0, Lcom/amap/api/mapcore/ak;->g:F

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/v;->a(F)V

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/s;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 56
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    cmpl-double v0, p2, v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    invoke-interface {v0, p2, p3}, Lcom/amap/api/mapcore/s;->a(D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_1
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/MyLocationStyle;)V
    .locals 1

    .prologue
    .line 33
    iput-object p1, p0, Lcom/amap/api/mapcore/ak;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    .line 34
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->b:Lcom/amap/api/mapcore/v;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->c:Lcom/amap/api/mapcore/s;

    if-nez v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 38
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_1
    invoke-direct {p0}, Lcom/amap/api/mapcore/ak;->d()V

    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
