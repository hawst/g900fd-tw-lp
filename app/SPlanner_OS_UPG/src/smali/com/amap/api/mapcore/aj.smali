.class Lcom/amap/api/mapcore/aj;
.super Ljava/lang/Object;
.source "MarkerDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/v;


# static fields
.field private static c:I


# instance fields
.field a:Z

.field b:I

.field private d:F

.field private e:Z

.field private f:Ljava/nio/FloatBuffer;

.field private g:Ljava/lang/String;

.field private h:Lcom/amap/api/maps/model/LatLng;

.field private i:Lcom/amap/api/maps/model/LatLng;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:F

.field private m:F

.field private n:Z

.field private o:Z

.field private p:Lcom/amap/api/mapcore/ah;

.field private q:Ljava/nio/FloatBuffer;

.field private r:Ljava/lang/Object;

.field private s:Z

.field private t:Ljava/util/ArrayList;

.field private u:[I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput v0, Lcom/amap/api/mapcore/aj;->c:I

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/maps/model/MarkerOptions;Lcom/amap/api/mapcore/ah;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/aj;->d:F

    .line 53
    iput-boolean v1, p0, Lcom/amap/api/mapcore/aj;->e:Z

    .line 145
    iput-object v3, p0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    .line 155
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/amap/api/mapcore/aj;->l:F

    .line 156
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/amap/api/mapcore/aj;->m:F

    .line 157
    iput-boolean v1, p0, Lcom/amap/api/mapcore/aj;->n:Z

    .line 158
    iput-boolean v2, p0, Lcom/amap/api/mapcore/aj;->o:Z

    .line 162
    iput-boolean v1, p0, Lcom/amap/api/mapcore/aj;->s:Z

    .line 164
    iput-object v3, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    .line 167
    iput-boolean v1, p0, Lcom/amap/api/mapcore/aj;->v:Z

    .line 421
    iput-boolean v1, p0, Lcom/amap/api/mapcore/aj;->a:Z

    .line 588
    iput-boolean v1, p0, Lcom/amap/api/mapcore/aj;->w:Z

    .line 832
    iput v1, p0, Lcom/amap/api/mapcore/aj;->b:I

    .line 945
    iput-boolean v2, p0, Lcom/amap/api/mapcore/aj;->x:Z

    .line 949
    iput v1, p0, Lcom/amap/api/mapcore/aj;->y:I

    .line 953
    const/16 v0, 0x14

    iput v0, p0, Lcom/amap/api/mapcore/aj;->z:I

    .line 199
    iput-object p2, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    .line 200
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->isGps()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->v:Z

    .line 201
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 202
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->v:Z

    if-eqz v0, :cond_0

    .line 204
    :try_start_0
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    iget-wide v0, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/amap/api/mapcore/util/w;->a(DD)[D

    move-result-object v0

    .line 205
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    const/4 v2, 0x1

    aget-wide v2, v0, v2

    const/4 v4, 0x0

    aget-wide v4, v0, v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    iput-object v1, p0, Lcom/amap/api/mapcore/aj;->i:Lcom/amap/api/maps/model/LatLng;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->h:Lcom/amap/api/maps/model/LatLng;

    .line 212
    :cond_1
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getAnchorU()F

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/aj;->l:F

    .line 213
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getAnchorV()F

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/aj;->m:F

    .line 214
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getPeriod()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/aj;->z:I

    .line 216
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getIcons()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/aj;->b(Ljava/util/ArrayList;)V

    .line 218
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 220
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getIcon()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/amap/api/mapcore/aj;->b(Lcom/amap/api/maps/model/BitmapDescriptor;)V

    .line 223
    :cond_2
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->o:Z

    .line 224
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getSnippet()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->k:Ljava/lang/String;

    .line 225
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->j:Ljava/lang/String;

    .line 226
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->isDraggable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->n:Z

    .line 227
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->g:Ljava/lang/String;

    .line 228
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->isPerspective()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->s:Z

    .line 229
    return-void

    .line 206
    :catch_0
    move-exception v0

    .line 207
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->i:Lcom/amap/api/maps/model/LatLng;

    goto :goto_0
.end method

.method private F()V
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    iget-object v0, v0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    iget-object v0, v0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 364
    :cond_0
    return-void
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 933
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    iget-object v0, v0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->H()I

    move-result v0

    .line 934
    if-nez v0, :cond_0

    .line 935
    new-array v0, v2, [I

    aput v1, v0, v1

    .line 936
    invoke-interface {p1, v2, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 937
    aget v0, v0, v1

    .line 939
    :cond_0
    return v0
.end method

.method private a(Lcom/autonavi/amap/mapcore/MapProjection;IIIILcom/autonavi/amap/mapcore/FPoint;)V
    .locals 5

    .prologue
    .line 772
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->s()I

    move-result v0

    .line 773
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->A()I

    move-result v1

    .line 775
    int-to-float v2, p4

    int-to-float v0, v0

    iget v3, p0, Lcom/amap/api/mapcore/aj;->l:F

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    .line 776
    int-to-float v2, p5

    int-to-float v1, v1

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/amap/api/mapcore/aj;->m:F

    sub-float/2addr v3, v4

    mul-float/2addr v1, v3

    sub-float v1, v2, v1

    .line 778
    invoke-direct {p0, v0, v1}, Lcom/amap/api/mapcore/aj;->b(FF)Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v0

    .line 779
    iget v1, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    add-int/2addr v1, p2

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v0, p3, v0

    invoke-virtual {p1, v1, v0, p6}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 780
    return-void
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;ILjava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V
    .locals 7

    .prologue
    const/16 v6, 0x1406

    const/16 v5, 0xbe2

    const/16 v4, 0xde1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 799
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 826
    :cond_0
    :goto_0
    return-void

    .line 802
    :cond_1
    invoke-interface {p1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 803
    const/4 v0, 0x1

    const/16 v1, 0x303

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 807
    invoke-interface {p1, v2, v2, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 810
    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 811
    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 812
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 813
    invoke-interface {p1, v4, p2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 818
    const/4 v0, 0x3

    invoke-interface {p1, v0, v6, v3, p3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 819
    const/4 v0, 0x2

    invoke-interface {p1, v0, v6, v3, p4}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 820
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-interface {p1, v0, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 822
    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 823
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 824
    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 825
    invoke-interface {p1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_0
.end method

.method private b(FF)Lcom/autonavi/amap/mapcore/IPoint;
    .locals 8

    .prologue
    .line 783
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    iget v2, p0, Lcom/amap/api/mapcore/aj;->d:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 784
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 785
    float-to-double v2, p1

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    float-to-double v4, p2

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 786
    float-to-double v2, p2

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    float-to-double v4, p1

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 787
    return-object v1
.end method

.method private b(Lcom/amap/api/maps/model/BitmapDescriptor;)V
    .locals 2

    .prologue
    .line 192
    if-eqz p1, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->y()V

    .line 194
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/amap/api/maps/model/BitmapDescriptor;->clone()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_0
    return-void
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 28
    sget v0, Lcom/amap/api/mapcore/aj;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/amap/api/mapcore/aj;->c:I

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/amap/api/mapcore/aj;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()I
    .locals 12

    .prologue
    const/4 v1, 0x0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 266
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->B()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getHeight()I

    move-result v0

    .line 267
    iget-object v2, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/ah;->a()Lcom/amap/api/mapcore/r;

    move-result-object v2

    .line 268
    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v3

    .line 269
    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v3

    .line 270
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->z()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v4

    .line 271
    iget-boolean v5, p0, Lcom/amap/api/mapcore/aj;->s:Z

    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    const v5, 0x358637bd    # 1.0E-6f

    cmpg-float v5, v3, v5

    if-gez v5, :cond_1

    .line 282
    :cond_0
    :goto_0
    return v0

    .line 275
    :cond_1
    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->k()I

    move-result v2

    .line 276
    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    .line 277
    int-to-double v8, v0

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    div-int/lit8 v4, v2, 0x2

    sub-int/2addr v3, v4

    int-to-double v4, v3

    mul-double/2addr v4, v8

    div-int/lit8 v2, v2, 0x2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    int-to-double v2, v2

    div-double v2, v4, v2

    div-double v4, v10, v6

    sub-double/2addr v4, v10

    mul-double/2addr v2, v4

    const-wide v4, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    add-int/2addr v0, v2

    .line 279
    if-gtz v0, :cond_0

    move v0, v1

    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    move v0, v1

    .line 282
    goto :goto_0
.end method

.method public B()Lcom/amap/api/maps/model/BitmapDescriptor;
    .locals 2

    .prologue
    .line 474
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 475
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->y()V

    .line 476
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-static {}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->defaultMarker()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 484
    :goto_0
    return-object v0

    .line 477
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 478
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 479
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->B()Lcom/amap/api/maps/model/BitmapDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 482
    :catch_0
    move-exception v0

    .line 483
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 484
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public C()F
    .locals 1

    .prologue
    .line 544
    iget v0, p0, Lcom/amap/api/mapcore/aj;->l:F

    return v0
.end method

.method public D()F
    .locals 1

    .prologue
    .line 549
    iget v0, p0, Lcom/amap/api/mapcore/aj;->m:F

    return v0
.end method

.method public E()Z
    .locals 32

    .prologue
    .line 633
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    if-nez v2, :cond_0

    .line 634
    const/4 v2, 0x0

    .line 738
    :goto_0
    return v2

    .line 636
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/ah;->a()Lcom/amap/api/mapcore/r;

    move-result-object v3

    .line 637
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/amap/api/mapcore/aj;->v:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/aj;->i:Lcom/amap/api/maps/model/LatLng;

    .line 638
    :goto_1
    if-nez v2, :cond_2

    .line 639
    const/4 v2, 0x0

    goto :goto_0

    .line 637
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/aj;->d()Lcom/amap/api/maps/model/LatLng;

    move-result-object v2

    goto :goto_1

    .line 641
    :cond_2
    invoke-interface {v3}, Lcom/amap/api/mapcore/r;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v10

    .line 642
    new-instance v8, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 643
    iget-wide v4, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v6, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v3 .. v8}, Lcom/amap/api/mapcore/r;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 644
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/aj;->s()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/aj;->A()I

    move-result v28

    .line 646
    iget v2, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-float v2, v2

    move/from16 v0, v20

    int-to-float v4, v0

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/aj;->C()F

    move-result v5

    mul-float/2addr v4, v5

    sub-float/2addr v2, v4

    float-to-int v2, v2

    iget v4, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-float v4, v4

    move/from16 v0, v28

    int-to-float v5, v0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/aj;->D()F

    move-result v7

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 653
    sub-int v5, v2, v20

    invoke-interface {v3}, Lcom/amap/api/mapcore/r;->j()I

    move-result v6

    if-gt v5, v6, :cond_3

    move/from16 v0, v20

    neg-int v5, v0

    mul-int/lit8 v5, v5, 0x2

    if-lt v2, v5, :cond_3

    move/from16 v0, v28

    neg-int v5, v0

    mul-int/lit8 v5, v5, 0x2

    if-lt v4, v5, :cond_3

    sub-int v5, v4, v28

    invoke-interface {v3}, Lcom/amap/api/mapcore/r;->k()I

    move-result v3

    if-le v5, v3, :cond_4

    .line 656
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 672
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/aj;->B()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v3

    .line 675
    if-nez v3, :cond_5

    .line 676
    const/4 v2, 0x0

    goto :goto_0

    .line 678
    :cond_5
    invoke-virtual {v3}, Lcom/amap/api/maps/model/BitmapDescriptor;->getWidth()I

    move-result v5

    .line 679
    invoke-virtual {v3}, Lcom/amap/api/maps/model/BitmapDescriptor;->getHeight()I

    move-result v6

    .line 683
    invoke-virtual {v3}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 684
    invoke-virtual {v3}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 685
    int-to-float v5, v5

    int-to-float v3, v3

    div-float v3, v5, v3

    .line 686
    int-to-float v5, v6

    int-to-float v6, v7

    div-float/2addr v5, v6

    .line 687
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    if-nez v6, :cond_6

    .line 688
    const/16 v6, 0x8

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/4 v9, 0x0

    aput v9, v6, v7

    const/4 v7, 0x1

    aput v5, v6, v7

    const/4 v7, 0x2

    aput v3, v6, v7

    const/4 v7, 0x3

    aput v5, v6, v7

    const/4 v5, 0x4

    aput v3, v6, v5

    const/4 v3, 0x5

    const/4 v5, 0x0

    aput v5, v6, v3

    const/4 v3, 0x6

    const/4 v5, 0x0

    aput v5, v6, v3

    const/4 v3, 0x7

    const/4 v5, 0x0

    aput v5, v6, v3

    invoke-static {v6}, Lcom/amap/api/mapcore/util/v;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    .line 692
    :cond_6
    const/16 v3, 0xc

    new-array v0, v3, [F

    move-object/from16 v30, v0

    .line 694
    new-instance v15, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v15}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 696
    new-instance v22, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct/range {v22 .. v22}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 698
    new-instance v29, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct/range {v29 .. v29}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 700
    new-instance v31, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct/range {v31 .. v31}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 702
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/aj;->d:F

    const/4 v5, 0x0

    cmpl-float v3, v3, v5

    if-nez v3, :cond_7

    .line 703
    invoke-virtual {v10, v2, v4, v15}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 704
    add-int v3, v2, v20

    move-object/from16 v0, v22

    invoke-virtual {v10, v3, v4, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 705
    add-int v3, v2, v20

    sub-int v5, v4, v28

    move-object/from16 v0, v29

    invoke-virtual {v10, v3, v5, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 706
    sub-int v3, v4, v28

    move-object/from16 v0, v31

    invoke-virtual {v10, v2, v3, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 714
    :goto_2
    const/4 v2, 0x0

    iget v3, v15, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v3, v30, v2

    .line 715
    const/4 v2, 0x1

    iget v3, v15, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v3, v30, v2

    .line 716
    const/4 v2, 0x2

    const/4 v3, 0x0

    aput v3, v30, v2

    .line 718
    const/4 v2, 0x3

    move-object/from16 v0, v22

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v3, v30, v2

    .line 719
    const/4 v2, 0x4

    move-object/from16 v0, v22

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v3, v30, v2

    .line 720
    const/4 v2, 0x5

    const/4 v3, 0x0

    aput v3, v30, v2

    .line 722
    const/4 v2, 0x6

    move-object/from16 v0, v29

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v3, v30, v2

    .line 723
    const/4 v2, 0x7

    move-object/from16 v0, v29

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v3, v30, v2

    .line 724
    const/16 v2, 0x8

    const/4 v3, 0x0

    aput v3, v30, v2

    .line 726
    const/16 v2, 0x9

    move-object/from16 v0, v31

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v3, v30, v2

    .line 727
    const/16 v2, 0xa

    move-object/from16 v0, v31

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v3, v30, v2

    .line 728
    const/16 v2, 0xb

    const/4 v3, 0x0

    aput v3, v30, v2

    .line 732
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    if-nez v2, :cond_8

    .line 733
    invoke-static/range {v30 .. v30}, Lcom/amap/api/mapcore/util/v;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    .line 738
    :goto_3
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 708
    :cond_7
    iget v11, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v12, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v15}, Lcom/amap/api/mapcore/aj;->a(Lcom/autonavi/amap/mapcore/MapProjection;IIIILcom/autonavi/amap/mapcore/FPoint;)V

    .line 709
    iget v0, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move/from16 v18, v0

    iget v0, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move/from16 v19, v0

    const/16 v21, 0x0

    move-object/from16 v16, p0

    move-object/from16 v17, v10

    invoke-direct/range {v16 .. v22}, Lcom/amap/api/mapcore/aj;->a(Lcom/autonavi/amap/mapcore/MapProjection;IIIILcom/autonavi/amap/mapcore/FPoint;)V

    .line 710
    iget v0, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move/from16 v25, v0

    iget v0, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move/from16 v26, v0

    move-object/from16 v23, p0

    move-object/from16 v24, v10

    move/from16 v27, v20

    invoke-direct/range {v23 .. v29}, Lcom/amap/api/mapcore/aj;->a(Lcom/autonavi/amap/mapcore/MapProjection;IIIILcom/autonavi/amap/mapcore/FPoint;)V

    .line 711
    iget v4, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object v3, v10

    move/from16 v7, v28

    move-object/from16 v8, v31

    invoke-direct/range {v2 .. v8}, Lcom/amap/api/mapcore/aj;->a(Lcom/autonavi/amap/mapcore/MapProjection;IIIILcom/autonavi/amap/mapcore/FPoint;)V

    goto/16 :goto_2

    .line 735
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    move-object/from16 v0, v30

    invoke-static {v0, v2}, Lcom/amap/api/mapcore/util/v;->a([FLjava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    goto :goto_3
.end method

.method public a(F)V
    .locals 2

    .prologue
    const/high16 v1, 0x43b40000    # 360.0f

    .line 41
    neg-float v0, p1

    rem-float/2addr v0, v1

    add-float/2addr v0, v1

    rem-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/aj;->d:F

    .line 42
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 44
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->d(Lcom/amap/api/mapcore/v;)V

    .line 47
    :cond_0
    return-void
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 531
    iget v0, p0, Lcom/amap/api/mapcore/aj;->l:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/aj;->m:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    .line 540
    :goto_0
    return-void

    .line 533
    :cond_0
    iput p1, p0, Lcom/amap/api/mapcore/aj;->l:F

    .line 534
    iput p2, p0, Lcom/amap/api/mapcore/aj;->m:F

    .line 535
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 536
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 537
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->d(Lcom/amap/api/mapcore/v;)V

    .line 539
    :cond_1
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 962
    if-gt p1, v0, :cond_0

    .line 963
    iput v0, p0, Lcom/amap/api/mapcore/aj;->z:I

    .line 967
    :goto_0
    return-void

    .line 965
    :cond_0
    iput p1, p0, Lcom/amap/api/mapcore/aj;->z:I

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/BitmapDescriptor;)V
    .locals 1

    .prologue
    .line 450
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 454
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->a:Z

    .line 458
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->w:Z

    .line 459
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    .line 460
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 461
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    .line 463
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 464
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 465
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->d(Lcom/amap/api/mapcore/v;)V

    .line 468
    :cond_3
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/LatLng;)V
    .locals 6

    .prologue
    .line 381
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->v:Z

    if-eqz v0, :cond_0

    .line 383
    :try_start_0
    iget-wide v0, p1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-wide v2, p1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/amap/api/mapcore/util/w;->a(DD)[D

    move-result-object v0

    .line 384
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    const/4 v2, 0x1

    aget-wide v2, v0, v2

    const/4 v4, 0x0

    aget-wide v4, v0, v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    iput-object v1, p0, Lcom/amap/api/mapcore/aj;->i:Lcom/amap/api/maps/model/LatLng;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/amap/api/mapcore/aj;->h:Lcom/amap/api/maps/model/LatLng;

    .line 390
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    .line 391
    return-void

    .line 385
    :catch_0
    move-exception v0

    .line 386
    iput-object p1, p0, Lcom/amap/api/mapcore/aj;->i:Lcom/amap/api/maps/model/LatLng;

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 971
    iput-object p1, p0, Lcom/amap/api/mapcore/aj;->r:Ljava/lang/Object;

    .line 972
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/amap/api/mapcore/aj;->j:Ljava/lang/String;

    .line 396
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    .line 397
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 425
    if-nez p1, :cond_0

    .line 441
    :goto_0
    return-void

    .line 428
    :cond_0
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/aj;->b(Ljava/util/ArrayList;)V

    .line 431
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->w:Z

    .line 432
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 434
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    .line 436
    :cond_1
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 438
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->d(Lcom/amap/api/mapcore/v;)V

    .line 440
    :cond_2
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    goto :goto_0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;Lcom/amap/api/mapcore/r;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 836
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->o:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->d()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->B()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 930
    :cond_0
    :goto_0
    return-void

    .line 849
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->w:Z

    if-nez v0, :cond_4

    .line 852
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I

    if-eqz v0, :cond_2

    .line 853
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I

    array-length v0, v0

    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->u:[I

    const/4 v3, 0x0

    invoke-interface {p1, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    move v0, v2

    .line 854
    :goto_1
    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->u:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 855
    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->u:[I

    aget v1, v1, v0

    invoke-interface {p2, v1}, Lcom/amap/api/mapcore/r;->d(I)V

    .line 854
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 858
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 859
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I

    .line 860
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 861
    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 862
    if-eqz v4, :cond_8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_8

    .line 863
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/aj;->a(Ljavax/microedition/khronos/opengles/GL10;)I

    move-result v5

    .line 864
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I

    aput v5, v0, v1

    .line 865
    add-int/lit8 v0, v1, 0x1

    .line 866
    invoke-static {p1, v5, v4}, Lcom/amap/api/mapcore/util/v;->a(Ljavax/microedition/khronos/opengles/GL10;ILandroid/graphics/Bitmap;)I

    :goto_3
    move v1, v0

    .line 868
    goto :goto_2

    .line 869
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_7

    .line 870
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->x:Z

    .line 874
    :goto_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->w:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 907
    :cond_4
    :goto_5
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I

    if-eqz v0, :cond_0

    .line 910
    iget v0, p0, Lcom/amap/api/mapcore/aj;->y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/aj;->y:I

    .line 911
    iget v0, p0, Lcom/amap/api/mapcore/aj;->z:I

    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->u:[I

    array-length v1, v1

    mul-int/2addr v0, v1

    .line 913
    iget v1, p0, Lcom/amap/api/mapcore/aj;->y:I

    if-lt v1, v0, :cond_5

    .line 914
    iput v2, p0, Lcom/amap/api/mapcore/aj;->y:I

    .line 916
    :cond_5
    iget v0, p0, Lcom/amap/api/mapcore/aj;->y:I

    iget v1, p0, Lcom/amap/api/mapcore/aj;->z:I

    div-int/2addr v0, v1

    .line 918
    iget-boolean v1, p0, Lcom/amap/api/mapcore/aj;->x:Z

    if-nez v1, :cond_6

    .line 919
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    .line 922
    :cond_6
    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->u:[I

    iget-object v2, p0, Lcom/amap/api/mapcore/aj;->u:[I

    array-length v2, v2

    rem-int/2addr v0, v2

    aget v0, v1, v0

    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/amap/api/mapcore/aj;->a(Ljavax/microedition/khronos/opengles/GL10;ILjava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V

    goto/16 :goto_0

    .line 872
    :cond_7
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->x:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 876
    :catch_0
    move-exception v0

    .line 877
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 417
    iput-boolean p1, p0, Lcom/amap/api/mapcore/aj;->n:Z

    .line 418
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    .line 419
    return-void
.end method

.method public a()Z
    .locals 5

    .prologue
    .line 351
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    .line 352
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I

    if-eqz v0, :cond_0

    .line 353
    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->u:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 354
    iget-object v4, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    iget-object v4, v4, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v4, v3}, Lcom/amap/api/mapcore/r;->d(I)V

    .line 353
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->b(Lcom/amap/api/mapcore/v;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/amap/api/mapcore/v;)Z
    .locals 2

    .prologue
    .line 554
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    :cond_0
    const/4 v0, 0x1

    .line 558
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/amap/api/mapcore/aj;->k:Ljava/lang/String;

    .line 407
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    .line 408
    return-void
.end method

.method public b(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I

    .line 180
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->y()V

    .line 181
    if-eqz p1, :cond_2

    .line 182
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 183
    if-eqz v0, :cond_0

    .line 184
    iget-object v2, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->clone()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I

    .line 189
    :cond_2
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 517
    iput-boolean p1, p0, Lcom/amap/api/mapcore/aj;->o:Z

    .line 518
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 521
    :cond_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    .line 522
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 957
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->x:Z

    return v0
.end method

.method public c()Landroid/graphics/Rect;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 316
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->f()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v1

    .line 317
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->s()I

    move-result v2

    .line 318
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->A()I

    move-result v3

    .line 319
    if-nez v1, :cond_0

    .line 320
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v4, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 346
    :goto_0
    return-object v0

    .line 322
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 323
    iget v4, p0, Lcom/amap/api/mapcore/aj;->d:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 324
    iget v4, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-float v4, v4

    int-to-float v5, v3

    iget v6, p0, Lcom/amap/api/mapcore/aj;->m:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v0, Landroid/graphics/Rect;->top:I

    .line 325
    iget v4, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-float v4, v4

    iget v5, p0, Lcom/amap/api/mapcore/aj;->l:F

    int-to-float v6, v2

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 326
    iget v4, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-float v4, v4

    int-to-float v3, v3

    iget v5, p0, Lcom/amap/api/mapcore/aj;->m:F

    sub-float v5, v9, v5

    mul-float/2addr v3, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 327
    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-float v1, v1

    iget v3, p0, Lcom/amap/api/mapcore/aj;->l:F

    sub-float v3, v9, v3

    int-to-float v2, v2

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 329
    :cond_1
    iget v4, p0, Lcom/amap/api/mapcore/aj;->l:F

    neg-float v4, v4

    int-to-float v5, v2

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/amap/api/mapcore/aj;->m:F

    sub-float/2addr v5, v9

    int-to-float v6, v3

    mul-float/2addr v5, v6

    invoke-direct {p0, v4, v5}, Lcom/amap/api/mapcore/aj;->b(FF)Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v4

    .line 331
    iget v5, p0, Lcom/amap/api/mapcore/aj;->l:F

    neg-float v5, v5

    int-to-float v6, v2

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/amap/api/mapcore/aj;->m:F

    int-to-float v7, v3

    mul-float/2addr v6, v7

    invoke-direct {p0, v5, v6}, Lcom/amap/api/mapcore/aj;->b(FF)Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v5

    .line 333
    iget v6, p0, Lcom/amap/api/mapcore/aj;->l:F

    sub-float v6, v9, v6

    int-to-float v7, v2

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/amap/api/mapcore/aj;->m:F

    int-to-float v8, v3

    mul-float/2addr v7, v8

    invoke-direct {p0, v6, v7}, Lcom/amap/api/mapcore/aj;->b(FF)Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v6

    .line 335
    iget v7, p0, Lcom/amap/api/mapcore/aj;->l:F

    sub-float v7, v9, v7

    int-to-float v2, v2

    mul-float/2addr v2, v7

    iget v7, p0, Lcom/amap/api/mapcore/aj;->m:F

    sub-float/2addr v7, v9

    int-to-float v3, v3

    mul-float/2addr v3, v7

    invoke-direct {p0, v2, v3}, Lcom/amap/api/mapcore/aj;->b(FF)Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v2

    .line 337
    iget v3, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v8, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v9, v6, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v10, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    sub-int/2addr v3, v7

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 339
    iget v3, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v8, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v9, v6, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v10, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int/2addr v3, v7

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 341
    iget v3, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v8, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v9, v6, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v10, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    sub-int/2addr v3, v7

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 343
    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v6, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 981
    iput-boolean p1, p0, Lcom/amap/api/mapcore/aj;->s:Z

    .line 982
    return-void
.end method

.method public d()Lcom/amap/api/maps/model/LatLng;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->h:Lcom/amap/api/maps/model/LatLng;

    return-object v0
.end method

.method public e()Lcom/amap/api/maps/model/LatLng;
    .locals 1

    .prologue
    .line 1003
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->i:Lcom/amap/api/maps/model/LatLng;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->h:Lcom/amap/api/maps/model/LatLng;

    goto :goto_0
.end method

.method public f()Lcom/autonavi/amap/mapcore/IPoint;
    .locals 1

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->z()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v0

    .line 306
    if-nez v0, :cond_0

    .line 307
    const/4 v0, 0x0

    .line 312
    :cond_0
    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 374
    const-string v0, "Marker"

    invoke-static {v0}, Lcom/amap/api/mapcore/aj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->g:Ljava/lang/String;

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 991
    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->u:[I

    if-nez v1, :cond_0

    .line 994
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->u:[I

    aget v0, v1, v0

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->j:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->k:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 490
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->n:Z

    return v0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    :goto_0
    return-void

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->d(Lcom/amap/api/mapcore/v;)V

    .line 499
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    goto :goto_0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 506
    invoke-direct {p0}, Lcom/amap/api/mapcore/aj;->F()V

    .line 508
    :cond_0
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->f(Lcom/amap/api/mapcore/v;)Z

    move-result v0

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 526
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->o:Z

    return v0
.end method

.method public p()V
    .locals 2

    .prologue
    .line 92
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/amap/api/mapcore/aj;->e:Z

    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    iget-object v0, v0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 96
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    iget-object v0, v0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->K()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    .line 136
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 137
    const-string v0, "destroy erro"

    const-string v1, "MarkerDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 563
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public r()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 976
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->r:Ljava/lang/Object;

    return-object v0
.end method

.method public s()I
    .locals 12

    .prologue
    const/4 v1, 0x0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 244
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->B()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getWidth()I

    move-result v0

    .line 245
    iget-object v2, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/ah;->a()Lcom/amap/api/mapcore/r;

    move-result-object v2

    .line 246
    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v3

    .line 247
    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v3

    .line 248
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->z()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v4

    .line 249
    iget-boolean v5, p0, Lcom/amap/api/mapcore/aj;->s:Z

    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    const v5, 0x358637bd    # 1.0E-6f

    cmpg-float v5, v3, v5

    if-gez v5, :cond_1

    .line 260
    :cond_0
    :goto_0
    return v0

    .line 253
    :cond_1
    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->k()I

    move-result v2

    .line 254
    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    .line 255
    int-to-double v8, v0

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    div-int/lit8 v4, v2, 0x2

    sub-int/2addr v3, v4

    int-to-double v4, v3

    mul-double/2addr v4, v8

    div-int/lit8 v2, v2, 0x2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    int-to-double v2, v2

    div-double v2, v4, v2

    div-double v4, v10, v6

    sub-double/2addr v4, v10

    mul-double/2addr v2, v4

    const-wide v4, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    add-int/2addr v0, v2

    .line 257
    if-gtz v0, :cond_0

    move v0, v1

    goto :goto_0

    .line 259
    :catch_0
    move-exception v0

    move v0, v1

    .line 260
    goto :goto_0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 986
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->s:Z

    return v0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 999
    iget v0, p0, Lcom/amap/api/mapcore/aj;->z:I

    return v0
.end method

.method public v()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    return-object v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->e:Z

    return v0
.end method

.method public x()V
    .locals 2

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->e:Z

    if-eqz v0, :cond_1

    .line 60
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->a()Z

    .line 61
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 62
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 63
    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 85
    const-string v0, "destroy erro"

    const-string v1, "MarkerDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_1
    :goto_1
    return-void

    .line 70
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    .line 72
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_4

    .line 73
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->q:Ljava/nio/FloatBuffer;

    .line 76
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_5

    .line 77
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->f:Ljava/nio/FloatBuffer;

    .line 80
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->h:Lcom/amap/api/maps/model/LatLng;

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->r:Ljava/lang/Object;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->u:[I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method y()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public z()Lcom/autonavi/amap/mapcore/IPoint;
    .locals 7

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->d()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    if-nez v0, :cond_0

    .line 233
    const/4 v6, 0x0

    .line 239
    :goto_0
    return-object v6

    .line 235
    :cond_0
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 236
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aj;->v:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/aj;->i:Lcom/amap/api/maps/model/LatLng;

    .line 237
    :goto_1
    iget-object v1, p0, Lcom/amap/api/mapcore/aj;->p:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/ah;->a()Lcom/amap/api/mapcore/r;

    move-result-object v1

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v1 .. v6}, Lcom/amap/api/mapcore/r;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_0

    .line 236
    :cond_1
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aj;->d()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    goto :goto_1
.end method
