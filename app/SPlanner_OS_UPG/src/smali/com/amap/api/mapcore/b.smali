.class Lcom/amap/api/mapcore/b;
.super Landroid/opengl/GLSurfaceView;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;
.implements Lcom/amap/api/mapcore/r;


# static fields
.field private static final aq:D

.field private static au:Landroid/os/Handler;


# instance fields
.field private A:Lcom/amap/api/mapcore/au;

.field private B:Lcom/amap/api/mapcore/ad;

.field private C:Lcom/amap/api/mapcore/k;

.field private D:Lcom/amap/api/mapcore/ap;

.field private E:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

.field private F:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

.field private G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

.field private H:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

.field private I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

.field private J:Lcom/amap/api/maps/AMap$OnMapClickListener;

.field private K:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

.field private L:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

.field private M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

.field private N:Landroid/view/View;

.field private O:Lcom/amap/api/mapcore/v;

.field private P:Lcom/amap/api/mapcore/z;

.field private Q:Lcom/amap/api/mapcore/ac;

.field private R:Lcom/amap/api/maps/LocationSource;

.field private S:Landroid/graphics/Rect;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Lcom/amap/api/mapcore/f;

.field private X:Lcom/amap/api/mapcore/util/b;

.field private Y:Lcom/amap/api/mapcore/ak;

.field private Z:Lcom/amap/api/mapcore/g;

.field a:Lcom/amap/api/mapcore/ag;

.field private aA:Z

.field private aB:Lcom/amap/api/maps/model/Marker;

.field private aC:Lcom/amap/api/mapcore/v;

.field private aD:Z

.field private aE:Z

.field private aF:Z

.field private aG:I

.field private aH:Z

.field private aI:Ljava/lang/Thread;

.field private aJ:Lcom/amap/api/maps/model/LatLngBounds;

.field private aK:Z

.field private aL:Z

.field private aM:Z

.field private aN:I

.field private aO:I

.field private aa:Lcom/amap/api/mapcore/h;

.field private ab:I

.field private ac:I

.field private ad:Lcom/amap/api/maps/AMap$CancelableCallback;

.field private ae:Z

.field private af:I

.field private ag:Z

.field private ah:Z

.field private ai:Landroid/graphics/drawable/Drawable;

.field private aj:Landroid/location/Location;

.field private ak:Ljava/lang/Boolean;

.field private final al:[I

.field private am:Z

.field private an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

.field private ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

.field private ap:Ljava/util/Timer;

.field private ar:Lcom/amap/api/mapcore/m;

.field private as:Z

.field private at:Z

.field private av:Ljava/lang/Runnable;

.field private aw:Z

.field private ax:Z

.field private ay:Z

.field private az:Z

.field b:Lcom/amap/api/mapcore/av;

.field c:Lcom/amap/api/mapcore/ar;

.field d:Z

.field e:Lcom/amap/api/maps/CustomRenderer;

.field f:Lcom/amap/api/mapcore/p;

.field g:Ljava/lang/Runnable;

.field final h:Landroid/os/Handler;

.field private i:I

.field private j:I

.field private k:Landroid/graphics/Bitmap;

.field private l:Landroid/graphics/Bitmap;

.field private m:Z

.field private n:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private o:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private p:I

.field private q:Lcom/autonavi/amap/mapcore/MapCore;

.field private r:Landroid/content/Context;

.field private s:Lcom/amap/api/mapcore/a;

.field private t:Lcom/autonavi/amap/mapcore/MapProjection;

.field private u:Landroid/view/GestureDetector;

.field private v:Landroid/view/ScaleGestureDetector;

.field private w:Lcom/amap/api/mapcore/util/c;

.field private x:Landroid/view/SurfaceHolder;

.field private y:Lcom/amap/api/mapcore/ai;

.field private z:Lcom/amap/api/mapcore/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 197
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/amap/api/mapcore/b;->aq:D

    .line 876
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/amap/api/mapcore/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 250
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    .line 251
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->x:Landroid/view/SurfaceHolder;

    .line 252
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 254
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    .line 287
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->i:I

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->j:I

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->m:Z

    .line 125
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 126
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->p:I

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    .line 139
    new-instance v0, Lcom/amap/api/mapcore/ag;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ag;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->x:Landroid/view/SurfaceHolder;

    .line 165
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->S:Landroid/graphics/Rect;

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->T:Z

    .line 167
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->U:Z

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->V:Z

    .line 169
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->d:Z

    .line 177
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b;->ab:I

    .line 178
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b;->ac:I

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ae:Z

    .line 181
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b;->af:I

    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ag:Z

    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ah:Z

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    .line 186
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Ljava/lang/Boolean;

    .line 187
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->al:[I

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    .line 213
    new-instance v0, Lcom/amap/api/mapcore/p;

    invoke-direct {v0}, Lcom/amap/api/mapcore/p;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    .line 430
    new-instance v0, Lcom/amap/api/mapcore/m;

    invoke-direct {v0}, Lcom/amap/api/mapcore/m;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Lcom/amap/api/mapcore/m;

    .line 874
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->as:Z

    .line 875
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    .line 877
    new-instance v0, Lcom/amap/api/mapcore/c;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/c;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->av:Ljava/lang/Runnable;

    .line 1136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aw:Z

    .line 1848
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ax:Z

    .line 2118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ay:Z

    .line 2122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->az:Z

    .line 2124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aA:Z

    .line 2125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    .line 2126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    .line 2127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aD:Z

    .line 2128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aE:Z

    .line 2129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aF:Z

    .line 2177
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b;->aG:I

    .line 2181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aH:Z

    .line 2886
    new-instance v0, Lcom/amap/api/mapcore/d;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/d;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    .line 2914
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aJ:Lcom/amap/api/maps/model/LatLngBounds;

    .line 2925
    new-instance v0, Lcom/amap/api/mapcore/e;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/e;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    .line 3134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aK:Z

    .line 3136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aL:Z

    .line 3137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    .line 288
    const/16 v0, 0xff

    const/16 v1, 0xeb

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->setBackgroundColor(I)V

    .line 289
    new-instance v0, Lcom/amap/api/mapcore/g;

    invoke-direct {v0}, Lcom/amap/api/mapcore/g;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Z:Lcom/amap/api/mapcore/g;

    .line 290
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    .line 291
    new-instance v0, Lcom/autonavi/amap/mapcore/MapCore;

    invoke-direct {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    .line 292
    new-instance v0, Lcom/amap/api/mapcore/a;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/a;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    .line 293
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0, v1}, Lcom/autonavi/amap/mapcore/MapCore;->setMapCallback(Lcom/autonavi/amap/mapcore/IMapCallback;)V

    .line 295
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapCore;->getMapstate()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    .line 305
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    const-wide v2, 0x4043f64cb5bb3850L    # 39.924216

    const-wide v4, 0x405d1976a004eda6L    # 116.3978653

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/high16 v2, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;FFF)Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 308
    new-instance v0, Lcom/amap/api/mapcore/ao;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ao;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->P:Lcom/amap/api/mapcore/z;

    .line 309
    new-instance v0, Lcom/amap/api/mapcore/f;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/f;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->W:Lcom/amap/api/mapcore/f;

    .line 310
    new-instance v0, Lcom/amap/api/mapcore/as;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/as;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Q:Lcom/amap/api/mapcore/ac;

    .line 319
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$c;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->u:Landroid/view/GestureDetector;

    .line 324
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Landroid/view/GestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$b;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 325
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Landroid/view/GestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 330
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$e;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$e;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->v:Landroid/view/ScaleGestureDetector;

    .line 336
    new-instance v0, Lcom/amap/api/mapcore/util/c;

    new-instance v1, Lcom/amap/api/mapcore/b$d;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$d;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/util/c;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/util/c$a;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/util/c;

    .line 342
    new-instance v0, Lcom/amap/api/mapcore/util/b;

    new-instance v1, Lcom/amap/api/mapcore/b$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$a;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/util/b;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/util/b$a;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->X:Lcom/amap/api/mapcore/util/b;

    .line 347
    new-instance v0, Lcom/amap/api/mapcore/ai;

    invoke-direct {v0, p1, p0}, Lcom/amap/api/mapcore/ai;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    .line 348
    new-instance v0, Lcom/amap/api/mapcore/au;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/au;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    .line 349
    new-instance v0, Lcom/amap/api/mapcore/ap;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/ap;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    .line 350
    new-instance v0, Lcom/amap/api/mapcore/ar;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/ar;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    .line 351
    new-instance v0, Lcom/amap/api/mapcore/av;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/av;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    .line 353
    new-instance v0, Lcom/amap/api/mapcore/ad;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-direct {v0, v1, v2, p0}, Lcom/amap/api/mapcore/ad;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ag;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    .line 354
    new-instance v0, Lcom/amap/api/mapcore/k;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-direct {v0, v1, v2, p0}, Lcom/amap/api/mapcore/k;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ag;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    .line 355
    new-instance v0, Lcom/amap/api/mapcore/ah;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p0}, Lcom/amap/api/mapcore/ah;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    .line 357
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/au;->setBackgroundColor(I)V

    .line 358
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->setBackgroundColor(I)V

    .line 359
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ai;->setBackgroundColor(I)V

    .line 360
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ar;->setBackgroundColor(I)V

    .line 361
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setBackgroundColor(I)V

    .line 362
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ah;->setBackgroundColor(I)V

    .line 363
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setBackgroundColor(I)V

    .line 365
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 368
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 370
    new-instance v1, Lcom/amap/api/mapcore/ai$a;

    invoke-direct {v1, v0}, Lcom/amap/api/mapcore/ai$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 373
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v2, v3, v1}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 375
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 377
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 379
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 381
    new-instance v0, Lcom/amap/api/mapcore/ai$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x53

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 385
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 387
    new-instance v0, Lcom/amap/api/mapcore/ai$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x53

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 392
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 394
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->w()Lcom/amap/api/mapcore/ac;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/ac;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setVisibility(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :cond_0
    :goto_0
    new-instance v0, Lcom/amap/api/mapcore/ai$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x33

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 406
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 407
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    .line 409
    new-instance v0, Lcom/amap/api/mapcore/h;

    invoke-direct {v0, p1}, Lcom/amap/api/mapcore/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    .line 411
    new-instance v0, Lcom/amap/api/mapcore/ak;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ak;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    .line 413
    invoke-virtual {p0, p0}, Lcom/amap/api/mapcore/b;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 416
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->ZoomControllerViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setId(I)V

    .line 417
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->ScaleControlsViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->setId(I)V

    .line 418
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->MyLocationViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setId(I)V

    .line 419
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->CompassViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setId(I)V

    .line 428
    return-void

    .line 397
    :catch_0
    move-exception v0

    .line 398
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 187
    nop

    :array_0
    .array-data 4
        0x989680
        0x4c4b40
        0x1e8480
        0xf4240
        0x7a120
        0x30d40
        0x186a0
        0xc350
        0x7530
        0x4e20
        0x2710
        0x1388
        0x7d0
        0x3e8
        0x1f4
        0xc8
        0x64
        0x32
        0x19
        0xa
        0x5
    .end array-data
.end method

.method static synthetic A(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapClickListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Lcom/amap/api/maps/AMap$OnMapClickListener;

    return-object v0
.end method

.method static synthetic B(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aE:Z

    return v0
.end method

.method static synthetic C(Lcom/amap/api/mapcore/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic D(Lcom/amap/api/mapcore/b;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic E(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/g;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Z:Lcom/amap/api/mapcore/g;

    return-object v0
.end method

.method static synthetic F(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnCameraChangeListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    return-object v0
.end method

.method static synthetic G(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLoadedListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->H:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

    return-object v0
.end method

.method static synthetic H(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/k;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    return-object v0
.end method

.method static synthetic I(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/au;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    return-object v0
.end method

.method static synthetic J(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/v;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    return-object v0
.end method

.method static synthetic K(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    return-object v0
.end method

.method static synthetic L(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    return-object v0
.end method

.method static synthetic M(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ae:Z

    return v0
.end method

.method static synthetic N(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ah:Z

    return v0
.end method

.method static synthetic P()D
    .locals 2

    .prologue
    .line 93
    sget-wide v0, Lcom/amap/api/mapcore/b;->aq:D

    return-wide v0
.end method

.method private Q()V
    .locals 1

    .prologue
    .line 939
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    if-eqz v0, :cond_0

    .line 941
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapCore;->destroy()V

    .line 944
    :cond_0
    return-void
.end method

.method private declared-synchronized R()V
    .locals 6

    .prologue
    .line 1120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1121
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->S()V

    .line 1123
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 1124
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    .line 1126
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    new-instance v1, Lcom/amap/api/mapcore/b$f;

    invoke-direct {v1, p0, p0}, Lcom/amap/api/mapcore/b$f;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x14

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1127
    monitor-exit p0

    return-void

    .line 1120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized S()V
    .locals 1

    .prologue
    .line 1130
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1131
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1134
    :cond_0
    monitor-exit p0

    return-void

    .line 1130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized T()V
    .locals 3

    .prologue
    .line 1143
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aw:Z

    if-nez v0, :cond_0

    .line 1144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aw:Z

    .line 1147
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "bk.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1150
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1151
    const/16 v1, 0xf0

    if-lt v0, v1, :cond_1

    .line 1152
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v1, "icn_h.data"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->a([B)[B

    move-result-object v0

    .line 1154
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1163
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "roadarrow.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1166
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "LineRound.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1169
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "tgl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1172
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "trl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1175
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "tyl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1178
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "dash.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1185
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 1157
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v1, "icn.data"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->a([B)[B

    move-result-object v0

    .line 1159
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1182
    :catch_0
    move-exception v0

    .line 1183
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private U()Lcom/amap/api/maps/model/LatLng;
    .locals 6

    .prologue
    .line 2087
    new-instance v0, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2088
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2089
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 2090
    iget v2, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v2, v1, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2091
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v0, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v4, v0, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 2092
    return-object v1
.end method

.method private V()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2134
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aH:Z

    if-eqz v0, :cond_0

    .line 2135
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->aH:Z

    .line 2136
    :cond_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aD:Z

    if-eqz v0, :cond_1

    .line 2137
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->aD:Z

    .line 2138
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2140
    iput-boolean v3, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 2141
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 2143
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ay:Z

    if-eqz v0, :cond_2

    .line 2144
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->ay:Z

    .line 2145
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2147
    iput-boolean v3, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 2148
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 2150
    :cond_2
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->az:Z

    .line 2151
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->aA:Z

    .line 2152
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_3

    .line 2153
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$OnMarkerDragListener;->onMarkerDragEnd(Lcom/amap/api/maps/model/Marker;)V

    .line 2154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    .line 2156
    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 93
    iput p1, p0, Lcom/amap/api/mapcore/b;->aG:I

    return p1
.end method

.method public static a(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 3166
    mul-int v0, p2, p3

    new-array v9, v0, [I

    .line 3167
    mul-int v0, p2, p3

    new-array v10, v0, [I

    .line 3168
    invoke-static {v9}, Ljava/nio/IntBuffer;->wrap([I)Ljava/nio/IntBuffer;

    move-result-object v7

    .line 3169
    invoke-virtual {v7, v8}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 3170
    const/16 v5, 0x1908

    const/16 v6, 0x1401

    move-object v0, p4

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    move v1, v8

    .line 3176
    :goto_0
    if-ge v1, p3, :cond_1

    move v0, v8

    .line 3177
    :goto_1
    if-ge v0, p2, :cond_0

    .line 3178
    mul-int v2, v1, p2

    add-int/2addr v2, v0

    aget v2, v9, v2

    .line 3179
    shr-int/lit8 v3, v2, 0x10

    and-int/lit16 v3, v3, 0xff

    .line 3180
    shl-int/lit8 v4, v2, 0x10

    const/high16 v5, 0xff0000

    and-int/2addr v4, v5

    .line 3181
    const v5, -0xff0100

    and-int/2addr v2, v5

    or-int/2addr v2, v4

    or-int/2addr v2, v3

    .line 3182
    sub-int v3, p3, v1

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, p2

    add-int/2addr v3, v0

    aput v2, v10, v3

    .line 3177
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3176
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3188
    :cond_1
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3189
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v10

    move v3, p2

    move v6, p2

    move v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 3194
    :goto_2
    return-object v0

    .line 3190
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 3191
    const/4 v0, 0x0

    .line 3192
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/v;)Lcom/amap/api/mapcore/v;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$CancelableCallback;)Lcom/amap/api/maps/AMap$CancelableCallback;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;)Lcom/amap/api/maps/model/LatLngBounds;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->aJ:Lcom/amap/api/maps/model/LatLngBounds;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/Marker;)Lcom/amap/api/maps/model/Marker;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    return-object p1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 10

    .prologue
    .line 2159
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aA:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_0

    .line 2160
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 2161
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    const/high16 v2, 0x42700000    # 60.0f

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 2162
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    invoke-interface {v2}, Lcom/amap/api/mapcore/v;->e()Lcom/amap/api/maps/model/LatLng;

    move-result-object v2

    .line 2163
    iget-object v3, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    invoke-interface {v3}, Lcom/amap/api/mapcore/v;->d()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    .line 2164
    new-instance v4, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2165
    invoke-virtual {p0, v0, v1, v4}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2166
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v8, v4, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    add-double/2addr v6, v8

    iget-wide v8, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    sub-double/2addr v6, v8

    iget-wide v8, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-wide v4, v4, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    add-double/2addr v4, v8

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    sub-double v2, v4, v2

    invoke-direct {v0, v6, v7, v2, v3}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 2169
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/model/Marker;->setPosition(Lcom/amap/api/maps/model/LatLng;)V

    .line 2170
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$OnMarkerDragListener;->onMarkerDrag(Lcom/amap/api/maps/model/Marker;)V

    .line 2172
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->Q()V

    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->as:Z

    return p1
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 93
    iput p1, p0, Lcom/amap/api/mapcore/b;->ab:I

    return p1
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    return v0
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->at:Z

    return p1
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 93
    iput p1, p0, Lcom/amap/api/mapcore/b;->ac:I

    return p1
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ay:Z

    return v0
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->az:Z

    return p1
.end method

.method static synthetic d(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->az:Z

    return v0
.end method

.method static synthetic d(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aD:Z

    return p1
.end method

.method static synthetic e(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ac;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Q:Lcom/amap/api/mapcore/ac;

    return-object v0
.end method

.method static synthetic e(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aF:Z

    return p1
.end method

.method static synthetic f(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    return v0
.end method

.method static synthetic f(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aH:Z

    return p1
.end method

.method static synthetic g(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/amap/api/mapcore/b;->aN:I

    return v0
.end method

.method static synthetic g(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aA:Z

    return p1
.end method

.method static synthetic h(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/amap/api/mapcore/b;->aO:I

    return v0
.end method

.method static synthetic h(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aE:Z

    return p1
.end method

.method static synthetic i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    return-object v0
.end method

.method static synthetic i(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ay:Z

    return p1
.end method

.method static synthetic j(Lcom/amap/api/mapcore/b;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->V()V

    return-void
.end method

.method static synthetic j(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ag:Z

    return p1
.end method

.method static synthetic k(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aF:Z

    return v0
.end method

.method static synthetic k(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ah:Z

    return p1
.end method

.method static synthetic l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    return-object v0
.end method

.method static synthetic m(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$CancelableCallback;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    return-object v0
.end method

.method static synthetic n(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/util/b;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->X:Lcom/amap/api/mapcore/util/b;

    return-object v0
.end method

.method static synthetic o(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/amap/api/mapcore/b;->ab:I

    return v0
.end method

.method static synthetic p(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/amap/api/mapcore/b;->ac:I

    return v0
.end method

.method static synthetic q(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLongClickListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    return-object v0
.end method

.method static synthetic r(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ah;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    return-object v0
.end method

.method static synthetic s(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerDragListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    return-object v0
.end method

.method static synthetic t(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/v;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    return-object v0
.end method

.method static synthetic u(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/model/Marker;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    return-object v0
.end method

.method static synthetic v(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/amap/api/mapcore/b;->aG:I

    return v0
.end method

.method static synthetic w(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aH:Z

    return v0
.end method

.method static synthetic x(Lcom/amap/api/mapcore/b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    return-object v0
.end method

.method static synthetic y(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->L:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

    return-object v0
.end method

.method static synthetic z(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerClickListener;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->F:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2622
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2623
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 2624
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ai;->removeView(Landroid/view/View;)V

    .line 2625
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2626
    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2627
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2636
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    .line 2638
    :cond_0
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    .line 2639
    return-void
.end method

.method public B()F
    .locals 1

    .prologue
    .line 2883
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v0

    return v0
.end method

.method C()V
    .locals 2

    .prologue
    .line 2917
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2918
    return-void
.end method

.method public D()Lcom/amap/api/maps/model/LatLngBounds;
    .locals 1

    .prologue
    .line 2922
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aJ:Lcom/amap/api/maps/model/LatLngBounds;

    return-object v0
.end method

.method E()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3145
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aK:Z

    if-nez v0, :cond_1

    .line 3146
    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->setBackgroundColor(I)V

    .line 3147
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/au;->setBackgroundColor(I)V

    .line 3148
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->setBackgroundColor(I)V

    .line 3149
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ai;->setBackgroundColor(I)V

    .line 3150
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ar;->setBackgroundColor(I)V

    .line 3151
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    if-eqz v0, :cond_0

    .line 3152
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setBackgroundColor(I)V

    .line 3154
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ah;->setBackgroundColor(I)V

    .line 3155
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setBackgroundColor(I)V

    .line 3156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aK:Z

    .line 3157
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->postInvalidate()V

    .line 3159
    :cond_1
    return-void
.end method

.method F()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 3162
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/au;->c()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public G()F
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 3250
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    .line 3251
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 3252
    new-instance v2, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 3253
    invoke-virtual {p0, v3, v3, v1}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 3254
    invoke-virtual {p0, v0, v3, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 3255
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v2, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v2, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/amap/api/mapcore/util/v;->a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)D

    move-result-wide v2

    .line 3258
    int-to-double v0, v0

    div-double v0, v2, v0

    double-to-float v0, v0

    return v0
.end method

.method public H()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3299
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 3300
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3301
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 3302
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    .line 3303
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3305
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public I()Ljava/util/List;
    .locals 2

    .prologue
    .line 3310
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "\u5730\u56fe\u672a\u521d\u59cb\u5316\u5b8c\u6210\uff01"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/e;->a(ZLjava/lang/Object;)V

    .line 3312
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ah;->f()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 3310
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()V
    .locals 1

    .prologue
    .line 3317
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->c()V

    .line 3318
    return-void
.end method

.method public K()V
    .locals 1

    .prologue
    .line 3327
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aL:Z

    .line 3328
    return-void
.end method

.method public L()Z
    .locals 1

    .prologue
    .line 3332
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aL:Z

    return v0
.end method

.method public M()V
    .locals 1

    .prologue
    .line 3337
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    if-eqz v0, :cond_0

    .line 3338
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ah;->g()V

    .line 3340
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aL:Z

    .line 3341
    return-void
.end method

.method public N()I
    .locals 1

    .prologue
    .line 3365
    iget v0, p0, Lcom/amap/api/mapcore/b;->af:I

    return v0
.end method

.method public O()Z
    .locals 1

    .prologue
    .line 3370
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->U:Z

    return v0
.end method

.method public a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/s;
    .locals 4

    .prologue
    .line 1700
    if-nez p1, :cond_0

    .line 1701
    const/4 v0, 0x0

    .line 1713
    :goto_0
    return-object v0

    .line 1703
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/j;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/j;-><init>(Lcom/amap/api/mapcore/r;)V

    .line 1704
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->b(I)V

    .line 1705
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getCenter()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 1706
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(Z)V

    .line 1707
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->b(F)V

    .line 1708
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(F)V

    .line 1709
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(I)V

    .line 1710
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getRadius()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/amap/api/mapcore/j;->a(D)V

    .line 1711
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/w;)V

    .line 1712
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/GroundOverlayOptions;)Lcom/amap/api/mapcore/t;
    .locals 3

    .prologue
    .line 1719
    if-nez p1, :cond_0

    .line 1720
    const/4 v0, 0x0

    .line 1737
    :goto_0
    return-object v0

    .line 1722
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/q;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/q;-><init>(Lcom/amap/api/mapcore/r;)V

    .line 1724
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getAnchorU()F

    move-result v1

    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getAnchorV()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/mapcore/q;->b(FF)V

    .line 1726
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getWidth()F

    move-result v1

    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getHeight()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/mapcore/q;->a(FF)V

    .line 1728
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getImage()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/BitmapDescriptor;)V

    .line 1729
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getLocation()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 1730
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getBounds()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/LatLngBounds;)V

    .line 1731
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getBearing()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->c(F)V

    .line 1732
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getTransparency()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->d(F)V

    .line 1733
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(Z)V

    .line 1734
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(F)V

    .line 1735
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/w;)V

    .line 1736
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/PolygonOptions;)Lcom/amap/api/mapcore/x;
    .locals 2

    .prologue
    .line 1682
    if-nez p1, :cond_0

    .line 1683
    const/4 v0, 0x0

    .line 1694
    :goto_0
    return-object v0

    .line 1685
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/am;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/am;-><init>(Lcom/amap/api/mapcore/r;)V

    .line 1686
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->a(I)V

    .line 1687
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->a(Ljava/util/List;)V

    .line 1688
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->a(Z)V

    .line 1689
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->b(F)V

    .line 1690
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->a(F)V

    .line 1691
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->b(I)V

    .line 1692
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/w;)V

    .line 1693
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/mapcore/y;
    .locals 3

    .prologue
    .line 1653
    if-nez p1, :cond_0

    .line 1654
    const/4 v0, 0x0

    .line 1676
    :goto_0
    return-object v0

    .line 1657
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/an;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/an;-><init>(Lcom/amap/api/mapcore/r;)V

    .line 1658
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(I)V

    .line 1659
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isGeodesic()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->b(Z)V

    .line 1660
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isDottedLine()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->c(Z)V

    .line 1661
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(Ljava/util/List;)V

    .line 1662
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(Z)V

    .line 1663
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->b(F)V

    .line 1664
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(F)V

    .line 1665
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isUseTexture()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->d(Z)V

    .line 1667
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getCustomTexture()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1668
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getCustomTexture()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/maps/model/BitmapDescriptor;->clone()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    .line 1669
    invoke-virtual {v1}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1670
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 1671
    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/an;->a(Landroid/graphics/Bitmap;)V

    .line 1674
    :cond_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/w;)V

    .line 1675
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method a()Lcom/amap/api/maps/CustomRenderer;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    return-object v0
.end method

.method public a(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;
    .locals 2

    .prologue
    .line 1742
    if-nez p1, :cond_0

    .line 1743
    const/4 v0, 0x0

    .line 1749
    :goto_0
    return-object v0

    .line 1745
    :cond_0
    new-instance v1, Lcom/amap/api/mapcore/aj;

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-direct {v1, p1, v0}, Lcom/amap/api/mapcore/aj;-><init>(Lcom/amap/api/maps/model/MarkerOptions;Lcom/amap/api/mapcore/ah;)V

    .line 1747
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ah;->a(Lcom/amap/api/mapcore/v;)V

    .line 1748
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1749
    new-instance v0, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/v;)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/TileOverlayOptions;)Lcom/amap/api/maps/model/TileOverlay;
    .locals 2

    .prologue
    .line 1775
    if-nez p1, :cond_0

    .line 1776
    const/4 v0, 0x0

    .line 1782
    :goto_0
    return-object v0

    .line 1778
    :cond_0
    new-instance v1, Lcom/amap/api/mapcore/aq;

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-direct {v1, p1, v0}, Lcom/amap/api/mapcore/aq;-><init>(Lcom/amap/api/maps/model/TileOverlayOptions;Lcom/amap/api/mapcore/ar;)V

    .line 1780
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ar;->a(Lcom/amap/api/mapcore/ab;)V

    .line 1781
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1782
    new-instance v0, Lcom/amap/api/maps/model/TileOverlay;

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/TileOverlay;-><init>(Lcom/amap/api/mapcore/ab;)V

    goto :goto_0
.end method

.method public a(DDLcom/autonavi/amap/mapcore/FPoint;)V
    .locals 3

    .prologue
    .line 2033
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2035
    invoke-static {p3, p4, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2036
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v1, v2, v0, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2037
    return-void
.end method

.method public a(DDLcom/autonavi/amap/mapcore/IPoint;)V
    .locals 1

    .prologue
    .line 2042
    invoke-static {p3, p4, p1, p2, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2043
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 595
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 596
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ak;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ak;->a(F)V

    .line 598
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 5

    .prologue
    const/16 v4, 0x7db

    const/4 v0, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1825
    if-ne p1, v0, :cond_0

    .line 1826
    iput v0, p0, Lcom/amap/api/mapcore/b;->p:I

    .line 1827
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    new-instance v1, Lcom/amap/api/mapcore/af;

    invoke-direct {v1, v4}, Lcom/amap/api/mapcore/af;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/af;->a(Z)Lcom/amap/api/mapcore/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/af;)V

    .line 1830
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/au;->a(Z)V

    .line 1838
    :goto_0
    return-void

    .line 1832
    :cond_0
    iput v2, p0, Lcom/amap/api/mapcore/b;->p:I

    .line 1833
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    new-instance v1, Lcom/amap/api/mapcore/af;

    invoke-direct {v1, v4}, Lcom/amap/api/mapcore/af;-><init>(I)V

    invoke-virtual {v1, v3}, Lcom/amap/api/mapcore/af;->a(Z)Lcom/amap/api/mapcore/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/af;)V

    .line 1836
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0, v3}, Lcom/amap/api/mapcore/au;->a(Z)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 3345
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_0

    .line 3346
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    .line 3347
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0, p1, p2}, Lcom/amap/api/mapcore/a;->a(II)V

    .line 3348
    iput p1, p0, Lcom/amap/api/mapcore/b;->aN:I

    .line 3349
    iput p2, p0, Lcom/amap/api/mapcore/b;->aO:I

    .line 3351
    :cond_0
    return-void
.end method

.method public a(IIII)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1107
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v1, 0x899

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1111
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v1, 0x89a

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1117
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/DPoint;)V
    .locals 4

    .prologue
    .line 2016
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2018
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2019
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2020
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v2, v3, v0, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2021
    iget v0, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v0, v1, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2022
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/FPoint;)V
    .locals 1

    .prologue
    .line 2047
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0, p1, p2, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2048
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/IPoint;)V
    .locals 3

    .prologue
    .line 2026
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2027
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2028
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v1, v2, v0, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2029
    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 607
    if-nez p1, :cond_0

    .line 643
    :goto_0
    return-void

    .line 610
    :cond_0
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 613
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    if-nez v0, :cond_5

    .line 614
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ak;->a()V

    .line 615
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 627
    :catch_0
    move-exception v0

    .line 628
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 630
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/amap/api/mapcore/ak;->a(Lcom/amap/api/maps/model/LatLng;D)V

    .line 632
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    if-eqz v0, :cond_4

    .line 633
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_4

    .line 638
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    invoke-interface {v0, p1}, Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;->onMyLocationChange(Landroid/location/Location;)V

    .line 641
    :cond_4
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    .line 642
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0

    .line 618
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    if-nez v0, :cond_2

    .line 619
    :cond_6
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    if-nez v0, :cond_7

    .line 620
    new-instance v0, Lcom/amap/api/mapcore/ak;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ak;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    .line 622
    :cond_7
    if-eqz v1, :cond_2

    .line 623
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v0

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;F)Lcom/amap/api/mapcore/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public a(Lcom/amap/api/mapcore/i;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1295
    iget-object v0, p1, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v2, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-ne v0, v2, :cond_0

    .line 1296
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "the map must have a size"

    invoke-static {v0, v2}, Lcom/amap/api/mapcore/util/e;->a(ZLjava/lang/Object;)V

    .line 1299
    :cond_0
    iput-boolean v1, p1, Lcom/amap/api/mapcore/i;->p:Z

    .line 1300
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    iput-boolean v0, p1, Lcom/amap/api/mapcore/i;->n:Z

    .line 1301
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 1302
    return-void

    .line 1296
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 24

    .prologue
    .line 1324
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_0

    .line 1325
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v2

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    const-string v3, "the map must have a size"

    invoke-static {v2, v3}, Lcom/amap/api/mapcore/util/e;->a(ZLjava/lang/Object;)V

    .line 1328
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Z)V

    .line 1330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    if-eqz v2, :cond_1

    .line 1331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-interface {v2}, Lcom/amap/api/maps/AMap$CancelableCallback;->onCancel()V

    .line 1333
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->b(Z)V

    .line 1334
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1335
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/amap/api/mapcore/b;->ag:Z

    if-eqz v2, :cond_2

    .line 1336
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->ah:Z

    .line 1338
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->ae:Z

    .line 1339
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->k:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_5

    .line 1340
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->c:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    .line 1342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1635
    :goto_1
    return-void

    .line 1325
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1345
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->b(Z)V

    .line 1346
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1347
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1348
    new-instance v10, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v10}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1349
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p1

    iget v4, v0, Lcom/amap/api/mapcore/i;->b:F

    float-to-int v4, v4

    add-int/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p1

    iget v5, v0, Lcom/amap/api/mapcore/i;->c:F

    float-to-int v5, v5

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v10}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1352
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v8}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    iget v9, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v11, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v9, v11

    iget v10, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v10, v2

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    .line 1634
    :goto_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_1

    .line 1358
    :cond_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->b:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_8

    .line 1359
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1360
    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v2, v6

    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v2

    sub-float v11, v2, v6

    .line 1361
    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_6

    .line 1362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1365
    :cond_6
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1366
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_7

    .line 1367
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1371
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1372
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto :goto_2

    .line 1369
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_3

    .line 1375
    :cond_8
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->h:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_b

    .line 1376
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1377
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v6, v2

    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v2

    sub-float v11, v2, v6

    .line 1378
    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_9

    .line 1379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1382
    :cond_9
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1383
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_a

    .line 1384
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1388
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1389
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1386
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_4

    .line 1392
    :cond_b
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->i:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_e

    .line 1393
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1394
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->d:F

    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v2

    sub-float v11, v2, v6

    .line 1396
    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_c

    .line 1397
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1400
    :cond_c
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1401
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_d

    .line 1402
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1406
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1407
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1404
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_5

    .line 1410
    :cond_e
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->j:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_12

    .line 1411
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->b(Z)V

    .line 1412
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->e:F

    .line 1413
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1414
    add-float v3, v6, v2

    invoke-static {v3}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v3

    sub-float v11, v3, v6

    .line 1415
    const/4 v3, 0x0

    cmpl-float v3, v11, v3

    if-nez v3, :cond_f

    .line 1416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1419
    :cond_f
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->m:Landroid/graphics/Point;

    .line 1420
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1421
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v4, v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1422
    const/4 v9, 0x0

    .line 1423
    const/4 v10, 0x0

    .line 1424
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1425
    if-eqz v3, :cond_11

    .line 1426
    iget v7, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3, v4}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1427
    iget v3, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v3, v7

    .line 1428
    iget v7, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v4, v7, v4

    .line 1429
    int-to-double v8, v3

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    float-to-double v14, v2

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    div-double/2addr v8, v12

    int-to-double v12, v3

    sub-double/2addr v8, v12

    double-to-int v9, v8

    .line 1430
    int-to-double v12, v4

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    float-to-double v2, v2

    invoke-static {v14, v15, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double v2, v12, v2

    int-to-double v12, v4

    sub-double/2addr v2, v12

    double-to-int v10, v2

    .line 1440
    :cond_10
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1441
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1432
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_10

    .line 1433
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7, v4}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1434
    iget v3, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v3, v7

    .line 1435
    iget v7, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v4, v7, v4

    .line 1436
    int-to-double v8, v3

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    float-to-double v14, v2

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    div-double/2addr v8, v12

    int-to-double v12, v3

    sub-double/2addr v8, v12

    double-to-int v9, v8

    .line 1437
    int-to-double v12, v4

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    float-to-double v2, v2

    invoke-static {v14, v15, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double v2, v12, v2

    int-to-double v12, v4

    sub-double/2addr v2, v12

    double-to-int v10, v2

    goto :goto_6

    .line 1444
    :cond_12
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->l:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_16

    .line 1445
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1446
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_14

    .line 1447
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1451
    :goto_7
    new-instance v3, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1452
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/amap/api/mapcore/i;->h:Lcom/amap/api/maps/model/CameraPosition;

    .line 1453
    iget-object v5, v4, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v5, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-object v5, v4, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v5, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-static {v6, v7, v8, v9, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1455
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1456
    iget v5, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v9, v5, v7

    .line 1457
    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v10, v3, v5

    .line 1458
    iget v3, v4, Lcom/amap/api/maps/model/CameraPosition;->zoom:F

    invoke-static {v3}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v3

    sub-float v11, v3, v6

    .line 1459
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    .line 1460
    iget v3, v4, Lcom/amap/api/maps/model/CameraPosition;->bearing:F

    const/high16 v5, 0x43b40000    # 360.0f

    rem-float/2addr v3, v5

    const/high16 v5, 0x43b40000    # 360.0f

    rem-float v5, v7, v5

    sub-float v12, v3, v5

    .line 1461
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v5, 0x43340000    # 180.0f

    cmpl-float v3, v3, v5

    if-ltz v3, :cond_13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v3

    const/high16 v5, 0x43b40000    # 360.0f

    mul-float/2addr v3, v5

    sub-float/2addr v12, v3

    .line 1463
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    .line 1464
    iget v3, v4, Lcom/amap/api/maps/model/CameraPosition;->tilt:F

    invoke-static {v3}, Lcom/amap/api/mapcore/util/v;->a(F)F

    move-result v3

    sub-float v13, v3, v8

    .line 1465
    if-nez v9, :cond_15

    if-nez v10, :cond_15

    const/4 v3, 0x0

    cmpl-float v3, v11, v3

    if-nez v3, :cond_15

    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-nez v3, :cond_15

    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-nez v3, :cond_15

    .line 1467
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1449
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto/16 :goto_7

    .line 1470
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1471
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1473
    :cond_16
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->e:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_1a

    .line 1474
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    .line 1475
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->g:F

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v2, v3

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float v3, v7, v3

    sub-float v12, v2, v3

    .line 1477
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x43340000    # 180.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_17

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    sub-float/2addr v12, v2

    .line 1479
    :cond_17
    const/4 v2, 0x0

    cmpl-float v2, v12, v2

    if-nez v2, :cond_18

    .line 1480
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1483
    :cond_18
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1484
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_19

    .line 1485
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1489
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1490
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1487
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_8

    .line 1493
    :cond_1a
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->d:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_1d

    .line 1494
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    .line 1495
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->f:F

    sub-float v13, v2, v8

    .line 1496
    const/4 v2, 0x0

    cmpl-float v2, v13, v2

    if-nez v2, :cond_1b

    .line 1497
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1500
    :cond_1b
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1501
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_1c

    .line 1502
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1506
    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1507
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1504
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_9

    .line 1510
    :cond_1d
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->c:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_20

    .line 1511
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1512
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_1e

    .line 1513
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1517
    :goto_a
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->o:Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v9, v3, v4

    .line 1518
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->o:Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v10, v3, v4

    .line 1519
    if-nez v9, :cond_1f

    if-nez v10, :cond_1f

    .line 1520
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1515
    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_a

    .line 1523
    :cond_1f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1525
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1528
    :cond_20
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-eq v2, v3, :cond_21

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->n:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_2a

    .line 1530
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->b(Z)V

    .line 1533
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_23

    .line 1534
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v3

    .line 1535
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v2

    move v15, v2

    move/from16 v16, v3

    .line 1540
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float v18, v2, v3

    .line 1541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v19

    .line 1542
    const/4 v2, 0x0

    cmpl-float v2, v18, v2

    if-nez v2, :cond_22

    const/4 v2, 0x0

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_24

    :cond_22
    const/4 v2, 0x1

    .line 1543
    :goto_c
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/amap/api/mapcore/i;->i:Lcom/amap/api/maps/model/LatLngBounds;

    .line 1544
    move-object/from16 v0, p1

    iget v0, v0, Lcom/amap/api/mapcore/i;->j:I

    move/from16 v20, v0

    .line 1545
    new-instance v21, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v21 .. v21}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1546
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1547
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v17

    .line 1548
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1549
    if-nez v2, :cond_28

    .line 1550
    new-instance v22, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v22 .. v22}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1551
    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-object v4, v9, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    iget-object v4, v9, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v6, v9, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps/model/LatLng;->latitude:D

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    move-object/from16 v0, v22

    invoke-static {v2, v3, v4, v5, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1556
    new-instance v8, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1557
    new-instance v14, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v14}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1558
    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/amap/api/mapcore/b;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1560
    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v10, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v12, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v14}, Lcom/amap/api/mapcore/b;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1563
    iget v2, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v3, v14, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v2, v3

    .line 1564
    iget v3, v14, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v3, v4

    .line 1566
    if-nez v2, :cond_25

    if-nez v3, :cond_25

    .line 1567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1537
    :cond_23
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->k:I

    .line 1538
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->l:I

    move v15, v2

    move/from16 v16, v3

    goto/16 :goto_b

    .line 1542
    :cond_24
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 1571
    :cond_25
    int-to-float v2, v2

    mul-int/lit8 v4, v20, 0x2

    sub-int v4, v16, v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    .line 1573
    int-to-float v3, v3

    mul-int/lit8 v4, v20, 0x2

    sub-int v4, v15, v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 1576
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1578
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_26

    .line 1579
    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->c(F)I

    move-result v2

    int-to-float v2, v2

    sub-float v2, v17, v2

    .line 1581
    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v2

    .line 1587
    :goto_d
    move-object/from16 v0, v22

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v9, v3, v4

    .line 1588
    move-object/from16 v0, v22

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v10, v3, v4

    .line 1589
    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v2

    sub-float v11, v2, v17

    .line 1590
    if-nez v9, :cond_27

    if-nez v10, :cond_27

    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_27

    .line 1591
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1582
    :cond_26
    float-to-double v4, v2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2b

    .line 1583
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v2, v3, v2

    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->c(F)I

    move-result v2

    int-to-float v2, v2

    add-float v2, v2, v17

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    .line 1585
    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v2

    goto :goto_d

    .line 1594
    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1595
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    move-object/from16 v0, v21

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v21

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    const/4 v12, 0x0

    const/4 v13, 0x0

    move/from16 v6, v17

    move/from16 v7, v18

    move/from16 v8, v19

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1598
    :cond_28
    move/from16 v0, v18

    neg-float v12, v0

    .line 1599
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x43340000    # 180.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_29

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    sub-float/2addr v12, v2

    .line 1601
    :cond_29
    move/from16 v0, v19

    neg-float v13, v0

    .line 1603
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1604
    new-instance v2, Lcom/amap/api/mapcore/b$3;

    move-object/from16 v3, p0

    move-object v4, v9

    move/from16 v5, v16

    move v6, v15

    move/from16 v7, v20

    move-wide/from16 v8, p2

    invoke-direct/range {v2 .. v10}, Lcom/amap/api/mapcore/b$3;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;IIIJLcom/amap/api/maps/AMap$CancelableCallback;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1625
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->ae:Z

    .line 1626
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    move-object/from16 v0, v21

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v21

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v14, 0xfa

    move/from16 v6, v17

    move/from16 v7, v18

    move/from16 v8, v19

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1631
    :cond_2a
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 1632
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    goto/16 :goto_2

    :cond_2b
    move/from16 v2, v17

    goto/16 :goto_d
.end method

.method public a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 2

    .prologue
    .line 1315
    const-wide/16 v0, 0xfa

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V

    .line 1317
    return-void
.end method

.method public a(Lcom/amap/api/mapcore/o;)V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Lcom/amap/api/mapcore/m;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/m;->a(Lcom/amap/api/mapcore/o;)Z

    .line 439
    return-void
.end method

.method public a(Lcom/amap/api/mapcore/v;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, -0x2

    const/high16 v5, -0x1000000

    .line 2519
    .line 2520
    if-nez p1, :cond_1

    .line 2604
    :cond_0
    :goto_0
    return-void

    .line 2523
    :cond_1
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2527
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    if-eqz v0, :cond_0

    .line 2528
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V

    .line 2529
    new-instance v1, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v1, p1}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/v;)V

    .line 2530
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$InfoWindowAdapter;->getInfoWindow(Lcom/amap/api/maps/model/Marker;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    .line 2533
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_3

    .line 2534
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v3, "infowindow_bg.9.png"

    invoke-static {v0, v3}, Lcom/amap/api/mapcore/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2542
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-nez v0, :cond_4

    .line 2543
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$InfoWindowAdapter;->getInfoContents(Lcom/amap/api/maps/model/Marker;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    .line 2545
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 2546
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2554
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2584
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2585
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 2586
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setDrawingCacheQuality(I)V

    .line 2587
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->c()Landroid/graphics/Rect;

    move-result-object v1

    .line 2588
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->f()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v3

    .line 2589
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    iget v5, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v4, v5

    .line 2590
    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v5, v1, v3

    .line 2593
    if-eqz v0, :cond_7

    .line 2594
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2595
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2597
    :goto_3
    new-instance v0, Lcom/amap/api/mapcore/ai$a;

    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->e()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    add-int/lit8 v5, v5, 0x2

    const/16 v6, 0x51

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 2601
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    .line 2602
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 2538
    :catch_0
    move-exception v0

    .line 2539
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    .line 2558
    :cond_6
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2560
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2561
    new-instance v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2562
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2563
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2564
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2565
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2566
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2567
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2568
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2569
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2571
    iput-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    goto/16 :goto_2

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method public a(Lcom/amap/api/maps/AMap$InfoWindowAdapter;)V
    .locals 0

    .prologue
    .line 1990
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    .line 1991
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnCameraChangeListener;)V
    .locals 0

    .prologue
    .line 1934
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    .line 1935
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;)V
    .locals 0

    .prologue
    .line 1984
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->L:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

    .line 1985
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapClickListener;)V
    .locals 0

    .prologue
    .line 1954
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->J:Lcom/amap/api/maps/AMap$OnMapClickListener;

    .line 1955
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapLoadedListener;)V
    .locals 0

    .prologue
    .line 1978
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->H:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

    .line 1979
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V
    .locals 0

    .prologue
    .line 1960
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    .line 1961
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V
    .locals 1

    .prologue
    .line 3224
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    .line 3225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 3226
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V
    .locals 0

    .prologue
    .line 1966
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->F:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    .line 1967
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V
    .locals 0

    .prologue
    .line 1972
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    .line 1973
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    .line 549
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)V
    .locals 1

    .prologue
    .line 3218
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    .line 3219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 3220
    return-void
.end method

.method public a(Lcom/amap/api/maps/CustomRenderer;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    .line 207
    return-void
.end method

.method public a(Lcom/amap/api/maps/LocationSource;)V
    .locals 2

    .prologue
    .line 1913
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    .line 1914
    if-eqz p1, :cond_0

    .line 1915
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->a(Z)V

    .line 1919
    :goto_0
    return-void

    .line 1917
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->a(Z)V

    goto :goto_0
.end method

.method a(Lcom/amap/api/maps/model/CameraPosition;)V
    .locals 2

    .prologue
    .line 1939
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1940
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1941
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1942
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1943
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/MyLocationStyle;)V
    .locals 1

    .prologue
    .line 588
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 589
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ak;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ak;->a(Lcom/amap/api/maps/model/MyLocationStyle;)V

    .line 591
    :cond_0
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    const/16 v2, 0x1f4

    const/4 v0, 0x0

    .line 231
    iget-boolean v1, p0, Lcom/amap/api/mapcore/b;->m:Z

    if-eqz v1, :cond_0

    .line 239
    :goto_0
    return-void

    .line 233
    :cond_0
    new-array v1, v2, [I

    .line 234
    invoke-interface {p1, v2, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 235
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 236
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    aget v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 238
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->m:Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 648
    if-eqz p1, :cond_0

    .line 649
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setVisibility(I)V

    .line 653
    :goto_0
    return-void

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setVisibility(I)V

    goto :goto_0
.end method

.method protected a(ZLcom/amap/api/maps/model/CameraPosition;)V
    .locals 1

    .prologue
    .line 3268
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    if-nez v0, :cond_1

    .line 3288
    :cond_0
    :goto_0
    return-void

    .line 3271
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3274
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3277
    if-nez p2, :cond_2

    .line 3279
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->n()Lcom/amap/api/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 3284
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    invoke-interface {v0, p2}, Lcom/amap/api/maps/AMap$OnCameraChangeListener;->onCameraChangeFinish(Lcom/amap/api/maps/model/CameraPosition;)V

    goto :goto_0

    .line 3280
    :catch_0
    move-exception v0

    .line 3281
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 862
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 863
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/p;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(F)F
    .locals 1

    .prologue
    .line 2000
    invoke-static {p1}, Lcom/amap/api/mapcore/util/v;->b(F)F

    move-result v0

    return v0
.end method

.method public b(I)F
    .locals 1

    .prologue
    .line 2005
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapLenWithWin(I)F

    move-result v0

    return v0
.end method

.method public b(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/mapcore/aj;
    .locals 2

    .prologue
    .line 1755
    if-nez p1, :cond_0

    .line 1756
    const/4 v0, 0x0

    .line 1762
    :goto_0
    return-object v0

    .line 1758
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/aj;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/aj;-><init>(Lcom/amap/api/maps/model/MarkerOptions;Lcom/amap/api/mapcore/ah;)V

    .line 1760
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ah;->a(Lcom/amap/api/mapcore/v;)V

    .line 1761
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method public b()Lcom/autonavi/amap/mapcore/MapCore;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    return-object v0
.end method

.method public b(DDLcom/autonavi/amap/mapcore/IPoint;)V
    .locals 5

    .prologue
    .line 2070
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2071
    new-instance v1, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2073
    invoke-static {p3, p4, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2074
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v0, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2075
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v1, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v1, v1, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v0, v2, v1, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Win(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2078
    return-void
.end method

.method public b(IILcom/autonavi/amap/mapcore/DPoint;)V
    .locals 0

    .prologue
    .line 2065
    invoke-static {p1, p2, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2066
    return-void
.end method

.method public b(IILcom/autonavi/amap/mapcore/FPoint;)V
    .locals 1

    .prologue
    .line 2053
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0, p2, p1, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2054
    return-void
.end method

.method public b(Lcom/amap/api/mapcore/i;)V
    .locals 1

    .prologue
    .line 1308
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V

    .line 1309
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 782
    if-eqz p1, :cond_0

    .line 783
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setVisibility(I)V

    .line 787
    :goto_0
    return-void

    .line 785
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setVisibility(I)V

    goto :goto_0
.end method

.method public b(Lcom/amap/api/mapcore/v;)Z
    .locals 2

    .prologue
    .line 2614
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2615
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2617
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1802
    const/4 v2, 0x0

    .line 1804
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v1, p1}, Lcom/amap/api/mapcore/ah;->a(Ljava/lang/String;)Lcom/amap/api/mapcore/v;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1808
    :goto_0
    if-eqz v1, :cond_0

    .line 1809
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1810
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ah;->b(Lcom/amap/api/mapcore/v;)Z

    move-result v0

    .line 1812
    :cond_0
    return v0

    .line 1805
    :catch_0
    move-exception v1

    .line 1806
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v1, v2

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/amap/api/mapcore/b;->i:I

    return v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 3239
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    if-eqz v0, :cond_0

    .line 3240
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/au;->a(I)V

    .line 3241
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/au;->invalidate()V

    .line 3242
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ap;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 3243
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ap;->invalidate()V

    .line 3246
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 791
    if-eqz p1, :cond_0

    .line 795
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    .line 796
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/k;->b()V

    .line 801
    :goto_0
    return-void

    .line 799
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    goto :goto_0
.end method

.method public d()Lcom/autonavi/amap/mapcore/MapProjection;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapCore;->getMapstate()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    return-object v0
.end method

.method public d(I)V
    .locals 3

    .prologue
    .line 3291
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3292
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3293
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    .line 3295
    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 820
    if-eqz p1, :cond_0

    .line 821
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ap;->setVisibility(I)V

    .line 822
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->h()V

    .line 828
    :goto_0
    return-void

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->a(Ljava/lang/String;)V

    .line 825
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ap;->a(I)V

    .line 826
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->setVisibility(I)V

    goto :goto_0
.end method

.method public e()Lcom/amap/api/mapcore/ak;
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    return-object v0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 3360
    iput p1, p0, Lcom/amap/api/mapcore/b;->af:I

    .line 3361
    return-void
.end method

.method public declared-synchronized e(Z)V
    .locals 4

    .prologue
    .line 894
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 895
    :try_start_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->as:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    if-nez v0, :cond_0

    .line 896
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    .line 897
    sget-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->av:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 907
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 902
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    .line 903
    sget-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->av:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 904
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->as:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 894
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 667
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Ljava/lang/Boolean;

    .line 672
    :try_start_0
    new-instance v0, Lcom/amap/api/mapcore/b$1;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/b$1;-><init>(Lcom/amap/api/mapcore/b;)V

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->queueEvent(Ljava/lang/Runnable;)V

    .line 678
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    const-wide/16 v0, 0x32

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 679
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->S()V

    .line 680
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 682
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    .line 684
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 685
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 686
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    .line 688
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 689
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 690
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    .line 692
    :cond_2
    sget-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 693
    sget-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->av:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 696
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    if-eqz v0, :cond_4

    .line 697
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/av;->a()V

    .line 700
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    if-eqz v0, :cond_5

    .line 701
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ap;->a()V

    .line 703
    :cond_5
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    if-eqz v0, :cond_6

    .line 704
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/au;->a()V

    .line 706
    :cond_6
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    if-eqz v0, :cond_7

    .line 707
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ad;->a()V

    .line 709
    :cond_7
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    if-eqz v0, :cond_8

    .line 710
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/k;->a()V

    .line 712
    :cond_8
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_9

    .line 713
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->b()V

    .line 715
    :cond_9
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    if-eqz v0, :cond_a

    .line 716
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->b()V

    .line 718
    :cond_a
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    if-eqz v0, :cond_b

    .line 719
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ah;->e()V

    .line 721
    :cond_b
    sget-object v0, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_c

    .line 722
    sget-object v0, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 724
    :cond_c
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    if-eqz v0, :cond_d

    .line 725
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 727
    :cond_d
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_e

    .line 728
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/a;->OnMapDestory(Lcom/autonavi/amap/mapcore/MapCore;)V

    .line 732
    :cond_e
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    if-eqz v0, :cond_f

    .line 733
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ai;->removeAllViews()V

    .line 735
    :cond_f
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V

    .line 737
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->a(Landroid/graphics/drawable/Drawable;)V

    .line 739
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_10

    .line 740
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 741
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 744
    :cond_10
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_11

    .line 745
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 746
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 770
    :cond_11
    const-string v0, "amap"

    const-string v1, "\u5b8c\u5168\u91ca\u653e"

    const/16 v2, 0x71

    invoke-static {v0, v1, v2}, Lcom/amap/api/mapcore/util/r;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 776
    :goto_0
    return-void

    .line 771
    :catch_0
    move-exception v0

    .line 772
    const-string v1, "amap"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u6ca1\u6709\u5b8c\u5168\u91ca\u653e"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x6f

    invoke-static {v1, v2, v3}, Lcom/amap/api/mapcore/util/r;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 774
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public f(Z)V
    .locals 3

    .prologue
    .line 1855
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ax:Z

    .line 1856
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    new-instance v1, Lcom/amap/api/mapcore/af;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/amap/api/mapcore/af;-><init>(I)V

    invoke-virtual {v1, p1}, Lcom/amap/api/mapcore/af;->a(Z)Lcom/amap/api/mapcore/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/af;)V

    .line 1858
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    .line 804
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 805
    return-void
.end method

.method public g(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1877
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    if-eqz v0, :cond_4

    .line 1878
    if-eqz p1, :cond_2

    .line 1879
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->W:Lcom/amap/api/mapcore/f;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/LocationSource;->activate(Lcom/amap/api/maps/LocationSource$OnLocationChangedListener;)V

    .line 1880
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->a(Z)V

    .line 1881
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    if-nez v0, :cond_0

    .line 1882
    new-instance v0, Lcom/amap/api/mapcore/ak;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ak;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    .line 1895
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    .line 1896
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Q:Lcom/amap/api/mapcore/ac;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/ac;->d(Z)V

    .line 1898
    :cond_1
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->T:Z

    .line 1899
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1900
    return-void

    .line 1885
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    if-eqz v0, :cond_3

    .line 1886
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ak;->a()V

    .line 1887
    iput-object v1, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    .line 1889
    :cond_3
    iput-object v1, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    .line 1890
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    invoke-interface {v0}, Lcom/amap/api/maps/LocationSource;->deactivate()V

    goto :goto_0

    .line 1893
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ad;->a(Z)V

    goto :goto_0
.end method

.method public h(Z)Lcom/amap/api/maps/model/CameraPosition;
    .locals 6

    .prologue
    .line 2096
    .line 2097
    if-eqz p1, :cond_0

    .line 2098
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2099
    iget v0, p0, Lcom/amap/api/mapcore/b;->aN:I

    iget v2, p0, Lcom/amap/api/mapcore/b;->aO:I

    invoke-virtual {p0, v0, v2, v1}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2100
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 2104
    :goto_0
    invoke-static {}, Lcom/amap/api/maps/model/CameraPosition;->builder()Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->target(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->bearing(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->tilt(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->build()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    return-object v0

    .line 2102
    :cond_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->U()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    goto :goto_0
.end method

.method h()V
    .locals 2

    .prologue
    .line 831
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 832
    return-void
.end method

.method i()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 835
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    if-nez v0, :cond_0

    .line 852
    :goto_0
    return-void

    .line 838
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    .line 839
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 840
    new-instance v2, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 841
    invoke-virtual {p0, v3, v3, v1}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 842
    invoke-virtual {p0, v0, v3, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 843
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v2, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v2, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/amap/api/mapcore/util/v;->a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)D

    move-result-wide v2

    .line 846
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v1

    float-to-int v1, v1

    .line 847
    iget-object v4, p0, Lcom/amap/api/mapcore/b;->al:[I

    aget v4, v4, v1

    mul-int/2addr v0, v4

    int-to-double v4, v0

    div-double v2, v4, v2

    double-to-int v0, v2

    .line 848
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->al:[I

    aget v1, v2, v1

    invoke-static {v1}, Lcom/amap/api/mapcore/util/v;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 849
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v2, v0}, Lcom/amap/api/mapcore/ap;->a(I)V

    .line 850
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->a(Ljava/lang/String;)V

    .line 851
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ap;->invalidate()V

    goto :goto_0
.end method

.method i(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3262
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x14

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 3264
    return-void

    :cond_0
    move v0, v1

    .line 3262
    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->S:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->S:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 1251
    iget v0, p0, Lcom/amap/api/mapcore/b;->j:I

    return v0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 1260
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    if-eqz v0, :cond_1

    .line 1261
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ai$a;

    .line 1263
    if-eqz v0, :cond_0

    .line 1264
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    invoke-interface {v1}, Lcom/amap/api/mapcore/v;->e()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    iput-object v1, v0, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 1266
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ai;->a()V

    .line 1267
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1269
    :cond_1
    return-void
.end method

.method public n()Lcom/amap/api/maps/model/CameraPosition;
    .locals 1

    .prologue
    .line 1278
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->h(Z)Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public o()F
    .locals 1

    .prologue
    .line 1283
    const/high16 v0, 0x41a00000    # 20.0f

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 949
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1008
    :cond_0
    :goto_0
    return-void

    .line 955
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    if-eqz v0, :cond_2

    .line 956
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v1

    invoke-static {v3, v3, v0, v1, p1}, Lcom/amap/api/mapcore/b;->a(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 957
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 958
    iput-boolean v3, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 960
    :cond_2
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->as:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 973
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p1, v5}, Lcom/amap/api/mapcore/ah;->a(Ljavax/microedition/khronos/opengles/GL10;Z)V

    goto :goto_0

    .line 986
    :cond_3
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-interface {p1, v4, v4, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 988
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 989
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->drawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 990
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 991
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    iget v1, p0, Lcom/amap/api/mapcore/b;->af:I

    invoke-virtual {v0, p1, v3, v1}, Lcom/amap/api/mapcore/p;->a(Ljavax/microedition/khronos/opengles/GL10;ZI)V

    .line 992
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ar;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 993
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p1, v3}, Lcom/amap/api/mapcore/ah;->a(Ljavax/microedition/khronos/opengles/GL10;Z)V

    .line 994
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Lcom/amap/api/mapcore/m;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/m;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 997
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 998
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1000
    :cond_4
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->U:Z

    if-nez v0, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1002
    iput-boolean v5, p0, Lcom/amap/api/mapcore/b;->U:Z

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 573
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->S()V

    .line 578
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/a;->onPause()V

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_1

    .line 582
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->d()V

    .line 584
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 553
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->R()V

    .line 554
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/a;->onResume()V

    .line 556
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_1

    .line 559
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->e()V

    .line 569
    :cond_1
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 12

    .prologue
    const/4 v10, 0x2

    const/16 v2, 0x64

    const/4 v4, 0x1

    const/16 v1, 0x803

    const/4 v5, 0x0

    .line 1032
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v5, v5, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->S:Landroid/graphics/Rect;

    .line 1033
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1034
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1, p2, p3}, Lcom/autonavi/amap/mapcore/MapCore;->surfaceChange(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1035
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1036
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->T()V

    .line 1038
    const/16 v3, 0x78

    if-gt v0, v3, :cond_2

    .line 1039
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x32

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1079
    :goto_0
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x3fd

    move v6, v5

    move v7, v5

    invoke-virtual/range {v2 .. v7}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1080
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v7, 0x3fe

    move v8, v5

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1081
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x3ff

    move v6, v5

    move v7, v5

    invoke-virtual/range {v2 .. v7}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1088
    invoke-virtual {p0, v5}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1090
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1091
    new-instance v0, Lcom/amap/api/mapcore/b$2;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/b$2;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    .line 1099
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1100
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    if-eqz v0, :cond_1

    .line 1101
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    invoke-interface {v0, p1, p2, p3}, Lcom/amap/api/maps/CustomRenderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1103
    :cond_1
    return-void

    .line 1040
    :cond_2
    const/16 v3, 0xa0

    if-gt v0, v3, :cond_5

    .line 1041
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_3

    .line 1042
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x50

    move v7, v1

    move v9, v2

    move v10, v4

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1043
    :cond_3
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v3, 0x1e0

    if-gt v0, v3, :cond_4

    .line 1044
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v2, 0x78

    const/16 v3, 0x3c

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1047
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x50

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1051
    :cond_5
    const/16 v3, 0xd7

    if-gt v0, v3, :cond_6

    .line 1052
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x50

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1053
    :cond_6
    const/16 v3, 0xf0

    if-gt v0, v3, :cond_8

    .line 1054
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_7

    .line 1055
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x3c

    move v7, v1

    move v9, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1057
    :cond_7
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v9, 0x5a

    move v7, v1

    move v8, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1060
    :cond_8
    const/16 v3, 0x140

    if-gt v0, v3, :cond_b

    .line 1061
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v3, 0x500

    if-gt v0, v3, :cond_9

    .line 1062
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x5a

    move v7, v1

    move v9, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1063
    :cond_9
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v2, 0x870

    if-lt v0, v2, :cond_a

    .line 1064
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x32

    const/16 v9, 0xaa

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1066
    :cond_a
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x46

    const/16 v9, 0x96

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1068
    :cond_b
    const/16 v2, 0x1e0

    if-gt v0, v2, :cond_c

    .line 1069
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x46

    const/16 v9, 0x96

    const/4 v10, 0x3

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1071
    :cond_c
    const/16 v2, 0x280

    if-ne v0, v2, :cond_d

    .line 1072
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x32

    const/16 v9, 0xb4

    const/4 v10, 0x3

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1074
    :cond_d
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x3c

    const/16 v9, 0xb4

    const/4 v10, 0x3

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 7

    .prologue
    const/16 v1, 0x3e9

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1190
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->setRenderMode(I)V

    .line 1191
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->R()V

    .line 1192
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1194
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v4, "style_v3.data"

    invoke-static {v3, v4}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setStyleData([BI)V

    .line 1196
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v4, "style_sv3.data"

    invoke-static {v3, v4}, Lcom/amap/api/mapcore/util/v;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Lcom/autonavi/amap/mapcore/MapCore;->setStyleData([BI)V

    .line 1199
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->surfaceCreate(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1200
    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    .line 1201
    if-eqz v0, :cond_0

    .line 1202
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1203
    const-string v3, "adreno"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-le v0, v3, :cond_7

    .line 1204
    iput-boolean v6, p0, Lcom/amap/api/mapcore/b;->V:Z

    .line 1205
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1212
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1213
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v1, "lineTexture.png"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/v;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    .line 1215
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1216
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v1, "lineDashTexture.png"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/util/v;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    .line 1222
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/amap/api/mapcore/util/v;->a(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/b;->i:I

    .line 1223
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    invoke-static {p1, v0, v6}, Lcom/amap/api/mapcore/util/v;->a(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;Z)I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/b;->j:I

    .line 1225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    .line 1234
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->T()V

    .line 1235
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1236
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->d:Z

    if-nez v0, :cond_5

    .line 1238
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    const-string v1, "AuthThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1239
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1240
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->d:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1245
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    if-eqz v0, :cond_6

    .line 1246
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    invoke-interface {v0, p1, p2}, Lcom/amap/api/maps/CustomRenderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1248
    :cond_6
    return-void

    .line 1207
    :cond_7
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->V:Z

    .line 1208
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1241
    :catch_0
    move-exception v0

    .line 1242
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2190
    iget-boolean v2, p0, Lcom/amap/api/mapcore/b;->aK:Z

    if-nez v2, :cond_0

    .line 2207
    :goto_0
    return v0

    .line 2194
    :cond_0
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 2195
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->u:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2196
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->v:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2197
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/util/c;

    invoke-virtual {v2, p1}, Lcom/amap/api/mapcore/util/c;->a(Landroid/view/MotionEvent;)Z

    .line 2198
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->X:Lcom/amap/api/mapcore/util/b;

    invoke-virtual {v2, p1}, Lcom/amap/api/mapcore/util/b;->a(Landroid/view/MotionEvent;)Z

    .line 2199
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2200
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2201
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b;->a(Landroid/view/MotionEvent;)V

    .line 2203
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_2

    .line 2204
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->V()V

    .line 2206
    :cond_2
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    move v0, v1

    .line 2207
    goto :goto_0
.end method

.method public p()F
    .locals 1

    .prologue
    .line 1288
    const/high16 v0, 0x40800000    # 4.0f

    return v0
.end method

.method public q()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1639
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1640
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/h;->a(Z)V

    .line 1641
    invoke-virtual {p0, v1, v2}, Lcom/amap/api/mapcore/b;->a(ZLcom/amap/api/maps/model/CameraPosition;)V

    .line 1642
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    if-eqz v0, :cond_0

    .line 1643
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-interface {v0}, Lcom/amap/api/maps/AMap$CancelableCallback;->onCancel()V

    .line 1645
    :cond_0
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1647
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1648
    return-void
.end method

.method public r()V
    .locals 4

    .prologue
    .line 1788
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V

    .line 1789
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->a()V

    .line 1790
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->b()V

    .line 1791
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ah;->c()V

    .line 1792
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1798
    :goto_0
    return-void

    .line 1793
    :catch_0
    move-exception v0

    .line 1794
    const-string v1, "amapApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AMapDelegateImpGLSurfaceView clear erro"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1796
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 1817
    iget v0, p0, Lcom/amap/api/mapcore/b;->p:I

    return v0
.end method

.method public setZOrderOnTop(Z)V
    .locals 0

    .prologue
    .line 1273
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->setZOrderOnTop(Z)V

    .line 1274
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 1845
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ax:Z

    return v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 1872
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->T:Z

    return v0
.end method

.method public v()Landroid/location/Location;
    .locals 1

    .prologue
    .line 1904
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    if-eqz v0, :cond_0

    .line 1905
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->W:Lcom/amap/api/mapcore/f;

    iget-object v0, v0, Lcom/amap/api/mapcore/f;->a:Landroid/location/Location;

    .line 1907
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Lcom/amap/api/mapcore/ac;
    .locals 1

    .prologue
    .line 1923
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Q:Lcom/amap/api/mapcore/ac;

    return-object v0
.end method

.method public x()Lcom/amap/api/mapcore/z;
    .locals 1

    .prologue
    .line 1928
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->P:Lcom/amap/api/mapcore/z;

    return-object v0
.end method

.method public y()Lcom/amap/api/maps/AMap$OnCameraChangeListener;
    .locals 1

    .prologue
    .line 1948
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    return-object v0
.end method

.method public z()Landroid/view/View;
    .locals 1

    .prologue
    .line 1995
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    return-object v0
.end method
