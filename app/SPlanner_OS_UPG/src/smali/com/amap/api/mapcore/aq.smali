.class public Lcom/amap/api/mapcore/aq;
.super Ljava/lang/Object;
.source "TileOverlayDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/ab;


# static fields
.field private static f:I


# instance fields
.field private a:Lcom/amap/api/mapcore/ar;

.field private b:Lcom/amap/api/maps/model/TileProvider;

.field private c:Ljava/lang/Float;

.field private d:Z

.field private e:Lcom/amap/api/mapcore/r;

.field private g:I

.field private h:I

.field private i:I

.field private j:Lcom/amap/api/mapcore/util/o;

.field private k:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private l:Z

.field private m:Lcom/amap/api/mapcore/aq$b;

.field private n:Ljava/lang/String;

.field private o:Ljava/nio/FloatBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput v0, Lcom/amap/api/mapcore/aq;->f:I

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/maps/model/TileOverlayOptions;Lcom/amap/api/mapcore/ar;)V
    .locals 6

    .prologue
    const/16 v0, 0x100

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput v0, p0, Lcom/amap/api/mapcore/aq;->g:I

    .line 35
    iput v0, p0, Lcom/amap/api/mapcore/aq;->h:I

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/mapcore/aq;->i:I

    .line 38
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 39
    iput-boolean v3, p0, Lcom/amap/api/mapcore/aq;->l:Z

    .line 40
    iput-object v1, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    .line 47
    iput-object v1, p0, Lcom/amap/api/mapcore/aq;->n:Ljava/lang/String;

    .line 162
    iput-object v1, p0, Lcom/amap/api/mapcore/aq;->o:Ljava/nio/FloatBuffer;

    .line 51
    iput-object p2, p0, Lcom/amap/api/mapcore/aq;->a:Lcom/amap/api/mapcore/ar;

    .line 52
    invoke-virtual {p1}, Lcom/amap/api/maps/model/TileOverlayOptions;->getTileProvider()Lcom/amap/api/maps/model/TileProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->b:Lcom/amap/api/maps/model/TileProvider;

    .line 53
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->b:Lcom/amap/api/maps/model/TileProvider;

    invoke-interface {v0}, Lcom/amap/api/maps/model/TileProvider;->getTileWidth()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/aq;->g:I

    .line 54
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->b:Lcom/amap/api/maps/model/TileProvider;

    invoke-interface {v0}, Lcom/amap/api/maps/model/TileProvider;->getTileHeight()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/aq;->h:I

    .line 55
    iget v0, p0, Lcom/amap/api/mapcore/aq;->g:I

    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->a(I)I

    move-result v0

    .line 56
    iget v1, p0, Lcom/amap/api/mapcore/aq;->h:I

    invoke-static {v1}, Lcom/amap/api/mapcore/util/v;->a(I)I

    move-result v1

    .line 57
    iget v2, p0, Lcom/amap/api/mapcore/aq;->g:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 58
    iget v2, p0, Lcom/amap/api/mapcore/aq;->h:I

    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 59
    iget-object v2, p0, Lcom/amap/api/mapcore/aq;->o:Ljava/nio/FloatBuffer;

    if-nez v2, :cond_0

    .line 60
    const/16 v2, 0x8

    new-array v2, v2, [F

    aput v4, v2, v3

    aput v1, v2, v5

    const/4 v3, 0x2

    aput v0, v2, v3

    const/4 v3, 0x3

    aput v1, v2, v3

    const/4 v1, 0x4

    aput v0, v2, v1

    const/4 v0, 0x5

    aput v4, v2, v0

    const/4 v0, 0x6

    aput v4, v2, v0

    const/4 v0, 0x7

    aput v4, v2, v0

    invoke-static {v2}, Lcom/amap/api/mapcore/util/v;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->o:Ljava/nio/FloatBuffer;

    .line 63
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/maps/model/TileOverlayOptions;->getZIndex()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->c:Ljava/lang/Float;

    .line 64
    invoke-virtual {p1}, Lcom/amap/api/maps/model/TileOverlayOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/aq;->d:Z

    .line 65
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aq;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->n:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->a:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    .line 68
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->n:Ljava/lang/String;

    const-string v1, "TileOverlay"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/aq;->i:I

    .line 70
    new-instance v0, Lcom/amap/api/mapcore/util/n$a;

    iget-object v1, p0, Lcom/amap/api/mapcore/aq;->a:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/ar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/mapcore/aq;->n:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/amap/api/mapcore/util/n$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Lcom/amap/api/maps/model/TileOverlayOptions;->getMemoryCacheEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/util/n$a;->a(Z)V

    .line 78
    invoke-virtual {p1}, Lcom/amap/api/maps/model/TileOverlayOptions;->getDiskCacheEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/util/n$a;->b(Z)V

    .line 79
    invoke-virtual {p1}, Lcom/amap/api/maps/model/TileOverlayOptions;->getMemCacheSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/util/n$a;->a(I)V

    .line 80
    invoke-virtual {p1}, Lcom/amap/api/maps/model/TileOverlayOptions;->getDiskCacheSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/util/n$a;->b(I)V

    .line 81
    invoke-virtual {p1}, Lcom/amap/api/maps/model/TileOverlayOptions;->getDiskCacheDir()Ljava/lang/String;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 83
    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/util/n$a;->a(Ljava/lang/String;)V

    .line 87
    :cond_1
    new-instance v1, Lcom/amap/api/mapcore/util/o;

    iget-object v2, p0, Lcom/amap/api/mapcore/aq;->a:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/ar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/amap/api/mapcore/aq;->g:I

    iget v4, p0, Lcom/amap/api/mapcore/aq;->h:I

    invoke-direct {v1, v2, v3, v4}, Lcom/amap/api/mapcore/util/o;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    .line 89
    iget-object v1, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    iget-object v2, p0, Lcom/amap/api/mapcore/aq;->b:Lcom/amap/api/maps/model/TileProvider;

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/util/o;->a(Lcom/amap/api/maps/model/TileProvider;)V

    .line 90
    iget-object v1, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/util/o;->a(Lcom/amap/api/mapcore/util/n$a;)V

    .line 92
    invoke-virtual {p0, v5}, Lcom/amap/api/mapcore/aq;->b(Z)V

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/aq;)Lcom/amap/api/mapcore/r;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    sget v0, Lcom/amap/api/mapcore/aq;->f:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/amap/api/mapcore/aq;->f:I

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/amap/api/mapcore/aq;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(III)Ljava/util/ArrayList;
    .locals 18

    .prologue
    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    .line 282
    const v3, 0x7fffffff

    .line 283
    const/4 v4, 0x0

    .line 284
    const v5, 0x7fffffff

    .line 285
    const/4 v6, 0x0

    .line 287
    new-instance v7, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v7}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 288
    new-instance v8, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 289
    new-instance v9, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v9}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 291
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11, v7}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 292
    iget v10, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v11, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v2, v10, v11, v8}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 293
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v3, v10}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 294
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v4, v10}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 295
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v5, v10}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 296
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 298
    const/4 v10, 0x0

    move/from16 v0, p2

    invoke-virtual {v2, v0, v10, v7}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 299
    iget v10, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v11, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v2, v10, v11, v8}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 300
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v3, v10}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 301
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v4, v10}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 302
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v5, v10}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 303
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 305
    const/4 v10, 0x0

    move/from16 v0, p3

    invoke-virtual {v2, v10, v0, v7}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 306
    iget v10, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v11, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v2, v10, v11, v8}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 307
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v3, v10}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 308
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v4, v10}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 309
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v5, v10}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 310
    iget v10, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 312
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v2, v0, v1, v7}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 313
    iget v10, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v7, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v2, v10, v7, v8}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 314
    iget v7, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 315
    iget v7, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 316
    iget v4, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 317
    iget v5, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 319
    const/4 v5, 0x1

    rsub-int/lit8 v6, p1, 0x14

    shl-int/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/amap/api/mapcore/aq;->g:I

    mul-int/2addr v5, v6

    sub-int v14, v3, v5

    .line 321
    const/4 v3, 0x1

    rsub-int/lit8 v5, p1, 0x14

    shl-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/amap/api/mapcore/aq;->h:I

    mul-int/2addr v3, v5

    sub-int v15, v4, v3

    .line 324
    invoke-virtual {v2, v9}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 325
    iget v2, v9, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    rsub-int/lit8 v3, p1, 0x14

    shr-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/aq;->g:I

    div-int v4, v2, v3

    .line 326
    iget v2, v9, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    rsub-int/lit8 v3, p1, 0x14

    shr-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/aq;->h:I

    div-int v5, v2, v3

    .line 327
    rsub-int/lit8 v2, p1, 0x14

    shl-int v2, v4, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/aq;->g:I

    mul-int v8, v2, v3

    .line 328
    rsub-int/lit8 v2, p1, 0x14

    shl-int v2, v5, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/aq;->h:I

    mul-int v9, v2, v3

    .line 329
    new-instance v2, Lcom/amap/api/mapcore/aq$a;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/amap/api/mapcore/aq;->i:I

    move-object/from16 v3, p0

    move/from16 v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/amap/api/mapcore/aq$a;-><init>(Lcom/amap/api/mapcore/aq;IIII)V

    .line 331
    new-instance v3, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v3, v8, v9}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    iput-object v3, v2, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    .line 332
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/amap/api/mapcore/aq;->a(Lcom/amap/api/mapcore/aq$a;)Z

    .line 334
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 335
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    const/4 v2, 0x1

    move v3, v2

    .line 345
    :goto_0
    const/4 v2, 0x0

    .line 346
    sub-int v8, v4, v3

    :goto_1
    add-int v6, v4, v3

    if-gt v8, v6, :cond_4

    .line 348
    add-int v9, v5, v3

    .line 350
    new-instance v17, Lcom/autonavi/amap/mapcore/IPoint;

    rsub-int/lit8 v6, p1, 0x14

    shl-int v6, v8, v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/amap/api/mapcore/aq;->g:I

    mul-int/2addr v6, v7

    rsub-int/lit8 v7, p1, 0x14

    shl-int v7, v9, v7

    move-object/from16 v0, p0

    iget v10, v0, Lcom/amap/api/mapcore/aq;->h:I

    mul-int/2addr v7, v10

    move-object/from16 v0, v17

    invoke-direct {v0, v6, v7}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    .line 354
    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-ge v6, v12, :cond_1

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-le v6, v14, :cond_1

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-ge v6, v13, :cond_1

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-le v6, v15, :cond_1

    .line 356
    if-nez v2, :cond_0

    .line 357
    const/4 v2, 0x1

    .line 359
    :cond_0
    new-instance v6, Lcom/amap/api/mapcore/aq$a;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/amap/api/mapcore/aq;->i:I

    move-object/from16 v7, p0

    move/from16 v10, p1

    invoke-direct/range {v6 .. v11}, Lcom/amap/api/mapcore/aq$a;-><init>(Lcom/amap/api/mapcore/aq;IIII)V

    .line 361
    move-object/from16 v0, v17

    iput-object v0, v6, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    .line 362
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/amap/api/mapcore/aq;->a(Lcom/amap/api/mapcore/aq$a;)Z

    .line 363
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    :cond_1
    sub-int v9, v5, v3

    .line 368
    new-instance v17, Lcom/autonavi/amap/mapcore/IPoint;

    rsub-int/lit8 v6, p1, 0x14

    shl-int v6, v8, v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/amap/api/mapcore/aq;->g:I

    mul-int/2addr v6, v7

    rsub-int/lit8 v7, p1, 0x14

    shl-int v7, v9, v7

    move-object/from16 v0, p0

    iget v10, v0, Lcom/amap/api/mapcore/aq;->h:I

    mul-int/2addr v7, v10

    move-object/from16 v0, v17

    invoke-direct {v0, v6, v7}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    .line 372
    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-ge v6, v12, :cond_3

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-le v6, v14, :cond_3

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-ge v6, v13, :cond_3

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-le v6, v15, :cond_3

    .line 374
    if-nez v2, :cond_2

    .line 375
    const/4 v2, 0x1

    .line 377
    :cond_2
    new-instance v6, Lcom/amap/api/mapcore/aq$a;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/amap/api/mapcore/aq;->i:I

    move-object/from16 v7, p0

    move/from16 v10, p1

    invoke-direct/range {v6 .. v11}, Lcom/amap/api/mapcore/aq$a;-><init>(Lcom/amap/api/mapcore/aq;IIII)V

    .line 379
    move-object/from16 v0, v17

    iput-object v0, v6, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    .line 380
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/amap/api/mapcore/aq;->a(Lcom/amap/api/mapcore/aq$a;)Z

    .line 381
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 385
    :cond_4
    add-int v6, v5, v3

    add-int/lit8 v9, v6, -0x1

    :goto_2
    sub-int v6, v5, v3

    if-le v9, v6, :cond_9

    .line 386
    add-int v8, v4, v3

    .line 389
    new-instance v17, Lcom/autonavi/amap/mapcore/IPoint;

    rsub-int/lit8 v6, p1, 0x14

    shl-int v6, v8, v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/amap/api/mapcore/aq;->g:I

    mul-int/2addr v6, v7

    rsub-int/lit8 v7, p1, 0x14

    shl-int v7, v9, v7

    move-object/from16 v0, p0

    iget v10, v0, Lcom/amap/api/mapcore/aq;->h:I

    mul-int/2addr v7, v10

    move-object/from16 v0, v17

    invoke-direct {v0, v6, v7}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    .line 393
    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-ge v6, v12, :cond_6

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-le v6, v14, :cond_6

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-ge v6, v13, :cond_6

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-le v6, v15, :cond_6

    .line 395
    if-nez v2, :cond_5

    .line 396
    const/4 v2, 0x1

    .line 398
    :cond_5
    new-instance v6, Lcom/amap/api/mapcore/aq$a;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/amap/api/mapcore/aq;->i:I

    move-object/from16 v7, p0

    move/from16 v10, p1

    invoke-direct/range {v6 .. v11}, Lcom/amap/api/mapcore/aq$a;-><init>(Lcom/amap/api/mapcore/aq;IIII)V

    .line 400
    move-object/from16 v0, v17

    iput-object v0, v6, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    .line 401
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/amap/api/mapcore/aq;->a(Lcom/amap/api/mapcore/aq$a;)Z

    .line 402
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    :cond_6
    sub-int v8, v4, v3

    .line 407
    new-instance v17, Lcom/autonavi/amap/mapcore/IPoint;

    rsub-int/lit8 v6, p1, 0x14

    shl-int v6, v8, v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/amap/api/mapcore/aq;->g:I

    mul-int/2addr v6, v7

    rsub-int/lit8 v7, p1, 0x14

    shl-int v7, v9, v7

    move-object/from16 v0, p0

    iget v10, v0, Lcom/amap/api/mapcore/aq;->h:I

    mul-int/2addr v7, v10

    move-object/from16 v0, v17

    invoke-direct {v0, v6, v7}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    .line 411
    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-ge v6, v12, :cond_8

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-le v6, v14, :cond_8

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-ge v6, v13, :cond_8

    move-object/from16 v0, v17

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-le v6, v15, :cond_8

    .line 413
    if-nez v2, :cond_7

    .line 414
    const/4 v2, 0x1

    .line 416
    :cond_7
    new-instance v6, Lcom/amap/api/mapcore/aq$a;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/amap/api/mapcore/aq;->i:I

    move-object/from16 v7, p0

    move/from16 v10, p1

    invoke-direct/range {v6 .. v11}, Lcom/amap/api/mapcore/aq$a;-><init>(Lcom/amap/api/mapcore/aq;IIII)V

    .line 418
    move-object/from16 v0, v17

    iput-object v0, v6, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    .line 419
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/amap/api/mapcore/aq;->a(Lcom/amap/api/mapcore/aq$a;)Z

    .line 420
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    :cond_8
    add-int/lit8 v9, v9, -0x1

    goto/16 :goto_2

    .line 424
    :cond_9
    if-nez v2, :cond_a

    .line 429
    return-object v16

    .line 344
    :cond_a
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/amap/api/mapcore/aq;III)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/amap/api/mapcore/aq;->a(III)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;ILjava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V
    .locals 7

    .prologue
    const v6, 0x8074

    const/16 v5, 0x1406

    const/16 v4, 0xde1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 222
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    const/16 v0, 0xbe2

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 226
    const/16 v0, 0x302

    const/16 v1, 0x303

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 227
    invoke-interface {p1, v2, v2, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 229
    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 230
    invoke-interface {p1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 231
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 232
    invoke-interface {p1, v4, p2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 234
    const/4 v0, 0x3

    invoke-interface {p1, v0, v5, v3, p3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 235
    const/4 v0, 0x2

    invoke-interface {p1, v0, v5, v3, p4}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 236
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-interface {p1, v0, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 238
    invoke-interface {p1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 239
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 240
    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_0
.end method

.method private a(Lcom/amap/api/mapcore/aq$a;)Z
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 170
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    .line 171
    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v1

    .line 172
    iget v2, p0, Lcom/amap/api/mapcore/aq;->g:I

    iget v3, p0, Lcom/amap/api/mapcore/aq;->h:I

    .line 174
    iget-object v4, p1, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget-object v5, p1, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    iget v5, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    float-to-int v6, v1

    rsub-int/lit8 v6, v6, 0x14

    shl-int v6, v11, v6

    mul-int/2addr v6, v3

    add-int/2addr v5, v6

    .line 177
    const/16 v6, 0xc

    new-array v6, v6, [F

    .line 179
    new-instance v7, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v7}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 180
    invoke-virtual {v0, v4, v5, v7}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 183
    new-instance v8, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 184
    float-to-int v9, v1

    rsub-int/lit8 v9, v9, 0x14

    shl-int v9, v11, v9

    mul-int/2addr v9, v2

    add-int/2addr v9, v4

    invoke-virtual {v0, v9, v5, v8}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 187
    new-instance v9, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v9}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 188
    float-to-int v10, v1

    rsub-int/lit8 v10, v10, 0x14

    shl-int v10, v11, v10

    mul-int/2addr v2, v10

    add-int/2addr v2, v4

    float-to-int v10, v1

    rsub-int/lit8 v10, v10, 0x14

    shl-int v10, v11, v10

    mul-int/2addr v10, v3

    sub-int v10, v5, v10

    invoke-virtual {v0, v2, v10, v9}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 192
    new-instance v2, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 193
    float-to-int v1, v1

    rsub-int/lit8 v1, v1, 0x14

    shl-int v1, v11, v1

    mul-int/2addr v1, v3

    sub-int v1, v5, v1

    invoke-virtual {v0, v4, v1, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 195
    const/4 v0, 0x0

    iget v1, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v1, v6, v0

    .line 196
    iget v0, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v0, v6, v11

    .line 197
    const/4 v0, 0x2

    aput v12, v6, v0

    .line 199
    const/4 v0, 0x3

    iget v1, v8, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v1, v6, v0

    .line 200
    const/4 v0, 0x4

    iget v1, v8, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v1, v6, v0

    .line 201
    const/4 v0, 0x5

    aput v12, v6, v0

    .line 203
    const/4 v0, 0x6

    iget v1, v9, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v1, v6, v0

    .line 204
    const/4 v0, 0x7

    iget v1, v9, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v1, v6, v0

    .line 205
    const/16 v0, 0x8

    aput v12, v6, v0

    .line 207
    const/16 v0, 0x9

    iget v1, v2, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v1, v6, v0

    .line 208
    const/16 v0, 0xa

    iget v1, v2, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v1, v6, v0

    .line 209
    const/16 v0, 0xb

    aput v12, v6, v0

    .line 210
    iget-object v0, p1, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    if-nez v0, :cond_0

    .line 211
    invoke-static {v6}, Lcom/amap/api/mapcore/util/v;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p1, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    .line 217
    :goto_0
    return v11

    .line 213
    :cond_0
    iget-object v0, p1, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    invoke-static {v6, v0}, Lcom/amap/api/mapcore/util/v;->a([FLjava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p1, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    goto :goto_0
.end method

.method static synthetic a(Lcom/amap/api/mapcore/aq;Ljava/util/List;IZ)Z
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/amap/api/mapcore/aq;->a(Ljava/util/List;IZ)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;IZ)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 434
    if-nez p1, :cond_0

    move v0, v2

    .line 476
    :goto_0
    return v0

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_1

    move v0, v2

    .line 438
    goto :goto_0

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/aq$a;

    .line 442
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amap/api/mapcore/aq$a;

    .line 443
    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/aq$a;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-boolean v6, v0, Lcom/amap/api/mapcore/aq$a;->g:Z

    if-eqz v6, :cond_3

    .line 444
    iget-boolean v5, v0, Lcom/amap/api/mapcore/aq$a;->g:Z

    iput-boolean v5, v1, Lcom/amap/api/mapcore/aq$a;->g:Z

    .line 445
    iget v5, v0, Lcom/amap/api/mapcore/aq$a;->f:I

    iput v5, v1, Lcom/amap/api/mapcore/aq$a;->f:I

    move v1, v3

    .line 450
    :goto_2
    if-nez v1, :cond_2

    .line 451
    invoke-virtual {v0}, Lcom/amap/api/mapcore/aq$a;->b()V

    goto :goto_1

    .line 454
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 456
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->o()F

    move-result v0

    float-to-int v0, v0

    if-gt p2, v0, :cond_5

    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->p()F

    move-result v0

    float-to-int v0, v0

    if-ge p2, v0, :cond_6

    :cond_5
    move v0, v2

    .line 458
    goto :goto_0

    .line 460
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 461
    if-gtz v1, :cond_7

    move v0, v2

    .line 462
    goto :goto_0

    .line 466
    :cond_7
    :goto_3
    if-ge v2, v1, :cond_a

    .line 467
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/aq$a;

    .line 468
    if-nez v0, :cond_9

    .line 466
    :cond_8
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 471
    :cond_9
    iget-object v4, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    iget-boolean v4, v0, Lcom/amap/api/mapcore/aq$a;->g:Z

    if-nez v4, :cond_8

    .line 473
    iget-object v4, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    invoke-virtual {v4, p3, v0}, Lcom/amap/api/mapcore/util/o;->a(ZLcom/amap/api/mapcore/aq$a;)V

    goto :goto_4

    :cond_a
    move v0, v3

    .line 476
    goto :goto_0

    :cond_b
    move v1, v2

    goto :goto_2
.end method

.method static synthetic b(Lcom/amap/api/mapcore/aq;)Lcom/amap/api/mapcore/util/o;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    return-object v0
.end method

.method static synthetic c(Lcom/amap/api/mapcore/aq;)Lcom/amap/api/mapcore/ar;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->a:Lcom/amap/api/mapcore/ar;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/aq$b;->a()Lcom/amap/api/mapcore/util/f$d;

    move-result-object v0

    sget-object v1, Lcom/amap/api/mapcore/util/f$d;->b:Lcom/amap/api/mapcore/util/f$d;

    if-ne v0, v1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/aq$b;->a(Z)Z

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/aq$a;

    .line 102
    invoke-virtual {v0}, Lcom/amap/api/mapcore/aq$a;->b()V

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 105
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/util/o;->h()V

    .line 106
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->a:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/ar;->b(Lcom/amap/api/mapcore/ab;)Z

    .line 107
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 108
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 125
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->c:Ljava/lang/Float;

    .line 126
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->a:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->c()V

    .line 127
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    .prologue
    .line 245
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 272
    :cond_0
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/aq$a;

    .line 249
    iget-boolean v1, v0, Lcom/amap/api/mapcore/aq$a;->g:Z

    if-nez v1, :cond_4

    .line 251
    :try_start_0
    iget-object v1, v0, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    .line 252
    iget-object v3, v0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v1, :cond_4

    .line 254
    iget-object v1, v0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    invoke-static {p1, v1}, Lcom/amap/api/mapcore/util/v;->a(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;)I

    move-result v1

    iput v1, v0, Lcom/amap/api/mapcore/aq$a;->f:I

    .line 256
    iget v1, v0, Lcom/amap/api/mapcore/aq$a;->f:I

    if-eqz v1, :cond_3

    .line 257
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/amap/api/mapcore/aq$a;->g:Z

    .line 260
    :cond_3
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :cond_4
    :goto_1
    iget-boolean v1, v0, Lcom/amap/api/mapcore/aq$a;->g:Z

    if-eqz v1, :cond_2

    .line 268
    iget v1, v0, Lcom/amap/api/mapcore/aq$a;->f:I

    iget-object v0, v0, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    iget-object v3, p0, Lcom/amap/api/mapcore/aq;->o:Ljava/nio/FloatBuffer;

    invoke-direct {p0, p1, v1, v0, v3}, Lcom/amap/api/mapcore/aq;->a(Ljavax/microedition/khronos/opengles/GL10;ILjava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V

    goto :goto_0

    .line 262
    :catch_0
    move-exception v1

    .line 263
    const-string v3, "TileOverlayDelegateImp"

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x70

    invoke-static {v3, v1, v4}, Lcom/amap/api/mapcore/util/r;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/amap/api/mapcore/aq;->d:Z

    .line 137
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 138
    if-eqz p1, :cond_0

    .line 139
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/aq;->b(Z)V

    .line 141
    :cond_0
    return-void
.end method

.method public a(Lcom/amap/api/mapcore/ab;)Z
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/ab;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/aq;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    :cond_0
    const/4 v0, 0x1

    .line 154
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/util/o;->f()V

    .line 113
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 481
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aq;->l:Z

    if-eqz v0, :cond_0

    .line 490
    :goto_0
    return-void

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/aq$b;->a()Lcom/amap/api/mapcore/util/f$d;

    move-result-object v0

    sget-object v1, Lcom/amap/api/mapcore/util/f$d;->b:Lcom/amap/api/mapcore/util/f$d;

    if-ne v0, v1, :cond_1

    .line 486
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/aq$b;->a(Z)Z

    .line 488
    :cond_1
    new-instance v0, Lcom/amap/api/mapcore/aq$b;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/mapcore/aq$b;-><init>(Lcom/amap/api/mapcore/aq;Z)V

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    .line 489
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->m:Lcom/amap/api/mapcore/aq$b;

    new-array v1, v2, [Lcom/amap/api/mapcore/r;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/amap/api/mapcore/aq;->e:Lcom/amap/api/mapcore/r;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/aq$b;->c([Ljava/lang/Object;)Lcom/amap/api/mapcore/util/f;

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 118
    const-string v0, "TileOverlay"

    invoke-static {v0}, Lcom/amap/api/mapcore/aq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aq;->n:Ljava/lang/String;

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->n:Ljava/lang/String;

    return-object v0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 506
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aq;->l:Z

    if-eq v0, p1, :cond_0

    .line 507
    iput-boolean p1, p0, Lcom/amap/api/mapcore/aq;->l:Z

    .line 508
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/util/o;->b(Z)V

    .line 510
    :cond_0
    return-void
.end method

.method public d()F
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->c:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aq;->d:Z

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 159
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/util/o;->b(Z)V

    .line 495
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/util/o;->a(Z)V

    .line 496
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/util/o;->g()V

    .line 497
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Lcom/amap/api/mapcore/aq;->j:Lcom/amap/api/mapcore/util/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/util/o;->a(Z)V

    .line 502
    return-void
.end method
