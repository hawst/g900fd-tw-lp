.class Lcom/amap/api/mapcore/ai;
.super Landroid/view/ViewGroup;
.source "MapOverlayViewGroup.java"


# instance fields
.field private a:Lcom/amap/api/mapcore/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p2, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/r;

    .line 22
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/ai;->setBackgroundColor(I)V

    .line 23
    return-void
.end method

.method private a(Landroid/view/View;IIIII)V
    .locals 3

    .prologue
    .line 172
    .line 174
    and-int/lit8 v0, p6, 0x7

    .line 175
    and-int/lit8 v1, p6, 0x70

    .line 177
    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    .line 178
    sub-int/2addr p4, p2

    .line 183
    :cond_0
    :goto_0
    const/16 v0, 0x50

    if-ne v1, v0, :cond_3

    .line 184
    sub-int/2addr p5, p3

    .line 188
    :cond_1
    :goto_1
    add-int v0, p4, p2

    add-int v1, p5, p3

    invoke-virtual {p1, p4, p5, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 189
    return-void

    .line 179
    :cond_2
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 180
    div-int/lit8 v0, p2, 0x2

    sub-int/2addr p4, v0

    goto :goto_0

    .line 185
    :cond_3
    const/16 v0, 0x10

    if-ne v1, v0, :cond_1

    .line 186
    div-int/lit8 v0, p3, 0x2

    sub-int/2addr p5, v0

    goto :goto_1
.end method

.method private a(Landroid/view/View;II[I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 142
    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 144
    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    aput v1, p4, v2

    .line 146
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    aput v0, p4, v3

    .line 149
    :cond_0
    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    .line 150
    :cond_1
    invoke-virtual {p1, v2, v2}, Landroid/view/View;->measure(II)V

    .line 152
    :cond_2
    if-ne p2, v4, :cond_3

    .line 153
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    aput v0, p4, v2

    .line 160
    :goto_0
    if-ne p3, v4, :cond_5

    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    aput v0, p4, v3

    .line 168
    :goto_1
    return-void

    .line 154
    :cond_3
    if-ne p2, v5, :cond_4

    .line 155
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ai;->getMeasuredWidth()I

    move-result v0

    aput v0, p4, v2

    goto :goto_0

    .line 157
    :cond_4
    aput p2, p4, v2

    goto :goto_0

    .line 162
    :cond_5
    if-ne p3, v5, :cond_6

    .line 163
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ai;->getMeasuredHeight()I

    move-result v0

    aput v0, p4, v3

    goto :goto_1

    .line 165
    :cond_6
    aput p3, p4, v3

    goto :goto_1
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 110
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 111
    iget v1, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;II[I)V

    .line 112
    aget v2, v0, v4

    const/4 v1, 0x1

    aget v3, v0, v1

    const/16 v6, 0x33

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;IIIII)V

    .line 114
    return-void
.end method

.method private a(Landroid/view/View;Lcom/amap/api/mapcore/ai$a;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 118
    iget v1, p2, Lcom/amap/api/mapcore/ai$a;->width:I

    iget v2, p2, Lcom/amap/api/mapcore/ai$a;->height:I

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;II[I)V

    .line 119
    instance-of v1, p1, Lcom/amap/api/mapcore/av;

    if-eqz v1, :cond_1

    .line 120
    aget v2, v0, v4

    aget v3, v0, v11

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ai;->getWidth()I

    move-result v1

    aget v0, v0, v4

    sub-int v4, v1, v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ai;->getHeight()I

    move-result v5

    iget v6, p2, Lcom/amap/api/mapcore/ai$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;IIIII)V

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    instance-of v1, p1, Lcom/amap/api/mapcore/ad;

    if-eqz v1, :cond_2

    .line 124
    aget v2, v0, v4

    aget v3, v0, v11

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ai;->getWidth()I

    move-result v1

    aget v4, v0, v4

    sub-int v4, v1, v4

    aget v5, v0, v11

    iget v6, p2, Lcom/amap/api/mapcore/ai$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;IIIII)V

    goto :goto_0

    .line 127
    :cond_2
    instance-of v1, p1, Lcom/amap/api/mapcore/k;

    if-eqz v1, :cond_3

    .line 128
    aget v2, v0, v4

    aget v3, v0, v11

    iget v6, p2, Lcom/amap/api/mapcore/ai$a;->e:I

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;IIIII)V

    goto :goto_0

    .line 130
    :cond_3
    iget-object v1, p2, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    if-eqz v1, :cond_0

    .line 131
    new-instance v10, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v10}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 132
    iget-object v5, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/r;

    iget-object v1, p2, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v1, p2, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v5 .. v10}, Lcom/amap/api/mapcore/r;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 134
    iget v1, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v2, p2, Lcom/amap/api/mapcore/ai$a;->c:I

    add-int/2addr v1, v2

    iput v1, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 135
    iget v1, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v2, p2, Lcom/amap/api/mapcore/ai$a;->d:I

    add-int/2addr v1, v2

    iput v1, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 137
    aget v2, v0, v4

    aget v3, v0, v11

    iget v4, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v6, p2, Lcom/amap/api/mapcore/ai$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;IIIII)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 106
    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/mapcore/ai;->onLayout(ZIIII)V

    .line 107
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ai;->getChildCount()I

    move-result v2

    .line 80
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 81
    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/ai;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 82
    if-nez v3, :cond_0

    .line 80
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lcom/amap/api/mapcore/ai$a;

    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ai$a;

    .line 88
    invoke-direct {p0, v3, v0}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;Lcom/amap/api/mapcore/ai$a;)V

    goto :goto_1

    .line 99
    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/amap/api/mapcore/ai;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 103
    :cond_2
    return-void
.end method
