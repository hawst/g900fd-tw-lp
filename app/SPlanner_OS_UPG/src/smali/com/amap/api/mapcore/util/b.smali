.class public Lcom/amap/api/mapcore/util/b;
.super Ljava/lang/Object;
.source "MultiTouchSupport.java"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected b:Ljava/lang/reflect/Method;

.field protected c:Ljava/lang/reflect/Method;

.field protected d:Ljava/lang/reflect/Method;

.field protected e:Ljava/lang/reflect/Method;

.field private f:Z

.field private final g:Lcom/amap/api/mapcore/util/b$a;

.field private h:J

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/mapcore/util/b$a;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v2, p0, Lcom/amap/api/mapcore/util/b;->f:Z

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/mapcore/util/b;->h:J

    .line 66
    iput-boolean v2, p0, Lcom/amap/api/mapcore/util/b;->i:Z

    .line 35
    iput-object p1, p0, Lcom/amap/api/mapcore/util/b;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/amap/api/mapcore/util/b;->g:Lcom/amap/api/mapcore/util/b$a;

    .line 37
    invoke-direct {p0}, Lcom/amap/api/mapcore/util/b;->d()V

    .line 38
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 54
    :try_start_0
    const-class v0, Landroid/view/MotionEvent;

    const-string v1, "getPointerCount"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/util/b;->b:Ljava/lang/reflect/Method;

    .line 55
    const-class v0, Landroid/view/MotionEvent;

    const-string v1, "getPointerId"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/util/b;->e:Ljava/lang/reflect/Method;

    .line 57
    const-class v0, Landroid/view/MotionEvent;

    const-string v1, "getX"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/util/b;->c:Ljava/lang/reflect/Method;

    .line 58
    const-class v0, Landroid/view/MotionEvent;

    const-string v1, "getY"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/util/b;->d:Ljava/lang/reflect/Method;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/util/b;->f:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    iput-boolean v5, p0, Lcom/amap/api/mapcore/util/b;->f:Z

    .line 62
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/amap/api/mapcore/util/b;->f:Z

    return v0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 69
    invoke-virtual {p0}, Lcom/amap/api/mapcore/util/b;->a()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v8

    .line 100
    :goto_0
    return v2

    .line 72
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v10, v2, 0xff

    .line 74
    :try_start_0
    iget-object v2, p0, Lcom/amap/api/mapcore/util/b;->b:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 75
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v13, :cond_1

    move v2, v8

    .line 76
    goto :goto_0

    .line 78
    :cond_1
    iget-object v2, p0, Lcom/amap/api/mapcore/util/b;->c:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/Float;

    move-object v4, v0

    .line 79
    iget-object v2, p0, Lcom/amap/api/mapcore/util/b;->c:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/Float;

    move-object v6, v0

    .line 80
    iget-object v2, p0, Lcom/amap/api/mapcore/util/b;->d:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v5

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/Float;

    move-object v5, v0

    .line 81
    iget-object v2, p0, Lcom/amap/api/mapcore/util/b;->d:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v3, v7

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/Float;

    move-object v7, v0

    .line 82
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v11

    sub-float/2addr v3, v11

    mul-float/2addr v2, v3

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v11

    sub-float/2addr v3, v11

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v11

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v12

    sub-float/2addr v11, v12

    mul-float/2addr v3, v11

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v3, v2

    .line 84
    const/4 v2, 0x5

    if-ne v10, v2, :cond_2

    .line 85
    iget-object v2, p0, Lcom/amap/api/mapcore/util/b;->g:Lcom/amap/api/mapcore/util/b$a;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-interface/range {v2 .. v7}, Lcom/amap/api/mapcore/util/b$a;->a(FFFFF)V

    .line 86
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/amap/api/mapcore/util/b;->i:Z

    move v2, v9

    .line 87
    goto/16 :goto_0

    .line 88
    :cond_2
    const/4 v2, 0x6

    if-ne v10, v2, :cond_4

    .line 89
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/amap/api/mapcore/util/b;->h:J

    .line 90
    iget-boolean v2, p0, Lcom/amap/api/mapcore/util/b;->i:Z

    if-eqz v2, :cond_3

    .line 91
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/amap/api/mapcore/util/b;->i:Z

    :cond_3
    move v2, v8

    .line 93
    goto/16 :goto_0

    .line 94
    :cond_4
    iget-boolean v2, p0, Lcom/amap/api/mapcore/util/b;->i:Z

    if-eqz v2, :cond_5

    if-ne v10, v13, :cond_5

    .line 95
    iget-object v2, p0, Lcom/amap/api/mapcore/util/b;->g:Lcom/amap/api/mapcore/util/b$a;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    move-object v3, p1

    invoke-interface/range {v2 .. v7}, Lcom/amap/api/mapcore/util/b$a;->a(Landroid/view/MotionEvent;FFFF)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto/16 :goto_0

    .line 97
    :catch_0
    move-exception v2

    .line 98
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_5
    move v2, v8

    .line 100
    goto/16 :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/amap/api/mapcore/util/b;->i:Z

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/amap/api/mapcore/util/b;->h:J

    return-wide v0
.end method
