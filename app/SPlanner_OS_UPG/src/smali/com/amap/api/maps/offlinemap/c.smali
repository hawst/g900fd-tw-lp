.class Lcom/amap/api/maps/offlinemap/c;
.super Ljava/lang/Thread;
.source "FileSplitterFetch.java"


# instance fields
.field a:Ljava/lang/String;

.field b:J

.field c:J

.field d:I

.field e:Z

.field f:Z

.field g:Lcom/amap/api/maps/offlinemap/b;

.field h:Ljava/net/HttpURLConnection;

.field i:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JJI)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 12
    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->e:Z

    .line 13
    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->f:Z

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->g:Lcom/amap/api/maps/offlinemap/b;

    .line 20
    iput-object p1, p0, Lcom/amap/api/maps/offlinemap/c;->a:Ljava/lang/String;

    .line 21
    iput-wide p3, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    .line 22
    iput-wide p5, p0, Lcom/amap/api/maps/offlinemap/c;->c:J

    .line 23
    iput p7, p0, Lcom/amap/api/maps/offlinemap/c;->d:I

    .line 24
    new-instance v0, Lcom/amap/api/maps/offlinemap/b;

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    invoke-direct {v0, p2, v2, v3}, Lcom/amap/api/maps/offlinemap/b;-><init>(Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->g:Lcom/amap/api/maps/offlinemap/b;

    .line 25
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->f:Z

    .line 80
    invoke-virtual {p0}, Lcom/amap/api/maps/offlinemap/c;->interrupt()V

    .line 81
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 82
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 88
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    .line 84
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 85
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 28
    :cond_0
    :goto_0
    iget-wide v0, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_6

    iget-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->f:Z

    if-nez v0, :cond_6

    .line 30
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 31
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    .line 32
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    const-string v1, "User-Agent"

    sget-object v2, Lcom/amap/api/mapcore/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    const-string v1, "Content-Type"

    const-string v2, "text/xml;"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    const-string v2, "RANGE"

    invoke-virtual {v1, v2, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/n;->a(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    .line 39
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 42
    :goto_1
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    const/4 v2, 0x0

    const/16 v3, 0x400

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    if-lez v1, :cond_2

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/c;->c:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    iget-boolean v2, p0, Lcom/amap/api/maps/offlinemap/c;->f:Z

    if-nez v2, :cond_2

    .line 43
    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    iget-object v4, p0, Lcom/amap/api/maps/offlinemap/c;->g:Lcom/amap/api/maps/offlinemap/b;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5, v1}, Lcom/amap/api/maps/offlinemap/b;->a([BII)I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->b:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 47
    :catch_0
    move-exception v0

    .line 48
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 52
    :try_start_2
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 56
    :goto_2
    iput-object v6, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 60
    iput-object v6, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    goto/16 :goto_0

    .line 45
    :cond_2
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Thread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/amap/api/maps/offlinemap/c;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is over!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/n;->a(Ljava/lang/String;)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->e:Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 50
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    if-eqz v0, :cond_3

    .line 52
    :try_start_4
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 56
    :goto_3
    iput-object v6, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    .line 58
    :cond_3
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 60
    iput-object v6, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    goto/16 :goto_0

    .line 53
    :catch_1
    move-exception v0

    .line 54
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 53
    :catch_2
    move-exception v0

    .line 54
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 50
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    if-eqz v1, :cond_4

    .line 52
    :try_start_5
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 56
    :goto_4
    iput-object v6, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    .line 58
    :cond_4
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_5

    .line 59
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 60
    iput-object v6, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    :cond_5
    throw v0

    .line 53
    :catch_3
    move-exception v1

    .line 54
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 64
    :cond_6
    return-void
.end method
