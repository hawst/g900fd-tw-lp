.class Lcom/amap/api/maps/offlinemap/k;
.super Ljava/lang/Thread;
.source "SiteFileFetch.java"


# instance fields
.field a:Lcom/amap/api/maps/offlinemap/l;

.field b:[J

.field c:[J

.field d:[Lcom/amap/api/maps/offlinemap/c;

.field e:J

.field f:Z

.field g:Z

.field h:Ljava/io/File;

.field i:Ljava/io/DataOutputStream;

.field j:Lcom/amap/api/maps/offlinemap/d;

.field k:Lcom/amap/api/maps/offlinemap/m;

.field private l:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/amap/api/maps/offlinemap/l;Lcom/amap/api/maps/offlinemap/d;Lcom/amap/api/maps/offlinemap/m;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->a:Lcom/amap/api/maps/offlinemap/l;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/k;->f:Z

    .line 27
    iput-boolean v3, p0, Lcom/amap/api/maps/offlinemap/k;->g:Z

    .line 36
    iput-object p1, p0, Lcom/amap/api/maps/offlinemap/k;->a:Lcom/amap/api/maps/offlinemap/l;

    .line 39
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/l;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/l;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->h:Ljava/io/File;

    .line 41
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/amap/api/maps/offlinemap/k;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iput-boolean v3, p0, Lcom/amap/api/maps/offlinemap/k;->f:Z

    .line 43
    invoke-direct {p0}, Lcom/amap/api/maps/offlinemap/k;->e()Z

    .line 48
    :goto_0
    iput-object p2, p0, Lcom/amap/api/maps/offlinemap/k;->j:Lcom/amap/api/maps/offlinemap/d;

    .line 49
    iput-object p3, p0, Lcom/amap/api/maps/offlinemap/k;->k:Lcom/amap/api/maps/offlinemap/m;

    .line 50
    iput-object p4, p0, Lcom/amap/api/maps/offlinemap/k;->l:Landroid/content/Context;

    .line 51
    return-void

    .line 45
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/l;->d()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    .line 46
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/l;->d()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 235
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error Code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 118
    sget v0, Lcom/amap/api/mapcore/g;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 119
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x3

    if-ge v1, v0, :cond_0

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/mapcore/g;->a(Landroid/content/Context;)Z
    :try_end_0
    .catch Lcom/amap/api/maps/AMapException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 122
    if-eqz v0, :cond_1

    .line 131
    :cond_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    const-string v2, "AuthFailure"

    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->printStackTrace()V

    .line 119
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private d()Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x64

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 171
    .line 173
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/k;->h:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->i:Ljava/io/DataOutputStream;

    .line 174
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->i:Ljava/io/DataOutputStream;

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    invoke-virtual {v1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 175
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->i:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v1, v0

    move-wide v2, v4

    .line 177
    :goto_0
    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v6, v6

    if-ge v1, v6, :cond_1

    .line 179
    if-nez v1, :cond_0

    .line 180
    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v6, v6, v1

    iget-wide v6, v6, Lcom/amap/api/maps/offlinemap/c;->b:J

    add-long/2addr v2, v6

    .line 184
    :goto_1
    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/k;->i:Ljava/io/DataOutputStream;

    iget-object v7, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v7, v7, v1

    iget-wide v8, v7, Lcom/amap/api/maps/offlinemap/c;->b:J

    invoke-virtual {v6, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 185
    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/k;->i:Ljava/io/DataOutputStream;

    iget-object v7, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v7, v7, v1

    iget-wide v8, v7, Lcom/amap/api/maps/offlinemap/c;->c:J

    invoke-virtual {v6, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 177
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 182
    :cond_0
    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v6, v6, v1

    iget-wide v6, v6, Lcom/amap/api/maps/offlinemap/c;->b:J

    iget-object v8, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    add-int/lit8 v9, v1, -0x1

    aget-object v8, v8, v9

    iget-wide v8, v8, Lcom/amap/api/maps/offlinemap/c;->c:J

    sub-long/2addr v6, v8

    add-long/2addr v2, v6

    goto :goto_1

    .line 187
    :cond_1
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->i:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 188
    iget-wide v6, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    cmp-long v1, v6, v4

    if-lez v1, :cond_3

    .line 189
    mul-long v4, v2, v10

    iget-wide v6, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    div-long/2addr v4, v6

    .line 190
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->k:Lcom/amap/api/maps/offlinemap/m;

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/maps/offlinemap/m;->a(J)V

    .line 191
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->k:Lcom/amap/api/maps/offlinemap/m;

    long-to-int v2, v4

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/offlinemap/m;->b(I)V

    .line 192
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->l:Landroid/content/Context;

    invoke-static {v1}, Lcom/amap/api/mapcore/util/v;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 193
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->j:Lcom/amap/api/maps/offlinemap/d;

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/k;->k:Lcom/amap/api/maps/offlinemap/m;

    const/4 v3, 0x0

    long-to-int v6, v4

    invoke-virtual {v1, v2, v3, v6}, Lcom/amap/api/maps/offlinemap/d;->a(Lcom/amap/api/maps/offlinemap/m;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 196
    :cond_2
    cmp-long v1, v4, v10

    if-ltz v1, :cond_3

    .line 197
    const/4 v0, 0x1

    .line 205
    :cond_3
    :goto_2
    return v0

    .line 200
    :catch_0
    move-exception v1

    .line 201
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 202
    :catch_1
    move-exception v1

    .line 203
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method

.method private e()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 209
    .line 211
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/k;->h:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 213
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    .line 214
    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-gtz v1, :cond_0

    .line 231
    :goto_0
    return v0

    .line 217
    :cond_0
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 218
    new-array v3, v1, [J

    iput-object v3, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    .line 219
    new-array v1, v1, [J

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    move v1, v0

    .line 220
    :goto_1
    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 221
    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 222
    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 220
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 224
    :cond_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 225
    const/4 v0, 0x1

    goto :goto_0

    .line 226
    :catch_0
    move-exception v1

    .line 227
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 228
    :catch_1
    move-exception v1

    .line 229
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 5

    .prologue
    .line 134
    const/4 v1, -0x1

    .line 136
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/k;->a:Lcom/amap/api/maps/offlinemap/l;

    invoke-virtual {v2}, Lcom/amap/api/maps/offlinemap/l;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 139
    const-string v2, "User-Agent"

    sget-object v3, Lcom/amap/api/mapcore/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    .line 142
    const/16 v3, 0x190

    if-lt v2, v3, :cond_0

    .line 143
    invoke-direct {p0, v2}, Lcom/amap/api/maps/offlinemap/k;->a(I)V

    .line 144
    const-wide/16 v0, -0x2

    .line 167
    :goto_0
    return-wide v0

    .line 147
    :cond_0
    const/4 v2, 0x1

    .line 151
    :goto_1
    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v3

    .line 152
    if-eqz v3, :cond_2

    .line 153
    const-string v4, "Content-Length"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 154
    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_2
    move v1, v0

    .line 166
    :goto_3
    invoke-static {v1}, Lcom/amap/api/maps/offlinemap/n;->b(I)V

    .line 167
    int-to-long v0, v1

    goto :goto_0

    .line 147
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 161
    :catch_0
    move-exception v0

    .line 162
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 163
    :catch_1
    move-exception v0

    .line 164
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_3

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/k;->g:Z

    .line 240
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 241
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 242
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/amap/api/maps/offlinemap/c;->a()V

    .line 240
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    :cond_1
    return-void
.end method

.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/mapcore/util/v;->d(Landroid/content/Context;)Z

    move-result v0

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/amap/api/maps/offlinemap/k;->c()V

    .line 59
    :cond_0
    sget v0, Lcom/amap/api/mapcore/g;->a:I

    if-eq v0, v10, :cond_2

    .line 115
    :cond_1
    :goto_0
    return-void

    .line 62
    :cond_2
    iget-boolean v0, p0, Lcom/amap/api/maps/offlinemap/k;->f:Z

    if-eqz v0, :cond_3

    .line 63
    invoke-virtual {p0}, Lcom/amap/api/maps/offlinemap/k;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    .line 64
    iget-wide v0, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 65
    const-string v0, "File Length is not known!"

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/n;->a(Ljava/lang/String;)V

    .line 78
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v0, v0

    new-array v0, v0, [Lcom/amap/api/maps/offlinemap/c;

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    move v8, v9

    .line 79
    :goto_2
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v0, v0

    if-ge v8, v0, :cond_8

    .line 80
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    new-instance v1, Lcom/amap/api/maps/offlinemap/c;

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/k;->a:Lcom/amap/api/maps/offlinemap/l;

    invoke-virtual {v2}, Lcom/amap/api/maps/offlinemap/l;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/amap/api/maps/offlinemap/k;->a:Lcom/amap/api/maps/offlinemap/l;

    invoke-virtual {v4}, Lcom/amap/api/maps/offlinemap/l;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/amap/api/maps/offlinemap/k;->a:Lcom/amap/api/maps/offlinemap/l;

    invoke-virtual {v4}, Lcom/amap/api/maps/offlinemap/l;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    aget-wide v4, v4, v8

    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    aget-wide v6, v6, v8

    invoke-direct/range {v1 .. v8}, Lcom/amap/api/maps/offlinemap/c;-><init>(Ljava/lang/String;Ljava/lang/String;JJI)V

    aput-object v1, v0, v8

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Thread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , nStartPos = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    aget-wide v2, v1, v8

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nEndPos = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    aget-wide v2, v1, v8

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/n;->a(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/amap/api/maps/offlinemap/c;->start()V

    .line 79
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 66
    :cond_4
    iget-wide v0, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 67
    const-string v0, "File is not access!"

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/n;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/amap/api/maps/AMapException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 110
    :catch_0
    move-exception v0

    .line 111
    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->printStackTrace()V

    goto/16 :goto_0

    :cond_5
    move v0, v9

    .line 69
    :goto_3
    :try_start_1
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 70
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    int-to-long v2, v0

    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v6, v6

    int-to-long v6, v6

    div-long/2addr v4, v6

    mul-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move v0, v9

    .line 72
    :goto_4
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    .line 73
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    add-int/lit8 v3, v0, 0x1

    aget-wide v2, v2, v3

    aput-wide v2, v1, v0

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 75
    :cond_7
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->c:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/k;->e:J

    aput-wide v2, v0, v1
    :try_end_1
    .catch Lcom/amap/api/maps/AMapException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 112
    :catch_1
    move-exception v0

    .line 113
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    :cond_8
    move v0, v9

    .line 90
    :cond_9
    :try_start_2
    iget-boolean v1, p0, Lcom/amap/api/maps/offlinemap/k;->g:Z

    if-nez v1, :cond_a

    .line 91
    invoke-direct {p0}, Lcom/amap/api/maps/offlinemap/k;->d()Z

    move-result v0

    .line 92
    const/16 v1, 0x1f4

    invoke-static {v1}, Lcom/amap/api/maps/offlinemap/n;->a(I)V

    move v1, v9

    .line 94
    :goto_5
    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/k;->b:[J

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 95
    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/k;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v2, v2, v1

    iget-boolean v2, v2, Lcom/amap/api/maps/offlinemap/c;->e:Z

    if-nez v2, :cond_c

    move v1, v9

    .line 100
    :goto_6
    if-nez v1, :cond_a

    if-eqz v0, :cond_9

    .line 103
    :cond_a
    iget-boolean v1, p0, Lcom/amap/api/maps/offlinemap/k;->g:Z

    if-nez v1, :cond_b

    .line 104
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/k;->j:Lcom/amap/api/maps/offlinemap/d;

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/k;->k:Lcom/amap/api/maps/offlinemap/m;

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/offlinemap/d;->b(Lcom/amap/api/maps/offlinemap/m;)V

    .line 106
    :cond_b
    if-eqz v0, :cond_1

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/k;->g:Z
    :try_end_2
    .catch Lcom/amap/api/maps/AMapException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 94
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_d
    move v1, v10

    goto :goto_6
.end method
