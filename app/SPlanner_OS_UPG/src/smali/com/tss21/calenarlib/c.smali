.class public Lcom/tss21/calenarlib/c;
.super Ljava/lang/Object;
.source "TSDate.java"


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 43
    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v1

    add-int/lit16 v1, v1, 0x76c

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0}, Ljava/util/Date;->getDate()I

    move-result v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/tss21/calenarlib/c;->a(III)V

    .line 44
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p0, p1, p2, p3}, Lcom/tss21/calenarlib/c;->a(III)V

    .line 59
    return-void
.end method


# virtual methods
.method public a(III)V
    .locals 0

    .prologue
    .line 73
    iput p1, p0, Lcom/tss21/calenarlib/c;->a:I

    .line 74
    iput p2, p0, Lcom/tss21/calenarlib/c;->b:I

    .line 75
    iput p3, p0, Lcom/tss21/calenarlib/c;->c:I

    .line 76
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 87
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%04d-%02d-%02d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/tss21/calenarlib/c;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 88
    iget v4, p0, Lcom/tss21/calenarlib/c;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/tss21/calenarlib/c;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 87
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
