.class public Lcom/tss21/calenarlib/a;
.super Ljava/lang/Exception;
.source "CalendarConvertException.java"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    const/16 v0, -0x63

    invoke-direct {p0, p1, v0}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 72
    iput p2, p0, Lcom/tss21/calenarlib/a;->a:I

    .line 73
    return-void
.end method

.method public static a(II)Lcom/tss21/calenarlib/a;
    .locals 3

    .prologue
    .line 100
    new-instance v0, Lcom/tss21/calenarlib/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 101
    invoke-static {p1}, Lcom/tss21/calenarlib/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Year Value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 102
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    .line 100
    invoke-direct {v0, v1, v2}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static b(II)Lcom/tss21/calenarlib/a;
    .locals 3

    .prologue
    .line 120
    new-instance v0, Lcom/tss21/calenarlib/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 121
    invoke-static {p1}, Lcom/tss21/calenarlib/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Month Value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 122
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x2

    .line 120
    invoke-direct {v0, v1, v2}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static c(II)Lcom/tss21/calenarlib/a;
    .locals 3

    .prologue
    .line 140
    new-instance v0, Lcom/tss21/calenarlib/a;

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/tss21/calenarlib/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 142
    const-string v2, " Day Value :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 141
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 142
    const/4 v2, -0x3

    .line 140
    invoke-direct {v0, v1, v2}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method
