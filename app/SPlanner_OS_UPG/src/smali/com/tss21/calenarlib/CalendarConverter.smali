.class public Lcom/tss21/calenarlib/CalendarConverter;
.super Ljava/lang/Object;
.source "CalendarConverter.java"


# static fields
.field private static a:Z


# instance fields
.field private b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    sput-boolean v1, Lcom/tss21/calenarlib/CalendarConverter;->a:Z

    .line 41
    :try_start_0
    const-string v0, "tscalendar"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 42
    const/4 v0, 0x1

    sput-boolean v0, Lcom/tss21/calenarlib/CalendarConverter;->a:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    sput-boolean v1, Lcom/tss21/calenarlib/CalendarConverter;->a:Z

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput p1, p0, Lcom/tss21/calenarlib/CalendarConverter;->b:I

    .line 62
    iput p2, p0, Lcom/tss21/calenarlib/CalendarConverter;->c:I

    .line 63
    return-void
.end method

.method private static a(IIIII)Lcom/tss21/calenarlib/c;
    .locals 4

    .prologue
    .line 320
    if-gez p0, :cond_0

    .line 321
    packed-switch p0, :pswitch_data_0

    .line 324
    new-instance v0, Lcom/tss21/calenarlib/a;

    const-string v1, "Unknown Error"

    invoke-direct {v0, v1}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :pswitch_0
    invoke-static {p1, p4}, Lcom/tss21/calenarlib/a;->a(II)Lcom/tss21/calenarlib/a;

    move-result-object v0

    throw v0

    .line 329
    :pswitch_1
    invoke-static {p2, p4}, Lcom/tss21/calenarlib/a;->b(II)Lcom/tss21/calenarlib/a;

    move-result-object v0

    throw v0

    .line 332
    :pswitch_2
    invoke-static {p3, p4}, Lcom/tss21/calenarlib/a;->c(II)Lcom/tss21/calenarlib/a;

    move-result-object v0

    throw v0

    .line 338
    :cond_0
    shr-int/lit8 v0, p0, 0x10

    const v1, 0xffff

    and-int/2addr v0, v1

    .line 339
    shr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    .line 340
    and-int/lit16 v2, p0, 0xff

    .line 341
    new-instance v3, Lcom/tss21/calenarlib/c;

    invoke-direct {v3, v0, v1, v2}, Lcom/tss21/calenarlib/c;-><init>(III)V

    return-object v3

    .line 321
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(IIIIII)Lcom/tss21/calenarlib/c;
    .locals 4

    .prologue
    const/4 v3, -0x4

    .line 165
    sget-boolean v0, Lcom/tss21/calenarlib/CalendarConverter;->a:Z

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Lcom/tss21/calenarlib/a;

    const-string v1, "Library Load fail : libtscalendar.so"

    invoke-direct {v0, v1}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    invoke-static {p3}, Lcom/tss21/calenarlib/CalendarConverter;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 170
    new-instance v0, Lcom/tss21/calenarlib/a;

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fromCalendarID is not supported :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-direct {v0, v1, v3}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 175
    :cond_1
    invoke-static {p4}, Lcom/tss21/calenarlib/CalendarConverter;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 176
    new-instance v0, Lcom/tss21/calenarlib/a;

    .line 177
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "toCalendarID is not supported :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-direct {v0, v1, v3}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 181
    :cond_2
    if-ne p3, p4, :cond_3

    .line 182
    new-instance v0, Lcom/tss21/calenarlib/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/tss21/calenarlib/c;-><init>(III)V

    .line 184
    :goto_0
    return-object v0

    .line 185
    :cond_3
    invoke-static/range {p0 .. p5}, Lcom/tss21/calenarlib/CalendarConverter;->nativeConvertDate(IIIIII)I

    move-result v0

    .line 184
    invoke-static {v0, p0, p1, p2, p3}, Lcom/tss21/calenarlib/CalendarConverter;->a(IIIII)Lcom/tss21/calenarlib/c;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 346
    invoke-static {p0}, Lcom/tss21/calenarlib/b;->a(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 347
    const/4 v0, 0x0

    .line 349
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(III)I
    .locals 2

    .prologue
    .line 280
    sget-boolean v0, Lcom/tss21/calenarlib/CalendarConverter;->a:Z

    if-nez v0, :cond_0

    .line 281
    new-instance v0, Lcom/tss21/calenarlib/a;

    const-string v1, "Library Load fail : libtscalendar.so"

    invoke-direct {v0, v1}, Lcom/tss21/calenarlib/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/tss21/calenarlib/CalendarConverter;->nativeGetMaxDateOfYearMonth(III)I

    move-result v0

    .line 285
    if-nez v0, :cond_3

    .line 286
    const/4 v0, 0x1

    if-lt p1, v0, :cond_1

    const/16 v0, 0xc

    if-le p1, v0, :cond_2

    .line 287
    :cond_1
    invoke-static {p1, p2}, Lcom/tss21/calenarlib/a;->b(II)Lcom/tss21/calenarlib/a;

    move-result-object v0

    throw v0

    .line 290
    :cond_2
    invoke-static {p0, p2}, Lcom/tss21/calenarlib/a;->a(II)Lcom/tss21/calenarlib/a;

    move-result-object v0

    throw v0

    .line 294
    :cond_3
    return v0
.end method

.method private static native nativeConvertDate(IIIIII)I
.end method

.method private static native nativeGetMaxDateOfYearMonth(III)I
.end method


# virtual methods
.method public a(III)Lcom/tss21/calenarlib/c;
    .locals 6

    .prologue
    .line 103
    iget v3, p0, Lcom/tss21/calenarlib/CalendarConverter;->b:I

    const/4 v4, 0x0

    .line 104
    iget v5, p0, Lcom/tss21/calenarlib/CalendarConverter;->c:I

    move v0, p1

    move v1, p2

    move v2, p3

    .line 103
    invoke-static/range {v0 .. v5}, Lcom/tss21/calenarlib/CalendarConverter;->a(IIIIII)Lcom/tss21/calenarlib/c;

    move-result-object v0

    return-object v0
.end method

.method public b(III)Lcom/tss21/calenarlib/c;
    .locals 6

    .prologue
    .line 135
    const/4 v3, 0x0

    iget v4, p0, Lcom/tss21/calenarlib/CalendarConverter;->b:I

    .line 136
    iget v5, p0, Lcom/tss21/calenarlib/CalendarConverter;->c:I

    move v0, p1

    move v1, p2

    move v2, p3

    .line 135
    invoke-static/range {v0 .. v5}, Lcom/tss21/calenarlib/CalendarConverter;->a(IIIIII)Lcom/tss21/calenarlib/c;

    move-result-object v0

    return-object v0
.end method
