.class public Lcom/google/android/gms/internal/fy;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/internal/fz;

.field public static final b:Lcom/google/android/gms/internal/ga;

.field public static final c:Lcom/google/android/gms/internal/gc;

.field public static final d:Lcom/google/android/gms/internal/gb;

.field public static final e:Lcom/google/android/gms/internal/gd;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const v3, 0x3e8fa0

    new-instance v0, Lcom/google/android/gms/internal/fz;

    const-string v1, "created"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/fz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fy;->a:Lcom/google/android/gms/internal/fz;

    new-instance v0, Lcom/google/android/gms/internal/ga;

    const-string v1, "lastOpenedTime"

    const v2, 0x419ce0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/ga;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fy;->b:Lcom/google/android/gms/internal/ga;

    new-instance v0, Lcom/google/android/gms/internal/gc;

    const-string v1, "modified"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/gc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fy;->c:Lcom/google/android/gms/internal/gc;

    new-instance v0, Lcom/google/android/gms/internal/gb;

    const-string v1, "modifiedByMe"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/gb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fy;->d:Lcom/google/android/gms/internal/gb;

    new-instance v0, Lcom/google/android/gms/internal/gd;

    const-string v1, "sharedWithMe"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/gd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fy;->e:Lcom/google/android/gms/internal/gd;

    return-void
.end method
