.class public abstract Lcom/google/android/gms/internal/dx;
.super Ljava/lang/Object;


# static fields
.field private static final c:Ljava/lang/Object;

.field private static d:Lcom/google/android/gms/internal/ec;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/dx;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/internal/dx;->d:Lcom/google/android/gms/internal/ec;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/dx;->e:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/internal/dx;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/internal/dx;->b:Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/dx;
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/dz;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/internal/dz;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/dx;
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/eb;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/internal/eb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/dx;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/dy;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/dy;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method
