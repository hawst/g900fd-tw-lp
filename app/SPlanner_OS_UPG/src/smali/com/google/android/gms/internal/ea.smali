.class public final Lcom/google/android/gms/internal/ea;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/bt;
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/bo;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/internal/bq;

.field public final c:Lcom/google/android/gms/internal/br;

.field public final d:Landroid/content/Context;

.field public final e:Lcom/google/android/gms/internal/bp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bo;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bo;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ea;->CREATOR:Lcom/google/android/gms/internal/bo;

    return-void
.end method

.method constructor <init>(ILandroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ea;->a:I

    invoke-static {p2}, Lcom/google/android/gms/b/k;->a(Landroid/os/IBinder;)Lcom/google/android/gms/b/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Lcom/google/android/gms/b/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bq;

    iput-object v0, p0, Lcom/google/android/gms/internal/ea;->b:Lcom/google/android/gms/internal/bq;

    invoke-static {p3}, Lcom/google/android/gms/b/k;->a(Landroid/os/IBinder;)Lcom/google/android/gms/b/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Lcom/google/android/gms/b/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/br;

    iput-object v0, p0, Lcom/google/android/gms/internal/ea;->c:Lcom/google/android/gms/internal/br;

    invoke-static {p4}, Lcom/google/android/gms/b/k;->a(Landroid/os/IBinder;)Lcom/google/android/gms/b/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Lcom/google/android/gms/b/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/internal/ea;->d:Landroid/content/Context;

    invoke-static {p5}, Lcom/google/android/gms/b/k;->a(Landroid/os/IBinder;)Lcom/google/android/gms/b/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Lcom/google/android/gms/b/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bp;

    iput-object v0, p0, Lcom/google/android/gms/internal/ea;->e:Lcom/google/android/gms/internal/bp;

    return-void
.end method


# virtual methods
.method a()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ea;->e:Lcom/google/android/gms/internal/bp;

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/b/j;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method b()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ea;->b:Lcom/google/android/gms/internal/bq;

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/b/j;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method c()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ea;->c:Lcom/google/android/gms/internal/br;

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/b/j;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method d()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ea;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/b/m;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/b/j;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/bo;->a(Lcom/google/android/gms/internal/ea;Landroid/os/Parcel;I)V

    return-void
.end method
