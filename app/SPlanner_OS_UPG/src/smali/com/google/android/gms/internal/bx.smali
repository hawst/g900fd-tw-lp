.class public Lcom/google/android/gms/internal/bx;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/bt;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final c:Lcom/google/android/gms/internal/bx;


# instance fields
.field public final b:Ljava/lang/String;

.field private final d:Ljava/lang/Object;

.field private final e:Lcom/google/android/gms/internal/by;

.field private f:Ljava/math/BigInteger;

.field private final g:Ljava/util/HashSet;

.field private final h:Ljava/util/HashMap;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Landroid/content/Context;

.field private m:Lcom/google/android/gms/internal/gs;

.field private n:Z

.field private o:Lcom/google/android/gms/internal/c;

.field private p:Lcom/google/android/gms/internal/d;

.field private q:Lcom/google/android/gms/internal/b;

.field private r:Ljava/util/LinkedList;

.field private s:Z

.field private t:Landroid/os/Bundle;

.field private u:Lcom/google/android/gms/internal/bs;

.field private v:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bx;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bx;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/bx;->c:Lcom/google/android/gms/internal/bx;

    sget-object v0, Lcom/google/android/gms/internal/bx;->c:Lcom/google/android/gms/internal/bx;

    iget-object v0, v0, Lcom/google/android/gms/internal/bx;->b:Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/bx;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/bx;->d:Ljava/lang/Object;

    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    iput-object v0, p0, Lcom/google/android/gms/internal/bx;->f:Ljava/math/BigInteger;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/bx;->g:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/bx;->h:Ljava/util/HashMap;

    iput-boolean v2, p0, Lcom/google/android/gms/internal/bx;->i:Z

    iput-boolean v3, p0, Lcom/google/android/gms/internal/bx;->j:Z

    iput-boolean v2, p0, Lcom/google/android/gms/internal/bx;->k:Z

    iput-boolean v3, p0, Lcom/google/android/gms/internal/bx;->n:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/bx;->o:Lcom/google/android/gms/internal/c;

    iput-object v1, p0, Lcom/google/android/gms/internal/bx;->p:Lcom/google/android/gms/internal/d;

    iput-object v1, p0, Lcom/google/android/gms/internal/bx;->q:Lcom/google/android/gms/internal/b;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/bx;->r:Ljava/util/LinkedList;

    iput-boolean v2, p0, Lcom/google/android/gms/internal/bx;->s:Z

    invoke-static {}, Lcom/google/android/gms/internal/u;->a()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bx;->t:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/google/android/gms/internal/bx;->u:Lcom/google/android/gms/internal/bs;

    invoke-static {}, Lcom/google/android/gms/internal/cf;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bx;->b:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/internal/by;

    iget-object v1, p0, Lcom/google/android/gms/internal/bx;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/by;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/bx;->e:Lcom/google/android/gms/internal/by;

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/bx;->c:Lcom/google/android/gms/internal/bx;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/bx;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/bx;->c:Lcom/google/android/gms/internal/bx;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/internal/bx;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/bx;->c:Lcom/google/android/gms/internal/bx;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/bx;->b(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static c()Landroid/os/Bundle;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/bx;->c:Lcom/google/android/gms/internal/bx;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/bx;->d()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/bx;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bx;->v:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bx;->m:Lcom/google/android/gms/internal/gs;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/gs;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/bx;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bx;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/b;->b(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1
.end method

.method public b(Ljava/lang/Throwable;)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/bx;->k:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/bs;

    iget-object v1, p0, Lcom/google/android/gms/internal/bx;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/internal/bx;->m:Lcom/google/android/gms/internal/gs;

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/gms/internal/bs;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/gs;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/bs;->b(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public d()Landroid/os/Bundle;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/bx;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/bx;->t:Landroid/os/Bundle;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
