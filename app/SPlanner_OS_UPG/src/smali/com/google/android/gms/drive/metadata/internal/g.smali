.class public final Lcom/google/android/gms/drive/metadata/internal/g;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/g;->a:Ljava/util/Map;

    sget-object v0, Lcom/google/android/gms/internal/fn;->a:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->A:Lcom/google/android/gms/internal/fv;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->r:Lcom/google/android/gms/internal/fr;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->y:Lcom/google/android/gms/internal/fu;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->B:Lcom/google/android/gms/internal/fw;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->l:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->m:Lcom/google/android/gms/internal/fq;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->j:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->o:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->w:Lcom/google/android/gms/internal/fs;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->b:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->t:Lcom/google/android/gms/drive/metadata/g;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->d:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->k:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->e:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->f:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->g:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->q:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->n:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->s:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->u:Lcom/google/android/gms/drive/metadata/internal/o;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->v:Lcom/google/android/gms/drive/metadata/internal/o;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->x:Lcom/google/android/gms/internal/ft;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->C:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->D:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->i:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->h:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->z:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->p:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->c:Lcom/google/android/gms/internal/fp;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->E:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->F:Lcom/google/android/gms/drive/metadata/internal/d;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fn;->G:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fy;->a:Lcom/google/android/gms/internal/fz;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fy;->c:Lcom/google/android/gms/internal/gc;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fy;->d:Lcom/google/android/gms/internal/gb;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fy;->e:Lcom/google/android/gms/internal/gd;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/fy;->b:Lcom/google/android/gms/internal/ga;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/gf;->a:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/gf;->b:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/g;->a(Lcom/google/android/gms/drive/metadata/a;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/a;
    .locals 1

    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/g;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/a;

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/drive/metadata/a;)V
    .locals 3

    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/g;->a:Ljava/util/Map;

    invoke-interface {p0}, Lcom/google/android/gms/drive/metadata/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicate field name registered: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/gms/drive/metadata/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/g;->a:Ljava/util/Map;

    invoke-interface {p0}, Lcom/google/android/gms/drive/metadata/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
