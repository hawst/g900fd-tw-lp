.class public Lcom/google/android/gms/internal/fn;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lcom/google/android/gms/internal/fv;

.field public static final B:Lcom/google/android/gms/internal/fw;

.field public static final C:Lcom/google/android/gms/drive/metadata/a;

.field public static final D:Lcom/google/android/gms/drive/metadata/a;

.field public static final E:Lcom/google/android/gms/drive/metadata/a;

.field public static final F:Lcom/google/android/gms/drive/metadata/internal/d;

.field public static final G:Lcom/google/android/gms/drive/metadata/a;

.field public static final a:Lcom/google/android/gms/drive/metadata/a;

.field public static final b:Lcom/google/android/gms/drive/metadata/a;

.field public static final c:Lcom/google/android/gms/internal/fp;

.field public static final d:Lcom/google/android/gms/drive/metadata/a;

.field public static final e:Lcom/google/android/gms/drive/metadata/a;

.field public static final f:Lcom/google/android/gms/drive/metadata/a;

.field public static final g:Lcom/google/android/gms/drive/metadata/a;

.field public static final h:Lcom/google/android/gms/drive/metadata/a;

.field public static final i:Lcom/google/android/gms/drive/metadata/a;

.field public static final j:Lcom/google/android/gms/drive/metadata/a;

.field public static final k:Lcom/google/android/gms/drive/metadata/a;

.field public static final l:Lcom/google/android/gms/drive/metadata/a;

.field public static final m:Lcom/google/android/gms/internal/fq;

.field public static final n:Lcom/google/android/gms/drive/metadata/a;

.field public static final o:Lcom/google/android/gms/drive/metadata/a;

.field public static final p:Lcom/google/android/gms/drive/metadata/a;

.field public static final q:Lcom/google/android/gms/drive/metadata/a;

.field public static final r:Lcom/google/android/gms/internal/fr;

.field public static final s:Lcom/google/android/gms/drive/metadata/a;

.field public static final t:Lcom/google/android/gms/drive/metadata/g;

.field public static final u:Lcom/google/android/gms/drive/metadata/internal/o;

.field public static final v:Lcom/google/android/gms/drive/metadata/internal/o;

.field public static final w:Lcom/google/android/gms/internal/fs;

.field public static final x:Lcom/google/android/gms/internal/ft;

.field public static final y:Lcom/google/android/gms/internal/fu;

.field public static final z:Lcom/google/android/gms/drive/metadata/a;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const v8, 0x4c4b40

    const v7, 0x432380

    const v6, 0x5b8d80

    const v5, 0x3e8fa0

    const v4, 0x419ce0

    sget-object v0, Lcom/google/android/gms/internal/ge;->a:Lcom/google/android/gms/internal/ge;

    sput-object v0, Lcom/google/android/gms/internal/fn;->a:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "alternateLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->b:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/internal/fp;

    invoke-direct {v0, v8}, Lcom/google/android/gms/internal/fp;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->c:Lcom/google/android/gms/internal/fp;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "description"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->d:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "embedLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->e:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "fileExtension"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->f:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/i;

    const-string v1, "fileSize"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->g:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "hasThumbnail"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->h:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "indexableText"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->i:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "isAppData"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->j:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "isCopyable"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->k:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "isEditable"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->l:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/internal/fq;

    const-string v1, "isPinned"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/fq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->m:Lcom/google/android/gms/internal/fq;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "isRestricted"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->n:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "isShared"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->o:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "isTrashable"

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->p:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "isViewed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->q:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/internal/fr;

    const-string v1, "mimeType"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/fr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->r:Lcom/google/android/gms/internal/fr;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "originalFilename"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->s:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "ownerNames"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->t:Lcom/google/android/gms/drive/metadata/g;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/o;

    const-string v1, "lastModifyingUser"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->u:Lcom/google/android/gms/drive/metadata/internal/o;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/o;

    const-string v1, "sharingUser"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->v:Lcom/google/android/gms/drive/metadata/internal/o;

    new-instance v0, Lcom/google/android/gms/internal/fs;

    const-string v1, "parents"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/fs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->w:Lcom/google/android/gms/internal/fs;

    new-instance v0, Lcom/google/android/gms/internal/ft;

    const-string v1, "quotaBytesUsed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/internal/ft;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->x:Lcom/google/android/gms/internal/ft;

    new-instance v0, Lcom/google/android/gms/internal/fu;

    const-string v1, "starred"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/fu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->y:Lcom/google/android/gms/internal/fu;

    new-instance v0, Lcom/google/android/gms/internal/fo;

    const-string v1, "thumbnail"

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/google/android/gms/internal/fo;-><init>(Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->z:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/internal/fv;

    const-string v1, "title"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/fv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->A:Lcom/google/android/gms/internal/fv;

    new-instance v0, Lcom/google/android/gms/internal/fw;

    const-string v1, "trashed"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/fw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->B:Lcom/google/android/gms/internal/fw;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "webContentLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->C:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "webViewLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->D:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "uniqueIdentifier"

    invoke-direct {v0, v1, v8}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->E:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    const-string v1, "writersCanShare"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->F:Lcom/google/android/gms/drive/metadata/internal/d;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "role"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/fn;->G:Lcom/google/android/gms/drive/metadata/a;

    return-void
.end method
