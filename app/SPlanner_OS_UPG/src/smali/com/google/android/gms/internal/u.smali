.class public final Lcom/google/android/gms/internal/u;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/bt;
.end annotation


# static fields
.field public static a:Lcom/google/android/gms/internal/dx;

.field public static b:Lcom/google/android/gms/internal/dx;

.field public static c:Lcom/google/android/gms/internal/dx;

.field public static d:Lcom/google/android/gms/internal/dx;

.field public static e:Lcom/google/android/gms/internal/dx;

.field public static f:Lcom/google/android/gms/internal/dx;

.field public static g:Lcom/google/android/gms/internal/dx;

.field public static h:Lcom/google/android/gms/internal/dx;

.field public static i:Lcom/google/android/gms/internal/dx;

.field public static j:Lcom/google/android/gms/internal/dx;

.field public static k:Lcom/google/android/gms/internal/dx;

.field public static l:Lcom/google/android/gms/internal/dx;

.field public static m:Lcom/google/android/gms/internal/dx;

.field public static n:Lcom/google/android/gms/internal/dx;

.field private static final o:Landroid/os/Bundle;

.field private static p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0xa

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/u;->o:Landroid/os/Bundle;

    sput-boolean v3, Lcom/google/android/gms/internal/u;->p:Z

    const-string v0, "gads:sdk_core_location"

    const-string v2, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/sdk-core-v40.html"

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->a:Lcom/google/android/gms/internal/dx;

    const-string v2, "gads:sdk_core_experiment_id"

    move-object v0, v1

    nop

    nop

    invoke-static {v2, v0}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->b:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:sdk_crash_report_enabled"

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->c:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:sdk_crash_report_full_stacktrace"

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->d:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:block_autoclicks"

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->e:Lcom/google/android/gms/internal/dx;

    const-string v2, "gads:block_autoclicks_experiment_id"

    move-object v0, v1

    nop

    nop

    invoke-static {v2, v0}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->f:Lcom/google/android/gms/internal/dx;

    sput-boolean v4, Lcom/google/android/gms/internal/u;->p:Z

    const-string v0, "gads:enable_content_fetching"

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->i:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:content_length_weight"

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;I)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->j:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:content_age_weight"

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;I)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->k:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:min_content_len"

    const/16 v2, 0xb

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;I)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->l:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:fingerprint_number"

    invoke-static {v0, v5}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;I)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->m:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:sleep_sec"

    invoke-static {v0, v5}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;I)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->n:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:spam_app_context:enabled"

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->g:Lcom/google/android/gms/internal/dx;

    const-string v0, "gads:spam_app_context:experiment_id"

    nop

    nop

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/u;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/u;->h:Lcom/google/android/gms/internal/dx;

    return-void
.end method

.method public static a()Landroid/os/Bundle;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/u;->o:Landroid/os/Bundle;

    return-object v0
.end method

.method private static a(Ljava/lang/String;I)Lcom/google/android/gms/internal/dx;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/u;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/internal/dx;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/dx;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/u;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/dx;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/dx;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/u;->o:Landroid/os/Bundle;

    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/dx;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/dx;

    move-result-object v0

    return-object v0
.end method
