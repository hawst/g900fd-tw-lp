.class public Lcom/google/android/gms/internal/ct;
.super Landroid/webkit/WebViewClient;


# annotations
.annotation runtime Lcom/google/android/gms/internal/bt;
.end annotation


# instance fields
.field protected final a:Lcom/google/android/gms/internal/cr;

.field private final b:Ljava/util/HashMap;

.field private final c:Ljava/lang/Object;

.field private d:Lcom/google/android/gms/internal/ig;

.field private e:Lcom/google/android/gms/internal/bf;

.field private f:Lcom/google/android/gms/internal/cv;

.field private g:Lcom/google/android/gms/internal/w;

.field private h:Z

.field private i:Lcom/google/android/gms/internal/aj;

.field private j:Z

.field private k:Lcom/google/android/gms/internal/bl;

.field private final l:Lcom/google/android/gms/internal/au;

.field private m:Lcom/google/android/gms/internal/ih;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/cr;Z)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/internal/au;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/cr;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/internal/t;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/cr;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/t;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/internal/au;-><init>(Lcom/google/android/gms/internal/cr;Landroid/content/Context;Lcom/google/android/gms/internal/t;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/internal/ct;-><init>(Lcom/google/android/gms/internal/cr;ZLcom/google/android/gms/internal/au;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/internal/cr;ZLcom/google/android/gms/internal/au;)V
    .locals 1

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ct;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ct;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ct;->h:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    iput-boolean p2, p0, Lcom/google/android/gms/internal/ct;->j:Z

    iput-object p3, p0, Lcom/google/android/gms/internal/ct;->l:Lcom/google/android/gms/internal/au;

    return-void
.end method

.method private static a(Landroid/net/Uri;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "https"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/net/Uri;)V
    .locals 6

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/ai;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/internal/cf;->a(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/android/gms/internal/cp;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received GMSG: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/internal/cp;->d(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/internal/cp;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/ai;->a(Lcom/google/android/gms/internal/cr;Ljava/util/Map;)V

    :goto_1
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No GMSG handler found for GMSG: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/cp;->d(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public a()Lcom/google/android/gms/internal/ih;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->m:Lcom/google/android/gms/internal/ih;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/internal/cv;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ct;->f:Lcom/google/android/gms/internal/cv;

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/do;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->i()Z

    move-result v1

    new-instance v0, Lcom/google/android/gms/internal/dr;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/cr;->d()Lcom/google/android/gms/internal/ay;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/gms/internal/ay;->e:Z

    if-nez v2, :cond_0

    move-object v2, v3

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/internal/ct;->k:Lcom/google/android/gms/internal/bl;

    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/cr;->h()Lcom/google/android/gms/internal/gs;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/dr;-><init>(Lcom/google/android/gms/internal/do;Lcom/google/android/gms/internal/ig;Lcom/google/android/gms/internal/bf;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/gs;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ct;->a(Lcom/google/android/gms/internal/dr;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/internal/ct;->d:Lcom/google/android/gms/internal/ig;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/internal/ct;->e:Lcom/google/android/gms/internal/bf;

    goto :goto_1
.end method

.method protected a(Lcom/google/android/gms/internal/dr;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/az;->a(Landroid/content/Context;Lcom/google/android/gms/internal/dr;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/ig;Lcom/google/android/gms/internal/bf;Lcom/google/android/gms/internal/w;Lcom/google/android/gms/internal/bl;ZLcom/google/android/gms/internal/aj;Lcom/google/android/gms/internal/ih;)V
    .locals 2

    if-nez p7, :cond_0

    new-instance p7, Lcom/google/android/gms/internal/ih;

    const/4 v0, 0x0

    invoke-direct {p7, v0}, Lcom/google/android/gms/internal/ih;-><init>(Z)V

    :cond_0
    const-string v0, "/appEvent"

    new-instance v1, Lcom/google/android/gms/internal/v;

    invoke-direct {v1, p3}, Lcom/google/android/gms/internal/v;-><init>(Lcom/google/android/gms/internal/w;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/canOpenURLs"

    sget-object v1, Lcom/google/android/gms/internal/x;->b:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/canOpenIntents"

    sget-object v1, Lcom/google/android/gms/internal/x;->c:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/click"

    sget-object v1, Lcom/google/android/gms/internal/x;->d:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/close"

    sget-object v1, Lcom/google/android/gms/internal/x;->e:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/customClose"

    sget-object v1, Lcom/google/android/gms/internal/x;->f:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/httpTrack"

    sget-object v1, Lcom/google/android/gms/internal/x;->g:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/log"

    sget-object v1, Lcom/google/android/gms/internal/x;->h:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/open"

    new-instance v1, Lcom/google/android/gms/internal/al;

    invoke-direct {v1, p6, p7}, Lcom/google/android/gms/internal/al;-><init>(Lcom/google/android/gms/internal/aj;Lcom/google/android/gms/internal/ih;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/touch"

    sget-object v1, Lcom/google/android/gms/internal/x;->i:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/video"

    sget-object v1, Lcom/google/android/gms/internal/x;->j:Lcom/google/android/gms/internal/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    const-string v0, "/mraid"

    new-instance v1, Lcom/google/android/gms/internal/ak;

    invoke-direct {v1}, Lcom/google/android/gms/internal/ak;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ct;->a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V

    iput-object p1, p0, Lcom/google/android/gms/internal/ct;->d:Lcom/google/android/gms/internal/ig;

    iput-object p2, p0, Lcom/google/android/gms/internal/ct;->e:Lcom/google/android/gms/internal/bf;

    iput-object p3, p0, Lcom/google/android/gms/internal/ct;->g:Lcom/google/android/gms/internal/w;

    iput-object p6, p0, Lcom/google/android/gms/internal/ct;->i:Lcom/google/android/gms/internal/aj;

    iput-object p4, p0, Lcom/google/android/gms/internal/ct;->k:Lcom/google/android/gms/internal/bl;

    iput-object p7, p0, Lcom/google/android/gms/internal/ct;->m:Lcom/google/android/gms/internal/ih;

    invoke-virtual {p0, p5}, Lcom/google/android/gms/internal/ct;->a(Z)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/internal/ai;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/internal/ct;->h:Z

    return-void
.end method

.method public final a(ZI)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->i()Z

    move-result v1

    new-instance v0, Lcom/google/android/gms/internal/dr;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/cr;->d()Lcom/google/android/gms/internal/ay;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/ay;->e:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/internal/ct;->e:Lcom/google/android/gms/internal/bf;

    iget-object v3, p0, Lcom/google/android/gms/internal/ct;->k:Lcom/google/android/gms/internal/bl;

    iget-object v4, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    iget-object v5, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v5}, Lcom/google/android/gms/internal/cr;->h()Lcom/google/android/gms/internal/gs;

    move-result-object v7

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/internal/dr;-><init>(Lcom/google/android/gms/internal/ig;Lcom/google/android/gms/internal/bf;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/cr;ZILcom/google/android/gms/internal/gs;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ct;->a(Lcom/google/android/gms/internal/dr;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->d:Lcom/google/android/gms/internal/ig;

    goto :goto_0
.end method

.method public final a(ZILjava/lang/String;)V
    .locals 11

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->i()Z

    move-result v3

    new-instance v0, Lcom/google/android/gms/internal/dr;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/cr;->d()Lcom/google/android/gms/internal/ay;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/ay;->e:Z

    if-nez v1, :cond_0

    move-object v1, v2

    :goto_0
    if-eqz v3, :cond_1

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/internal/ct;->g:Lcom/google/android/gms/internal/w;

    iget-object v4, p0, Lcom/google/android/gms/internal/ct;->k:Lcom/google/android/gms/internal/bl;

    iget-object v5, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    iget-object v6, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v6}, Lcom/google/android/gms/internal/cr;->h()Lcom/google/android/gms/internal/gs;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/gms/internal/ct;->i:Lcom/google/android/gms/internal/aj;

    move v6, p1

    move v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/internal/dr;-><init>(Lcom/google/android/gms/internal/ig;Lcom/google/android/gms/internal/bf;Lcom/google/android/gms/internal/w;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/cr;ZILjava/lang/String;Lcom/google/android/gms/internal/gs;Lcom/google/android/gms/internal/aj;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ct;->a(Lcom/google/android/gms/internal/dr;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->d:Lcom/google/android/gms/internal/ig;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/internal/ct;->e:Lcom/google/android/gms/internal/bf;

    goto :goto_1
.end method

.method public final a(ZILjava/lang/String;Ljava/lang/String;)V
    .locals 12

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->i()Z

    move-result v2

    new-instance v0, Lcom/google/android/gms/internal/dr;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/cr;->d()Lcom/google/android/gms/internal/ay;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/internal/ay;->e:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/internal/ct;->g:Lcom/google/android/gms/internal/w;

    iget-object v4, p0, Lcom/google/android/gms/internal/ct;->k:Lcom/google/android/gms/internal/bl;

    iget-object v5, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    iget-object v6, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v6}, Lcom/google/android/gms/internal/cr;->h()Lcom/google/android/gms/internal/gs;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/gms/internal/ct;->i:Lcom/google/android/gms/internal/aj;

    move v6, p1

    move v7, p2

    move-object v8, p3

    move-object/from16 v9, p4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/internal/dr;-><init>(Lcom/google/android/gms/internal/ig;Lcom/google/android/gms/internal/bf;Lcom/google/android/gms/internal/w;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/cr;ZILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/gs;Lcom/google/android/gms/internal/aj;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ct;->a(Lcom/google/android/gms/internal/dr;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->d:Lcom/google/android/gms/internal/ig;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/internal/ct;->e:Lcom/google/android/gms/internal/bf;

    goto :goto_1
.end method

.method public b()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ct;->j:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->c:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ct;->h:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ct;->j:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->c()Lcom/google/android/gms/internal/az;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/co;->a()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/gms/internal/co;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/internal/cu;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/internal/cu;-><init>(Lcom/google/android/gms/internal/ct;Lcom/google/android/gms/internal/az;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/internal/az;->k()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Loading resource: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/cp;->d(Ljava/lang/String;)V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "gmsg"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mobileads.google.com"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/ct;->b(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->f:Lcom/google/android/gms/internal/cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->f:Lcom/google/android/gms/internal/cv;

    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/cv;->a(Lcom/google/android/gms/internal/cr;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/ct;->f:Lcom/google/android/gms/internal/cv;

    :cond_0
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 8

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdWebView shouldOverrideUrlLoading: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/cp;->d(Ljava/lang/String;)V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "gmsg"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mobileads.google.com"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/ct;->b(Landroid/net/Uri;)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/internal/ct;->h:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    if-ne p1, v1, :cond_1

    invoke-static {v0}, Lcom/google/android/gms/internal/ct;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/cr;->willNotDraw()Z

    move-result v1

    if-nez v1, :cond_5

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/cr;->g()Lcom/google/android/gms/internal/eq;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/eq;->b(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/internal/ct;->a:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/cr;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/internal/eq;->a(Landroid/net/Uri;Landroid/content/Context;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/google/android/gms/internal/fc; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_2
    move-object v2, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->m:Lcom/google/android/gms/internal/ih;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->m:Lcom/google/android/gms/internal/ih;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ih;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    new-instance v0, Lcom/google/android/gms/internal/do;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/internal/do;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ct;->a(Lcom/google/android/gms/internal/do;)V

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to append parameter to URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/internal/cp;->e(Ljava/lang/String;)V

    move-object v2, v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/ct;->m:Lcom/google/android/gms/internal/ih;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/internal/ih;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdWebView unable to handle URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/cp;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
