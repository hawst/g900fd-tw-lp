.class Lcom/google/android/gms/maps/r;
.super Lcom/google/android/gms/b/b;


# instance fields
.field protected a:Lcom/google/android/gms/b/n;

.field private final b:Landroid/support/v4/app/Fragment;

.field private c:Landroid/app/Activity;

.field private final d:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/b/b;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/maps/r;->d:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/maps/r;->b:Landroid/support/v4/app/Fragment;

    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/maps/r;->c:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/r;->g()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/maps/r;Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/maps/r;->a(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/b/n;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/maps/r;->a:Lcom/google/android/gms/b/n;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/r;->g()V

    return-void
.end method

.method public g()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/maps/r;->c:Landroid/app/Activity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/maps/r;->a:Lcom/google/android/gms/b/n;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/maps/r;->a()Lcom/google/android/gms/b/a;

    move-result-object v0

    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/r;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/n;->a(Landroid/content/Context;)I

    iget-object v0, p0, Lcom/google/android/gms/maps/r;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/cu;->a(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/r;->c:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/gms/b/m;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/j;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/al;->b(Lcom/google/android/gms/b/j;)Lcom/google/android/gms/maps/internal/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/r;->a:Lcom/google/android/gms/b/n;

    new-instance v2, Lcom/google/android/gms/maps/p;

    iget-object v3, p0, Lcom/google/android/gms/maps/r;->b:Landroid/support/v4/app/Fragment;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/maps/p;-><init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/maps/internal/j;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/b/n;->a(Lcom/google/android/gms/b/a;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/o;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/r;->a()Lcom/google/android/gms/b/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/p;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/p;->a(Lcom/google/android/gms/maps/o;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/a; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/e;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/e;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/maps/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/common/a; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return-void

    :catch_1
    move-exception v0

    goto :goto_1
.end method
