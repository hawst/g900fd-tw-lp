.class public Lcom/a/a;
.super Ljava/lang/Object;
.source "APS.java"

# interfaces
.implements Lcom/a/am;


# static fields
.field private static e:Lcom/a/a;


# instance fields
.field private A:J

.field private B:Lcom/a/an;

.field private C:I

.field private D:Ljava/lang/String;

.field private E:Lcom/a/ba;

.field private F:Lcom/a/bg;

.field private G:Ljava/lang/StringBuilder;

.field private H:J

.field private I:J

.field private J:Landroid/telephony/CellLocation;

.field private K:Z

.field a:Ljava/util/TimerTask;

.field b:Ljava/util/Timer;

.field c:Lcom/a/bg;

.field d:I

.field private f:Landroid/content/Context;

.field private g:I

.field private h:Landroid/net/ConnectivityManager;

.field private i:Landroid/net/wifi/WifiManager;

.field private j:Landroid/telephony/TelephonyManager;

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;

.field private m:Ljava/util/Map;

.field private n:Lcom/a/ae;

.field private o:Landroid/telephony/PhoneStateListener;

.field private p:I

.field private q:Lcom/a/d;

.field private r:Landroid/net/wifi/WifiInfo;

.field private s:Lorg/json/JSONObject;

.field private t:Ljava/lang/String;

.field private u:Lcom/a/af;

.field private v:J

.field private w:Z

.field private x:Z

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/a/a;->e:Lcom/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v1, p0, Lcom/a/a;->f:Landroid/content/Context;

    .line 52
    const/16 v0, 0x9

    iput v0, p0, Lcom/a/a;->g:I

    .line 53
    iput-object v1, p0, Lcom/a/a;->h:Landroid/net/ConnectivityManager;

    .line 54
    iput-object v1, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    .line 55
    iput-object v1, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    .line 70
    new-instance v0, Lcom/a/ae;

    invoke-direct {v0}, Lcom/a/ae;-><init>()V

    iput-object v0, p0, Lcom/a/a;->n:Lcom/a/ae;

    .line 71
    iput-object v1, p0, Lcom/a/a;->o:Landroid/telephony/PhoneStateListener;

    .line 72
    const/16 v0, -0x71

    iput v0, p0, Lcom/a/a;->p:I

    .line 73
    new-instance v0, Lcom/a/d;

    invoke-direct {v0, p0, v1}, Lcom/a/d;-><init>(Lcom/a/a;Lcom/a/b;)V

    iput-object v0, p0, Lcom/a/a;->q:Lcom/a/d;

    .line 74
    iput-object v1, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    .line 75
    iput-object v1, p0, Lcom/a/a;->s:Lorg/json/JSONObject;

    .line 76
    iput-object v1, p0, Lcom/a/a;->t:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/a/a;->u:Lcom/a/af;

    .line 78
    iput-wide v2, p0, Lcom/a/a;->v:J

    .line 81
    iput-boolean v4, p0, Lcom/a/a;->w:Z

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/a;->x:Z

    .line 83
    iput-wide v2, p0, Lcom/a/a;->y:J

    .line 84
    iput-wide v2, p0, Lcom/a/a;->z:J

    .line 86
    iput-wide v2, p0, Lcom/a/a;->A:J

    .line 89
    invoke-static {}, Lcom/a/an;->a()Lcom/a/an;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a;->B:Lcom/a/an;

    .line 90
    iput v4, p0, Lcom/a/a;->C:I

    .line 92
    const-string v0, "00:00:00:00:00:00"

    iput-object v0, p0, Lcom/a/a;->D:Ljava/lang/String;

    .line 105
    iput-object v1, p0, Lcom/a/a;->E:Lcom/a/ba;

    .line 106
    iput-object v1, p0, Lcom/a/a;->F:Lcom/a/bg;

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    .line 140
    iput-wide v2, p0, Lcom/a/a;->H:J

    .line 272
    iput-wide v2, p0, Lcom/a/a;->I:J

    .line 706
    iput-object v1, p0, Lcom/a/a;->J:Landroid/telephony/CellLocation;

    .line 2739
    iput-boolean v4, p0, Lcom/a/a;->K:Z

    .line 3201
    iput v4, p0, Lcom/a/a;->d:I

    .line 119
    return-void
.end method

.method static synthetic a(Lcom/a/a;J)J
    .locals 1

    .prologue
    .line 49
    iput-wide p1, p0, Lcom/a/a;->y:J

    return-wide p1
.end method

.method static synthetic a(Lcom/a/a;Landroid/telephony/CellLocation;)Landroid/telephony/CellLocation;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/a/a;->J:Landroid/telephony/CellLocation;

    return-object p1
.end method

.method private a([BZ)Lcom/a/af;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 1949
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2065
    :cond_0
    :goto_0
    return-object v0

    .line 1954
    :cond_1
    new-instance v2, Lcom/a/ao;

    invoke-direct {v2}, Lcom/a/ao;-><init>()V

    .line 1958
    iget-object v0, p0, Lcom/a/a;->B:Lcom/a/an;

    iget-object v3, p0, Lcom/a/a;->f:Landroid/content/Context;

    iget-object v4, p0, Lcom/a/a;->s:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, v3, v4}, Lcom/a/an;->a([BLandroid/content/Context;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    .line 1965
    const-string v3, ""

    .line 1968
    iget-object v3, p0, Lcom/a/a;->s:Lorg/json/JSONObject;

    invoke-static {v3}, Lcom/a/an;->a(Lorg/json/JSONObject;)[Ljava/lang/String;

    move-result-object v3

    .line 1969
    if-eqz v0, :cond_3

    const-string v4, "<saps>"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 1980
    invoke-virtual {v2, v0}, Lcom/a/ao;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1981
    iget-object v3, p0, Lcom/a/a;->n:Lcom/a/ae;

    const-string v4, "GBK"

    invoke-virtual {v3, v0, v4}, Lcom/a/ae;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1996
    :goto_1
    invoke-virtual {v2, v0}, Lcom/a/ao;->b(Ljava/lang/String;)Lcom/a/af;

    move-result-object v0

    .line 1997
    iget-object v2, p0, Lcom/a/a;->E:Lcom/a/ba;

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 1998
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 2001
    :try_start_0
    invoke-virtual {v0}, Lcom/a/af;->l()Lorg/json/JSONObject;

    move-result-object v3

    .line 2002
    const-string v4, "eab"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2003
    const-string v5, "e"

    invoke-virtual {v2, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2004
    const-string v5, "d"

    const-string v6, "ctl"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2005
    const-string v5, "u"

    const-string v6, "suc"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2009
    iget-object v3, p0, Lcom/a/a;->E:Lcom/a/ba;

    iget-object v5, p0, Lcom/a/a;->F:Lcom/a/bg;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Lcom/a/ba;->a(Lcom/a/bg;Ljava/lang/String;)V

    .line 2010
    if-eqz v4, :cond_2

    .line 2011
    const-string v2, "0"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2015
    iget-object v2, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v2}, Lcom/a/ba;->c()V

    .line 2016
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/a/a;->E:Lcom/a/ba;

    .line 2017
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/a/a;->K:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2053
    :cond_2
    :goto_2
    invoke-static {v0}, Lcom/a/av;->a(Lcom/a/af;)Z

    move-result v2

    if-nez v2, :cond_6

    move-object v0, v1

    .line 2057
    goto/16 :goto_0

    .line 1986
    :cond_3
    aget-object v3, v3, v6

    const-string v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1987
    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "api return pure"

    aput-object v4, v3, v6

    invoke-static {v3}, Lcom/a/av;->a([Ljava/lang/Object;)V

    goto :goto_1

    .line 1989
    :cond_4
    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "aps return pure"

    aput-object v4, v3, v6

    invoke-static {v3}, Lcom/a/av;->a([Ljava/lang/Object;)V

    goto :goto_1

    .line 2018
    :cond_5
    :try_start_1
    const-string v2, "1"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    .line 2036
    :catch_0
    move-exception v2

    .line 2037
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    .line 2059
    :cond_6
    invoke-virtual {v0}, Lcom/a/af;->l()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    .line 2032
    :catch_1
    move-exception v2

    goto :goto_2
.end method

.method private a(Landroid/telephony/NeighboringCellInfo;)Lcom/a/ai;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2433
    invoke-static {}, Lcom/a/av;->b()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    .line 2456
    :goto_0
    return-object v0

    .line 2440
    :cond_0
    :try_start_0
    new-instance v1, Lcom/a/ai;

    invoke-direct {v1}, Lcom/a/ai;-><init>()V

    .line 2441
    iget-object v2, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-static {v2}, Lcom/a/av;->a(Landroid/telephony/TelephonyManager;)[Ljava/lang/String;

    move-result-object v2

    .line 2442
    const/4 v3, 0x0

    aget-object v3, v2, v3

    iput-object v3, v1, Lcom/a/ai;->a:Ljava/lang/String;

    .line 2443
    const/4 v3, 0x1

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/a/ai;->b:Ljava/lang/String;

    .line 2444
    invoke-virtual {p1}, Landroid/telephony/NeighboringCellInfo;->getLac()I

    move-result v2

    iput v2, v1, Lcom/a/ai;->c:I

    .line 2445
    invoke-virtual {p1}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v2

    iput v2, v1, Lcom/a/ai;->d:I

    .line 2446
    invoke-virtual {p1}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v2

    .line 2447
    invoke-static {v2}, Lcom/a/av;->a(I)I

    move-result v2

    iput v2, v1, Lcom/a/ai;->j:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 2448
    goto :goto_0

    .line 2449
    :catch_0
    move-exception v1

    .line 2450
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static declared-synchronized a()Lcom/a/am;
    .locals 2

    .prologue
    .line 128
    const-class v1, Lcom/a/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/a/a;->e:Lcom/a/a;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/a/a;

    invoke-direct {v0}, Lcom/a/a;-><init>()V

    sput-object v0, Lcom/a/a;->e:Lcom/a/a;

    .line 131
    :cond_0
    sget-object v0, Lcom/a/a;->e:Lcom/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(III)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3337
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 3338
    const-string v1, "e"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 3339
    const-string v1, "d"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 3340
    const-string v1, "u"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 3341
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/a/a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/a/a;->l:Ljava/util/List;

    return-object p1
.end method

.method private a(Landroid/telephony/CellLocation;)V
    .locals 1

    .prologue
    .line 908
    iget-boolean v0, p0, Lcom/a/a;->w:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    .line 912
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object p1

    .line 914
    :cond_0
    if-nez p1, :cond_2

    .line 939
    :cond_1
    :goto_0
    return-void

    .line 917
    :cond_2
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/a/av;->a(Landroid/telephony/CellLocation;Landroid/content/Context;)I

    move-result v0

    .line 918
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 923
    :pswitch_0
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    .line 924
    invoke-direct {p0, p1}, Lcom/a/a;->c(Landroid/telephony/CellLocation;)V

    goto :goto_0

    .line 931
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/a/a;->d(Landroid/telephony/CellLocation;)V

    goto :goto_0

    .line 918
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/a/a;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/a/a;->b(I)V

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, -0x1

    .line 2075
    if-nez p1, :cond_1

    .line 2113
    :cond_0
    return-void

    .line 2078
    :cond_1
    const/16 v1, 0x17

    new-array v1, v1, [Ljava/lang/String;

    .line 2079
    const-string v2, " phnum=\"\""

    aput-object v2, v1, v0

    .line 2080
    const/4 v2, 0x1

    const-string v3, " nettype=\"\""

    aput-object v3, v1, v2

    .line 2081
    const/4 v2, 0x2

    const-string v3, " nettype=\"UNKNOWN\""

    aput-object v3, v1, v2

    .line 2082
    const/4 v2, 0x3

    const-string v3, " inftype=\"\""

    aput-object v3, v1, v2

    .line 2083
    const/4 v2, 0x4

    const-string v3, "<macs><![CDATA[]]></macs>"

    aput-object v3, v1, v2

    .line 2084
    const/4 v2, 0x5

    const-string v3, "<nb></nb>"

    aput-object v3, v1, v2

    .line 2085
    const/4 v2, 0x6

    const-string v3, "<mmac><![CDATA[]]></mmac>"

    aput-object v3, v1, v2

    .line 2086
    const/4 v2, 0x7

    const-string v3, " gtype=\"0\""

    aput-object v3, v1, v2

    .line 2087
    const/16 v2, 0x8

    const-string v3, " glong=\"0.0\""

    aput-object v3, v1, v2

    .line 2088
    const/16 v2, 0x9

    const-string v3, " glat=\"0.0\""

    aput-object v3, v1, v2

    .line 2089
    const/16 v2, 0xa

    const-string v3, " precision=\"0.0\""

    aput-object v3, v1, v2

    .line 2090
    const/16 v2, 0xb

    const-string v3, " glong=\"0\""

    aput-object v3, v1, v2

    .line 2091
    const/16 v2, 0xc

    const-string v3, " glat=\"0\""

    aput-object v3, v1, v2

    .line 2092
    const/16 v2, 0xd

    const-string v3, " precision=\"0\""

    aput-object v3, v1, v2

    .line 2093
    const/16 v2, 0xe

    const-string v3, "<smac>null</smac>"

    aput-object v3, v1, v2

    .line 2094
    const/16 v2, 0xf

    const-string v3, "<smac>00:00:00:00:00:00</smac>"

    aput-object v3, v1, v2

    .line 2095
    const/16 v2, 0x10

    const-string v3, "<imei>000000000000000</imei>"

    aput-object v3, v1, v2

    .line 2096
    const/16 v2, 0x11

    const-string v3, "<imsi>000000000000000</imsi>"

    aput-object v3, v1, v2

    .line 2097
    const/16 v2, 0x12

    const-string v3, "<mcc>000</mcc>"

    aput-object v3, v1, v2

    .line 2098
    const/16 v2, 0x13

    const-string v3, "<mcc>0</mcc>"

    aput-object v3, v1, v2

    .line 2099
    const/16 v2, 0x14

    const-string v3, "<lac>0</lac>"

    aput-object v3, v1, v2

    .line 2100
    const/16 v2, 0x15

    const-string v3, "<cellid>0</cellid>"

    aput-object v3, v1, v2

    .line 2101
    const/16 v2, 0x16

    const-string v3, "<key></key>"

    aput-object v3, v1, v2

    .line 2102
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 2103
    :goto_1
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v6, :cond_2

    .line 2104
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 2105
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v4

    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2102
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2109
    :cond_3
    :goto_2
    const-string v0, "*<"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v6, :cond_0

    .line 2110
    const-string v0, "*<"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 2111
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private declared-synchronized a(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 1494
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 1531
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1500
    :cond_1
    :try_start_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1501
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1502
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 1503
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x14

    if-le v3, v4, :cond_2

    iget v3, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-direct {p0, v3}, Lcom/a/a;->a(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1501
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1509
    :cond_2
    iget-object v3, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 1510
    iget-object v3, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    const-string v4, "*"

    const-string v5, "."

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    .line 1514
    :goto_3
    iget v3, v0, Landroid/net/wifi/ScanResult;->level:I

    mul-int/lit8 v3, v3, 0x1e

    add-int/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1512
    :cond_3
    :try_start_2
    const-string v3, "null"

    iput-object v3, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    goto :goto_3

    .line 1517
    :cond_4
    new-instance v1, Ljava/util/TreeMap;

    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 1518
    invoke-virtual {v1, v2}, Ljava/util/TreeMap;->putAll(Ljava/util/Map;)V

    .line 1519
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 1520
    invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1521
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1522
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1523
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v4, 0x1d

    if-le v0, v4, :cond_5

    .line 1527
    :cond_6
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 1529
    invoke-virtual {v1}, Ljava/util/TreeMap;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private a(I)Z
    .locals 3

    .prologue
    const/16 v1, 0x14

    const/4 v0, 0x1

    .line 1540
    .line 1542
    const/16 v2, 0x14

    :try_start_0
    invoke-static {p1, v2}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1550
    :goto_0
    if-lt v1, v0, :cond_0

    :goto_1
    return v0

    .line 1543
    :catch_0
    move-exception v2

    .line 1548
    invoke-static {v2}, Lcom/a/av;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1550
    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(J)Z
    .locals 9

    .prologue
    .line 948
    const/4 v0, 0x0

    .line 949
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v4

    .line 950
    sub-long v2, v4, p1

    .line 954
    const-wide/16 v6, 0x12c

    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    .line 958
    const-wide/16 v2, 0x0

    .line 959
    iget-object v1, p0, Lcom/a/a;->u:Lcom/a/af;

    if-eqz v1, :cond_0

    .line 960
    iget-object v1, p0, Lcom/a/a;->u:Lcom/a/af;

    invoke-virtual {v1}, Lcom/a/af;->f()J

    move-result-wide v2

    sub-long v2, v4, v2

    .line 962
    :cond_0
    const-wide/16 v4, 0x2710

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 974
    :cond_1
    :goto_0
    return v0

    .line 967
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/net/wifi/WifiInfo;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1092
    const/4 v1, 0x1

    .line 1093
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1102
    :cond_0
    :goto_0
    return v0

    .line 1095
    :cond_1
    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1097
    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    const-string v3, "00:00:00:00:00:00"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1099
    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/a/a;)Z
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/a/a;->r()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/a/a;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/a/a;->w:Z

    return p1
.end method

.method private declared-synchronized a(Ljava/lang/Object;)[B
    .locals 17

    .prologue
    .line 1178
    monitor-enter p0

    :try_start_0
    new-instance v7, Lcom/a/at;

    invoke-direct {v7}, Lcom/a/at;-><init>()V

    .line 1179
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1180
    const-string v5, "0"

    .line 1181
    const-string v8, "0"

    .line 1182
    const-string v9, "0"

    .line 1183
    const-string v10, "0"

    .line 1184
    const-string v11, "0"

    .line 1185
    const-string v2, ""

    .line 1186
    const-string v1, "888888888888888"

    sput-object v1, Lcom/a/aj;->a:Ljava/lang/String;

    .line 1187
    const-string v1, "888888888888888"

    sput-object v1, Lcom/a/aj;->b:Ljava/lang/String;

    .line 1188
    const-string v1, ""

    sput-object v1, Lcom/a/aj;->c:Ljava/lang/String;

    .line 1189
    const-string v4, ""

    .line 1190
    const-string v3, ""

    .line 1191
    const-string v1, ""

    .line 1192
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 1193
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 1194
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 1195
    move-object/from16 v0, p0

    iget v6, v0, Lcom/a/a;->g:I

    const/4 v15, 0x2

    if-ne v6, v15, :cond_15

    .line 1196
    const-string v5, "1"

    move-object v6, v5

    .line 1223
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    if-eqz v5, :cond_6

    .line 1224
    sget-object v5, Lcom/a/aj;->a:Ljava/lang/String;

    const-string v15, "888888888888888"

    invoke-virtual {v5, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_0

    .line 1226
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/a/aj;->a:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1234
    :cond_0
    :goto_1
    :try_start_2
    sget-object v5, Lcom/a/aj;->a:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 1235
    const-string v5, "888888888888888"

    sput-object v5, Lcom/a/aj;->a:Ljava/lang/String;

    .line 1237
    :cond_1
    sget-object v5, Lcom/a/aj;->b:Ljava/lang/String;

    if-eqz v5, :cond_2

    sget-object v5, Lcom/a/aj;->b:Ljava/lang/String;

    const-string v15, "888888888888888"

    invoke-virtual {v5, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1238
    :cond_2
    const-string v5, "888888888888888"

    sput-object v5, Lcom/a/aj;->b:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1240
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/a/aj;->b:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1248
    :cond_3
    :goto_2
    :try_start_4
    sget-object v5, Lcom/a/aj;->b:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 1249
    const-string v5, "888888888888888"

    sput-object v5, Lcom/a/aj;->b:Ljava/lang/String;

    .line 1252
    :cond_4
    sget-object v5, Lcom/a/aj;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1253
    const-string v5, ""

    sput-object v5, Lcom/a/aj;->c:Ljava/lang/String;

    .line 1260
    :cond_5
    sget-object v5, Lcom/a/aj;->c:Ljava/lang/String;

    if-nez v5, :cond_6

    .line 1261
    const-string v5, ""

    sput-object v5, Lcom/a/aj;->c:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1264
    :cond_6
    const/4 v5, 0x0

    .line 1266
    :try_start_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/a/a;->h:Landroid/net/ConnectivityManager;

    invoke-virtual {v15}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
    :try_end_5
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v5

    .line 1273
    :goto_3
    :try_start_6
    invoke-static {v5}, Lcom/a/an;->a(Landroid/net/NetworkInfo;)I

    move-result v5

    const/4 v15, -0x1

    if-eq v5, v15, :cond_b

    .line 1274
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-static {v3}, Lcom/a/an;->a(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v4

    .line 1275
    invoke-direct/range {p0 .. p0}, Lcom/a/a;->u()Z

    move-result v3

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/a/a;->a(Landroid/net/wifi/WifiInfo;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1276
    const-string v3, "2"

    .line 1286
    :cond_7
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->s:Lorg/json/JSONObject;

    invoke-static {v5}, Lcom/a/an;->a(Lorg/json/JSONObject;)[Ljava/lang/String;

    move-result-object v5

    .line 1287
    const/4 v15, 0x0

    aget-object v15, v5, v15

    const-string v16, "true"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1288
    const/4 v2, 0x1

    aget-object v2, v5, v2

    .line 1290
    :cond_8
    iput-object v6, v7, Lcom/a/at;->i:Ljava/lang/String;

    .line 1291
    iput-object v8, v7, Lcom/a/at;->j:Ljava/lang/String;

    .line 1292
    iput-object v9, v7, Lcom/a/at;->l:Ljava/lang/String;

    .line 1293
    iput-object v10, v7, Lcom/a/at;->m:Ljava/lang/String;

    .line 1294
    iput-object v11, v7, Lcom/a/at;->n:Ljava/lang/String;

    .line 1295
    sget-object v5, Lcom/a/aj;->d:Ljava/lang/String;

    iput-object v5, v7, Lcom/a/at;->c:Ljava/lang/String;

    .line 1296
    sget-object v5, Lcom/a/aj;->e:Ljava/lang/String;

    iput-object v5, v7, Lcom/a/at;->d:Ljava/lang/String;

    .line 1297
    iput-object v2, v7, Lcom/a/at;->o:Ljava/lang/String;

    .line 1298
    sget-object v5, Lcom/a/aj;->a:Ljava/lang/String;

    iput-object v5, v7, Lcom/a/at;->p:Ljava/lang/String;

    .line 1299
    sget-object v5, Lcom/a/aj;->c:Ljava/lang/String;

    iput-object v5, v7, Lcom/a/at;->s:Ljava/lang/String;

    .line 1300
    sget-object v5, Lcom/a/aj;->b:Ljava/lang/String;

    iput-object v5, v7, Lcom/a/at;->q:Ljava/lang/String;

    .line 1301
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->D:Ljava/lang/String;

    iput-object v5, v7, Lcom/a/at;->z:Ljava/lang/String;

    .line 1302
    iput-object v4, v7, Lcom/a/at;->t:Ljava/lang/String;

    .line 1303
    iput-object v3, v7, Lcom/a/at;->u:Ljava/lang/String;

    .line 1304
    invoke-static {}, Lcom/amap/api/location/core/c;->g()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lcom/a/at;->f:Ljava/lang/String;

    .line 1305
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "android"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/amap/api/location/core/c;->j()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lcom/a/at;->g:Ljava/lang/String;

    .line 1306
    invoke-static {}, Lcom/amap/api/location/core/c;->i()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lcom/a/at;->h:Ljava/lang/String;

    .line 1308
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v15, "<?xml version=\"1.0\" encoding=\""

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1309
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v15, "GBK"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, "\"?>"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1310
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v15, "<Cell_Req ver=\"3.0\"><HDR version=\"3.0\" cdma=\""

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1311
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1312
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v6, "\" gtype=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1313
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v6, "\" glong=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1314
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v6, "\" glat=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1315
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v6, "\" precision=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1316
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v6, "\"><src>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/a/aj;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1317
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v6, "</src><license>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/a/aj;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1318
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v6, "</license><key>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v5, "</key><clientid>"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v5, Lcom/a/aj;->f:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v5, "</clientid><imei>"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v5, Lcom/a/aj;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v5, "</imei><imsi>"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v5, Lcom/a/aj;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v5, "</imsi><smac>"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/a/a;->D:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v5, "</smac></HDR><DRR phnum=\""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v5, Lcom/a/aj;->c:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v5, "\" nettype=\""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v4, "\" inftype=\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_14

    .line 1327
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1328
    move-object/from16 v0, p0

    iget v2, v0, Lcom/a/a;->g:I

    packed-switch v2, :pswitch_data_0

    .line 1372
    :goto_5
    const/4 v2, 0x0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move-object v3, v1

    .line 1375
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/a/a;->u()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1376
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/a/a;->a(Landroid/net/wifi/WifiInfo;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1377
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1378
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1379
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "*"

    const-string v4, "."

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1381
    :cond_9
    const/4 v1, 0x0

    move v2, v1

    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_11

    .line 1382
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    .line 1383
    iget-object v4, v1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1384
    iget v1, v1, Landroid/net/wifi/ScanResult;->level:I

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1385
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "*"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1381
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_7

    .line 1278
    :cond_a
    const-string v3, "1"

    .line 1279
    invoke-direct/range {p0 .. p0}, Lcom/a/a;->u()Z

    move-result v5

    if-nez v5, :cond_7

    .line 1280
    invoke-direct/range {p0 .. p0}, Lcom/a/a;->p()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_4

    .line 1178
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1284
    :cond_b
    const/4 v5, 0x0

    :try_start_7
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    goto/16 :goto_4

    .line 1330
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->k:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/ai;

    .line 1331
    const/4 v2, 0x0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1332
    const-string v2, "<mcc>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/a/ai;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</mcc>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1333
    const-string v2, "<mnc>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/a/ai;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</mnc>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1334
    const-string v2, "<lac>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/a/ai;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</lac>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1335
    const-string v2, "<cellid>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/a/ai;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1336
    const-string v2, "</cellid>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1337
    const-string v2, "<signal>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Lcom/a/ai;->j:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1338
    const-string v1, "</signal>"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1339
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1340
    const/4 v1, 0x0

    move v3, v1

    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_e

    .line 1341
    if-nez v3, :cond_d

    .line 1340
    :cond_c
    :goto_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_8

    .line 1344
    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/ai;

    .line 1345
    iget v5, v1, Lcom/a/ai;->c:I

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1346
    iget v5, v1, Lcom/a/ai;->d:I

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1347
    iget v1, v1, Lcom/a/ai;->j:I

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1348
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v3, v1, :cond_c

    .line 1349
    const-string v1, "*"

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    :cond_e
    move-object v1, v2

    .line 1353
    goto/16 :goto_5

    .line 1355
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->k:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/ai;

    .line 1356
    const/4 v2, 0x0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1357
    const-string v2, "<mcc>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/a/ai;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</mcc>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1358
    const-string v2, "<sid>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/a/ai;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</sid>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1359
    const-string v2, "<nid>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/a/ai;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</nid>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1360
    const-string v2, "<bid>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/a/ai;->i:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</bid>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1361
    iget v2, v1, Lcom/a/ai;->f:I

    if-lez v2, :cond_f

    iget v2, v1, Lcom/a/ai;->e:I

    if-lez v2, :cond_f

    .line 1362
    const-string v2, "<lon>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/a/ai;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</lon>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1363
    const-string v2, "<lat>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/a/ai;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</lat>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1365
    :cond_f
    const-string v2, "<signal>"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Lcom/a/ai;->j:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</signal>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1366
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 1388
    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/a/a;->p()V

    .line 1393
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1394
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v2, "<nb>%s</nb>"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v12, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1395
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_13

    .line 1396
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v2, "<macs><![CDATA[%s]]></macs>"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v14, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1401
    :goto_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v2, "<mmac><![CDATA[%s]]></mmac>"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v14, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1402
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v2, "</DRR></Cell_Req>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1403
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/a/a;->a(Ljava/lang/StringBuilder;)V

    .line 1405
    iput-object v3, v7, Lcom/a/at;->w:Ljava/lang/String;

    .line 1406
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/a/at;->x:Ljava/lang/String;

    .line 1407
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/a/at;->z:Ljava/lang/String;

    .line 1408
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/a/at;->y:Ljava/lang/String;

    .line 1409
    move-object/from16 v0, p0

    iget v1, v0, Lcom/a/a;->g:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/a/at;->v:Ljava/lang/String;

    .line 1436
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->E:Lcom/a/ba;

    if-eqz v1, :cond_12

    move-object/from16 v0, p0

    iget v1, v0, Lcom/a/a;->C:I

    if-ltz v1, :cond_12

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/a/a;->x:Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v1, :cond_12

    .line 1440
    :try_start_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v1}, Lcom/a/ba;->d()Lcom/a/bg;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/a/a;->F:Lcom/a/bg;

    .line 1441
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->F:Lcom/a/bg;

    if-eqz v1, :cond_12

    .line 1442
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->F:Lcom/a/bg;

    invoke-virtual {v1}, Lcom/a/bg;->a()[B

    move-result-object v1

    .line 1443
    array-length v2, v1

    if-lez v2, :cond_12

    .line 1445
    array-length v2, v1

    new-array v2, v2, [B

    iput-object v2, v7, Lcom/a/at;->A:[B

    .line 1446
    const/4 v2, 0x0

    iget-object v3, v7, Lcom/a/at;->A:[B

    const/4 v4, 0x0

    array-length v5, v1

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0xb

    const-string v4, "<COR><inf>"

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1451
    invoke-static {v1}, Lcom/a/ae;->a([B)Ljava/lang/String;

    move-result-object v1

    .line 1452
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0xb

    invoke-virtual {v2, v3, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1453
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0xb

    const-string v3, "</inf></COR>"

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1478
    :cond_12
    :goto_b
    const/4 v1, 0x0

    :try_start_9
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v12, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1480
    const/4 v1, 0x0

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v13, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1482
    const/4 v1, 0x0

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v14, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1484
    invoke-virtual {v7}, Lcom/a/at;->a()[B
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 1398
    :cond_13
    :try_start_a
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1399
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    const-string v2, "<macs><![CDATA[%s]]></macs>"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v13, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1463
    :catch_0
    move-exception v1

    .line 1464
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_b

    .line 1267
    :catch_1
    move-exception v15

    goto/16 :goto_3

    .line 1241
    :catch_2
    move-exception v5

    goto/16 :goto_2

    .line 1227
    :catch_3
    move-exception v5

    goto/16 :goto_1

    :cond_14
    move-object v3, v1

    goto/16 :goto_6

    :cond_15
    move-object v6, v5

    goto/16 :goto_0

    .line 1328
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/a/a;I)I
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lcom/a/a;->p:I

    return p1
.end method

.method static synthetic b(Lcom/a/a;)Landroid/telephony/TelephonyManager;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private b(Landroid/telephony/CellLocation;)Lcom/a/ai;
    .locals 3

    .prologue
    .line 2411
    check-cast p1, Landroid/telephony/gsm/GsmCellLocation;

    .line 2412
    new-instance v0, Lcom/a/ai;

    invoke-direct {v0}, Lcom/a/ai;-><init>()V

    .line 2413
    iget-object v1, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-static {v1}, Lcom/a/av;->a(Landroid/telephony/TelephonyManager;)[Ljava/lang/String;

    move-result-object v1

    .line 2414
    const/4 v2, 0x0

    aget-object v2, v1, v2

    iput-object v2, v0, Lcom/a/ai;->a:Ljava/lang/String;

    .line 2415
    const/4 v2, 0x1

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/a/ai;->b:Ljava/lang/String;

    .line 2416
    invoke-virtual {p1}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v1

    iput v1, v0, Lcom/a/ai;->c:I

    .line 2417
    invoke-virtual {p1}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v1

    iput v1, v0, Lcom/a/ai;->d:I

    .line 2418
    iget v1, p0, Lcom/a/a;->p:I

    iput v1, v0, Lcom/a/ai;->j:I

    .line 2423
    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    const/16 v0, -0x71

    .line 2165
    if-ne p1, v0, :cond_1

    .line 2166
    iput v0, p0, Lcom/a/a;->p:I

    .line 2180
    :cond_0
    :goto_0
    return-void

    .line 2169
    :cond_1
    iput p1, p0, Lcom/a/a;->p:I

    .line 2170
    iget v0, p0, Lcom/a/a;->g:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2173
    :pswitch_0
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2174
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/ai;

    iget v1, p0, Lcom/a/a;->p:I

    iput v1, v0, Lcom/a/ai;->j:I

    goto :goto_0

    .line 2170
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/a/a;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/a/a;->x:Z

    return p1
.end method

.method static synthetic c(Lcom/a/a;)Landroid/telephony/CellLocation;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/a;->J:Landroid/telephony/CellLocation;

    return-object v0
.end method

.method private c(I)V
    .locals 6

    .prologue
    .line 3126
    :try_start_0
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/a/a;->H:J

    sub-long/2addr v0, v2

    .line 3127
    const-wide/32 v2, 0xafc8

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 3159
    :cond_0
    :goto_0
    return-void

    .line 3136
    :cond_1
    invoke-virtual {p0}, Lcom/a/a;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v0}, Lcom/a/ba;->f()I

    move-result v0

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 3140
    :cond_2
    invoke-direct {p0}, Lcom/a/a;->z()V

    .line 3141
    iget-object v0, p0, Lcom/a/a;->a:Ljava/util/TimerTask;

    if-nez v0, :cond_3

    .line 3142
    new-instance v0, Lcom/a/c;

    invoke-direct {v0, p0, p1}, Lcom/a/c;-><init>(Lcom/a/a;I)V

    iput-object v0, p0, Lcom/a/a;->a:Ljava/util/TimerTask;

    .line 3149
    :cond_3
    iget-object v0, p0, Lcom/a/a;->b:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 3150
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/a/a;->b:Ljava/util/Timer;

    .line 3151
    iget-object v0, p0, Lcom/a/a;->b:Ljava/util/Timer;

    iget-object v1, p0, Lcom/a/a;->a:Ljava/util/TimerTask;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0xbb8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3153
    :catch_0
    move-exception v0

    .line 3154
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private c(Landroid/telephony/CellLocation;)V
    .locals 7

    .prologue
    const/16 v6, 0x9

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 2466
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_1

    .line 2501
    :cond_0
    :goto_0
    return-void

    .line 2469
    :cond_1
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move-object v0, p1

    .line 2470
    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    .line 2471
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 2472
    iput v6, p0, Lcom/a/a;->g:I

    .line 2474
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "case 1,gsm illegal"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/a/av;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 2476
    :cond_2
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v1

    if-eq v1, v3, :cond_3

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v1

    const v2, 0xffff

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v0

    const v1, 0xfffffff

    if-ne v0, v1, :cond_4

    .line 2478
    :cond_3
    iput v6, p0, Lcom/a/a;->g:I

    .line 2479
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "case 2,gsm illegal"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/a/av;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 2482
    :cond_4
    iput v4, p0, Lcom/a/a;->g:I

    .line 2484
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/a/a;->b(Landroid/telephony/CellLocation;)Lcom/a/ai;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2485
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNeighboringCellInfo()Ljava/util/List;

    move-result-object v0

    .line 2486
    if-eqz v0, :cond_0

    .line 2492
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/NeighboringCellInfo;

    .line 2493
    invoke-virtual {v0}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v2

    if-eq v2, v3, :cond_5

    .line 2496
    invoke-direct {p0, v0}, Lcom/a/a;->a(Landroid/telephony/NeighboringCellInfo;)Lcom/a/ai;

    move-result-object v0

    .line 2497
    if-eqz v0, :cond_5

    .line 2498
    iget-object v2, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method static synthetic c(Lcom/a/a;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/a/a;->d(I)V

    return-void
.end method

.method static synthetic d(Lcom/a/a;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/a/a;->g:I

    return v0
.end method

.method private d(I)V
    .locals 5

    .prologue
    const v1, 0x282fffff

    const v0, 0x42fffff

    .line 3203
    invoke-virtual {p0}, Lcom/a/a;->f()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3287
    :cond_0
    :goto_0
    return-void

    .line 3210
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/a/a;->y()V

    .line 3212
    packed-switch p1, :pswitch_data_0

    .line 3232
    :goto_1
    :pswitch_0
    iget-object v1, p0, Lcom/a/a;->E:Lcom/a/ba;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-direct {p0, v3, v0, v4}, Lcom/a/a;->a(III)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/a/ba;->a(Lcom/a/bg;Ljava/lang/String;)V

    .line 3233
    iget-object v1, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v1}, Lcom/a/ba;->d()Lcom/a/bg;

    move-result-object v1

    iput-object v1, p0, Lcom/a/a;->c:Lcom/a/bg;

    .line 3234
    iget-object v1, p0, Lcom/a/a;->c:Lcom/a/bg;

    if-eqz v1, :cond_2

    .line 3235
    iget-object v1, p0, Lcom/a/a;->c:Lcom/a/bg;

    invoke-virtual {v1}, Lcom/a/bg;->a()[B

    move-result-object v1

    .line 3239
    iget-object v2, p0, Lcom/a/a;->B:Lcom/a/an;

    iget-object v3, p0, Lcom/a/a;->f:Landroid/content/Context;

    invoke-virtual {v2, v1, v3}, Lcom/a/an;->a([BLandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3248
    invoke-virtual {p0}, Lcom/a/a;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3249
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3250
    iget-object v1, p0, Lcom/a/a;->E:Lcom/a/ba;

    iget-object v2, p0, Lcom/a/a;->c:Lcom/a/bg;

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-direct {p0, v3, v0, v4}, Lcom/a/a;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/a/ba;->a(Lcom/a/bg;Ljava/lang/String;)V

    .line 3260
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/a/a;->z()V

    .line 3266
    invoke-virtual {p0}, Lcom/a/a;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v0}, Lcom/a/ba;->f()I

    move-result v0

    if-nez v0, :cond_5

    .line 3273
    invoke-direct {p0}, Lcom/a/a;->x()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3283
    :catch_0
    move-exception v0

    .line 3284
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 3285
    invoke-static {v0}, Lcom/a/av;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 3218
    goto :goto_1

    .line 3220
    :pswitch_2
    :try_start_1
    invoke-direct {p0}, Lcom/a/a;->o()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 3224
    goto :goto_1

    .line 3226
    :cond_3
    const v0, 0x7c2fffff

    .line 3228
    goto :goto_1

    .line 3252
    :cond_4
    iget v1, p0, Lcom/a/a;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/a;->d:I

    .line 3253
    iget-object v1, p0, Lcom/a/a;->E:Lcom/a/ba;

    iget-object v2, p0, Lcom/a/a;->c:Lcom/a/bg;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, v3, v0, v4}, Lcom/a/a;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/a/ba;->a(Lcom/a/bg;Ljava/lang/String;)V

    goto :goto_2

    .line 3274
    :cond_5
    iget v0, p0, Lcom/a/a;->d:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 3281
    invoke-direct {p0}, Lcom/a/a;->x()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 3212
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private d(Landroid/telephony/CellLocation;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2510
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2511
    invoke-static {}, Lcom/a/av;->b()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 2555
    :goto_0
    return-void

    .line 2518
    :cond_0
    :try_start_0
    check-cast p1, Landroid/telephony/cdma/CdmaCellLocation;

    .line 2519
    invoke-virtual {p1}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v0

    if-gtz v0, :cond_1

    .line 2520
    const/16 v0, 0x9

    iput v0, p0, Lcom/a/a;->g:I

    .line 2521
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "cdma illegal"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/a/av;->a([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2549
    :catch_0
    move-exception v0

    .line 2550
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 2523
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 2524
    const/16 v0, 0x9

    iput v0, p0, Lcom/a/a;->g:I

    .line 2525
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "cdma illegal"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/a/av;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 2527
    :cond_2
    invoke-virtual {p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 2528
    const/16 v0, 0x9

    iput v0, p0, Lcom/a/a;->g:I

    .line 2529
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "cdma illegal"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/a/av;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 2532
    :cond_3
    const/4 v0, 0x2

    iput v0, p0, Lcom/a/a;->g:I

    .line 2533
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-static {v0}, Lcom/a/av;->a(Landroid/telephony/TelephonyManager;)[Ljava/lang/String;

    move-result-object v0

    .line 2534
    new-instance v1, Lcom/a/ai;

    invoke-direct {v1}, Lcom/a/ai;-><init>()V

    .line 2535
    const/4 v2, 0x0

    aget-object v2, v0, v2

    iput-object v2, v1, Lcom/a/ai;->a:Ljava/lang/String;

    .line 2536
    const/4 v2, 0x1

    aget-object v0, v0, v2

    iput-object v0, v1, Lcom/a/ai;->b:Ljava/lang/String;

    .line 2537
    invoke-virtual {p1}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v0

    iput v0, v1, Lcom/a/ai;->g:I

    .line 2538
    invoke-virtual {p1}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v0

    iput v0, v1, Lcom/a/ai;->h:I

    .line 2539
    invoke-virtual {p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v0

    iput v0, v1, Lcom/a/ai;->i:I

    .line 2540
    iget v0, p0, Lcom/a/a;->p:I

    iput v0, v1, Lcom/a/ai;->j:I

    .line 2541
    invoke-virtual {p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLatitude()I

    move-result v0

    iput v0, v1, Lcom/a/ai;->e:I

    .line 2542
    invoke-virtual {p1}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLongitude()I

    move-result v0

    iput v0, v1, Lcom/a/ai;->f:I

    .line 2543
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/a/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/a/a;)Landroid/net/wifi/WifiManager;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic g(Lcom/a/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    return-object v0
.end method

.method private static g()V
    .locals 1

    .prologue
    .line 665
    const/4 v0, 0x0

    sput-object v0, Lcom/a/a;->e:Lcom/a/a;

    .line 666
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 685
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-static {v0, v1}, Lcom/a/av;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    .line 686
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 687
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 688
    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 689
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 690
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 691
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 692
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 693
    const-string v1, "android.location.GPS_FIX_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 694
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 695
    const-string v1, "android.location.PROVIDERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 696
    iget-object v1, p0, Lcom/a/a;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/a/a;->q:Lcom/a/d;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 697
    invoke-direct {p0}, Lcom/a/a;->q()V

    .line 698
    return-void
.end method

.method static synthetic h(Lcom/a/a;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/a/a;->p()V

    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/4 v1, 0x2

    .line 709
    const-string v0, "connectivity"

    .line 710
    iget-object v2, p0, Lcom/a/a;->f:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/a/av;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/a/a;->h:Landroid/net/ConnectivityManager;

    .line 711
    invoke-static {}, Landroid/telephony/CellLocation;->requestLocationUpdate()V

    .line 712
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/a;->z:J

    .line 713
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    const-string v2, "phone"

    invoke-static {v0, v2}, Lcom/a/av;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    .line 714
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a;->J:Landroid/telephony/CellLocation;

    .line 715
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    .line 716
    packed-switch v0, :pswitch_data_0

    .line 724
    const/16 v0, 0x9

    iput v0, p0, Lcom/a/a;->g:I

    .line 728
    :goto_0
    new-instance v0, Lcom/a/b;

    invoke-direct {v0, p0}, Lcom/a/b;-><init>(Lcom/a/a;)V

    iput-object v0, p0, Lcom/a/a;->o:Landroid/telephony/PhoneStateListener;

    .line 847
    invoke-static {}, Lcom/a/av;->b()I

    move-result v0

    const/4 v2, 0x7

    if-ge v0, v2, :cond_0

    move v0, v1

    .line 862
    :goto_1
    if-nez v0, :cond_1

    .line 863
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/a/a;->o:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 873
    :goto_2
    return-void

    .line 718
    :pswitch_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/a/a;->g:I

    goto :goto_0

    .line 721
    :pswitch_1
    iput v1, p0, Lcom/a/a;->g:I

    goto :goto_0

    .line 856
    :cond_0
    const/16 v0, 0x100

    goto :goto_1

    .line 867
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/a/a;->o:Landroid/telephony/PhoneStateListener;

    or-int/2addr v0, v4

    invoke-virtual {v1, v2, v0}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 868
    :catch_0
    move-exception v0

    .line 869
    invoke-static {v0}, Lcom/a/av;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 716
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic i(Lcom/a/a;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/a/a;->q()V

    return-void
.end method

.method static synthetic j(Lcom/a/a;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/a/a;->C:I

    return v0
.end method

.method private j()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 993
    invoke-direct {p0}, Lcom/a/a;->w()V

    .line 994
    const-string v0, ""

    .line 995
    const-string v2, ""

    .line 999
    const-string v2, "network"

    .line 1001
    invoke-direct {p0}, Lcom/a/a;->u()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1002
    iget-object v3, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    .line 1006
    :goto_0
    const-string v3, ""

    .line 1010
    iget v3, p0, Lcom/a/a;->g:I

    sparse-switch v3, :sswitch_data_0

    :cond_0
    move-object v1, v0

    .line 1082
    :cond_1
    :goto_1
    return-object v1

    .line 1004
    :cond_2
    invoke-direct {p0}, Lcom/a/a;->p()V

    goto :goto_0

    .line 1012
    :sswitch_0
    iget-object v1, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/ai;

    .line 1014
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1015
    iget-object v3, v0, Lcom/a/ai;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1016
    iget-object v3, v0, Lcom/a/ai;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1017
    iget v3, v0, Lcom/a/ai;->c:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1018
    iget v0, v0, Lcom/a/ai;->d:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "#"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1019
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1020
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 1021
    const-string v0, "cellwifi"

    .line 1025
    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1026
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1023
    :cond_3
    const-string v0, "cell"

    goto :goto_2

    .line 1032
    :sswitch_1
    iget-object v1, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1033
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/ai;

    .line 1034
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1035
    iget-object v3, v0, Lcom/a/ai;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1036
    iget-object v3, v0, Lcom/a/ai;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1037
    iget v3, v0, Lcom/a/ai;->g:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1038
    iget v3, v0, Lcom/a/ai;->h:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1039
    iget v0, v0, Lcom/a/ai;->i:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "#"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1040
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1041
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1042
    const-string v0, "cellwifi"

    .line 1046
    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1047
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 1044
    :cond_4
    const-string v0, "cell"

    goto :goto_3

    .line 1053
    :sswitch_2
    const-string v0, "#%s#"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1054
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_5

    iget-object v0, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-direct {p0, v0}, Lcom/a/a;->a(Landroid/net/wifi/WifiInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1057
    :cond_5
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1059
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_6

    iget-object v0, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-direct {p0, v0}, Lcom/a/a;->a(Landroid/net/wifi/WifiInfo;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1060
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 1061
    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v1

    :goto_4
    move-object v1, v0

    .line 1064
    goto/16 :goto_1

    .line 1065
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_7
    move-object v0, v2

    goto :goto_4

    .line 1010
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch
.end method

.method private k()Ljava/lang/StringBuilder;
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1116
    invoke-direct {p0}, Lcom/a/a;->w()V

    .line 1117
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v0, 0x2bc

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1118
    iget v0, p0, Lcom/a/a;->g:I

    packed-switch v0, :pswitch_data_0

    .line 1134
    :cond_0
    iget-object v0, p0, Lcom/a/a;->D:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/a/a;->D:Ljava/lang/String;

    const-string v1, "00:00:00:00:00:00"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135
    :cond_1
    iget-object v0, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    if-eqz v0, :cond_2

    .line 1136
    iget-object v0, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a;->D:Ljava/lang/String;

    .line 1137
    iget-object v0, p0, Lcom/a/a;->D:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1138
    const-string v0, "00:00:00:00:00:00"

    iput-object v0, p0, Lcom/a/a;->D:Ljava/lang/String;

    .line 1142
    :cond_2
    invoke-direct {p0}, Lcom/a/a;->u()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1143
    const-string v0, ""

    .line 1144
    iget-object v1, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-direct {p0, v1}, Lcom/a/a;->a(Landroid/net/wifi/WifiInfo;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1145
    iget-object v0, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    move v3, v2

    move v4, v2

    .line 1149
    :goto_1
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 1150
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget-object v7, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    .line 1151
    const-string v0, "nb"

    .line 1152
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1153
    const-string v0, "access"

    move v4, v5

    .line 1156
    :cond_3
    const-string v8, "#%s,%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v7, v9, v2

    aput-object v0, v9, v5

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1149
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :pswitch_0
    move v1, v2

    .line 1120
    :goto_2
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1121
    if-nez v1, :cond_4

    .line 1120
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1124
    :cond_4
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/ai;

    .line 1125
    const-string v3, "#"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/a/ai;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1126
    const-string v3, "|"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/a/ai;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1127
    const-string v3, "|"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Lcom/a/ai;->d:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1158
    :cond_5
    if-nez v4, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    .line 1159
    const-string v0, "#"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1160
    const-string v0, ",access"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1165
    :cond_6
    :goto_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 1166
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1168
    :cond_7
    return-object v6

    .line 1163
    :cond_8
    invoke-direct {p0}, Lcom/a/a;->p()V

    goto :goto_4

    :cond_9
    move-object v1, v0

    goto/16 :goto_0

    .line 1118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic k(Lcom/a/a;)Z
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/a/a;->o()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized l()[B
    .locals 2

    .prologue
    .line 1560
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/a/a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1561
    invoke-static {}, Landroid/telephony/CellLocation;->requestLocationUpdate()V

    .line 1562
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a;->z:J

    .line 1567
    :cond_0
    invoke-direct {p0}, Lcom/a/a;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1568
    invoke-direct {p0}, Lcom/a/a;->q()V

    .line 1570
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/a;->a(Ljava/lang/Object;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1642
    monitor-exit p0

    return-object v0

    .line 1560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private m()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1658
    const/4 v1, 0x1

    .line 1659
    iget-boolean v2, p0, Lcom/a/a;->w:Z

    if-eqz v2, :cond_1

    .line 1666
    :cond_0
    :goto_0
    return v0

    .line 1661
    :cond_1
    iget-wide v2, p0, Lcom/a/a;->z:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1663
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/a/a;->z:J

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/a/aj;->j:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private n()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1676
    const/4 v1, 0x1

    .line 1677
    invoke-direct {p0}, Lcom/a/a;->u()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1684
    :cond_0
    :goto_0
    return v0

    .line 1679
    :cond_1
    iget-wide v2, p0, Lcom/a/a;->A:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1681
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/a/a;->A:J

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/a/aj;->i:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private o()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1860
    iget-object v1, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    if-nez v1, :cond_1

    .line 1883
    :cond_0
    :goto_0
    return v0

    .line 1864
    :cond_1
    invoke-direct {p0}, Lcom/a/a;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1867
    :try_start_0
    iget-object v1, p0, Lcom/a/a;->h:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1868
    invoke-static {v1}, Lcom/a/an;->a(Landroid/net/NetworkInfo;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1869
    iget-object v1, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/a/a;->a(Landroid/net/wifi/WifiInfo;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1870
    const/4 v0, 0x1

    goto :goto_0

    .line 1875
    :catch_0
    move-exception v1

    .line 1876
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 1873
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 2564
    iget-object v0, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2565
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a;->r:Landroid/net/wifi/WifiInfo;

    .line 2566
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2590
    invoke-direct {p0}, Lcom/a/a;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2592
    :try_start_0
    iget-object v0, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 2596
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a;->A:J
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2603
    :cond_0
    :goto_0
    return-void

    .line 2597
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private r()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2696
    iget-wide v2, p0, Lcom/a/a;->y:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2703
    :cond_0
    :goto_0
    return v0

    .line 2699
    :cond_1
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/a/a;->y:J

    sub-long/2addr v2, v4

    .line 2703
    const-wide/16 v4, 0x7d0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private s()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2817
    iget-object v0, p0, Lcom/a/a;->u:Lcom/a/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-ge v0, v11, :cond_1

    .line 2865
    :cond_0
    return-void

    .line 2821
    :cond_1
    iget-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2822
    :cond_2
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2824
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2825
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    .line 2826
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2827
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2828
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2829
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/al;

    .line 2830
    invoke-virtual {v0}, Lcom/a/al;->a()J

    move-result-wide v6

    .line 2831
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_4

    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_3

    .line 2837
    :cond_4
    const/4 v6, 0x4

    new-array v6, v6, [D

    .line 2838
    iget-wide v8, v0, Lcom/a/al;->b:D

    aput-wide v8, v6, v10

    .line 2839
    iget-wide v8, v0, Lcom/a/al;->a:D

    aput-wide v8, v6, v11

    .line 2840
    const/4 v7, 0x2

    iget-object v8, p0, Lcom/a/a;->u:Lcom/a/af;

    invoke-virtual {v8}, Lcom/a/af;->d()D

    move-result-wide v8

    aput-wide v8, v6, v7

    .line 2841
    const/4 v7, 0x3

    iget-object v8, p0, Lcom/a/a;->u:Lcom/a/af;

    invoke-virtual {v8}, Lcom/a/af;->c()D

    move-result-wide v8

    aput-wide v8, v6, v7

    .line 2842
    invoke-static {v6}, Lcom/a/av;->a([D)F

    move-result v6

    .line 2846
    iget v7, v0, Lcom/a/al;->c:F

    cmpl-float v7, v6, v7

    if-gez v7, :cond_3

    .line 2849
    const-string v7, "distance"

    invoke-virtual {v4, v7, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 2850
    const-string v6, "fence"

    invoke-virtual {v0}, Lcom/a/al;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2851
    invoke-virtual {v3, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2856
    :try_start_0
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-virtual {v1, v0, v6, v3}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2857
    :catch_0
    move-exception v0

    .line 2858
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    .prologue
    const/16 v1, 0x9

    .line 2874
    iget v0, p0, Lcom/a/a;->g:I

    packed-switch v0, :pswitch_data_0

    .line 2894
    :cond_0
    :goto_0
    return-void

    .line 2876
    :pswitch_0
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2880
    iput v1, p0, Lcom/a/a;->g:I

    goto :goto_0

    .line 2884
    :pswitch_1
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2888
    iput v1, p0, Lcom/a/a;->g:I

    goto :goto_0

    .line 2874
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private u()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2946
    iget-object v1, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    if-nez v1, :cond_1

    .line 2973
    :cond_0
    :goto_0
    return v0

    .line 2951
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 2955
    :goto_1
    if-nez v0, :cond_0

    invoke-static {}, Lcom/a/av;->b()I

    move-result v1

    const/16 v2, 0x11

    if-le v1, v2, :cond_0

    .line 2960
    :try_start_1
    iget-object v1, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    const-string v2, "isScanAlwaysAvailable"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/a/as;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2961
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0

    .line 2967
    :catch_0
    move-exception v1

    goto :goto_0

    .line 2952
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private v()Lcom/a/af;
    .locals 3

    .prologue
    .line 3048
    invoke-direct {p0}, Lcom/a/a;->l()[B

    move-result-object v0

    .line 3049
    iget-object v1, p0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/a/a;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3050
    iget-object v1, p0, Lcom/a/a;->u:Lcom/a/af;

    if-eqz v1, :cond_1

    .line 3055
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a;->v:J

    .line 3056
    iget-object v0, p0, Lcom/a/a;->u:Lcom/a/af;

    .line 3072
    :goto_0
    return-object v0

    .line 3059
    :cond_0
    iget-object v1, p0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 3060
    iget-object v1, p0, Lcom/a/a;->G:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/a/a;->t:Ljava/lang/String;

    .line 3071
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/a/a;->a([BZ)Lcom/a/af;

    move-result-object v0

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 3079
    iget-boolean v0, p0, Lcom/a/a;->w:Z

    if-eqz v0, :cond_0

    .line 3083
    const/16 v0, 0x9

    iput v0, p0, Lcom/a/a;->g:I

    .line 3084
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 3088
    :goto_0
    return-void

    .line 3086
    :cond_0
    invoke-direct {p0}, Lcom/a/a;->t()V

    goto :goto_0
.end method

.method private x()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3167
    iget-object v0, p0, Lcom/a/a;->b:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 3168
    iget-object v0, p0, Lcom/a/a;->b:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 3169
    iput-object v1, p0, Lcom/a/a;->b:Ljava/util/Timer;

    .line 3171
    :cond_0
    iget-object v0, p0, Lcom/a/a;->a:Ljava/util/TimerTask;

    if-eqz v0, :cond_1

    .line 3172
    iget-object v0, p0, Lcom/a/a;->a:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 3173
    iput-object v1, p0, Lcom/a/a;->a:Ljava/util/TimerTask;

    .line 3175
    :cond_1
    return-void
.end method

.method private y()V
    .locals 2

    .prologue
    .line 3183
    invoke-virtual {p0}, Lcom/a/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3192
    :goto_0
    return-void

    .line 3187
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    const/16 v1, 0x300

    invoke-virtual {v0, v1}, Lcom/a/ba;->a(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3188
    :catch_0
    move-exception v0

    .line 3189
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 3190
    invoke-static {v0}, Lcom/a/av;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private z()V
    .locals 1

    .prologue
    .line 3303
    invoke-virtual {p0}, Lcom/a/a;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3325
    :cond_0
    :goto_0
    return-void

    .line 3306
    :cond_1
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v0}, Lcom/a/ba;->f()I

    move-result v0

    if-gtz v0, :cond_0

    .line 3310
    :try_start_0
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v0}, Lcom/a/ba;->e()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 3319
    :catch_0
    move-exception v0

    .line 3320
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a(ZI)I
    .locals 1

    .prologue
    .line 3109
    if-nez p1, :cond_0

    .line 3110
    invoke-direct {p0}, Lcom/a/a;->x()V

    .line 3114
    :goto_0
    invoke-virtual {p0}, Lcom/a/a;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v0}, Lcom/a/ba;->f()I

    move-result v0

    :goto_1
    return v0

    .line 3112
    :cond_0
    invoke-direct {p0, p2}, Lcom/a/a;->c(I)V

    goto :goto_0

    .line 3114
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 561
    if-nez p1, :cond_0

    .line 568
    :goto_0
    return-void

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 142
    if-nez p1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 159
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    .line 169
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    const-string v1, "in debug mode, only for test"

    invoke-static {v0, v1}, Lcom/a/av;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 182
    invoke-direct {p0}, Lcom/a/a;->h()V

    .line 183
    invoke-direct {p0}, Lcom/a/a;->i()V

    .line 184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a;->H:J

    goto :goto_0
.end method

.method public a(Lcom/a/al;Landroid/app/PendingIntent;)V
    .locals 4

    .prologue
    .line 526
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 529
    :cond_1
    invoke-virtual {p1}, Lcom/a/al;->a()J

    move-result-wide v0

    .line 530
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 539
    :cond_2
    iget-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 540
    iget-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 541
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542
    iget-object v1, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 544
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 545
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546
    iget-object v1, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 481
    if-eqz p1, :cond_0

    const-string v0, "##"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 502
    :cond_0
    :goto_0
    return-void

    .line 487
    :cond_1
    const-string v0, "##"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 488
    array-length v1, v0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 495
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Lcom/a/aj;->a(Ljava/lang/String;)V

    .line 496
    sget-object v1, Lcom/a/aj;->e:Ljava/lang/String;

    aget-object v2, v0, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 497
    invoke-static {}, Lcom/a/ag;->a()Lcom/a/ag;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/ag;->c()V

    .line 500
    :cond_2
    aget-object v1, v0, v3

    invoke-static {v1}, Lcom/a/aj;->b(Ljava/lang/String;)V

    .line 501
    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/aj;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Lcom/a/a;->s:Lorg/json/JSONObject;

    .line 516
    return-void
.end method

.method public declared-synchronized b()Lcom/a/af;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 275
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/a/a;->f:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 447
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 281
    :cond_1
    :try_start_1
    sget-object v1, Lcom/a/aj;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 286
    sget-object v1, Lcom/a/aj;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/a/a;->s:Lorg/json/JSONObject;

    invoke-static {v1}, Lcom/a/an;->a(Lorg/json/JSONObject;)[Ljava/lang/String;

    move-result-object v1

    .line 293
    const-string v2, "false"

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 294
    const-string v1, "AuthLocation"

    const-string v2, "key\u9274\u6743\u5931\u8d25"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 300
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/a/a;->m()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 301
    invoke-static {}, Landroid/telephony/CellLocation;->requestLocationUpdate()V

    .line 302
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/a;->z:J

    .line 307
    :cond_3
    invoke-direct {p0}, Lcom/a/a;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 308
    invoke-direct {p0}, Lcom/a/a;->q()V

    .line 310
    :cond_4
    iget v1, p0, Lcom/a/a;->C:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/a;->C:I

    .line 311
    iget v1, p0, Lcom/a/a;->C:I

    if-le v1, v4, :cond_5

    .line 312
    invoke-virtual {p0}, Lcom/a/a;->d()V

    .line 314
    :cond_5
    iget v1, p0, Lcom/a/a;->C:I

    if-ne v1, v4, :cond_6

    .line 315
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/a;->I:J

    .line 316
    iget-object v1, p0, Lcom/a/a;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/a/av;->a(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/a/a;->w:Z

    .line 317
    iget-object v1, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    if-eqz v1, :cond_6

    .line 318
    iget-object v1, p0, Lcom/a/a;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/a/a;->l:Ljava/util/List;

    .line 321
    :cond_6
    iget-object v1, p0, Lcom/a/a;->l:Ljava/util/List;

    if-nez v1, :cond_7

    .line 322
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/a/a;->l:Ljava/util/List;

    .line 324
    :cond_7
    iget v1, p0, Lcom/a/a;->C:I

    if-ne v1, v4, :cond_8

    invoke-direct {p0}, Lcom/a/a;->u()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-wide v2, p0, Lcom/a/a;->I:J

    iget-wide v4, p0, Lcom/a/a;->H:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7d0

    cmp-long v1, v2, v4

    if-gez v1, :cond_8

    .line 325
    const/4 v1, 0x4

    .line 326
    :goto_1
    if-lez v1, :cond_8

    .line 327
    iget-object v2, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_8

    .line 328
    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 329
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 338
    :cond_8
    iget-wide v2, p0, Lcom/a/a;->v:J

    invoke-direct {p0, v2, v3}, Lcom/a/a;->a(J)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 339
    iget-object v1, p0, Lcom/a/a;->u:Lcom/a/af;

    if-eqz v1, :cond_9

    .line 340
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a;->v:J

    .line 344
    iget-object v0, p0, Lcom/a/a;->u:Lcom/a/af;

    goto/16 :goto_0

    .line 348
    :cond_9
    iget-object v1, p0, Lcom/a/a;->J:Landroid/telephony/CellLocation;

    invoke-direct {p0, v1}, Lcom/a/a;->a(Landroid/telephony/CellLocation;)V

    .line 349
    iget-object v1, p0, Lcom/a/a;->l:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/a/a;->a(Ljava/util/List;)V

    .line 352
    invoke-direct {p0}, Lcom/a/a;->j()Ljava/lang/String;

    move-result-object v1

    .line 356
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 363
    invoke-direct {p0}, Lcom/a/a;->k()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 365
    invoke-static {}, Lcom/a/ag;->a()Lcom/a/ag;

    move-result-object v2

    const-string v3, "mem"

    invoke-virtual {v2, v1, v0, v3}, Lcom/a/ag;->a(Ljava/lang/String;Ljava/lang/StringBuilder;Ljava/lang/String;)Lcom/a/af;

    move-result-object v2

    .line 366
    if-nez v2, :cond_a

    .line 403
    invoke-direct {p0}, Lcom/a/a;->v()Lcom/a/af;

    move-result-object v2

    .line 405
    iput-object v2, p0, Lcom/a/a;->u:Lcom/a/af;

    .line 442
    :goto_2
    invoke-static {}, Lcom/a/ag;->a()Lcom/a/ag;

    move-result-object v2

    iget-object v3, p0, Lcom/a/a;->u:Lcom/a/af;

    iget-object v4, p0, Lcom/a/a;->f:Landroid/content/Context;

    invoke-virtual {v2, v1, v3, v0, v4}, Lcom/a/ag;->a(Ljava/lang/String;Lcom/a/af;Ljava/lang/StringBuilder;Landroid/content/Context;)V

    .line 443
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 445
    invoke-static {}, Lcom/a/av;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a;->v:J

    .line 446
    invoke-direct {p0}, Lcom/a/a;->s()V

    .line 447
    iget-object v0, p0, Lcom/a/a;->u:Lcom/a/af;

    goto/16 :goto_0

    .line 437
    :cond_a
    iput-object v2, p0, Lcom/a/a;->u:Lcom/a/af;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 586
    :try_start_0
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v0}, Lcom/a/ba;->c()V

    .line 588
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/a/a;->K:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 597
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/a/a;->F:Lcom/a/bg;

    .line 619
    :try_start_1
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 620
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/a/a;->q:Lcom/a/d;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627
    :cond_1
    iput-object v3, p0, Lcom/a/a;->q:Lcom/a/d;

    .line 629
    :goto_1
    invoke-direct {p0}, Lcom/a/a;->x()V

    .line 634
    :try_start_2
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/a/a;->o:Landroid/telephony/PhoneStateListener;

    if-eqz v0, :cond_2

    .line 635
    iget-object v0, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/a/a;->o:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 641
    :cond_2
    :goto_2
    invoke-static {}, Lcom/a/ag;->a()Lcom/a/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/ag;->c()V

    .line 642
    invoke-static {v4}, Lcom/a/aj;->a(Z)V

    .line 644
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/a/a;->v:J

    .line 645
    iget-object v0, p0, Lcom/a/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 646
    iget-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 647
    const/16 v0, -0x71

    iput v0, p0, Lcom/a/a;->p:I

    .line 648
    invoke-direct {p0}, Lcom/a/a;->p()V

    .line 649
    iput-object v3, p0, Lcom/a/a;->t:Ljava/lang/String;

    .line 650
    iput-object v3, p0, Lcom/a/a;->u:Lcom/a/af;

    .line 653
    iput-object v3, p0, Lcom/a/a;->f:Landroid/content/Context;

    .line 654
    iput-object v3, p0, Lcom/a/a;->j:Landroid/telephony/TelephonyManager;

    .line 659
    invoke-static {}, Lcom/a/a;->g()V

    .line 662
    return-void

    .line 590
    :catch_0
    move-exception v0

    .line 591
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 622
    :catch_1
    move-exception v0

    .line 627
    iput-object v3, p0, Lcom/a/a;->q:Lcom/a/d;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v3, p0, Lcom/a/a;->q:Lcom/a/d;

    throw v0

    .line 637
    :catch_2
    move-exception v0

    .line 638
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 639
    invoke-static {v0}, Lcom/a/av;->a(Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public d()V
    .locals 2

    .prologue
    .line 2747
    :try_start_0
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    if-nez v0, :cond_0

    .line 2748
    iget-object v0, p0, Lcom/a/a;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/ba;->a(Landroid/content/Context;)Lcom/a/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    .line 2749
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Lcom/a/ba;->a(I)V

    .line 2752
    :cond_0
    iget-boolean v0, p0, Lcom/a/a;->K:Z

    if-nez v0, :cond_1

    .line 2753
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/a;->K:Z

    .line 2754
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    invoke-virtual {v0}, Lcom/a/ba;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2764
    :cond_1
    :goto_0
    return-void

    .line 2757
    :catch_0
    move-exception v0

    .line 2758
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 2804
    iget-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 2805
    iget-object v0, p0, Lcom/a/a;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    .line 2807
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()Z
    .locals 1

    .prologue
    .line 3290
    iget-object v0, p0, Lcom/a/a;->E:Lcom/a/ba;

    if-nez v0, :cond_0

    .line 3291
    const/4 v0, 0x0

    .line 3293
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
