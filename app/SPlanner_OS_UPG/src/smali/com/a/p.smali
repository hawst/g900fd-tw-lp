.class final Lcom/a/p;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lcom/a/ba;


# direct methods
.method constructor <init>(Lcom/a/ba;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    invoke-static {}, Landroid/os/Looper;->prepare()V

    iget-object v0, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/ba;->a(Lcom/a/ba;Landroid/os/Looper;)Landroid/os/Looper;

    iget-object v0, p0, Lcom/a/p;->a:Lcom/a/ba;

    new-instance v1, Lcom/a/r;

    iget-object v2, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-direct {v1, v2}, Lcom/a/r;-><init>(Lcom/a/ba;)V

    invoke-static {v0, v1}, Lcom/a/ba;->a(Lcom/a/ba;Lcom/a/r;)Lcom/a/r;

    iget-object v0, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-static {v0}, Lcom/a/ba;->e(Lcom/a/ba;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-static {v1}, Lcom/a/ba;->d(Lcom/a/ba;)Lcom/a/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    iget-object v0, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-static {v0}, Lcom/a/ba;->e(Lcom/a/ba;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-static {v1}, Lcom/a/ba;->d(Lcom/a/ba;)Lcom/a/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addNmeaListener(Landroid/location/GpsStatus$NmeaListener;)Z

    iget-object v0, p0, Lcom/a/p;->a:Lcom/a/ba;

    new-instance v1, Lcom/a/q;

    invoke-direct {v1, p0}, Lcom/a/q;-><init>(Lcom/a/p;)V

    invoke-static {v0, v1}, Lcom/a/ba;->a(Lcom/a/ba;Landroid/os/Handler;)Landroid/os/Handler;

    iget-object v0, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-static {v0}, Lcom/a/ba;->e(Lcom/a/ba;)Landroid/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "passive"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-static {v0}, Lcom/a/ba;->e(Lcom/a/ba;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "passive"

    const-wide/16 v2, 0x3e8

    invoke-static {}, Lcom/a/ba;->l()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/a/p;->a:Lcom/a/ba;

    invoke-static {v5}, Lcom/a/ba;->f(Lcom/a/ba;)Landroid/location/LocationListener;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
