.class Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback$Stub$Proxy;
.super Ljava/lang/Object;
.source "ISnsImageCacheCallback.java"

# interfaces
.implements Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback;


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 75
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const-string v0, "com.sec.android.app.snsimagecache.ISnsImageCacheCallback"

    return-object v0
.end method

.method public onImageGetResponse(IZLandroid/net/Uri;ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 86
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 88
    :try_start_0
    const-string v3, "com.sec.android.app.snsimagecache.ISnsImageCacheCallback"

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    if-eqz p3, :cond_1

    .line 92
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p3, v2, v0}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    .line 98
    :goto_1
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 105
    return-void

    :cond_0
    move v0, v1

    .line 90
    goto :goto_0

    .line 96
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 103
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method
