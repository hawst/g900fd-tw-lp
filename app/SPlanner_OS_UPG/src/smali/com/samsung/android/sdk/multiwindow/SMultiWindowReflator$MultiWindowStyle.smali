.class public Lcom/samsung/android/sdk/multiwindow/SMultiWindowReflator$MultiWindowStyle;
.super Ljava/lang/Object;
.source "SMultiWindowReflator.java"


# static fields
.field static FIELD_NAMES:[Ljava/lang/String;

.field public static OPTION_SCALE:I

.field public static TYPE_CASCADE:I

.field public static TYPE_NORMAL:I

.field public static TYPE_SPLIT:I

.field public static ZONE_A:I

.field public static ZONE_B:I

.field public static ZONE_C:I

.field public static ZONE_D:I

.field public static ZONE_E:I

.field public static ZONE_F:I

.field public static ZONE_FULL:I

.field public static ZONE_UNKNOWN:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 213
    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "TYPE_NORMAL"

    aput-object v2, v1, v0

    const/4 v2, 0x1

    const-string v3, "TYPE_SPLIT"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "TYPE_CASCADE"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "ZONE_UNKNOWN"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "ZONE_A"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "ZONE_B"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "ZONE_C"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "ZONE_D"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "ZONE_E"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "ZONE_F"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "ZONE_FULL"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "OPTION_SCALE"

    aput-object v3, v1, v2

    sput-object v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowReflator$MultiWindowStyle;->FIELD_NAMES:[Ljava/lang/String;

    .line 229
    sget-object v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowReflator$MultiWindowStyle;->FIELD_NAMES:[Ljava/lang/String;

    array-length v1, v1

    .line 230
    :goto_0
    if-ge v0, v1, :cond_0

    .line 232
    :try_start_0
    const-class v2, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    sget-object v3, Lcom/samsung/android/sdk/multiwindow/SMultiWindowReflator$MultiWindowStyle;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 234
    const-class v3, Lcom/samsung/android/sdk/multiwindow/SMultiWindowReflator$MultiWindowStyle;

    sget-object v4, Lcom/samsung/android/sdk/multiwindow/SMultiWindowReflator$MultiWindowStyle;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 235
    invoke-virtual {v2, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v3, v3, v2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 241
    :cond_0
    return-void

    .line 238
    :catch_0
    move-exception v2

    goto :goto_1

    .line 237
    :catch_1
    move-exception v2

    goto :goto_1

    .line 236
    :catch_2
    move-exception v2

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
