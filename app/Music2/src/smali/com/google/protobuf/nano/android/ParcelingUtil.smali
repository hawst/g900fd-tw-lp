.class final Lcom/google/protobuf/nano/android/ParcelingUtil;
.super Ljava/lang/Object;
.source "ParcelingUtil.java"


# direct methods
.method static createFromParcel(Landroid/os/Parcel;)Lcom/google/protobuf/nano/MessageNano;
    .locals 9
    .param p0, "in"    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "className":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 47
    .local v3, "data":[B
    const/4 v6, 0x0

    .line 50
    .local v6, "proto":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 51
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    .line 52
    .local v5, "instance":Ljava/lang/Object;
    move-object v0, v5

    check-cast v0, Lcom/google/protobuf/nano/MessageNano;

    move-object v6, v0

    .line 53
    invoke-static {v6, v3}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_3

    .line 64
    .end local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "instance":Ljava/lang/Object;
    :goto_0
    return-object v6

    .line 54
    :catch_0
    move-exception v4

    .line 55
    .local v4, "e":Ljava/lang/ClassNotFoundException;
    const-string v7, "ParcelingUtil"

    const-string v8, "Exception trying to create proto from parcel"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 56
    .end local v4    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v4

    .line 57
    .local v4, "e":Ljava/lang/IllegalAccessException;
    const-string v7, "ParcelingUtil"

    const-string v8, "Exception trying to create proto from parcel"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 58
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v4

    .line 59
    .local v4, "e":Ljava/lang/InstantiationException;
    const-string v7, "ParcelingUtil"

    const-string v8, "Exception trying to create proto from parcel"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 60
    .end local v4    # "e":Ljava/lang/InstantiationException;
    :catch_3
    move-exception v4

    .line 61
    .local v4, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    const-string v7, "ParcelingUtil"

    const-string v8, "Exception trying to create proto from parcel"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
