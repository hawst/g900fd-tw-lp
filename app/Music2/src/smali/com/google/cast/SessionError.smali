.class public Lcom/google/cast/SessionError;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private c:I

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "start application"

    aput-object v1, v0, v2

    const-string v1, "create channel"

    aput-object v1, v0, v3

    const-string v1, "connect channel"

    aput-object v1, v0, v4

    const-string v1, "disconnect channel"

    aput-object v1, v0, v5

    const-string v1, "stop application"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/cast/SessionError;->a:[Ljava/lang/String;

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "network I/O timeout"

    aput-object v1, v0, v2

    const-string v1, "request failed"

    aput-object v1, v0, v3

    const-string v1, "protocol error"

    aput-object v1, v0, v4

    const-string v1, "no application is running"

    aput-object v1, v0, v5

    const-string v1, "application instance changed"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "no channel info received"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/cast/SessionError;->b:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(II)V
    .locals 2
    .param p1, "category"    # I
    .param p2, "code"    # I

    .prologue
    const/4 v1, 0x1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    if-lt p1, v1, :cond_0

    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    if-lt p2, v1, :cond_0

    const/4 v0, 0x6

    if-le p2, v0, :cond_1

    .line 60
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid category and/or code"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    iput p1, p0, Lcom/google/cast/SessionError;->c:I

    .line 63
    iput p2, p0, Lcom/google/cast/SessionError;->d:I

    .line 64
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 85
    const-string v0, "failed to %s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/cast/SessionError;->a:[Ljava/lang/String;

    iget v4, p0, Lcom/google/cast/SessionError;->c:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/cast/SessionError;->b:[Ljava/lang/String;

    iget v4, p0, Lcom/google/cast/SessionError;->d:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
